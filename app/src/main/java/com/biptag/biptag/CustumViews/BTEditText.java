package com.biptag.biptag.CustumViews;

import com.biptag.biptag.NavigationActivity;

import android.content.Context;
import android.util.Log;
import android.view.KeyEvent;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

public class BTEditText  extends EditText {
	Context mContext ; 
	public BTEditText(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
		this.mContext = context ; 
	}
	 
	@Override
	public boolean onKeyPreIme(int keyCode, KeyEvent event) {
		// TODO Auto-generated method stub
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			//Log.d("edit text", "hide keybode") ;
			NavigationActivity.keyboardDidHide() ; 
		}
		
		return false;
	}

}
