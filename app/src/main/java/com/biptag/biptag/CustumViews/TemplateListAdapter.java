package com.biptag.biptag.CustumViews;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.w3c.dom.Text;

import com.biptag.biptag.R;
import com.biptag.biptag.managers.BiptagUserManger;

import android.R.integer;
import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class TemplateListAdapter extends ArrayAdapter<String> {
	
	
	public TemplateListAdapter(Context context, int resource,
			List<String> objects) {
		super(context, resource, objects);
		// TODO Auto-generated constructor stub
	}
	



	Context  context ; 
	List<String> templates , ids ; 
	List<JSONArray> templatesAttachement ; 
	int ressousrce ;
	
	public TemplateListAdapter(Context context, int resource,
			List<String> objects , List<JSONArray> templateAttachement , List<String> templateIds) {
		super(context, resource, objects);
		// TODO Auto-generated constructor stub
		this.templates = objects ; 
		this.templatesAttachement = templateAttachement ; 
		this.context = context ; 
		this.ressousrce= ressousrce ; 
		this.ids= templateIds ; 
	}
	
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		
		View v = convertView ; 
		
		if(v== null){
			LayoutInflater inflater  = LayoutInflater.from(this.context) ; 
			v= inflater.inflate(R.layout.followup_sidebar_item, null) ; 
		}
		
		TextView templateName = (TextView) v.findViewById(R.id.followup_item_template_name) ; 
		TextView attachemntNumbe= (TextView) v.findViewById(R.id.followup_item_file_number) ; 
		ImageView attachemntIcon= (ImageView) v.findViewById(R.id.followup_item_file_icon)  ; 
		TextView id = (TextView) v.findViewById(R.id.followup_item_id) ;    
		id.setText(ids.get(position)) ;
		templateName.setText(this.templates.get(position)) ; 
		int number = 0 ; 
		try {
			JSONArray attachements = this.templatesAttachement.get(position) ; 
			for (int i = 0; i < attachements.length(); i++) {
				if(!attachements.getJSONObject(i).getString("type").equals("cc-email")){
					number ++  ; 
				}
			}
			
			attachemntNumbe.setText(String.valueOf(number)) ; 
			if(number  == 0){
				attachemntIcon.setVisibility(View.INVISIBLE) ; 
				attachemntNumbe.setVisibility(View.INVISIBLE) ; 
			}else{
				attachemntIcon.setVisibility(View.VISIBLE) ;
				attachemntNumbe.setVisibility(View.VISIBLE) ;
					
			}
			
			
		} catch (JSONException e) {
			// TODO: handle exception
		}
		
		
		v.setBackgroundColor(Color.parseColor("#ffffff")) ; 
		templateName.setTextColor(Color.parseColor("#000000")) ; 
		String selected_template = String.valueOf(BiptagUserManger.getUserValue("selected_template", "string", context)) ;
		if(selected_template != null){
			Log.d(" template color ", selected_template+ " " + ids.get(position)+" " + position );
			if(selected_template.equals(ids.get(position))){
				v.setBackgroundColor(Color.parseColor("#33b5e5")) ;
			templateName.setTextColor(Color.parseColor("#ffffff")) ;

				}

		} 
		 
		
		
		return v;
	
	
	}

}
