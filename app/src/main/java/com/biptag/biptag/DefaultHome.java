package com.biptag.biptag;

import java.util.List;
import java.util.Vector;

import android.app.ActionBar;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;

import com.biptag.biptag.fragments.home.ViewPagerAdapter;
import com.biptag.biptag.fragments.home.home_step0;
import com.biptag.biptag.fragments.home.home_step1;
import com.biptag.biptag.fragments.home.home_step2;
import com.biptag.biptag.fragments.home.home_step3;





public class DefaultHome extends FragmentActivity {

    private PagerAdapter mPagerAdapter;
    public  static DefaultHome instance ;
    ViewPager pager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            super.setContentView(R.layout.activity_main);
            instance = this ;
            ActionBar mActionBar = getActionBar();
    		mActionBar.setDisplayShowHomeEnabled(false);
    		mActionBar.setDisplayShowTitleEnabled(false);
    		LayoutInflater mInflater = LayoutInflater.from(this);

    		View mCustomView = mInflater.inflate(R.layout.custom_actionbar, null);
    		ActionBar.LayoutParams actionParams = new ActionBar.LayoutParams(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT) ;
    		mCustomView.setLayoutParams(actionParams) ; 
    		mActionBar.setCustomView(mCustomView);
    		mActionBar.setDisplayShowCustomEnabled(true);
            
            
            // Création de la liste de Fragments que fera défiler le PagerAdapter
            List fragments = new Vector();

            // Ajout des Fragments dans la liste
            fragments.add(Fragment.instantiate(this,home_step0.class.getName()));
            fragments.add(Fragment.instantiate(this,home_step1.class.getName()));
            fragments.add(Fragment.instantiate(this,home_step2.class.getName()));
            fragments.add(Fragment.instantiate(this,home_step3.class.getName()));

            // Création de l'adapter qui s'occupera de l'affichage de la liste de
            // Fragments
            this.mPagerAdapter = new ViewPagerAdapter(super.getSupportFragmentManager(), fragments);

             pager = (ViewPager) super.findViewById(R.id.obedevpanelpager);
            // Affectation de l'adapter au ViewPager
            pager.setAdapter(this.mPagerAdapter);
    }

    @Override
    protected void onStop() {
        super.onStop();
      //  this.pager=null;
      // mPagerAdapter=null;
    }

    @Override
    protected void onResume() {
        super.onResume();
    }
}
 
