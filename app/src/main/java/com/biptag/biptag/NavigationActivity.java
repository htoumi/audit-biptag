package com.biptag.biptag;

import java.util.LinkedList;
import java.util.List;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TabHost;
import android.widget.TextView;

import com.biptag.biptag.dbmanager.datasource.BTContactDataSource;
import com.biptag.biptag.dbmanager.objects.BTContact;
import com.biptag.biptag.fragements.ConnectedhomeFragment;
import com.biptag.biptag.fragements.ContactFragement;
import com.biptag.biptag.fragements.create.FillingFormStep1;
import com.biptag.biptag.fragements.edit.EditFormStep1;
import com.biptag.biptag.fragements.settings.SettingMenuFragment;
import com.biptag.biptag.managers.BiptagNavigationManager;
import com.biptag.biptag.managers.BiptagUserManger;

public class NavigationActivity extends Activity {
	 public static final String M_CURRENT_TAB = "M_CURRENT_TAB";
	    private TabHost mTabHost;
	    private String mCurrentTab;
	    public int tabWidgetHeith  = 0 ;
	    public static final String TAB_FORM = "TAB_FORM";
	    public static final String TAB_CONTACT = "TAB_CONTACT";
	    public static final String TAB_DISCONNECT = "TAB_DISCONNECT";
	    public static final String TAB_IMAGE = "TAB_IMAGE";
	    int stackIndex =  1 ;
	   public static Fragment tab1ActivFragment  ;
	   public static Fragment tab2ActivFragment  ;
	   public static Fragment tab3ActivFragment  ;
	   public static List<Fragment> stack1 ;
	   public static List<Fragment> stack2 ;
	   public static List<Fragment> stack3 ;
	   boolean  forceTab1= false ;
	   boolean forceTab2  = true ;
	   boolean forceTab3= false ;
	   static int activTav = 1  ;
	   int preivousActivTab = 1 ;
	   public static Context context ;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		NavigationActivity.context = NavigationActivity.this ;

		ActionBar mActionBar = getActionBar();
		mActionBar.setDisplayShowHomeEnabled(false);
		mActionBar.setDisplayShowTitleEnabled(false);
		LayoutInflater mInflater = LayoutInflater.from(this);

		View mCustomView = mInflater.inflate(R.layout.custom_actionbar, null);
		ActionBar.LayoutParams actionParams = new ActionBar.LayoutParams(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT) ;
		mCustomView.setLayoutParams(actionParams) ;
		mActionBar.setCustomView(mCustomView);
		mActionBar.setDisplayShowCustomEnabled(true);

		setContentView(R.layout.activity_navigation);
		 Constant.context = getApplicationContext() ;
	        mTabHost = (TabHost) findViewById(android.R.id.tabhost);
	        tabWidgetHeith = mTabHost.getHeight() ;
	        mTabHost.setup();
	        if (savedInstanceState != null) {
	            mCurrentTab = savedInstanceState.getString(M_CURRENT_TAB);
	            initializeTabs();
	            mTabHost.setCurrentTabByTag(mCurrentTab);
	            /*
	            when resume state it's important to set listener after initializeTabs
	            */
	          //  mTabHost.setOnTabChangedListener(listener);
	        } else {
	            //mTabHost.setOnTabChangedListener(listener);
	            initializeTabs();
	        }

	}
	private View createTabView(final int id, final String text , boolean showContactNumber) {
        View view = LayoutInflater.from(this).inflate(R.layout.tabbaritem, null);
        ImageView imageView = (ImageView) view.findViewById(R.id.tab_icon);
        imageView.setImageDrawable(getResources().getDrawable(id));
        TextView textView = (TextView) view.findViewById(R.id.tab_text);
        textView.setText(text);

    	int agent_id  = Integer.parseInt(String.valueOf(BiptagUserManger.getUserValue("AGENT_ID", "int", NavigationActivity.this))) ;
    	BTContactDataSource dataSource = new BTContactDataSource(NavigationActivity.this) ;
    	List<BTContact> contacts = dataSource.getAllDistincContacts(agent_id) ;
        LinearLayout tab_contact_number=(LinearLayout) view.findViewById(R.id.tab_contact_number) ;
        if(!showContactNumber || contacts.size() == 0){
        	tab_contact_number.setVisibility(View.INVISIBLE) ;
        }else{

        	TextView textView2 = (TextView) tab_contact_number.findViewById(R.id.contact_number_txt) ;
        	textView2.setText(String.valueOf(contacts.size() )) ;
        }


        return view;
    }
	 public void initializeTabs() {


		 	stack1 = new LinkedList<Fragment>() ;
		 	stack2 = new LinkedList<Fragment>() ;
		 	stack3 = new LinkedList<Fragment>() ;

		 	String userChanged = String.valueOf(BiptagUserManger.getUserValue("USER_CHANGED", "string", NavigationActivity.getContext())) ;
			if(userChanged.equals("1")){
				ContactFragement.fragmentView = null ;
				ConnectedhomeFragment.fragmentView = null ;
				BiptagUserManger.saveUserData("USER_CHANGED", "", NavigationActivity.getContext()) ;

			}

		 	TabHost.TabSpec spec;



	        spec = mTabHost.newTabSpec(TAB_FORM);
	        spec.setContent(new TabHost.TabContentFactory() {
	            public View createTabContent(String tag) {
	                return findViewById(R.id.realtabcontent);
	            }
	        }); //bottom_home_selected_btn
	        spec.setIndicator(createTabView(R.drawable.home_active, getResources().getString(R.string.my_forms), false));
	        mTabHost.addTab(spec);

	        spec = mTabHost.newTabSpec(TAB_CONTACT);
	        spec.setContent(new TabHost.TabContentFactory() {
	            public View createTabContent(String tag) {
	                return findViewById(R.id.realtabcontent);
	            }
	        });
	        spec.setIndicator(createTabView(R.drawable.user_inactive, getResources().getString(R.string.my_contacts), true));
	        mTabHost.addTab(spec);


	        spec = mTabHost.newTabSpec(TAB_DISCONNECT);
	        spec.setContent(new TabHost.TabContentFactory() {
	            public View createTabContent(String tag) {
	                return findViewById(R.id.realtabcontent);
	            }
	        });
	        spec.setIndicator(createTabView(R.drawable.setting_inactive, getResources().getString(R.string.my_settings), false));

	        mTabHost.addTab(spec);

	        mTabHost.getTabWidget().getChildAt(0).setOnTouchListener(new OnTouchListener() {

				@Override
				public boolean onTouch(View v, MotionEvent event) {
					// TODO Auto-generated method stub
					if (event.getAction() == MotionEvent.ACTION_DOWN) {

						String isSync = String.valueOf(BiptagUserManger.getUserValue("IS_SYNC", "string", getApplicationContext()));
						if (isSync.equals("")) {

							forceTab2 = true;
							forceTab1 = !forceTab1;
							preivousActivTab = activTav;
							activTav = 1;
							ConnectedhomeFragment connectedhomeFragment = new ConnectedhomeFragment();
							pushFragments(connectedhomeFragment, false, "connectedhome", 0);
							//Log.d("touch tabhost", "touch tabhost") ;
							stackIndex = 1;
						}

					}

					return true;
				}
			}) ;

	        mTabHost.getTabWidget().getChildAt(1).setOnTouchListener(new OnTouchListener() {

				@Override
				public boolean onTouch(View v, MotionEvent event) {
					// TODO Auto-generated method stub
					if(event.getAction()== MotionEvent.ACTION_DOWN){

						//Log.d("navigation tab contact", "navigation navigation tab contact touched ") ;
						String isSync = String.valueOf(BiptagUserManger.getUserValue("IS_SYNC", "string", getApplicationContext())) ;

						if(isSync.equals("")){
							//Log.d("navigation tab contact", "navigation navigation tab contact  is sync no ") ;
							forceTab2 = !forceTab2 ;
							forceTab1 = true ;
							preivousActivTab = activTav ;
							activTav = 2 ;
							//Log.d("navigation tab contact", "navigation navigation tab contact touched "+forceTab2) ;
							ContactFragement contactFragement = new ContactFragement()  ;
			                pushFragments(contactFragement,false, "contact", 1);
			                //Log.d("touch tabhost", "touch tabhost") ;
			                stackIndex = 2 ;

						}



					}
					return true;
				}
			}) ;

	        mTabHost.getTabWidget().getChildAt(2).setOnTouchListener(new OnTouchListener() {

				@Override
				public boolean onTouch(View v, MotionEvent event) {
					// TODO Auto-generated method stub

					//setting_menu_fragment


					if(event.getAction() == MotionEvent.ACTION_DOWN){

						String isSync = String.valueOf(BiptagUserManger.getUserValue("IS_SYNC", "string", getApplicationContext())) ;

						if(isSync.equals("")){


							try {
								View tab1= mTabHost.getTabWidget().getChildAt(0) ;
								ImageView imageView1 = (ImageView) tab1.findViewById(R.id.tab_icon);
						        imageView1.setImageDrawable(getResources().getDrawable(R.drawable.home_inactive));

							} catch (OutOfMemoryError e) {
								// TODO: handle exception
							}


						        try {
							        View tab2= mTabHost.getTabWidget().getChildAt(1) ;
								    ImageView imageView2 = (ImageView) tab2.findViewById(R.id.tab_icon);
								    imageView2.setImageDrawable(getResources().getDrawable(R.drawable.user_inactive));
								    LinearLayout tab_contact_number=(LinearLayout) tab2.findViewById(R.id.tab_contact_number) ;
							        TextView view  = (TextView) tab_contact_number.findViewById(R.id.contact_number_txt) ;
								    view.setBackground(getResources().getDrawable(R.drawable.black_cercle)) ;

								} catch (OutOfMemoryError e) {
									// TODO: handle exception
								}


						        try {
								    View tab3= mTabHost.getTabWidget().getChildAt(2) ;
								    ImageView imageView3 = (ImageView) tab3.findViewById(R.id.tab_icon);
								    imageView3.setImageDrawable(getResources().getDrawable(R.drawable.setting_active));

								} catch (OutOfMemoryError e) {
									// TODO: handle exception
								}

							forceTab1 = true ;
							forceTab2 = true ;
							forceTab3 =!forceTab3 ;
							preivousActivTab = activTav ;
							activTav = 3 ;

						SettingMenuFragment fragment = new SettingMenuFragment() ;
						pushFragments(fragment, false, "setting_menu" , 2) ;
						stackIndex = 3 ;
						forceTab3 =!forceTab3 ;

						}
					}
									return true;
				}
			}) ;

		 ConnectedhomeFragment connectedhomeFragment = new ConnectedhomeFragment() ;
	        stack1.add(connectedhomeFragment) ;
	        //forceTab1 = !forceTab1 ;
	        pushFragments(connectedhomeFragment,false, "connectedhome" , 0);

	    }

	    /*
	    first time listener will be trigered immediatelly after first: mTabHost.addTab(spec);
	    for set correct Tab in setmTabHost.setCurrentTabByTag ignore first call of listener
	    */


	    TabHost.OnTabChangeListener listener = new TabHost.OnTabChangeListener() {
	        public void onTabChanged(String tabId) {
/*
	            mCurrentTab = tabId;

	            if (tabId.equals(TAB_FORM)) {
	            	ConnectedhomeFragment connectedhomeFragment = new ConnectedhomeFragment() ;
	                pushFragments(connectedhomeFragment,false, null);
	            } else if (tabId.equals(TAB_CONTACT)) {
	            	ContactFragement contactFragement = new ContactFragement()  ;
	                pushFragments(contactFragement,false, null);
	            } else if (tabId.equals(TAB_DISCONNECT)) {



	            }*/

	        }
	    };

	/*
	Example of starting nested fragment from another fragment:

	Fragment newFragment = ManagerTagFragment.newInstance(tag.getMac());
	                TagsActivity tAct = (TagsActivity)getActivity();
	                tAct.pushFragments(newFragment, true, true, null);
	 */





	    public  void pushFragments(android.app.Fragment frag, boolean shouldAdd, String tag, int tabIndex) {
	        FragmentManager manager = getFragmentManager();


	        FragmentTransaction ft = manager.beginTransaction();

	        //Log.d("navigation force tab 1", "navigation force tab 1 "+forceTab1+" "+tabIndex+ 1) ;
	        if(tabIndex+ 1 == 1 ){
	        	if(forceTab1 && activTav == 1){
	        		if(stack1.size()>0){
						 Fragment fragment = stack1.get(stack1.size()-1) ;

						 boolean isEditing = false ;
						 if(fragment.getClass().getName().toUpperCase().indexOf("CREATE")!=-1||fragment.getClass().getName().toUpperCase().indexOf("EDIT")!=-1||fragment.getClass().getName().toUpperCase().indexOf("GALLERY")!=-1||fragment.getClass().getName().toUpperCase().indexOf("LISTINGFILE")!=-1)
							 isEditing = true;

	        			if(isEditing){
	        				AlertDialog.Builder builder = new AlertDialog.Builder(NavigationActivity.this) ;
							Log.d("data ", "perte des donnes " + getResources().getString(R.string.lost_data_desc));
			        		builder.setMessage(getResources().getString(R.string.lost_data_desc)) ;
			        		builder.setTitle(getResources().getString(R.string.lost_data_title)) ;
			        		builder.setCancelable(false) ;

			        		builder.setNegativeButton(getResources().getString(R.string.yes), new OnClickListener() {

								@Override
								public void onClick(DialogInterface dialog, int which) {
									// TODO Auto-generated method stub
									stack1 = new LinkedList<Fragment>() ;

									if(!tab1ActivFragment.getClass().getName().equals(ConnectedhomeFragment.class.getName())){

								        FragmentManager manager = getFragmentManager();
								        FragmentTransaction ft = manager.beginTransaction();
										FillingFormStep1.fragmentView=null;
						        		 ConnectedhomeFragment fragment = new ConnectedhomeFragment() ;
							        	 FrameLayout realtabcontent = (FrameLayout) findViewById(R.id.realtabcontent) ;
							        	 realtabcontent.removeAllViews() ;
							        	 ft.replace(R.id.realtabcontent, fragment ) ;
							        	 ft.commit();
							        	 tab1ActivFragment = fragment ;
							        	 activTav = 1 ;

									}
									stack1.add(tab1ActivFragment) ;

								try {
									View tab1= mTabHost.getTabWidget().getChildAt(0) ;
									ImageView imageView1 = (ImageView) tab1.findViewById(R.id.tab_icon);
							        imageView1.setImageDrawable(getResources().getDrawable(R.drawable.home_active));

								} catch (OutOfMemoryError e) {
									// TODO: handle exception
								}
							    try {
							        View tab2= mTabHost.getTabWidget().getChildAt(1) ;
								    ImageView imageView2 = (ImageView) tab2.findViewById(R.id.tab_icon);
								    imageView2.setImageDrawable(getResources().getDrawable(R.drawable.user_inactive));
							        LinearLayout tab_contact_number=(LinearLayout) tab2.findViewById(R.id.tab_contact_number) ;
							        TextView view  = (TextView) tab_contact_number.findViewById(R.id.contact_number_txt) ;
							        view.setBackground(getResources().getDrawable(R.drawable.black_cercle)) ;

								} catch (OutOfMemoryError e) {
									// TODO: handle exception
								}


							    try {
							        View tab3= mTabHost.getTabWidget().getChildAt(2) ;
								    ImageView imageView3 = (ImageView) tab3.findViewById(R.id.tab_icon);
								    imageView3.setImageDrawable(getResources().getDrawable(R.drawable.setting_inactive));

								} catch (OutOfMemoryError e) {
									// TODO: handle exception
								}
							    forceTab1 = !forceTab1 ;

								}
							}) ;
			        		builder.setPositiveButton(getResources().getString(R.string.no), new OnClickListener() {

								@Override
								public void onClick(DialogInterface dialog, int which) {
									// TODO Auto-generated method stub
									forceTab1 = !forceTab1 ;
									dialog.dismiss() ;
									activTav = preivousActivTab ;
								}
							});
			        		builder.show() ;

	        			}else{
       					 stack1 = new LinkedList<Fragment>() ;
	        				if(!tab1ActivFragment.getClass().getName().equals(ConnectedhomeFragment.class.getName())){
				        		 ConnectedhomeFragment fragment1 = new ConnectedhomeFragment() ;
					        	 FrameLayout realtabcontent = (FrameLayout) findViewById(R.id.realtabcontent) ;
					        	 realtabcontent.removeAllViews() ;
					        	 ft.replace(R.id.realtabcontent, fragment1 ) ;
					        	 activTav = 1 ;
					        	 tab1ActivFragment = fragment1 ;
	        				}
	        				stack1.add(tab1ActivFragment) ;

	        			try {
	        				View tab1= mTabHost.getTabWidget().getChildAt(0) ;
							ImageView imageView1 = (ImageView) tab1.findViewById(R.id.tab_icon);
					        imageView1.setImageDrawable(getResources().getDrawable(R.drawable.home_active));

						} catch (OutOfMemoryError e) {
							// TODO: handle exception
						}


	        			try {
					        View tab2= mTabHost.getTabWidget().getChildAt(1) ;
						    ImageView imageView2 = (ImageView) tab2.findViewById(R.id.tab_icon);
						    imageView2.setImageDrawable(getResources().getDrawable(R.drawable.user_inactive));
						    LinearLayout tab_contact_number=(LinearLayout) tab2.findViewById(R.id.tab_contact_number) ;
						    TextView view  = (TextView) tab_contact_number.findViewById(R.id.contact_number_txt) ;
						    view.setBackground(getResources().getDrawable(R.drawable.black_cercle)) ;

						} catch (OutOfMemoryError e) {
							// TODO: handle exception
						}

	        				try {
							    View tab3= mTabHost.getTabWidget().getChildAt(2) ;
							    ImageView imageView3 = (ImageView) tab3.findViewById(R.id.tab_icon);
							    imageView3.setImageDrawable(getResources().getDrawable(R.drawable.setting_inactive));

							} catch (OutOfMemoryError e) {
								// TODO: handle exception
							}

	        			}
	        		}else{
	        			//Log.d("stack 1", "stack 1 empty") ;
	        		}
	        	}else{
	        		int indexBack = 0 ;
	        		if(tab1ActivFragment != null){
	        			if(stack1.size()>0){
	        				frag = stack1.get(0) ;
		        			while( !tab1ActivFragment.getClass().getName().equals(frag.getClass().getName()) && indexBack < stack1.size()){

		        				frag = stack1.get(indexBack) ;
		        				indexBack++ ;
		        			}
	        			}

	        		}
	        		indexBack -- ;
	        		if(stack1.size()>1 && indexBack <stack1.size() && indexBack> -1 ){
						frag = stack1.get(indexBack) ;
	        		}else{
	        			frag = new ConnectedhomeFragment() ;

	        		}
	        			//Log.d("navigation force tab", "navigation force tab 1 "+stack1.size()+" "+frag.getClass().getName()) ;
	        		 FrameLayout realtabcontent = (FrameLayout) findViewById(R.id.realtabcontent) ;
	        		 realtabcontent.removeAllViews() ;
		        	 ft.replace(R.id.realtabcontent, frag) ;
		        	 tab1ActivFragment = frag ;
		        	 activTav = 1 ;
		        	 stack1.add(frag) ;
		        		View tab1= mTabHost.getTabWidget().getChildAt(0) ;
						ImageView imageView1 = (ImageView) tab1.findViewById(R.id.tab_icon);
				        imageView1.setImageDrawable(getResources().getDrawable(R.drawable.home_active));

				        View tab2= mTabHost.getTabWidget().getChildAt(1) ;
					    ImageView imageView2 = (ImageView) tab2.findViewById(R.id.tab_icon);
					    imageView2.setImageDrawable(getResources().getDrawable(R.drawable.user_inactive));
					    LinearLayout tab_contact_number=(LinearLayout) tab2.findViewById(R.id.tab_contact_number) ;
					    TextView view  = (TextView) tab_contact_number.findViewById(R.id.contact_number_txt) ;
					    view.setBackground(getResources().getDrawable(R.drawable.black_cercle)) ;

					    View tab3= mTabHost.getTabWidget().getChildAt(2) ;
					    ImageView imageView3 = (ImageView) tab3.findViewById(R.id.tab_icon);
					    imageView3.setImageDrawable(getResources().getDrawable(R.drawable.setting_inactive));

		        }


	        }
	        if(tabIndex+ 1 == 2 ){

	        	//Log.d("tab 2 navigation", "tab 2 navigation "+forceTab2) ;
	        	if(forceTab2){

	        		if(stack2.size()>0){

						 Fragment fragment = stack2.get(stack2.size()-1) ;
						 boolean isEditing = false ;
						 if(fragment.getClass().getName().toUpperCase().indexOf("CREATE")!=-1||fragment.getClass().getName().toUpperCase().indexOf("EDIT")!=-1 || fragment.getClass().getName().toUpperCase().indexOf("GALLERY")!=-1||fragment.getClass().getName().toUpperCase().indexOf("LISTINGFILE")!=-1)
							 isEditing = true;


						 //Log.d("tab 2 navigation", "tab 2 navigation isediting "+isEditing) ;
	        			if(isEditing){
	        				AlertDialog.Builder builder = new AlertDialog.Builder(NavigationActivity.this) ;
			        		builder.setMessage(R.string.lost_data_desc) ;
			        		builder.setTitle(R.string.lost_data_title) ;
			        		builder.setCancelable(false) ;

			        		builder.setNegativeButton(getResources().getString(R.string.yes), new OnClickListener() {

								@Override
								public void onClick(DialogInterface dialog, int which) {
									// TODO Auto-generated method stub
									stack2 = new LinkedList<Fragment>() ;
							       EditFormStep1.fragmentView = null ;
									FragmentManager manager = getFragmentManager();
							        FragmentTransaction ft = manager.beginTransaction();
					        		ContactFragement fragment = new ContactFragement() ;
						        	 FrameLayout realtabcontent = (FrameLayout) findViewById(R.id.realtabcontent) ;
						        	 realtabcontent.removeAllViews() ;
						        	 ft.replace(R.id.realtabcontent, fragment ) ;
						        	 ft.commit();
						        	activTav = 2 ;
						        	tab2ActivFragment = fragment ;
						        	stack2.add(fragment) ;

						        	View tab1= mTabHost.getTabWidget().getChildAt(0) ;
									ImageView imageView1 = (ImageView) tab1.findViewById(R.id.tab_icon);
							        imageView1.setImageDrawable(getResources().getDrawable(R.drawable.home_inactive));

							        View tab2= mTabHost.getTabWidget().getChildAt(1) ;
								    ImageView imageView2 = (ImageView) tab2.findViewById(R.id.tab_icon);
								    imageView2.setImageDrawable(getResources().getDrawable(R.drawable.user_active));
								    LinearLayout tab_contact_number=(LinearLayout) tab2.findViewById(R.id.tab_contact_number) ;
								    TextView view  = (TextView) tab_contact_number.findViewById(R.id.contact_number_txt) ;

								    view.setBackground(getResources().getDrawable(R.drawable.blue_cercle)) ;
								    View tab3= mTabHost.getTabWidget().getChildAt(2) ;
								    ImageView imageView3 = (ImageView) tab3.findViewById(R.id.tab_icon);
								    imageView3.setImageDrawable(getResources().getDrawable(R.drawable.setting_inactive));
									forceTab2 = !forceTab2 ;
								}
							}) ;
			        		builder.setPositiveButton(getResources().getString(R.string.no), new OnClickListener() {

								@Override
								public void onClick(DialogInterface dialog, int which) {
									// TODO Auto-generated method stub
									forceTab2 = !forceTab2 ;
									dialog.dismiss() ;
									activTav = preivousActivTab ;
								}
							});
			        		builder.show() ;

	        			}else{
	    					stack2 = new LinkedList<Fragment>() ;
			        		if(!tab2ActivFragment.getClass().getName().equals(ContactFragement.class.getName())){
			        			ContactFragement fragment1 = new ContactFragement() ;
					        	 FrameLayout realtabcontent = (FrameLayout) findViewById(R.id.realtabcontent) ;
					        	 realtabcontent.removeAllViews() ;
					        	 ft.replace(R.id.realtabcontent, fragment1 ) ;
					        	 activTav = 2  ;
					        	 tab2ActivFragment = fragment1 ;
			        		}
				        	 stack2.add(tab2ActivFragment) ;
				        		View tab1= mTabHost.getTabWidget().getChildAt(0) ;
								ImageView imageView1 = (ImageView) tab1.findViewById(R.id.tab_icon);
						        imageView1.setImageDrawable(getResources().getDrawable(R.drawable.home_inactive));

						        View tab2= mTabHost.getTabWidget().getChildAt(1) ;
							    ImageView imageView2 = (ImageView) tab2.findViewById(R.id.tab_icon);
							    imageView2.setImageDrawable(getResources().getDrawable(R.drawable.user_active));
							    LinearLayout tab_contact_number=(LinearLayout) tab2.findViewById(R.id.tab_contact_number) ;
							    TextView view  = (TextView) tab_contact_number.findViewById(R.id.contact_number_txt) ;
							    view.setBackground(getResources().getDrawable(R.drawable.blue_cercle)) ;


							    View tab3= mTabHost.getTabWidget().getChildAt(2) ;
							    ImageView imageView3 = (ImageView) tab3.findViewById(R.id.tab_icon);
							    imageView3.setImageDrawable(getResources().getDrawable(R.drawable.setting_inactive));

	        			}
	        		}else{


	        			frag = new ContactFragement() ;
	        		FrameLayout realtabcontent = (FrameLayout) findViewById(R.id.realtabcontent) ;
		        	 realtabcontent.removeAllViews() ;
		        	 ft.replace(R.id.realtabcontent, frag) ;
		        	 activTav = 2 ;
		        	 tab2ActivFragment = frag ;
		        	 stack2.add(frag) ;

		        		View tab1= mTabHost.getTabWidget().getChildAt(0) ;
						ImageView imageView1 = (ImageView) tab1.findViewById(R.id.tab_icon);
				        imageView1.setImageDrawable(getResources().getDrawable(R.drawable.home_inactive));

				        View tab2= mTabHost.getTabWidget().getChildAt(1) ;
					    ImageView imageView2 = (ImageView) tab2.findViewById(R.id.tab_icon);
					    imageView2.setImageDrawable(getResources().getDrawable(R.drawable.user_active));
					    LinearLayout tab_contact_number=(LinearLayout) tab2.findViewById(R.id.tab_contact_number) ;
					    TextView view  = (TextView) tab_contact_number.findViewById(R.id.contact_number_txt) ;
					    view.setBackground(getResources().getDrawable(R.drawable.blue_cercle)) ;


					    View tab3= mTabHost.getTabWidget().getChildAt(2) ;
					    ImageView imageView3 = (ImageView) tab3.findViewById(R.id.tab_icon);
					    imageView3.setImageDrawable(getResources().getDrawable(R.drawable.setting_inactive));

	        		}
	        	}else{
	        		int indexBack = 0 ;
	        		if(tab2ActivFragment != null){
	        			if(stack2.size()>0){
	        				frag = stack2.get(0) ;
		        			while( !tab2ActivFragment.getClass().getName().equals(frag.getClass().getName()) && indexBack < stack2.size()){
		        				frag = stack2.get(indexBack) ;
		        				indexBack++ ;
		        			}
	        			}
	        		}
	        		indexBack -- ;
	        		if(stack2.size()>1 && indexBack <stack2.size() && indexBack> -1 ){
						frag = stack2.get(indexBack) ;
	        		}else{
	        			frag = new ContactFragement() ;

	        		}
	        		FrameLayout realtabcontent = (FrameLayout) findViewById(R.id.realtabcontent) ;
		        	 realtabcontent.removeAllViews() ;
		        	 ft.replace(R.id.realtabcontent, frag) ;
		        	 activTav = 2 ;
		        	 tab2ActivFragment = frag ;
		        	 stack2.add(frag) ;
		        		View tab1= mTabHost.getTabWidget().getChildAt(0) ;
						ImageView imageView1 = (ImageView) tab1.findViewById(R.id.tab_icon);
				        imageView1.setImageDrawable(getResources().getDrawable(R.drawable.home_inactive));

				        View tab2= mTabHost.getTabWidget().getChildAt(1) ;
					    ImageView imageView2 = (ImageView) tab2.findViewById(R.id.tab_icon);
					    imageView2.setImageDrawable(getResources().getDrawable(R.drawable.user_active));
					    LinearLayout tab_contact_number=(LinearLayout) tab2.findViewById(R.id.tab_contact_number) ;
					    TextView view  = (TextView) tab_contact_number.findViewById(R.id.contact_number_txt) ;
					    view.setBackground(getResources().getDrawable(R.drawable.blue_cercle)) ;


					    View tab3= mTabHost.getTabWidget().getChildAt(2) ;
					    ImageView imageView3 = (ImageView) tab3.findViewById(R.id.tab_icon);
					    imageView3.setImageDrawable(getResources().getDrawable(R.drawable.setting_inactive));

	        	}


	        }
	        if(tabIndex+ 1 == 3 ){


	        	if(!forceTab3 ){
	        		int indexBack = 0 ;
        		if(tab3ActivFragment != null){
        			if(stack3.size()>0){
        				frag = stack3.get(0) ;
            			while( !tab3ActivFragment.getClass().getName().equals(frag.getClass().getName()) && indexBack < stack1.size()){

            				frag = stack3.get(indexBack) ;
            				indexBack++ ;
            			}
        			}
        		}
        		indexBack -- ;
        		if(stack3.size()>1 && indexBack <stack3.size() && indexBack> -1){
					frag = stack3.get(indexBack) ;
        		}else{
        			frag = new SettingMenuFragment() ;

        		}
	        		//Log.d("navigation tab 3", "navigation tab 3 "+forceTab3+" "+frag.getClass().getName()) ;
		        		FrameLayout realtabcontent = (FrameLayout) findViewById(R.id.realtabcontent) ;
			        	 realtabcontent.removeAllViews() ;

			        	 ft.replace(R.id.realtabcontent, frag ) ;
			        	 tab3ActivFragment = frag ;
			        activTav = 3 ;
			        stack3.add(frag) ;

	        	}else{
	        		 stack3 = new LinkedList<Fragment>() ;
	        			 //Log.d("navigation tab 3", "navigation tab 3 clearing stack 3") ;
		        		 FrameLayout realtabcontent = (FrameLayout) findViewById(R.id.realtabcontent) ;
			        	 realtabcontent.removeAllViews() ;
			        	 SettingMenuFragment fragment = new SettingMenuFragment() ;
			        	 ft.replace(R.id.realtabcontent, fragment ) ;
			        	 activTav= 3  ;
			        	 tab3ActivFragment = fragment ;

		        	 stack3.add(tab3ActivFragment) ;
	        	}

	        }



	        ft.commit() ;
	        	      //  String topEntry = manager.getBackStackEntryAt(manager.getBackStackEntryCount()-1).getName() ;
	        	      //  BiptagUserManger.saveUserData("STACK_"+stack, topEntry, context) ;

	    }



	    public static void startUrself(Activity context) {
	        Intent newActivity = new Intent(context, NavigationActivity.class);
	        newActivity.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
	        context.startActivity(newActivity);
	        context.finish();
	    }




	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		//getMenuInflater().inflate(R.menu.navigation, menu);

		//MenuInflater inflater = getMenuInflater() ;
		//inflater.inflate(R.menu.navigation, menu) ;

		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
	 /*
		   switch (item.getItemId()) {
		    // action with ID action_refresh was selected
		    case R.id.navigation_menu_contact_bar_item:
		      Toast.makeText(this, "Refresh selected", Toast.LENGTH_SHORT)
		          .show();
		      break;
		    // action with ID action_settings was selected
		    case R.id.action_settings:
		      Toast.makeText(this, "Settings selected", Toast.LENGTH_SHORT)
		          .show();
		      break;
		    default:
		      break;
		    }*/
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub




		if(stackIndex == 1){

			BiptagNavigationManager.pushFragments(tab1ActivFragment, false, tab1ActivFragment.getClass().getName(), NavigationActivity.this, null, stackIndex) ;
		}
		if(stackIndex == 2){
			BiptagNavigationManager.pushFragments(tab2ActivFragment, false, tab2ActivFragment.getClass().getName(), NavigationActivity.this, null, stackIndex) ;
		}
		if(stackIndex == 3){
			BiptagNavigationManager.pushFragments(tab3ActivFragment, false, tab3ActivFragment.getClass().getName(), NavigationActivity.this, null, stackIndex) ;
		}
		//super.onBackPressed() ;
	}

	public void updateContactNumberView(){

		View contactTab = mTabHost.getTabWidget().getChildTabViewAt(1) ;


		int agent_id  = Integer.parseInt(String.valueOf(BiptagUserManger.getUserValue("AGENT_ID", "int", NavigationActivity.this))) ;
    	BTContactDataSource dataSource = new BTContactDataSource(NavigationActivity.this) ;
    	List<BTContact> contacts = dataSource.getAllDistincContacts(agent_id) ;
        LinearLayout tab_contact_number=(LinearLayout) contactTab.findViewById(R.id.tab_contact_number) ;
        //Log.d("update contact number", "update contact number "+contacts.size()) ;
        if(contacts.size() == 0){
        	tab_contact_number.setVisibility(View.INVISIBLE) ;
        }else{
        	tab_contact_number.setVisibility(View.VISIBLE) ;
        	TextView textView2 = (TextView) tab_contact_number.findViewById(R.id.contact_number_txt) ;
        	textView2.setText(String.valueOf(contacts.size() )) ;
        }


	}

	public static Context getContext(){
		return NavigationActivity.context ;
	}

	public static void keyboardDidHide(){


		if(activTav == 1){
			if(tab1ActivFragment != null){
				if(tab1ActivFragment.getClass().getName().equals(FillingFormStep1.class.getName())){
					((FillingFormStep1) tab1ActivFragment).removeScroll() ;
				}
			}
		}

		if(activTav == 2){
			if(tab2ActivFragment != null){
				if(tab2ActivFragment.getClass().getName().equals(EditFormStep1.class.getName())){
					((EditFormStep1) tab2ActivFragment).removeScroll() ;
				}
			}
		}



	}

	public  void changeTabulationLanguage(){
		if(mTabHost !=null){
		TextView homeTextView    = (TextView)	mTabHost.getTabWidget().getChildAt(0).findViewById(R.id.tab_text) ;
		TextView contactTextView = (TextView)	mTabHost.getTabWidget().getChildAt(1).findViewById(R.id.tab_text) ;
		TextView settingTextView = (TextView)	mTabHost.getTabWidget().getChildAt(2).findViewById(R.id.tab_text) ;
		homeTextView.setText(NavigationActivity.getContext().getString(R.string.my_forms)) ;
		contactTextView.setText(NavigationActivity.getContext().getString(R.string.my_contacts)) ;
		settingTextView.setText(NavigationActivity.getContext().getString(R.string.my_settings)) ;





		}

	}







}





