package com.biptag.biptag;

import android.content.Context;

public class Constant {
	
	public static  Context context ;
public static final String base_url = "http://www.locstunisie.com/biptag-dev/ws/";
//public static final String base_url = "http://www.biptag.fr/ws-prod2";
	public static final String authentification_ws ="authentification.php" ;
	public static final String get_forms_ws ="getforms.php";
	public static final String get_contact_ws = "getcontacts.php";
	public static final String get_files_ws = "getfiles.php";
	public static final String insert_contact_ws = "insertResponse.php";
	public static final String update_contact_ws = "updateResponse.php";
	public static final String get_contacts_hash = "get_contact_hash.php";
	public static final String get_contact_with_ids = "get_contact_with_ids.php";
	public static final String insert_followup = "insert_followup.php";
	public static final String update_followup = "update_followup.php";
	public static final String get_templates_whth_hashcode = "get_templates_whth_hashcode.php";
	public static final String get_templates_with_ids = "get_templates_with_ids.php";
	public static final String select_logs = "select_logs.php";
	public static final String signup = "signup.php";
	public static final String publish_form = "publishform.php";
	public static final String send_mail = "send_mail.php";
	public static final String get_available_countries = "get_available_countries.php";
	public static final String accept_images="accept_images.php";
}
