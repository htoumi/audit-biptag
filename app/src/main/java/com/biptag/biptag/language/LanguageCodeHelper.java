package com.biptag.biptag.language;

import android.content.Context;
import android.content.res.Resources;
import android.util.Log;

//import eniso.ii.pfe.myocrapp.R;
import com.biptag.biptag.R;
import com.biptag.biptag.ocr.CaptureActivity;

public class LanguageCodeHelper {
	public static final String TAG = "LanguageCodeHelper";
	 /*private constructor */
		private LanguageCodeHelper(){
			throw new AssertionError();
		}
		/**
		 * Map an ISO 639-3 language code to an ISO 639-1 language code.
		 * 
		 * There is one entry here for each language recognized by the OCR engine.
		 * 
		 * param languageCode
		 *            ISO 639-3 language code
		 * return ISO 639-1 language code
		 */
		public static String mapLanguageCode(String languageCode) {	  
		 if (languageCode.equals("ara")) { // Arabic
		    return "ar";
	    } else if (languageCode.equals("eng")) { // English
	      return "en";
	    } else if (languageCode.equals("fra")) { // French
	      return "fr";
	     } else {
		    return "";
		  }
		}

		public static String getOcrLanguageName(Context context, String languageCode) {
			Resources res = context.getResources();
			String[] language6393 = res.getStringArray(R.array.iso6393);
			String[] languageNames = res.getStringArray(R.array.languagenames);
			int len;

			// Finds the given language code in the iso6393 array, and takes the name with the same index
			// from the languagenames array.
			for (len = 0; len < language6393.length; len++) {
				if (language6393[len].equals(languageCode)) {
					//Log.d(TAG, "getOcrLanguageName: " + languageCode + "->" + languageNames[len]);
					return languageNames[len];
				}
			}
			
			//Log.d(TAG, "languageCode: Could not find language name for ISO 693-3: " + languageCode);
			return languageCode;
		}
}
