package com.biptag.biptag.dbmanager.objects;

public class BTAvailableCountry {
	
	  int id ; 
	  String name ; 
	  String file_name  ; 
	  String last_update ; 
	  
	  public BTAvailableCountry() {
		// TODO Auto-generated constructor stub
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getFile_name() {
		return file_name;
	}

	public void setFile_name(String file_name) {
		this.file_name = file_name;
	}

	public String getLast_update() {
		return last_update;
	}

	public void setLast_update(String last_update) {
		this.last_update = last_update;
	} 
	  
	  
	

}
