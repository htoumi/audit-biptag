package com.biptag.biptag.dbmanager.datasource;

import java.util.ArrayList;
import java.util.List;

import android.R.bool;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.biptag.biptag.dbmanager.MySQLiteHelper;
import com.biptag.biptag.dbmanager.objects.BTAvailableCountry;
import com.biptag.biptag.dbmanager.objects.BTCity;
import com.biptag.biptag.dbmanager.objects.BTContact;

public class BTAvailableCountryDataSource {
	
	  private SQLiteDatabase database;
	  private MySQLiteHelper dbHelper;
	
	
	  private String[] allColumns = { MySQLiteHelper.AVAILBLE_COUNTRIES_COLUMN_ID,
			  MySQLiteHelper.AVAILBLE_COUNTRIES_COLUMN_NAME,
			  MySQLiteHelper.AVAILBLE_COUNTRIES_COLUMN_FILE_NAME,
			  MySQLiteHelper.AVAILBLE_COUNTRIES_COLUMN_LAST_UPDATE,
			  };
	 
	  public BTAvailableCountryDataSource(Context  context) {
		  dbHelper = new MySQLiteHelper(context);
	  }
	  private void open() throws SQLException {
	    database = dbHelper.getWritableDatabase();
	  }
	
	  private void close() {
	    dbHelper.close();
	  }
	  
	  
	  public long insertCountry(BTAvailableCountry country){
		  
		  this.open() ; 
		  long insertId = 0 ; 
		  try{
			  ContentValues values = new ContentValues() ; 
			  values.put(MySQLiteHelper.AVAILBLE_COUNTRIES_COLUMN_NAME, country.getName()) ; 
			  values.put(MySQLiteHelper.AVAILBLE_COUNTRIES_COLUMN_FILE_NAME, country.getFile_name()) ; 
			  values.put(MySQLiteHelper.AVAILBLE_COUNTRIES_COLUMN_LAST_UPDATE, country.getLast_update()) ; 
			  insertId = database.insert(MySQLiteHelper.AVAILBLE_COUNTRY_TABLE_NAME, null,
				        values);
				    
				    
		  }finally{
			  this.close() ; 
		  }
		  
		  return insertId  ; 
		  
	  }
	  
	  public List<BTAvailableCountry> selectAllAvailableCountry(){
		  this.open() ; 
		  List<BTAvailableCountry> countries= new ArrayList<BTAvailableCountry>();
		  Cursor cursor  = null  ; 		
		 cursor = database.query(MySQLiteHelper.AVAILBLE_COUNTRY_TABLE_NAME,
				    		allColumns , null , null, null, null, null);
				    
		  		
		    
		   try {
			   if(cursor!= null){ 
				    cursor.moveToFirst();
				    while (!cursor.isAfterLast()) {
				      BTAvailableCountry country= cursorToCountry(cursor);
				      if(country!= null)
				      countries.add(country);
				      cursor.moveToNext();
				    }
				    // make sure to close the cursor
				  }
		}
		finally{
			   cursor.close() ; 
			   this.close() ; 
		}
		    
		    return countries; 
	  }
	   
	  
	  public BTAvailableCountry cursorToCountry(Cursor cursor){
		  BTAvailableCountry country = new BTAvailableCountry() ; 
		  country.setId(cursor.getInt(0)) ; 
		  country.setName(cursor.getString(1))  ; 
		  country.setFile_name(cursor.getString(2)) ; 
		  country.setLast_update(cursor.getString(3)) ; 
		  return country ; 
	  }
	  
	  public void createCountryTable(String countryName ){
		  
		  this.open() ;  
		  try{
			  this.database.execSQL("create table if not exists "+countryName+" ( ville text  , code_postal  text , is_user_entry integer )") ;
			    
		  }finally{
			  this.close() ; 
		  }
		  
	  }
	  
	  
	  public void insertCityForCountry(String tableName , String  city , String zipcode, int isUserEntry){
		  //ville text  , code_postal  text , is_user_entry
		  long id = 0 ; 
		 try{
			 this.open() ;
		  ContentValues  values = new ContentValues() ; 
		  values.put("ville", city) ; 
		  values.put("code_postal", zipcode) ; 
		  values.put("is_user_entry", isUserEntry) ; 
		   
		 id = this.database.insert(tableName, null, values) ; 
		 }finally{
			 this.close() ; 
		 }
		  
	  }
	  
	  
	  public List<BTCity> selectCities(String tableName){
		  this.open() ; 
		  Cursor cursor  = null  ; 		
			 cursor = database.query(tableName,
					    		new String[]{"ville" , "code_postal"   , "is_user_entry"} , null , null, null, null, null);
			 List<BTCity> list = new ArrayList<BTCity>() ; 
			 
			 try{
				 if(cursor!= null){ 
					    cursor.moveToFirst();
					    while (!cursor.isAfterLast()) {
					    	BTCity city = new BTCity() ; 
					    	city.setCityName(cursor.getString(0)) ; 
					    	city.setZipCode(cursor.getString(1)) ; 
					    	city.setUserEntry(cursor.getInt(2)== 1) ; 
					    	list.add(city) ; 
					    	cursor.moveToNext();
					    }
					    // make sure to close the cursor
					  }
			 }finally{
				 cursor.close() ;
				 this.close() ; 
			 }
			 
			 return list ; 
				    
	  }
	   
	  
  
}
