package com.biptag.biptag.dbmanager.objects;

import org.json.JSONArray;

public class BTFormulaire {
	private String formName ; 
	private JSONArray formStructure ; 
	private String eventName ; 
	private String eventStartDate ; 
	private String eventEndDate ; 
	private String eventEdition ; 
	private String formStatus ; 
	private int agentId ; 
	private int formId ; 
	
	
	
	public BTFormulaire() {
		// TODO Auto-generated constructor stub
	}



	public String getFormName() {
		return formName;
	}



	public void setFormName(String formName) {
		this.formName = formName;
	}



	public JSONArray getFormStructure() {
		return formStructure;
	}



	public void setFormStructure(JSONArray formStructure) {
		this.formStructure = formStructure;
	}



	public String getEventName() {
		return eventName;
	}



	public void setEventName(String eventName) {
		this.eventName = eventName;
	}



	public String getEventStartDate() {
		return eventStartDate;
	}



	public void setEventStartDate(String eventStartDate) {
		this.eventStartDate = eventStartDate;
	}



	public String getEventEndDate() {
		return eventEndDate;
	}



	public void setEventEndDate(String eventEndDate) {
		this.eventEndDate = eventEndDate;
	}



	public String getEventEdition() {
		return eventEdition;
	}



	public void setEventEdition(String eventEdition) {
		this.eventEdition = eventEdition;
	}



	public String getFormStatus() {
		return formStatus;
	}



	public void setFormStatus(String formStatus) {
		this.formStatus = formStatus;
	}



	public int getAgentId() {
		return agentId;
	}



	public void setAgentId(int agentId) {
		this.agentId = agentId;
	}



	public int getFormId() {
		return formId;
	}



	public void setFormId(int formId) {
		this.formId = formId;
	}
	
	

}
