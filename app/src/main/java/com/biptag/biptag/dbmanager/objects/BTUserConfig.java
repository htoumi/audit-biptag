package com.biptag.biptag.dbmanager.objects;

import android.content.ContentValues;
import android.content.Context;
import android.util.Log;

import com.biptag.biptag.dbmanager.datasource.BtUserConfigDataSource;

public class BTUserConfig {
	private int agent_id ; 
	private String language ; 
	private String defaultCountry ; 
	private long configId ; 

	
	public BTUserConfig() {
		// TODO Auto-generated constructor stub
	}
	
	
	
	
	
	

	public long getConfigId() {
		return configId;
	}




	public void setConfigId(long configId) {
		this.configId = configId;
	}




	public int getAgent_id() {
		return agent_id;
	}

	public void setAgent_id(int agent_id) {
		this.agent_id = agent_id;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getDefaultCountry() {
		return defaultCountry;
	}

	public void setDefaultCountry(String defaultCountry) {
		this.defaultCountry = defaultCountry;
	}
	
	
	
	
	
	
	
	

}
