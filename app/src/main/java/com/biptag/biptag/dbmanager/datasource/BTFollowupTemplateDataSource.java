package com.biptag.biptag.dbmanager.datasource;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;

import android.content.ContentValues;
import android.content.Context; 
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.biptag.biptag.dbmanager.MySQLiteHelper;
import com.biptag.biptag.dbmanager.objects.BTFollowupTemplate;

public class BTFollowupTemplateDataSource {
	 private SQLiteDatabase database;
	  private MySQLiteHelper dbHelper;
	
	  /*fu_id integer , ba_id integer , fu_content text  ,  fu_subject text , fu_attach text , fu_temp_name text 
		  * , is_synchronized integer , fu_local_id , fu_hashcode
			*/
	private String[] allcolumns =new String[]{
			MySQLiteHelper.FOLLOW_UP_COLUMN_FU_ID ,
			MySQLiteHelper.FOLLOW_UP_COLUMN_BA_ID ,
			MySQLiteHelper.FOLLOW_UP_COLUMN_FU_CONTENT ,
			MySQLiteHelper.FOLLOW_UP_COLUMN_FU_SUBJECT ,
			MySQLiteHelper.FOLLOW_UP_COLUMN_FU_ATTACH ,
			MySQLiteHelper.FOLLOW_UP_COLUMN_FU_NAME ,
			MySQLiteHelper.FOLLOW_UP_COLUMN_IS_SYNC , 
			MySQLiteHelper.FOLLOW_UP_COLUMN_LOCAl_ID , 
			MySQLiteHelper.FOLLOW_UP_COLUMN_FU_HASHCODE 
	} ; 

	public BTFollowupTemplateDataSource(Context  context) {
		// TODO Auto-generated constructor stub
	    dbHelper = new MySQLiteHelper(context);
			  }
		  private void open() throws SQLException {
			    database = dbHelper.getWritableDatabase();
			  }
			
		  private void close() {
			    dbHelper.close();
			  }
		  
		  
		  public long insertTemplate(BTFollowupTemplate template){
			  
			  this.open() ; 
			  long insertId= 0  ;

			  
			  try{
				  ContentValues values = new ContentValues();

				  values.put(MySQLiteHelper.FOLLOW_UP_COLUMN_BA_ID, template.getBa_id()) ;
				  values.put(MySQLiteHelper.FOLLOW_UP_COLUMN_FU_ATTACH, template.getFu_attach().toString()) ;
				  values.put(MySQLiteHelper.FOLLOW_UP_COLUMN_FU_CONTENT, template.getFu_content()) ;
				  values.put(MySQLiteHelper.FOLLOW_UP_COLUMN_FU_HASHCODE, template.getFu_hashcode()) ;
				  values.put(MySQLiteHelper.FOLLOW_UP_COLUMN_FU_ID, template.getFu_id()) ;
				  values.put(MySQLiteHelper.FOLLOW_UP_COLUMN_FU_NAME, template.getTemp_name()) ;
				  values.put(MySQLiteHelper.FOLLOW_UP_COLUMN_FU_SUBJECT, template.getFu_subject()) ;
				  values.put(MySQLiteHelper.FOLLOW_UP_COLUMN_IS_SYNC, template.isSynchonized()) ;
				  
				  insertId = database.insert(MySQLiteHelper.TABLE_FOLLOW_UPS, null,
					        values);
				  Log.d(" template  saved  ",insertId+"" );
			  }finally{
				  this.close() ; 
			  }
			  
				    	return insertId ; 
				    
				
		  }
		  
		  
		  public boolean updateTemplate(BTFollowupTemplate template){
			  
			  long insertId  = 0; 
			  
			  this.open() ; 
			  
			  try{

				  ContentValues values = new ContentValues();

				  values.put(MySQLiteHelper.FOLLOW_UP_COLUMN_BA_ID, template.getBa_id()) ;
				  values.put(MySQLiteHelper.FOLLOW_UP_COLUMN_FU_ATTACH, template.getFu_attach().toString()) ;
				  values.put(MySQLiteHelper.FOLLOW_UP_COLUMN_FU_CONTENT, template.getFu_content()) ;
				  values.put(MySQLiteHelper.FOLLOW_UP_COLUMN_FU_HASHCODE, template.getFu_hashcode()) ;
				  values.put(MySQLiteHelper.FOLLOW_UP_COLUMN_FU_ID, template.getFu_id()) ;
				  values.put(MySQLiteHelper.FOLLOW_UP_COLUMN_FU_NAME, template.getTemp_name()) ;
				  values.put(MySQLiteHelper.FOLLOW_UP_COLUMN_FU_SUBJECT, template.getFu_subject()) ;
				  values.put(MySQLiteHelper.FOLLOW_UP_COLUMN_IS_SYNC, template.isSynchonized()) ;
				  insertId = database.update(MySQLiteHelper.TABLE_FOLLOW_UPS, values, MySQLiteHelper.FOLLOW_UP_COLUMN_LOCAl_ID+"= ?", new String[]{String.valueOf(template.getLocal_id()) }) ; 
				  
			  }finally{
				  this.close() ; 
			  }
			  
				    if(insertId!= 0){
				    	//Log.d("Insert form ", "tempalte updated OK ") ;
				    	return true ; 
				    }else{
				    	//Log.d("Insert form ", "tempalte updated KO ") ;
				    	return false ; 
				    }
				
		  }
		  
		  
		  
		  public List<BTFollowupTemplate> getAllTempalte( int ba_id) {
			  this.open() ; 
			  List<BTFollowupTemplate> templates= new ArrayList<BTFollowupTemplate>();
			  Cursor cursor  = null  ; 		
	  			cursor = database.query(MySQLiteHelper.TABLE_FOLLOW_UPS,
			    		allcolumns , MySQLiteHelper.FOLLOW_UP_COLUMN_BA_ID+"= ? ",
			    		new String[]{String.valueOf(ba_id)}, null, null, null);
	   
	  			try{
				    if(cursor!= null){ 
					    cursor.moveToFirst();
					    while (!cursor.isAfterLast()) {
					      BTFollowupTemplate template= cursorToTemplate(cursor);
							Log.d("ba id db", "ba id db " + template.getBa_id() + "  " + template.getTemp_name() + "  ba id param " + ba_id +"  "+template.getLocal_id()) ;

							if(template!= null)
					      templates.add(template);
					      cursor.moveToNext();
					    }
					    // make sure to close the cursor
					  }
	  			}finally{
	  				cursor.close() ; 
	  				this.close() ; 
	  			}
			    
			    return templates;
			   
		  
		  }
		  
		  
		  public BTFollowupTemplate getTemplateWithID( int ba_id,  int fu_id) {
			  this.open() ;

			  Cursor cursor = database.query(MySQLiteHelper.TABLE_FOLLOW_UPS,
					    		allcolumns , MySQLiteHelper.FOLLOW_UP_COLUMN_BA_ID+"= ? AND "+MySQLiteHelper.FOLLOW_UP_COLUMN_FU_ID+"= ?",
					    		new String[]{String.valueOf(ba_id) , String.valueOf(fu_id)}, null, null, null);
					     		
			  BTFollowupTemplate  followupTemplate= null ;
			    try{
			    	if(cursor!= null){
						Log.d(" cursor template ", " not null "  + " " +cursor.getCount());
						cursor.moveToFirst();
					    while (!cursor.isAfterLast()) {
					      followupTemplate= cursorToTemplate(cursor);
							Log.d(" curso template ", " not null " + followupTemplate);
							cursor.moveToNext();
					    }
					    // make sure to close the cursor
					  }
			    }finally{
			    	cursor.close() ; 
			    	this.close() ; 
			    }
//			  Log.d(" cursor template ", followupTemplate.toString());
			    return followupTemplate;
				    
		  }
			   
		  
		  
		  public BTFollowupTemplate getTemplateWithLocalID( int ba_id,  int fu_id) {
			  this.open() ; 
			  Cursor cursor = database.query(MySQLiteHelper.TABLE_FOLLOW_UPS,
					    		allcolumns ,MySQLiteHelper.FOLLOW_UP_COLUMN_BA_ID+"= ? AND "+MySQLiteHelper.FOLLOW_UP_COLUMN_LOCAl_ID+"= ?",
					    		new String[]{String.valueOf(ba_id) , String.valueOf(fu_id)}, null, null, null);
					     		
			  BTFollowupTemplate  followupTemplate= null ;
			    try{
			    	if(cursor!= null){ 
					    cursor.moveToFirst();
					    while (!cursor.isAfterLast()) {
							Log.d("ba id db" , "ba id db getTemplateWithLocalID "+cursor.getInt(1)) ;

							followupTemplate= cursorToTemplate(cursor);
							Log.d("ba id db" , "ba id db getTemplateWithLocalID "+followupTemplate.getBa_id()+ " "+followupTemplate.getLocal_id()) ;
						  	cursor.moveToNext();
					    }
					    // make sure to close the cursor
					  }
			    }finally{
			    	cursor.close() ; 
			    	this.close() ; 
			    }
			    return followupTemplate;
				    
		  }
		  
		  
		  
		  
		  public BTFollowupTemplate cursorToTemplate(Cursor cursor){
			 
		 /*fu_id integer , ba_id integer , fu_content text  ,  fu_subject text , fu_attach text , fu_temp_name text 
		  * , is_synchronized integer , fu_local_id , fu_hashcode
			*/
			  try {
				 
				  BTFollowupTemplate template= new BTFollowupTemplate() ;
				  template.setFu_id(cursor.getInt(0)) ;
				  template.setBa_id(cursor.getInt(1)) ; 
				  template.setFu_content(cursor.getString(2)); 
				  template.setFu_subject(cursor.getString(3))  ; 
				  template.setFu_attach(new JSONArray(cursor.getString(4))); 
				  template.setTemp_name(cursor.getString(5)) ; 
				  template.setSynchonized(cursor.getInt(6) ==  1);
				  template.setLocal_id(cursor.getInt(7)) ; 
				  template.setFu_hashcode(cursor.getString(8)) ; 
				  return template; 
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				
				Log.d("contact response db", "ba id  db "+e.getMessage()) ;
				e.printStackTrace();
				return null  ; 
			} 
			  
		  }
		  
		  
	
}
