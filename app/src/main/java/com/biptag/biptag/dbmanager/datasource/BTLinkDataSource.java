package com.biptag.biptag.dbmanager.datasource;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import com.biptag.biptag.dbmanager.MySQLiteHelper;
import com.biptag.biptag.dbmanager.objects.BTLink;

public class BTLinkDataSource {

	 private SQLiteDatabase database;
	  private MySQLiteHelper dbHelper;
	
	private String[] allcolumns =new String[]{
			MySQLiteHelper.LINK_COLUMN_ID ,
			MySQLiteHelper.LINK_COLUMN_URL,
			MySQLiteHelper.LINK_COLUMN_LABEL,
			MySQLiteHelper.LINK_COLUMN_BA_ID
	} ; 

	public BTLinkDataSource(Context  context) {
		// TODO Auto-generated constructor stub
	    dbHelper = new MySQLiteHelper(context);
			  }
		  private void open() throws SQLException {
			    database = dbHelper.getWritableDatabase();
			  }
			
		  private void close() {
			    dbHelper.close();
			  }
		  
		  
		  public boolean insertLink(BTLink link){
			  
			  long id = 0 ; 
			  
			  try{
				  this.open() ; 
				  
				  ContentValues values = new ContentValues() ; 
				  values.put(MySQLiteHelper.LINK_COLUMN_ID, link.getId()) ; 
				  values.put(MySQLiteHelper.LINK_COLUMN_URL, link.getUrl()) ; 
				  values.put(MySQLiteHelper.LINK_COLUMN_LABEL, link.getLable()) ; 
				  values.put(MySQLiteHelper.LINK_COLUMN_BA_ID, link.getBaID()) ; 
				  
				  
				  id = this.database.insert(MySQLiteHelper.TABLE_LINKS, null, values) ; 
				  
			  }finally{
				  this.close() ; 
			  }
			  return id!=0 ; 
			  
			  
		  }
		  
		  
		  
		  public BTLink getLink(int ba_id , int id){
			  BTLink link = null ; 
			  Cursor cursor = null ; 
			  
			  try{
				  this.open() ; 
				  cursor = this.database.query(true, MySQLiteHelper.TABLE_LINKS, allcolumns, "ba_id = ? and id = ?", new String[]{String.valueOf(ba_id), String.valueOf(id)}, null, null, null, null, null) ; 
				  
				  if(cursor != null){
					  cursor.moveToFirst() ; 
					  while(!cursor.isAfterLast()){
						  link = cursorToLink(cursor) ; 
						  cursor.moveToNext() ; 
					  }
				  }
				  
				  
			  }finally{
				  if(cursor!=null){
					  cursor.close()  ; 
				  }
				  
				  this.close() ; 
			  }
			  

			  
			  return link; 
		  }
		  
		  
		  public List<BTLink> selectLinks(int ba_id){
			  List<BTLink> links = new LinkedList<BTLink>() ; 
			  
			  Cursor cursor = null ; 
			  
			  try{
				  this.open() ; 
				  cursor = this.database.query(true, MySQLiteHelper.TABLE_LINKS, allcolumns, "ba_id = ?", new String[]{String.valueOf(ba_id)}, null, null, null, null, null) ; 
				  
				  if(cursor != null){
					  cursor.moveToFirst() ; 
					  
					  while(!cursor.isAfterLast()){
						  
						  BTLink link = cursorToLink(cursor) ; 
						  
						  links.add(link) ; 
						  
					  }
				  }
				  
				  
			  }finally{
				  if(cursor!=null){
					  cursor.close()  ; 
				  }
				  
				  this.close() ; 
			  }
			  
			  
			  
			  
			  return links; 
		  }
		private BTLink cursorToLink(Cursor cursor) {
			// TODO Auto-generated method stub
			
				/*private String[] allcolumns =new String[]{
			MySQLiteHelper.LINK_COLUMN_ID ,
			MySQLiteHelper.LINK_COLUMN_URL,
			MySQLiteHelper.LINK_COLUMN_LABEL,
			MySQLiteHelper.LINK_COLUMN_BA_ID
	} ; */
			
		BTLink link = new BTLink() ; 
		link.setId(cursor.getInt(0)) ; 
		link.setUrl(cursor.getString(1)) ; 
		link.setLable(cursor.getString(2)) ; 
		link.setBaID(cursor.getInt(3)) ; 
			return link;
		}
		  
	
	
}
