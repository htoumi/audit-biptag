package com.biptag.biptag.dbmanager.objects;

public class BTLog {
	/*   fl_contact_id integer ,  agent_id integer */
	private int log_id  ; 
	private String log_type ; 
	private String log_timestamps ; 
	private String log_rel_object ; 
	private int log_rel_object_id ;
	private String log_rel_object_2 ; 
	private int log_rel_object_2_id ;
	private String fl_subject ;
	private String fl_content ; 
	private String fl_send_date ; 
	private int fl_id ; 
	private int fl_contact_id ;
	private int fl_agent_id ;
	
	public BTLog() {
		// TODO Auto-generated constructor stub
	}

	public int getLog_id() {
		return log_id;
	}

	public void setLog_id(int log_id) {
		this.log_id = log_id;
	}

	public String getLog_type() {
		return log_type;
	}

	public void setLog_type(String log_type) {
		this.log_type = log_type;
	}

	public String getLog_timestamps() {
		return log_timestamps;
	}

	public void setLog_timestamps(String log_timestamps) {
		this.log_timestamps = log_timestamps;
	}

	public String getLog_rel_object() {
		return log_rel_object;
	}

	public void setLog_rel_object(String log_rel_object) {
		this.log_rel_object = log_rel_object;
	}

	public int getLog_rel_object_id() {
		return log_rel_object_id;
	}

	public void setLog_rel_object_id(int log_rel_object_id) {
		this.log_rel_object_id = log_rel_object_id;
	}

	public String getLog_rel_object_2() {
		return log_rel_object_2;
	}

	public void setLog_rel_object_2(String log_rel_object_2) {
		this.log_rel_object_2 = log_rel_object_2;
	}

	public int getLog_rel_object_2_id() {
		return log_rel_object_2_id;
	}

	public void setLog_rel_object_2_id(int log_rel_object_2_id) {
		this.log_rel_object_2_id = log_rel_object_2_id;
	}

	public String getFl_subject() {
		return fl_subject;
	}

	public void setFl_subject(String fl_subject) {
		this.fl_subject = fl_subject;
	}

	public String getFl_content() {
		return fl_content;
	}

	public void setFl_content(String fl_content) {
		this.fl_content = fl_content;
	}

	public String getFl_send_date() {
		return fl_send_date;
	}

	public void setFl_send_date(String fl_send_date) {
		this.fl_send_date = fl_send_date;
	}

	public int getFl_id() {
		return fl_id;
	}

	public void setFl_id(int fl_id) {
		this.fl_id = fl_id;
	}

	public int getFl_contact_id() {
		return fl_contact_id;
	}

	public void setFl_contact_id(int fl_contact_id) {
		this.fl_contact_id = fl_contact_id;
	}

	public int getFl_agent_id() {
		return fl_agent_id;
	}

	public void setFl_agent_id(int fl_agent_id) {
		this.fl_agent_id = fl_agent_id;
	}
	
	
	
}
