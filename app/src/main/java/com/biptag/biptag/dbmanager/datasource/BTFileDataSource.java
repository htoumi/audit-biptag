package com.biptag.biptag.dbmanager.datasource;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import com.biptag.biptag.dbmanager.MySQLiteHelper;
import com.biptag.biptag.dbmanager.objects.BTFile;

public class BTFileDataSource {

	 private SQLiteDatabase database;
	  private MySQLiteHelper dbHelper;
	
	private String[] allcolumns =new String[]{
			MySQLiteHelper.FILE_COLUMN_ID ,
			MySQLiteHelper.FILE_COLUMN_NAME,
			MySQLiteHelper.FILE_COLUMN_PH_NAME,
			MySQLiteHelper.FILE_COLUMN_BA_ID
	} ; 

	public BTFileDataSource(Context  context) {
		// TODO Auto-generated constructor stub
	    dbHelper = new MySQLiteHelper(context);
			  }
		  private void open() throws SQLException {
			    database = dbHelper.getWritableDatabase();
			  }
			
		  private void close() {
			    dbHelper.close();
			  }
		  
	
	public boolean insertFile(BTFile file){
		
		long id =  0 ; 
		try{
			this.open() ; 
			ContentValues values = new ContentValues()  ; 
			values.put(MySQLiteHelper.FILE_COLUMN_ID, file.getId()) ; 
			values.put(MySQLiteHelper.FILE_COLUMN_NAME, file.getName()) ; 
			values.put(MySQLiteHelper.FILE_COLUMN_PH_NAME, file.getPhName()) ; 
			values.put(MySQLiteHelper.FILE_COLUMN_BA_ID, file.getBaId()) ; 
			
			
			id = this.database.insert(MySQLiteHelper.TABLE_FILES, null, values) ; 
			
			
		}finally{
			this.close() ; 
		}
		
		return id != 0 ; 

	}
	
	
	public List<BTFile> selectFiles(int ba_id){
		
		List<BTFile> files = new LinkedList<BTFile>() ; 
		
		Cursor cursor = null ; 
		try{
			this.open() ; 
			
			cursor = this.database.query(true, MySQLiteHelper.TABLE_FILES, allcolumns, " ba_id = ? ", new String[]{String.valueOf(ba_id)}, null, null, null, null, null) ; 
			if(cursor!= null){
				cursor.moveToFirst() ; 
				
				while(!cursor.isAfterLast()){
					
					BTFile file = cursorToFile(cursor) ; 
					files.add(file) ; 
					cursor.moveToNext() ; 
				}
			}
			
		}finally{
			if(cursor !=null)
				cursor.close() ; 
			this.close() ; 
		}		
		return files ; 
		
		
	}
	
		public BTFile getFile(int ba_id , int id ){
		
		BTFile file = null; 
		
		Cursor cursor = null ; 
		try{
			this.open() ; 
			
			cursor = this.database.query(true, MySQLiteHelper.TABLE_FILES, allcolumns, " ba_id = ? and id = ? ", new String[]{String.valueOf(ba_id), String.valueOf(id)}, null, null, null, null, null) ; 
			if(cursor!= null){
				cursor.moveToFirst() ; 
				
				while(!cursor.isAfterLast()){
					
					file = cursorToFile(cursor) ; 
					cursor.moveToNext() ; 
				}
			}
			
		}finally{
			if(cursor !=null)
				cursor.close() ; 
			this.close() ; 
		}		
		
		return file ; 
		
		
	}
	
	
	private BTFile cursorToFile(Cursor cursor) {
		// TODO Auto-generated method stub
		
		BTFile file = new BTFile() ; 
		
		
		file.setId(cursor.getInt(0)) ; 
		file.setName(cursor.getString(1)) ; 
		file.setPhName(cursor.getString(2)) ; 
		file.setBaId(cursor.getInt(3)) ; 
		
		
		
		return file;
	}
	
	
	
	
	
	
}
