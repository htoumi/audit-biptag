package com.biptag.biptag.dbmanager.datasource;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.biptag.biptag.dbmanager.MySQLiteHelper;
import com.biptag.biptag.dbmanager.objects.BTContact;

public class BTContactDataSource {
	 private SQLiteDatabase database;
	  private MySQLiteHelper dbHelper;
	
	
	  private String[] allColumns = { MySQLiteHelper.CONTACT_COLUMN_LOCAL_ID,
			  MySQLiteHelper.CONTACT_COLUMN_EMAIL,
			  MySQLiteHelper.CONTACT_COLUMN_ID,
			  MySQLiteHelper.CONTACT_COLUMN_FORM_ID,
			  MySQLiteHelper.CONTACT_COLUMN_FR_ID,
			  MySQLiteHelper.CONTACT_COLUMN_FR_HASHCODE,
			  MySQLiteHelper.CONTACT_COLUMN_AGENT_ID,
			  MySQLiteHelper.CONTACT_COLUMN_FR_CONTENT,
			  MySQLiteHelper.CONTACT_COLUMN_IS_SYNCHRONIZED,
			  MySQLiteHelper.CONTACT_COLUMN_DO_FOLLOW_UP ,
			  MySQLiteHelper.CONTACT_COLUMN_NAME ,
			  MySQLiteHelper.CONTACT_COLUMN_CC_EMAIL,
			  MySQLiteHelper.CONTACT_COLUMN_CCI_EMAIL,
			  MySQLiteHelper.CONTACT_COLUMN_DATE
			  };
	  
	  
	  
	  		public BTContactDataSource(Context  context) {
		// TODO Auto-generated constructor stub
		    dbHelper = new MySQLiteHelper(context);
				  }
			  private void open() throws SQLException {
				    database = dbHelper.getWritableDatabase();
				  }
				
			  private void close() {
				    dbHelper.close();
				  }
			  
			  
			  public long insertContact(BTContact contact){
				  this.open() ; 
				  long insertId = 0 ; 
				  try{
					  ContentValues values = new ContentValues();

					  values.put(MySQLiteHelper.CONTACT_COLUMN_EMAIL, contact.getContactEmail()) ;
					  values.put(MySQLiteHelper.CONTACT_COLUMN_ID, contact.getContactId()) ;
					  values.put(MySQLiteHelper.CONTACT_COLUMN_FORM_ID, contact.getContactFormId()) ;
					 
					  values.put(MySQLiteHelper.CONTACT_COLUMN_FR_ID, contact.getContactFrID()) ;
					  values.put(MySQLiteHelper.CONTACT_COLUMN_FR_CONTENT, contact.getContactFrResponse().toString()) ;
					  values.put(MySQLiteHelper.CONTACT_COLUMN_AGENT_ID, contact.getContactFrAgentID()) ;
					  values.put(MySQLiteHelper.CONTACT_COLUMN_FR_HASHCODE, contact.getContactFrHashcode()) ;
					  values.put(MySQLiteHelper.CONTACT_COLUMN_IS_SYNCHRONIZED, contact.isSynchronized()) ;
					  values.put(MySQLiteHelper.CONTACT_COLUMN_DO_FOLLOW_UP, contact.isDoFollowup()) ;
					  values.put(MySQLiteHelper.CONTACT_COLUMN_NAME, contact.getContactName()) ; 
					  values.put(MySQLiteHelper.CONTACT_COLUMN_CC_EMAIL, contact.getCcEmailRegister().toString()) ; 
					  values.put(MySQLiteHelper.CONTACT_COLUMN_CCI_EMAIL, contact.getCciEmailRegister().toString()) ; 
					  values.put(MySQLiteHelper.CONTACT_COLUMN_DATE, contact.getCreateTime()) ;
					  insertId = database.insert(MySQLiteHelper.TABLE_CONTACT, null,
						        values);
						    

					  
				  }finally{
					  this.close() ; 
				  }
				  
					    	return insertId ; 
					    
					
			  }
			  
			  
			  public boolean updateContact(BTContact contact){
				  this.open() ; 
				  long insertId  = 0; 
				  
				  
				  try{
					  ContentValues values = new ContentValues();

					  values.put(MySQLiteHelper.CONTACT_COLUMN_EMAIL, contact.getContactEmail()) ;
					  values.put(MySQLiteHelper.CONTACT_COLUMN_ID, contact.getContactId()) ;
					  values.put(MySQLiteHelper.CONTACT_COLUMN_FORM_ID, contact.getContactFormId()) ;
					 
					  values.put(MySQLiteHelper.CONTACT_COLUMN_FR_ID, contact.getContactFrID()) ;
					  values.put(MySQLiteHelper.CONTACT_COLUMN_FR_CONTENT, contact.getContactFrResponse().toString()) ;
					  values.put(MySQLiteHelper.CONTACT_COLUMN_AGENT_ID, contact.getContactFrAgentID()) ;
					  values.put(MySQLiteHelper.CONTACT_COLUMN_FR_HASHCODE, contact.getContactFrHashcode()) ;
					  values.put(MySQLiteHelper.CONTACT_COLUMN_IS_SYNCHRONIZED, contact.isSynchronized()) ;
					  values.put(MySQLiteHelper.CONTACT_COLUMN_DO_FOLLOW_UP, contact.isDoFollowup()) ;
					  values.put(MySQLiteHelper.CONTACT_COLUMN_NAME, contact.getContactName()) ; 
					  values.put(MySQLiteHelper.CONTACT_COLUMN_CC_EMAIL, contact.getCcEmailRegister().toString()) ; 
					  values.put(MySQLiteHelper.CONTACT_COLUMN_CCI_EMAIL, contact.getCciEmailRegister().toString()) ;
					  values.put(MySQLiteHelper.CONTACT_COLUMN_DATE, contact.getCreateTime()) ;


					  insertId = database.update(MySQLiteHelper.TABLE_CONTACT, values, MySQLiteHelper.CONTACT_COLUMN_LOCAL_ID + "= ?", new String[]{String.valueOf(contact.getContact_local_id())}) ;
					  
				  }finally{
					  this.close() ; 
				  }
					    if(insertId!= 0){
					    	return true ; 
					    }else{
					    	return false ; 
					    }
					
			  }

			  
			  
			  
			  
			  public List<BTContact> getAllContacts( int agent_id  ,  int isSync) {
				  this.open() ; 
				  List<BTContact> contacts= new LinkedList<BTContact>();
				  Cursor cursor  = null  ; 		
				  if(isSync != -1 ){
				  			cursor = database.query(MySQLiteHelper.TABLE_CONTACT,
						    		allColumns , MySQLiteHelper.CONTACT_COLUMN_AGENT_ID+"= ? AND "+MySQLiteHelper.CONTACT_COLUMN_IS_SYNCHRONIZED +"= ?",
						    		new String[]{String.valueOf(agent_id) , String.valueOf(isSync)}, null, null, null);
						     		
				  		}else{
				  			cursor = database.query(MySQLiteHelper.TABLE_CONTACT,
						    		allColumns , MySQLiteHelper.CONTACT_COLUMN_AGENT_ID+"= ? " , 
						    		new String[]{String.valueOf(agent_id) }, null, null, null);
						    
				  		}
				     
				  	try{
					    if(cursor!= null){ 
						    cursor.moveToFirst();
						    while (!cursor.isAfterLast()) {
						      BTContact contact= cursorToContact(cursor);

						      if(contact != null)
						      contacts.add(contact);
						      cursor.moveToNext();
						    }
						    // make sure to close the cursor
						  }
				  	}finally{
				  		cursor.close() ; 
				  		this.close() ; 
				  	}
				  
				    
				    return contacts;
					  
			  
			  }


		public List<BTContact> getAllContactsByMail( int agent_id  ,  int isSync) {
				  this.open() ;
				  List<BTContact> contacts= new LinkedList<BTContact>();
				  Cursor cursor  = null  ;
				  if(isSync != -1 ){
				cursor = database.rawQuery("SELECT  DISTINCT " +
						MySQLiteHelper.CONTACT_COLUMN_EMAIL+","+
						MySQLiteHelper.CONTACT_COLUMN_LOCAL_ID+","+
						MySQLiteHelper.CONTACT_COLUMN_ID+","+
						MySQLiteHelper.CONTACT_COLUMN_FORM_ID+","+
						MySQLiteHelper.CONTACT_COLUMN_FR_ID+","+
						MySQLiteHelper.CONTACT_COLUMN_FR_HASHCODE+","+
						MySQLiteHelper.CONTACT_COLUMN_AGENT_ID+","+
						MySQLiteHelper.CONTACT_COLUMN_FR_CONTENT+","+
						MySQLiteHelper.CONTACT_COLUMN_IS_SYNCHRONIZED+","+
						MySQLiteHelper.CONTACT_COLUMN_DO_FOLLOW_UP +","+
						MySQLiteHelper.CONTACT_COLUMN_NAME +","+
						MySQLiteHelper.CONTACT_COLUMN_CC_EMAIL+","+
						MySQLiteHelper.CONTACT_COLUMN_CCI_EMAIL+","+
						MySQLiteHelper.CONTACT_COLUMN_DATE+" FROM "+ MySQLiteHelper.TABLE_CONTACT +" WHERE "+MySQLiteHelper.CONTACT_COLUMN_AGENT_ID+" = '"+agent_id+"' AND "+MySQLiteHelper.CONTACT_COLUMN_IS_SYNCHRONIZED +"= '"+isSync+"'" , null);

				  		}else{
					  cursor = database.rawQuery("SELECT  DISTINCT " +
							  MySQLiteHelper.CONTACT_COLUMN_EMAIL + "," +
							  MySQLiteHelper.CONTACT_COLUMN_LOCAL_ID + "," +
							  MySQLiteHelper.CONTACT_COLUMN_ID + "," +
							  MySQLiteHelper.CONTACT_COLUMN_FORM_ID + "," +
							  MySQLiteHelper.CONTACT_COLUMN_FR_ID + "," +
							  MySQLiteHelper.CONTACT_COLUMN_FR_HASHCODE + "," +
							  MySQLiteHelper.CONTACT_COLUMN_AGENT_ID + "," +
							  MySQLiteHelper.CONTACT_COLUMN_FR_CONTENT + "," +
							  MySQLiteHelper.CONTACT_COLUMN_IS_SYNCHRONIZED + "," +
							  MySQLiteHelper.CONTACT_COLUMN_DO_FOLLOW_UP + "," +
							  MySQLiteHelper.CONTACT_COLUMN_NAME + "," +
							  MySQLiteHelper.CONTACT_COLUMN_CC_EMAIL + "," +
							  MySQLiteHelper.CONTACT_COLUMN_CCI_EMAIL + "," +
							  MySQLiteHelper.CONTACT_COLUMN_DATE + " FROM " + MySQLiteHelper.TABLE_CONTACT + " WHERE " + MySQLiteHelper.CONTACT_COLUMN_AGENT_ID + "= '" + agent_id + "'", null);

				  		}

				  	try{
					    if(cursor!= null){
						    cursor.moveToFirst();
						    while (!cursor.isAfterLast()) {
						      BTContact contact= cursorToContactByMail(cursor);

						      if(contact != null)
						      contacts.add(contact);
						      cursor.moveToNext();
						    }
						    // make sure to close the cursor
						  }
				  	}finally{
				  		cursor.close() ;
				  		this.close() ;
				  	}


				    return contacts;


			  }
			  public List<BTContact> getAllContactsByDate( int agent_id  ,  int isSync, String date) {
				  this.open() ;
				  Calendar calendar = Calendar.getInstance();
				Log.d(" calender ", " debut" + calendar.getTime() + " date " + date);
				    List<BTContact> contacts= new LinkedList<BTContact>();
				  Cursor cursor  = null  ;
				  if(isSync != -1 ){
					  Log.d(" calender "," if isSync != -1");
					  cursor = database.rawQuery("SELECT  DISTINCT " +
							  MySQLiteHelper.CONTACT_COLUMN_EMAIL + "," +
							  MySQLiteHelper.CONTACT_COLUMN_LOCAL_ID + "," +
							  MySQLiteHelper.CONTACT_COLUMN_ID + "," +
							  MySQLiteHelper.CONTACT_COLUMN_FORM_ID + "," +
							  MySQLiteHelper.CONTACT_COLUMN_FR_ID + "," +
							  MySQLiteHelper.CONTACT_COLUMN_FR_HASHCODE + "," +
							  MySQLiteHelper.CONTACT_COLUMN_AGENT_ID + "," +
							  MySQLiteHelper.CONTACT_COLUMN_FR_CONTENT + "," +
							  MySQLiteHelper.CONTACT_COLUMN_IS_SYNCHRONIZED + "," +
							  MySQLiteHelper.CONTACT_COLUMN_DO_FOLLOW_UP + "," +
							  MySQLiteHelper.CONTACT_COLUMN_NAME + "," +
							  MySQLiteHelper.CONTACT_COLUMN_CC_EMAIL + "," +
							  MySQLiteHelper.CONTACT_COLUMN_CCI_EMAIL + "," +
							  MySQLiteHelper.CONTACT_COLUMN_DATE + " FROM " + MySQLiteHelper.TABLE_CONTACT + " WHERE " + MySQLiteHelper.CONTACT_COLUMN_AGENT_ID + " = '" + agent_id + "' AND " + MySQLiteHelper.CONTACT_COLUMN_IS_SYNCHRONIZED + "= '" + isSync + "'"+"AND datetime(" + MySQLiteHelper.CONTACT_COLUMN_DATE + ") >= datetime(?)",new String[]{ String.valueOf(date)},null );

				  }else {
					  Log.d(" calender "," if isSync = -1");
					  cursor = database.rawQuery("SELECT  DISTINCT " +
							  MySQLiteHelper.CONTACT_COLUMN_EMAIL + "," +
							  MySQLiteHelper.CONTACT_COLUMN_LOCAL_ID + "," +
							  MySQLiteHelper.CONTACT_COLUMN_ID + "," +
							  MySQLiteHelper.CONTACT_COLUMN_FORM_ID + "," +
							  MySQLiteHelper.CONTACT_COLUMN_FR_ID + "," +
							  MySQLiteHelper.CONTACT_COLUMN_FR_HASHCODE + "," +
							  MySQLiteHelper.CONTACT_COLUMN_AGENT_ID + "," +
							  MySQLiteHelper.CONTACT_COLUMN_FR_CONTENT + "," +
							  MySQLiteHelper.CONTACT_COLUMN_IS_SYNCHRONIZED + "," +
							  MySQLiteHelper.CONTACT_COLUMN_DO_FOLLOW_UP + "," +
							  MySQLiteHelper.CONTACT_COLUMN_NAME + "," +
							  MySQLiteHelper.CONTACT_COLUMN_CC_EMAIL + "," +
							  MySQLiteHelper.CONTACT_COLUMN_CCI_EMAIL + "," +
							  MySQLiteHelper.CONTACT_COLUMN_DATE + " FROM " + MySQLiteHelper.TABLE_CONTACT + " WHERE " + MySQLiteHelper.CONTACT_COLUMN_AGENT_ID + "= '" + agent_id + "'" + " AND datetime(" + MySQLiteHelper.CONTACT_COLUMN_DATE + ") >= datetime(?)", new String[]{date}, null);
				  }
Log.d(" contact ", " cursor number " +  cursor.getCount());

				  	try{
					    if(cursor!= null){
						    cursor.moveToFirst();
						    while (!cursor.isAfterLast()) {
						      BTContact contact= cursorToContactByMail(cursor);
						      if(contact != null) {
								  Log.d("contact select " , "contact select by date "+contact.getContactEmail()+ " "+cursor.getString(13)) ;

								  contacts.add(contact);
							  }
						      cursor.moveToNext();
						    }
						    // make sure to close the cursor
						  }
				  	}finally{
				  		cursor.close() ;
				  		this.close() ;
				  	}

				  Log.d(" calender ", " fin " + calendar.getTime());
				    return contacts;


			  }

			  public List<BTContact> loadContactWithLimit( int agent_id  ,  int isSync  , int limit ,  int lastIdSelected) {
				  this.open() ; 
				  List<BTContact> contacts= new LinkedList<BTContact>();
				  int counter =  0 ; 
				  Cursor cursor  = null  ; 		
				  if(isSync != -1 ){
				  			cursor = database.query(MySQLiteHelper.TABLE_CONTACT,
						    		allColumns , MySQLiteHelper.CONTACT_COLUMN_AGENT_ID+"= ? AND "+MySQLiteHelper.CONTACT_COLUMN_IS_SYNCHRONIZED +"= ? AND "+MySQLiteHelper.CONTACT_COLUMN_LOCAL_ID+" > ?",
						    		new String[]{String.valueOf(agent_id) , String.valueOf(isSync) , String.valueOf(lastIdSelected)}, null, null, null );
						     		
				  		}else{
				  			cursor = database.query(MySQLiteHelper.TABLE_CONTACT,
						    		allColumns , MySQLiteHelper.CONTACT_COLUMN_AGENT_ID+"= ? AND  "+MySQLiteHelper.CONTACT_COLUMN_LOCAL_ID +" > ?"  , 
						    		new String[]{String.valueOf(agent_id) , String.valueOf(lastIdSelected) }, null, null, null  );
						    
				  		}
				    
				  try{
					    if(cursor!= null){ 
						    cursor.moveToFirst();
						    while (!cursor.isAfterLast()) {
						      BTContact contact= cursorToContact(cursor);
							  if(counter < limit){

							      if(contact != null){
							    	  contacts.add(contact);
								      cursor.moveToNext();
								    	  counter++  ; 
							      }  
							  }else{
								  
								  
						    	  break ; 
						      }
						    }
						    // make sure to close the cursor
						  }
						   
				  }finally{
					  cursor.close() ; 
					  this.close() ; 
				  }
			  
				    
				    return contacts;
					  
				    
			  }
			  
			  
			  
			  public List<BTContact> getF20irstContacts( int agent_id  ,  int isSync) {
				  this.open() ; 
				  List<BTContact> contacts= new LinkedList<BTContact>();
				  Cursor cursor  = null  ; 		
				  if(isSync != -1 ){
				  			cursor = database.query(MySQLiteHelper.TABLE_CONTACT,
						    		allColumns , MySQLiteHelper.CONTACT_COLUMN_AGENT_ID+"= ? AND "+MySQLiteHelper.CONTACT_COLUMN_IS_SYNCHRONIZED +"= ?",
						    		new String[]{String.valueOf(agent_id) , String.valueOf(isSync)}, null, null, null , "20");
						     		
				  		}else{
				  			cursor = database.query(MySQLiteHelper.TABLE_CONTACT,
						    		allColumns , MySQLiteHelper.CONTACT_COLUMN_AGENT_ID+"= ? " , 
						    		new String[]{String.valueOf(agent_id) }, null, null, null , "20");
						    
				  		}
				    
				  try{
					    if(cursor!= null){ 
						    cursor.moveToFirst();
						    while (!cursor.isAfterLast()) {
						      BTContact contact= cursorToContact(cursor);

						      if(contact != null)
						      contacts.add(contact);
						      cursor.moveToNext();
						    }
						    // make sure to close the cursor
						  }
					  
				  }finally{
					  cursor.close() ; 
					  this.close() ; 
				  }
				    
				    
				    
				    
				    return contacts;
					  
			  }
			  
			  
			  public List<BTContact> getAllDistincContacts( int agent_id) {
				  this.open() ; 
				  List<BTContact> contacts= new LinkedList<BTContact>();
				  Cursor cursor  = null  ; 		
				  			cursor = database.query(true ,MySQLiteHelper.TABLE_CONTACT,
						    		allColumns , MySQLiteHelper.CONTACT_COLUMN_AGENT_ID+"= ? " , 
						    		new String[]{String.valueOf(agent_id) }, MySQLiteHelper.CONTACT_COLUMN_EMAIL, null, null, null);

				  			
				  			
				  			
				  			try{
							    if(cursor!= null){ 
								    cursor.moveToFirst();
								    while (!cursor.isAfterLast()) {
								      BTContact contact= cursorToContact(cursor);

								      if(contact != null)
								      contacts.add(contact);
								      cursor.moveToNext();
								    }
								    // make sure to close the cursor
								  }
								  
				  			}finally{
				  				cursor.close() ; 
				  				this.close() ; 
				  			}
			  
				    
				    
				    return contacts;
					  
			  }
			  
			  public List<BTContact> getResponseFormContact( int agent_id  ,  int isSync , int contact_id ) {
				  this.open() ; 
				  List<BTContact> contacts= new LinkedList<BTContact>();
				  Cursor cursor  = null  ; 		
				  if(isSync != -1 ){

					  if(isSync == 0){
						  cursor = database.query(MySQLiteHelper.TABLE_CONTACT,
								  allColumns , MySQLiteHelper.CONTACT_COLUMN_AGENT_ID+"= ? AND "+MySQLiteHelper.CONTACT_COLUMN_IS_SYNCHRONIZED +"= ? AND "+MySQLiteHelper.CONTACT_COLUMN_LOCAL_ID+" = ?",
								  new String[]{String.valueOf(agent_id) , String.valueOf(isSync) , String.valueOf(contact_id)}, null, null, null);

					  }else{
						  cursor = database.query(MySQLiteHelper.TABLE_CONTACT,
								  allColumns , MySQLiteHelper.CONTACT_COLUMN_AGENT_ID+"= ? AND "+MySQLiteHelper.CONTACT_COLUMN_IS_SYNCHRONIZED +"= ? AND "+MySQLiteHelper.CONTACT_COLUMN_ID+" = ?",
								  new String[]{String.valueOf(agent_id) , String.valueOf(isSync) , String.valueOf(contact_id)}, null, null, null);

					  }

				  		}else{
				  			cursor = database.query(MySQLiteHelper.TABLE_CONTACT,
									allColumns, MySQLiteHelper.CONTACT_COLUMN_AGENT_ID + "= ? AND  " + MySQLiteHelper.CONTACT_COLUMN_ID + "= ? ",
									new String[]{String.valueOf(agent_id), String.valueOf(contact_id)}, null, null, null);
				  		}
				     
				  try{
					    if(cursor!= null){ 
						    cursor.moveToFirst();
						    while (!cursor.isAfterLast()) {
						      BTContact contact= cursorToContact(cursor);

						      if(contact != null)
						      contacts.add(contact);
						      cursor.moveToNext();
						    }
						    // make sure to close the cursor
						  }
				  }finally{
					  cursor.close() ; 
					  this.close() ; 
				  }
				   
			  
				    
				    
				    return contacts;
					  
				    
			  }
			    public List<BTContact> getResponseFormContact( int agent_id  ,  int isSync , String email , int contact_id ) {
				  this.open() ;
				  List<BTContact> contacts= new LinkedList<BTContact>();
				  Cursor cursor  = null  ;
				  if(isSync != -1 ){

					  if(isSync == 0){
						  cursor = database.query(MySQLiteHelper.TABLE_CONTACT,
								  allColumns , MySQLiteHelper.CONTACT_COLUMN_AGENT_ID+"= ? AND "+MySQLiteHelper.CONTACT_COLUMN_IS_SYNCHRONIZED +"= ? AND "+MySQLiteHelper.CONTACT_COLUMN_EMAIL+" = ?",
								  new String[]{String.valueOf(agent_id) , String.valueOf(isSync) , email}, null, null, null);

					  }else{
						  cursor = database.query(MySQLiteHelper.TABLE_CONTACT,
								  allColumns , MySQLiteHelper.CONTACT_COLUMN_AGENT_ID+"= ? AND "+MySQLiteHelper.CONTACT_COLUMN_IS_SYNCHRONIZED +"= ? AND "+MySQLiteHelper.CONTACT_COLUMN_ID+" = ?",
								  new String[]{String.valueOf(agent_id) , String.valueOf(isSync) , String.valueOf(contact_id)}, null, null, null);

					  }

				  		}else{
				  			cursor = database.query(MySQLiteHelper.TABLE_CONTACT,
						    		allColumns , MySQLiteHelper.CONTACT_COLUMN_AGENT_ID+"= ? AND  "+MySQLiteHelper.CONTACT_COLUMN_ID+"= ? " ,
						    		new String[]{String.valueOf(agent_id) , String.valueOf(contact_id) }, null, null, null);
				  		}

				  try{
					    if(cursor!= null){
						    cursor.moveToFirst();
						    while (!cursor.isAfterLast()) {
						      BTContact contact= cursorToContact(cursor);

						      if(contact != null)
						      contacts.add(contact);
						      cursor.moveToNext();
						    }
						    // make sure to close the cursor
						  }
				  }finally{
					  cursor.close() ;
					  this.close() ;
				  }




				    return contacts;


			  }

			  
			  public BTContact getContactWithID( int agent_id  ,  int contact_id) {
				  this.open() ; 
				  Cursor cursor = database.query(MySQLiteHelper.TABLE_CONTACT,
						    		allColumns , MySQLiteHelper.CONTACT_COLUMN_AGENT_ID+"= ? AND "+MySQLiteHelper.CONTACT_COLUMN_ID +"= ?",
						    		new String[]{String.valueOf(agent_id) , String.valueOf(contact_id)}, null, null, null);
						     		
				  BTContact contact = null ;
				    try{
				    	if(cursor!= null){ 
						    cursor.moveToFirst();
						    while (!cursor.isAfterLast()) {
						      contact= cursorToContact(cursor);
							  	cursor.moveToNext();
						    }
						    // make sure to close the cursor
						    cursor.close();
						  }
						   
				    }finally{
				    	cursor.close() ; 
				    	this.close() ; 
				    }
			  
				    
				    
				    return contact;
					  
			  }
			  
			  public BTContact getContactWithLocalID( int agent_id  ,  int contact_id) {
				  this.open() ; 
				  Cursor cursor = database.query(MySQLiteHelper.TABLE_CONTACT,
						    		allColumns , MySQLiteHelper.CONTACT_COLUMN_AGENT_ID+"= ? AND "+MySQLiteHelper.CONTACT_COLUMN_LOCAL_ID +"= ?",
						    		new String[]{String.valueOf(agent_id) , String.valueOf(contact_id)}, null, null, null);
						     		
				  BTContact contact = null ;
				    try{
				    	if(cursor!= null){ 
						    cursor.moveToFirst();
						    while (!cursor.isAfterLast()) {

						      contact= cursorToContact(cursor);
						      //Log.d("found a contact", "found a contact "+contact.getContactEmail()) ;
						      cursor.moveToNext();
						    }
						    // make sure to close the cursor
						  }
						  
				    }finally{
				    	cursor.close() ; 
				    	this.close() ; 
				    }
			  
				    
				    return contact;
					  
			  }
			  
			  
			  public BTContact getContactWithFrId( int agent_id  ,  int fr_id) {
				  this.open() ; 
				  Cursor cursor = database.query(MySQLiteHelper.TABLE_CONTACT,
						    		allColumns , MySQLiteHelper.CONTACT_COLUMN_AGENT_ID+"= ? AND "+MySQLiteHelper.CONTACT_COLUMN_FR_ID +"= ?",
						    		new String[]{String.valueOf(agent_id) , String.valueOf(fr_id)}, null, null, null);
						     		
				  BTContact contact = null ;
				    try{
				    	if(cursor!= null){ 
						    cursor.moveToFirst();
						    while (!cursor.isAfterLast()) {
						      contact= cursorToContact(cursor);
							  	cursor.moveToNext();
						    }
						    // make sure to close the cursor
						  }
						  
				    }finally{
				    	cursor.close() ;
				    	this.close() ; 
				    }
				    
				    return contact;
					  
			  
			  }
			  
			  
			  public BTContact cursorToContact(Cursor cursor){
				 
			 
				
				  try {
					 
					  BTContact contact = new BTContact() ;
					contact.setContactFrResponse(new JSONObject(cursor.getString(7))) ;
					  contact.setContact_local_id(cursor.getInt(0)) ;
					  contact.setContactEmail(cursor.getString(1)) ;
					//  Log.d(" contact cursor to contact ", contact.getContactEmail());

					  contact.setContactFrHashcode(cursor.getString(5)) ; 
					  contact.setContactId(cursor.getInt(2)) ; 
					  contact.setContactFormId(cursor.getInt(3)) ; 
					
					  contact.setContactFrID(cursor.getInt(4)) ; 
					  contact.setSynchronized((cursor.getInt(8) )) ; 
					  contact.setDoFollowup((cursor.getInt(9) == 1 ? true : false )) ; 
					  contact.setContactFrAgentID(cursor.getInt(6)) ; 
					  try {
						contact.setCcEmailRegister(new JSONArray(cursor.getString(11))) ; 
					} catch (JSONException e) {
						// TODO: handle exception
						contact.setCcEmailRegister(new JSONArray()) ; 
					}
					  try {
							contact.setCciEmailRegister(new JSONArray(cursor.getString(12))) ; 
						} catch (JSONException e) {
							// TODO: handle exception
							contact.setCciEmailRegister(new JSONArray()) ; 
						}
				/*	  if (cursor.getString(13)==null || cursor.getString(13).equals("")){
						  Calendar cal = Calendar.getInstance();
						  Date today = new Date();
						  cal.setTime(today);


						  SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");


						  String 	referenceDate = formatter.format(cal.getTime());
						  contact.setCreateTime(referenceDate);

					  }else {
				*/		  contact.setCreateTime(cursor.getString(13));
				//	  }

					  contact.setContactName(cursor.getString(10)) ;
					  return contact ; 
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					Log.d(" contact cursor to contact ", e.toString());
					e.printStackTrace();
					return null  ; 
				} 
		
				
				  
				  
				
				  
				 
			  }
			   public BTContact cursorToContactByMail(Cursor cursor){



				  try {

					  BTContact contact = new BTContact() ;
					contact.setContactFrResponse(new JSONObject(cursor.getString(7))) ;
					  contact.setContact_local_id(cursor.getInt(1)) ;
					  contact.setContactEmail(cursor.getString(0)) ;
					  contact.setContactFrHashcode(cursor.getString(5)) ;
					  contact.setContactId(cursor.getInt(2)) ;
					  contact.setContactFormId(cursor.getInt(3)) ;

					  contact.setContactFrID(cursor.getInt(4)) ;
					  contact.setSynchronized((cursor.getInt(8) )) ;
					  contact.setDoFollowup((cursor.getInt(9) == 1 ? true : false )) ;
					  contact.setContactFrAgentID(cursor.getInt(6)) ;
					  try {
						contact.setCcEmailRegister(new JSONArray(cursor.getString(11))) ;
					} catch (JSONException e) {
						// TODO: handle exception
						contact.setCcEmailRegister(new JSONArray()) ;
					}
					  try {
							contact.setCciEmailRegister(new JSONArray(cursor.getString(12))) ;
						} catch (JSONException e) {
							// TODO: handle exception
							contact.setCciEmailRegister(new JSONArray()) ;
						}
					  contact.setCreateTime(cursor.getString(13));
					  contact.setContactName(cursor.getString(10)) ;
					  return contact ;
				} catch (JSONException e) {
					// TODO Auto-generated catch block

					e.printStackTrace();
					return null  ;
				}







			  }

}
