package com.biptag.biptag.dbmanager.datasource;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.biptag.biptag.dbmanager.MySQLiteHelper;
import com.biptag.biptag.dbmanager.objects.BTFollowupTemplate;
import com.biptag.biptag.dbmanager.objects.BTLog;

public class BTLogDataSource {
	
	 private SQLiteDatabase database;
	  private MySQLiteHelper dbHelper;
	  private String[] allColumns = { MySQLiteHelper.LOG_COLUMN_LOG_ID,
			  MySQLiteHelper.LOG_COLUMN_LOG_TYPE,
			  MySQLiteHelper.LOG_COLUMN_LOG_TIMESTAMP,
			  MySQLiteHelper.LOG_COLUMN_LOG_REL_OBJECT,
			  MySQLiteHelper.LOG_COLUMN_LOG_REL_OBJECT_ID,
			  MySQLiteHelper.LOG_COLUMN_LOG_REL_OBJECT_2,
			  MySQLiteHelper.LOG_COLUMN_LOG_REL_OBJECT_2_ID,
			  MySQLiteHelper.LOG_COLUMN_FL_SUBJECT,
			  MySQLiteHelper.LOG_COLUMN_FL_CONTENT, 
			  MySQLiteHelper.LOG_COLUMN_FL_ID , 
			  MySQLiteHelper.LOG_COLUMN_FL_CONTACT_ID , 
			  MySQLiteHelper.LOG_COLUMN_AGENT_ID, 
			  MySQLiteHelper.LOG_COLUMN_SEND_DATE 
			  };
	  
		public BTLogDataSource(Context  context) {
		// TODO Auto-generated constructor stub
		    dbHelper = new MySQLiteHelper(context);
				  }
			  private void open() throws SQLException {
				    database = dbHelper.getWritableDatabase();
				  }
				
			  private void close() {
				    dbHelper.close();
				  }
			  
			  
			  public Boolean insertLog(BTLog log){
				  long insertId = 0 ; 
				  this.open() ; 
				  
				  try{
					  ContentValues values = new ContentValues();

					  values.put(MySQLiteHelper.LOG_COLUMN_LOG_ID, log.getLog_id()) ;
					  values.put(MySQLiteHelper.LOG_COLUMN_LOG_TYPE, log.getLog_type()) ;
					  values.put(MySQLiteHelper.LOG_COLUMN_LOG_TIMESTAMP,log.getLog_timestamps()) ;
					  values.put(MySQLiteHelper.LOG_COLUMN_LOG_REL_OBJECT, log.getLog_rel_object()) ;
					  values.put(MySQLiteHelper.LOG_COLUMN_LOG_REL_OBJECT_ID, log.getLog_rel_object_id()) ;
					  values.put(MySQLiteHelper.LOG_COLUMN_LOG_REL_OBJECT_2, log.getLog_rel_object_2()) ;
					  values.put(MySQLiteHelper.LOG_COLUMN_LOG_REL_OBJECT_2_ID, log.getLog_rel_object_id()) ;
					  values.put(MySQLiteHelper.LOG_COLUMN_FL_SUBJECT, log.getFl_subject()) ;
					  values.put(MySQLiteHelper.LOG_COLUMN_FL_CONTENT, log.getFl_content()) ;
					  values.put(MySQLiteHelper.LOG_COLUMN_FL_ID , log.getFl_id());
					  values.put(MySQLiteHelper.LOG_COLUMN_FL_CONTACT_ID , log.getFl_contact_id());
					  values.put(MySQLiteHelper.LOG_COLUMN_AGENT_ID ,log.getFl_agent_id() );
					  values.put(MySQLiteHelper.LOG_COLUMN_SEND_DATE ,log.getFl_send_date());
					  insertId = database.insert(MySQLiteHelper.LOG_TALBLE, null,
						        values);
						
				  }finally{
					  this.close() ; 
				  }
				  
				  	
					    if(insertId!= 0){
					    	return true; 
					    }else{
					    	return false ; 
					    }
					
			  }
			  
			  
			  
			  public boolean updateLog(BTLog log){
				  long insertId  = 0; 
				  this.open() ; 
				  try{

					  
					  ContentValues values = new ContentValues();

					  values.put(MySQLiteHelper.LOG_COLUMN_LOG_ID, log.getLog_id()) ;
					  values.put(MySQLiteHelper.LOG_COLUMN_LOG_TYPE, log.getLog_type()) ;
					  values.put(MySQLiteHelper.LOG_COLUMN_LOG_TIMESTAMP,log.getLog_timestamps()) ;
					  values.put(MySQLiteHelper.LOG_COLUMN_LOG_REL_OBJECT, log.getLog_rel_object()) ;
					  values.put(MySQLiteHelper.LOG_COLUMN_LOG_REL_OBJECT_ID, log.getLog_rel_object_id()) ;
					  values.put(MySQLiteHelper.LOG_COLUMN_LOG_REL_OBJECT_2, log.getLog_rel_object_2()) ;
					  values.put(MySQLiteHelper.LOG_COLUMN_LOG_REL_OBJECT_2_ID, log.getLog_rel_object_id()) ;
					  values.put(MySQLiteHelper.LOG_COLUMN_FL_SUBJECT, log.getFl_subject()) ;
					  values.put(MySQLiteHelper.LOG_COLUMN_FL_CONTENT, log.getFl_content()) ;
					  values.put(MySQLiteHelper.LOG_COLUMN_FL_ID , log.getFl_id());
					  values.put(MySQLiteHelper.LOG_COLUMN_FL_CONTACT_ID , log.getFl_contact_id());
					  values.put(MySQLiteHelper.LOG_COLUMN_AGENT_ID ,log.getFl_agent_id() );
					  values.put(MySQLiteHelper.LOG_COLUMN_SEND_DATE ,log.getFl_send_date());
					  insertId = database.update(MySQLiteHelper.LOG_TALBLE, values, MySQLiteHelper.LOG_COLUMN_LOG_ID+"= ?", new String[]{String.valueOf(log.getLog_id()) }) ; 

				  }finally{
					  this.close() ; 
				  }
				  
					    if(insertId!= 0){
					    	return true ; 
					    }else{
					    	return false ; 
					    }
					
			  }
			  
			  
			  
			  public List<BTLog> getAllLogs( int ba_id) {
				  this.open() ; 
				  List<BTLog> logs= new LinkedList<BTLog>();
				  Cursor cursor  = null  ; 		
		  			cursor = database.query(MySQLiteHelper.LOG_TALBLE,
				    		allColumns , MySQLiteHelper.LOG_COLUMN_AGENT_ID+"= ? ",
				    		new String[]{String.valueOf(ba_id)}, null, null, null);
		   
		  			try{
					    if(cursor!= null){ 
						    cursor.moveToFirst();
						    while (!cursor.isAfterLast()) {
						    	BTLog log= cursorToLog(cursor);

						      if(log!= null)
						      logs.add(log);
						      cursor.moveToNext();
						    }
						    // make sure to close the cursor
						    
						  }
		  			}finally{
		  				cursor.close() ; 
		  				this.close() ; 
		  			}
				    
				    return logs;
				   
			  
			  }

			  public List<BTLog> findLogsFormContact( int ba_id , int contact_id) {
				  this.open() ; 
				  List<BTLog> logs= new LinkedList<BTLog>(); 
				  Cursor cursor  = null  ; 		
		  			cursor = database.query(MySQLiteHelper.LOG_TALBLE,
				    		allColumns , MySQLiteHelper.LOG_COLUMN_AGENT_ID+"= ?  AND "+MySQLiteHelper.LOG_COLUMN_FL_CONTACT_ID+"= ?",
				    		new String[]{String.valueOf(ba_id) , String.valueOf(contact_id)}, null, null, null);
		   
		  			try{
					    if(cursor!= null){ 
						    cursor.moveToFirst();
						    while (!cursor.isAfterLast()) {
						    	BTLog log= cursorToLog(cursor);

						      if(log!= null)
						      logs.add(log);
						      cursor.moveToNext();
						    }
						    
						  }
		  			}finally{
		  				cursor.close() ; 
		  				this.close() ; 
		  			}
				    
				    return logs;
				   
			  
			  }
			  
			  
			  public BTLog findLogWithId( int ba_id , int log_id) {
				  BTLog log = null ; 
				  this.open() ; 
				  Cursor cursor  = null  ; 		
		  			cursor = database.query(MySQLiteHelper.LOG_TALBLE,
				    		allColumns , MySQLiteHelper.LOG_COLUMN_AGENT_ID+"= ? AND  "+MySQLiteHelper.LOG_COLUMN_LOG_ID+"= ?",
				    		new String[]{String.valueOf(ba_id) , String.valueOf(log_id)}, null, null, null);
		   
		  			try{
					    if(cursor!= null){ 
						    cursor.moveToFirst();
						    while (!cursor.isAfterLast()) {
						    	 log= cursorToLog(cursor);

						    }
						    
						  }
		  			}finally{
		  				cursor.close() ; 
		  				this.close() ; 
		  			}
				    
				    return log;
				   
			  
			  } 
			  
			  
			  public BTLog cursorToLog(Cursor cursor){
				  BTLog log = new BTLog() ; 
				  
				  try {
					  log.setLog_id(cursor.getInt(0)) ; 
					  log.setLog_type(cursor.getString(1)) ; 
					  log.setLog_timestamps(cursor.getString(2)) ; 
					  log.setLog_rel_object(cursor.getString(3)) ; 
					  log.setLog_rel_object_id(cursor.getInt(4)) ; 
					  log.setLog_rel_object_2(cursor.getString(5)) ;
					  log.setLog_rel_object_id(cursor.getInt(6));
					//  log.setLog_rel_object_2_id(cursor.getInt(6)) ;
					  log.setFl_subject(cursor.getString(7)) ; 
					  log.setFl_content(cursor.getString(8))  ; 
					  log.setFl_id(cursor.getInt(9)) ; 
					  log.setFl_contact_id(cursor.getInt(10)) ; 
					  log.setFl_agent_id(cursor.getInt(11)) ; 
					  log.setFl_send_date(cursor.getString(12)) ;  
					    
					  	 
				} catch (OutOfMemoryError  e) {
					// TODO: handle exception
				}
				  
				
				  
				  
				  return log ; 
				  
			  }
			  
			  
}
