package com.biptag.biptag.dbmanager.datasource;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.biptag.biptag.dbmanager.MySQLiteHelper;
import com.biptag.biptag.dbmanager.objects.BTContact;
import com.biptag.biptag.dbmanager.objects.BTImage;

public class BTImageDataSource {
	 private SQLiteDatabase database;
	  private MySQLiteHelper dbHelper;

	
	  private String[] allColumns = { MySQLiteHelper.IMAGE_COLUMN_IMG_ID,
			  MySQLiteHelper.IMAGE_COLUMN_IMAGE_NAME,
			  MySQLiteHelper.IMAGE_COLIMN_AGENT_ID,
			  MySQLiteHelper.IMAGE_COLUMN_RESPONSE_ID,
			  MySQLiteHelper.IMAGE_COLUMN_IS_SENT,
			  MySQLiteHelper.IMAGE_COLUMN_IMAGE_DATE,
			  MySQLiteHelper.IMAGE_COLUMN_Data_OCR,
			  MySQLiteHelper.IMAGE_COLUMN_Path_OCR,
			  MySQLiteHelper.IMAGE_COLUMN_txt_OCR
			  };
	  
	  
	  
	  		public BTImageDataSource(Context  context) {
		// TODO Auto-generated constructor stub
		    dbHelper = new MySQLiteHelper(context);
				  }
			  private void open() throws SQLException {

				    database = dbHelper.getWritableDatabase();
				  }
				
			  private void close() {
				    dbHelper.close();
				  }
			  
			  public long saveImage(BTImage image){
				  ContentValues values = new ContentValues();
				  long insertId = 0 ; 
				  this.open() ; 
				  try{

					  values.put(MySQLiteHelper.IMAGE_COLUMN_IMAGE_NAME, image.getImageName()) ; 
					  values.put(MySQLiteHelper.IMAGE_COLIMN_AGENT_ID, image.getAgentId()) ; 
					  values.put(MySQLiteHelper.IMAGE_COLUMN_IS_SENT, image.getIsSent()) ; 
					  values.put(MySQLiteHelper.IMAGE_COLUMN_RESPONSE_ID,image.getResponseId()) ; 
					  values.put(MySQLiteHelper.IMAGE_COLUMN_IMAGE_DATE, image.getImageDate()) ; 
					  values.put(MySQLiteHelper.IMAGE_COLUMN_Data_OCR, image.getImageDataOCR()) ;
					  values.put(MySQLiteHelper.IMAGE_COLUMN_Path_OCR, image.getImagePathOCR()) ;
					  values.put(MySQLiteHelper.IMAGE_COLUMN_txt_OCR, image.getImagetxtOCR()) ;
					  insertId = database.insert(MySQLiteHelper.IMAGE_TALBLE, null,
						        values);
					  Log.d("insert image ", " image data ocr " + image.getImageDataOCR() + " " + image.getAgentId());
				  }finally{
					  this.close() ; 
				  }
				  
				  return insertId ; 
			  }
			  
			  public boolean updateImage(BTImage image){
				  this.open() ; 
				  long insertId = 0 ; 
				  
				try{
					  ContentValues values = new ContentValues();
					  values.put(MySQLiteHelper.IMAGE_COLUMN_IMAGE_NAME, image.getImageName()) ; 
					  values.put(MySQLiteHelper.IMAGE_COLIMN_AGENT_ID, image.getAgentId()) ; 
					  values.put(MySQLiteHelper.IMAGE_COLUMN_IS_SENT, image.getIsSent()) ; 
					  values.put(MySQLiteHelper.IMAGE_COLUMN_RESPONSE_ID,image.getResponseId()) ; 
					  values.put(MySQLiteHelper.IMAGE_COLUMN_IMAGE_DATE, image.getImageDate()) ; 
					  values.put(MySQLiteHelper.IMAGE_COLUMN_Data_OCR, image.getImageDataOCR()) ;
					  values.put(MySQLiteHelper.IMAGE_COLUMN_Path_OCR, image.getImagePathOCR()) ;
					  values.put(MySQLiteHelper.IMAGE_COLUMN_txt_OCR, image.getImagetxtOCR()) ;
					  insertId = database.update(MySQLiteHelper.IMAGE_TALBLE, values, MySQLiteHelper.IMAGE_COLUMN_IMG_ID+"= ?", new String[]{String.valueOf(image.getImageId()) }) ;

				}finally{
					this.close() ; 
				}
				  if(insertId!=0){
					  return true ; 
				  }else{
					  return false ; 
				  }
				  
			  }
			  
			  public List<BTImage> selectAllImages(int agent_id){
				  
				  this.open() ; 
				  List<BTImage> images= new ArrayList<BTImage>();
				  Cursor cursor  = null  ; 		
				  
				  cursor = database.query(MySQLiteHelper.IMAGE_TALBLE,
						    		allColumns , MySQLiteHelper.IMAGE_COLIMN_AGENT_ID+"= ?",
						    		new String[]{String.valueOf(agent_id)}, null, null, null);
		
				  
				  try{
						 if(cursor.moveToFirst()){
							 while (!cursor.isAfterLast()) {
								  BTImage image = cursorToImage(cursor) ; 
								  images.add(image) ;
								  cursor.moveToNext() ; 
							  }
						 }
				
				  }finally{
					  cursor.close() ; 
					  this.close() ; 
				  }
				  
				  return images ; 			
			  
			  }
			  
			  
			  public BTImage selectImagesWithId(int imageId ,  int agent_id){
				  this.open() ; 
				  Cursor cursor  = null  ; 		
				  BTImage image  = null  ; 
				  cursor = database.query(MySQLiteHelper.IMAGE_TALBLE,
						    		allColumns , MySQLiteHelper.IMAGE_COLIMN_AGENT_ID+"= ? AND "+MySQLiteHelper.IMAGE_COLUMN_IMG_ID+"= ?",
						    		new String[]{String.valueOf(agent_id) , String.valueOf(imageId)}, null, null, null);
				  try{
					  if(cursor.moveToFirst()){
						  while (!cursor.isAfterLast()) {
							  Log.d(" cursor length ", " image database " + cursor.getCount() );
							  image = cursorToImage(cursor) ;
							  cursor.moveToNext() ; 
							}
					  }
				  }finally{
					  cursor.close() ; 
					  this.close() ; 
				  }
				 return image ;
			  
			  }
			  
			  public BTImage selectImagesWithName(String imageName,  int agent_id){
				  this.open() ; 
				  Cursor cursor  = null  ; 		
				  BTImage image  = null  ; 
				  cursor = database.query(MySQLiteHelper.IMAGE_TALBLE,
						    		allColumns , MySQLiteHelper.IMAGE_COLIMN_AGENT_ID+"= ? AND "+MySQLiteHelper.IMAGE_COLUMN_IMAGE_NAME+"= ?",
						    		new String[]{String.valueOf(agent_id) , imageName}, null, null, null);
				  try{
					  if(cursor.moveToFirst()){
						  while (!cursor.isAfterLast()) {
							  image = cursorToImage(cursor) ; 
							  cursor.moveToNext() ; 
							}
					  }
				  }finally{
					  cursor.close() ; 
					  this.close() ; 
				  }
			  Log.d(" image ",  " image details "+ cursor.getCount() + " " + cursor.getColumnCount() );
				  return image ; 			
			  
			  }
			  
			  
			  private BTImage cursorToImage(Cursor cursor){
				  /*image_id integer primary key autoincrement , image_name text , agent_id integer 
				   * , response_id  integer , is_sent integer , img_date*/
				  BTImage image = new BTImage() ; 
				  image.setImageId(cursor.getInt(0)) ; 
				  image.setImageName(cursor.getString(1)) ; 
				  image.setAgentId(cursor.getInt(2)) ; 
				  image.setResponseId(cursor.getInt(3)) ; 
				  image.setIsSent(cursor.getInt(4)) ; 
				  image.setImageDate(cursor.getString(5)) ;
				  image.setImageDataOCR(cursor.getString(6));
				  image.setImagePath_OCR(cursor.getString(7));
				  image.setImagetxtOcr(cursor.getString(8));

				  return image ;   
			  }
			  

}
