package com.biptag.biptag.dbmanager.datasource;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import com.biptag.biptag.dbmanager.MySQLiteHelper;
import com.biptag.biptag.dbmanager.objects.BTCard;

public class BTCardDataSource {
	
	
	 private SQLiteDatabase database;
	  private MySQLiteHelper dbHelper;
	
	private String[] allcolumns =new String[]{
			MySQLiteHelper.VCARD_COLUMN_ID ,
			MySQLiteHelper.VCARD_COLUMN_DEF,
			MySQLiteHelper.VCARD_COLUMN_BA_ID
	} ; 

	public BTCardDataSource(Context  context) {
		// TODO Auto-generated constructor stub
	    dbHelper = new MySQLiteHelper(context);
			  }
		  private void open() throws SQLException {
			    database = dbHelper.getWritableDatabase();
			  }
			
		  private void close() {
			    dbHelper.close();
			  }
		  
		  
		  public boolean insertVcar(BTCard card ){
			  long id = 0 ; 
			  try{
				  this.open() ; 
				  ContentValues values = new  ContentValues()  ; 
				  
				  values.put(MySQLiteHelper.VCARD_COLUMN_ID, card.getId() ) ; 
				  values.put(MySQLiteHelper.VCARD_COLUMN_DEF, card.getDef().toString()) ; 
				  values.put(MySQLiteHelper.VCARD_COLUMN_BA_ID, card.getBaID()) ; 
				  
				  id = this.database.insert(MySQLiteHelper.TABLE_VCARDS, null, values) ; 
				  
			  }finally{
				  this.close() ; 
			  }
			  
			  
			  return id!=0 ; 
		  }
		  
		  
		  public List<BTCard> selectCards(int ba_id){
			  List<BTCard> cards = new LinkedList<BTCard>() ;
			  
			  Cursor cursor= null ; 
			  try{
				  this.open() ; 
				  
				  cursor = this.database.query(true, MySQLiteHelper.TABLE_VCARDS, allcolumns, "ba_id = ?", new String[]{String.valueOf(ba_id) }, null, null, null, null, null) ; 
				  if(cursor!=null){
					  cursor.moveToFirst() ; 
					  
					  while(!cursor.isAfterLast()){
						  
						  BTCard card = cursorToCard(cursor) ; 
						  
						  cards.add(card) ; 
						  
						  
						  cursor.moveToNext() ; 
					  }
				  }
				  
				  
			  }finally{
				  if(cursor != null){
					  cursor.close() ; 
				  }
				  
				  this.close() ; 
			  }
			  
			  
			  
			  
			  return cards ; 
		  }

		  
		  public BTCard getCard(int ba_id , int id ){
			  BTCard card = null; 
			  
			  Cursor cursor= null ; 
			  try{
				  this.open() ; 
				  
				  cursor = this.database.query(true, MySQLiteHelper.TABLE_VCARDS, allcolumns, "ba_id = ? and id= ?", new String[]{String.valueOf(ba_id), String.valueOf(id) }, null, null, null, null, null) ; 
				  if(cursor!=null){
					  cursor.moveToFirst() ; 
					  
					  while(!cursor.isAfterLast()){
						  
						  card = cursorToCard(cursor) ; 
						  
						  
						  
						  cursor.moveToNext() ; 
					  }
				  }
				  
				  
			  }finally{
				  if(cursor != null){
					  cursor.close() ; 
				  }
				  
				  this.close() ; 
			  }
			  
			  
			  
			  
			  return card ; 
		  }

		  
		  
		  private BTCard cursorToCard(Cursor cursor) {
			// TODO Auto-generated method stub

			BTCard  card = new BTCard() ; 
			
			
			card.setId(cursor.getInt(0)) ; 
			try {
				JSONObject object = new JSONObject(cursor.getString(1)) ; 
				card.setDef(object) ; 
			} catch (JSONException e) {
				// TODO: handle exception
			}
			card.setBaID(cursor.getInt(2)) ;
			
			return card;
		}
		  

}
