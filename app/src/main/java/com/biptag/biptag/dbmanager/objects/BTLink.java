package com.biptag.biptag.dbmanager.objects;

public class BTLink {

	private int id ; 
	private String url ; 
	private String lable ; 
	private int baID  ; 
	
	public BTLink() {
		// TODO Auto-generated constructor stub
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getLable() {
		return lable;
	}

	public void setLable(String lable) {
		this.lable = lable;
	}

	public int getBaID() {
		return baID;
	}

	public void setBaID(int baID) {
		this.baID = baID;
	}
	
	
	
	
}
