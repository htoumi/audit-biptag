package com.biptag.biptag.dbmanager.datasource;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.biptag.biptag.dbmanager.MySQLiteHelper;
import com.biptag.biptag.dbmanager.objects.BTUserConfig;

public class BtUserConfigDataSource {
	 private SQLiteDatabase database;
	  private MySQLiteHelper dbHelper;
	
	
	  private String[] allColumns = { MySQLiteHelper.CONFIG_COLUMN_CONFIG_ID,
			  MySQLiteHelper.CONFIG_COLUMN_DEFAULT_COUNTRY,
			  MySQLiteHelper.CONFIG_COLUMN_LANGUAGE,
			  MySQLiteHelper.CONFIG_COLUMN_AGENT_ID,
			  };
	  
	  
	  public BtUserConfigDataSource(Context context) {
		// TODO Auto-generated constructor stub
		  dbHelper = new MySQLiteHelper(context);
	  }
	  private void open() throws SQLException { 
		  database = dbHelper.getWritableDatabase();
	  }
	
	  private void close() { 
		  
	    dbHelper.close();
	  }  
	  
	  public long insertConfig(BTUserConfig config){
		  
		  this.open() ; 
		  long insertId = 0 ; 
		
		  try{
			  ContentValues values = new ContentValues() ; 
			  values.put(MySQLiteHelper.CONFIG_COLUMN_AGENT_ID, config.getAgent_id()) ; 
			  values.put(MySQLiteHelper.CONFIG_COLUMN_LANGUAGE, config.getLanguage()) ; 
			  values.put(MySQLiteHelper.CONFIG_COLUMN_DEFAULT_COUNTRY, config.getDefaultCountry()) ; 
			  insertId = database.insert(MySQLiteHelper.CONFIG_TALBLE, null,
				        values);
			 
			  
		  }finally{
			  this.close() ; 
		  }
		  
		  return insertId ; 
	  }
	  
	  
	  public boolean updateConfig(BTUserConfig config){

		  long insertId  = 0;
		  this.open() ; 
		  
		  try{

			  ContentValues values = new ContentValues() ; 
			  values.put(MySQLiteHelper.CONFIG_COLUMN_AGENT_ID, config.getAgent_id()) ; 
			  values.put(MySQLiteHelper.CONFIG_COLUMN_LANGUAGE, config.getLanguage()) ; 
			  values.put(MySQLiteHelper.CONFIG_COLUMN_DEFAULT_COUNTRY, config.getDefaultCountry()) ; 
			  
			  insertId = database.update(MySQLiteHelper.CONFIG_TALBLE, values, MySQLiteHelper.CONFIG_COLUMN_AGENT_ID+"= ?", new String[]{String.valueOf(config.getAgent_id()) }) ; 
			  
		  }finally{
			  this.close() ; 
		  }
		  
		  if(insertId == 0){
			  return false ; 
		  }else{
			  return true ; 
		  }
		  
	  }
	  
	  public BTUserConfig getUserConfig(int agent_id){
		  
			 BTUserConfig config = null ; 
			 this.open() ; 
		  if(database!= null){
			  Cursor  cursor = database.query(MySQLiteHelper.CONFIG_TALBLE,
			    		allColumns , MySQLiteHelper.CONFIG_COLUMN_AGENT_ID+"= ?" ,
			    		new String[]{String.valueOf(agent_id)}, null, null, null);
			try{
				if(cursor != null){
					 cursor.moveToFirst();
					    while (cursor.moveToNext()){
					    	config = cursorToUserCOnfig(cursor) ; 
					    }
				}	  
			  
			}finally{
				cursor.close() ; 
				this.close() ; 
			}
		  
		  
		  }else{
		  }
		
		return config ; 
		
	  }
	  
	  public BTUserConfig cursorToUserCOnfig(Cursor cursor){
		  BTUserConfig config = new BTUserConfig() ; 
		  config.setConfigId(cursor.getInt(0)) ; 
		  config.setDefaultCountry(cursor.getString(1)) ; 
		  config.setLanguage(cursor.getString(2)) ; 
		  config.setAgent_id(cursor.getInt(3)) ; 
		  
		  return config ; 
	  }
	  
	  
}
