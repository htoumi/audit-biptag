package com.biptag.biptag.dbmanager.objects;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Calendar;
import java.util.Date;

import com.biptag.biptag.NavigationActivity;
import com.biptag.biptag.managers.BiptagUserManger;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.text.format.Time;
import android.util.Log;

public class BTImage {
	 private int imageId  ; 
	  private String imageName ; 
	  private int agentId ; 
	  private int responseId ; 
	  private int IsSent ; 
	  private String imageDate ;
	  private String imagePath ;
	  private String imagePath_OCR ;
	  private String image_txtOCR ;
	  private String imageDataOCR ;
	  private Context context ;
	  private Bitmap bitmap ; 

	

	

	public BTImage() {
			// TODO Auto-generated constructor stub
		}

	  public BTImage(Context c) {
			// TODO Auto-generated constructor stub
		  this.context =c ; 
	  }
	  
	  
	  public Bitmap getBitmap() {
			return bitmap;
		}

		public void setBitmap(Bitmap bitmap) {
			this.bitmap = bitmap;
		}
	  public String getImagePath() {
			return imagePath;
		}
	  
	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}

	
	public int getImageId() {
		return imageId;
	}

	public void setImageId(int imageId) {
		this.imageId = imageId;
	}
	public void setImageDataOCR(String imageDataOCR) {
		this.imageDataOCR = imageDataOCR;
	}
	public void setImagetxtOcr(String image_txtOCR) {

		this.image_txtOCR = image_txtOCR;
	}
	public void setImagePath_OCR(String imagePath_OCR) {

		this.imagePath_OCR = imagePath_OCR;
	}


	public String getImageDataOCR() {
		return imageDataOCR;
	}
	public String getImagetxtOCR() {
		return image_txtOCR;
	}
	public String getImagePathOCR() {
		return imagePath_OCR;
	}
public String getImageName() {
		return imageName;
	}

	public void setImageName(String imageName) {
		this.imageName = imageName;
	}

	public int getAgentId() {
		return agentId;
	}

	public void setAgentId(int agentId) {
		this.agentId = agentId;
	}

	public int getResponseId() {
		return responseId;
	}

	public void setResponseId(int responseId) {
		this.responseId = responseId;
	}

	public int getIsSent() {
		return IsSent;
	}

	public void setIsSent(int isSent) {
		IsSent = isSent;
	}

	public String getImageDate() {
		return imageDate;
	}

	public void setImageDate(String imageDate) {
		this.imageDate = imageDate;
	}
	
	public boolean cacheImage(){
		boolean saved = true ; 
		File imageFile = new File(imagePath) ; 
		if(imageFile.exists()){
			
			File directory = this.context.getCacheDir() ; 
			if(!directory.exists()){
				directory.mkdir() ;	
			}
			Calendar calendar = Calendar.getInstance() ; 
			String seconds = calendar.get(Calendar.DATE) +""+ calendar.get(Calendar.SECOND) ;
			Log.d(" time ",  seconds + " millisecond " + System.currentTimeMillis());
			String NameImage=	String.valueOf(BiptagUserManger.getUserValue("EMAIL", "string", NavigationActivity.getContext())) +  seconds;


			Log.d(" Image ", " name " + NameImage);
			String imageFileName = md5(String.valueOf(seconds)) ;
			Log.d(" image ", imageFileName);
			if(!imageFileName.equals("")){
				this.setImageName(imageFileName) ;
				
				File savedImage = new File(directory.getAbsolutePath()+"/"+imageFileName+".jpg") ; 
					try {
						savedImage.createNewFile() ;
						File source= new File(this.getImagePath()) ; 
						this.copy(source, savedImage) ; 
						saved = true ; 
						
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						saved = false ; 
					} 
					
			}else{
				saved = false ;
			}
			
			
		}else{
			saved = false ; 
		}
		
		
		return saved; 
	}
	public void copy(File src, File dst) throws IOException {
	    InputStream in = new FileInputStream(src);
	    OutputStream out = new FileOutputStream(dst);

	    // Transfer bytes from in to out
	    byte[] buf = new byte[1024];
	    int len;
	    while ((len = in.read(buf)) > 0) {
	        out.write(buf, 0, len);
	    }
	    in.close();
	    out.close();
	}
	
	public static final String md5(final String s) {
	    final String MD5 = "MD5";
	    try {
	        // Create MD5 Hash
	        MessageDigest digest = java.security.MessageDigest
	                .getInstance(MD5);
	        digest.update(s.getBytes());
	        byte messageDigest[] = digest.digest();

	        // Create Hex String
	        StringBuilder hexString = new StringBuilder();
	        for (byte aMessageDigest : messageDigest) {
	            String h = Integer.toHexString(0xFF & aMessageDigest);
	            while (h.length() < 2)
	                h = "0" + h;
	            hexString.append(h);
	        } 
	        return hexString.toString();

	    } catch (NoSuchAlgorithmException e) {
	        e.printStackTrace();
	    }
	    return "";
	}
	
	public void saveBitmap(){
		

		File directory = this.context.getCacheDir() ; 
		if(!directory.exists()){
			directory.mkdir() ;	
		}
		Calendar calendar = Calendar.getInstance() ;
		String seconds = calendar.get(Calendar.DATE)+calendar.get(Calendar.YEAR) +""+ calendar.get(Calendar.SECOND) ;
		Calendar cal = Calendar.getInstance();
		Date today = new Date();
		cal.setTime(today);
		Time today2 = new Time(Time.getCurrentTimezone());
		today2.setToNow();
		seconds = today2.toString();
		Log.d(" time ",  seconds + " millisecond " + System.currentTimeMillis());
		String NameImage=	String.valueOf(BiptagUserManger.getUserValue("EMAIL", "string", NavigationActivity.getContext())) +  seconds;

		Log.d(" Image ", " name " + NameImage);
		int agent_id = Integer.parseInt(String.valueOf(BiptagUserManger.getUserValue("AGENT_ID", "int",NavigationActivity.getContext()))) ; 

		String imageFileName = md5(String.valueOf(agent_id)+"-"+String.valueOf(NameImage)) ;
		if(!imageFileName.equals("")){
			this.setImageName(imageFileName) ;
		 
		ByteArrayOutputStream bytes = new ByteArrayOutputStream();
		
		this.bitmap.compress(Bitmap.CompressFormat.PNG, 100, bytes);
		 
		//you can create a new file name "test.jpg" in sdcard folder.
		File f = new File(directory.getAbsolutePath()
		                        + File.separator + imageFileName+".jpg") ; 
		try {
			f.createNewFile();
			FileOutputStream fo = new FileOutputStream(f);
			fo.write(bytes.toByteArray());

			// remember close de FileOutput
			fo.close();
			this.setImagePath(directory.getAbsolutePath()
		                        + File.separator + imageFileName+".jpg") ; 
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//write the bytes in file
		}
	}
	  
	  
	  

}
