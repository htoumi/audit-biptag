package com.biptag.biptag.dbmanager.objects;

import org.json.JSONObject;

public class BTCard {

	private int id ; 
	private JSONObject def ; 
	private int baID ; 
	
	public BTCard() {
		// TODO Auto-generated constructor stub
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public JSONObject getDef() {
		return def;
	}

	public void setDef(JSONObject def) {
		this.def = def;
	}

	public int getBaID() {
		return baID;
	}

	public void setBaID(int baID) {
		this.baID = baID;
	}
	
	
	
	
}
