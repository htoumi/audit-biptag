package com.biptag.biptag.dbmanager.objects;

public class BTCity {

	String cityName ; 
	String zipCode ; 
	boolean isUserEntry  ; 
	
	public BTCity() {
		// TODO Auto-generated constructor stub
	}

	public String getCityName() {
		return cityName;
	}

	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public boolean isUserEntry() {
		return isUserEntry;
	}

	public void setUserEntry(boolean isUserEntry) {
		this.isUserEntry = isUserEntry;
	}
	
	
	
	
}
