package com.biptag.biptag.dbmanager.datasource;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.w3c.dom.Comment;

import com.biptag.biptag.dbmanager.MySQLiteHelper;
import com.biptag.biptag.dbmanager.objects.BTFormulaire;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import android.webkit.WebChromeClient.CustomViewCallback;

public class BTFormDataSource {

	 private SQLiteDatabase database;
	  private MySQLiteHelper dbHelper;
	
	  private String[] allColumns = { MySQLiteHelper.FORM_COLUMN_ID,
			  MySQLiteHelper.FORM_COLUMN_LABEL,
			  MySQLiteHelper.FORM_COLUMN_STRUCTURE,
			  MySQLiteHelper.FORM_COLUMN_STATUS,
			  MySQLiteHelper.FORM_COLUMN_EV_NAME,
			  MySQLiteHelper.FORM_COLUMN_EV_START_DATE,
			  MySQLiteHelper.FORM_COLUMN_EV_END_DATE,
			  MySQLiteHelper.FORM_COLUMN_EV_EDITION,
			  MySQLiteHelper.FORM_COLUMN_AGENT_ID };
	 
	  public BTFormDataSource(Context context) {
		    dbHelper = new MySQLiteHelper(context);
		  }
	  private void open() throws SQLException {
		    database = dbHelper.getWritableDatabase();
		  }
		
	  private void close() {
		    dbHelper.close();
		  }
	  
	  public boolean insertForm(BTFormulaire formulaire) {
		  	this.open() ; 
		  	
		  	
		  	long insertId = 0 ; 
		  	
		  	try{
			    ContentValues values = new ContentValues();
			    values.put(MySQLiteHelper.FORM_COLUMN_AGENT_ID, formulaire.getAgentId());
			    values.put(MySQLiteHelper.FORM_COLUMN_EV_EDITION, formulaire.getEventEdition()); 
			    values.put(MySQLiteHelper.FORM_COLUMN_EV_END_DATE , formulaire.getEventEndDate() ) ;
			    values.put(MySQLiteHelper.FORM_COLUMN_EV_NAME , formulaire.getEventName()) ;
			    values.put(MySQLiteHelper.FORM_COLUMN_EV_START_DATE , formulaire.getEventStartDate()) ;
			    values.put(MySQLiteHelper.FORM_COLUMN_STATUS , formulaire.getFormStatus()) ;
			    values.put(MySQLiteHelper.FORM_COLUMN_LABEL , formulaire.getFormName()) ;

			    values.put(MySQLiteHelper.FORM_COLUMN_ID , formulaire.getFormId()) ;
			    values.put(MySQLiteHelper.FORM_COLUMN_STRUCTURE , formulaire.getFormStructure().toString()) ;
			    
			    
			    insertId = database.insert(MySQLiteHelper.TABLE_FORMS, null,
			        values);
		  	}finally{
		  		this.close() ; 
		  	}
		    
		    if(insertId!= 0){

		    	return true ; 
		    }else{

		    	return false ; 
		    }
		    
		  }
	  
	  public boolean updateForm(BTFormulaire formulaire) {
		  	
		  long  insertId = 0 ; 
		  this.open() ; 
		  
		  
		  try{

			  ContentValues values = new ContentValues();
			    values.put(MySQLiteHelper.FORM_COLUMN_AGENT_ID, formulaire.getAgentId());
			    values.put(MySQLiteHelper.FORM_COLUMN_EV_EDITION, formulaire.getEventEdition()); 
			    values.put(MySQLiteHelper.FORM_COLUMN_EV_END_DATE , formulaire.getEventEndDate() ) ;
			    values.put(MySQLiteHelper.FORM_COLUMN_EV_NAME , formulaire.getEventName()) ;
			    values.put(MySQLiteHelper.FORM_COLUMN_EV_START_DATE , formulaire.getEventStartDate()) ;
			    values.put(MySQLiteHelper.FORM_COLUMN_STATUS , formulaire.getFormStatus()) ;
			    values.put(MySQLiteHelper.FORM_COLUMN_LABEL , formulaire.getFormName()) ;
			    values.put(MySQLiteHelper.FORM_COLUMN_STRUCTURE , formulaire.getFormStructure().toString()) ;
			    
			    
			    insertId = database.update(MySQLiteHelper.TABLE_FORMS, values, MySQLiteHelper.FORM_COLUMN_ID+"= ?", new String[]{String.valueOf(formulaire.getFormId()) }) ;
			    
		  }finally{
			  this.close() ; 
		  }
		  if(insertId!= 0){

		    	return true ; 
		    }else{

		    	return false ; 
		    }
		    
		  }
	  
	  
	  public List<BTFormulaire> getAllForms( int agent_id ) {
		  this.open() ; 
		  List<BTFormulaire> forms= new LinkedList<BTFormulaire>();

		    Cursor cursor = database.query(MySQLiteHelper.TABLE_FORMS,
		    		allColumns , MySQLiteHelper.FORM_COLUMN_AGENT_ID+"= ?",
		    		new String[]{String.valueOf(agent_id)}, null, null, null);
		   try{
			   
			    if(cursor!= null){
			    cursor.moveToFirst();
			    while (!cursor.isAfterLast()) {
			      BTFormulaire form= cursorToForm(cursor);
			      forms.add(form);
			      cursor.moveToNext();
			    }
			    // make sure to close the cursor
			  }
		   }finally{
			   cursor.close() ; 
			   this.close() ; 
		   }
		    
		    
		    
		    return forms;
			  
		   
	  
	  }
	  
	  
	  public BTFormulaire getFormWithID( int agent_id  , int form_id ) {
		  	this.open() ; 
		  	BTFormulaire form  = null  ; 
		  
		    Cursor cursor = database.query(MySQLiteHelper.TABLE_FORMS,
		    		allColumns , MySQLiteHelper.FORM_COLUMN_AGENT_ID+"= ? AND "+MySQLiteHelper.FORM_COLUMN_ID +"= ? ",
		    		new String[]{String.valueOf(agent_id) , String.valueOf(form_id)}, null, null, null);
		     
		    try{
			    if(cursor!= null){
				    cursor.moveToFirst();
				    while (!cursor.isAfterLast()) {
				      form= cursorToForm(cursor);
				      cursor.moveToNext();
				    }

				  }
		    }finally{
		    	cursor.close() ; 
		    	this.close() ; 
		    }
		    
		    return form ; 
	  }
	  
	  
	  public BTFormulaire cursorToForm(Cursor cursor){
		  BTFormulaire formulaire = new BTFormulaire() ;
		  
		  formulaire.setFormId(cursor.getInt(0)) ; 
		  formulaire.setFormName(cursor.getString(1)) ; 
		  try {
			formulaire.setFormStructure(new JSONArray(cursor.getString(2))) ;
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			
			e.printStackTrace();
		}  
		  formulaire.setFormStatus(cursor.getString(3)) ; 
		  formulaire.setEventName(cursor.getString(4)) ; 
		  formulaire.setEventStartDate(cursor.getString(5)) ; 
		  formulaire.setEventEndDate(cursor.getString(6)) ; 
		  formulaire.setEventEdition(cursor.getString(7)) ; 
		  formulaire.setAgentId(cursor.getInt(8))  ; 
		  
		
		  
		  return formulaire ; 
		  
	  }
	
	
}
