package com.biptag.biptag.dbmanager.objects;

public class BTFile {
	
	private int id  ; 
	private String name ; 
	private String phName  ;
	private int baId ; 
	
	public BTFile() {
		// TODO Auto-generated constructor stub
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPhName() {
		return phName;
	}

	public void setPhName(String phName) {
		this.phName = phName;
	}

	public int getBaId() {
		return baId;
	}

	public void setBaId(int baId) {
		this.baId = baId;
	}
	
	
	

}
