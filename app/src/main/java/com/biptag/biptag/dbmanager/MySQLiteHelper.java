package com.biptag.biptag.dbmanager;

import org.json.JSONArray;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class MySQLiteHelper extends SQLiteOpenHelper {

	// table forms
  public static final String TABLE_FORMS = "forms";
  public static final String FORM_COLUMN_ID = "form_id";
  public static final String FORM_COLUMN_LABEL= "form_label";
  public static final String FORM_COLUMN_STRUCTURE= "form_structure";
  public static final String FORM_COLUMN_EV_NAME= "ev_name";
  public static final String FORM_COLUMN_EV_EDITION= "ev_edition";
  public static final String FORM_COLUMN_EV_START_DATE= "ev_start_date";
  public static final String FORM_COLUMN_EV_END_DATE= "ev_end_date";
  public static final String FORM_COLUMN_STATUS= "form_status";
  public static final String FORM_COLUMN_AGENT_ID= "form_agent_id";
  
  
  // table contact 

  public static final String TABLE_CONTACT = "contacts" ; 
  public static final String CONTACT_COLUMN_LOCAL_ID = "contact_local_id" ;
  public static final String CONTACT_COLUMN_EMAIL = "contact_email" ;
  public static final String CONTACT_COLUMN_ID = "contact_id" ; 
  public static final String CONTACT_COLUMN_FORM_ID = "form_id" ;
  public static final String CONTACT_COLUMN_FR_ID = "fr_id" ;
  public static final String CONTACT_COLUMN_FR_HASHCODE = "fr_hashcode" ;
  public static final String CONTACT_COLUMN_AGENT_ID = "contact_agent_id" ;
  public static final String CONTACT_COLUMN_FR_CONTENT = "fr_content" ;
  public static final String CONTACT_COLUMN_IS_SYNCHRONIZED = "is_synchronized" ;
  public static final String CONTACT_COLUMN_DO_FOLLOW_UP = "do_followu_up" ; 
  public static final String CONTACT_COLUMN_NAME = "contact_name" ; 
  public static final String CONTACT_COLUMN_CC_EMAIL = "cc_register" ; 
  public static final String CONTACT_COLUMN_CCI_EMAIL= "cci_register" ;
  public static final String CONTACT_COLUMN_DATE= "contact_date" ;

  
  // table followups  
  public static final String TABLE_FOLLOW_UPS = "followups" ; 
  public static final String FOLLOW_UP_COLUMN_FU_ID = "fu_id" ;
  public static final String FOLLOW_UP_COLUMN_BA_ID = "ba_id" ;
  public static final String FOLLOW_UP_COLUMN_FU_CONTENT = "fu_content" ;
  public static final String FOLLOW_UP_COLUMN_FU_SUBJECT = "fu_subject" ;
  public static final String FOLLOW_UP_COLUMN_FU_ATTACH = "fu_attach" ;
  public static final String FOLLOW_UP_COLUMN_FU_NAME = "fu_temp_name" ;
  public static final String FOLLOW_UP_COLUMN_FU_HASHCODE = "fu_hashcode" ;
  public static final String FOLLOW_UP_COLUMN_IS_SYNC = "is_synchronized" ; 
  public static final String FOLLOW_UP_COLUMN_LOCAl_ID = "fu_local_id" ; 
  public static final String follow_up_create_sql= "create table if not exists followups (fu_id integer , ba_id integer , fu_content text  ,  fu_subject text , fu_attach text , fu_temp_name text , is_synchronized integer , fu_local_id integer primary key autoincrement , fu_hashcode text ) ; " ;
  public static final String logs_create_sql = "create table if not exists logs (log_id integer , log_type text , log_timestamp text , log_rel_object text , log_rel_object_id  integer ,log_rel_object_2 text , log_rel_object_2_id  integer ,fl_subject text , fl_content text , fl_send_date text , fl_id integer ,  fl_contact_id integer ,  agent_id integer )   ;" ; 
  public static final String images_create_sql = "create table if not exists images (image_id integer primary key autoincrement , image_name text , agent_id integer , response_id  integer , is_sent integer , img_date text , img_data_ocr text , img_path_ocr text , img_txt_ocr text ) ;" ;
  public static final String configuration_create_sql = "create table if not exists configuration (config_id integer  primary key autoincrement , default_country text, language text , agent_id integer  ) ;" ; 
  public static final String available_countries_sql = "create table if not exists available_countries (ac_id integer , ac_name text , ac_file_name text , ac_last_update text)" ; 
  public static final String AVAILBLE_COUNTRIES_COLUMN_ID = "ac_id" ; 
  public static final String AVAILBLE_COUNTRIES_COLUMN_NAME= "ac_name" ; 
  public static final String AVAILBLE_COUNTRIES_COLUMN_FILE_NAME= "ac_file_name" ; 
  public static final String AVAILBLE_COUNTRIES_COLUMN_LAST_UPDATE = "ac_last_update" ; 
  public static final String AVAILBLE_COUNTRY_TABLE_NAME = "available_countries" ; 
  
  
  
  
  
  
  
  
  
  public static final String LOG_TALBLE = "logs" ; 
  /*log_id , log_type , log_timestamp , log_rel_object ,
   *  log_rel_object_id  ,log_rel_object_2 , 
   * log_rel_object_2_id  ,fl_subject , fl_content , 
   * fl_send_date , fl_id ,  fl_contact_id ,  agent_id */
  public static final String LOG_COLUMN_LOG_ID = "log_id" ;
  public static final String LOG_COLUMN_LOG_TYPE = "log_type" ;
  public static final String LOG_COLUMN_LOG_TIMESTAMP= "log_timestamp" ;
  public static final String LOG_COLUMN_LOG_REL_OBJECT = "log_rel_object" ;
  public static final String LOG_COLUMN_LOG_REL_OBJECT_ID= "log_rel_object_id" ;
  public static final String LOG_COLUMN_LOG_REL_OBJECT_2 = "log_rel_object_2" ;
  public static final String LOG_COLUMN_LOG_REL_OBJECT_2_ID = "log_rel_object_2_id" ;
  public static final String LOG_COLUMN_FL_SUBJECT = "fl_subject" ;
  public static final String LOG_COLUMN_FL_CONTENT = "fl_content" ;
  public static final String LOG_COLUMN_FL_ID = "fl_id" ;
  public static final String LOG_COLUMN_FL_CONTACT_ID= "fl_contact_id" ;
  public static final String LOG_COLUMN_AGENT_ID = "agent_id" ;
  public static final String LOG_COLUMN_SEND_DATE= "fl_send_date" ;
  
  
  /*image_id integer primary key autoincrement , image_name text , agent_id integer 
   * , response_id  integer , is_sent integer , img_date*/
  public static final String IMAGE_COLUMN_IMG_ID = "image_id" ;
  public static final String IMAGE_COLUMN_IMAGE_NAME = "image_name" ; 
  public static final String IMAGE_COLIMN_AGENT_ID = "agent_id" ; 
  public static final String IMAGE_COLUMN_RESPONSE_ID = "response_id" ; 
  public static final String IMAGE_COLUMN_IS_SENT = "is_sent" ; 
  public static final String IMAGE_COLUMN_IMAGE_DATE = "img_date" ; 
  public static final String IMAGE_COLUMN_Data_OCR = "img_data_ocr" ;
  public static final String IMAGE_COLUMN_Path_OCR = "img_path_ocr" ;
  public static final String IMAGE_COLUMN_txt_OCR = "img_txt_ocr" ;

  
  
  public static final String IMAGE_TALBLE = "images" ; 
  public static final String CONFIG_TALBLE = "configuration" ; 
  public static final String CONFIG_COLUMN_CONFIG_ID  ="config_id";
  public static final String CONFIG_COLUMN_DEFAULT_COUNTRY = "default_country" ; 
  public static final String CONFIG_COLUMN_LANGUAGE = "language" ; 
  public static final String CONFIG_COLUMN_AGENT_ID = "agent_id" ; 
  
  
  private static final String DATABASE_NAME = "biptag.db";
  private static final int DATABASE_VERSION = 3;

  // Database creation sql statement
  private static final String DATABASE_CREATE_FORM = "create table if not exists  "
      + TABLE_FORMS + "(" + FORM_COLUMN_ID
      + " integer, " + FORM_COLUMN_LABEL
      + " text not null, " + FORM_COLUMN_STRUCTURE
      + " text not null, " + FORM_COLUMN_STATUS
      + " text not null, " + FORM_COLUMN_EV_NAME
      + " text not null, " + FORM_COLUMN_EV_START_DATE
      + " text not null, " + FORM_COLUMN_EV_END_DATE
      + " text not null, " + FORM_COLUMN_EV_EDITION
      + " text not null, " + FORM_COLUMN_AGENT_ID
      + " integer);";
  private static final String DATABASE_CREATE_CONTACTS = "create table if not exists "+TABLE_CONTACT+" ( " +
  		""+CONTACT_COLUMN_LOCAL_ID+" integer primary key autoincrement  , " +
  				"" +CONTACT_COLUMN_EMAIL+" text not null , "+
  				CONTACT_COLUMN_FR_CONTENT+" text not null , "+
  				CONTACT_COLUMN_FR_HASHCODE+" text not null , "+
  				CONTACT_COLUMN_ID+" integer, "+
  				CONTACT_COLUMN_FORM_ID+" integer, "+
  				CONTACT_COLUMN_FR_ID+" integer, "+
  				CONTACT_COLUMN_IS_SYNCHRONIZED+" integer, "+
  				CONTACT_COLUMN_AGENT_ID+" integer, "+
  				CONTACT_COLUMN_DO_FOLLOW_UP+" integer ,"+
  				CONTACT_COLUMN_NAME +" text ," +
  				CONTACT_COLUMN_CC_EMAIL+" text ,"+
  				CONTACT_COLUMN_CCI_EMAIL+" text "+
  				" , "+CONTACT_COLUMN_DATE+" text) ; " ;
  
  
  public static final String TABLE_FILES = "files" ; 
  public static final String FILE_COLUMN_ID = "id" ; 
  public static final String FILE_COLUMN_NAME = "name" ; 
  public static final String FILE_COLUMN_PH_NAME="ph_name"   ; 
  public static final String FILE_COLUMN_BA_ID = "ba_id" ; 
  
  
  private static final String create_table_files = "create table if not exists "+TABLE_FILES+" ( "+FILE_COLUMN_ID+" integer , "+
  FILE_COLUMN_NAME+" text , "+FILE_COLUMN_PH_NAME+" text  , "+FILE_COLUMN_BA_ID +" integer)" ; 
  
  
  public static final String TABLE_LINKS = "links" ;
  public static final String LINK_COLUMN_ID = "id" ; 
  public static final String LINK_COLUMN_URL = "url" ; 
  public static final String LINK_COLUMN_LABEL = "label" ; 
  public static final String LINK_COLUMN_BA_ID = "ba_id" ; 
  
  private static final String create_table_links = "create table if not exists "+TABLE_LINKS+" ( "+LINK_COLUMN_ID+" integer  , "+
  LINK_COLUMN_URL+" text , "+LINK_COLUMN_LABEL+" text , "+LINK_COLUMN_BA_ID +" integer )" ; 
  
  
  public static final String TABLE_VCARDS = "vcards" ; 
  
  public static final String VCARD_COLUMN_ID = "id" ; 
  public static final String VCARD_COLUMN_DEF = "vcard_def" ; 
  public static final String VCARD_COLUMN_BA_ID = "ba_id" ; 
  
  private static final String create_table_vcards = "create table if not exists "+TABLE_VCARDS+" ( "+VCARD_COLUMN_ID+" integer ,"+VCARD_COLUMN_DEF+" text  , "+VCARD_COLUMN_BA_ID +" integer) " ;

  private static final String DATABASE_ALTER_Imag = "ALTER TABLE "
          + IMAGE_TALBLE + " ADD COLUMN " + IMAGE_COLUMN_Data_OCR + " string ;";
  private static final String DATABASE_ALTER_Image = "ALTER TABLE "
          + IMAGE_TALBLE + " ADD COLUMN " + IMAGE_COLUMN_Path_OCR + " string ;";
  private static final String DATABASE_ALTER_Images = "ALTER TABLE "
          + IMAGE_TALBLE + " ADD COLUMN "  + IMAGE_COLUMN_txt_OCR  + " string ;";

 private static final String DATABASE_ALTER_Contact = "ALTER TABLE "
          + TABLE_CONTACT + " ADD COLUMN " + CONTACT_COLUMN_DATE + " string;";
  


  public MySQLiteHelper(Context context) {
    super(context, DATABASE_NAME, null, DATABASE_VERSION);
  } 
  
  @Override
  public void onCreate(SQLiteDatabase database) {
    database.execSQL(DATABASE_CREATE_CONTACTS);
    database.execSQL(DATABASE_CREATE_FORM) ; 
    database.execSQL(follow_up_create_sql) ; 
    database.execSQL(images_create_sql); 
    database.execSQL(logs_create_sql);
    database.execSQL(configuration_create_sql) ; 
    database.execSQL(available_countries_sql) ;  
    database.execSQL(create_table_files) ; 
    database.execSQL(create_table_links) ; 
    database.execSQL(create_table_vcards) ; 
  
  }
  @Override
  public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    //Log.w(MySQLiteHelper.class.getName(),"Upgrading database from version " + oldVersion + " to " + newVersion + ", which will destroy all old data");
  if (DATABASE_VERSION==3) {
    db.execSQL(DATABASE_ALTER_Imag);
    db.execSQL(DATABASE_ALTER_Image);
    db.execSQL(DATABASE_ALTER_Images);

  }
   // db.execSQL("DROP TABLE IF EXISTS " + TABLE_FORMS);
    //db.execSQL("DROP TABLE IF EXISTS " + TABLE_FOLLOW_UPS);
    //db.execSQL("DROP TABLE IF EXISTS " + IMAGE_TALBLE);
    /*db.execSQL("DROP TABLE IF EXISTS " + LOG_TALBLE);
    db.execSQL("DROP TABLE IF EXISTS " + CONFIG_TALBLE);
    db.execSQL("DROP TABLE IF EXISTS " + create_table_files);
    db.execSQL("DROP TABLE IF EXISTS " + create_table_links);
    db.execSQL("DROP TABLE IF EXISTS " + create_table_vcards);
    */
    Cursor res = db.rawQuery("PRAGMA table_info("+TABLE_CONTACT+")",null);

    if (res.getCount()==13) {
  if (DATABASE_VERSION == 2) {
    db.execSQL(DATABASE_ALTER_Contact);
  }
}

    
    onCreate(db);
  }

} 