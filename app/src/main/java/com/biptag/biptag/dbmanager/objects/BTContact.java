package com.biptag.biptag.dbmanager.objects;

import org.json.JSONArray;
import org.json.JSONObject;

public class BTContact {
	private int contact_local_id ; 
	private String contactEmail ; 
	private int contactId ; 
	private int contactFormId ;
	private int contactFrID ; 
	private String contactFrHashcode ; 
	private int  contactFrAgentID ; 
	private JSONObject contactFrResponse ; 
	private int  isSynchronized ; 
	private boolean doFollowup  ;
	private String contactName ;
	private JSONArray ccEmailRegister ; 
	private JSONArray cciEmailRegister ; 
	private  String createTime ;
	
 
 






	public BTContact() {
		// TODO Auto-generated constructor stub
		this.setCcEmailRegister(new JSONArray()) ; 
		this.setCciEmailRegister(new JSONArray()) ; 
	}
	
	
	

	public JSONArray getCcEmailRegister() {
		return ccEmailRegister;
	}









	public void setCcEmailRegister(JSONArray ccEmailRegister) {
		this.ccEmailRegister = ccEmailRegister;
	}









	public JSONArray getCciEmailRegister() {
		return cciEmailRegister;
	}









	public void setCciEmailRegister(JSONArray cciEmailRegister) {
		this.cciEmailRegister = cciEmailRegister;
	}





	public String getContactName() {
		return contactName;
	}









	public int getContactFrAgentID() {
		return contactFrAgentID;
	}









	public void setContactFrAgentID(int contactFrAgentID) {
		this.contactFrAgentID = contactFrAgentID;
	}









	public void setContactName(String contactName) {
		this.contactName = contactName;
	}









	public int isSynchronized() {
		return isSynchronized;
	}




	public void setSynchronized(int isSynchronized) {
		this.isSynchronized = isSynchronized;
	}




	public boolean isDoFollowup() {
		return doFollowup;
	}




	public void setDoFollowup(boolean doFollowup) {
		this.doFollowup = doFollowup;
	}




	public int getContact_local_id() {
		return contact_local_id;
	}




	public void setContact_local_id(int contact_local_id) {
		this.contact_local_id = contact_local_id;
	}




	public String getContactEmail() {
		return contactEmail;
	}

	public void setContactEmail(String contactEmail) {
		this.contactEmail = contactEmail;
	}

	public int getContactId() {
		return contactId;
	}

	public void setContactId(int contactId) {
		this.contactId = contactId;
	}

	public int getContactFormId() {
		return contactFormId;
	}

	public void setContactFormId(int contactFormId) {
		this.contactFormId = contactFormId;
	}

	public int getContactFrID() {
		return contactFrID;
	}

	public void setContactFrID(int contactFrID) {
		this.contactFrID = contactFrID;
	}

	public String getContactFrHashcode() {
		return contactFrHashcode;
	}

	public void setContactFrHashcode(String contactFrHashcode) {
		this.contactFrHashcode = contactFrHashcode;
	}

	

	public JSONObject getContactFrResponse() {
		return contactFrResponse;
	}

	public void setContactFrResponse(JSONObject contactFrResponse) {
		this.contactFrResponse = contactFrResponse;
	}


	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}
}
