package com.biptag.biptag.dbmanager.objects;

import org.json.JSONArray;

public class BTFollowupTemplate {
	
	/* public static final String FOLLOW_UP_COLUMN_FU_ID = "fu_id" ;
  public static final String FOLLOW_UP_COLUMN_BA_ID = "ba_id" ;
  public static final String FOLLOW_UP_COLUMN_FU_CONTENT = "fu_content" ;
  public static final String FOLLOW_UP_COLUMN_FU_SUBJECT = "fu_subject" ;
  public static final String FOLLOW_UP_COLUMN_FU_ATTACH = "fu_attach" ;
  public static final String FOLLOW_UP_COLUMN_FU_NAME = "fu_temp_name" ;
  public static final String FOLLOW_UP_COLUMN_FU_HASHCODE = "fu_hashcode" ;*/

	public int fu_id ;
	public int ba_id ;
	public String fu_content ;
	public String fu_subject ;
	public JSONArray fu_attach ;
	public String temp_name ;
	public String fu_hashcode ;
	public boolean isSynchonized ;
	public long local_id  ;

	public BTFollowupTemplate() {
		// TODO Auto-generated constructor stub
	}

	public int getFu_id() {
		return fu_id;
	}

	public void setFu_id(int fu_id) {
		this.fu_id = fu_id;
	}

	public int getBa_id() {
		return ba_id;
	}

	public void setBa_id(int ba_id) {
		this.ba_id = ba_id;
	}

	public String getFu_content() {
		return fu_content;
	}

	public void setFu_content(String fu_content) {
		this.fu_content = fu_content;
	}

	public String getFu_subject() {
		return fu_subject;
	}

	public void setFu_subject(String fu_subject) {
		this.fu_subject = fu_subject;
	}

	public JSONArray getFu_attach() {
		return fu_attach;
	}

	public void setFu_attach(JSONArray fu_attach) {
		this.fu_attach = fu_attach;
	}

	public String getTemp_name() {
		return temp_name;
	}

	public void setTemp_name(String temp_name) {
		this.temp_name = temp_name;
	}

	public String getFu_hashcode() {
		return fu_hashcode;
	}

	public void setFu_hashcode(String fu_hashcode) {
		this.fu_hashcode = fu_hashcode;
	}

	public boolean isSynchonized() {
		return isSynchonized;
	}

	public void setSynchonized(boolean isSynchonized) {
		this.isSynchonized = isSynchonized;
	}

	public long getLocal_id() {
		return local_id;
	}

	public void setLocal_id(long local_id) {
		this.local_id = local_id;
	}





}
