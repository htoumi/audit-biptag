package com.biptag.biptag.fragements.settings;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Point;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ListView;
import android.widget.TextView;

import com.biptag.biptag.Constant;
import com.biptag.biptag.NavigationActivity;
import com.biptag.biptag.R;
import com.biptag.biptag.dbmanager.datasource.BTAvailableCountryDataSource;
import com.biptag.biptag.dbmanager.objects.BTAvailableCountry;
import com.biptag.biptag.managers.BiptagNavigationManager;
import com.biptag.biptag.managers.BiptagUserManger;

public class SettingCountrySynchronisaiton extends Fragment {
	ListView listView;
	JSONObject countres  ; 
	String[] values  ; 
	CountryAvailabelAdapter adapter ; 
	String filename , tableName ; 
	public SettingCountrySynchronisaiton() {
		// TODO Auto-generated constructor stub
	}
	@SuppressLint({"NewApi", "ValidFragment"})
	public SettingCountrySynchronisaiton(JSONObject countries) {
		// TODO Auto-generated constructor stub
		this.countres = countries  ; 
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		final View fragmentView = inflater.inflate(R.layout.setting_country_synchronisation_fragment,null);
		listView = (ListView) fragmentView.findViewById(R.id.general_change_listview)  ; 
		TextView separator = (TextView) fragmentView.findViewById(R.id.general_change_seprator) ; 
		LayoutParams  layoutParams = (LayoutParams)separator.getLayoutParams() ; 
		   WindowManager wm = (WindowManager) getActivity().getSystemService(Context.WINDOW_SERVICE);
			Display display  =wm.getDefaultDisplay() ;
			Point point  = new Point() ; 
			display.getSize(point) ; 
			int screenWidth = point.x ; 
			layoutParams.setMargins(screenWidth/40,0, screenWidth/40, 0) ; 
			separator.setLayoutParams(layoutParams)  ; 
			
			TextView title = (TextView) fragmentView.findViewById(R.id.general_change_title ) ; 
			//title.setTextSize(screenWidth/20) ; 
			title.setTextSize(NavigationActivity.getContext().getResources().getDimension(R.dimen.acc_info_size));
			LayoutParams params = (LayoutParams) title.getLayoutParams() ; 
			params.setMargins(screenWidth/40,0, screenWidth/40, 0) ;
			title.setLayoutParams(params) ; 
			
			
			try {
				
				JSONArray countries = this.countres.getJSONArray("countries") ; 
				values = new String[countries.length()] ; 
				for (int i = 0; i < countries.length(); i++) { 
					values[i] = countries.getJSONObject(i).getString("AC_NAME") ; 
				}
				
				
			} catch (JSONException e) {
				// TODO: handle exception
			}
			
			BTAvailableCountryDataSource dataSource = new BTAvailableCountryDataSource(getActivity()) ; 
			List<BTAvailableCountry> countries = dataSource.selectAllAvailableCountry() ; 
			final  String[] availableCountries = new String[countries.size()] ; 
			for (int i = 0; i < availableCountries.length; i++) {
				availableCountries[i] = countries.get(i).getName() ; 
			}
			
			adapter = new CountryAvailabelAdapter(getActivity(), android.R.layout.simple_list_item_1, values , availableCountries) ; 
			listView.setAdapter(adapter) ;
			
			
			listView.setOnItemClickListener(new OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> parent, View view,
						int position, long id) {
					// TODO Auto-generated method stub
					ImageView img = (ImageView) view.findViewById(R.id.general_change_icon) ; 
					img.setVisibility(View.VISIBLE) ; 
					TextView txt = (TextView) view.findViewById(R.id.general_change_item) ; 
					boolean found =false  ; 
					tableName= txt.getText().toString() ; 
					
					for (int i = 0; i < availableCountries.length; i++) {
						if(availableCountries[i].equals(txt.getText().toString())){
							found=  true  ; 
							break ; 
						}
					}
					
					if(!found){
						
						try {
							JSONObject jsonCountry  = countres.getJSONArray("countries").getJSONObject(position) ; 
							BTAvailableCountry country = new BTAvailableCountry() ; 
							country.setName(jsonCountry.getString("AC_NAME")) ;
							country.setFile_name(jsonCountry.getString("AC_FILE_NAME") ) ; 
							country.setLast_update(jsonCountry.getString("AC_LAST_UPDATE") ) ; 
							
							BTAvailableCountryDataSource dataSource = new BTAvailableCountryDataSource(getActivity()) ; 
							long insertId  =dataSource.insertCountry(country) ; 
							if(insertId != 0){
								dataSource.createCountryTable(txt.getText().toString()) ; 
							
							
							}
							filename = jsonCountry.getString("AC_FILE_NAME") ; 
							new  CoutryFileDownloader().execute() ; 
							
							
							
						} catch (JSONException e) {
							// TODO: handle exception
							//Log.d("item click ex", "service before exception "+e.getMessage()) ;
						}
						
						
						
						
						
					}
					
					
					
				}
			}) ;
		Button actionBarButton = (Button)	getActivity().getActionBar().getCustomView().findViewById(R.id.formste1_btn_container) ;
		actionBarButton.setBackground(NavigationActivity.getContext().getResources().getDrawable(R.drawable.go_back));

		//actionBarButton.setText(NavigationActivity.getContext().getResources().getString(R.string.return_image_list));

		actionBarButton.setVisibility(View.VISIBLE) ;
		actionBarButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				SettingSynchronisationFragment settingSynchronisationFragment = new SettingSynchronisationFragment();
				BiptagNavigationManager.pushFragments(settingSynchronisationFragment, true, this.getClass().getName(), getActivity(), null, 3);

			}
		}) ;

		return fragmentView;
	}
	
	
	static String response = null;
    public final static int GET = 1;
    public final static int POST = 2;
	  /**
     * Making service call
     * @url - url to make request
     * @method - http request method
     * @params - http request params
     * */
    public String makeServiceCall(String url, int method,
            List<NameValuePair> params) {
        try {
            // http client
            DefaultHttpClient httpClient = new DefaultHttpClient();
            HttpEntity httpEntity = null;
            HttpResponse httpResponse = null;
             
            // Checking http request method type
            if (method ==  POST) {
                HttpPost httpPost = new HttpPost(url);
                // adding post params
                if (params != null) {
                    httpPost.setEntity(new UrlEncodedFormEntity(params));
                }
 
                httpResponse = httpClient.execute(httpPost);
 
            } else if (method == GET) {
                // appending params to url
                if (params != null) {
                    String paramString = URLEncodedUtils
                            .format(params, "utf-8");
                    url += "?" + paramString;
                }
                HttpGet httpGet = new HttpGet(url);
 
                httpResponse = httpClient.execute(httpGet);
 
            }
            httpEntity = httpResponse.getEntity();
            response = EntityUtils.toString(httpEntity);
 
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            //Log.d("service call", "service call "+e.getMessage()) ;
        } catch (ClientProtocolException e) {
        	//Log.d("service call", "service call "+e.getMessage()) ;
            e.printStackTrace();
        } catch (IOException e) {
        	//Log.d("service call", "service call "+e.getMessage()) ;
            e.printStackTrace();
        }
         
        return response;
 
    }

	

	class CoutryFileDownloader extends AsyncTask<String, String, String>{
		
		ProgressDialog dialog ; 
		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			dialog = new ProgressDialog(getActivity()) ; 
			dialog.setMessage(NavigationActivity.getContext().getResources().getString(R.string.country_download_desc)+" "+tableName+" "+NavigationActivity.getContext().getResources().getString(R.string.download_now)) ; 
			dialog.setTitle(NavigationActivity.getContext().getResources().getString(R.string.country_download_title)) ; 
			dialog.setCancelable(false) ; 
			dialog.setIndeterminate(true) ; 
			dialog.show() ; 
		
		 
		}

		@Override
		protected String doInBackground(String... params) {
			// TODO Auto-generated method stub
			
			String url  = Constant.base_url+"countries/"+filename;
			
			String fileContent = makeServiceCall(url, 1, null) ; 
			//Log.d("service call ", "service call "+fileContent) ;
			try {
				JSONArray array = new JSONArray(fileContent) ; 
				BTAvailableCountryDataSource dataSource = new BTAvailableCountryDataSource(getActivity()) ; 
				for (int i = 0; i < array.length(); i++) {
					JSONObject object = array.getJSONObject(i) ; 
					dataSource.insertCityForCountry(tableName, object.getString("VILLE"), object.getString("CODE_POSTALE"), 0)  ; 
					
					//Log.d("insert city service ", "insert city service "+object.getString("VILLE")) ;
					
				}
			} catch (JSONException e) {
				// TODO: handle exception
				//Log.d("insert city service excpetion", "insert city service exception "+e.getMessage()) ;
			}
			 
			return null;
		}
		
		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			dialog.dismiss()  ; 
			BiptagNavigationManager.pushFragments(SettingCountrySynchronisaiton.this, false, SettingCountrySynchronisaiton.class.getName(), NavigationActivity.getContext(), null, 3) ; 
		
		}
		 
	}
	
 

}
