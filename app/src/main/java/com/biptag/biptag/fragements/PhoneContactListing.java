package com.biptag.biptag.fragements;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;

import com.biptag.biptag.R;

public class PhoneContactListing extends Activity {
	ListView listView ; 
	 ArrayList <String> aa= new ArrayList<String>();
	 JSONArray contactList  ; 

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
	
		setContentView(R.layout.activity_phone_contact_listing);

		listView = (ListView) findViewById(R.id.contact_listing_listview)  ; 
		contactList = new JSONArray() ; 
		 Cursor phones = getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,null,null, null);
	        while (phones.moveToNext())
	        {
	          String name=phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
	          String lname=phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.StructuredName.FAMILY_NAME));
	          String fname=phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.StructuredName.GIVEN_NAME));
	          String phoneNumber = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
	          String email= phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Email.ADDRESS));
	          String address= phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.SipAddress.SIP_ADDRESS));
	          String company= phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Organization.COMPANY));
	          String fonction= phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Organization.TITLE));
	          String country= phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.StructuredPostal.COUNTRY));
	          String city= phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.StructuredPostal.CITY));
	          String zipcode= phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.StructuredPostal.POSTCODE));
	          
	          System.out.println(".................."+phoneNumber); 
	          aa.add(!name.equals("") ? name : phoneNumber);
	          JSONObject contact =  new JSONObject() ;
	          try {
	        	  
	        	  contact.put("PHONE", phoneNumber)  ; 
		          contact.put("FNAME"	, fname) ;
		          contact.put("LNAME"	, lname) ;
		          contact.put("EMAIL", email) ; 
		          contact.put("COMPANY", company) ; 
		          contact.put("TITLE", fonction) ; 
		          contact.put("ADDRESS", address) ; 
		          contact.put("CITY", city) ; 
		          contact.put("ZIPCODE", zipcode) ; 
		          contact.put("COUNTRY", country) ; 
		           
		          //Log.d("choisen contact", "choisen contact init "+fname) ;
		     //     Log.d("choisen contact", "choisen contact init "+lname) ;
		      //    Log.d("choisen contact", "choisen contact init "+email) ;
		        //  Log.d("choisen contact", "choisen contact init "+phoneNumber) ;
		        //  Log.d("choisen contact", "choisen contact init "+country) ;
		          
		          contactList.put(contact) ; 
		    } catch (JSONException e) {
				// TODO: handle exception
				//Log.d("phone list ", "phone list jsonexception "+e.getMessage()) ;
			}
	          
	          
	          
	        }
	                 phones.close() ;// close cursor
	          final ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
	                    android.R.layout.simple_list_item_1,aa);
	          listView.setAdapter(adapter);
	                  //display contact numbers in the list
	          listView.setOnItemClickListener(new OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> parent, View view,
						int position, long id) {
					// TODO Auto-generated method stub
					try {
						Intent intent = new Intent() ; 
						JSONObject chosenContact = contactList.getJSONObject(position) ;
						
						//Log.d("choisen contact", "choisen contact "+chosenContact.toString() ) ;
						//intent.putExtra("contact", chosenContact.toString()) ; 
						
						
						//setResult(RESULT_OK, intent) ; 
						//finish() ; 
						
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						//Log.d("phone list ", "phone list finsih  jsonexception "+e.getMessage()) ;
							
					} 			
					
				}
			}) ; 
	          EditText searchBox = (EditText) findViewById(R.id.phone_list_searchbox) ; 
	          searchBox.addTextChangedListener(new TextWatcher() {
				
				@Override
				public void onTextChanged(CharSequence s, int start, int before, int count) {
					// TODO Auto-generated method stub
					
						adapter.getFilter().filter(s) ; 	
				
					
					
				}
				
				@Override
				public void beforeTextChanged(CharSequence s, int start, int count,
						int after) {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void afterTextChanged(Editable s) {
					// TODO Auto-generated method stub
					
				}
			});
	          
	          
	
	}

}
