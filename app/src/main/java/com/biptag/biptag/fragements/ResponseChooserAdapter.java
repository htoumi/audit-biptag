package com.biptag.biptag.fragements;

import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import com.biptag.biptag.R;
import com.biptag.biptag.dbmanager.objects.BTContact;
import com.biptag.biptag.fragements.edit.EditFormStep1;
import com.biptag.biptag.managers.BiptagNavigationManager;

import android.content.Context;
import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.OvalShape;
import android.graphics.drawable.shapes.Shape;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AbsListView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout.LayoutParams;
import android.widget.PopupWindow;
import android.widget.TextView;

public class ResponseChooserAdapter extends ArrayAdapter<String> {
	String[] formNames  ; 
	String[] editions ; 
	int[] colors ;
	String[] timesStamps ; 
	Context context ; 
	int screenWidth ; 
	int screenHeight ; 
	List<BTContact> contacts ; 
	PopupWindow window ; 
	public ResponseChooserAdapter(Context context, int resource,  String[] objects) {
		super(context, resource, objects);
		// TODO Auto-generated constructor stub
	}
	
	
	public ResponseChooserAdapter(Context context, int resource,  String[] formNames  , String[] editions , int[] colors , String[] timesStamps , List<BTContact> contacts , PopupWindow window) {
		super(context, resource, formNames);
		// TODO Auto-generated constructor stub
		this.context = context ; 
		this.colors = colors ; 
		this.formNames  = formNames ; 
		this.editions = editions ;
		this.timesStamps = timesStamps ; 
		WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
		Display display  =wm.getDefaultDisplay() ;
		Point point  = new Point() ; 
		display.getSize(point) ; 
		 screenWidth = point.x ; 
		 screenHeight = point.y ;
		 this.contacts = contacts ; 
		 this.window = window ; 
	}
	
	
	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
	
		if(convertView == null){
			 LayoutInflater inflater = (LayoutInflater) getContext()
	                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	            convertView = inflater.inflate(R.layout.popup_response_chooser_item, null);
	
		}  
		  
		TextView formNameText  = (TextView) convertView.findViewById(R.id.response_form_name) ; 
		TextView formEditionText = (TextView) convertView.findViewById(R.id.response_edition) ; 
		TextView colorPastille = (TextView) convertView.findViewById(R.id.response_chooser_item_color_pastille) ; 
		TextView timeStampText = (TextView) convertView.findViewById(R.id.response_timestamp) ; 
		formNameText.setText(formNames[position]) ; 
		formEditionText.setText(editions[position]) ; 
		timeStampText.setText(timesStamps[position]) ; 
		
		
		GradientDrawable  drawable =(GradientDrawable ) context.getResources().getDrawable(R.drawable.response_color_pastille) ; 
		drawable.setColor(colors[position]) ; 
		colorPastille.setBackground(drawable) ; 
		colorPastille.setWidth(screenHeight/80) ; 
		colorPastille.setHeight(screenHeight/80) ; 
		
		convertView.setLayoutParams(new AbsListView.LayoutParams(AbsListView.LayoutParams.MATCH_PARENT, screenHeight/15)) ; 
	
		convertView.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
			
				BTContact contact = contacts.get(position) ; 
				JSONObject object = new JSONObject() ; 
				try {
					object.put("CONTACT_FORM_ID", contact.getContactFormId()) ; 
					object.put("CONTACT_RESPONSE", contact.getContactFrResponse()) ; 
					object.put("CONTACT_LOCAL_ID", contact.getContact_local_id()) ; 
					object.put("CONTACT_ID", contact.getContactId()) ; 
					object.put("FR_ID", contact.getContactFrID()) ; 
					EditFormStep1 editFormStep1 = new EditFormStep1() ; 
					window.dismiss() ; 
					BiptagNavigationManager.pushFragments(editFormStep1, true, "Edit Contact", context, object.toString(), 2) ;	
				
				}catch(JSONException exception){
					
				}
			}
		}) ; 
		return convertView;
	}

	 
	
}
