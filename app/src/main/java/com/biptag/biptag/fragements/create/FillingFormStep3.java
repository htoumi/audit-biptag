package com.biptag.biptag.fragements.create;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.graphics.Point;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.GradientDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.biptag.biptag.Constant;
import com.biptag.biptag.ContactAdapter;
import com.biptag.biptag.ContactItem;
import com.biptag.biptag.JSONParser;
import com.biptag.biptag.ListingFilesActivity;
import com.biptag.biptag.NavigationActivity;
import com.biptag.biptag.R;
import com.biptag.biptag.dbmanager.datasource.BTContactDataSource;
import com.biptag.biptag.dbmanager.datasource.BTFollowupTemplateDataSource;
import com.biptag.biptag.dbmanager.datasource.BTFormDataSource;
import com.biptag.biptag.dbmanager.datasource.BTImageDataSource;
import com.biptag.biptag.dbmanager.objects.BTContact;
import com.biptag.biptag.dbmanager.objects.BTFollowupTemplate;
import com.biptag.biptag.dbmanager.objects.BTFormulaire;
import com.biptag.biptag.dbmanager.objects.BTImage;
import com.biptag.biptag.dbmanager.objects.BTLog;
import com.biptag.biptag.fragements.ConnectedhomeFragment;
import com.biptag.biptag.fragements.ContactFragement;
import com.biptag.biptag.fragements.EditFollowUpFragement;
import com.biptag.biptag.fragements.edit.EditFormStep1;
import com.biptag.biptag.fragements.edit.EditFormStep2;
import com.biptag.biptag.managers.BiptagNavigationManager;
import com.biptag.biptag.managers.BiptagUserManger;
import com.biptag.biptag.ocr.CaptureActivity;

public class FillingFormStep3 extends Fragment {
	Button addfilesButton ; 
	boolean isFinishClicked = false ; 
	public static Button finish  ; 
	Button backButton ; 
	Button openFollowUpActivity ; 
	JSONObject jsonObject ; 
	int RESULT_CODE_SELECT_FILES= 200 ; 
	String files = new JSONArray().toString() ; 
	String links = new JSONArray().toString() ; 
	String vcards = new JSONArray().toString(); 
	LinearLayout filecontainer ; 
	int screenWidth ; 
	int screenHeight ;
	ListingFilesActivity filesActivity ; 
	PopupWindow deletePopUp ; 
	View fragmentView; 
	public final static String createImageArrayUserPreferenceName = "images_for_user" ; 
	private static String add_files_result_create = "add_files_result" ; 
	 
	
	// LZR : 14/04/2016 funtion to move to the previous step
	public void previousAction(){
		BiptagNavigationManager.pushFragments(FillingFormStep3.this, false, this.getClass().getName(), NavigationActivity.getContext(), null, 1) ; 

	}
	
	 public void  initFilDarianeNavigation() {
			Button btn1 = (Button) fragmentView.findViewById(R.id.fildaraiane_step3_button1) ; 
			Button btn2 = (Button) fragmentView.findViewById(R.id.fildaraiane_step3_button2) ; 
			Button btn3 = (Button) fragmentView.findViewById(R.id.fildaraiane_step3_button3) ; 
			 btn1.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					previousAction() ; 
				}
			}) ; 
			 btn2.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					previousAction() ; 
				}
			}) ; 
			 
			 btn3.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
				}
			}) ; 
			
			
		}
	
	public void fixLayout(){
		
		LinearLayout followup_widget_container =(LinearLayout) fragmentView.findViewById(R.id.followup_widget_container) ; 

		
		 	GradientDrawable border = new GradientDrawable();
		    border.setColor(0xFFFAFAFA); //white background
		    border.setStroke(3, 0xFFE6E6EA); //black border with full opacity
		    if(Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
		    	followup_widget_container.setBackgroundDrawable(border);
		    } else { 
		    	followup_widget_container.setBackground(border);
		    }
		    
		    WindowManager wm = (WindowManager) getActivity().getSystemService(Context.WINDOW_SERVICE);
			Display display  =wm.getDefaultDisplay() ;
			Point point  = new Point() ; 
			display.getSize(point) ; 
			 screenWidth = point.x ; 
			screenHeight = point.y ;
		    
			LinearLayout.LayoutParams params = (android.widget.LinearLayout.LayoutParams) followup_widget_container.getLayoutParams() ; 
		    params.setMargins(screenWidth/30, screenHeight/30, screenWidth/30, screenHeight/100) ;
		    followup_widget_container.setLayoutParams(params) ;
		    
		     
		    /*
		    LinearLayout fil_dariane = (LinearLayout) fragmentView.findViewById(R.id.fil_dariane) ; 
		    android.widget.LinearLayout.LayoutParams arianeLayoutParams = new android.widget.LinearLayout.LayoutParams(android.widget.LinearLayout.LayoutParams.MATCH_PARENT, getResources().getDimensionPixelSize(R.dimen.next_prev_height_button)) ; 
		    fil_dariane.setLayoutParams(arianeLayoutParams) ;
		    *//*
		    LinearLayout step3_btn_container = (LinearLayout) fragmentView.findViewById(R.id.step3_btn_container) ; 
		    LinearLayout.LayoutParams btn_layaout_params = (LinearLayout.LayoutParams) step3_btn_container.getLayoutParams() ; 
		  //  btn_layaout_params.setMargins(0, screenHeight/5, 0, 0) ;  
		    step3_btn_container.setLayoutParams(btn_layaout_params) ; 
		     */
	}
	
	@Override
	public void onStop() {
		// TODO Auto-generated method stub
		if(deletePopUp!= null){
			
			deletePopUp.dismiss();
			//deletePopUp = null;
		}
		super.onStop();
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		System.gc();
		 fragmentView = inflater.inflate(R.layout.fragement_filling_form_step3	,null);
		addfilesButton = (Button) fragmentView.findViewById(R.id.filling_form_step3_add_files) ; 
		filecontainer = (LinearLayout) fragmentView.findViewById(R.id.step3_file_container) ; 
		BiptagUserManger.clearField(NavigationActivity.getContext(),"cardscanned");
		  LinearLayout step3_file_container_super = (LinearLayout) fragmentView.findViewById(R.id.step3_file_container_super) ; 
		    FrameLayout.LayoutParams paramsSuper = (FrameLayout.LayoutParams) step3_file_container_super.getLayoutParams() ; 
		    paramsSuper.setMargins(screenWidth/60, screenHeight/60, screenWidth/60, screenHeight/60) ;  
		    step3_file_container_super.setLayoutParams(paramsSuper) ;
		
			fixLayout() ; 
		   ScrollView step3_files_container = (ScrollView) fragmentView.findViewById(R.id.step3_files_container) ; 
		   
		   GradientDrawable border = new GradientDrawable();
		    border.setColor(0xFFFAFAFA); //white background
		    border.setStroke(3, 0xFFE6E6EA); //black border with full opacity
		    if(Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
		    	step3_files_container.setBackgroundDrawable(border);
		    } else { 
		    	step3_files_container.setBackground(border);
		    }
		    
		    WindowManager wm = (WindowManager) getActivity().getSystemService(Context.WINDOW_SERVICE);
			Display display  =wm.getDefaultDisplay() ;
			Point point  = new Point() ; 
			display.getSize(point) ; 
			int screenWidth = point.x ; 
			int screenHeight = point.y ;
		    LinearLayout.LayoutParams params = (android.widget.LinearLayout.LayoutParams) step3_files_container.getLayoutParams() ; 
		    params.setMargins(screenWidth/30, screenHeight/30, screenWidth/30, screenHeight/30) ; 
		    step3_files_container.setLayoutParams(params) ;
		    
		    backButton = (Button) fragmentView.findViewById(R.id.step3_previousbutton) ; 
		   
		    backButton.setOnClickListener(new View.OnClickListener() {
				 
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					BiptagNavigationManager.pushFragments(FillingFormStep3.this, false, this.getClass().getName(), NavigationActivity.getContext(), null, 1) ; 

				}
			})  ; 
		     
		    
		CheckBox checkBox = (CheckBox) fragmentView.findViewById(R.id.checkBox1) ; 
		checkBox.setButtonDrawable( NavigationActivity.getContext().getResources().getDrawable(R.drawable.checkbox_states)) ; 
		checkBox.setPadding(checkBox.getPaddingLeft() + (int)(10.0f * 2 + 0.5f), 0, 0, 0);

		
		
		openFollowUpActivity = (Button) fragmentView.findViewById(R.id.step3_open_edit_follow_up) ; 
		openFollowUpActivity.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
			//Intent intent = new Intent(getActivity(), EditFollowUpMailActivity.class) ; 
			//startActivity(intent) ;
//LZR 26 04 2016 : start the activity of edit follow up fragment
			EditFollowUpFragement fragement = new EditFollowUpFragement(1) ; 
			
			
			JSONObject data = new JSONObject() ; 
			try {
				data.put("mode", 0) ; 
				data.put("stack", 1) ; 
				data.put("email", "") ; 
				data.put("id", 0) ; 
			} catch (JSONException e) {
				// TODO: handle exception
			}
			BiptagNavigationManager.pushFragments(fragement, true, "edit_follow_up", NavigationActivity.getContext(), data.toString(), 1) ; 
			
			 
			
			} 
		});
		
		LayoutParams fileIconLayoutParams = new LayoutParams(screenHeight/8, screenHeight/8) ; 
		fileIconLayoutParams.setMargins(screenWidth/60, screenHeight/60, screenWidth/60, screenHeight/60) ; 
		addfilesButton.setLayoutParams(fileIconLayoutParams); 
		
		addfilesButton.setWidth(screenHeight/8) ; 
		addfilesButton.setHeight(screenHeight/8) ; 
		
		LayoutParams buttonLayoutParams  = (LayoutParams) addfilesButton.getLayoutParams() ; 
		buttonLayoutParams.setMargins(screenWidth/60, screenHeight/60, screenWidth/60, screenHeight/60) ;
		
		
// LZR 26 04 2016 ajout des fichiers
		addfilesButton.setOnClickListener(new View.OnClickListener() {
			 
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
			/*Intent intent= new Intent(getActivity(), ListingFilesActivity.class) ; 
			startActivityForResult(intent,RESULT_CODE_SELECT_FILES ) ; 
			*/

// LZR 26 04 2016 start activity permettant d'afficher la liste des fichiers
				filesActivity = null ;
				filesActivity = new ListingFilesActivity(1) ; 
				BiptagNavigationManager.pushFragments(filesActivity, true, "add_file_fragment", NavigationActivity.getContext(), null, 1) ; 
			
			
			}
		});
// LZR 26 04 2016 enregistrement du contact apres l'appuis sur le bouton "Finish"
		finish = (Button) fragmentView.findViewById(R.id.step3_nextbutton) ; 
		finish.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(!isFinishClicked){
// insertion du contact et liberation des variables utilisées pour ce contact
					new InsertContact().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, null) ; 
					isFinishClicked = true 		 ;
					BiptagUserManger.clearField(NavigationActivity.getContext(),"cardscanned");
					BiptagUserManger.clearField(NavigationActivity.getContext(),"ocrScanReturn");
					BiptagUserManger.clearField(NavigationActivity.getContext(),"validate");
					BiptagUserManger.clearField(NavigationActivity.getContext(),"EditscanOCR_Creat");
					FillingFormStep1.fragmentView=null;
				}else{
					Toast.makeText(NavigationActivity.getContext(), "Processing", Toast.LENGTH_LONG).show() ; 
				}
		
			}
			 
		}); 
		
		
		
		
		initFilDarianeNavigation() ; 
		
		
		
		return fragmentView ; 
	}

	
	// LZR : 14/04/2016 arranger la liste des fichiers ( docs carte de visite lien ... ) apres ajout ou suppression des fichiers
	public void rearrangeFiles(){

		//filling_form_step3_add_files
		//step3_file_container_first_row
		do {
			int rowCount = filecontainer.getChildCount();
			Log.d(" rowcount ", rowCount + " row count " + filecontainer.getScrollBarSize());
			for (int i = 0; i < rowCount; i++) {
				View row = filecontainer.getChildAt(i);
				if (i == 0) {
					LinearLayout firstRow = (LinearLayout) filecontainer.getChildAt(0);
					int childCount = firstRow.getChildCount();
					Log.d("first row 1 ", "first row ");
					for (int j = 0; j < childCount; j++) {
						Log.d("first row 1 ", "first row  child count " + firstRow.getChildCount());
						View addButton = firstRow.getChildAt(j);
						if (addButton != null)
							if (j != 0) {
								Log.d("first row 1 ", "first row not the button");

								try {
									firstRow.removeViewAt(j);
									//	filecontainer.removeViewAt(j) ;
								} catch (Exception e) {
									// TODO: handle exception
									Log.d("add file", "add file remove file icon exc " + e.getMessage());
								}
							}

					}
				} else {
					try {
						filecontainer.removeViewAt(i);

					} catch (Exception e) {
						// TODO: handle exception
						Log.d("add file", "add file remove file icon exc 1 " + e.getMessage());

					}
				}
			}
		}while (filecontainer.getChildCount()!=1);
/*
		int rowCount1 = filecontainer.getChildCount() ;
		Log.d(" rowcount 1 ", rowCount +" row count " + filecontainer.getScrollBarSize());
		for (int i = 0; i < rowCount1; i++) {
			View row = filecontainer.getChildAt(i) ;
			if(i == 0)
			{
				LinearLayout firstRow = (LinearLayout ) filecontainer.getChildAt(0) ;
				int childCount = firstRow.getChildCount();
				Log.d("first row 2 ", "first row " + childCount) ;
				for (int j = 0; j < childCount ; j++) {
					Log.d("first row 2 ", "first row  child count "+firstRow.getChildCount() + " i "+i +" j " + j) ;
					View addButton = firstRow.getChildAt(j) ;
					if(addButton != null)
						if(j  == 1){

							try {
									firstRow.removeViewAt(j) ;
							//	filecontainer.removeViewAt(j) ;
								Log.d("first row 2 ", "first row not the button " + j) ;


							} catch (Exception e) {
								// TODO: handle exception
							}
						}

				}
			}else{

				try {
					filecontainer.removeViewAt(i) ;

				} catch (Exception e) {
					// TODO: handle exception
				}
			}
		}


*/


	//	Log.d("activity result ", " activity result file selection  "+files) ;
	//	Log.d("activity result ", " activity result file selection  "+links) ;
	//	Log.d("activity result ", " activity result file selection  "+vcards) ;

		addFileIcons();





	}
	// LZR : 14/04/2016 delete the file clicked
	public void deleteAttachedFile(String type , String id ){

		// TODO Auto-generated method stub
		//Log.d("delte item ", "delete item "+files) ; 
		JSONArray filesSelectedCopy  = new JSONArray() ; 
		JSONArray linksSelectedCopy  = new JSONArray() ; 
		JSONArray cardsSelectedCopy  = new JSONArray() ;
		
		
		
		if(type.equals("FILE")){
			
			try {
				JSONArray filesSelected = new JSONArray(files) ; 
				for (int j = 0; j < filesSelected.length()	; j++) {
					if(!filesSelected.getJSONArray(j).getString(2).equals(id)){
						filesSelectedCopy.put(filesSelected.getJSONArray(j)) ; 
					}else{
						//Log.d("delte item", "delete item found") ; 
					}
				}
				
				files = filesSelectedCopy.toString() ; 
				//Log.d("delte item ", "delete item after "+files) ;
			} catch (JSONException e) {
				// TODO: handle exception
				//Log.d("delete attached file exception", "delete attached file file exception "+e.getMessage()) ; 
				//Log.d("delte item exception", "delete item exception "+e.getMessage()) ; 
			}
			
			
			
		}else{
			try {
				filesSelectedCopy  = new JSONArray(files) ; 
			} catch (JSONException e) {
				// TODO: handle exception
			}
		}
		
		
			if(type.equals("LINK")){
			
			try {
				JSONArray linskSelected = new JSONArray(links) ;
				for (int j = 0; j < linskSelected.length()	; j++) {
					//Log.d("delete linkg", "delete file item "+linskSelected.getJSONArray(j)) ; 
					if(!linskSelected.getJSONArray(j).getString(3).equals(id)){
						linksSelectedCopy.put(linskSelected.getJSONArray(j)) ; 
					}
				}
				links = linksSelectedCopy.toString() ; 
				
			} catch (JSONException e) {
				// TODO: handle exception
				//Log.d("delete attached file exception", "delete attached file link exception "+e.getMessage()) ; 

			}
			
			
			
		}else{
			try {
				linksSelectedCopy = new JSONArray(links) ; 
			} catch (JSONException e) {
				// TODO: handle exception
			}
		}
			
			if(type.equals("VCARD")){
				
				try {
					JSONArray cardsSelected = new JSONArray(vcards) ; 
					for (int j = 0; j < cardsSelected.length()	; j++) {
						if(!cardsSelected.getJSONArray(j).getString(2).equals(id)){
							cardsSelectedCopy.put(cardsSelected.getJSONArray(j)) ; 
						}
					}
					vcards = cardsSelectedCopy.toString() ; 
					
				} catch (JSONException e) {
					// TODO: handle exception
					//Log.d("delete attached file exception", "delete attached file vcard exception "+e.getMessage()) ; 

				}
				
				
				 
			}else{
				try {
					cardsSelectedCopy = new JSONArray(vcards) ; 
				} catch (JSONException e) {
					// TODO: handle exception
				}
			}
		
			JSONObject object = new JSONObject() ; 

			try {
				object.put("FILES", filesSelectedCopy) ; 
				object.put("LINKS", linksSelectedCopy) ; 
				object.put("VCARDS", cardsSelectedCopy) ; 
			} catch (JSONException e) {
				// TODO: handle exception
				//Log.d("delete attached file exception", "delete attached file exception "+e.getMessage()) ; 
				
			}
			
			
			Log.d("delete attahced file", "delete attached file after delete "+object.toString()) ;
			BiptagUserManger.saveUserData(add_files_result_create, object.toString(),  NavigationActivity.getContext()) ; 
			
			deletePopUp.dismiss() ; 
				
			//((LinearLayout)fileicon.getParent()).removeView(fileicon) ; 
// LZR : 26 04 2016 arranger les fichiers restants apres avoir supprimer quelques uns
				rearrangeFiles() ;
		String add_files_resultString = String.valueOf(BiptagUserManger.getUserValue(add_files_result_create, "string",  NavigationActivity.getContext())) ;

		Log.d("file selected", "file selected "+add_files_resultString) ;
		/*if(add_files_resultString.length()!= 0){
			try {
				JSONObject add_files_result = new JSONObject(add_files_resultString) ;

				files = add_files_result.getJSONArray("FILES").toString() ;
				links= add_files_result.getJSONArray("LINKS").toString() ;
				vcards = add_files_result.getJSONArray("VCARDS").toString() ;
				//Log.d("activity result ", " activity result file selection  "+files) ;
				//Log.d("activity result ", " activity result file selection  "+links) ;
				//Log.d("activity result ", " activity result file selection  "+vcards) ;

				rearrangeFiles() ;


			} catch (JSONException e) {
				// TODO: handle exception

				//Log.d("onr esume file exception", "onr esume file exception "+e.getMessage()) ;
			}
*/

//		}




	}
	// LZR : 14/04/2016 add new files
	public void addFileIcons(){
		LayoutInflater inflater = LayoutInflater.from( NavigationActivity.getContext());
		try {
			  JSONArray filesSelected = new JSONArray(files) ; 
			
			
			for (int i = 0; i < filesSelected.length(); i++) {
				JSONArray item = filesSelected.getJSONArray(i) ;  
				Log.d("item selected", "item selected "+item) ;
				Log.d("item selected", "item selected "+item.getString(0)) ;
				final  String type = "FILE" ; 
				final String id = item.getString(2) ; 
				Log.d("item selected", "item selected size "+item.getString(0).split("\\.").length) ;
				String filename = item.getString(0) ;
				String nameOnly = filename.split("\\.")[0] ; 
				String extension = filename.split("\\.")[filename.split("\\.").length-1] ; 
				final LinearLayout fileicon = (LinearLayout) inflater.inflate(R.layout.fileicon, null, false);
					((TextView)fileicon.findViewById(R.id.file_icon_file_name)).setText(nameOnly + "." + extension) ; 
					//((TextView)fileicon.findViewById(R.id.file_icon_file_type)).setText(extension) ; 
					((TextView)fileicon.findViewById(R.id.file_icon_type)).setText(type) ; 
					((TextView)fileicon.findViewById(R.id.file_icon_id)).setText(String.valueOf(id)) ; 
// LZR 26 04 2016 popup suppression d'un fichier lors du click sur le fichier
					fileicon.setOnClickListener(new View.OnClickListener() {
						
						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
							Log.d("file click", "file click") ;
							if(deletePopUp!= null){
								deletePopUp.dismiss() ; 
							}
							LayoutInflater inflater= LayoutInflater.from( NavigationActivity.getContext()) ; 
							View deleteLayout= inflater.inflate(R.layout.pop_delete_file, null) ;
							WindowManager wm = (WindowManager) getActivity().getSystemService(Context.WINDOW_SERVICE);
							Display display  =wm.getDefaultDisplay() ;
							Point point  = new Point() ; 
							display.getSize(point) ; 
							int screenWidth = point.x ; 
							int screenHeight = point.y ; 
							deletePopUp = new PopupWindow(deleteLayout , screenWidth/6, screenHeight/12) ;
							deletePopUp.showAsDropDown(v); 
							deletePopUp.setBackgroundDrawable(new BitmapDrawable());
// LZR 26 04 2016 supression d'un fichier lors du click sur le bouton "Delete"
							
							Button delteItemBTN = (Button) deleteLayout.findViewById(R.id.popup_delete_button) ;
							delteItemBTN.setOnClickListener(new View.OnClickListener() {
								
								@Override
								public void onClick(View v) {
// LZR 26 04 2016  fonction permettant la suppression d'un fichier avec le type du fichier et son emplacement
									deleteAttachedFile(type , id) ; 
									
								}

								
							}) ; 
							
							
							
							
						}
					}) ; 
					
					
					LayoutParams fileIconLayoutParams = new LayoutParams(screenHeight/8, screenHeight/8) ; 
					fileIconLayoutParams.setMargins(screenWidth/60, screenHeight/60, screenWidth/60, screenHeight/60) ; 
					fileicon.setLayoutParams(fileIconLayoutParams); 
					int rowNumber = filecontainer.getChildCount();
					LinearLayout lastRow = (LinearLayout)filecontainer.getChildAt(rowNumber-1) ; 
				//	fileicon.setLayoutParams(new TableLayout.LayoutParams(	LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT, 1f)) ; 
					if(lastRow.getChildCount()==4){
						LinearLayout row = new LinearLayout( NavigationActivity.getContext()) ;
						filecontainer.addView(row) ;   
					}
					int newrowNumber = filecontainer.getChildCount();
					LinearLayout newlastRow = (LinearLayout)filecontainer.getChildAt(newrowNumber-1) ; 
					newlastRow.addView(fileicon) ; 
			}
			
			
		} catch (JSONException e) {
			// TODO: handle exception
			Log.d("activity result ", " activity result file exception  "+e.getMessage()) ;
			
		}
// LZR 26 04 2016 le même traitement pour les cartes visites
		try {
			JSONArray cardsSelected = new JSONArray(vcards) ; 
			for (int i = 0; i < cardsSelected.length(); i++) {
				JSONArray item = cardsSelected.getJSONArray(i) ; 
				final  String type = "VCARD" ; 
				final  String id = item.getString(2) ;
				final LinearLayout fileicon = (LinearLayout) inflater.inflate(R.layout.fileicon, null, false);
					
					((TextView)fileicon.findViewById(R.id.file_icon_file_name)).setText("CARD : " + item.getString(0)) ; 
				//	((TextView)fileicon.findViewById(R.id.file_icon_file_type)).setText("CARD") ; 
					((TextView)fileicon.findViewById(R.id.file_icon_type)).setText(type) ; 
					((TextView)fileicon.findViewById(R.id.file_icon_id)).setText(id) ; 
fileicon.setOnClickListener(new View.OnClickListener() {
						
						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
							//Log.d("file click", "file click") ; 
							if(deletePopUp!= null){
								deletePopUp.dismiss() ; 
							}
							LayoutInflater inflater= LayoutInflater.from( NavigationActivity.getContext()) ; 
							View deleteLayout= inflater.inflate(R.layout.pop_delete_file, null) ;
							WindowManager wm = (WindowManager) getActivity().getSystemService(Context.WINDOW_SERVICE);
							Display display  =wm.getDefaultDisplay() ;
							Point point  = new Point() ; 
							display.getSize(point) ; 
							int screenWidth = point.x ; 
							int screenHeight = point.y ;
							deletePopUp = new PopupWindow(deleteLayout , screenWidth/6, screenHeight/12) ;
							deletePopUp.showAsDropDown(v); 
							deletePopUp.setBackgroundDrawable(new BitmapDrawable());
							
							
							Button delteItemBTN = (Button) deleteLayout.findViewById(R.id.popup_delete_button) ;
							delteItemBTN.setOnClickListener(new View.OnClickListener() {
								
								@Override
								public void onClick(View v) {
									
									deleteAttachedFile(type, id) ; 
									
								}
							}) ; 
							
							
							
							
						}
					}) ; 
					LayoutParams fileIconLayoutParams = new LayoutParams(screenHeight/8, screenHeight/8) ; 
					fileIconLayoutParams.setMargins(screenWidth/60, screenHeight/60, screenWidth/60, screenHeight/60) ; 
					fileicon.setLayoutParams(fileIconLayoutParams); 
					int rowNumber = filecontainer.getChildCount();
					LinearLayout lastRow = (LinearLayout)filecontainer.getChildAt(rowNumber-1) ; 
				//	fileicon.setLayoutParams(new TableLayout.LayoutParams(	LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT, 1f)) ; 
					if(lastRow.getChildCount()==4){
						LinearLayout row = new LinearLayout( NavigationActivity.getContext()) ;
						filecontainer.addView(row) ;   
					}
					int newrowNumber = filecontainer.getChildCount();
					LinearLayout newlastRow = (LinearLayout)filecontainer.getChildAt(newrowNumber-1) ; 
					newlastRow.addView(fileicon) ; 
			}
			
			
		} catch (JSONException e) {
			// TODO: handle exception
			//Log.d("activity result ", " activity result file exception  "+e.getMessage()) ; 
			
		}
// LZR 26 04 2016 : le même traitement que les fichiers et les cartes visites
		try {
			JSONArray linskSelected = new JSONArray(links) ; 
			for (int i = 0; i < linskSelected.length(); i++) {
				JSONArray item = linskSelected.getJSONArray(i) ; 
				final  String type = "LINK" ; 
				final  String id = item.getString(3) ;
				final LinearLayout fileicon = (LinearLayout) inflater.inflate(R.layout.fileicon, null, false);
					((TextView)fileicon.findViewById(R.id.file_icon_file_name)).setText("Http : " +item.getString(0)) ; 
					//((TextView)fileicon.findViewById(R.id.file_icon_file_type)).setText("Http") ; 
					((TextView)fileicon.findViewById(R.id.file_icon_type)).setText(type) ;
					((TextView)fileicon.findViewById(R.id.file_icon_id)).setText(id) ; 
					fileicon.setOnClickListener(new View.OnClickListener() {
						
						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
							//Log.d("file click", "file click") ; 
							if(deletePopUp!= null){
								deletePopUp.dismiss() ; 
							}
							LayoutInflater inflater= LayoutInflater.from( NavigationActivity.getContext()) ; 
							View deleteLayout= inflater.inflate(R.layout.pop_delete_file, null) ;
							WindowManager wm = (WindowManager) getActivity().getSystemService(Context.WINDOW_SERVICE);
							Display display  =wm.getDefaultDisplay() ;
							Point point  = new Point() ; 
							display.getSize(point) ; 
							int screenWidth = point.x ; 
							int screenHeight = point.y ;
							deletePopUp = new PopupWindow(deleteLayout , screenWidth/6, screenHeight/12) ;
							deletePopUp.showAsDropDown(v); 
							deletePopUp.setBackgroundDrawable(new BitmapDrawable());
							
							
							Button delteItemBTN = (Button) deleteLayout.findViewById(R.id.popup_delete_button) ;
							delteItemBTN.setOnClickListener(new View.OnClickListener() {
								
								@Override
								public void onClick(View v) {
									
									deleteAttachedFile(type, id) ; 
								}
							}) ; 
							
							
							
							
						}
					}) ; 
					
					LayoutParams fileIconLayoutParams = new LayoutParams(screenHeight/8, screenHeight/8) ; 
					fileIconLayoutParams.setMargins(screenWidth/60, screenHeight/60, screenWidth/60, screenHeight/60) ; 
					fileicon.setLayoutParams(fileIconLayoutParams); 
					int rowNumber = filecontainer.getChildCount();
					LinearLayout lastRow = (LinearLayout)filecontainer.getChildAt(rowNumber-1) ; 
				//	fileicon.setLayoutParams(new TableLayout.LayoutParams(	LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT, 1f)) ; 
					if(lastRow.getChildCount()==4){
						LinearLayout row = new LinearLayout( NavigationActivity.getContext()) ;
						filecontainer.addView(row) ;   
					}
					int newrowNumber = filecontainer.getChildCount();
					LinearLayout newlastRow = (LinearLayout)filecontainer.getChildAt(newrowNumber-1) ; 
					newlastRow.addView(fileicon) ; 
			}
			 
			 
		} catch (JSONException e) {
			// TODO: handle exception
			//Log.d("activity result ", " activity result file exception  "+e.getMessage()) ; 
			
		} 
		 
		
		
			
	}
	
	
	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
// LZR 26 04 2016 avoir le même etat en revenant a cet ecran avant de le quitter
		isFinishClicked = false ;
// LZR 26 04 2016 get files added for this contact
		String add_files_resultString = String.valueOf(BiptagUserManger.getUserValue(add_files_result_create, "string",  NavigationActivity.getContext())) ; 
// LZR 26 04 2016 arranger les fichiers selon le type
		//Log.d("file selected", "file selected "+add_files_resultString) ; 
		if(add_files_resultString.length()!= 0){
		try {
			JSONObject add_files_result = new JSONObject(add_files_resultString) ; 
			
			files = add_files_result.getJSONArray("FILES").toString() ;   
			links= add_files_result.getJSONArray("LINKS").toString() ; 
			vcards = add_files_result.getJSONArray("VCARDS").toString() ; 
			//Log.d("activity result ", " activity result file selection  "+files) ; 
			//Log.d("activity result ", " activity result file selection  "+links) ; 
			//Log.d("activity result ", " activity result file selection  "+vcards) ; 
		
			rearrangeFiles() ; 
		
			
		} catch (JSONException e) {
			// TODO: handle exception
			
			//Log.d("onr esume file exception", "onr esume file exception "+e.getMessage()) ; 
		}
		
		
		}



		BiptagUserManger.clearField(NavigationActivity.getContext() , CaptureActivity.vcardIdKeyCreate);
		BiptagUserManger.clearField(NavigationActivity.getContext() , CaptureActivity.vcardIdKeyEdit);

	}
	
	
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		
		if(requestCode== RESULT_CODE_SELECT_FILES){}
	
	
	}
	
	
	
// LZR 26 04 2016  class permettant l'insertion du contact
	class InsertContact extends AsyncTask<String, String, String>{
		long contact_local_id; 
		@Override
		protected String doInBackground(String... params) {
			// TODO Auto-generated method stub
			try {
// get the data of step 1 step 2 and step 3 of this contact
				final int agent_id = Integer.parseInt(String.valueOf(BiptagUserManger.getUserValue("AGENT_ID", "int",NavigationActivity.getContext()))) ;
				int ba_id = Integer.parseInt(String.valueOf(BiptagUserManger.getUserValue("BA_ID", "int",  NavigationActivity.getContext()))) ; 

				String data_form_1 = String.valueOf(BiptagUserManger.getUserValue("form_data_step1", "string",  NavigationActivity.getContext()))	;
				String data_form_2 = String.valueOf(BiptagUserManger.getUserValue("form_data_step2", "string",  NavigationActivity.getContext()))	;
				String images = String.valueOf(BiptagUserManger.getUserValue("images", "string",  NavigationActivity.getContext())); 
				JSONArray images_json_array = new JSONArray() ; 
				//Log.d("finish contact ", "finish contact image  "+images) ; 
				if(!images.equals("")) {
					images_json_array = new JSONArray(images) ; 
					
				}
				ArrayList<String> images_list_array = new ArrayList<String>();     
				
				if (images_json_array != null) { 
				   int len = images_json_array.length();
				   for (int i=0;i<len;i++){ 
					   images_list_array.add(images_json_array.get(i).toString());
				   } 
				} 
				
				
				String imageArrayString = String.valueOf(BiptagUserManger.getUserValue(createImageArrayUserPreferenceName, "string",  NavigationActivity.getContext())) ; 
				JSONArray imageArray  ; 
				try {
					//Log.d("sendin image", "sendin image string "+imageArrayString) ; 
					 JSONArray imageArrayIds = new JSONArray(imageArrayString)  ;
					 imageArray = new JSONArray() ; 
					 for (int i = 0; i < imageArrayIds.length(); i++) {
						int imageId = imageArrayIds.getInt(i) ; 
						BTImageDataSource imageDataSource = new BTImageDataSource( NavigationActivity.getContext()) ; 
						BTImage image  = imageDataSource.selectImagesWithId(imageId, ba_id) ; 
						if(image != null){
							String imageName = image.getImageName() ; 
							imageArray.put(imageName) ; 
							BTImage uImage = image ; 
							uImage.setIsSent(0) ; 
							 imageDataSource.updateImage(uImage) ;





						}else{
							//Log.d("sendin image ", "sendin image null") ; 
						}
					}
					 
					 
				} catch (JSONException e) {
					// TODO: handle exception
					//Log.d("sendin image ", "sendin image exception "+e.getMessage()) ; 
					imageArray =  new JSONArray() ; 
				}
				
				//Log.d("sendin image ", "sendin image array  "+imageArray.toString()) ; 

				final String email = String.valueOf(BiptagUserManger.getUserValue("contact_email", "string",  NavigationActivity.getContext())) ;
				int user_id= Integer.parseInt(String.valueOf(BiptagUserManger.getUserValue("USER_ID", "int",  NavigationActivity.getContext()))) ; 
				 
				String product_config = String.valueOf(BiptagUserManger.getUserValue("PRODUCT_CONFIG", "string",  NavigationActivity.getContext())) ; 
				JSONObject product_config_object = new JSONObject(product_config) ; 
				//Log.d("fisih contact ", "finish contact product config "+product_config_object.toString()) ; 
				String follow_up = product_config_object.getString("follow_up_mail") ; 
				String contact_actif =product_config_object.getString("contact_actif") ; 
				final String form_id_string = getArguments().getString("data") ; 
				
				//Log.d("step3 finish", "step3 finish data1 "+data_form_1) ; 
				//Log.d("step3 finish", "step3 finish data2 "+data_form_2) ; 
				//Log.d("step3 finish", "step3 finish email  "+email) ;
// LZR 26 04 2016 creat jsonobject with data of contact
				JSONObject contact_response = new JSONObject(data_form_2) ;
//LZR 26 04 2016 add files links and vcards to this object
				contact_response.put("FILES", files) ; 
				contact_response.put("LINKS", links) ;
				contact_response.put("VCARDS", vcards) ; 
				List<NameValuePair> list = new ArrayList<NameValuePair>() ; 
				list.add(new BasicNameValuePair("ba_id", String.valueOf(ba_id))) ;
				list.add(new BasicNameValuePair("email", email)) ; 
				
				list.add(new BasicNameValuePair("agent_id", String.valueOf(agent_id))) ; 
				list.add(new BasicNameValuePair("form_id", form_id_string)) ; 
				list.add(new BasicNameValuePair("user_id", String.valueOf(user_id)));
				list.add(new BasicNameValuePair("contact_actif", contact_actif)) ; 
				list.add(new BasicNameValuePair("follow_up", follow_up)) ;
				list.add(new BasicNameValuePair("do_follow_up", ((CheckBox) getView().findViewById(R.id.checkBox1)).isChecked() ? "1" : "0")) ; 

				list.add(new BasicNameValuePair("files", files)) ;
				list.add(new BasicNameValuePair("links", links)) ;
				list.add(new BasicNameValuePair("cards", vcards)); 

				String  subject = "" ;
				String savedCCEmails = String.valueOf(BiptagUserManger.getUserValue("FU_CCEMAILS_TEMPS", "string",  NavigationActivity.getContext())) ; 
				String savedCCIEmails = String.valueOf(BiptagUserManger.getUserValue("FU_CCIEMAILS_TEMPS", "string",  NavigationActivity.getContext())) ;
				String savedContent= String.valueOf(BiptagUserManger.getUserValue("FU_CONTENT_TEMPS", "string",  NavigationActivity.getContext())) ;
				String savedSubject = String.valueOf(BiptagUserManger.getUserValue("FU_SUBJECT_TEMPS", "string",  NavigationActivity.getContext())) ; 
				if(savedContent.equals("") && savedSubject.equals("")){
					int selectedTemplateId = 0 ; 	
					try {
							selectedTemplateId = Integer.parseInt(String.valueOf(BiptagUserManger.getUserValue("selected_template", "string",  NavigationActivity.getContext()))) ; 
							BTFollowupTemplateDataSource dataSource = new BTFollowupTemplateDataSource( NavigationActivity.getContext()) ; 
							BTFollowupTemplate followupTemplate =dataSource.getTemplateWithLocalID(ba_id, selectedTemplateId) ;
							savedContent = followupTemplate.getFu_content() ;
							savedSubject = followupTemplate.getFu_subject() ; 
							JSONArray attach =followupTemplate.getFu_attach() ; 
							JSONArray ccEmails = new JSONArray() ;
							JSONArray cciEmails = new JSONArray() ;
							for (int i = 0; i < attach.length(); i++) {
								try {
									if(attach.getJSONObject(i).getString("type").equals("cci_email")){
										
									}
								} catch (JSONException e) {
									// TODO: handle exception
								}
							}
						} catch (NumberFormatException e) {
							// TODO: handle exception
						}
					
					
					
			 		
				}
				 
				//savedContent = savedContent.replaceAll("\\n", "<br>") ; 
				//Log.d("content", "content  "+savedContent) ;
				list.add(new BasicNameValuePair("text", savedContent));
				list.add(new BasicNameValuePair("subject", savedSubject)) ;
				try {
				    savedCCEmails = URLEncoder.encode(savedCCEmails,"UTF-8");
				    savedCCIEmails = URLEncoder.encode(savedCCIEmails,"UTF-8");
				} catch (UnsupportedEncodingException e) {
				    e.printStackTrace();
				} 
				//Log.d("follow up save ", "follow up save  cc  "+savedCCEmails) ; 
				//Log.d("follow up save ", "follow up save  cci  "+savedCCIEmails) ; 
				list.add(new BasicNameValuePair("cci", savedCCEmails )) ;
				list.add(new BasicNameValuePair("cc",savedCCIEmails )) ;
				list.add(new BasicNameValuePair("user_email", String.valueOf(BiptagUserManger.getUserValue("EMAIL", "string",  NavigationActivity.getContext())))); 
				List<NameValuePair> postList = new ArrayList<NameValuePair>() ; 
			
				
				contact_response.put("CONTACT_IMAGE", imageArray) ; 
				
				String contact_response_encoded=""; 
				try {
					contact_response_encoded = URLEncoder.encode(contact_response.toString(), "UTF-8");
				} catch (UnsupportedEncodingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} 
				postList.add(new BasicNameValuePair("json_response", contact_response_encoded )) ; 
				//Log.d("send contact ", "json hybrid response "+contact_response.toString()) ; 
				final int form_id = Integer.parseInt(form_id_string) ;
			
				BTFormulaire form_concerned  = null;
			
				
				BTFormDataSource formDataSource = new BTFormDataSource( NavigationActivity.getContext()) ; 
				form_concerned = formDataSource.getFormWithID(agent_id, form_id);
				JSONArray structure = form_concerned.getFormStructure(); 
				
			//	ContactItem  contact = new ContactItem(email, email, contact_response, structure, 0, form_id, 0	, false) ; 
// LZR 26 04 2016 cration d'un jsonObject et lui attribuer les donnees
				JSONObject contactItem = new JSONObject()  ;
				contactItem.put("CONTACT_NAME", email)  ; 
				contactItem.put("CONTACT_MAIL", email) ; 
				contactItem.put("CONTACT_FORM", structure)  ; 
				contactItem.put("CONTACT_RESPONSE", contact_response) ; 
				contactItem.put("CONTACT_ID", 0) ; 
				contactItem.put("CONTACT_FORM_ID", form_id);
				contactItem.put("CONTACT_FR_ID",0) ;
				final BTContact contact = new BTContact() ;
				contact.setContactEmail(email) ;

				Calendar cal = Calendar.getInstance();
				Date today = new Date();
				cal.setTime(today);


				SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");


				String 	referenceDate = formatter.format(cal.getTime());
				contact.setCreateTime(referenceDate);




				List<String> formKeys = new ArrayList<String>() ; 
				
				for (int i = 0; i < structure.length(); i++) {
					if(structure.getJSONObject(i).getString("type").equals("LABEL")){
						formKeys.add(structure.getJSONObject(i).getJSONArray("VALUE").getString(0)) ; 
					}
				}
				String name = contact_response.getString(formKeys.get(1))+" "+contact_response.getString(formKeys.get(2));
				contact.setContactName(name) ; 
				contact.setContactFormId(form_id) ; 
				contact.setContactFrAgentID(agent_id) ;
				contact.setContactFrHashcode("") ; 
				contact.setContactFrID(0) ; 
				contact.setContactFrResponse(contact_response) ;
				contact.setContactId(0) ; 
				contact.setDoFollowup(((CheckBox) getView().findViewById(R.id.checkBox1)).isChecked()) ;
				contact.setSynchronized(0)   ;
				JSONArray ccEmail = new JSONArray() ; 
				try {
					ccEmail = new JSONArray(savedCCEmails) ; 
				} catch (JSONException e) {
					// TODO: handle exception
				}
				JSONArray cciEmail = new JSONArray() ; 
				try {
					cciEmail = new JSONArray(savedCCIEmails) ; 
				} catch (JSONException e) {
					// TODO: handle exception
				}
				contact.setCcEmailRegister(ccEmail)  ; 
				contact.setCciEmailRegister(cciEmail) ; 
				BTContactDataSource dataSource = new BTContactDataSource( NavigationActivity.getContext()) ; 
//LZR 26 04 2016 insertion du contact dans BDD locale
					contact_local_id = dataSource.insertContact(contact) ;
				Log.d(" contact local id ", contact_local_id + " ");
 				String url= Constant.base_url +Constant.insert_contact_ws;
 				if(contact_local_id != 0){
 					contact.setContact_local_id((int) contact_local_id) ; 
 				}
				 
				
 				final BTContact btContact = contact ;
 				getActivity().runOnUiThread(new Runnable() {
					 
					@Override
					public void run() {
						// TODO Auto-generated method stub
						//ContactFragement.fragmentView = null ;
						Log.d(" contact ", " agent id " + agent_id + " formid " + form_id);
						BTFormDataSource btFormDataSource = new BTFormDataSource(NavigationActivity.getContext());
						ContactItem contact  = new ContactItem( btContact.getContactEmail() ,btContact.getContactName()  ,btContact.getContactFrResponse(),
								btFormDataSource.getFormWithID(agent_id,form_id).getFormName(), btContact.getContactId(),
								btContact.getContactFormId(),  btContact.getContactFrID(), true, btContact.getContact_local_id());
						 List<BTLog> logs  = null ;
						List<ContactItem>  data_sync = new LinkedList<ContactItem>() ;
						List<ContactItem>  data_Unsync = new LinkedList<ContactItem>() ;

						if(BiptagUserManger.isOnline(getActivity())) {
// LZR 26 04 2016 si l'ajout en mode connecté : ajouter le contact dans la liste des contacs sync
							data_sync.add(contact);
							ContactAdapter adapterSync = new ContactAdapter(NavigationActivity.getContext(), R.layout.contact_row, data_sync, logs);
							adapterSync.addContact(contact);
							Log.d(" contact ", " add  name " + contact.contact_name + " email " + contact.contact_email + " id " + contact.contactId + " " + adapterSync.getCount());

							ContactFragement.insertConctact(contact, 0);
						}else {
// LZR 26 04 2016 si l'ajout en mode deconnecté : ajouter le contact dans la liste des contacs unsync
						data_Unsync.add(contact);
							ContactAdapter adapterUnSync = new ContactAdapter(NavigationActivity.getContext(), R.layout.contact_row, data_Unsync, logs);
							adapterUnSync.addContact(contact);
							Log.d(" contact ", " add  name " + contact.contact_name + " email " + contact.contact_email + " id " + contact.contactId + " " + adapterUnSync.getCount());

							ContactFragement.insertConctact(contact, 1);

						}

						isFinishClicked= false ; 
						BiptagUserManger.clearField(NavigationActivity.getContext(), add_files_result_create) ;
						BiptagUserManger.clearField(NavigationActivity.getContext(), createImageArrayUserPreferenceName)  ; 
						BiptagUserManger.clearField(NavigationActivity.getContext(), "contact_email") ; 
						BiptagUserManger.clearField(NavigationActivity.getContext(), "updated_contact_creat") ;
					/*
						BiptagUserManger.saveUserData(add_files_result_create, "",  NavigationActivity.getContext()) ; 
		 				 BiptagUserManger.saveUserData(createImageArrayUserPreferenceName, "",  NavigationActivity.getContext())  ; 
		 				 BiptagUserManger.saveUserData("contact_email", "",	  NavigationActivity.getContext()) ; 
		 				 */
						
		 				 FillingFormStep1 formStep1 =  new FillingFormStep1();
		 					
		 					String form_id_1 = getArguments().getString("data") ; 
		 					//Log.d("form id ", "form id step 3 finish "+form_id_1) ;
		 					 
		 					
		 					
		 					
		 					List<Fragment> stack= NavigationActivity.stack1 ; 
		 					for (int i = 0; i < NavigationActivity.stack1.size(); i++) {
		 						if(NavigationActivity.stack1.get(i).getClass().getName().equals(FillingFormStep1.class.getName())){
		 							if(NavigationActivity.stack1.get(i) != null)
		 								try {
		 									NavigationActivity.stack1.get(i).onDestroy() ; 
		 			 									
										} catch (NullPointerException e) {
											// TODO: handle exception
										}
		 						}
		 						
		 						if(NavigationActivity.stack1.get(i).getClass().getName().equals(FillingFormStep2.class.getName())){
		 							if(NavigationActivity.stack1.get(i) != null)
		 								try {
		 									NavigationActivity.stack1.get(i).onDestroy() ; 
		 			 									
										} catch (NullPointerException e) {
											// TODO: handle exception
										}
		 						}
		 					}
		 					
						  
							
		 					NavigationActivity.stack1 = new LinkedList<Fragment>() ; 
		 					ConnectedhomeFragment connectedhomeFragment = new ConnectedhomeFragment() ; 
		 					NavigationActivity.stack1.add(connectedhomeFragment) ; 
		 					
		 					((NavigationActivity) getActivity()).updateContactNumberView() ; 
		 					
		 					BiptagNavigationManager.pushFragments(formStep1, true, "", NavigationActivity.getContext() , form_id_1, 1 );	
		 					onDestroy() ; 
					}
				}) ; 
 				
 				
				if(BiptagUserManger.isOnline( NavigationActivity.getContext())){
					JSONParser parser = new JSONParser() ; 
					try {
						
						JSONObject insertResponse = parser.makeHybridHttpRequest(url, list, postList) ; 
						
						if(insertResponse != null){
							Log.d("finish contact ", "finish contact url "+insertResponse.toString()) ;
							
							if(insertResponse.getInt("success") == 1){
								
								contact.setContactFrID(insertResponse.getInt("FR_ID")) ; 
								contact.setSynchronized(1) ; 
								contact.setContactId(insertResponse.getInt("CONTACT_ID")) ; 
								contact.setContactFrHashcode(insertResponse.getString("HASH_CODE")) ; 
										 
								boolean update= dataSource.updateContact(contact) ; 
								Log.d("finish contact ", "finish contact url update contact "+update+" contact id"+contact.getContactId() +"  "+agent_id+ "  "+contact.getContactFrAgentID()) ;

							}else{
								Log.d("finish contact ", "finish contact url success 0") ;

							}
						
						}
						
						
					} catch (JSONException e) {
						// TODO: handle exception
						Log.d("finish contact ", "finish contact url exception "+e.getMessage()) ;

					}
				
				}

				Log.d("finish contact ", "finish contact url "+url) ;
		//		Log.d("finish contact ", "finish contact "+jsonObject.toString()) ;

			} catch (JSONException e) {
				// TODO: handle exception
				Log.d("finish contact ", "finish contact eception  "+e.getMessage()) ;
			}
			return null;
		}
		
		 @Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
		//	super.onPostExecute(result);
		
		 
		 }
	}
	public int getcontactId( int id){

		return id;
	}

}
