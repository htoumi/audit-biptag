package com.biptag.biptag.fragements.settings;

import java.util.List;

import android.content.Context;
import android.graphics.Point;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.AbsListView.LayoutParams;

import com.biptag.biptag.R;

public class SettingMasterListviewAdapter extends ArrayAdapter {

	String[] values ; 
	int[]thumbNailsId ;
	Context context ; 
	int screenWidth , screenHeight ; 
	
	
	public SettingMasterListviewAdapter(Context context, int resource, List objects) {
		super(context, resource,  objects);
		// TODO Auto-generated constructor stub
		
		
		
		  WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
			Display display  =wm.getDefaultDisplay() ;
			Point point  = new Point() ; 
			display.getSize(point) ; 
			 screenWidth = point.x ; 
			 screenHeight = point.y ; 
		
		
		
	}
	
	public SettingMasterListviewAdapter(Context context, int resource, String[] objects, int[] thumbNails ) {
		super(context, resource,  objects);
		// TODO Auto-generated constructor stub
		this.values = objects ; 
		this.thumbNailsId = thumbNails ; 
		this.context = context ; 
	
	}
	
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		TextView txt = null ; 
		ImageView icon = null ; 
		if(convertView == null){
			 LayoutInflater inflater = (LayoutInflater) getContext()
	                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	            convertView = inflater.inflate(R.layout.setting_master_item, null);
	           
		}
		
		  txt = (TextView) convertView.findViewById(R.id.master_setting_item_txt) ; 
          icon = (ImageView) convertView.findViewById(R.id.master_setting_item_icon) ; 
  
        icon.setImageDrawable(context.getResources().getDrawable(this.thumbNailsId[position])) ; 
        txt.setText(this.values[position]) ; 
        
        
        LayoutParams layoutParams = new LayoutParams(LayoutParams.MATCH_PARENT,screenHeight/15) ; 
        convertView.setLayoutParams(layoutParams) ; 

		return convertView;
	}

}
