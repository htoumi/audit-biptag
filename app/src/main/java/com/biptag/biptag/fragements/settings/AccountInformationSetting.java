package com.biptag.biptag.fragements.settings;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Fragment;
import android.content.Context;
import android.graphics.Point;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ListView;
import android.widget.TextView;

import com.biptag.biptag.NavigationActivity;
import com.biptag.biptag.R;
import com.biptag.biptag.dbmanager.datasource.BTContactDataSource;
import com.biptag.biptag.dbmanager.datasource.BTFollowupTemplateDataSource;
import com.biptag.biptag.dbmanager.datasource.BTFormDataSource;
import com.biptag.biptag.dbmanager.datasource.BTLogDataSource;
import com.biptag.biptag.dbmanager.objects.BTContact;
import com.biptag.biptag.dbmanager.objects.BTFormulaire;
import com.biptag.biptag.dbmanager.objects.BTLog;
import com.biptag.biptag.managers.BiptagNavigationManager;
import com.biptag.biptag.managers.BiptagUserManger;

public class AccountInformationSetting extends Fragment {
	ListView listView ; 
	AccountInformationAdapter adapter ; 
	String[] menus ; 
	String[] values ; 
	View fragmentView ; 
	int agentId ; 
	int baId ; 
	int screenWidth  ; 
	int screenHeight ;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		
		fragmentView = inflater.inflate(R.layout.account_information_layout,null);
		//listView = (ListView) fragmentView.findViewById(R.id.account_info_listview)  ; 
	   WindowManager wm = (WindowManager) getActivity().getSystemService(Context.WINDOW_SERVICE);
			Display display  =wm.getDefaultDisplay() ;
			Point point  = new Point() ; 
			display.getSize(point) ; 
			 screenWidth = point.x ; 
			 screenHeight = point.y ;
			 
			 
				TextView separator = (TextView) fragmentView.findViewById(R.id.account_info_seprator) ; 
				LayoutParams  layoutParams = (LayoutParams)separator.getLayoutParams() ; 
				
			layoutParams.setMargins(screenWidth/40,0, screenWidth/40, 0) ; 
			separator.setLayoutParams(layoutParams)  ; 
			separator.setHeight(screenHeight /480) ;
			//Log.d("screen height", "screen heigth "+screenHeight) ;
			TextView title = (TextView) fragmentView.findViewById(R.id.account_info_title ) ; 
			//title.setTextSize(screenWidth/20) ; 
			title.setTextSize(NavigationActivity.getContext().getResources().getDimension(R.dimen.acc_info_size));
			LayoutParams params = (LayoutParams) title.getLayoutParams() ; 
			params.setMargins(screenWidth/40,0, screenWidth/40, 0) ;
			title.setLayoutParams(params) ; 
			
			TextView separator1 = (TextView) fragmentView.findViewById(R.id.conso_info_seprator) ; 
			LayoutParams  layoutParams1 = (LayoutParams)separator.getLayoutParams() ; 
			
			layoutParams1.setMargins(screenWidth/40,0, screenWidth/40, 0) ; 
			separator1.setLayoutParams(layoutParams1)  ; 
			separator1.setHeight(screenHeight /480) ; 
		
			TextView title1 = (TextView) fragmentView.findViewById(R.id.conso_info_title ) ; 
			//title1.setTextSize(screenWidth/20) ; 
			title1.setTextSize(NavigationActivity.getContext().getResources().getDimension(R.dimen.acc_info_size));
			LayoutParams params1 = (LayoutParams) title.getLayoutParams() ; 
			params1.setMargins(screenWidth/40,0, screenWidth/40, 0) ;
			title1.setLayoutParams(params1) ; 
			 
			TextView separator2 = (TextView) fragmentView.findViewById(R.id.quota_info_seprator) ; 
			LayoutParams  layoutParams2 = (LayoutParams)separator.getLayoutParams() ; 
			
			layoutParams2.setMargins(screenWidth/40,0, screenWidth/40, 0) ; 
			separator2.setLayoutParams(layoutParams2)  ; 
			separator2.setHeight(screenHeight /480) ;
			TextView title2 = (TextView) fragmentView.findViewById(R.id.quota_info_title) ; 
			//title2.setTextSize(screenWidth/20) ; 
			title2.setTextSize(NavigationActivity.getContext().getResources().getDimension(R.dimen.acc_info_size));
			LayoutParams params2 = (LayoutParams) title.getLayoutParams() ; 
			params2.setMargins(screenWidth/40,0, screenWidth/40, 0) ;
			title2.setLayoutParams(params2) ; 
		
			
			
			menus = new String[]{
					NavigationActivity.getContext().getResources().getString(R.string.inf_user) , NavigationActivity.getContext().getResources().getString(R.string.inf_societes) , NavigationActivity.getContext().getResources().getString(R.string.inf_email) , NavigationActivity.getContext().getResources().getString(R.string.inf_account_type) , NavigationActivity.getContext().getResources().getString(R.string.inf_admin_accounts) 	
			} ; 
			/*BiptagUserManger.saveUserData("EMAIL", email, Connexion.this) ;
							BiptagUserManger.saveUserData("USER_ID", jsonObject.getInt("USER_ID"), Connexion.this) ;
							BiptagUserManger.saveUserData("BA_ID", jsonObject.getInt("BA_ID"), Connexion.this) ;
							BiptagUserManger.saveUserData("AGENT_ID", jsonObject.getInt("AGENT_ID"), Connexion.this) ;
							BiptagUserManger.saveUserData("ROLE_DESC", jsonObject.getString("ROLE_DESC"), Connexion.this) ;
							BiptagUserManger.saveUserData("AGENT_FNAME", jsonObject.getString("AGENT_FNAME"), Connexion.this) ;
							BiptagUserManger.saveUserData("AGENT_LNAME", jsonObject.getString("AGENT_LNAME"), Connexion.this) ;
							BiptagUserManger.saveUserData("BA_ADRESS", jsonObject.getString("BA_ADRESS"), Connexion.this) ;
							BiptagUserManger.saveUserData("BA_ZIPCODE", jsonObject.getString("BA_ZIPCODE"), Connexion.this) ;
							BiptagUserManger.saveUserData("BA_COUNTRY", jsonObject.getString("BA_COUNTRY"), Connexion.this) ;
							BiptagUserManger.saveUserData("BA_COMPANY", jsonObject.getString("BA_COMPANY"), Connexion.this) ;
							BiptagUserManger.saveUserData("LAST_UPDATE", jsonObject.getString("LAST_UPDATE"), Connexion.this) ;
							BiptagUserManger.saveUserData("PRODUCT_CONFIG", jsonObject.getString("PRODUCT_CONFIG"), Connexion.this) ;
							*/
			values = new String[]{
					String.valueOf(BiptagUserManger.getUserValue("AGENT_FNAME", "string", getActivity()))+" "+String.valueOf(BiptagUserManger.getUserValue("AGENT_LNAME", "string", getActivity())) , 
					String.valueOf(BiptagUserManger.getUserValue("BA_COMPANY", "string", getActivity())) ,
					String.valueOf(BiptagUserManger.getUserValue("EMAIL", "string", getActivity())) , 
					String.valueOf(BiptagUserManger.getUserValue("ROLE_DESC", "string", getActivity())) ,
					""	
			};
			//adapter = new AccountInformationAdapter(getActivity(), android.R.layout.simple_list_item_1, menus, values, 4)  ; 
			//listView.setAdapter(adapter) ; 
			
			setItemValue(R.id.info_user_item, NavigationActivity.getContext().getResources().getString(R.string.inf_user), String.valueOf(BiptagUserManger.getUserValue("AGENT_FNAME", "string", getActivity()))+" "+String.valueOf(BiptagUserManger.getUserValue("AGENT_LNAME", "string", getActivity())), true)	 ; 
			setItemValue(R.id.info_email_item, NavigationActivity.getContext().getResources().getString(R.string.inf_email), String.valueOf(BiptagUserManger.getUserValue("EMAIL", "string", getActivity())), true) ; 
			setItemValue(R.id.info_company_item, NavigationActivity.getContext().getResources().getString(R.string.inf_societes), String.valueOf(BiptagUserManger.getUserValue("BA_COMPANY", "string", getActivity())), true) ; 
			setItemValue(R.id.info_role_item, NavigationActivity.getContext().getResources().getString(R.string.inf_account_type), String.valueOf(BiptagUserManger.getUserValue("ROLE_DESC", "string", getActivity())), true) ; 
			String agentIdString = String.valueOf(BiptagUserManger.getUserValue("AGENT_ID", "int", getActivity())) ; 
			agentId  = Integer.parseInt(agentIdString) ; 
			
			BTFormDataSource dataSource = new BTFormDataSource(getActivity()) ; 
			List<BTFormulaire>  formulaires = dataSource.getAllForms(agentId) ;
			int publishedForms  = 0 ; 
			for (int i = 0; i < formulaires.size(); i++) {
				if(formulaires.get(i).getFormStatus().equals("PUBLISHED")){
					publishedForms ++  ; 
				}
			}
			setItemValue(R.id.conso_form_item, NavigationActivity.getContext().getResources().getString(R.string.inf_number_forms), String.valueOf(publishedForms), true) ; 
			String docsJson = String.valueOf(BiptagUserManger.getUserValue("docs", "string", getActivity())) ; 
			String coWorkers = String.valueOf(BiptagUserManger.getUserValue("CO-WORKERS", "string", getActivity())) ; 
			//Log.d("json exeption", "account info json exception "+docsJson) ;
			//Log.d("json exeption", "account info json exception "+coWorkers) ;
			
			try {
				JSONObject object  = new JSONObject(docsJson) ; 
				JSONArray docs = object.getJSONArray("DOC").getJSONObject(0).getJSONArray("FILES") ; 
				JSONArray links = object.getJSONArray("LINK").getJSONObject(0).getJSONArray("LINKS") ; 
				JSONArray cards = object.getJSONArray("CARDS").getJSONObject(0).getJSONArray("CARDS") ; 
				
				int mediaNumber  = docs.length() + links.length() +cards.length() ; 
				
				
				 
				setItemValue(R.id.conso_docs_item, NavigationActivity.getContext().getResources().getString(R.string.inf_number_documents), String.valueOf(mediaNumber), true) ; 
			} catch (JSONException e) {
				// TODO: handle exception
				//Log.d("json exeption", "account info json exception "+e.getMessage()) ;
				setItemValue(R.id.conso_docs_item, "Documents", String.valueOf(0), true) ; 

			}
			
			
			try {
				final JSONArray array = new JSONArray(coWorkers) ; 
				setItemValue(R.id.info_accounts_item, NavigationActivity.getContext().getResources().getString(R.string.inf_admin_accounts), String.valueOf(array.length()), false) ; 
				
				LinearLayout accounts = (LinearLayout) fragmentView.findViewById(R.id.info_accounts_item) ; 
				accounts.setOnClickListener(new View.OnClickListener() {
					
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						
						AccountsListingFragment fragment = new AccountsListingFragment(array)  ; 
						BiptagNavigationManager.pushFragments(fragment, true, "list_accounts", getActivity(), null , 3) ; 
						
						
					}
				}) ; 
				
				
			} catch (JSONException e) {
				// TODO: handle exception
				setItemValue(R.id.info_accounts_item, NavigationActivity.getContext().getResources().getString(R.string.inf_admin_accounts), String.valueOf(0), false) ; 
				
			}
			
			String baIdString = String.valueOf(BiptagUserManger.getUserValue("BA_ID", "int", getActivity())) ; 
			baId  = Integer.parseInt(baIdString) ; 
			BTFollowupTemplateDataSource  dataSource2 = new BTFollowupTemplateDataSource(getActivity()) ; 
			int fuNumber = dataSource2.getAllTempalte(baId).size() ; 
			//Log.d("follow up number", "follow up number "+fuNumber) ;
			setItemValue(R.id.conso_fu_item, NavigationActivity.getContext().getResources().getString(R.string.inf_number_templates), String.valueOf(fuNumber), true) ; 
			 
			BTContactDataSource dataSource3 = new BTContactDataSource(getActivity()) ; 
			List<BTContact> contacts   = dataSource3.getAllContacts(agentId, -1) ;
			List<BTContact> distinctContact = new ArrayList<BTContact>() ; 
			for (int i = 0; i < contacts.size(); i++) {
				boolean found = false  ; 
				for (int j = 0; j < distinctContact.size(); j++) {
					if(contacts.get(i).getContactEmail().equals(distinctContact.get(j).getContactEmail())){
						found = true ; 
						break ; 
					} 
				}
				
				if(!found){
					distinctContact.add(contacts.get(i) ) ; 
				}
			}
			
			
			BTLogDataSource  source = new BTLogDataSource(getActivity()) ; 
			List<BTLog> logs = source.getAllLogs(agentId) ; 
			
			
			int openers = 0 ; 
			int clickers= 0 ; 
			int passifs = 0 ; 
			for (int i = 0; i < logs.size(); i++) {
				//Log.d("log type", "log type "+logs.get(i).getLog_type()) ;
				if(logs.get(i).getLog_type().equals("opened")){
					openers++ ; 
				}
				if(logs.get(i).getLog_type().equals("download") || logs.get(i).getLog_type().equals("click")){
					clickers++ ; 
				}
			}
			
			passifs =  logs.size()- openers  ;
		int pourcentClickers =  clickers;
		int pourcentPassifs = passifs ;
		int pourcentOppeners = openers ;
		if (logs.size()!=0) {
			 pourcentClickers = (int) (clickers * 100) / logs.size();
			 pourcentPassifs = (int) (passifs * 100) / logs.size();
			 pourcentOppeners = (int) (openers * 100) / logs.size();
		}

			setItemValue(R.id.quota_contact, NavigationActivity.getContext().getResources().getString(R.string.inf_number_contacts), String.valueOf(distinctContact.size()), true) ;
			setItemValue(R.id.quota_click, NavigationActivity.getContext().getResources().getString(R.string.inf_clicker), String.valueOf(pourcentClickers), true) ;
			setItemValue(R.id.quota_email_sent, NavigationActivity.getContext().getResources().getString(R.string.inf_emails_send),String.valueOf(logs.size()), true) ; 
			setItemValue(R.id.quota_open, NavigationActivity.getContext().getResources().getString(R.string.inf_openers), String.valueOf(pourcentOppeners), true);
			setItemValue(R.id.quota_passif, NavigationActivity.getContext().getResources().getString(R.string.inf_passif), String.valueOf(pourcentPassifs), true)  ;


		Button actionBarButton = (Button)	getActivity().getActionBar().getCustomView().findViewById(R.id.formste1_btn_container) ;
		actionBarButton.setBackground(NavigationActivity.getContext().getResources().getDrawable(R.drawable.go_back));

	//	actionBarButton.setText(NavigationActivity.getContext().getResources().getString(R.string.return_image_list));
		actionBarButton.setVisibility(View.VISIBLE) ;
		actionBarButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				SettingMenuFragment settingMenuFragment = new SettingMenuFragment();
				BiptagNavigationManager.pushFragments(settingMenuFragment, true, this.getClass().getName(), getActivity(), null, 3);

			}
		}) ;


		return fragmentView ;
	}
	
	
	
	public void setItemValue(int id , String title , String value , boolean hideThumbNail){
		View item = (View) fragmentView.findViewById(id) ; 
		TextView titleView = (TextView) item.findViewById(R.id.account_info_title) ; 
		TextView valueView = (TextView) item.findViewById(R.id.account_info_value)  ; 
		titleView.setText(title) ; 
		valueView.setText(value) ; 
		if(hideThumbNail){
			ImageView img = (ImageView)item.findViewById(R.id.account_info_thumbnail) ; 
			img.setVisibility(View.INVISIBLE) ; 
		} 
		
		LayoutParams layoutParams = (LayoutParams) item.getLayoutParams() ; 
		layoutParams.setMargins(screenWidth/40, 0, screenWidth/40, screenHeight/40) ;
		item.setLayoutParams(layoutParams) ; 
		TextView sep = (TextView) item.findViewById(R.id.item_cell_separator) ; 
		sep.setHeight(screenHeight/480) ; 
		
	}

	@Override
	public void onResume() {
		super.onResume();
		Button actionBarButton = (Button)	getActivity().getActionBar().getCustomView().findViewById(R.id.formste1_btn_container) ;
		actionBarButton.setBackground(NavigationActivity.getContext().getResources().getDrawable(R.drawable.go_back));

	//	actionBarButton.setText(NavigationActivity.getContext().getResources().getString(R.string.return_image_list));

		actionBarButton.setVisibility(View.VISIBLE) ;

	}

	@Override
	public void onStop() {
		super.onStop();
		Button actionBarButton = (Button)	getActivity().getActionBar().getCustomView().findViewById(R.id.formste1_btn_container) ;
		actionBarButton.setBackground(NavigationActivity.getContext().getResources().getDrawable(R.drawable.go_back));

//		actionBarButton.setText(NavigationActivity.getContext().getResources().getString(R.string.return_image_list));

		actionBarButton.setVisibility(View.INVISIBLE) ;

	}
}
