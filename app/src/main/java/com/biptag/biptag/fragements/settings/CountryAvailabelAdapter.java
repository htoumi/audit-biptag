

package com.biptag.biptag.fragements.settings;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.biptag.biptag.R;

public class CountryAvailabelAdapter extends ArrayAdapter<String> {

	private String[] values ; 
	private String[] availableValues; 
	public CountryAvailabelAdapter(Context context, int resource,
			String[] objects , String[] availableValues) {
		super(context, resource, objects);
		this.values = objects ;
		this.availableValues  = availableValues  ; 
	
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		if(convertView ==  null){
			 LayoutInflater inflater = (LayoutInflater) getContext()
	                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	            convertView = inflater.inflate(R.layout.general_change_list_item, null);
		}
		TextView txt = (TextView) convertView.findViewById(R.id.general_change_item) ; 
		txt.setText(values[position]) ; 
		
		
		
		ImageView img = (ImageView) convertView.findViewById(R.id.general_change_icon) ; 
		boolean found = false ; 
		for (int i = 0; i < availableValues.length && i < values.length; i++) {
			Log.d(" countries ", availableValues[i] + " " + values[position]+"  "+position );

			if(availableValues[i].equals(values[position]) ){
				found = true ;
				break  ; 
			}
			}

Log.d(" countries ", values[position] + " " + found );
		if(!found){
			img.setVisibility(View.INVISIBLE) ; 
				
		}else{
			img.setVisibility(View.VISIBLE) ; 
		}
		
		return convertView;
	}

	 
	
}

