package com.biptag.biptag.fragements.settings;

import android.app.AlertDialog;
import android.app.Fragment;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.biptag.biptag.R;
import com.biptag.biptag.fragements.DisconnectFragement;
import com.biptag.biptag.managers.BiptagNavigationManager;

public class SettingMenuFragment extends Fragment {

    ListView menuSettingListView;

    //setting_menu_fragment
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        final View fragmentView = inflater.inflate(R.layout.setting_menu_fragment, null);
        menuSettingListView = (ListView) fragmentView.findViewById(R.id.menuSettingListView);

        String[] menuItems = new String[] {
                getResources().getString(R.string.setting_general), getResources().getString(R.string.setting_synch), getResources().getString(R.string.setting_info), getResources().getString(R.string.logout)
        };
        int[] menuItemsThumbNailsId = new int[] {
                R.drawable.setting_general_icon, R.drawable.setting_sync_icon, R.drawable.setting_stat_icon, R.drawable.setting_logout_icon
        };

        SettingMasterListviewAdapter adapter = new SettingMasterListviewAdapter(getActivity(), android.R.layout.simple_list_item_1, menuItems, menuItemsThumbNailsId);

        menuSettingListView.setAdapter(adapter);


        menuSettingListView.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                // TODO Auto-generated method stub

                if (position == 0) {
                    SettingGeneralMenu generalMenu = new SettingGeneralMenu();
                    BiptagNavigationManager.pushFragments(generalMenu, true, this.getClass().getName(), getActivity(), null, 3);
                }
                if (position == 1) {
                    SettingSynchronisationFragment fragment = new SettingSynchronisationFragment();
                    BiptagNavigationManager.pushFragments(fragment, true, this.getClass().getName(), getActivity(), null, 3);
                }

                if (position == 2) {
                    AccountInformationSetting informationSetting = new AccountInformationSetting();
                    BiptagNavigationManager.pushFragments(informationSetting, true, this.getClass().getName(), getActivity(), null, 3);

                }

                if (position == 3) {
                    logOut();
                }


            }
        });


        return fragmentView;

    }

    public void logOut() {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(getResources().getString(R.string.logout));
        builder.setMessage(getResources().getString(R.string.logout_confirm));
        builder.setPositiveButton(getResources().getString(R.string.no), new OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                // TODO Auto-generated method stub
                dialog.dismiss();
            }
        });
        builder.setNegativeButton(getResources().getString(R.string.yes), new OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                // TODO Auto-generated method stub

                DisconnectFragement disconnectFragement = new DisconnectFragement();
                //  BiptagNavigationManager.pushFragments(disconnectFragement,false, "logout");
                BiptagNavigationManager.pushFragments(disconnectFragement, true, this.getClass().getName(), getActivity(), null, 3);

            }
        });
        builder.show();
        Log.d("touch tabhost", "touch tabhost");


    }

}
