package com.biptag.biptag.fragements;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.biptag.biptag.CustumViews.BTEditText;
import com.biptag.biptag.NavigationActivity;
import com.biptag.biptag.R;
import com.biptag.biptag.dbmanager.datasource.BTImageDataSource;
import com.biptag.biptag.dbmanager.objects.BTImage;
import com.biptag.biptag.managers.BiptagNavigationManager;
import com.biptag.biptag.managers.BiptagUserManger;
import com.biptag.biptag.ocr.CaptureActivity;
import com.biptag.biptag.ocr.OcrResult;
import com.biptag.biptag.webservices.ImageDownloader;
import com.googlecode.leptonica.android.ReadFile;
import com.googlecode.tesseract.android.ResultIterator;
import com.googlecode.tesseract.android.TessBaseAPI;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Fragment;
import android.content.ClipData;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.res.AssetManager;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.PointF;
import android.graphics.Rect;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.text.Editable;
import android.text.InputType;
import android.text.Selection;
import android.util.FloatMath;
import android.util.Log;
import android.view.Display;
import android.view.DragEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnDragListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.View.DragShadowBuilder;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.DialerFilter;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.HorizontalScrollView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.LinearLayout.LayoutParams;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

public class EditOCRFragment extends Fragment implements View.OnTouchListener {
	int screenWidth, screenHeight ;
	public static View fragmentView ;
	String[] propositions   ;
	LinearLayout ocrPropostionContainer ;
	JSONObject buitlContact, updatedContact ;
	ImageView imageView;
	private static final String TAG = "Touch";
	@SuppressWarnings("unused")
	private static final float MIN_ZOOM = 1f,MAX_ZOOM = 1f;
	String key;
	// These matrices will be used to scale points of the image
	Matrix matrix = new Matrix();
	Matrix savedMatrix = new Matrix();
	ResultIterator iterator;
	String RecognizedText;
	// The 3 states (events) which the user is trying to perform
	static final int NONE = 0;
	static final int DRAG = 1;
	static final int ZOOM = 2;
	int mode = NONE;
	int stack;
	boolean isEditMode;
	JSONArray jsonArray;
	String bitmapName = null;
	Bitmap	bitmapInter;
	TessBaseAPI baseApi;
	// these PointF objects are used to record the point(s) the user is touching
	PointF start = new PointF();
	PointF mid = new PointF();
	float oldDist = 1f;
	Bitmap bitmap = null;
	String ocrResult;
	private Paint paint = new Paint();
	@SuppressLint({"NewApi", "ValidFragment"})
public EditOCRFragment(JSONArray array,boolean editMode,int stack){
	this.stack=stack;
	this.isEditMode=editMode;
	this.jsonArray=array;
}
	@SuppressLint({"NewApi", "ValidFragment"})
	public  EditOCRFragment(){

	}
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		fragmentView = null;
		fragmentView = inflater.inflate(R.layout.ocr_edit_layout, null);
		WindowManager wm = (WindowManager) NavigationActivity.getContext().getSystemService(Context.WINDOW_SERVICE);
		Display display = wm.getDefaultDisplay();
		Point point = new Point();
		display.getSize(point);
		screenWidth = point.x;
		screenHeight = point.y;
		fixLayout();
		Bundle bundle = new Bundle();
	Log.d(" isEditMode ", " ocrReturned "+isEditMode);
		bundle.clear();
		bundle = this.getArguments();

		final int Mode = bundle.getInt("Create_Mode");

		if (Mode == 5) {
			key = "images_for_user";
		} else {
			key = "images_for_user_edit";
		}
		String imageJsonString = String.valueOf(BiptagUserManger.getUserValue(key, "string", NavigationActivity.getContext()));
		JSONArray imagesJson = new JSONArray();





		try {
			imagesJson = new JSONArray(imageJsonString);
			Log.d(" image string received ", " image received " + imagesJson.toString() + " " + imageJsonString);

		} catch (JSONException e) {
			// TODO Auto-generated catch block

		}

//organisation du traitement d'OCR selon mod ( creation ou edition ) et scan pour la première fois ou plus
		ocrRecognation(Mode, isEditMode);

		Log.d("path exists", bitmapName);
		bitmap = BitmapFactory.decodeFile(bitmapName);

		 bitmapInter = bitmap;
		 baseApi = new TessBaseAPI();
		baseApi.setDebug(true);
		String language = "fra";
		String tessdataDir = String.valueOf(BiptagUserManger.getUserValue("tessdataDir", "string", NavigationActivity.getContext()));
		baseApi.init(tessdataDir, language);
		if (bitmapInter!=null) {
			baseApi.setImage(ReadFile.readBitmap(bitmapInter));
		}else
		{
			AlertDialog.Builder builder = new AlertDialog.Builder(NavigationActivity.getContext());
			builder.setTitle(NavigationActivity.getContext().getResources().getString(R.string.image_deleted_title));
			builder.setMessage(NavigationActivity.getContext().getResources().getString(R.string.image_deleted_dsc));
			builder.setCancelable(false);
			builder.setPositiveButton("OK", new OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					// TODO Auto-generated method stub
					BiptagUserManger.clearField(NavigationActivity.getContext(),"ocrResult");
					BiptagUserManger.clearField(NavigationActivity.getContext(),"ocrOrigin");

					if (Mode == 5) {
						BiptagUserManger.clearField(NavigationActivity.context,"ScanOCR_Creat");
						BiptagNavigationManager.pushFragments(EditOCRFragment.this, false, EditOCRFragment.class.getName(), NavigationActivity.getContext(), null, stack);
					}
					if (Mode == 6) {
						BiptagUserManger.clearField(NavigationActivity.context,"ScanOCR_Edit");
						BiptagNavigationManager.pushFragments(EditOCRFragment.this, false, EditOCRFragment.class.getName(), NavigationActivity.getContext(), null, stack);

					}

				}
			});
		builder.show();
		}

		String RecognizedText = baseApi.getUTF8Text();

	BiptagUserManger.saveUserData("cardscanned", 1, NavigationActivity.getContext());
		BiptagUserManger.saveUserData("ocrScanReturn", "1", NavigationActivity.getContext());

		ocrPropostionContainer = (LinearLayout) fragmentView.findViewById(R.id.ocr_prop_container);
		//getArguments().clear();

// LZR : 20/03/2016 :imageView contenant le Bitmap
		imageView = (ImageView) fragmentView.findViewById(R.id.scanOcrImgView);
//		Log.d(" bitmap exists ", bitmap.getWidth() + " " + bitmap.getHeight());
//		if (bitmap!=null) {
			imageView.setImageBitmap(bitmap);
/*		}else
		{
			AlertDialog.Builder builder = new AlertDialog.Builder(NavigationActivity.getContext());
			builder.setTitle(" Image perdue ou a été suprimée ");
			builder.setMessage("Merci de refaire le scan");
			builder.setCancelable(false);
			builder.setPositiveButton("OK", new OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					// TODO Auto-generated method stub
					BiptagUserManger.clearField(NavigationActivity.getContext(),"ocrResult");
					BiptagUserManger.clearField(NavigationActivity.getContext(),"ocrOrigin");

					if (Mode == 5) {
						BiptagUserManger.clearField(NavigationActivity.context,"ScanOCR_Creat");
						BiptagNavigationManager.pushFragments(EditOCRFragment.this, false, EditOCRFragment.class.getName(), NavigationActivity.getContext(), null, stack);
					}
					if (Mode == 6) {
						BiptagUserManger.clearField(NavigationActivity.context,"ScanOCR_Edit");
						BiptagNavigationManager.pushFragments(EditOCRFragment.this, false, EditOCRFragment.class.getName(), NavigationActivity.getContext(), null, stack);

					}

				}
			});
builder.show();
		}
*/		imageView.setOnTouchListener(this);
//resultat d' ocr
		String ocr_result = ocrResult.replaceAll("\n\n\n","\n");
		ocr_result=ocr_result.replaceAll("\n\n","\n");
		ocr_result=ocr_result.replaceAll("\n ", "");
//list des lignes pour le matching
		propositions=ocr_result.split("\n");

		updatedContact = buitlContact;
	//	BiptagUserManger.saveUserData("updated_contact_creat", updatedContact.toString(),NavigationActivity.getContext());
		Log.d(" updatedContact ", updatedContact + " isEditMode buitlcontact " + buitlContact);
		loadMatchedProposition() ;
if (ocr_result.length()>1) {
	loadproposition();
}


		Log.d("Image ", "mode : " + Mode);

		 ((Button) fragmentView.findViewById(R.id.ocr_edit_layout_cancel_btn)).setOnClickListener(new View.OnClickListener() {

			 @Override
			 public void onClick(View v) {
				 // TODO Auto-generated method stub
				 BiptagUserManger.clearField(NavigationActivity.getContext(),"ocrResult");
				 BiptagUserManger.clearField(NavigationActivity.getContext(),"ocrOrigin");

				 if (Mode == 5) {
					 BiptagUserManger.clearField(NavigationActivity.context,"ScanOCR_Creat");
					 BiptagNavigationManager.pushFragments(EditOCRFragment.this, false, EditOCRFragment.class.getName(), NavigationActivity.getContext(), null, stack);
				 }
				 if (Mode == 6) {
					 BiptagUserManger.clearField(NavigationActivity.context,"ScanOCR_Edit");
					 BiptagNavigationManager.pushFragments(EditOCRFragment.this, false, EditOCRFragment.class.getName(), NavigationActivity.getContext(), null, stack);

				 }


			 }
		 }) ;


		final String  finalOcr_result= ocrResult;

		//final String finalOcr_result1 = finalOcr_result;
		((Button) fragmentView.findViewById(R.id.ocr_edit_layout_ok_btn)).setOnClickListener(new View.OnClickListener() {

			 @Override
			 public void onClick(View v) {
				 // TODO Auto-generated method stub
				 //		 if (Mode == 6) {
				 saveOcrBitmap(finalOcr_result);
				 BiptagUserManger.clearField(NavigationActivity.getContext(), "ocrResult");
				 BiptagUserManger.clearField(NavigationActivity.getContext(),"ocrOrigin");

				 if (Mode == 5) {
					 JSONObject contactResult = getBuiltContact();
					 BiptagUserManger.saveUserData("EditscanOCR_Creat", "editscan", NavigationActivity.getContext());
					 BiptagUserManger.saveUserData("contact_result_create", contactResult.toString(), NavigationActivity.getContext());
					 BiptagUserManger.saveUserData("must_fill_with_data", "1", NavigationActivity.getContext());
					 BiptagNavigationManager.pushFragments(EditOCRFragment.this, false, EditOCRFragment.class.getName(), NavigationActivity.getContext(), null, stack);
				 }
				 if (Mode == 6) {
					 JSONObject contactResult = getBuiltContact();
					 BiptagUserManger.saveUserData("EditscanOCR_Edit", "editscan", NavigationActivity.getContext());
					 BiptagUserManger.saveUserData("contact_result_edit", contactResult.toString(), NavigationActivity.getContext());
					 BiptagUserManger.saveUserData("must_fill_with_data", "2", NavigationActivity.getContext());
					 BiptagNavigationManager.pushFragments(EditOCRFragment.this, false, EditOCRFragment.class.getName(), NavigationActivity.getContext(), null, stack);
				 }
			 }}) ;

		return fragmentView;
	}
//LZR : 04/04/2016 organisation of ocr data
public void ocrRecognation(int Mode, boolean iSeditmod){
	String ImagesPath ;
	String ImagetxtOcr ;
	String ImageDataOcr ;

	if (Mode==6){
//Mode edition


		if (String.valueOf(BiptagUserManger.getUserValue("ScanOCR_Edit","string",NavigationActivity.getContext())).indexOf("scanocr")!=-1){
//LZR : 04/04/2016 Reprendre le scan
				File path = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/Biptag");
			String[] fileNames;
			if (path.exists()) {
				fileNames = path.list();
				Log.d("path exists", fileNames.length + " ");
				bitmapName = String.valueOf(BiptagUserManger.getUserValue("bitmapName", "string", NavigationActivity.getContext()));
				ocrResult = String.valueOf(BiptagUserManger.getUserValue("ocrResultEdit", "string", NavigationActivity.getContext()));
				Log.d("ocrRecognation01 ", " Mode " + Mode + " isEditMode " + String.valueOf(BiptagUserManger.getUserValue("ScanOCR_Creat","string",NavigationActivity.getContext())) + " " + bitmapName + " " + ocrResult);
				//	bitmap = BitmapFactory.decodeFile(bitmapName);
			}
			try {
				updatedContact = buitlContact = new JSONObject(String.valueOf(BiptagUserManger.getUserValue("updated_contact_edit", "string", NavigationActivity.getContext())));
				Log.d("ocrRecognation01 ", " Mode " + Mode + " isEditMode " + isEditMode + " " + buitlContact);


			} catch (JSONException e) {
				e.printStackTrace();
			}

		}else if (isEditMode) {
//LZR : 04/04/2016afficher l'ecran des tags en edition (apres avoir enregistrer le contact )
			JSONArray imagesJson = jsonArray;
			JSONArray imageJsonTemp = jsonArray;

			JSONArray copy = imagesJson;
			int ba_id = Integer.parseInt(String.valueOf(BiptagUserManger.getUserValue("BA_ID", "int", NavigationActivity.getContext())));

			try {
				BTImageDataSource source = new BTImageDataSource(NavigationActivity.getContext());
				for (int i = 0; i < copy.length(); i++) {

					String imgName = copy.getString(i);
					Log.d("image json recognation4", "recognation search " + imgName);
					BTImage image = source.selectImagesWithName(imgName, ba_id);


					if (image != null) {

						imageJsonTemp.put(image.getImageId());
						if (image.getImagePathOCR() != null) {
//LZR : 04/04/2016 si l'image contient des inf OCR (prise par l'OCR )
							ImagesPath = image.getImagePathOCR();
							ImagetxtOcr = String.valueOf(BiptagUserManger.getUserValue("updated_contact_edit", "string", NavigationActivity.getContext()));

							ImageDataOcr = image.getImageDataOCR();
							updatedContact = buitlContact = new JSONObject(ImagetxtOcr);

						//	Log.d("image json recognation4", "recognation id " + image.getImageId() + " ocr path " + image.getImagePathOCR() + " ocr data " + image.getImageDataOCR());

							bitmapName = ImagesPath;
							ocrResult = ImageDataOcr;
							BiptagUserManger.saveUserData("ocrResultedit", ocrResult, NavigationActivity.getContext());
							BiptagUserManger.saveUserData("updated_contact_edit", buitlContact.toString(), NavigationActivity.getContext());

							//Log.d("ocrRecognation5 ", " Mode " + Mode + " isEditMode " + isEditMode + " " + bitmapName + " " + ocrResult + buitlContact);
						}
						//Log.d("image json recognation5", "recognation img ID " + image.getImageId());
					}


				}
			} catch (JSONException e) {
				// TODO: handle exception
			}


		}
			else if (String.valueOf(BiptagUserManger.getUserValue("EditscanOCR_Edit","string",NavigationActivity.getContext())).indexOf("editscan")!=-1){
//LZR : 04/04/2016 afficher l'ecran des tags  avant d'enregister le contact

			JSONArray	imagesJson ;
			JSONArray imageJsonTemp ;


			String imageJsonString = String.valueOf(BiptagUserManger.getUserValue("images_for_user_edit", "string", NavigationActivity.getContext()));
			imagesJson = new JSONArray();




			try {
				imagesJson = new JSONArray(imageJsonString);
				Log.d(" image string received ", " image received " + imagesJson.toString() + " " + imageJsonString);

			} catch (JSONException e) {
				// TODO Auto-generated catch block

			}

			JSONArray copy = imagesJson;
			imageJsonTemp = imagesJson;
			int ba_id = Integer.parseInt(String.valueOf(BiptagUserManger.getUserValue("BA_ID", "int", NavigationActivity.getContext())));

			try {
				BTImageDataSource source = new BTImageDataSource(NavigationActivity.getContext());
				Log.d("image json recognation02", "recognation copy.length " + copy.length());

				if (copy.length()>0){

					int imgName = copy.getInt(0);
					Log.d("image json recognation02", "recognation search " + imgName);
					//BTImage image = source.selectImagesWithName(imgName, ba_id);
					BTImage image = source.selectImagesWithId(imgName, ba_id) ;


					if (image != null) {
						imageJsonTemp.put(image.getImageId());
						String imagePath = NavigationActivity.getContext().getCacheDir().getAbsolutePath()+"/"+image.getImageName()+".jpg" ;
						Log.d(" image path ", " path " + imagePath + " image name " + image.getImageName() + " ocr " + image.getImageDataOCR() + " path ocr "+ image.getImagePathOCR() + " txt " + image.getImagetxtOCR());
						if (image.getImagePathOCR() != null) {

							ImagesPath = image.getImagePathOCR();
							ImagetxtOcr =  String.valueOf(BiptagUserManger.getUserValue("updated_contact_edit", "string", NavigationActivity.getContext()));
							ImageDataOcr = image.getImageDataOCR();
							ocrResult=	ImageDataOcr;
							updatedContact=	buitlContact =new JSONObject( ImagetxtOcr);

							bitmapName = ImagesPath;
							//	ocrResult = buitlContact.getString("ocr");
							BiptagUserManger.saveUserData("ocrResultEdit",ocrResult,NavigationActivity.getContext());
							BiptagUserManger.saveUserData("updated_contact_edit", buitlContact.toString(), NavigationActivity.getContext());
							Log.d("ocrRecognation02 "," Mode " + Mode+ " isEditMode "+ isEditMode+" "+ bitmapName+" " + ocrResult+ " " + buitlContact);

						}
						Log.d("image json recognation2", "recognation img ID " + image.getImageId());

					}


				}
			} catch (JSONException e) {
				// TODO: handle exception
			}


		}else {
//LZR : 04/04/2016 scan en mode edition pour la premiere fois
			File path = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/Biptag");
			String[] fileNames;
			if (path.exists()) {
				fileNames = path.list();
				Log.d("path exists", fileNames.length + " ");
				bitmapName = String.valueOf(BiptagUserManger.getUserValue("bitmapName", "string", NavigationActivity.getContext()));
				ocrResult = String.valueOf(BiptagUserManger.getUserValue("ocrResultEdit", "string", NavigationActivity.getContext()));
				Log.d("ocrRecognation03 ", " Mode " + Mode + " isEditMode " + isEditMode + " " + bitmapName + " " + ocrResult);
				//	bitmap = BitmapFactory.decodeFile(bitmapName);
			}
			try {
				updatedContact = buitlContact = new JSONObject(String.valueOf(BiptagUserManger.getUserValue("updated_contact_edit", "string", NavigationActivity.getContext())));
				Log.d("ocrRecognation03 ", " Mode " + Mode + " isEditMode " + isEditMode + " " + buitlContact);


			} catch (JSONException e) {
				e.printStackTrace();
			}

		}
	}









	if (Mode==5){

//LZR : 04/04/2016 creation du contact

		if (String.valueOf(BiptagUserManger.getUserValue("ScanOCR_Creat","string",NavigationActivity.getContext())).indexOf("scanocr")!=-1){
//LZR : 04/04/2016 reprendre le scan  pres avoir scanner une fois et valider le scan)
					File path = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/Biptag");
			String[] fileNames;
			if (path.exists()) {
				fileNames = path.list();
				Log.d("path exists", fileNames.length + " ");
				bitmapName = String.valueOf(BiptagUserManger.getUserValue("bitmapName", "string", NavigationActivity.getContext()));
				ocrResult = String.valueOf(BiptagUserManger.getUserValue("ocrResultCreat", "string", NavigationActivity.getContext()));
				Log.d("ocrRecognation1 ", " Mode " + Mode + " isEditMode " + String.valueOf(BiptagUserManger.getUserValue("ScanOCR_Creat","string",NavigationActivity.getContext())) + " " + bitmapName + " " + ocrResult);
				//	bitmap = BitmapFactory.decodeFile(bitmapName);
			}
			try {
				updatedContact = buitlContact = new JSONObject(String.valueOf(BiptagUserManger.getUserValue("updated_contact_creat", "string", NavigationActivity.getContext())));
				Log.d("ocrRecognation1 ", " Mode " + Mode + " isEditMode " + isEditMode + " " + buitlContact);


			} catch (JSONException e) {
				e.printStackTrace();
			}

		}
		else if (String.valueOf(BiptagUserManger.getUserValue("EditscanOCR_Creat","string",NavigationActivity.getContext())).indexOf("editscan")!=-1){
//LZR : 04/04/2016 afficher l'ecran des tags apres avoir scanner et valider

			JSONArray	imagesJson ;
			JSONArray imageJsonTemp ;


			String imageJsonString = String.valueOf(BiptagUserManger.getUserValue("images_for_user", "string", NavigationActivity.getContext()));
			imagesJson = new JSONArray();




			try {
				imagesJson = new JSONArray(imageJsonString);
				Log.d(" image string received ", " image received " + imagesJson.toString() + " " + imageJsonString);

			} catch (JSONException e) {
				// TODO Auto-generated catch block

			}

			JSONArray copy = imagesJson;
			imageJsonTemp = imagesJson;
			int ba_id = Integer.parseInt(String.valueOf(BiptagUserManger.getUserValue("BA_ID", "int", NavigationActivity.getContext())));

			try {
				BTImageDataSource source = new BTImageDataSource(NavigationActivity.getContext());
				Log.d("image json recognation2", "recognation copy.length " + copy.length());

				if (copy.length()>0){

					int imgName = copy.getInt(0);
					Log.d("image json recognation2", "recognation search " + imgName);
					//BTImage image = source.selectImagesWithName(imgName, ba_id);
					BTImage image = source.selectImagesWithId(imgName, ba_id) ;


					if (image != null) {
						imageJsonTemp.put(image.getImageId());
						String imagePath = NavigationActivity.getContext().getCacheDir().getAbsolutePath()+"/"+image.getImageName()+".jpg" ;
						Log.d(" image path ", " path " + imagePath + " image name " + image.getImageName() + " ocr " + image.getImageDataOCR() + " path ocr "+ image.getImagePathOCR() + " txt " + image.getImagetxtOCR());
						if (image.getImagePathOCR() != null) {

							ImagesPath = image.getImagePathOCR();
							ImagetxtOcr =  String.valueOf(BiptagUserManger.getUserValue("updated_contact_creat", "string", NavigationActivity.getContext()));
							ImageDataOcr = image.getImageDataOCR();
							ocrResult=	ImageDataOcr;
							updatedContact=	buitlContact =new JSONObject( ImagetxtOcr);

							bitmapName = ImagesPath;
						//	ocrResult = buitlContact.getString("ocr");
							BiptagUserManger.saveUserData("ocrResultCreat",ocrResult,NavigationActivity.getContext());
							BiptagUserManger.saveUserData("updated_contact_creat", buitlContact.toString(), NavigationActivity.getContext());
							Log.d("ocrRecognation2 "," Mode " + Mode+ " isEditMode "+ isEditMode+" "+ bitmapName+" " + ocrResult+ " " + buitlContact);

						}
						Log.d("image json recognation2", "recognation img ID " + image.getImageId());

					}


				}
			} catch (JSONException e) {
				// TODO: handle exception
			}


		}else {
//LZR : 04/04/2016 scan pour la première fois
			File path = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/Biptag");
			String[] fileNames;
			if (path.exists()) {
				fileNames = path.list();
				Log.d("path exists", fileNames.length + " ");
				bitmapName = String.valueOf(BiptagUserManger.getUserValue("bitmapName", "string", NavigationActivity.getContext()));
				ocrResult = String.valueOf(BiptagUserManger.getUserValue("ocrResultCreat", "string", NavigationActivity.getContext()));
				Log.d("ocrRecognation3 ", " Mode " + Mode + " isEditMode " + isEditMode + " " + bitmapName + " " + ocrResult);
				//	bitmap = BitmapFactory.decodeFile(bitmapName);
			}
			try {
				updatedContact = buitlContact = new JSONObject(String.valueOf(BiptagUserManger.getUserValue("updated_contact_creat", "string", NavigationActivity.getContext())));
				Log.d("ocrRecognation3 ", " Mode " + Mode + " isEditMode " + isEditMode + " " + buitlContact);


			} catch (JSONException e) {
				e.printStackTrace();
			}

		}
	}

}

	//LZR : 04/04/2016 save the ocr when button " OK " is clicked
	public  void saveOcrBitmap(String ocr_result){
		//enregistrement du scan et de limage lors de l'appuie sur le bouton "Validate"
		BTImage image = new BTImage(NavigationActivity.getContext());

		JSONObject contactResult = getBuiltContact();
		int agent_id = Integer.parseInt(String.valueOf(BiptagUserManger.getUserValue("BA_ID", "int", NavigationActivity.getContext())));

		Calendar c = Calendar.getInstance();
		SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
		String formattedDate = df.format(c.getTime());
		Log.d(" save image ", " image data " + contactResult.toString() + " data ocr " +ocr_result);
		image.setAgentId(agent_id);
		image.setImageDate(formattedDate);
		image.setResponseId(0);
		image.setIsSent(-1);
		image.setImagetxtOcr(contactResult.toString());
		image.setImageDataOCR(ocr_result);
		image.setImagePath_OCR(String.valueOf(BiptagUserManger.getUserValue("bitmapName", "string", NavigationActivity.getContext())));
		BTImageDataSource dataSource = new BTImageDataSource(NavigationActivity.getContext());

		image.setBitmap(bitmap);
		Log.d("image ", "saving image " + image.getImageDataOCR());
		image.saveBitmap();
		Log.d("image ", "image saved");
		BTImageDataSource imageDataSource = new BTImageDataSource(NavigationActivity.getContext());
		Long imageIdcache = imageDataSource.saveImage(image);
		Log.d("image ", "image saved id " + imageIdcache);
		//String key = "";

		String vcardIdKey = String.valueOf(BiptagUserManger.getUserValue("vcardIdKey", "string", NavigationActivity.getContext()));

		int vcardImageId = Integer.parseInt(String.valueOf(BiptagUserManger.getUserValue(vcardIdKey, "int", NavigationActivity.getContext())));

		//key = String.valueOf(BiptagUserManger.getUserValue("key", "string", NavigationActivity.getContext()));

		Log.d("Image ", "Key " + key);

		BTImage dbImage = dataSource.selectImagesWithId(imageIdcache.intValue(), agent_id);
		Log.d(" image db ", " image db details " + dbImage.getImageDataOCR() + " " + dbImage.getImageName() + " " + dbImage.getAgentId());
		String imageJsonString = String.valueOf(BiptagUserManger.getUserValue(key, "string", NavigationActivity.getContext()));

		JSONArray imagesJson = null;
		int oldIndex = -1;
		try {
			if (imageJsonString.length() != 0) {
				imagesJson = new JSONArray(imageJsonString);
			} else {
				imagesJson = new JSONArray();
			}
		//	vcardImageId = (int) dataSource.saveImage(image);
			//  for
			for (int i = 0; i <= imagesJson.length(); i++) {
				Log.d("image ", " json " + imagesJson.getInt(i) + " vcard " + vcardImageId);
				if (vcardImageId == imagesJson.getInt(i)) {
					//en cas de refaire le scan replacer l'ancienne carte de visite avec la nouvelle
					oldIndex = i;
				}
			}

		} catch (JSONException e) {
			// TODO Auto-generated catch block

		}
		//

		if (dbImage == null)
			vcardImageId = (int) dataSource.saveImage(image);
		else {
			vcardImageId = dbImage.getImageId();
			image.setImageId((int) vcardImageId);
			boolean update = dataSource.updateImage(image);
			Log.d("bdimage", "bdimage upadte " + update);

		}

		try {
			if (oldIndex != -1) {

				imagesJson.put(oldIndex, vcardImageId);

			} else {
				imagesJson.put(vcardImageId);
			}

			Log.d("sendin image string", "sendin image string " + imagesJson.toString() + " key " + key);
			BiptagUserManger.saveUserData(key, imagesJson.toString(), NavigationActivity.getContext());
			BiptagUserManger.saveUserData(vcardIdKey, vcardImageId, NavigationActivity.getContext());

		} catch (JSONException e) {
			e.printStackTrace();
		}

	}
// LZR : 04/04/2016 load the ocr text
	public void loadproposition(){
		final Button ocr_paging_next = (Button)fragmentView.findViewById(R.id.ocr_paging_next_button);
		final Button ocr_paging_prev = (Button)fragmentView.findViewById(R.id.ocr_paging_prev_button);
		final TextView ocr_paging_indicator = (TextView)fragmentView.findViewById(R.id.ocr_paging_indicator_img_button);
		final int  x;
		if ((propositions.length/7) == 1|| (propositions.length/7) == 2){
			x=propositions.length/7;
			}else{
				x =(int) (propositions.length/7) +1 ;
			}
Log.d(" ocr length ", " " + x);
		if (x>1){
			ocr_paging_next.setBackgroundColor(Color.parseColor("#82e491"));
		}
		ocr_paging_indicator.setText("1/" + x);
		lodProposition(0, 7);

		for (int k=1;k<x;k++){
final  int i = k;


if((7*k)<propositions.length-1) {
	ocr_paging_next.setOnClickListener(new View.OnClickListener() {


		@Override
		public void onClick(View v) {
			ocr_paging_next.setBackgroundColor(Color.parseColor("#AFAFAF"));
			ocr_paging_prev.setBackgroundColor(Color.parseColor("#82e491"));
			ocrPropostionContainer.removeAllViews();
			lodProposition(7 * i, 7 * (i + 1));
			ocr_paging_indicator.setText(i + 1 + "/" + x);
		}
	});
	ocr_paging_prev.setOnClickListener(new View.OnClickListener() {


		@Override
		public void onClick(View v) {
			if (propositions != null) {
				ocr_paging_prev.setBackgroundColor(Color.parseColor("#AFAFAF"));
				ocr_paging_next.setBackgroundColor(Color.parseColor("#82e491"));
				ocrPropostionContainer.removeAllViews();
				lodProposition(7 * (i - 1), (7 * i));
				ocr_paging_indicator.setText(i + "/" + x);

			}
		}

	});


}
}

	}

	@Override
	public void onResume() {
		super.onResume();

		if (stack==1){
			ocrResult = String.valueOf(BiptagUserManger.getUserValue("ocrResultCreat", "string", NavigationActivity.getContext()));
			Log.d("path exists",  ocrResult);
			//	bitmap = BitmapFactory.decodeFile(bitmapName);
			if(BiptagUserManger.getUserValue("updated_contact_creat", "string", NavigationActivity.getContext())!=null) {

				try {
					updatedContact = new JSONObject(String.valueOf(BiptagUserManger.getUserValue("updated_contact_creat", "string", NavigationActivity.getContext())));
				} catch (JSONException e) {
					e.printStackTrace();
				}
				BiptagUserManger.saveUserData("updated_contact_creat", updatedContact.toString(), NavigationActivity.getContext());
				buitlContact = updatedContact;
				Log.d(" updated contact " , "isEditMode " + isEditMode + " Mode "+stack +updatedContact );
			}

			loadMatchedProposition();
		}else {
			ocrResult = String.valueOf(BiptagUserManger.getUserValue("ocrResultedit", "string", NavigationActivity.getContext()));
			Log.d("path exists",  ocrResult);
			//	bitmap = BitmapFactory.decodeFile(bitmapName);
		if(BiptagUserManger.getUserValue("updated_contact_edit", "string", NavigationActivity.getContext())!=null) {
			try {
				updatedContact = new JSONObject(String.valueOf(BiptagUserManger.getUserValue("updated_contact_edit", "string", NavigationActivity.getContext())));
			} catch (JSONException e) {
				e.printStackTrace();
			}
			Log.d(" updated contact " , "isEditMode " + isEditMode + " Mode "+stack +updatedContact );
			loadMatchedProposition();
		}
		}

	}




	@Override
	public void onStop() {
		super.onStop();

	}

	public void lodProposition(int k, int l){






						LayoutInflater inflater = LayoutInflater.from(NavigationActivity.getContext()) ;

						for (int i =k; i<propositions.length && i<l ; i++) {

					final LinearLayout element = (LinearLayout) inflater.inflate(R.layout.ocr_proposition_item, null);


					LayoutParams params = new LayoutParams(LayoutParams.MATCH_PARENT, 50);
					params.setMargins(0, screenHeight / 150, 0, screenHeight / 150);

					element.setLayoutParams(params);


					TextView value = (TextView) element.findViewById(R.id.value_txt);
					value.setText(propositions[i]);
							value.setTextSize(10);
							Log.d("text ","proposition " + value.getText());

					element.setOnTouchListener(new View.OnTouchListener() {

						@Override
						public boolean onTouch(View v, MotionEvent event) {
							// TODO Auto-generated method stub


							if (event.getAction() == MotionEvent.ACTION_DOWN) {
								ClipData data = ClipData.newPlainText("", "");
								DragShadowBuilder dragShadowBuilder = new View.DragShadowBuilder(element);
								element.startDrag(data, dragShadowBuilder, element, 0);
								//element.setVisibility(View.INVISIBLE) ;
								return true;
							} else {
								return false;
							}


						}
					});

					ocrPropostionContainer.addView(element);


				}
						// TODO Auto-generated method stub


	}



	public JSONObject getBuiltContact(){
		int[] ids = { R.id.ocr_element_fname , R.id.ocr_element_lname , R.id.ocr_element_email , R.id.ocr_element_company , R.id.ocr_element_title , R.id.ocr_element_address , R.id.ocr_element_codezip , R.id.ocr_element_city , R.id.ocr_element_country , R.id.ocr_element_phone } ;
		int[] idDropContainers = { R.id.ocr_fname_value_container, R.id.ocr_lname_value_container, R.id.ocr_email_value_container, R.id.ocr_company_value_container, R.id.ocr_title_value_container, R.id.ocr_address_value_container, R.id.ocr_codezip_value_container, R.id.ocr_city_value_container, R.id.ocr_country_value_container, R.id.ocr_phone_value_container} ;
		JSONObject object = new JSONObject() ;



	for (int i = 0; i < 10; i++) {

				LinearLayout layout = (LinearLayout) fragmentView.findViewById(ids[i]) ;
				final LinearLayout valueContainer  = (LinearLayout) layout.findViewById(idDropContainers[i]) ;
				String key = "" ;

				switch (i) {
				case 0:
					key = "LNAME" ;
					break;
				case 1:
					key = "FNAME" ;
					break;
				case 2:
					key = "EMAIL" ;
					break;
				case 3:
					key = "COMPANY" ;
					break;
				case 4:
					key = "TITLE" ;
					break;
				case 5:
					key = "ADDRESS" ;
					break;
				case 6:
					key = "ZIPCODE" ;
					break;
				case 7:
					key = "CITY" ;
					break;
				case 8:
					key = "COUNTRY" ;
					break;
				case 9:
					key = "PHONE" ;
					break;

				default:
					break;
				}









			try {


				TextView view = (TextView) valueContainer.findViewById(R.id.value_txt) ;
					if(view != null){
						if(view.getText().toString().length() != 0){
							object.put(key, view.getText().toString()) ;

						}else{
							object.put(key, "") ;

						}
					}else{
						object.put(key, "") ;

					}

			} catch (JSONException e) {
				// TODO: handle exception
				//Log.d("contact chosen exception ", "contact chosen exception "+e.getMessage()) ;
			}


		}


		return object ;


	}


//LZR : 14/04/2016 set the data of contact after matching
public void loadMatchedProposition(){
		int[] ids = { R.id.ocr_element_fname , R.id.ocr_element_lname , R.id.ocr_element_email , R.id.ocr_element_company , R.id.ocr_element_title , R.id.ocr_element_address , R.id.ocr_element_codezip , R.id.ocr_element_city , R.id.ocr_element_country , R.id.ocr_element_phone } ;
		int[] idDropContainers = { R.id.ocr_fname_value_container, R.id.ocr_lname_value_container, R.id.ocr_email_value_container, R.id.ocr_company_value_container, R.id.ocr_title_value_container, R.id.ocr_address_value_container, R.id.ocr_codezip_value_container, R.id.ocr_city_value_container, R.id.ocr_country_value_container, R.id.ocr_phone_value_container} ;


	Log.d("value loadmatched ", updatedContact.toString());

	for (int i = 0; i < 10; i++) {

				LinearLayout layout = (LinearLayout) fragmentView.findViewById(ids[i]) ;
				final LinearLayout valueContainer  = (LinearLayout) layout.findViewById(idDropContainers[i]) ;
				String key = "" ;
				String key_prop = "" ;

				switch (i) {
				case 0:
					key = "LNAME" ;
					key_prop = NavigationActivity.getContext().getResources().getString(R.string.contact_lastname) ;

					break;
				case 1:
					key = "FNAME" ;
					key_prop = NavigationActivity.getContext().getResources().getString(R.string.contact_firstname_l) ;
					break;
				case 2:
					key = "EMAIL" ;
					key_prop = "Email" ;
					break;
				case 3:
					key = "COMPANY" ;
					key_prop = NavigationActivity.getContext().getResources().getString(R.string.contact_company_l) ;
					break;
				case 4:
					key = "TITLE" ;
					key_prop = NavigationActivity.getContext().getResources().getString(R.string.contact_position) ;
					break;
				case 5:
					key = "ADDRESS" ;
					key_prop = NavigationActivity.getContext().getResources().getString(R.string.contact_address) ;
					break;
				case 6:
					key = "ZIPCODE" ;
					key_prop = NavigationActivity.getContext().getResources().getString(R.string.contact_zip_code) ;
					break;
				case 7:
					key = "CITY" ;
					key_prop =  NavigationActivity.getContext().getResources().getString(R.string.contact_city) ;
					break;
				case 8:
					key = "COUNTRY" ;
					key_prop = NavigationActivity.getContext().getResources().getString(R.string.contact_country) ;
					break;
				case 9:
					key = "PHONE" ;
					key_prop = NavigationActivity.getContext().getResources().getString(R.string.contact_phone_l) ;
					break;

				default:
					break;
				}









			try {
					String valueForKey = updatedContact.getString(key) ;
					if(valueForKey.length() != 0){
						LayoutInflater inflater = LayoutInflater.from(NavigationActivity.getContext()) ;
						final LinearLayout element  = (LinearLayout) inflater.inflate(R.layout.ocr_proposition_item, null) ;
						final TextView value = (TextView) element.findViewById(R.id.value_txt);


						value.setText(valueForKey);
						value.setHeight(R.dimen.next_prev_height_button);
						value.setTextSize(10);
						Log.d(" value forkey ", valueForKey + " " + i + " " + value.getText().toString() + " key " + key +  " keyprop " + key_prop);

						final String finalKey_prop = key_prop;
						value.setOnTouchListener(new View.OnTouchListener() {

							@Override
							public boolean onTouch(View v, MotionEvent event) {
								// TODO Auto-generated method stub

								if (event.getAction() == MotionEvent.ACTION_DOWN) {
//focus lors du drag and drop ou matching
									getTextColor(value.getText().toString());

									AlertDialog.Builder builder = new AlertDialog.Builder(NavigationActivity.getContext());
									builder.setCancelable(false);
									builder.setTitle(NavigationActivity.getContext().getResources().getString(R.string.edit_champ_title));
									builder.setMessage(NavigationActivity.getContext().getResources().getString(R.string.edit_champ_desc) + " " + finalKey_prop);
									final BTEditText editText = new BTEditText(NavigationActivity.getContext());
									editText.setText(value.getText().toString());
									Log.d("etext", editText.getText() + " " + value.getText().length());
									editText.setSelection(value.getText().length());
									InputMethodManager imm = (InputMethodManager) NavigationActivity.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
									imm.showSoftInput(editText, InputMethodManager.SHOW_IMPLICIT);
									builder.setView(editText);
									if (finalKey_prop.indexOf(NavigationActivity.getContext().getResources().getString(R.string.contact_phone_l).toString()) != -1 || finalKey_prop.indexOf(NavigationActivity.getContext().getResources().getString(R.string.contact_zip_code).toString()) != -1) {
										editText.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_TEXT_VARIATION_PHONETIC|InputType.TYPE_CLASS_TEXT);
										editText.setRawInputType(Configuration.KEYBOARD_12KEY);
										editText.setFreezesText(true) ;

									} else if (finalKey_prop.indexOf(NavigationActivity.getContext().getResources().getString(R.string.contact_email_l).toString()) != -1 ) {
										editText.setInputType( InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS | InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS );
									}else {
										editText.setInputType(InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);

									}
										builder.setPositiveButton("Ok", new OnClickListener() {

											@Override
											public void onClick(DialogInterface dialog, int which) {
												// TODO Auto-generated method stub
												//getTextColor(value.getText().toString());

												if (editText.getText().toString().length() != 0) {
													value.setText(editText.getText().toString());
													element.setBackgroundColor(Color.parseColor("#82e491"));

												}
												if (stack == 1) {
													BiptagUserManger.saveUserData("updated_contact_creat", getBuiltContact().toString(), NavigationActivity.getContext());
												} else {
													BiptagUserManger.saveUserData("updated_contact_edit", getBuiltContact().toString(), NavigationActivity.getContext());

												}
												Log.d("value 2.01 ", value.getText().toString() + " contact " + getBuiltContact().toString());

												dialog.dismiss();

											}
										});

										builder.setNegativeButton(NavigationActivity.getContext().getResources().getString(R.string.cancel), new OnClickListener() {

											@Override
											public void onClick(DialogInterface dialog, int which) {
												// TODO Auto-generated method stub
												dialog.dismiss();
											}
										});
										builder.show();
										if (stack == 1) {
											BiptagUserManger.saveUserData("updated_contact_creat", getBuiltContact().toString(), NavigationActivity.getContext());
										} else {
											BiptagUserManger.saveUserData("updated_contact_edit", getBuiltContact().toString(), NavigationActivity.getContext());

										}

									}
					return false;
								}
							}

							);

							element.setBackgroundColor(Color.parseColor("#82e491"));
							element.setVisibility(View.VISIBLE);
									final ImageButton removeButton = (ImageButton) element.findViewById(R.id.remove_btn);
							removeButton.setVisibility(View.VISIBLE);

							removeButton.setOnClickListener(new View.OnClickListener()

							{

								@Override
								public void onClick (View v){
								// TODO Auto-generated method stub
							//	valueContainer.removeAllViews();
									value.setText("");
									element.setBackgroundColor(Color.WHITE);
									removeButton.setVisibility(View.INVISIBLE);
									if (stack == 1) {
									BiptagUserManger.saveUserData("updated_contact_creat", getBuiltContact().toString(), NavigationActivity.getContext());
								} else {
									BiptagUserManger.saveUserData("updated_contact_edit", getBuiltContact().toString(), NavigationActivity.getContext());

								}

								Log.d("value 2.3", value.getText().toString() + " contact " + getBuiltContact().toString());
								//Log.d("value 2.3 ", value.getText().toString() + " contact " + BiptagUserManger.getUserValue("updated_contact", "string", NavigationActivity.getContext()));

							}
							}

							);

							valueContainer.removeAllViews();
							valueContainer.addView(element);

						}else {
						LayoutInflater inflater = LayoutInflater.from(NavigationActivity.getContext()) ;
						final LinearLayout element  = (LinearLayout) inflater.inflate(R.layout.ocr_proposition_item, null) ;
						final TextView value = (TextView) element.findViewById(R.id.value_txt);

						final String[] valueEdited = {""};
						value.setText("");
						value.setText(valueForKey);
						value.setHeight(R.dimen.next_prev_height_button);
						value.setTextSize(10);
						final ImageButton removeButton = (ImageButton) element.findViewById(R.id.remove_btn);

						Log.d(" value forkey ", valueForKey + " " + i + " " + value.getText().toString());

						final String finalKey_prop = key_prop;
						value.setOnTouchListener(new View.OnTouchListener() {

							@Override
							public boolean onTouch(View v, MotionEvent event) {
								// TODO Auto-generated method stub

								if (event.getAction() == MotionEvent.ACTION_DOWN) {
									getTextColor(value.getText().toString());

									AlertDialog.Builder builder = new AlertDialog.Builder(NavigationActivity.getContext());
									builder.setCancelable(false);
									builder.setTitle(NavigationActivity.getContext().getResources().getString(R.string.edit_champ_title));
									builder.setMessage(NavigationActivity.getContext().getResources().getString(R.string.edit_champ_desc) + " " + finalKey_prop);
									final BTEditText editText = new BTEditText(NavigationActivity.getContext());
									editText.setText(value.getText().toString());
									Log.d("etext", editText.getText() + " " + value.getText().length());
									editText.setSelection(value.getText().length());
									InputMethodManager imm = (InputMethodManager) NavigationActivity.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
									imm.showSoftInput(editText, InputMethodManager.SHOW_IMPLICIT);
									builder.setView(editText);
									if (finalKey_prop.indexOf(NavigationActivity.getContext().getResources().getString(R.string.contact_phone_l).toString()) != -1 || finalKey_prop.indexOf(NavigationActivity.getContext().getResources().getString(R.string.contact_zip_code).toString()) != -1) {
										editText.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_TEXT_VARIATION_PHONETIC | InputType.TYPE_CLASS_TEXT);
										editText.setRawInputType(Configuration.KEYBOARD_12KEY);
										editText.setFreezesText(true);
									} else if (finalKey_prop.indexOf(NavigationActivity.getContext().getResources().getString(R.string.contact_email_l).toString()) != -1) {
										editText.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS | InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
										;
									} else {
										editText.setInputType(InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);

									}
									builder.setPositiveButton("Ok", new OnClickListener() {

										@Override
										public void onClick(DialogInterface dialog, int which) {
											// TODO Auto-generated method stub
								//			getTextColor(value.getText().toString());

											if (editText.getText().toString().length() != 0) {
												value.setText(editText.getText().toString());
												valueEdited[0] = editText.getText().toString();
												element.setBackgroundColor(Color.parseColor("#82e491"));
												element.setVisibility(View.VISIBLE);
												removeButton.setVisibility(View.VISIBLE);

											}
											if (stack == 1) {
												BiptagUserManger.saveUserData("updated_contact_creat", getBuiltContact().toString(), NavigationActivity.getContext());
											} else {
												BiptagUserManger.saveUserData("updated_contact_edit", getBuiltContact().toString(), NavigationActivity.getContext());

											}
											Log.d("value 2.01 ", value.getText().toString() + " contact " + updatedContact.toString());

											dialog.dismiss();

										}
									});

									builder.setNegativeButton(NavigationActivity.getContext().getResources().getString(R.string.cancel), new OnClickListener() {

										@Override
										public void onClick(DialogInterface dialog, int which) {
											// TODO Auto-generated method stub
											dialog.dismiss();
										}
									});
									builder.show();
									if (stack == 1) {
										BiptagUserManger.saveUserData("updated_contact_creat", getBuiltContact().toString(), NavigationActivity.getContext());
									} else {
										BiptagUserManger.saveUserData("updated_contact_edit", getBuiltContact().toString(), NavigationActivity.getContext());

									}
									imm.hideSoftInputFromWindow(editText.getWindowToken(), 0);

								}

								return false;
							}
						}) ;
						if(valueEdited[0].length()>1) {
							element.setBackgroundColor(Color.parseColor("#82e491"));
							element.setVisibility(View.VISIBLE);
						}else {
							//element.setBackgroundColor(Color.parseColor("#82e491")) ;
							element.setBackgroundColor(Color.WHITE);
						}

						removeButton.setOnClickListener(new View.OnClickListener() {

							@Override
							public void onClick(View v) {
								// TODO Auto-generated method stub
								//valueContainer.removeAllViews();
								value.setText("");
								element.setBackgroundColor(Color.WHITE);
								removeButton.setVisibility(View.INVISIBLE);
								if (stack == 1) {
									BiptagUserManger.saveUserData("updated_contact_creat", getBuiltContact().toString(), NavigationActivity.getContext());
								} else {
									BiptagUserManger.saveUserData("updated_contact_edit", getBuiltContact().toString(), NavigationActivity.getContext());

								}
								Log.d("value 2.3", value.getText().toString() + " contact " + getBuiltContact().toString());
								//Log.d("value 2.3 ", value.getText().toString() + " contact " + BiptagUserManger.getUserValue("updated_contact", "string", NavigationActivity.getContext()));

							}
						}) ;

						valueContainer.removeAllViews();
						valueContainer.addView(element) ;

					}


			} catch (JSONException e) {
				// TODO: handle exception
				//Log.d("contact chosen exception ", "contact chosen exception "+e.getMessage()) ;
			}


		}


	}




	public void fixLayout(){
		int[] ids = { R.id.ocr_element_fname , R.id.ocr_element_lname , R.id.ocr_element_email , R.id.ocr_element_company , R.id.ocr_element_title , R.id.ocr_element_address , R.id.ocr_element_codezip , R.id.ocr_element_city , R.id.ocr_element_country , R.id.ocr_element_phone } ;
		int[] idDropContainers = { R.id.ocr_fname_value_container, R.id.ocr_lname_value_container, R.id.ocr_email_value_container, R.id.ocr_company_value_container, R.id.ocr_title_value_container, R.id.ocr_address_value_container, R.id.ocr_codezip_value_container, R.id.ocr_city_value_container, R.id.ocr_country_value_container, R.id.ocr_phone_value_container} ;

		for (int i = 0; i < ids.length; i++) {
			LinearLayout layout = (LinearLayout) fragmentView.findViewById(ids[i]) ;
			LayoutParams params = new LayoutParams(LayoutParams.MATCH_PARENT, screenHeight/ 25) ;
			params.setMargins(screenWidth/100, screenHeight/100, screenWidth/100, screenHeight/100) ;
			layout.setLayoutParams(params) ;



			LinearLayout valueContainer = (LinearLayout) layout.findViewById(idDropContainers[i]) ;
			valueContainer.setBackgroundColor(Color.WHITE) ;
			String key = "" ;
			String key_prop = "" ;

			switch (i) {
				case 0:
					key = "FNAME" ;
					key_prop = NavigationActivity.getContext().getResources().getString(R.string.contact_lastname) ;

					break;
				case 1:
					key = "LNAME" ;
					key_prop = NavigationActivity.getContext().getResources().getString(R.string.contact_firstname_l) ;
					break;
				case 2:
					key = "EMAIL" ;
					key_prop = "Email" ;
					break;
				case 3:
					key = "COMPANY" ;
					key_prop = NavigationActivity.getContext().getResources().getString(R.string.contact_company_l) ;
					break;
				case 4:
					key = "TITLE" ;
					key_prop = NavigationActivity.getContext().getResources().getString(R.string.contact_position) ;
					break;
				case 5:
					key = "ADDRESS" ;
					key_prop = NavigationActivity.getContext().getResources().getString(R.string.contact_address) ;
					break;
				case 6:
					key = "ZIPCODE" ;
					key_prop = NavigationActivity.getContext().getResources().getString(R.string.contact_zip_code) ;
					break;
				case 7:
					key = "CITY" ;
					key_prop =  NavigationActivity.getContext().getResources().getString(R.string.contact_city) ;
					break;
				case 8:
					key = "COUNTRY" ;
					key_prop = NavigationActivity.getContext().getResources().getString(R.string.contact_country) ;
					break;
				case 9:
					key = "PHONE" ;
					key_prop = NavigationActivity.getContext().getResources().getString(R.string.contact_phone_l) ;
					break;

				default:
					break;
			}


			final String finalKey_prop = key_prop;
			valueContainer.setOnDragListener(new OnDragListener() {

				@Override
				public boolean onDrag(View v, DragEvent event) {
					// TODO Auto-generated method stub

					//Log.d("drag action", "drag action "+event.getAction()) ;
					switch (event.getAction()) {
					case DragEvent.ACTION_DRAG_STARTED:
						//Log.d("drag", "drag action started") ;
						break;
					case DragEvent.ACTION_DRAG_ENTERED :
					{

						//Log.d("drag", "drag action enter") ;
					}
						break ;
					case DragEvent.ACTION_DRAG_EXITED :{
					//	Log.d("drag", "drag action exited") ;
					}
						break ;
					case DragEvent.ACTION_DROP :
					{
						View view = (View) event.getLocalState() ;
						//ViewGroup parent =(ViewGroup) view.getParent() ;

						LayoutInflater inflater = LayoutInflater.from(NavigationActivity.getContext()) ;
						final LinearLayout element  = (LinearLayout) inflater.inflate(R.layout.ocr_proposition_item, null) ;
						final TextView value = (TextView) element.findViewById(R.id.value_txt);
						TextView oldValue = (TextView) view.findViewById(R.id.value_txt);
						final ImageButton removeButton = (ImageButton) element.findViewById(R.id.remove_btn);
						removeButton.setVisibility(View.VISIBLE);

						value.setText(oldValue.getText().toString());
						value.setHeight(R.dimen.next_prev_height_button);
						value.setTextSize(10);

						Log.d(" value 1.0", oldValue.getText().toString() + " new text " + value.getText().toString());
						value.setOnTouchListener(new View.OnTouchListener() {

							@Override
							public boolean onTouch(View v, MotionEvent event) {
								// TODO Auto-generated method stub

								if (event.getAction() == MotionEvent.ACTION_DOWN) {
									getTextColor(value.getText().toString());

									AlertDialog.Builder builder = new AlertDialog.Builder(NavigationActivity.getContext());
									builder.setCancelable(false);
									builder.setTitle(NavigationActivity.getContext().getResources().getString(R.string.edit_champ_title));
									builder.setMessage(NavigationActivity.getContext().getResources().getString(R.string.edit_champ_desc) + " " + finalKey_prop);
									final BTEditText editText = new BTEditText(NavigationActivity.getContext());
									editText.setText(value.getText().toString());
									Log.d("etext", editText.getText() + " " + value.getText().length());
									editText.setSelection(value.getText().length());
									if (finalKey_prop.indexOf(NavigationActivity.getContext().getResources().getString(R.string.contact_phone_l).toString()) != -1 || finalKey_prop.indexOf(NavigationActivity.getContext().getResources().getString(R.string.contact_zip_code).toString()) != -1) {
										editText.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_TEXT_VARIATION_PHONETIC|InputType.TYPE_CLASS_TEXT);
									//	editText.setRawInputType(DialerFilter.DIGITS_AND_LETTERS_NO_LETTERS);
										editText.setRawInputType(Configuration.KEYBOARD_12KEY);

										editText.setFreezesText(true) ;
									} else if (finalKey_prop.indexOf(NavigationActivity.getContext().getResources().getString(R.string.contact_email_l).toString()) != -1 ) {
										editText.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS | InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS); ;

									}else {
										editText.setInputType(InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);

									}



									builder.setView(editText);
									builder.setPositiveButton("Ok", new OnClickListener() {

										@Override
										public void onClick(DialogInterface dialog, int which) {
											// TODO Auto-generated method stub
									//		getTextColor(value.getText().toString());

											InputMethodManager imm = (InputMethodManager) NavigationActivity.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
											imm.showSoftInput(editText, InputMethodManager.SHOW_IMPLICIT);

											if (editText.getText().toString().length() != 0) {
												value.setText(editText.getText().toString());
												element.setBackgroundColor(Color.parseColor("#82e491"));

												removeButton.setVisibility(View.VISIBLE);

												if (stack == 1) {
													BiptagUserManger.saveUserData("updated_contact_creat", getBuiltContact().toString(), NavigationActivity.getContext());
												} else {
													BiptagUserManger.saveUserData("updated_contact_edit", getBuiltContact().toString(), NavigationActivity.getContext());

												}
												Log.d("value 1.01", value.getText().toString() + " contact " + getBuiltContact().toString());
												//Log.d("value 1.01 ", value.getText().toString() + " contact " + BiptagUserManger.getUserValue("updated_contact", "string", NavigationActivity.getContext()));

											}
										}
//									});
//											dialog.dismiss();

										//}
									});

									builder.setNegativeButton("Annuler", new OnClickListener() {

										@Override
										public void onClick(DialogInterface dialog, int which) {
											// TODO Auto-generated method stub
											if (stack == 1) {
												BiptagUserManger.saveUserData("updated_contact_creat", getBuiltContact().toString(), NavigationActivity.getContext());
											} else {
												BiptagUserManger.saveUserData("updated_contact_edit", getBuiltContact().toString(), NavigationActivity.getContext());

											}
											Log.d("value 1.02", value.getText().toString() + " contact " + getBuiltContact().toString());
											//Log.d("value 1.02 ", value.getText().toString() + " contact " + BiptagUserManger.getUserValue("updated_contact", "string", NavigationActivity.getContext()));

											dialog.dismiss();

										}
									});
									builder.show();

								}


								return false;
							}
						});
						if (stack==1) {
							BiptagUserManger.saveUserData("updated_contact_creat", getBuiltContact().toString(), NavigationActivity.getContext());
						}else {
							BiptagUserManger.saveUserData("updated_contact_edit", getBuiltContact().toString(), NavigationActivity.getContext());

						}
						Log.d("value 1.1", value.getText().toString() + " contact " + getBuiltContact().toString());
						//Log.d("value 1.1 ", value.getText().toString() + " contact " + BiptagUserManger.getUserValue("updated_contact", "string", NavigationActivity.getContext()));


						final LinearLayout container = (LinearLayout) v ;

						element.setBackgroundColor(Color.parseColor("#82e491")) ;
						element.setVisibility(View.VISIBLE);
						removeButton.setOnClickListener(new View.OnClickListener() {

							@Override
							public void onClick(View v) {
								// TODO Auto-generated method stub
								//container.removeAllViews();
								value.setText("");
								element.setBackgroundColor(Color.WHITE);
								removeButton.setVisibility(View.INVISIBLE);

								if (stack == 1) {
									BiptagUserManger.saveUserData("updated_contact_creat", getBuiltContact().toString(), NavigationActivity.getContext());
								} else {
									BiptagUserManger.saveUserData("updated_contact_edit", getBuiltContact().toString(), NavigationActivity.getContext());

								}
								Log.d("value 1.2", value.getText().toString() + " contact " + getBuiltContact().toString());
								//Log.d("value 1.2 ", value.getText().toString() + " contact " + BiptagUserManger.getUserValue("updated_contact", "string", NavigationActivity.getContext()));

							}
						}) ;

						container.removeAllViews() ;
						container.addView(element);
						view.setVisibility(view.VISIBLE);
						if (stack==1) {
							BiptagUserManger.saveUserData("updated_contact_creat", getBuiltContact().toString(), NavigationActivity.getContext());
						}else {
							BiptagUserManger.saveUserData("updated_contact_edit", getBuiltContact().toString(), NavigationActivity.getContext());

						}
						//Log.d("value 1.3", value.getText().toString() + " contact " + getBuiltContact().toString());
						//Log.d("value 1.3 ", value.getText().toString() + " contact " + BiptagUserManger.getUserValue("updated_contact", "string", NavigationActivity.getContext()));


					}
						break ;

					default:
						break;
					}


					return true;
				}
			}) ;



		}

	}

	private String CopyAssets(String name , String targetDirectory) {
		AssetManager assetManager = NavigationActivity.getContext().getResources().getAssets();
		//Log.d("tessdata", "tessdata files enter copy") ;
		String[] files = null;

		try {
			files = assetManager.list("tesseract-ocr-3.02.eng/tesseract-ocr/tessdata"); //tessdata is folder name
			Log.d("API ", "tesseract from tessocr");
		} catch (Exception e) {
			//Log.e("", "tessdata ERROR: " + e.toString());
		}

		File tessDataDirectory = new File(NavigationActivity.getContext().getCacheDir().getAbsolutePath()+"/tessdata") ;
		tessDataDirectory.mkdir() ;


//Log.d("tessdata", "tessdata files size"+files.length) ;

		for (int i = 0; i < files.length; i++) {
			InputStream in = null;
			OutputStream out = null;
			try {
				in = assetManager.open("tesseract-ocr-3.02.eng/tesseract-ocr/tessdata/" + files[i]);
				Log.d("API ", "tesseract in tessocr");
				out = new FileOutputStream(tessDataDirectory.getAbsolutePath()+"/" + files[i]);
				byte[] buffer = new byte[65536 * 2];
				int read;
				while ((read = in.read(buffer)) != -1) {
					out.write(buffer, 0, read);
				}
				in.close();
				in = null;
				out.flush();
				out.close();
				out = null;
				//Log.d("", "tessdata File "+files[i]+" Copied in "+tessDataDirectory.getAbsolutePath()+"/" + files[i]);
			} catch (Exception e) {
				// Log.e("", "ERROR: " + e.toString());
			}
		}

		return NavigationActivity.getContext().getCacheDir().getAbsolutePath() ;


	}





//LZR : 14/04/2016 set the focus of bitmap
public void getTextColor(String value){
	System.gc();
	final ResultIterator iterator = baseApi.getResultIterator();
	String lastUTF8Text;
	float lastConfidence;
	int[] lastBoundingBox = null;
	paint.reset();
	//verifier si le text en entré existe dans le bitmap scanné
	iterator.begin();
Log.d(" begin ", " begin ocr get text color " + iterator.getUTF8Text(TessBaseAPI.PageIteratorLevel.RIL_PARA));
	do {

		lastUTF8Text = iterator.getUTF8Text(TessBaseAPI.PageIteratorLevel.RIL_TEXTLINE);
		Log.d(" begin ", " begin ocr get text color do " + lastUTF8Text);

		//lastConfidence = iterator.confidence(TessBaseAPI.PageIteratorLevel.RIL_WORD);
	//	lastBoundingBox = iterator.getBoundingBox(TessBaseAPI.PageIteratorLevel.RIL_TEXTLINE);
		if(iterator.getUTF8Text(TessBaseAPI.PageIteratorLevel.RIL_TEXTLINE).indexOf(value)!=-1) {
			lastBoundingBox = iterator.getBoundingBox(TessBaseAPI.PageIteratorLevel.RIL_TEXTLINE);
			Log.d(" text color ", " existe " + value);
			break;
		}else{
			Log.d(" text color ", " doesn't existe " +iterator.getUTF8Text(TessBaseAPI.PageIteratorLevel.RIL_WORD)+" value " + value );

		}


	} while (iterator.next(TessBaseAPI.PageIteratorLevel.RIL_TEXTLINE) );

	bitmapInter = bitmap.copy(Bitmap.Config.RGB_565, true);
	Canvas canvas = new Canvas(bitmapInter);
	canvas.drawColor(0);
	// ajouter le cadrage pour chaque mot
	if (lastBoundingBox!=null) {
		for (int i = 0; i < lastBoundingBox.length; i++) {

			paint.setColor(Color.RED);
			paint.setStyle(Paint.Style.STROKE);
			paint.setStrokeWidth(2);


			Rect r = new Rect(lastBoundingBox[0], lastBoundingBox[1],
					lastBoundingBox[2], lastBoundingBox[3]);
			canvas.drawRect(r, paint);

		}
	}
//affichage du bitmap avec le cadrage
	if (bitmapInter!=null) {
		imageView.setImageBitmap(bitmapInter);
	}else
	{
		AlertDialog.Builder builder = new AlertDialog.Builder(NavigationActivity.getContext());
		builder.setTitle(NavigationActivity.getContext().getResources().getString(R.string.image_deleted_title));
		builder.setMessage(NavigationActivity.getContext().getResources().getString(R.string.image_deleted_dsc));
		builder.setCancelable(false);
		builder.setPositiveButton("OK", new OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				BiptagUserManger.clearField(NavigationActivity.getContext(), "ocrResult");
				BiptagUserManger.clearField(NavigationActivity.getContext(), "ocrOrigin");

			//	if (Mode == 5) {
					BiptagUserManger.clearField(NavigationActivity.context, "ScanOCR_Creat");
					BiptagNavigationManager.pushFragments(EditOCRFragment.this, false, EditOCRFragment.class.getName(), NavigationActivity.getContext(), null, stack);
			//	}
			//	if (Mode == 6) {
					BiptagUserManger.clearField(NavigationActivity.context, "ScanOCR_Edit");
			//		BiptagNavigationManager.pushFragments(EditOCRFragment.this, false, EditOCRFragment.class.getName(), NavigationActivity.getContext(), null, stack);

			//	}

			}
		});


	}}
//LZR : 14/04/2016 function of zood dezoom
	@Override
		public boolean onTouch(View v, MotionEvent event)
		{

				ImageView view = (ImageView) v;
			view.setScaleType(ImageView.ScaleType.MATRIX);
			float scale;

			switch (event.getAction() & MotionEvent.ACTION_MASK)
			{
				case MotionEvent.ACTION_DOWN:   // first finger down only
					savedMatrix.set(matrix);
					start.set(event.getX(), event.getY());
					mode = DRAG;
					break;

				case MotionEvent.ACTION_UP: // first finger lifted

				case MotionEvent.ACTION_POINTER_UP: // second finger lifted

					mode = NONE;
					break;

				case MotionEvent.ACTION_POINTER_DOWN: // first and second finger down

					oldDist = spacing(event);
					if (oldDist > 5f) {
						savedMatrix.set(matrix);
						midPoint(mid, event);
						mode = ZOOM;
					}
					break;

				case MotionEvent.ACTION_MOVE:
				//	HorizontalScrollView horizontalScrollView = (HorizontalScrollView) fragmentView.findViewById(R.id.horizontalScrollView1);

					if (mode == DRAG)
					{
						matrix.set(savedMatrix);
						matrix.postTranslate(event.getX() - start.x, event.getY() - start.y); // create the transformation in the matrix  of points
					//	Log.d(" drag ocr frame ", frameLayout.getWidth() + " height " + frameLayout.getHeight() + " SX " + frameLayout.getScrollX() + " SY " + frameLayout.getScrollY() + " Y " + frameLayout.getX() + " Y " + frameLayout.getY());
						Log.d(" drag ocr ",event.getX()+ " - "+ start.x+" = "+(event.getX() - start.x) +" y : "+event.getY()+" - "+ start.y+" = "+ (event.getY() - start.y) );

								}
					else if (mode == ZOOM)
					{
						// pinch zooming
						float newDist = spacing(event);
						if (newDist > 5f)
						{
					//		horizontalScrollView.setMinimumHeight((int)newDist);
							matrix.set(savedMatrix);
							scale = newDist / oldDist; // setting the scaling of the
							// matrix...if scale > 1 means
							// zoom in...if scale < 1 means
							// zoom out
							matrix.postScale(scale, scale, mid.x, mid.y);
						}
					}
					break;
			}

			view.setImageMatrix(matrix); // display the transformation on screen

			return true; // indicate event was handled
		}


    // verifier la distance entre deux doigts


		private float spacing(MotionEvent event)
		{
			float x = event.getX(0) - event.getX(1);
			float y = event.getY(0) - event.getY(1);
			return FloatMath.sqrt(x * x + y * y);
		}


    //  Description: calculates the midpoint between the two fingers


		private void midPoint(PointF point, MotionEvent event)
		{
			float x = event.getX(0) + event.getX(1);
			float y = event.getY(0) + event.getY(1);
			point.set(x / 2, y / 2);
		}

	}
