package com.biptag.biptag.fragements.create;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.drawable.GradientDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.provider.ContactsContract;
import android.provider.ContactsContract.CommonDataKinds;
import android.provider.ContactsContract.CommonDataKinds.StructuredPostal;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.InputType;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.View.OnFocusChangeListener;
import android.view.View.OnKeyListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.RadioButton;
import android.widget.TableLayout;
import android.widget.TableLayout.LayoutParams;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import android.widget.Toast;

import com.biptag.biptag.Constant;
import com.biptag.biptag.NavigationActivity;
import com.biptag.biptag.R;
import com.biptag.biptag.CustumViews.ToggleButtonGroupTableLayout;
import com.biptag.biptag.dbmanager.datasource.BTAvailableCountryDataSource;
import com.biptag.biptag.dbmanager.datasource.BTFormDataSource;
import com.biptag.biptag.dbmanager.datasource.BTImageDataSource;
import com.biptag.biptag.dbmanager.datasource.BtUserConfigDataSource;
import com.biptag.biptag.dbmanager.objects.BTCity;
import com.biptag.biptag.dbmanager.objects.BTFormulaire;
import com.biptag.biptag.dbmanager.objects.BTImage;
import com.biptag.biptag.dbmanager.objects.BTUserConfig;
import com.biptag.biptag.fragements.ConnectedhomeFragment;
import com.biptag.biptag.fragements.EditOCRFragment;
import com.biptag.biptag.fragements.GalleryImageFragment;
import com.biptag.biptag.fragements.SaveBitmap;
import com.biptag.biptag.interfaces.AsyncResponse;
import com.biptag.biptag.managers.BipTagFormsManager;
import com.biptag.biptag.managers.BiptagNavigationManager;
import com.biptag.biptag.managers.BiptagUserManger;
import com.biptag.biptag.ocr.CaptureActivity;
import com.biptag.biptag.ocr.TessOCR;
import com.biptag.biptag.regularepression.EmailValidator;
import com.biptag.biptag.regularepression.FullNameValidator;
import com.biptag.biptag.regularepression.PhoneValidator;

import static android.widget.Toast.*;

public class FillingFormStep1 extends Fragment implements AsyncResponse {

	public PopupWindow ocrPopupWindow;
    private static final int RESULT_CODE_CONTACT = 400 ;
    private static final int REQUEST_PICK_PHOTO = 300 ;
    public static final int MEDIA_TYPE_IMAGE = 1;
    public static final int MEDIA_TYPE_VIDEO = 2;
    private static final String IMAGE_DIRECTORY_NAME = "biptagimg";
    Uri imageUri ;
    private List<String> selectedImagePath;
    TableLayout imageContainer ;
    Bitmap vcardBitMap = null ;
    String vcardbitmapPath = null ;
    String vcardImageName  = null;
    int  vcardImageId=0;
    List<String[]> list ;
    String email  ;
     int imageCounter = 0 ;
     public static  View fragmentView ;
      FillingFormStep2 formStep2 = null  ;
       GalleryImageFragment galleryImageFragment ;
      int form_id , agent_id ;
      LinearLayout filling_form_step1_form_container ;
      int screenWidth ,screenHeight ;
      PopupWindow popupWindow ;
      int x  = 0 ;
      int y = 0 ;
	boolean	dragOcr = false;
  	private static final int REQUEST_CODE_FUNCTION= 600;



      public FillingFormStep1() {
		// TODO Auto-generated constructor stub
    	  galleryImageFragment = null ;
    	 // fragmentView = null ;
      }


      public void removeScroll(){
    	  //Log.d("remove scroll", "remove scroll ") ;
    	  if(filling_form_step1_form_container != null){
    		  android.widget.LinearLayout.LayoutParams params = (android.widget.LinearLayout.LayoutParams)	filling_form_step1_form_container.getLayoutParams() ;
  			params.setMargins(screenWidth/30, 0 , screenWidth/30, screenHeight/30) ;
  			filling_form_step1_form_container.setLayoutParams(params) ;

    	  }


    	  if(popupWindow != null){
    		  popupWindow.dismiss()  ;
    	  }

      }


//LZR : 14/04/2016 the function to save data befor moving to the next step
      public void nextAction(){

			// TODO Auto-generated method stub
			try {
//LZR : 14/04/2016 get the data of the contact
				JSONObject array=getData() ;
					Log.d("step 1", "step 1 is email valide '"+email+"' "+isValidEmail(email) ) ;

						email = email.replaceAll(" ", "") ;
					if(isValidEmail(email)){
					if(selectedImagePath.size() == 0){
						array.put("CONTACT_IMAGE", "no_image") ;

					}else {
						JSONArray images = new JSONArray(renameImage()) ;
						BiptagUserManger.saveUserData("images", images.toString(), getActivity()) ;
						array.put("CONTACT_IMAGE", images) ;
					}
					//Log.d("add image to response ", "adding image to response  array result "+array.toString())  ;
					BiptagUserManger.saveUserData("form_data_step1", array.toString(), getActivity()) ;
					BiptagUserManger.saveUserData("contact_email", email, getActivity()) ;

					if(formStep2 == null){
						formStep2 = new FillingFormStep2() ;
					}

					BiptagNavigationManager.pushFragments(formStep2, true, "create_form_2", NavigationActivity.getContext(), String.valueOf(form_id), 1) ;
					if(ocrPopupWindow!=null){
						ocrPopupWindow.dismiss();
					}
				}else{
					AlertDialog.Builder builder= new AlertDialog.Builder(getActivity()) ;
					builder.setTitle(getResources().getString(R.string.email_invalide_titre)) ;
					builder.setMessage(getResources().getString(R.string.email_invalide)) ;
					builder.setPositiveButton("Ok", new OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {
							// TODO Auto-generated method stub
							dialog.dismiss() ;
						}
					}) ;
					builder.show() ;
				}
			} catch (JSONException e) {
				// TODO: handle exception
			//Log.d("add image to response ", "adding image to response "+e.getMessage()) ;
			}


      }


      public static void updateLabelsText(){
    	  if(fragmentView != null){
    		  Button btn1 = (Button) fragmentView.findViewById(R.id.fildaraiane_step1_button1) ;
    			Button btn2 = (Button) fragmentView.findViewById(R.id.fildaraiane_step1_button2) ;
    			Button btn3 = (Button) fragmentView.findViewById(R.id.fildaraiane_step1_button3) ;
    			 btn1.setText(NavigationActivity.getContext().getResources().getString(R.string.coordinate)) ;
    			 btn2.setText(NavigationActivity.getContext().getResources().getString(R.string.quest_met)) ;
    			 btn3.setText(NavigationActivity.getContext().getString(R.string.support_commercials)) ;
    			Button nextButtonL =(Button) fragmentView.findViewById(R.id.nextprevious_nextBTN) ;
    			nextButtonL.setText(NavigationActivity.getContext().getString(R.string.next_btn))  ;


    			Button photoButton  = (Button) fragmentView.findViewById(R.id.add_photo_button) ;
    			Button listButton = (Button) fragmentView.findViewById(R.id.add_from_list_btn) ;
    			Button scanButton = (Button) fragmentView.findViewById(R.id.scan_vcard_button) ;

    			photoButton.setText(NavigationActivity.getContext().getResources().getString(R.string.add_photo)) ;
    			listButton.setText(NavigationActivity.getContext().getResources().getString(R.string.carnet_address))  ;
    			scanButton.setText(NavigationActivity.getContext().getResources().getString(R.string.scan_vcard)) ;


    	  }
      }

      public void  initFilDarianeNavigation() {
		Button btn1 = (Button) fragmentView.findViewById(R.id.fildaraiane_step1_button1) ;
		Button btn2 = (Button) fragmentView.findViewById(R.id.fildaraiane_step1_button2) ;
		Button btn3 = (Button) fragmentView.findViewById(R.id.fildaraiane_step1_button3) ;
		 btn1.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

			}
		}) ;
		 btn2.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				nextAction() ;
			}
		}) ;

		 btn3.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				nextAction() ;
			}
		}) ;


	}


	  public void fixLayout(View fragmentView  ){

			LinearLayout formste1_btn_container =(LinearLayout) fragmentView.findViewById(R.id.formste1_btn_container) ;
			 GradientDrawable border = new GradientDrawable();
			    border.setColor(0xFFFAFAFA); //white background
			    border.setStroke(3, 0xFFE6E6EA); //black border with full opacity
			    if(Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
			    	formste1_btn_container.setBackgroundDrawable(border);
			    } else {
			    	formste1_btn_container.setBackground(border);
			    }

			    WindowManager wm = (WindowManager) getActivity().getSystemService(Context.WINDOW_SERVICE);
				Display display  =wm.getDefaultDisplay() ;
				Point point  = new Point() ;
				display.getSize(point) ;
				screenWidth = point.x ;
				screenHeight = point.y ;
			    LinearLayout.LayoutParams params = (android.widget.LinearLayout.LayoutParams) formste1_btn_container.getLayoutParams() ;
			    params.setMargins(screenWidth/30, screenHeight/30, screenWidth/30, screenHeight/30) ;
			    formste1_btn_container.setLayoutParams(params) ;

			     filling_form_step1_form_container = (LinearLayout) fragmentView.findViewById(R.id.filling_form_step1_form_container) ;

			    GradientDrawable borderContainer = new GradientDrawable();
			    borderContainer.setColor(0xFFFAFAFA); //white background
			    borderContainer.setStroke(3, 0xFFE6E6EA); //black border with full opacity
			    if(Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
			    	filling_form_step1_form_container.setBackgroundDrawable(borderContainer);
			    } else {
			    	filling_form_step1_form_container.setBackground(borderContainer);
			    }

			    LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) filling_form_step1_form_container.getLayoutParams() ;
			    layoutParams.setMargins(screenWidth/30, 0, screenWidth/30, screenHeight/30) ;
			    filling_form_step1_form_container.setLayoutParams(layoutParams) ;


			    LinearLayout fil_dariane = (LinearLayout) fragmentView.findViewById(R.id.fil_dariane) ;
			    android.widget.LinearLayout.LayoutParams arianeLayoutParams = new LayoutParams(android.widget.LinearLayout.LayoutParams.MATCH_PARENT, getResources().getDimensionPixelSize(R.dimen.next_prev_height_button)) ;
			    fil_dariane.setLayoutParams(arianeLayoutParams) ;

			    //Log.d("btn layer ", "btn layer margin   "+screenHeight+"  "+(screenHeight- screenHeight/10)) ;
			    LinearLayout step1_btn_container = (LinearLayout) fragmentView.findViewById(R.id.step1_btn_container) ;
			    LinearLayout.LayoutParams btn_layaout_params = (LinearLayout.LayoutParams) step1_btn_container.getLayoutParams() ;
			   // btn_layaout_params.setMargins(0, screenHeight/13, 0, 0) ;
			    step1_btn_container.setLayoutParams(btn_layaout_params) ;



	  }

		public static void setListViewHeightBasedOnChildren(ListView listView) {
		    ListAdapter listAdapter = listView.getAdapter();
		    if (listAdapter == null)
		        return;

		    int desiredWidth = MeasureSpec.makeMeasureSpec(listView.getWidth(), MeasureSpec.UNSPECIFIED);
		    int totalHeight = 0;
		    View view = null;
		    for (int i = 0; i < listAdapter.getCount(); i++) {
		        view = listAdapter.getView(i, view, listView);
		        if (i == 0)
		            view.setLayoutParams(new ViewGroup.LayoutParams(desiredWidth, LayoutParams.WRAP_CONTENT));

		        view.measure(desiredWidth, MeasureSpec.UNSPECIFIED);
		        totalHeight += view.getMeasuredHeight();
		    }
		    ViewGroup.LayoutParams params = listView.getLayoutParams();
		    params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1))  ;
		    listView.setLayoutParams(params);
		    listView.requestLayout();
		}


	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		System.gc();
		if (fragmentView == null) {
			formStep2.fragmentView = null;
			formStep2 = null;
			fragmentView = inflater.inflate(R.layout.fragement_filling_form_step_1, null);
			fixLayout(fragmentView);
			BiptagUserManger.clearField(NavigationActivity.getContext(), "updated_contact_creat") ;

			selectedImagePath = new ArrayList<String>();

			String ais = String.valueOf(BiptagUserManger.getUserValue("AGENT_ID", "int", getActivity()));
			agent_id = Integer.parseInt(ais);

			Button addFromListBtn = (Button) fragmentView.findViewById(R.id.add_from_list_btn);
			addFromListBtn.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
					if (ocrPopupWindow != null) {
						ocrPopupWindow.dismiss();
					}
					startActivityForResult(intent, RESULT_CODE_CONTACT);

				}
			});

			Button addPhotoBtn = (Button) fragmentView.findViewById(R.id.add_photo_button);

			final String form_id_string = getArguments().getString("data");
			form_id = Integer.parseInt(form_id_string);
			list = BipTagFormsManager.buildFormComposant(form_id, NavigationActivity.getContext(), FillingFormStep1.this, true, 0, 17, R.id.filling_form_step1_form_container, fragmentView);
//LZR : 14/04/2016 verify if the formulaire is in new forma
			if (list.size() == 17) {

				AlertDialog.Builder builder = new AlertDialog.Builder(NavigationActivity.getContext());
				builder.setMessage(NavigationActivity.getContext().getResources().getString(R.string.form_ancien_dsc));
				builder.setTitle(NavigationActivity.getContext().getResources().getString(R.string.form_ancien_title));
				builder.setPositiveButton("OK", new OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						fragmentView = null;
						ConnectedhomeFragment connectedhomeFragment = new ConnectedhomeFragment();
						BiptagNavigationManager.pushFragments(connectedhomeFragment, false, "connectedhome", NavigationActivity.context, null, 1);

					}
				});

				builder.show();


			} else {
				for (int i = 0; i < list.size(); i++) {
					// ajout du listener pour le changement du code postal
					if (list.get(i)[0].equals("141")) {
						final EditText editText = (EditText) fragmentView.findViewById(Integer.parseInt(list.get(i)[2]));
						editText.setInputType(InputType.TYPE_CLASS_NUMBER);


						editText.addTextChangedListener(new TextWatcher() {

							@Override
							public void onTextChanged(CharSequence s, int start, int before, int count) {
								// TODO Auto-generated method stub
								String countrySelected = "";
								for (int j = 0; j < list.size(); j++) {
									if (list.get(j)[0].equals("143")) {
										EditText editText = (EditText) fragmentView.findViewById(Integer.parseInt(list.get(j)[2]));
										countrySelected = editText.getText().toString();
									}
								}


								//String countryCodes = BipTagFormsManager.loadJSONFromAsset(getActivity() , "FRANCE.json") ;
								////Log.d("france json", "france json "+countryCodes) ;
								EditText villeEditText = null;
								for (int j = 0; j < list.size(); j++) {
									if (list.get(j)[0].equals("142")) {
										villeEditText = (EditText) fragmentView.findViewById(Integer.parseInt(list.get(j)[2]));
									}
								}

								if (villeEditText != null) {

									try {

										BTAvailableCountryDataSource source = new BTAvailableCountryDataSource(getActivity());
										BtUserConfigDataSource source1 = new BtUserConfigDataSource(Constant.context);
										BTUserConfig config = source1.getUserConfig(agent_id);
										if (config != null) {
											if (config.getLanguage().equals("English")) {
												String coutriesString = BipTagFormsManager.loadJSONFromAsset(getActivity(), "pays.json");
												String key = "nom_en_gb";

												try {
													JSONArray countries = new JSONArray(coutriesString);
													for (int j = 0; j < countries.length(); j++) {
														if (countries.getJSONObject(j).getString(key).equals(countrySelected)) {
															countrySelected = countries.getJSONObject(j).getString("nom_fr_fr");
														}

													}
												} catch (JSONException e) {

												}
											}
										}


										List<BTCity> cities = source.selectCities(countrySelected);
										//JSONArray codes = new JSONArray(countryCodes) ;
										for (int j = 0; j < cities.size(); j++) {
											if (cities.get(j).getZipCode().equals(editText.getText().toString())) {
												villeEditText.setText(cities.get(j).getCityName());
											}
										}

									} catch (Exception e) {
										// TODO: handle exception
										//Log.d("zip city excption ", "zip city exception "+e.getMessage()) ;

									}


								}


							}

							@Override
							public void beforeTextChanged(CharSequence s, int start, int count,
														  int after) {
								// TODO Auto-generated method stub

							}

							@Override
							public void afterTextChanged(Editable s) {
								// TODO Auto-generated method stub

							}
						});
					}

					if (list.get(i)[0].equals("142")) {
						final EditText editText = (EditText) fragmentView.findViewById(Integer.parseInt(list.get(i)[2]));
						editText.addTextChangedListener(new TextWatcher() {
							String originalText;

							@Override
							public void onTextChanged(CharSequence s, int start, int before, int count) {
								// TODO Auto-generated method stub
								if (editText.getText().toString().length() != 0) {

									if (editText.getText().toString().charAt(editText.getText().toString().length() - 1) == '\n') {
										//Log.d("edit follow up keycode", "edit follow up keycode enter") ;
										editText.setText(originalText);
									}
								}
							}

							@Override
							public void beforeTextChanged(CharSequence s, int start, int count,
														  int after) {
								// TODO Auto-generated method stub
								originalText = editText.getText().toString();
								if (originalText.length() != 0) {
									originalText = originalText.replaceAll("\\n", "");
								}


							}

							@Override
							public void afterTextChanged(Editable s) {
								// TODO Auto-generated method stub

								originalText = editText.getText().toString();
								editText.setSelection(editText.getText().toString().length());

							}
						});
					}


					if (list.get(i)[0].equals("143")) {
						final EditText editText = (EditText) fragmentView.findViewById(Integer.parseInt(list.get(i)[2]));


						BtUserConfigDataSource source = new BtUserConfigDataSource(Constant.context);
						BTUserConfig config = source.getUserConfig(agent_id);
						if (config != null) {
							if (config.getDefaultCountry() != null && config.getDefaultCountry().length() != 0) {
								editText.setText(config.getDefaultCountry());

							} else {
								editText.setText("France");
							}
						} else {
							editText.setText("France");
						}


						LayoutInflater layoutInflater = LayoutInflater.from(getActivity());
						final View view = layoutInflater.inflate(R.layout.coutrines_propositions, null);
						popupWindow = new PopupWindow(view, android.app.ActionBar.LayoutParams.WRAP_CONTENT, screenHeight / 5);
						final ListView coutriesListView = (ListView) view.findViewById(R.id.countries_propositions_list_all);
						final ListView mostUserListView = (ListView) view.findViewById(R.id.countries_propositions_list);

						mostUserListView.setOnTouchListener(new View.OnTouchListener() {

							@Override
							public boolean onTouch(View v, MotionEvent event) {
								// TODO Auto-generated method stub
								if (event.getAction() == MotionEvent.ACTION_DOWN) {
									x = (int) event.getX();
									y = (int) event.getY();
								}
								int localX = 0;
								int localY = 0;
								if (event.getAction() == MotionEvent.ACTION_UP) {
									localX = (int) event.getX();
									localY = (int) event.getY();
								}

								//Log.d("most used ", "most used touch listener triggered "+x+" "+localX+"  "+y+"  "+localY) ;
								Rect rect = null;
								if (x == localX && y == localY) {
									for (int j = 0; j < mostUserListView.getChildCount(); j++) {
										View view = mostUserListView.getChildAt(j);
										rect = new Rect((int) view.getX(), (int) view.getY(), (int) view.getX() + view.getWidth(), (int) view.getY() + view.getHeight());
										if (rect.contains(x, y)) {
											TextView txt = (TextView) view.findViewById(android.R.id.text1);
											//Log.d("most used ", "most used  touch listener found item "+txt.getText().toString()) ;
											editText.setText(txt.getText().toString());
											popupWindow.dismiss();
											break;

										}

									}
								}


								return false;
							}
						});


						coutriesListView.setOnTouchListener(new View.OnTouchListener() {

							@Override
							public boolean onTouch(View v, MotionEvent event) {
								// TODO Auto-generated method stub
								if (event.getAction() == MotionEvent.ACTION_DOWN) {
									x = (int) event.getX();
									y = (int) event.getY();
								}
								int localX = 0;
								int localY = 0;
								if (event.getAction() == MotionEvent.ACTION_UP) {
									localX = (int) event.getX();
									localY = (int) event.getY();
								}

								//Log.d("most used ", "most used touch listener triggered "+x+" "+localX+"  "+y+"  "+localY) ;
								Rect rect = null;
								if (x == localX && y == localY) {
									for (int j = 0; j < coutriesListView.getChildCount(); j++) {
										View view = coutriesListView.getChildAt(j);
										rect = new Rect((int) view.getX(), (int) view.getY(), (int) view.getX() + view.getWidth(), (int) view.getY() + view.getHeight());
										if (rect.contains(x, y)) {
											TextView txt = (TextView) view.findViewById(android.R.id.text1);
											//Log.d("most used ", "most used  touch listener found item "+txt.getText().toString()) ;
											editText.setText(txt.getText().toString());
											popupWindow.dismiss();
											break;

										}

									}
								}


								return false;
							}
						});


						final List<String> countriesNames = new ArrayList<String>();
						final List<String> mostUserCountries = new ArrayList<String>();
						String key = "nom_fr_fr";
						if (config != null) {
							//Log.d("lang default", "lang defaut "+config.getLanguage()) ;
							if (config.getLanguage().equals("English")) {
								key = "nom_en_gb";
							}
						}

						final String fKey = key;
						String coutriesString = BipTagFormsManager.loadJSONFromAsset(NavigationActivity.getContext(), "pays.json");
						try {
							JSONArray countries = new JSONArray(coutriesString);

							for (int j = 0; j < countries.length(); j++) {


								if (j < 6) {
									mostUserCountries.add(countries.getJSONObject(j).getString(key));
								} else {
									countriesNames.add(countries.getJSONObject(j).getString(key));

								}


							}
						} catch (JSONException e) {

						}


						final ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(),
								android.R.layout.simple_list_item_1, android.R.id.text1, countriesNames);
						final ArrayAdapter<String> adapterMostUser = new ArrayAdapter<String>(getActivity(),
								android.R.layout.simple_list_item_1, android.R.id.text1, mostUserCountries);


						mostUserListView.setOnItemClickListener(new OnItemClickListener() {

							@Override
							public void onItemClick(AdapterView<?> parent, View view,
													int position, long id) {
								// TODO Auto-generated method stub
								String pays = String.valueOf(mostUserListView.getItemAtPosition(position));
								editText.setText(pays);
								popupWindow.dismiss();

							}
						});

						editText.addTextChangedListener(new TextWatcher() {
							String originalText;

							@Override
							public void onTextChanged(CharSequence s, int start, int before, int count) {
								// TODO Auto-generated method stub
								if (editText.getText().toString().length() != 0) {

									if (editText.getText().toString().charAt(editText.getText().toString().length() - 1) == '\n') {
										//Log.d("edit follow up keycode", "edit follow up keycode enter") ;
										editText.setText(originalText);
									}
								}
							}

							@Override
							public void beforeTextChanged(CharSequence s, int start, int count,
														  int after) {
								// TODO Auto-generated method stub
								originalText = editText.getText().toString();
								if (originalText.length() != 0) {
									originalText = originalText.replaceAll("\\n", "");
								}


							}

							@Override
							public void afterTextChanged(Editable s) {
								// TODO Auto-generated method stub

								originalText = editText.getText().toString();
								editText.setSelection(editText.getText().toString().length());


								String coutriesString = BipTagFormsManager.loadJSONFromAsset(NavigationActivity.getContext(), "pays.json");
								try {
									JSONArray countries = new JSONArray(coutriesString);
									String userEntry = s.toString();
									adapter.clear();
									adapterMostUser.clear();
									int counter = 0;
									int mostUsedCounter = 0;
									for (int j = 0; j < countries.length(); j++) {
										if (countries.getJSONObject(j).getString(fKey).toUpperCase().indexOf(userEntry.toUpperCase()) != -1) {
											if (j < 6) {
												adapterMostUser.insert(countries.getJSONObject(j).getString(fKey), mostUsedCounter);
												mostUsedCounter++;
											} else {
												adapter.insert(countries.getJSONObject(j).getString(fKey), counter);
												counter++;
											}

										}

										//countriesNames[j] = countries.getJSONObject(j).getString("nom_fr_fr") ;

									}
									//Log.d("show countries ", "show countries after the for") ;

									//Log.d("show countries ", "show countries before set adapter") ;
									adapter.notifyDataSetChanged();
									adapterMostUser.notifyDataSetChanged();

									setListViewHeightBasedOnChildren(mostUserListView);
									setListViewHeightBasedOnChildren(coutriesListView);
									//coutriesListView.setAdapter(adapter) ;
									//Log.d("show countries ", "show countries show at locaition  1") ;
									if (popupWindow != null) {
										//	if(!popupWindow.isShowing() )
										//				popupWindow.showAsDropDown(editText);

									}

								} catch (JSONException e) {
									// TODO: handle exception
									//Log.d("show countries ", "show countries exception "+e.getMessage()) ;
								}


							}
						});


						editText.setOnTouchListener(new OnTouchListener() {

							@Override
							public boolean onTouch(View v, MotionEvent event) {
								// TODO Auto-generated method stub

								if (event.getAction() == MotionEvent.ACTION_DOWN) {
							//		Log.d("edit text", "edit texxt touch " + editText.hasFocusable() + " " + editText.hasFocus());

									Rect rect = new Rect();
									View root = getActivity().getWindow().getDecorView();
									root.getWindowVisibleDisplayFrame(rect);
									int diff = screenHeight - rect.height();

									android.widget.LinearLayout.LayoutParams params = (android.widget.LinearLayout.LayoutParams) filling_form_step1_form_container.getLayoutParams();
									params.setMargins(screenWidth / 30, -popupWindow.getHeight() - screenHeight / 6, screenWidth / 30, screenHeight / 30);
									filling_form_step1_form_container.setLayoutParams(params);

								}

								if (editText.hasFocus()) {


									//Log.d("show countries ", "show countries enter before text changed ") ;
									String coutriesString = BipTagFormsManager.loadJSONFromAsset(NavigationActivity.getContext(), "pays.json");


									try {
										JSONArray countries = new JSONArray(coutriesString);
										String[] countriesNames = new String[countries.length()];
										for (int j = 0; j < countries.length(); j++) {
											countriesNames[j] = countries.getJSONObject(j).getString("nom_fr_fr");
										}
										coutriesListView.setAdapter(adapter);
										mostUserListView.setAdapter(adapterMostUser);
										setListViewHeightBasedOnChildren(mostUserListView);
										setListViewHeightBasedOnChildren(coutriesListView);
										//Log.d("show countries ", "show countries show at locaition") ;
										Point point = getPointOfView(editText);

											popupWindow.showAsDropDown(editText);

										//popupWindow.showAtLocation(editText, Gravity.CENTER, screenHeight / 3, -screenHeight / 5);
										//popupWindow.showAtLocation(view, Gravity.TOP, 0, 0) ;
										android.widget.LinearLayout.LayoutParams params = (android.widget.LinearLayout.LayoutParams) filling_form_step1_form_container.getLayoutParams();
										params.setMargins(screenWidth / 30, -popupWindow.getHeight() - screenHeight / 6, screenWidth / 30, screenHeight / 30);
										filling_form_step1_form_container.setLayoutParams(params);

									} catch (JSONException e) {

										// TODO: handle exception
										//Log.d("show countries ", "show countries exception "+e.getMessage()) ;
									}


								} else {
									popupWindow.dismiss();
								}

								return false;
							}
						});
						editText.setOnFocusChangeListener(new OnFocusChangeListener() {

							@Override
							public void onFocusChange(View v, boolean hasFocus) {
								// TODO Auto-generated method stub
							//	Log.d(" edit text ", " edit text " + editText.hasFocus());
								if (editText.hasFocus()) {


									//Log.d("show countries ", "show countries enter before text changed ") ;
									String coutriesString = BipTagFormsManager.loadJSONFromAsset(NavigationActivity.getContext(), "pays.json");


									try {
										JSONArray countries = new JSONArray(coutriesString);
										String[] countriesNames = new String[countries.length()];
										for (int j = 0; j < countries.length(); j++) {
											countriesNames[j] = countries.getJSONObject(j).getString("nom_fr_fr");
										}
										coutriesListView.setAdapter(adapter);
										mostUserListView.setAdapter(adapterMostUser);
										setListViewHeightBasedOnChildren(mostUserListView);
										setListViewHeightBasedOnChildren(coutriesListView);
										//Log.d("show countries ", "show countries show at locaition") ;
											popupWindow.showAsDropDown(editText);
										Point point = getPointOfView(editText);
										//popupWindow.showAtLocation(editText, Gravity.CENTER, screenHeight / 3, -screenHeight / 5);

										//popupWindow.showAtLocation(editText, Gravity.CENTER, point.x + editText.getHeight(), point.y+) ;

										//popupWindow.showAtLocation(view, Gravity.CENTER, 0, 0) ;
									} catch (JSONException e) {

										// TODO: handle exception
										//Log.d("show countries ", "show countries exception "+e.getMessage()) ;
									}


								} else {
									popupWindow.dismiss();
								}
							}
						});

					}

					if (list.get(i)[0].equals("16")) {

						final EditText editText = (EditText) fragmentView.findViewById(Integer.parseInt(list.get(i)[2]));
						editText.setOnTouchListener(new OnTouchListener() {

							@Override
							public boolean onTouch(View v, MotionEvent event) {
								// TODO Auto-generated method stub
								if (event.getAction() == MotionEvent.ACTION_DOWN) {
									//Log.d("edit text", "edit texxt touch") ;
									android.widget.LinearLayout.LayoutParams params = (android.widget.LinearLayout.LayoutParams) filling_form_step1_form_container.getLayoutParams();
									params.setMargins(screenWidth / 30, -screenHeight / 2, screenWidth / 30, screenHeight / 30);
									filling_form_step1_form_container.setLayoutParams(params);

								}
								return false;
							}
						});
						editText.setOnEditorActionListener(new OnEditorActionListener() {

							@Override
							public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
								// TODO Auto-generated method stub
								if (actionId == EditorInfo.IME_ACTION_DONE) {
									android.widget.LinearLayout.LayoutParams params = (android.widget.LinearLayout.LayoutParams)
											filling_form_step1_form_container.getLayoutParams();
									params.setMargins(screenWidth / 30, 0, screenWidth / 30, screenHeight / 30);
									filling_form_step1_form_container.setLayoutParams(params);

								}


								return false;
							}
						});

					}
					if (!list.get(i)[0].equals("143") && !list.get(i)[0].equals("16")) {


						View view = fragmentView.findViewById(Integer.parseInt(list.get(i)[2]));

						if (view.getClass() == EditText.class) {
							final EditText editText = (EditText) view;
							editText.setOnTouchListener(new OnTouchListener() {

								@Override
								public boolean onTouch(View v, MotionEvent event) {
									// TODO Auto-generated method stub
									if (event.getAction() == MotionEvent.ACTION_DOWN) {
										//Log.d("edit text", "edit texxt touch") ;
										android.widget.LinearLayout.LayoutParams params = (android.widget.LinearLayout.LayoutParams)
												filling_form_step1_form_container.getLayoutParams();
										params.setMargins(screenWidth / 30, 0, screenWidth / 30, screenHeight / 30);
										filling_form_step1_form_container.setLayoutParams(params);

									}
									return false;
								}
							});

						}

					}

					View view = fragmentView.findViewById(Integer.parseInt(list.get(i)[2]));

					if (view.getClass() == EditText.class) {
						final EditText editText = (EditText) view;

						editText.setOnKeyListener(new OnKeyListener() {

							@Override
							public boolean onKey(View v, int keyCode, KeyEvent event) {
								// TODO Auto-generated method stub
								//Log.d("key pressed key", "key pressed "+event.getKeyCode()) ;
								if (event.getKeyCode() == KeyEvent.KEYCODE_ESCAPE) {
									android.widget.LinearLayout.LayoutParams params = (android.widget.LinearLayout.LayoutParams)
											filling_form_step1_form_container.getLayoutParams();
									params.setMargins(screenWidth / 30, 0, screenWidth / 30, screenHeight / 30);
									filling_form_step1_form_container.setLayoutParams(params);


								}
								return false;
							}


						});


						editText.setOnEditorActionListener(new OnEditorActionListener() {

							@Override
							public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
								// TODO Auto-generated method stub
								if (actionId == EditorInfo.IME_ACTION_DONE) {
									android.widget.LinearLayout.LayoutParams params = (android.widget.LinearLayout.LayoutParams) filling_form_step1_form_container.getLayoutParams();
									params.setMargins(screenWidth / 30, 0, screenWidth / 30, screenHeight / 30);
									filling_form_step1_form_container.setLayoutParams(params);
								}


								return false;
							}
						});


					}


				}

//LZR : 14/04/2016 Add photo from camera
				addPhotoBtn.setOnClickListener(new View.OnClickListener() {


					@Override
					public void onClick(View v) {
					//	Log.d("Text ", "image liste");
						galleryImageFragment = new GalleryImageFragment(null, false, 1);
						Bundle bundle = new Bundle();
						bundle.putString("Email", email);

						BiptagNavigationManager.pushFragments(galleryImageFragment, true, "galleryimagefragment", NavigationActivity.getContext(), null, 1);


					}

				});
//LZR : 14/04/2016 next step
				Button nextButton = (Button) fragmentView.findViewById(R.id.nextprevious_nextBTN);
				nextButton.setOnClickListener(new View.OnClickListener() {

					@Override
					public void onClick(View v) {

						nextAction();

					}
				});
//LZR : 14/04/2016 scan vcard
				Button scanVcardButton = (Button) fragmentView.findViewById(R.id.scan_vcard_button);
				int buttonTextSize = (int) getResources().getDimension(R.dimen.step1_buttons_text_size);
				addFromListBtn.setTextSize(buttonTextSize);
				addPhotoBtn.setTextSize(buttonTextSize);
				scanVcardButton.setTextSize(buttonTextSize);

//LZR : 14/04/2016 check if vcard is allready scanned
				if (String.valueOf(BiptagUserManger.getUserValue("EditscanOCR_Creat","string",NavigationActivity.getContext())).indexOf("editscan")!=-1){
				//	Log.d(" isEditMode ","isEditMode true "+ BiptagUserManger.getUserValue("EditscanOCR_Creat","string",NavigationActivity.getContext()).toString());
					dragOcr =true;
				}else{
				//	Log.d(" isEditMode ","isEditMode false ");

					dragOcr = false;
				}




				scanVcardButton.setOnClickListener(new View.OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						String isScanned = "0";
						//verifier si le contact a une carte de visite
						String imageJsonStrin = String.valueOf(BiptagUserManger.getUserValue("images_for_user", "string", NavigationActivity.getContext()));
						try {
							JSONArray imagesJson = new JSONArray(imageJsonStrin);
							//Log.d(" image string received ", " image received " + imagesJson.toString() + " " + imageJsonStrin);
							BTImageDataSource dataSource = new BTImageDataSource(NavigationActivity.getContext());

							for (int i = 0; i < imagesJson.length(); i++) {


								final int imageId = imagesJson.getInt(i);
							//	Log.d(" image string receive ", " image string " + imageId + " " + imagesJson.toString());


								BTImage image = dataSource.selectImagesWithId(imageId, Integer.parseInt(String.valueOf(BiptagUserManger.getUserValue("BA_ID", "int", NavigationActivity.getContext()))));
								//Log.d(" image json string ", " image json " + image.toString() + " " + imageJsonStrin + " " + imageId + " " + image.getImageDataOCR());
								if (image != null) {
									String imagePath = NavigationActivity.getContext().getCacheDir().getAbsolutePath() + "/" + image.getImageName() + ".jpg";
								//	Log.d("image json ", "image json path " + image.getImageName() + " " + imagePath);
									BiptagUserManger.saveUserData("bitmapName", imagePath, NavigationActivity.getContext());

									if (image.getImageDataOCR() != null) {
										isScanned = "1";
										break;
									} else {
										isScanned = "0";
									}

								}
							}

						} catch (JSONException e) {
							// TODO Auto-generated catch block

						}

						//	String isScanned = (String.valueOf(BiptagUserManger.getUserValue("cardscanned", "int", NavigationActivity.getContext())));
						if (isScanned.indexOf("1") != -1) {
//si OUI : afficher un message
						//	Log.d(" is scacnned ", isScanned + " isScanned ");
							final AlertDialog.Builder builder = new AlertDialog.Builder(NavigationActivity.getContext());
							builder.setTitle("Carte de visite déjà scannée");
							builder.setMessage(" Vous avez déjà scanné une carte de visite pour ce contact. Voulez-vous scanner une nouvelle carte de visite en écrasant l’ancienne ? ");
							builder.setPositiveButton(" Reprendre ", new OnClickListener() {
										@Override
										public void onClick(DialogInterface dialog, int which) {
//Reprendre le scan :

											if (isAdded()) {
												BiptagUserManger.saveUserData("ScanOCR_Creat", "scanocr", NavigationActivity.context);
												Intent intent = new Intent(getActivity(), CaptureActivity.class);
												intent.putExtra("EditMode", 5);
												startActivity(intent);
												String key = "images_for_user";
												String vcard_id_key_edit = "vcard_id_key_create";
												BiptagUserManger.saveUserData("key", key, NavigationActivity.getContext());
												BiptagUserManger.saveUserData("vcardIdKey", vcard_id_key_edit, NavigationActivity.getContext());

												Calendar calendar = Calendar.getInstance();


												startActivityForResult(intent, REQUEST_CODE_FUNCTION);
											}
										}
									}
							);

							builder.setNegativeButton("Annuler", new OnClickListener() {
										@Override
										public void onClick(DialogInterface dialog, int which) {
											dialog.dismiss();
										}
									}

							);
							builder.show();
						} else {

							if (isAdded()) {


								Intent intent = new Intent(getActivity(), CaptureActivity.class);
								intent.putExtra("EditMode", 5);
								startActivity(intent);
								String key = "images_for_user";
								String vcard_id_key_edit = "vcard_id_key_create";
								BiptagUserManger.saveUserData("key", key, NavigationActivity.getContext());
								BiptagUserManger.saveUserData("vcardIdKey", vcard_id_key_edit, NavigationActivity.getContext());



								startActivityForResult(intent, REQUEST_CODE_FUNCTION);
								// TODO Auto-generated method stub
								// *****code OCR*****


							}}		}
				});

				initFilDarianeNavigation();


			}		}
			return fragmentView;
		}


	public boolean hasImageCaptureBug() {

	    // list of known devices that have the bug
	    ArrayList<String> devices = new ArrayList<String>();
	    devices.add("android-devphone1/dream_devphone/dream");
	    devices.add("generic/sdk/generic");
	    devices.add("vodafone/vfpioneer/sapphire");
	    devices.add("tmobile/kila/dream");
	    devices.add("verizon/voles/sholes");
	    devices.add("google_ion/google_ion/sapphire");

	    return devices.contains(android.os.Build.BRAND + "/" + android.os.Build.PRODUCT + "/"
				+ android.os.Build.DEVICE);

	}

	private File createTemporaryFile(String part, String ext) throws Exception
	{
	    File tempDir= NavigationActivity.getContext().getExternalCacheDir();
	    tempDir=new File(tempDir.getAbsolutePath()+"/.temp/");
	    if(!tempDir.exists())
	    {
	        tempDir.mkdir();
	    }
	    return File.createTempFile(part, ext, tempDir);
	}



//LZR : 14/04/2016 check if the email is valid
	public  boolean isValidEmail(String target) {
		  if (TextUtils.isEmpty(target)) {
		    return false;
		  } else {
			  boolean isValid = false;

			    String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
			    CharSequence inputStr = target;

			    Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
			    Matcher matcher = pattern.matcher(inputStr);
			    if (matcher.matches()) {
			        isValid = true;
			    }
			    return isValid;		  }
		}

	public List<String> renameImage() {
		try {


		   File mediaStorageDir = new File(
		            Environment
		                    .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
		            IMAGE_DIRECTORY_NAME);

		   int ba_id = Integer.parseInt(String.valueOf(BiptagUserManger.getUserValue("BA_ID", "int", getActivity()))) ;
 		  	List<String> imageNames = new ArrayList<String>()  ;
		for(int i =0 ; i < selectedImagePath.size() ; i++){
			String md5_image_name =  md5(ba_id+"_pciture"+i+".png_"+email+"_"+i+".jpg")  ;
			//Log.d("rename image ", "json response exception  path  1"+selectedImagePath.get(i)) ;
			//Log.d("rename image ", "rename image image path "+md5_image_name) ;
			File image  = new File(selectedImagePath.get(i))  ;
			//Log.d("rename image ", "json response exception  path  2"+image.getAbsolutePath()) ;
			//Log.d("json response exception", "json response exception non"+getActivity().getFilesDir() + File.separator+md5_image_name+".jpg") ;
			//File from = new File(image ,image.getAbsolutePath() ) ;
			//getActivity().getFilesDir() + File.separator+md5_image_name+".jpg"
			File to= new File(getActivity().getFilesDir() + File.separator+md5_image_name+".jpg")  ;
			//from.renameTo(to) ;
			InputStream in= new FileInputStream(image)  ;
			OutputStream out= new FileOutputStream(to) ;

			 byte[] buf = new byte[1024];
             int len;

             while ((len = in.read(buf)) > 0) {
                 out.write(buf, 0, len);
             }

             in.close();
             out.close();

             //Log.v("json response exception  path", " json response exception  path Copy file successful.");
			//Log.d("json response exception", "json response exception non path "+to.getAbsolutePath()) ;

			imageNames.add(md5_image_name) ;

		}
		return imageNames ;
		} catch (IOException e) {
			// TODO: handle exception
			//Log.d("json response exception", "json response exception io exception "+e.getMessage()) ;
			return new ArrayList<String>()  ;
		}
	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		//System.gc();

		super.onResume();

		BiptagUserManger.clearField(NavigationActivity.getContext(), "contact-creat");

		String must_fill_with_data = String.valueOf(BiptagUserManger.getUserValue("must_fill_with_data", "string", NavigationActivity.getContext()));
	//	Log.d("must_fill_with_data", must_fill_with_data);
		if (must_fill_with_data.equals("1")) {
			String contact_result_String = String.valueOf(BiptagUserManger.getUserValue("contact_result_create", "string", NavigationActivity.getContext()));
		//	Log.d("contact_result_String", contact_result_String);
			try {
				JSONObject jsonObject = new JSONObject(contact_result_String);
		//		Log.d("jsonobject", jsonObject.toString());
				fillFromTheAgenda(jsonObject);

			} catch (JSONException e) {
				// TODO: handle exception
			}
			BiptagUserManger.clearField(NavigationActivity.getContext(), "must_fill_with_data") ;
			BiptagUserManger.clearField(NavigationActivity.getContext(), "contact_result_create") ;

		}



		try {

			final JSONObject object = new JSONObject(getDataocr().toString());
			//final JSONObject object = new JSONObject(getData().toString());
		//	Log.d(" ocr result ", " buildcontact step1 " + object.toString());
			BiptagUserManger.saveUserData("updated_contact_creat", object.toString(), NavigationActivity.getContext());

		} catch (JSONException e) {
			e.printStackTrace();
		}

		// BiptagUserManger.saveUserData("ocr_result", array.toString(), NavigationActivity.getContext());
		// BiptagUserManger.saveUserData("contact_result", array.toString(), NavigationActivity.getContext());

		if (String.valueOf(BiptagUserManger.getUserValue("EditscanOCR_Creat", "string", NavigationActivity.getContext())).indexOf("editscan") != -1) {
		//	Log.d(" isEditMode ", "isEditMode true " + BiptagUserManger.getUserValue("EditscanOCR_Creat", "string", NavigationActivity.getContext()).toString());
			dragOcr = true;
		} else {
		//	Log.d(" isEditMode ", "isEditMode false ");

			dragOcr = false;
		}




//fragmentView = null;

			if(dragOcr) {
//LZR : 14/04/2016 if vcard scanned

				try {

					final JSONObject object = new JSONObject(getDataocr().toString());
					//final JSONObject object = new JSONObject(getData().toString());
				//	Log.d(" ocr result ", " object step1 " + object.toString());
					BiptagUserManger.saveUserData("updated_contact_creat", object.toString(), NavigationActivity.getContext());

				} catch (JSONException e) {
					e.printStackTrace();
				}


//LZR : 14/04/2016 Return to the activity of drag ocr
				Button scanOcrreturn = (Button)fragmentView.findViewById(R.id.scanOcrReturn);
				scanOcrreturn.setVisibility(1);
				scanOcrreturn.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						Bundle bundle = new Bundle();
						bundle.putInt("Create_Mode", 6);
						EditOCRFragment editOCRFragment = new EditOCRFragment(null, true, 1);
						editOCRFragment.setArguments(bundle);
						BiptagNavigationManager.pushFragments(editOCRFragment, true, editOCRFragment.getClass().getName(), NavigationActivity.getContext(), null, 1);

					}
				});
			}

		if(popupWindow != null){
			popupWindow.dismiss() ;
		}


	}

	@Override
	public void onStop() {
		// TODO Auto-generated method stub
		super.onStop();

		if(filling_form_step1_form_container != null) {
			android.widget.LinearLayout.LayoutParams params = (android.widget.LinearLayout.LayoutParams) filling_form_step1_form_container.getLayoutParams();
			params.setMargins(screenWidth / 30, 0, screenWidth / 30, screenHeight / 30);
			filling_form_step1_form_container.setLayoutParams(params);
		}
//LZR : 14/04/2016 get the data of step1 and save it before moving to other activity
	try {

		final JSONObject object = new JSONObject(getDataocr().toString());
	//	Log.d(" ocr result ", " object step1 " + object.toString());
		BiptagUserManger.saveUserData("updated_contact_creat", object.toString(), NavigationActivity.getContext());

	} catch (JSONException e) {
		e.printStackTrace();
	}

		BiptagUserManger.clearField(NavigationActivity.getContext(), "ocrScanReturn");

		if (ocrPopupWindow!=null){

			ocrPopupWindow.dismiss();
		}

	}

//LZR : 14/04/2016 get the data of the contact : firstName lastName email address ... and return a json Object
	public JSONObject getData() {

		JSONObject jsonArray = new JSONObject() ;
		try {
			if(list!= null){

			//	Log.i("get data result ", "get data result list is not null ") ;
				for (int i = 0; i < list.size(); i++) {

					//Log.d("get data result ", "get data result list elements  "+list.get(i)[0]+"  "+list.get(i)[1]+"  "+list.get(i)[2]) ;
					String type = list.get(i)[1] ;
					int viewid= Integer.parseInt(list.get(i)[2]) ;

					if(type.equals("text")){
						//Log.d("get data result ", "get data result enter text element") ;
						String type_1 = list.get(i-1)[1] ;
						int viewid_1= Integer.parseInt(list.get(i-1)[2]) ;
						if(type_1.equals("LABEL")) {
							//Log.d("get data result ", "get data result enter text element enter label ");

							if (getView() != null) {
								TextView textView = (TextView) getView().findViewById(viewid_1);
								String key = textView.getText().toString();
								EditText editText = (EditText) getView().findViewById(viewid);
								String value = editText.getText().toString();
							//	Log.d("get data empty text ", "get data empty text " + value + "    ");
								jsonArray.put(key, value);
								//jsonArray.(jsonObject) ;


							}

						}
					}

					if(type.equals("email")){
					//	Log.d("get data result  email", "get data result enter text element") ;
						String type_1 = list.get(i-1)[1] ;
						int viewid_1= Integer.parseInt(list.get(i-1)[2]) ;
						if(type_1.equals("LABEL")) {
						//	Log.d("get data result email ", "get data result enter text element enter label " + viewid_1);
							if (getView() != null) {
								TextView textView = (TextView) getView().findViewById(viewid_1);
								String key = textView.getText().toString();
								EditText editText = (EditText) getView().findViewById(viewid);
								String value = editText.getText().toString();
								value=value.replaceAll(" ","");
								jsonArray.put(key, value);
								//jsonArray.put(jsonObject) ;
								email = value;
						//		Log.d("get data result ", "get data result email value " + email);
							}
						}

					}
					if(type.equals("radio")){
						//Log.d("get data result ", "get data result enter text element") ;
						String type_1 = list.get(i-1)[1] ;
						int viewid_1= Integer.parseInt(list.get(i-1)[2]) ;
						if(type_1.equals("LABEL")) {
						//	Log.d("get data result ", "get data result enter text element enter label " + viewid_1);
							if (getView() != null) {
								TextView textView = (TextView) getView().findViewById(viewid_1);
								String key = textView.getText().toString();
								ToggleButtonGroupTableLayout group = (ToggleButtonGroupTableLayout) getView().findViewById(viewid);
								int selectedRadioId = group.getCheckedRadioButtonId();
							//	Log.d("get data empty radio ", "get data empty radio " + selectedRadioId);
								String value = "";
								if (selectedRadioId != -1) {
									RadioButton selectedRadioButton = (RadioButton) getView().findViewById(selectedRadioId);
									value = selectedRadioButton.getText().toString();

								}
								jsonArray.put(key, value);
								//jsonArray.put(jsonObject) ;

							}
						}

					}

					if(type.equals("address-details")){

						BTFormDataSource source = new BTFormDataSource(getActivity()) ;
						BTFormulaire  formulaire =  source.getFormWithID(agent_id, form_id);

						JSONArray formStructure = formulaire.getFormStructure() ;
						String zipCodeKey = "" ;
						String cityKey  = "" ;
						String  countryKey = "" ;

						for (int j = 0; j < formStructure.length(); j++) {
							if(formStructure.getJSONObject(j).getString("type").equals("address-details")){
								zipCodeKey = formStructure.getJSONObject(j).getJSONArray("VALUE").getString(0) ;
								cityKey =formStructure.getJSONObject(j).getJSONArray("VALUE").getString(1) ;
								countryKey = formStructure.getJSONObject(j).getJSONArray("VALUE").getString(2) ;

							}
						}
						String  zipCode = "" ;
						String city = "" ;
						String country = "" ;
						for (int l = 0; l < list.size(); l++) {
							// ajout du listener pour le changement du code postal

							if(list.get(l)[0].equals("141")){
								EditText editText= (EditText) fragmentView.findViewById(Integer.parseInt(list.get(l)[2])) ;
								zipCode = editText.getText().toString() ;
							}

							if(list.get(l)[0].equals("142")){
								EditText editText= (EditText) fragmentView.findViewById(Integer.parseInt(list.get(l)[2])) ;
								city= editText.getText().toString() ;
							}

							if(list.get(l)[0].equals("143")){
								EditText editText= (EditText) fragmentView.findViewById(Integer.parseInt(list.get(l)[2])) ;
								country= editText.getText().toString() ;
							}

						}

						JSONObject addressDetails = new JSONObject() ;
						addressDetails.put(zipCodeKey, zipCode) ;
						addressDetails.put(cityKey, city) ;
						addressDetails.put(countryKey, country) ;
						jsonArray.put("ADDRESS DETAILS", addressDetails) ;
					//	Log.d("Details", zipCode + city + country + addressDetails);

					}


				}


				if(vcardImageName != null){

					jsonArray.put("VCARD_IMAGE", vcardImageName) ;
					jsonArray.put("VCARD_CHECKED", "0") ;

				}



			}

			//Log.d("get data result ", "get data result  "+jsonArray.toString()) ;


		} catch (JSONException e) {
			// TODO: handle exception
			//Log.d("form step 1 getdata", "get data exception  "+e.getMessage()) ;
		}
	//	Log.d(" ocr result ", " step1 " + jsonArray.toString());
		return jsonArray ;

	}
	//LZR : 14/04/2016 get data of vcard scanned
	public JSONObject getDataocr() {

		JSONObject jsonArray = new JSONObject() ;
		try {
			if(list!= null) {

				//	Log.i("get data result ", "get data result list is not null ") ;
				for (int i = 0; i < list.size(); i++) {

				//	Log.d("get data result ", "get data result list elements  "+list.get(i)[0]+"  "+list.get(i)[1]+"  "+list.get(i)[2]) ;
					String type = list.get(i)[1];
					int viewid = Integer.parseInt(list.get(i)[2]);

					if (type.equals("text")) {
						//Log.d("get data result ", "get data result enter text element") ;
						String type_1 = list.get(i - 1)[1];
						int viewid_1 = Integer.parseInt(list.get(i - 1)[2]);
						if (type_1.equals("LABEL")) {
							//Log.d("get data result ", "get data result enter text element enter label ");

							if (getView() != null) {
								TextView textView = (TextView) getView().findViewById(viewid_1);
								String key = textView.getText().toString();
								EditText editText = (EditText) getView().findViewById(viewid);
								String value = editText.getText().toString();
									//Log.d("get data empty text ", "get data empty text " + value + "    "+textView.getText().toString());
								if (key.equals("Nom")){
									jsonArray.put("LNAME", value);

								}else if (key.equals("Prénom")){
									jsonArray.put("FNAME", value);

								}else if (key.equals("Société")){
									jsonArray.put("COMPANY", value);

								}else if (key.equals("Fonction")){
									jsonArray.put("TITLE", value);

								}else if (key.equals("Téléphone")){
									jsonArray.put("PHONE", value);

								}else  if (key.equals("Adresse")|| key.equals("Adresse (N° Voie, Rue, lieu-dit)")) {
								//	key = "ADDRESS";
									//Log.d(" address " , value + " key " + key);
									jsonArray.put("ADDRESS", value);
								}

								//jsonArray.(jsonObject) ;


							}

						}
					}

					if (type.equals("email")) {
						//	Log.d("get data result  email", "get data result enter text element") ;
						String type_1 = list.get(i - 1)[1];
						int viewid_1 = Integer.parseInt(list.get(i - 1)[2]);
						if (type_1.equals("LABEL")) {
							//	Log.d("get data result email ", "get data result enter text element enter label " + viewid_1);
							if (getView() != null) {
								TextView textView = (TextView) getView().findViewById(viewid_1);
								String key = "EMAIL";
								EditText editText = (EditText) getView().findViewById(viewid);
								String value = editText.getText().toString();
								value = value.replaceAll(" ", "");
								jsonArray.put(key, value);
								//jsonArray.put(jsonObject) ;
								email = value;
								//		Log.d("get data result ", "get data result email value " + email);
							}
						}

					}

					if (type.equals("address-details")) {
						String zipCode = "";
						String city = "";
						String country = "";
						for (int l = 0; l < list.size(); l++) {
							// ajout du listener pour le changement du code postal

							if (list.get(l)[0].equals("141")) {
								if (list.get(l)[2]!=null) {
									//Log.d("list.get(l)[2] ", list.get(l)[2]);
									EditText editText = (EditText) getView().findViewById(Integer.parseInt(list.get(l)[2]));
									zipCode = editText.getText().toString();
									jsonArray.put("ZIPCODE", zipCode);
								}
							}

							if (list.get(l)[0].equals("142")) {
								if(list.get(l)[2]!=null) {
									EditText editText = (EditText) getView().findViewById(Integer.parseInt(list.get(l)[2]));
									city = editText.getText().toString();
									jsonArray.put("CITY", city);
								}
							}

							if (list.get(l)[0].equals("143")) {
								if(list.get(l)[2]!=null) {
									EditText editText = (EditText) getView().findViewById(Integer.parseInt(list.get(l)[2]));
									country = editText.getText().toString();
									jsonArray.put("COUNTRY", country);
								}
							}

						}

						//jsonArray.put("ADDRESS", addressDetails) ;
						Log.d("Details", zipCode + city + country );

					}


				}

			}			Log.d("ocr data result ", "json string details  "+jsonArray.toString()) ;


		} catch (JSONException e) {
			// TODO: handle exception
			//Log.d("form step 1 getdata", "get data exception  "+e.getMessage()) ;
		}
		Log.d(" ocr result ", " step1 " + jsonArray.toString());
		return jsonArray ;

	}

	public  Uri getOutputMediaFileUri(int type) {
	    return Uri.fromFile(getOutputMediaFile(type));
	}

	/*
	 * returning image / video
	 */
	private  File getOutputMediaFile(int type) {

	    // External sdcard location
	    File mediaStorageDir = new File(
	            Environment
	                    .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
	            IMAGE_DIRECTORY_NAME);
	 //Log.d("json response exception", "json response exception  mdeia storage    "+mediaStorageDir.getPath()) ;
	    // Create the storage directory if it does not exist
	    if (!mediaStorageDir.exists()) {
	        if (!mediaStorageDir.mkdirs()) {
	            //Log.d(IMAGE_DIRECTORY_NAME, "Oops! Failed create "      + IMAGE_DIRECTORY_NAME + " directory");
	            return null;
	        }
	    }

	    // Create a media file name


	    File mediaFile;
	    if (type == MEDIA_TYPE_IMAGE) {
	        mediaFile = new File(mediaStorageDir.getPath() + File.separator
	                + "IMG_" +imageCounter  + ".jpg");
	    } else if (type == MEDIA_TYPE_VIDEO) {
	        mediaFile = new File(mediaStorageDir.getPath() + File.separator
	                + "VID_" + imageCounter + ".mp4");
	    } else {
	    	//Log.d("adding image ", "adding image to response null returned ") ;
	        return null;
	    }
	    try {
			mediaFile.createNewFile() ;
		} catch (IOException e) {
			// TODO: handle exception
			//Log.d("adding image ", "adding image faioled to create file  "+e.getMessage()			) ;
			return null ;
		}

	    selectedImagePath.add(mediaStorageDir.getPath() + File.separator
	                + "IMG_" +imageCounter  + ".jpg") ;
	    //Log.d("adding image ", "adding image to response incrementation ") ;
	    imageCounter++ ;
	    return mediaFile;
	}
	public String md5(String s) {
        try {
            // Create MD5 Hash
            MessageDigest digest = java.security.MessageDigest.getInstance("MD5");
            digest.update(s.getBytes());
            byte messageDigest[] = digest.digest();

            /* Create Hex String */
            StringBuffer hexString = new StringBuffer();
            for (int i=0; i<messageDigest.length; i++)
                hexString.append(Integer.toHexString(0xFF & messageDigest[i]));
            return hexString.toString();

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return "";
    }


	@TargetApi(Build.VERSION_CODES.KITKAT)
	public static String getPath(final Context context, final Uri uri) {

	    final boolean isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;

	    // DocumentProvider
	    if (isKitKat && DocumentsContract.isDocumentUri(context, uri)) {
	        // ExternalStorageProvider
	        if (isExternalStorageDocument(uri)) {
	            final String docId = DocumentsContract.getDocumentId(uri);
	            final String[] split = docId.split(":");
	            final String type = split[0];

	            if ("primary".equalsIgnoreCase(type)) {
	                return Environment.getExternalStorageDirectory() + "/" + split[1];
	            }

	            // TODO handle non-primary volumes
	        }
	        // DownloadsProvider
	        else if (isDownloadsDocument(uri)) {

	            final String id = DocumentsContract.getDocumentId(uri);
	            final Uri contentUri = ContentUris.withAppendedId(
	                    Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));

	            return getDataColumn(context, contentUri, null, null);
	        }
	        // MediaProvider
	        else if (isMediaDocument(uri)) {
	            final String docId = DocumentsContract.getDocumentId(uri);
	            final String[] split = docId.split(":");
	            final String type = split[0];

	            Uri contentUri = null;
	            if ("image".equals(type)) {
	                contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
	            } else if ("video".equals(type)) {
	                contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
	            } else if ("audio".equals(type)) {
	                contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
	            }

	            final String selection = "_id=?";
	            final String[] selectionArgs = new String[] {
	                    split[1]
	            };

	            return getDataColumn(context, contentUri, selection, selectionArgs);
	        }
	    }
	    // MediaStore (and general)
	    else if ("content".equalsIgnoreCase(uri.getScheme())) {
	        return getDataColumn(context, uri, null, null);
	    }
	    // File
	    else if ("file".equalsIgnoreCase(uri.getScheme())) {
	        return uri.getPath();
	    }

	    return null;
	}

	/**
	 * Get the value of the data column for this Uri. This is useful for
	 * MediaStore Uris, and other file-based ContentProviders.
	 *
	 * @param context The context.
	 * @param uri The Uri to query.
	 * @param selection (Optional) Filter used in the query.
	 * @param selectionArgs (Optional) Selection arguments used in the query.
	 * @return The value of the _data column, which is typically a file path.
	 */
	public static String getDataColumn(Context context, Uri uri, String selection,
	        String[] selectionArgs) {

	    Cursor cursor = null;
	    final String column = "_data";
	    final String[] projection = {
	            column
	    };

	    try {
	        cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs,
	                null);
	        if (cursor != null && cursor.moveToFirst()) {
	            final int column_index = cursor.getColumnIndexOrThrow(column);
	            return cursor.getString(column_index);
	        }
	    } finally {
	        if (cursor != null)
	            cursor.close();
	    }
	    return null;
	}


	/**
	 * @param uri The Uri to check.
	 * @return Whether the Uri authority is ExternalStorageProvider.
	 */
	public static boolean isExternalStorageDocument(Uri uri) {
	    return "com.android.externalstorage.documents".equals(uri.getAuthority());
	}

	/**
	 * @param uri The Uri to check.
	 * @return Whether the Uri authority is DownloadsProvider.
	 */
	public static boolean isDownloadsDocument(Uri uri) {
	    return "com.android.providers.downloads.documents".equals(uri.getAuthority());
	}

	/**
	 * @param uri The Uri to check.
	 * @return Whether the Uri authority is MediaProvider.
	 */
	public static boolean isMediaDocument(Uri uri) {
	    return "com.android.providers.media.documents".equals(uri.getAuthority());
	}
	/** Create a File for saving an image or video */
	private  File getOutputMediaFile(){
		// To be safe, you should check that the SDCard is mounted
		// using Environment.getExternalStorageState() before doing this.
		File mediaStorageDir = new File(
				Environment
						.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
				IMAGE_DIRECTORY_NAME);

		// This location works best if you want the created images to be shared
		// between applications and persist after your app has been uninstalled.

		// Create the storage directory if it does not exist
		if (! mediaStorageDir.exists()){
			if (! mediaStorageDir.mkdirs()){
				return null;
			}
		}
		// Create a media file name
		String timeStamp = new SimpleDateFormat("ddMMyyyy_HHmm").format(new Date());
		File mediaFile;
		String mImageName="MI_"+ timeStamp +".jpg";
		mediaFile = new File(mediaStorageDir.getPath() + File.separator + mImageName);
		return mediaFile;
	}

	private void storeImage(Bitmap image) {
		File pictureFile = getOutputMediaFile();
		if (pictureFile == null) {
			Log.d("text ",
					"Error creating media file, check storage permissions: ");// e.getMessage());
			return;
		}
		try {
			FileOutputStream fos = new FileOutputStream(pictureFile);
			image.compress(Bitmap.CompressFormat.PNG, 90, fos);
			fos.close();
		} catch (FileNotFoundException e) {
			Log.d("text ", "File not found: " + e.getMessage());
		} catch (IOException e) {
			Log.d("text ", "Error accessing file: " + e.getMessage());
		}
	}


	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);

		if(requestCode == RESULT_CODE_CONTACT){
			if(data!= null){


				if (resultCode == Activity.RESULT_OK) {
			        Uri contactData = data.getData();
			        Cursor c =  getActivity().getContentResolver().query(contactData, null, null, null, null);
			        if (c.moveToFirst()) {
			        	 String name=c.getString(c.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
			        	 int contact_id  =c.getInt(c.getColumnIndex(ContactsContract.Contacts._ID)) ;

			        	 String email= "" ;
			        	 Cursor contact  = getActivity().getContentResolver().query(ContactsContract.CommonDataKinds.Email.CONTENT_URI, null, ContactsContract.CommonDataKinds.Email.CONTACT_ID+"= ?", new String[]{String.valueOf(contact_id)}, null)  ;

			        	 while (contact.moveToNext()) {
			        		 email= contact.getString(contact.getColumnIndex(ContactsContract.CommonDataKinds.Email.ADDRESS));
						}
				          String phoneNumber = "";
				          contact.close() ;
				          contact  = getActivity().getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, ContactsContract.CommonDataKinds.Phone.CONTACT_ID+"= ?", new String[]{String.valueOf(contact_id)}, null)  ;

				        	 while (contact.moveToNext()) {
				        		 phoneNumber= contact.getString(contact.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
							}
				        	 contact.close() ;
			        	 String address = "" ;
			        	 String city  = ""  ;
			        	 String zipCode  = "" ;
			        	 String country = ""  ;
			        	 contact  = getActivity().getContentResolver().query(ContactsContract.Data.CONTENT_URI, new String[]{ StructuredPostal.STREET,
			        		        StructuredPostal.CITY,StructuredPostal.POSTCODE , StructuredPostal.COUNTRY}, ContactsContract.Data.CONTACT_ID+"= ? AND " +
            StructuredPostal.MIMETYPE + "=?", new String[]{String.valueOf(contact_id),  StructuredPostal.CONTENT_ITEM_TYPE}, null)  ;

			        	 while (contact.moveToNext()) {
			        		 address= contact.getString(contact.getColumnIndex(ContactsContract.CommonDataKinds.StructuredPostal.STREET));
			        		 city= contact.getString(contact.getColumnIndex(ContactsContract.CommonDataKinds.StructuredPostal.CITY));
			        		 zipCode= contact.getString(contact.getColumnIndex(ContactsContract.CommonDataKinds.StructuredPostal.POSTCODE));
			        		 country= contact.getString(contact.getColumnIndex(ContactsContract.CommonDataKinds.StructuredPostal.COUNTRY));

			        	 }
			        	 contact.close() ;
			        	 String fname = "";
			        	 String lname  = "" ;
			        	 String company= ""  ;
			        	 String fonction = "" ;
			        	 contact  = getActivity().getContentResolver().query(ContactsContract.Data.CONTENT_URI, new String[]{ CommonDataKinds.StructuredName.FAMILY_NAME,
			        		        CommonDataKinds.StructuredName.GIVEN_NAME}, ContactsContract.Data.CONTACT_ID+"= ? AND "+ContactsContract.Data.MIMETYPE+"= ?", new String[]{String.valueOf(contact_id) , CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE}, null)  ;

			        	 while (contact.moveToNext()) {
			        	 		lname=contact.getString(contact.getColumnIndex(ContactsContract.CommonDataKinds.StructuredName.FAMILY_NAME));
			        	 		fname=contact.getString(contact.getColumnIndex(ContactsContract.CommonDataKinds.StructuredName.GIVEN_NAME));
				         }
			        	 contact.close() ;

			        	 contact  = getActivity().getContentResolver().query(ContactsContract.Data.CONTENT_URI, new String[]{ CommonDataKinds.Organization.COMPANY,
			        		        CommonDataKinds.Organization.TITLE}, ContactsContract.Data.CONTACT_ID+"= ? AND "+ContactsContract.Data.MIMETYPE+"= ?", new String[]{String.valueOf(contact_id) , CommonDataKinds.Organization.CONTENT_ITEM_TYPE}, null)  ;

			        	 while (contact.moveToNext()) {
			        		 company= contact.getString(contact.getColumnIndex(ContactsContract.CommonDataKinds.Organization.COMPANY));
					         fonction= contact.getString(contact.getColumnIndex(ContactsContract.CommonDataKinds.Organization.TITLE));
				         }
			        	 contact.close() ;
			          Log.d("get contact", "get contact " + name + " " + lname + " " + fname + " " + email + " " + phoneNumber + " " + address + " " + company + " " + fonction + " " + country + " " + city + " " + zipCode) ;
			          JSONObject contactJson =  new JSONObject() ;
			          try {

			        	  contactJson.put("PHONE", phoneNumber)  ;
			        	  contactJson.put("FNAME"	, fname) ;
			        	  contactJson.put("LNAME"	, lname) ;
			        	  contactJson.put("EMAIL", email) ;
			        	  contactJson.put("COMPANY", company) ;
			        	  contactJson.put("TITLE", fonction) ;
			        	  contactJson.put("ADDRESS", address) ;
			        	  contactJson.put("CITY", city) ;
			        	  contactJson.put("ZIPCODE", zipCode) ;
			        	  contactJson.put("COUNTRY", country) ;
Log.d("details ", fname + lname + email + company + fonction + address + city + zipCode + country);
			        	  fillFromTheAgenda(contactJson) ;
				    } catch (JSONException e) {
						// TODO: handle exception
						//Log.d("phone list ", "phone list jsonexception "+e.getMessage()) ;
					}

			        }
			      }

			}

		}


		if(requestCode == REQUEST_CODE_FUNCTION ){
				String ocr_Result = String.valueOf( BiptagUserManger.getUserValue("ocrResult","string",NavigationActivity.getContext()));
			if(!ocr_Result.equals("")){
				String resultObject  =  String.valueOf(BiptagUserManger.getUserValue("ocrResult", "string", NavigationActivity.getContext()));
				final String originObject  =  String.valueOf(BiptagUserManger.getUserValue("ocrOrigin", "string", NavigationActivity.getContext()));
				BiptagUserManger.saveUserData("ocrResultCreat", originObject,NavigationActivity.getContext());
				BiptagUserManger.clearField(NavigationActivity.getContext(),"ocrResult");
				Log.d("ocr result", "contact  origine object" + originObject + " " + ocr_Result);
				try {

				final JSONObject object = new JSONObject(resultObject) ;
				Log.d("object",object.toString());
				fillFromTheAgenda(object) ;

				Bundle bundle = new Bundle() ;

				bundle.putString("ocr_result", originObject) ;
				bundle.putString("contact", object.toString()) ;
				Log.d("ocr result ", "contact object " + object.toString());
				Log.d(" ocr result  ", " bundle " + bundle.getString("contact"));


				BiptagUserManger.saveUserData("ocr_result" , originObject.toString() , NavigationActivity.getContext());
				BiptagUserManger.saveUserData("updated_contact_creat" , object.toString() , NavigationActivity.getContext());


				EditOCRFragment editOCRFragment = new EditOCRFragment(null,false,1) ;
				bundle.putInt("Create_Mode", 5);
				editOCRFragment.setArguments(bundle);
				BiptagUserManger.clearField(NavigationActivity.getContext(), "updated_contact") ;
				Log.d(" ocr result  ", " bundle get " + editOCRFragment.getArguments().getString("contact"));
				BiptagNavigationManager.pushFragments(editOCRFragment, true, editOCRFragment.getClass().getName(), NavigationActivity.getContext(), null, 1) ;
				Log.d("ocr result ", originObject);
				} catch (JSONException e) {
					// TODO: handle exception
				}
				EditOCRFragment editOCRFragment = new EditOCRFragment(null,false,1) ;
				Bundle bundle = new Bundle() ;
				bundle.putInt("Create_Mode", 5);
				editOCRFragment.setArguments(bundle);
				BiptagUserManger.clearField(NavigationActivity.getContext(), "updated_contact") ;
				BiptagNavigationManager.pushFragments(editOCRFragment, true, editOCRFragment.getClass().getName(), NavigationActivity.getContext(), null, 1) ;

			}else {
				Log.d(" ocr object ", " data null ");
			}
			BiptagUserManger.clearField(NavigationActivity.getContext(),"ocrResult");

		}

		if(requestCode ==  REQUEST_PICK_PHOTO){
			Log.d("text ", " request pick photo");
			if(imageUri != null){
					ContentResolver resolver = NavigationActivity.getContext().getContentResolver() ;

					if(hasImageCaptureBug()){
						File file = new File(NavigationActivity.getContext().getExternalCacheDir().getAbsolutePath()+"/.temp/" ) ;
						try {
							imageUri = Uri.parse(android.provider.MediaStore.Images.Media.insertImage(resolver, file.getAbsolutePath(), null, null));
						} catch (FileNotFoundException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}


					Bitmap bitmap = null;

					try {

						bitmap = android.provider.MediaStore.Images.Media.getBitmap(resolver, imageUri);
						} catch (FileNotFoundException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}


					if(bitmap != null){



						vcardBitMap = bitmap ;
						//	vcardbitmapPath = imagePath ;

							int agent_id= Integer.parseInt(String.valueOf(BiptagUserManger.getUserValue("BA_ID", "int", NavigationActivity.getContext()))) ;
			        		Calendar c = Calendar.getInstance();
			        		SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
			        		String formattedDate = df.format(c.getTime());
			                BTImage image = new BTImage(NavigationActivity.getContext()) ;
			                image.setAgentId(agent_id) ;
			                image.setImageDate(formattedDate) ;
			                image.setResponseId(0) ;
			                image.setIsSent(-1) ;
							//image.setImageEmail(email);
			                image.setBitmap(vcardBitMap) ;
			                image.saveBitmap();
						Log.d("text ", "image saved 2");
			                BTImageDataSource dataSource = new BTImageDataSource(NavigationActivity.getContext()) ;
			                long imageId = 0 ;
			                BTImage dbImage = dataSource.selectImagesWithId(vcardImageId, agent_id) ;
						String imageJsonString = String.valueOf(BiptagUserManger.getUserValue("images_for_user", "string", NavigationActivity.getContext())) ;

						JSONArray	imagesJson  = null  ;
						int oldIndex =-1 ;
						try {
							if(imageJsonString.length() != 0){
								imagesJson = new JSONArray(imageJsonString) ;
							}else{
								imagesJson = new JSONArray() ;
							}
									//  for
							for (int i=0; i<=imagesJson.length(); i++){
								if (vcardImageId==imagesJson.getInt(i)){
									oldIndex = i;
								}
							}
						} catch (JSONException e) {
							// TODO Auto-generated catch block

						}
						//

						if( dbImage== null )
			                vcardImageId  = (int) dataSource.saveImage(image) ;
			                else{
			                	vcardImageId = dbImage.getImageId() ;
			                	image.setImageId((int) vcardImageId) ;
			                	boolean update =dataSource.updateImage(image) ;
			                //	Log.d("bdimage", "bdimage upadte "+update) ;

			                }


						try {
							if (oldIndex!=-1){
								imagesJson.put(oldIndex, vcardImageId);
							}
							else{
								imagesJson.put(vcardImageId) ;
							}

				       		 //Log.d("sendin image string", "sendin image string "+imagesJson.toString()) ;
				       		 BiptagUserManger.saveUserData("images_for_user", imagesJson.toString(), NavigationActivity.getContext()) ;

				       		} catch (JSONException e) {
				       			// TODO Auto-generated catch block

				       		}

							TessOCR ocr = new TessOCR(bitmap, NavigationActivity.getContext(), FillingFormStep1.this)  ;
							ocr.execute() ;



					}






			}
		}


	}


	public  JSONObject decodeString(String ocrResult){

	     String[] lines = ocrResult.split("\n") ;

			Log.d("decodeString", "lines   " + lines.length) ;
			JSONObject object = new JSONObject() ;

				String EMAIL = "" ;
				String phone  = "";
				String firstName = "" ;
				String lastName = "" ;
				String fullName = "" ;
				String fonction = "" ;
				String societe = "" ;
				String address="" ;
				String zipcode =""  ;
				String city = "";
				String country = ""  ;
				EmailValidator emailValidator = new EmailValidator();
				PhoneValidator phonevalid=new PhoneValidator();
				FullNameValidator fullNamevalid = new FullNameValidator();

	     for (int i = 0; i < lines.length; i++) {

	    	 String origineLigne = lines[i] ;
	    	 lines[i]=lines[i].replaceAll(" " , "") ;
	    	String emailCondiate = origineLigne ;
	    	emailCondiate=emailCondiate.replace("©" , "@") ;
	    	emailCondiate=emailCondiate.replace("®" , "@") ;

	    	emailCondiate=emailCondiate.replace("E-MAIL :" , "") ;
	    	emailCondiate=emailCondiate.replace("e-mail :" , "") ;
	    	emailCondiate=emailCondiate.replace("email :" , "") ;
	    	emailCondiate=emailCondiate.replace("EMAIL :" , "") ;

			emailCondiate=emailCondiate.replace("E-MAIL:" , "") ;
	    	emailCondiate=emailCondiate.replace("e-mail:" , "") ;
	    	emailCondiate=emailCondiate.replace("email:" , "") ;
	    	emailCondiate=emailCondiate.replace("EMAIL:" , "") ;

	    	emailCondiate=emailCondiate.replace("E-MAIL" , "") ;
	    	emailCondiate=emailCondiate.replace("e-mail" , "") ;
	    	emailCondiate=emailCondiate.replace("email" , "") ;
	    	emailCondiate=emailCondiate.replace("EMAIL" , "") ;

	    	//System.out.println("email condidat : " +emailCondiate);
	    	//Log.println(0, "ocr email condidate : ",emailCondiate);


			Log.d("lines", "lines " + i + "  " + lines[i]);


			boolean validMail = emailValidator.validate(emailCondiate);
			origineLigne = lines[i] ;
			String phoneCondiate = lines[i] ;


	    	Log.d("lines", "ocr phone  condidate origine "+phoneCondiate);

			phoneCondiate = phoneCondiate.replaceAll("MOBILE", "") ;

			phoneCondiate = phoneCondiate.replaceAll("PHONE", "") ;
			phoneCondiate = phoneCondiate.replaceAll("FAX", "") ;
			phoneCondiate = phoneCondiate.replaceAll("DIRECT", "") ;
			phoneCondiate = phoneCondiate.replaceAll("Mobile", "") ;
			phoneCondiate = phoneCondiate.replaceAll("Phone", "") ;
			phoneCondiate = phoneCondiate.replaceAll("Fax", "") ;
			phoneCondiate = phoneCondiate.replaceAll("Direct", "") ;

			phoneCondiate = phoneCondiate.replaceAll("mobile", "") ;
			phoneCondiate = phoneCondiate.replaceAll("phone", "") ;
			phoneCondiate = phoneCondiate.replaceAll("fax", "") ;
			phoneCondiate = phoneCondiate.replaceAll("direct", "") ;

			phoneCondiate = phoneCondiate.replaceAll("-", "") ;
			phoneCondiate = phoneCondiate.replaceAll(":", "") ;

			String phoneCondiateOrigine = phoneCondiate ;

			phoneCondiate = phoneCondiate.replaceAll("\\.", "") ;
			phoneCondiate = phoneCondiate.replaceAll("\\(", "") ;
			phoneCondiate = phoneCondiate.replaceAll("\\)", "") ;
			phoneCondiate = phoneCondiate.replaceAll("\\+", "") ;
			phoneCondiate = phoneCondiate.replaceAll(" ", "") ;


	    	//Log.d("lines", "ocr phone  condidate "+phoneCondiate);

			boolean validphone = phonevalid.validate(phoneCondiate);
			boolean validfullName= fullNamevalid.validate(lines[i]);

			 /*
			boolean validUrl = urlvalid.validate(lines[i]);
			boolean validAdress = emailvalid.validate(lines[i]);
			*/
			if (validMail){
			//Log.d("" ,"ocr Email is HERE " + emailCondiate+"at line "+ i);
			EMAIL= emailCondiate;
			}

			if(validphone){
				//Log.d("" ,"ocr phone is HERE " + phoneCondiate+"at line "+ i);

				phone = phoneCondiateOrigine ;
			}
			if(validfullName && fullName.equals("") ){
				firstName = lines[i].split(" ")[0] ;
				if(lines[i].split(" ").length > 1){
					lastName = lines[i].split(" ")[1] ;
				}
			}else{
				if(validfullName && fonction.equals("") ){
					fonction= lines[i];

				}
			}





	     }


	    try {
			object.put("ocr", ocrResult)  ;
			object.put("EMAIL", EMAIL)	 ;
			object.put("PHONE", phone)	 ;
			object.put("FNAME", firstName) ;
			object.put("LNAME", lastName) ;
			object.put("TITLE", fonction) ;
			object.put("COMPANY", societe) ;
			object.put("ADDRESS", address) ;
			object.put("ZIPCODE", zipcode) ;
			object.put("CITY", city) ;
			object.put("COUNTRY", country) ;

		} catch (JSONException e) {
			// TODO: handle exception
		}





	    return object ;
	}

	private Point getPointOfView(View view) {
		int[] location = new int[2];
		view.getLocationInWindow(location);
		return new Point(location[0], location[1]);
	}





	  public String getPath(Uri uri) {
          // just some safety built in
          if( uri == null ) {
              // TODO perform some logging or show user feedback
              return null;
          }
          // try to retrieve the image from the media store first
          // this will only work for images selected from gallery
          String[] projection = { MediaStore.Images.Media.DATA };
          Cursor cursor = NavigationActivity.getContext().getContentResolver().query(uri, projection, null, null, null);
          if( cursor != null ){
              int column_index = cursor
              .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
              cursor.moveToFirst();
              return cursor.getString(column_index);
          }
          // this is our fallback here
          return uri.getPath();
  }
// LZR : 14/04/2016 set the data of contact in text view of the step1 activity
	public void fillFromTheAgenda(JSONObject contactChoisen  ){


		Log.d("contact chosen", "contact chosen "+contactChoisen.toString()) ;

		for (int i = 0; i < list.size(); i++) {



			try {
				if(list.get(i)[0].equals("141")){
					EditText editText= (EditText) fragmentView.findViewById(Integer.parseInt(list.get(i)[2])) ;
					editText.setText(contactChoisen.getString("ZIPCODE")) ;
				}
				if(list.get(i)[0].equals("142")){
					EditText editText= (EditText) fragmentView.findViewById(Integer.parseInt(list.get(i)[2])) ;
					editText.setText(contactChoisen.getString("CITY")) ;
				}
				if(list.get(i)[0].equals("143")){
					EditText editText= (EditText) fragmentView.findViewById(Integer.parseInt(list.get(i)[2])) ;
					editText.setText(contactChoisen.getString("COUNTRY")) ;
				}
				int viewid  = Integer.parseInt(list.get(i)[2]) ;

				switch (i) {
					case 3: {EditText editText= (EditText) getView().findViewById(viewid) ;
						editText.setText(contactChoisen.getString("LNAME")) ;
					Log.d(" contact " , contactChoisen.getString("LNAME") + " " + i);
					} break;
					case 5: {EditText editText= (EditText) getView().findViewById(viewid) ;
						editText.setText(contactChoisen.getString("FNAME")) ;
						Log.d(" contact ", contactChoisen.getString("FNAME") + " " + i);
					} break;
					case 7: {EditText editText= (EditText) getView().findViewById(viewid) ;
						editText.setText(contactChoisen.getString("EMAIL")) ;} break;
					case 9: {EditText editText= (EditText) getView().findViewById(viewid) ;
						editText.setText(contactChoisen.getString("COMPANY")) ;} break;
					case 11:{EditText editText= (EditText) getView().findViewById(viewid) ;
						editText.setText(contactChoisen.getString("TITLE")) ;}  break;
					case 13:{EditText editText= (EditText) getView().findViewById(viewid) ;
						editText.setText(contactChoisen.getString("ADDRESS")) ;}  break;
					case 18:{EditText editText= (EditText) getView().findViewById(viewid) ;
						editText.setText(contactChoisen.getString("PHONE")) ;}  break;

					case 17:{
						try {
							EditText editText= (EditText) getView().findViewById(viewid) ;
							editText.setText(contactChoisen.getString("PHONE")) ;
						} catch (Exception e) {
							// TODO: handle exception
						}
					}  break;

					case 14:{}  break;


					default:
						break;
				}
			} catch (JSONException e) {
				// TODO: handle exception
				Log.d("contact chosen exception ", "contact chosen exception "+e.getMessage());
			}


		}



		/*
		int viewid= Integer.parseInt(list.get(3)[2]) ;
		int lastnameviewid= Integer.parseInt(list.get(5)[2]) ;
		int phoneviewid= Integer.parseInt(list.get(15)[2]) ;

		String[] nameSplit = "".split(" ") ;
		EditText namefield = (EditText) getView().findViewById(viewid) ;
		EditText lastnamefield = (EditText) getView().findViewById(lastnameviewid) ;
		EditText phoneField= (EditText) getView().findViewById(phoneviewid) ;
		phoneField.setText("") ;
		if(nameSplit.length > 1){
			namefield.setText(nameSplit[0]) ;
			lastnamefield.setText(nameSplit[1]) ;
		}else {
			namefield.setText("") ;
			lastnamefield.setText("") ;

		}*/


	}


	@Override
	public void processFinish(String result) {
		// TODO Auto-generated method stub

		if(result!= null){

			final  JSONObject chosenContactBuilt = decodeString(result) ;

			//Toast.makeText(this,"result : " + chosenContactBuilt.getString("EMAIL") ,Toast.LENGTH_LONG).show();

			try {
				if(chosenContactBuilt.getString("EMAIL").length() != 0){

					 fillFromTheAgenda(chosenContactBuilt) ;
					    if(popupWindow != null){
					    	popupWindow.dismiss() ;
					    }


					Bundle bundle = new Bundle();
					bundle.putString("ocr_result", result) ;
					bundle.putString("contact", chosenContactBuilt.toString()) ;
					Log.d("ocr result ", "contact object finish " + result);
					Log.d("ocr result", "contact  origine object" + chosenContactBuilt.toString());
					//	EditOCRFragment.fragmentView = null;
					EditOCRFragment editOCRFragment = new EditOCRFragment(null,false,1) ;
					bundle.putInt("Create_Mode", 5);
					editOCRFragment.getArguments().clear();
					BiptagUserManger.clearField(NavigationActivity.getContext(), "updated_contact") ;

				//	editOCRFragment.setArguments(bundle) ;
			//	Log.d(" ocr result  ", " bundle " + editOCRFragment.getArguments().getString("contact"));
					BiptagNavigationManager.pushFragments(editOCRFragment, true, editOCRFragment.getClass().getName(), NavigationActivity.getContext(), null, 1) ;


				}else{

					AlertDialog.Builder builder = new AlertDialog.Builder(NavigationActivity.getContext()) ;
					builder.setTitle(NavigationActivity.getContext().getResources().getString(R.string.ocr_incomplete_reco_title)) ;
					builder.setMessage(NavigationActivity.getContext().getResources().getString(R.string.ocr_incomplete_reco_msg)) ;
					builder.setPositiveButton(NavigationActivity.getContext().getResources().getString(R.string.ocr_continue), new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {
							// TODO Auto-generated method stub
							dialog.dismiss() ;
						}
					}) ;

					builder.setNegativeButton(NavigationActivity.getContext().getResources().getString(R.string.ocr_repeat), new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {
							// TODO Auto-generated method stub

							dialog.dismiss() ;
							 Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
							ocrPopupWindow.dismiss();
							startActivityForResult(intent, REQUEST_PICK_PHOTO);

						}
					}) ;


					builder.show() ;



				}



			} catch (JSONException e) {
				// TODO: handle exception
			}


			/*

			Button btn1 = (Button) fragmentView.findViewById(R.id.fildaraiane_step1_button1) ;

			LayoutInflater layoutInflater = LayoutInflater.from(getActivity()) ;
			final View view =  layoutInflater.inflate(R.layout.ocr_action_bar, null) ;
			final PopupWindow ocrPopupWindow  = new PopupWindow(view , screenWidth , btn1.getHeight() )  ;
			int[] location ={0,0}  ;


			((Button) view.findViewById(R.id.ocr_ok_button)).setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					ocrPopupWindow.dismiss() ;
				}
			}) ;

			final String ocrResult = result ;
			((Button) view.findViewById(R.id.ocr_edit_button)).setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					ocrPopupWindow.dismiss() ;
					Bundle bundle = new Bundle() ;
					bundle.putString("ocr_result", ocrResult) ;
					bundle.putString("contact", chosenContactBuilt.toString()) ;
					EditOCRFragment editOCRFragment = new EditOCRFragment() ;


					editOCRFragment.setArguments(bundle) ;
					BiptagNavigationManager.pushFragments(editOCRFragment, true, editOCRFragment.getClass().getName(), NavigationActivity.getContext(), null, 1) ;


				}
			}) ;


			btn1.getLocationOnScreen(location) ;
			ocrPopupWindow.showAtLocation(view, Gravity.NO_GRAVITY, location[0], location[1]+btn1.getHeight()) ;


			*/



		}

	}


	@Override
	public void processFailed() {
		// TODO Auto-generated method stub

		AlertDialog.Builder builder = new AlertDialog.Builder(NavigationActivity.getContext()) ;

		builder.setMessage("Aucune donnée récuperée") ;
		builder.setTitle("Erruer de reconnaissance") ;
		builder.setPositiveButton("Ok", new OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				dialog.dismiss() ;
			}
		}) ;
		builder.show() ;


	}


	@Override
	public void synchronisationStepUp() {
		// TODO Auto-generated method stub

	}




}
