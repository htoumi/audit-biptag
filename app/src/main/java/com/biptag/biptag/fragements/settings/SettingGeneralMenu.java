package com.biptag.biptag.fragements.settings;

import java.util.Locale;

import android.app.Fragment;
import android.content.Context;
import android.graphics.Point;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ListView;
import android.widget.TextView;

import com.biptag.biptag.NavigationActivity;
import com.biptag.biptag.R;
import com.biptag.biptag.dbmanager.objects.BTUserConfig;
import com.biptag.biptag.managers.BipTagUserConfigurationManger;
import com.biptag.biptag.managers.BiptagNavigationManager;
import com.biptag.biptag.managers.BiptagUserManger;

public class SettingGeneralMenu extends Fragment {
	ListView general_menu_listview ; 
	String[] menu  ; 
	String[] subItems ; 
	BTUserConfig userConfig ; 
	int agentId ; 
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		final View fragmentView = inflater.inflate(R.layout.general_setting_layout,null);
		//Log.d("general ", "general 1") ;
		general_menu_listview = (ListView) fragmentView.findViewById(R.id.general_menu_listview) ; 

		
		TextView separator = (TextView) fragmentView.findViewById(R.id.general_menu_seprator) ; 
		LayoutParams  layoutParams = (LayoutParams)separator.getLayoutParams() ; 
		   WindowManager wm = (WindowManager) getActivity().getSystemService(Context.WINDOW_SERVICE);
			Display display  =wm.getDefaultDisplay() ;
			Point point  = new Point() ; 
			display.getSize(point) ; 
			int screenWidth = point.x ; 
			layoutParams.setMargins(screenWidth/40,0, screenWidth/40, 0) ; 
			separator.setLayoutParams(layoutParams)  ; 
			  
			TextView title = (TextView) fragmentView.findViewById(R.id.general_menu_title); 
			//title.setTextSize(screenWidth/30) ;
			title.setTextSize(NavigationActivity.getContext().getResources().getDimension(R.dimen.acc_info_size));
			LayoutParams params = (LayoutParams) title.getLayoutParams() ; 
			params.setMargins(screenWidth/40,0, screenWidth/40, 0) ;
			title.setLayoutParams(params) ; 
			
			general_menu_listview.setOnItemClickListener(new OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> parent, View view,
										int position, long id) {
					// TODO Auto-generated method stub
					SettingChangeGeneral general;
					boolean isChangeLanguageMode = position == 0;
					general = new SettingChangeGeneral(isChangeLanguageMode, agentId);
					BiptagNavigationManager.pushFragments(general, true, "change_general", getActivity(), null, 3);

				}
			}) ;

		Button actionBarButton = (Button)	getActivity().getActionBar().getCustomView().findViewById(R.id.formste1_btn_container) ;
		actionBarButton.setBackground(NavigationActivity.getContext().getResources().getDrawable(R.drawable.go_back));

		//actionBarButton.setText(NavigationActivity.getContext().getResources().getString(R.string.return_image_list));

		actionBarButton.setVisibility(View.VISIBLE) ;
		actionBarButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				SettingMenuFragment settingMenuFragment = new SettingMenuFragment();
				BiptagNavigationManager.pushFragments(settingMenuFragment, true, this.getClass().getName(), getActivity(), null, 3);

			}
		}) ;


		return fragmentView;
	}
	
	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		menu = new String[]{
				NavigationActivity.getContext().getResources().getString(R.string.languages_lbl)  , NavigationActivity.getContext().getResources().getString(R.string.default_countries_setting_lbl) 
		} ;  
		String agentIdString = String.valueOf(BiptagUserManger.getUserValue("AGENT_ID", "int", getActivity())) ; 
		
		agentId = Integer.parseInt(agentIdString) ; 
		//Log.d("BTUserConfig(", "create BTUserConfig settingGeneral/Menu") ;
		userConfig  = BipTagUserConfigurationManger.getUserConfig(agentId) ; 
		
		String language  = userConfig.getLanguage() ; 
		if(language.equals("")){
			language =Locale.getDefault().getDisplayLanguage() ; 
		}
		String defaultCountry = userConfig.getDefaultCountry() ; 
		if(defaultCountry == null || defaultCountry.length() == 0){
			defaultCountry = "France" ; 
		}
		
		subItems = new String[]{
				language ,defaultCountry
		} ;
		
		SettingGeneralMenuAdapter adapter = new SettingGeneralMenuAdapter(getActivity(), android.R.layout.simple_list_item_1, menu, subItems) ; 
		general_menu_listview.setAdapter(adapter) ;

		Button actionBarButton = (Button)	getActivity().getActionBar().getCustomView().findViewById(R.id.formste1_btn_container) ;
		actionBarButton.setBackground(NavigationActivity.getContext().getResources().getDrawable(R.drawable.go_back));

		//actionBarButton.setText(NavigationActivity.getContext().getResources().getString(R.string.return_image_list));

		actionBarButton.setVisibility(View.VISIBLE) ;

	}

	@Override
	public void onStop() {
		super.onStop();

		Button actionBarButton = (Button)	getActivity().getActionBar().getCustomView().findViewById(R.id.formste1_btn_container) ;
		actionBarButton.setBackground(NavigationActivity.getContext().getResources().getDrawable(R.drawable.go_back));

		//actionBarButton.setText(NavigationActivity.getContext().getResources().getString(R.string.return_image_list));

		actionBarButton.setVisibility(View.INVISIBLE) ;

	}
}
