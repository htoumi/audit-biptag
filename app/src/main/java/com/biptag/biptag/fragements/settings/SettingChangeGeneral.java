package com.biptag.biptag.fragements.settings;

import java.util.Locale;

import org.json.JSONArray;
import org.json.JSONException;

import android.annotation.SuppressLint;
import android.app.Fragment;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Point;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ListView;
import android.widget.TextView;

import com.biptag.biptag.NavigationActivity;
import com.biptag.biptag.R;
import com.biptag.biptag.dbmanager.objects.BTUserConfig;
import com.biptag.biptag.fragements.ContactFragement;
import com.biptag.biptag.fragements.create.FillingFormStep1;
import com.biptag.biptag.fragements.create.FillingFormStep2;
import com.biptag.biptag.fragements.edit.EditFormStep1;
import com.biptag.biptag.fragements.edit.EditFormStep2;
import com.biptag.biptag.managers.BipTagFormsManager;
import com.biptag.biptag.managers.BipTagUserConfigurationManger;
import com.biptag.biptag.managers.BiptagNavigationManager;

public class SettingChangeGeneral extends Fragment {
	ListView listView  ; 
	TextView title ; 
	String[] values ; 
	SettingChangeGeneralAdapter adapter ;
	int agentId  ;
	BTUserConfig userConfig ; 
	boolean changeLanguageMode ;
	public SettingChangeGeneral(){

	}
	@SuppressLint({"NewApi", "ValidFragment"})
	public SettingChangeGeneral(boolean changeLanguageMode, int agentid ) {
		// TODO Auto-generated constructor stub
		this.changeLanguageMode = changeLanguageMode ; 
		agentId = agentid ; 
		//Log.d("BTUserConfig(", "create BTUserConfig settingchangegeneral") ;
		
	}

 
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		final View fragmentView = inflater.inflate(R.layout.setting_change_general_layout,null);
		listView = (ListView) fragmentView.findViewById(R.id.general_change_listview)  ; 
		TextView separator = (TextView) fragmentView.findViewById(R.id.general_change_seprator) ; 
		userConfig  = BipTagUserConfigurationManger.getUserConfig(agentId) ; 
		if(userConfig.getConfigId() == 0){
			userConfig = new BTUserConfig() ; 
			userConfig.setAgent_id(agentId) ; 
			userConfig.setDefaultCountry("") ; 
			userConfig.setLanguage("") ; 
			BipTagUserConfigurationManger.commitConfig(userConfig) ; 
		} 
		LayoutParams  layoutParams = (LayoutParams)separator.getLayoutParams() ; 
		   WindowManager wm = (WindowManager) getActivity().getSystemService(Context.WINDOW_SERVICE);
			Display display  =wm.getDefaultDisplay() ;
			Point point  = new Point() ; 
			display.getSize(point) ; 
			int screenWidth = point.x ; 
			layoutParams.setMargins(screenWidth/40,0, screenWidth/40, 0) ; 
			separator.setLayoutParams(layoutParams)  ; 
			
			TextView title = (TextView) fragmentView.findViewById(R.id.general_change_title ) ; 
			//title.setTextSize(screenWidth/20) ; 
			title.setTextSize(NavigationActivity.getContext().getResources().getDimension(R.dimen.acc_info_size));
			LayoutParams params = (LayoutParams) title.getLayoutParams() ; 
			params.setMargins(screenWidth/40,0, screenWidth/40, 0) ;
			title.setLayoutParams(params) ; 
			int checkedIndex  = -1  ; 
			if(this.changeLanguageMode){
				Button actionBarButton = (Button)	getActivity().getActionBar().getCustomView().findViewById(R.id.formste1_btn_container) ;
				actionBarButton.setBackground(NavigationActivity.getContext().getResources().getDrawable(R.drawable.go_back));

			//	actionBarButton.setText(NavigationActivity.getContext().getResources().getString(R.string.return_image_list));

				actionBarButton.setVisibility(View.VISIBLE) ;
				actionBarButton.setOnClickListener(new View.OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						BiptagNavigationManager.pushFragments(SettingChangeGeneral.this, false, this.getClass().getName(), getActivity(), null, 3);

					}
				}) ;

				title.setText(NavigationActivity.getContext().getResources().getString(R.string.change_lgge)) ; 
				values = new String[]{
						"Français" , "English"
				} ; 
				if(userConfig!= null){
					if(userConfig.getLanguage().equals("Français")){
						checkedIndex = 0  ; 
					}
					if(userConfig.getLanguage().equals("English")){
						checkedIndex = 1  ; 
					}
				}
			}else{
				Button actionBarButton = (Button)	getActivity().getActionBar().getCustomView().findViewById(R.id.formste1_btn_container) ;
				actionBarButton.setBackground(NavigationActivity.getContext().getResources().getDrawable(R.drawable.go_back));

			//	actionBarButton.setText(NavigationActivity.getContext().getResources().getString(R.string.return_image_list));

				actionBarButton.setVisibility(View.VISIBLE) ;
				actionBarButton.setOnClickListener(new View.OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						BiptagNavigationManager.pushFragments(SettingChangeGeneral.this, false, this.getClass().getName(), getActivity(), null, 3);

					}
				}) ;

				title.setText(NavigationActivity.getContext().getResources().getString(R.string.default_country_setting_title)) ; 
				String coutriesString = BipTagFormsManager.loadJSONFromAsset(getActivity(), "pays.json") ; 
				String key = "nom_fr_fr" ;
				if(userConfig!= null){
					//Log.d("lang default", "lang defaut "+userConfig.getLanguage()) ;
					if(userConfig.getLanguage().equals("English") ){
						key = "nom_en_gb" ;
					}
				}
				try {
					JSONArray countries = new JSONArray(coutriesString) ; 
					values = new String[countries.length()] ; 
					for (int j = 0; j < countries.length(); j++) {
						if(userConfig != null){
							if(userConfig.getDefaultCountry().equals(countries.getJSONObject(j).getString(key) )){
								checkedIndex = j ; 
							}
						}
						values[j] = countries.getJSONObject(j).getString(key) ; 
					
					
					}
				}catch(JSONException e){
					
				}
				
			}
			
			adapter = new SettingChangeGeneralAdapter(getActivity(), android.R.layout.simple_list_item_1, values , checkedIndex) ; 
			listView.setAdapter(adapter) ;
			
			listView.setOnItemClickListener(new OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> parent, View view,
										int position, long id) {
					// TODO Auto-generated method stub
					for (int i = 0; i < values.length; i++) {
						View v = listView.getChildAt(i);
						if (v != null) {
							ImageView imageView = (ImageView) v.findViewById(R.id.general_change_icon);

							imageView.setVisibility(View.INVISIBLE);

						}
					}


					ImageView img = (ImageView) view.findViewById(R.id.general_change_icon);
					if (img != null)
						img.setVisibility(View.VISIBLE);
					TextView txt = (TextView) view.findViewById(R.id.general_change_item);
					if (changeLanguageMode) {
						userConfig.setLanguage(txt.getText().toString());
						String language_code = "fr";
						if (txt.getText().toString().equals("Français")) {
							language_code = "fr";
						} else {
							language_code = "en";
						}


						Resources res = getActivity().getResources();
						// Change locale settings in the app.
						DisplayMetrics dm = res.getDisplayMetrics();
						android.content.res.Configuration conf = res.getConfiguration();

						conf.locale = new Locale(language_code.toLowerCase());
						res.updateConfiguration(conf, dm);
						((NavigationActivity) getActivity()).changeTabulationLanguage();
						ContactFragement.fragmentView = null;

						FillingFormStep1.updateLabelsText();
						EditFormStep1.updateLabelsText();
						FillingFormStep2.updateLabelsText();
						EditFormStep2.updateLabelsText();

					} else {
						userConfig.setDefaultCountry(txt.getText().toString());
					}

					BipTagUserConfigurationManger.commitConfig(userConfig);
					BiptagNavigationManager.pushFragments(SettingChangeGeneral.this, false, this.getClass().getName(), getActivity(), null, 3);
				}
			});




		return fragmentView;
	
	
	}

	@Override
	public void onResume() {
		super.onResume();
		Button actionBarButton = (Button)	getActivity().getActionBar().getCustomView().findViewById(R.id.formste1_btn_container) ;
		actionBarButton.setBackground(NavigationActivity.getContext().getResources().getDrawable(R.drawable.go_back));

	//	actionBarButton.setText(NavigationActivity.getContext().getResources().getString(R.string.return_image_list));

		actionBarButton.setVisibility(View.VISIBLE) ;

	}

	@Override
	public void onStop() {
		super.onStop();
		Button actionBarButton = (Button)	getActivity().getActionBar().getCustomView().findViewById(R.id.formste1_btn_container) ;
		actionBarButton.setBackground(NavigationActivity.getContext().getResources().getDrawable(R.drawable.go_back));

		//actionBarButton.setText(NavigationActivity.getContext().getResources().getString(R.string.return_image_list));

		actionBarButton.setVisibility(View.INVISIBLE) ;

	}
}
