package com.biptag.biptag.fragements.settings;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.InputStreamEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.graphics.Point;
import android.graphics.drawable.GradientDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;

import com.biptag.biptag.Constant;
import com.biptag.biptag.ContactAdapter;
import com.biptag.biptag.JSONParser;
import com.biptag.biptag.NavigationActivity;
import com.biptag.biptag.R;
import com.biptag.biptag.dbmanager.datasource.BTAvailableCountryDataSource;
import com.biptag.biptag.dbmanager.datasource.BTContactDataSource;
import com.biptag.biptag.dbmanager.datasource.BTImageDataSource;
import com.biptag.biptag.dbmanager.datasource.BTLogDataSource;
import com.biptag.biptag.dbmanager.objects.BTAvailableCountry;
import com.biptag.biptag.dbmanager.objects.BTContact;
import com.biptag.biptag.dbmanager.objects.BTImage;
import com.biptag.biptag.fragements.ConnectedhomeFragment;
import com.biptag.biptag.fragements.ContactFragement;
import com.biptag.biptag.interfaces.AsyncResponse;
import com.biptag.biptag.managers.BiptagNavigationManager;
import com.biptag.biptag.managers.BiptagUserManger;
import com.biptag.biptag.webservices.DownloadContactWithHashCodes;
import com.biptag.biptag.webservices.DownloadContactsWithIds;
import com.biptag.biptag.webservices.DownloadFiles;
import com.biptag.biptag.webservices.DownloadForms;
import com.biptag.biptag.webservices.HttpFileUpload;
import com.biptag.biptag.webservices.UploadContacts;
import com.biptag.biptag.webservices.logs.DownloadLogs;
import com.biptag.biptag.webservices.templates.DownloadFollowupHashcodes;
import com.biptag.biptag.webservices.templates.TemplateUploader;

public class SettingSynchronisationFragment extends Fragment implements AsyncResponse {
	
	View fragmentView ; 
	TextView imagedSyncedTxt  ; 
	Button imageSynchronisationButton , countrySynchronisationButton , allImageButton  ; 
	ProgressDialog synchronisationDialog ; 
	List<BTImage> nonSentImages  ; 
	int nbSuccessSending= 0 ; 
	int nbFailureSending= 0 ;
	int agent_id ; 
	int ba_id ; 
	
		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			// TODO Auto-generated method stub
			System.gc();

			fragmentView = inflater.inflate(R.layout.synchronisation_layout,null);
			
			allImageButton = (Button) fragmentView.findViewById(R.id.sync_all_image_button) ; 
			imageSynchronisationButton =(Button) fragmentView.findViewById(R.id.sync_image_button); 
			countrySynchronisationButton = (Button) fragmentView.findViewById(R.id.sync_country_button);
			LinearLayout imageContainerLayout = (LinearLayout) fragmentView.findViewById(R.id.sync_image_container) ; 
			LinearLayout countryContainerLayout = (LinearLayout) fragmentView.findViewById(R.id.sync_coutries_container) ; 
			 
			 WindowManager wm = (WindowManager) getActivity().getSystemService(Context.WINDOW_SERVICE);
				Display display  =wm.getDefaultDisplay() ;
				Point point  = new Point() ; 
				display.getSize(point) ; 
				int screenWidth = point.x ; 
				int screenHeight = point.y ;
			GradientDrawable border = new GradientDrawable();
			    border.setColor(0xeeeeee); //white background
			    border.setStroke(3, 0xFFE6E6EA); //black border with full opacity
			    if(Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
			    	//imageContainerLayout.setBackgroundDrawable(border);
			    } else { 
			    	//imageContainerLayout.setBackground(border);
			    }
			    
			   
			    LinearLayout.LayoutParams params = (android.widget.LinearLayout.LayoutParams) imageContainerLayout.getLayoutParams() ; 
			    params.setMargins(screenWidth/60, screenHeight/60, screenWidth/60, screenHeight/60) ; 
			    imageContainerLayout.setLayoutParams(params) ; 
			    
			    
			    GradientDrawable border1 = new GradientDrawable();
			    border1.setColor(0xeeeeee); //white background
			    border1.setStroke(3, 0xFFE6E6EA); //black border with full opacity
			    if(Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
			    	//countryContainerLayout.setBackgroundDrawable(border1);
			    } else { 
			    	//countryContainerLayout.setBackground(border1);
			    }
			     
			   
			    LinearLayout.LayoutParams params1 = (android.widget.LinearLayout.LayoutParams) countryContainerLayout.getLayoutParams() ; 
			    params1.setMargins(screenWidth/60, screenHeight/60, screenWidth/60, screenHeight/60) ; 
			    countryContainerLayout.setLayoutParams(params1) ; 
			    
			    TextView separator1 = (TextView) fragmentView.findViewById(R.id.sync_separator_0) ; 
				LayoutParams  layoutParams1 = (LayoutParams)separator1.getLayoutParams() ; 
				
				layoutParams1.setMargins(screenWidth/40,0, screenWidth/40, 0) ; 
				separator1.setLayoutParams(layoutParams1)  ; 
				separator1.setHeight(screenHeight /480) ; 
				
				
				TextView separator = (TextView) fragmentView.findViewById(R.id.sync_seraprator_1) ; 
				LayoutParams  layoutParams = (LayoutParams)separator1.getLayoutParams() ; 
				
				layoutParams1.setMargins(screenWidth/40,0, screenWidth/40, 0) ; 
				separator.setLayoutParams(layoutParams)  ; 
				separator.setHeight(screenHeight /480) ; 
				
				TextView title1 = (TextView) fragmentView.findViewById(R.id.sync_title_1 ) ; 
			//	title1.setTextSize(screenWidth/30) ; 
				title1.setTextSize(NavigationActivity.getContext().getResources().getDimension(R.dimen.acc_info_size));
				LayoutParams params2 = (LayoutParams) title1.getLayoutParams() ; 
				params2.setMargins(screenWidth/40,0, screenWidth/40, 0) ;
				title1.setLayoutParams(params2) ; 
				
				TextView title0 = (TextView) fragmentView.findViewById(R.id.sync_title_0 ) ; 
				//title0.setTextSize(screenWidth/30) ; 
				title0.setTextSize(NavigationActivity.getContext().getResources().getDimension(R.dimen.acc_info_size));
				LayoutParams params3 = (LayoutParams) title0.getLayoutParams() ; 
				params3.setMargins(screenWidth/40,0, screenWidth/40, 0) ;
				title0.setLayoutParams(params3) ; 
				
				/*
				int btnWidth = imageSynchronisationButton.getWidth() ; 
				int btnHeight  = imageSynchronisationButton.getHeight() ; 
				LayoutParams syncImageButtonLinearLayoutParams = new LayoutParams(btnWidth / 2 , btnHeight/ 2) ; 
				syncImageButtonLinearLayoutParams.setMargins(btnWidth/4, btnHeight/4 , 0,0);
				imageSynchronisationButton.setLayoutParams(syncImageButtonLinearLayoutParams) ; 
				
				
				btnWidth = allImageButton.getWidth() ; 
				btnHeight  = allImageButton.getHeight() ; 
				LayoutParams AllImageButtonLinearLayoutParams = new LayoutParams(btnWidth / 2 , btnHeight/ 2) ; 
				syncImageButtonLinearLayoutParams.setMargins(btnWidth/4, btnHeight/4 , 0,0);
				allImageButton.setLayoutParams(AllImageButtonLinearLayoutParams) ;
				
				

				btnWidth = countrySynchronisationButton.getWidth() ; 
				btnHeight  = countrySynchronisationButton.getHeight() ; 
				LayoutParams countryButtonLinearLayoutParams = new LayoutParams(btnWidth / 2 , btnHeight/ 2) ; 
				syncImageButtonLinearLayoutParams.setMargins(btnWidth/4, btnHeight/4 , 0,0);
				countrySynchronisationButton.setLayoutParams(countryButtonLinearLayoutParams) ;
				*/
				String agent_id_s= String.valueOf(BiptagUserManger.getUserValue("AGENT_ID", "int", getActivity().getApplicationContext())) ; 
				agent_id = Integer.parseInt(agent_id_s) ; 

				String ba_id_s= String.valueOf(BiptagUserManger.getUserValue("BA_ID", "int", getActivity().getApplicationContext())) ; 
				ba_id = Integer.parseInt(ba_id_s) ; 
				
				BTImageDataSource dataSource = new BTImageDataSource(Constant.context) ; 
				List<BTImage> list  = null;
			list= dataSource.selectAllImages(ba_id) ;
			int numberOfImages=0 ;
			for (int i=0; i<list.size();i++){
				if (list.get(i).getIsSent()==1) {
					numberOfImages++;
				}
			}
			Log.d(" image "  , "liste image " + list.size() + " number of images " + numberOfImages);
				 nonSentImages= new ArrayList<BTImage>() ; 
				 
				for (int i = 0; i < list.size(); i++) {
					if(list.get(i).getIsSent() == 0){
						nonSentImages.add(list.get(i)) ; 
					}
				}
				
				if(nonSentImages.size() == 0){
					imageSynchronisationButton.setEnabled(false) ; 
				}
				
				
				
				imagedSyncedTxt = (TextView) fragmentView.findViewById(R.id.sync_number_images_txt) ; 
				imagedSyncedTxt.setText(numberOfImages+" "+NavigationActivity.getContext().getResources().getString(R.string.setting_nb_photos) +"\n" + nonSentImages.size() + " "+NavigationActivity.getContext().getResources().getString(R.string.setting_nb_photos_not_sync) ) ;
				
				
				imageSynchronisationButton.setOnClickListener(new View.OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub

						if (BiptagUserManger.isOnline(getActivity())) {


							Log.d(" synchro ", " synchro button setoncklick");
							nbSuccessSending = 0;
							nbFailureSending = 0;

							//Log.d("sending image ", "sending image before uploading image  ") ;
							final int ba_id = Integer.parseInt(String.valueOf(BiptagUserManger.getUserValue("BA_ID", "int", getActivity())));


							BiptagUserManger.saveUserData("IS_SYNC", "1", getActivity());

							BTContactDataSource btContactDataSource = new BTContactDataSource(getActivity());

							List<BTContact> list = btContactDataSource.getAllContacts(agent_id, 0);


							BTImageDataSource source = new BTImageDataSource(getActivity());
							List<BTImage> images = source.selectAllImages(ba_id);

							if (list.size() != 0) {
								Log.d(" synchro ", " synchro button if list non vide " + list.size());


								BiptagUserManger.saveUserData("IS_SYNC", "1", NavigationActivity.getContext());


								synchronisationDialog = new ProgressDialog(NavigationActivity.getContext());
								synchronisationDialog.setTitle(NavigationActivity.getContext().getResources().getString(R.string.sync_title));
								synchronisationDialog.setMessage(NavigationActivity.getContext().getResources().getString(R.string.sync_text));
								synchronisationDialog.setProgress(0);
								synchronisationDialog.setCancelable(false);
								synchronisationDialog.setCanceledOnTouchOutside(false);

								BTContactDataSource btContactDataSource2 = new BTContactDataSource(NavigationActivity.getContext());
								String agent_id = String.valueOf(BiptagUserManger.getUserValue("AGENT_ID", "int", NavigationActivity.getContext()));

								List<BTContact> list2 = btContactDataSource2.getAllContacts(Integer.parseInt(agent_id), 0);

								synchronisationDialog.setMax(3 + list2.size() - 1);
								synchronisationDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
								synchronisationDialog.show();
								new Thread(new Runnable() {

									@Override
									public void run() {
										// TODO Auto-generated method stub

										DownloadForms downloadForms = new DownloadForms(NavigationActivity.getContext().getApplicationContext());
										downloadForms.response = SettingSynchronisationFragment.this;
										downloadForms.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, null);
										TemplateUploader uploader = new TemplateUploader(NavigationActivity.getContext().getApplicationContext(), Integer.parseInt(String.valueOf(ba_id)));
										uploader.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, null);
										DownloadFollowupHashcodes downloadFollowupHashcodes = new DownloadFollowupHashcodes(getActivity());
										downloadFollowupHashcodes.execute();


									}
								}).start();

								ContactFragement.adapterSync = new ContactAdapter(NavigationActivity.getContext(), R.layout.contact_row, ContactFragement.data_sync, ContactFragement.logs);
								ContactFragement.adapterSync.notifyDataSetChanged();
								Log.d(" contact ", " syncronising finish" + ContactFragement.adapterSync.getCount());
								if (ContactFragement.adapterSync.getCount()>0) {
								ContactFragement.contactlistView.setAdapter(ContactFragement.adapterSync);
								ContactFragement.setListViewHeightBasedOnChildren(ContactFragement.contactlistView);
								Log.d(" contact ", " syncronising " + ContactFragement.adapterSync.getCount());
								}
								ContactAdapter adapterUnSync = new ContactAdapter(NavigationActivity.getContext(), R.layout.contact_row, ContactFragement.data_unsync, ContactFragement.logs);
								if (ContactFragement.adapterUnSync.getCount()>0) {
								ContactFragement.contactListViewUnSync.setAdapter(adapterUnSync);
								ContactFragement.setListViewHeightBasedOnChildren(ContactFragement.contactListViewUnSync);
								Log.d(" contact ", " syncronising Unsyn " + ContactFragement.adapterUnSync.getCount());
								ContactFragement.adapterUnSync.notifyDataSetChanged();
								Log.d(" contact ", " syncronising Unsyn finish" + ContactFragement.adapterUnSync.getCount());
									}

								if (ContactFragement.adapterSync != null) {
									ContactFragement.adapterSync.notifyDataSetChanged();
								}
								if (ContactFragement.adapterUnSync != null) {
									ContactFragement.adapterUnSync.notifyDataSetChanged();
								}
/*								ContactFragement.contactListViewUnSync.setAdapter(ContactFragement.adapterUnSync);
								ContactFragement.setListViewHeightBasedOnChildren(ContactFragement.contactListViewUnSync) ;
								ContactFragement.contactlistView.setAdapter(ContactFragement.adapterSync);
								ContactFragement.setListViewHeightBasedOnChildren(ContactFragement.contactlistView) ;
								Log.d(" contact ", " syncronising Unsyn " + ContactFragement.adapterUnSync.getCount());
									Log.d(" contact ", " syncronising syn finish" + ContactFragement.adapterSync.getCount());
*/
								int imgCount = 0;
								for (int i = 0; i < images.size(); i++) {

									if (images.get(i).getIsSent() == 0) {
										imgCount++;
									}
								}
								if (imgCount != 0) {

								/*	synchronisationDialog = new ProgressDialog(getActivity());
									synchronisationDialog.setTitle(getResources().getString(R.string.sendin_title));
									synchronisationDialog.setMessage(getResources().getString(R.string.sendin_image));
									synchronisationDialog.setCancelable(false);
									synchronisationDialog.setCanceledOnTouchOutside(false);
									synchronisationDialog.setMax(imgCount);
									synchronisationDialog.setProgressStyle(synchronisationDialog.STYLE_HORIZONTAL);
									synchronisationDialog.show();

								for (int i = 0; i < images.size(); i++) {
									//Log.d("sending image ", "sending image image  status "+images.get(i).getIsSent()) ;
									Log.d(" synchro ", "sending image size " + images.size());
									if (images.get(i).getIsSent() == 0) {
										Log.d(" synchro ", "sending image uploading image  ");
										uploadFile(images.get(i));

									}
								}



								BiptagUserManger.saveUserData("IS_SYNC", "", getActivity());
							}

							imageSynchronisationButton.setClickable(false);
							imageSynchronisationButton.setEnabled(false);


	//	imageSynchronisationButton.set
*/
								}
							} else {

								source = new BTImageDataSource(getActivity());
								images = source.selectAllImages(ba_id);

								int imgCount = 0;
								for (int i = 0; i < images.size(); i++) {

									if (images.get(i).getIsSent() == 0) {
										imgCount++;
									}
								}
								if (imgCount != 0) {

									synchronisationDialog = new ProgressDialog(getActivity());
									synchronisationDialog.setTitle(getResources().getString(R.string.sendin_title));
									synchronisationDialog.setMessage(getResources().getString(R.string.sendin_image));
									synchronisationDialog.setCancelable(false);
									synchronisationDialog.setCanceledOnTouchOutside(false);
									synchronisationDialog.setMax(imgCount);
									synchronisationDialog.setProgressStyle(synchronisationDialog.STYLE_HORIZONTAL);
									synchronisationDialog.show();

									for (int i = 0; i < images.size(); i++) {
										//Log.d("sending image ", "sending image image  status "+images.get(i).getIsSent()) ;
										Log.d(" synchro ", "sending image size " + images.size());
										if (images.get(i).getIsSent() == 0) {
											Log.d(" synchro ", "sending image uploading image  ");
											uploadFile(images.get(i));

										}
									}


									BiptagUserManger.saveUserData("IS_SYNC", "", getActivity());
								}


							}


						} else {

							AlertDialog.Builder builder = new AlertDialog.Builder(NavigationActivity.getContext());
							builder.setTitle(NavigationActivity.getContext().getResources().getString(R.string.error_synch_title));
							builder.setMessage(NavigationActivity.getContext().getResources().getString(R.string.error_synch_msg));
							builder.setPositiveButton("Ok", new OnClickListener() {

								@Override
								public void onClick(DialogInterface dialog, int which) {
									// TODO Auto-generated method stub
									dialog.dismiss();
								}
							});

							builder.show();


						}

					}
				}) ;
				
				countrySynchronisationButton.setOnClickListener(new View.OnClickListener() {
					
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub

						if(BiptagUserManger.isOnline(NavigationActivity.getContext())){
							new GetAvailableCountries().execute() ;

						}else{
							AlertDialog.Builder builder = new AlertDialog.Builder(NavigationActivity.getContext()) ;
							builder.setTitle(getResources().getString(R.string.cnx_error_title)) ;
							builder.setMessage(getResources().getString(R.string.cnx_error_desc)) ;
							builder.setPositiveButton("OK", new OnClickListener() {

								@Override
								public void onClick(DialogInterface dialog, int which) {
									// TODO Auto-generated method stub
									dialog.dismiss() ;
								}
							}) ;
							builder.show() ;



						}

					}
				}) ;
				
				TextView countryDownloaded = (TextView) fragmentView.findViewById(R.id.sync_country_downloaded) ; 
				BTAvailableCountryDataSource source = new BTAvailableCountryDataSource(getActivity()) ; 
				List<BTAvailableCountry >countries = source.selectAllAvailableCountry() ; 
				String txt = "" ;
				for (int i = 0; i < countries.size(); i++) {
					txt+=countries.get(i).getName()+" " ; 
					if(i != countries.size()- 1){
						txt+= ", " ; 
					}
				}
				
				countryDownloaded.setText(txt) ; 
				
				
				allImageButton.setOnClickListener(new View.OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub


						SettingAllImagesFragement fragement = new SettingAllImagesFragement();
						BiptagNavigationManager.pushFragments(fragement, true, "all_image_fragment", getActivity(), null, 3);

					}
				}) ;

			Button actionBarButton = (Button)	getActivity().getActionBar().getCustomView().findViewById(R.id.formste1_btn_container) ;
			actionBarButton.setBackground(NavigationActivity.getContext().getResources().getDrawable(R.drawable.go_back));

		//	actionBarButton.setText(NavigationActivity.getContext().getResources().getString(R.string.return_image_list));

			actionBarButton.setVisibility(View.VISIBLE) ;
			actionBarButton.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					SettingMenuFragment settingMenuFragment = new SettingMenuFragment() ;
					BiptagNavigationManager.pushFragments(settingMenuFragment, true, this.getClass().getName(), getActivity(), null, 3) ;

				}
			}) ;



			return fragmentView ;
		}
		
		 @Override
		public void onResume() {
			// TODO Auto-generated method stub
			super.onResume();
		System.gc();
			
			BTImageDataSource dataSource = new BTImageDataSource(Constant.context) ; 
			List<BTImage> list = dataSource.selectAllImages(ba_id) ; 
			 nonSentImages= new ArrayList<BTImage>() ; 
			 
			for (int i = 0; i < list.size(); i++) {
				if(list.get(i).getIsSent() == 0){
					nonSentImages.add(list.get(i)) ; 
				}
			}
			
			if(nonSentImages.size() == 0){
				imageSynchronisationButton.setEnabled(false) ; 
			}else{
				imageSynchronisationButton.setEnabled(true)  ; 
			}
			 Button actionBarButton = (Button)	getActivity().getActionBar().getCustomView().findViewById(R.id.formste1_btn_container) ;
			 actionBarButton.setBackground(NavigationActivity.getContext().getResources().getDrawable(R.drawable.go_back));

			// actionBarButton.setText(NavigationActivity.getContext().getResources().getString(R.string.return_image_list));

			 actionBarButton.setVisibility(View.VISIBLE) ;

		 }
		
		
		public void uploadImages(){
			
			
			if(synchronisationDialog !=  null){
				synchronisationDialog.dismiss() ; 
			}
			synchronisationDialog = new ProgressDialog(getActivity()) ; 
			synchronisationDialog.setTitle(getResources().getString(R.string.sendin_title)) ; 
			synchronisationDialog.setMessage(getResources().getString(R.string.sendin_image)) ; 
			synchronisationDialog.setProgress(0) ; 
			synchronisationDialog.setCancelable(false) ; 
			synchronisationDialog.setCanceledOnTouchOutside(false) ; 
			synchronisationDialog.setMax( nonSentImages.size()  ) ; 
			synchronisationDialog.setProgressStyle(synchronisationDialog.STYLE_HORIZONTAL);
			synchronisationDialog.show() ; 
		}
		
		
		public void uploadFile(BTImage image){ 
			
			
		String url  = Constant.base_url +Constant.accept_images  ;
		try {
		    // Set your file path here
			
			String uri =getActivity().getCacheDir().getAbsolutePath()+"/"+image.getImageName()+".jpg"  ; 
			//Log.d("sendin image", "sendin image path "+uri) ; 
			
			
		    FileInputStream fstrm = new FileInputStream(uri);
		    // Set your server page url (and the file title/description)
		    HttpFileUpload hfu = new HttpFileUpload(url, image.getImageName(), uri , image.getImageId());
		    hfu.response = this ; 
		    hfu.fStream = fstrm ;  
		    hfu.execute() ; 
		     
		  } catch (FileNotFoundException e) {
		    // Error: File not found
		  }
		
		}



		@Override
		public void processFinish(String result) {
			// TODO Auto-generated method stub
			
				
				try {
					JSONObject sending = new JSONObject(result) ; 
					Log.d(" sent image ", sending.toString());
					if(sending.getInt("sent")== 1){
						nbSuccessSending ++ ; 
						
						BTImageDataSource dataSource = new BTImageDataSource(getActivity()) ; 
						BTImage  image = dataSource.selectImagesWithId(sending.getInt("img_id"), ba_id) ; 
						if(image!= null){
							
							image.setIsSent(1) ; 
							dataSource.updateImage(image) ; 
							//Log.d("sendin image update", "sendin image update image ok") ; 
								
						}else{
							//Log.d("sendin image update", "sendin image update image null") ; 
						}
						
						
						
					}else{
						nbFailureSending++  ; 
					}
					synchronisationDialog.setProgress(synchronisationDialog.getProgress()+1) ;
					if (synchronisationDialog.getProgress() == synchronisationDialog.getMax())  {
						synchronisationDialog.dismiss() ; 
						
						AlertDialog.Builder builder = new AlertDialog.Builder(getActivity()) ; 
						builder.setTitle("Resultat d'envoi") ; 
						String message = " ";
						if(nbSuccessSending == 1){
							message = nbSuccessSending + " " +NavigationActivity.getContext().getResources().getString(R.string.synchro_image) ;
						}else if(nbSuccessSending > 1){
						message = nbSuccessSending+" " +NavigationActivity.getContext().getResources().getString(R.string.synchro_images) ; ;
						}
						if(nbFailureSending!=0){
							if (nbFailureSending>1) {
								if (nbSuccessSending>0) {
									message += "\n" + nbFailureSending + NavigationActivity.getContext().getResources().getString(R.string.non_synchro_images);
								}else {
									message += nbFailureSending + NavigationActivity.getContext().getResources().getString(R.string.non_synchro_images);
								}
							}else if(nbFailureSending==1){
								if (nbSuccessSending>0) {
									message += "\n" + nbFailureSending + NavigationActivity.getContext().getResources().getString(R.string.non_synchro_image);
								}else {
									message +=  nbFailureSending + NavigationActivity.getContext().getResources().getString(R.string.non_synchro_image);

								}
							}
						}
						builder.setMessage(message) ; 
						builder.setPositiveButton("Ok", new OnClickListener() {
							
							@Override
							public void onClick(DialogInterface dialog, int which) {
								// TODO Auto-generated method stub
								dialog.dismiss() ; 
							}
						}) ; 
						
						
						builder.show() ; 
						
						BTImageDataSource dataSource = new BTImageDataSource(Constant.context) ; 
						List<BTImage> list = dataSource.selectAllImages(ba_id) ; 
						 nonSentImages= new ArrayList<BTImage>() ; 
						 
						for (int i = 0; i < list.size(); i++) {
							if(list.get(i).getIsSent() == 0){
								nonSentImages.add(list.get(i)) ; 
							}
						}
						
						if(nonSentImages.size() == 0){
							imageSynchronisationButton.setEnabled(false) ; 
						}else{
							imageSynchronisationButton.setEnabled(true)  ; 
						}
						int numberOfImages=0 ;
						for (int i=0; i<list.size();i++){
							if (list.get(i).getIsSent()==1) {
								numberOfImages++;
							}
						}
						
						Log.d("image " , " liste des images " + list.size() + " nonsynchro " + nonSentImages.size());
						imagedSyncedTxt.setText(numberOfImages+" "+NavigationActivity.getContext().getResources().getString(R.string.setting_nb_photos)) ;



					}
					
				} catch (JSONException e) {
					// TODO: handle exception
				}
				
			
			
				if(result.equals("forms")){
					

					ConnectedhomeFragment.setIsFormAdded(false) ; 
					DownloadFiles downloadFiles = new DownloadFiles(NavigationActivity.getContext()) ; 
					downloadFiles.response =SettingSynchronisationFragment.this ; 
					if(BiptagUserManger.isOnline(NavigationActivity.getContext())){

						downloadFiles.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, null) ; ;
					}
					else
						processFailed() ; 
					
				}
				
				if(result.equals("docs")){ 
					// upload contact
					UploadContacts uploadContacts = new  UploadContacts(NavigationActivity.getContext()) ; 
					uploadContacts.response= SettingSynchronisationFragment.this  ; 
					if(BiptagUserManger.isOnline(NavigationActivity.getContext()))
					uploadContacts.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR	, null)  ; 
					else
						processFailed() ; 
				}
				
				if(result.equals("uploaded")){
					//download contacts 
					
					
					
					//new prepareAdapters(true).execute() ; 
					
					DownloadContactWithHashCodes contactWithHashCodes = new DownloadContactWithHashCodes(NavigationActivity.getContext()) ; 
					contactWithHashCodes.response = SettingSynchronisationFragment.this ; 
					if(BiptagUserManger.isOnline(NavigationActivity.getContext()) && !DownloadContactWithHashCodes.isExecuting )
					contactWithHashCodes.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR	, null) ;
					else
						processFailed() ; 
				}
				
				if(result.equals("ids_ready")){
					// list of ids ready to download  
					
					
					
					String ids_to_download  =	(String) BiptagUserManger.getUserValue("ids_to_download", "string", NavigationActivity.getContext()) ; 
					try {
						if(ids_to_download!= null){
							JSONArray ids = new JSONArray(ids_to_download) ; 
							if(synchronisationDialog!= null)
							synchronisationDialog.setMax(synchronisationDialog.getMax() + ids.length()) ; 
							DownloadContactsWithIds downloadContactsWithIds = new DownloadContactsWithIds(NavigationActivity.getContext()) ; 
							downloadContactsWithIds.response= SettingSynchronisationFragment.this ; 
							if(BiptagUserManger.isOnline(NavigationActivity.getContext()))
							downloadContactsWithIds.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR	, null) ;
							else
								processFailed() ; 
								
						}
					} catch (JSONException e) {
						// TODO: handle exception
					}
					
					
					
					
				} 
				
				if(result.equals("contacts_done")){
					DownloadLogs downloadLogs = new DownloadLogs(NavigationActivity.getContext(), SettingSynchronisationFragment.this) ; 
					if(BiptagUserManger.isOnline(NavigationActivity.getContext()))
					downloadLogs.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR	, null) ; 
					else
						processFailed() ; 

					
					
					
					if(synchronisationDialog != null)
						synchronisationDialog.dismiss() ; 
						BiptagUserManger.saveUserData("IS_SYNC", "", NavigationActivity.getContext()) ; 
				 
				}
				
				 
				if(result.equals("finish")){
					
					
					BiptagUserManger.saveUserData("IS_SYNC", "", NavigationActivity.getContext()) ; 
					
					DownloadFollowupHashcodes downloadFollowupHashcodes = new DownloadFollowupHashcodes(NavigationActivity.getContext()) ; 
					if(BiptagUserManger.isOnline(NavigationActivity.getContext()))
					downloadFollowupHashcodes.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR	, null) ; 
					else
						processFailed() ; 
				

					BTImageDataSource source = new BTImageDataSource(NavigationActivity.getContext()) ;
					List<BTImage> images = source.selectAllImages(ba_id) ; 
					
					//Log.d("sending image ", "sending image image  number "+images.size()) ; 
					
					int imgCount  = 0 ; 
					for (int i = 0; i < images.size(); i++) {
						
						if(images.get(i).getIsSent() == 0){
						imgCount++ ; 	
						}
					}
					if(imgCount != 0){
					synchronisationDialog = new ProgressDialog(getActivity()) ; 
					synchronisationDialog.setTitle(getResources().getString(R.string.sendin_title)) ; 
					synchronisationDialog.setMessage(getResources().getString(R.string.sendin_image)) ; 
					synchronisationDialog.setCancelable(false) ; 
					synchronisationDialog.setCanceledOnTouchOutside(false) ; 
					synchronisationDialog.setMax(imgCount ) ; 
					synchronisationDialog.setProgressStyle(synchronisationDialog.STYLE_HORIZONTAL);
					synchronisationDialog.show() ; 
					
					for (int i = 0; i < images.size(); i++) {
						//Log.d("sending image ", "sending image image  status "+images.get(i).getIsSent()) ; 
						
						if(images.get(i).getIsSent() == 0){
							//Log.d("sending image ", "sending image uploading image  ") ; 
							uploadFile(images.get(i)) ; 
							
						}
					}	
					
				}
				 
				
				
				BiptagUserManger.saveUserData("IS_SYNC", "", getActivity()) ;
					
				
				}
			
			
		} 

		@Override
		public void processFailed() {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void synchronisationStepUp() {
			// TODO Auto-generated method stub
			
		}
		
		@Override
		public void onStop() {
			// TODO Auto-generated method stub
			super.onStop();
			System.gc();

			nonSentImages= null  ;

			Button actionBarButton = (Button)	getActivity().getActionBar().getCustomView().findViewById(R.id.formste1_btn_container) ;
			actionBarButton.setBackground(NavigationActivity.getContext().getResources().getDrawable(R.drawable.go_back));

			//actionBarButton.setText(NavigationActivity.getContext().getResources().getString(R.string.return_image_list));

			actionBarButton.setVisibility(View.INVISIBLE) ;


		}
		
		class GetAvailableCountries extends AsyncTask<String, String, String>{
			
			ProgressDialog dialog  ; 
			JSONObject object ; 
			@Override
			protected void onPreExecute() {
				// TODO Auto-generated method stub
				super.onPreExecute();
				
			dialog= new ProgressDialog(getActivity()) ; 
			dialog.setTitle(NavigationActivity.getContext().getResources().getString(R.string.setting_sync_countries_popup_title)) ; 
			dialog.setMessage(NavigationActivity.getContext().getResources().getString(R.string.setting_sync_countries_popup_msg)) ; 
			dialog.setCancelable(true) ; 
			dialog.setIndeterminate(false) ; 
			dialog.show() ; 
			
			}

			@Override
			protected String doInBackground(String... params) {
				// TODO Auto-generated method stub
			
				
				String url = Constant.base_url+Constant.get_available_countries ;
				JSONParser parser= new JSONParser() ; 
				object=  parser.makeHttpRequest(url, "GET", new ArrayList<NameValuePair>()) ; 				
				Log.d(" countries ", object.toString());
				return null;
			}
			
			@Override
			protected void onPostExecute(String result) {
				// TODO Auto-generated method stub
				super.onPostExecute(result);
			
				dialog.dismiss() ; 
				if(object !=  null){

					SettingCountrySynchronisaiton countrySynchronisaiton = new  SettingCountrySynchronisaiton(object) ; 
					BiptagNavigationManager.pushFragments(countrySynchronisaiton, true, "country_sync", getActivity(), null, 3) ;
					Log.d("countries ", "countries available " + countrySynchronisaiton.countres) ;
				}else{
					AlertDialog.Builder builder = new AlertDialog.Builder(NavigationActivity.getContext()) ; 
					builder.setTitle(NavigationActivity.getContext().getResources().getString(R.string.cnx_error_title)) ; 
					builder.setMessage(NavigationActivity.getContext().getResources().getString(R.string.cnx_error_desc)) ; 
					builder.setPositiveButton("OK", new OnClickListener() {
						
						@Override
						public void onClick(DialogInterface dialog, int which) {
							// TODO Auto-generated method stub
							dialog.dismiss() ; 
						}
					}) ; 
					builder.show() ;
				}
			
			}
			
		}
}
