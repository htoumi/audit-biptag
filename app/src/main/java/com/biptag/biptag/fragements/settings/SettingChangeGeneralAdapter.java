package com.biptag.biptag.fragements.settings;

import com.biptag.biptag.R;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class SettingChangeGeneralAdapter extends ArrayAdapter<String> {

	private String[] values ; 
	private int checkedIndex ; 
	public SettingChangeGeneralAdapter(Context context, int resource,
			String[] objects , int checkedIndex) {
		super(context, resource, objects);
		this.values = objects ;
		this.checkedIndex = checkedIndex ; 
	
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		if(convertView ==  null){
			 LayoutInflater inflater = (LayoutInflater) getContext()
	                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	            convertView = inflater.inflate(R.layout.general_change_list_item, null);
		}
		TextView txt = (TextView) convertView.findViewById(R.id.general_change_item) ; 
		txt.setText(values[position]) ; 
		
		
		
		ImageView img = (ImageView) convertView.findViewById(R.id.general_change_icon) ; 
		//Log.d("check idnex", "check idnex "+this.checkedIndex) ;
		if(position != this.checkedIndex){
			img.setVisibility(View.INVISIBLE) ; 
				
		}else{
			img.setVisibility(View.VISIBLE) ; 
		}
		
		return convertView;
	}

	 
	
}
