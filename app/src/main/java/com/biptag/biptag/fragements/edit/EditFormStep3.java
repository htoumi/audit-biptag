package com.biptag.biptag.fragements.edit;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.graphics.Point;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.GradientDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.ScrollView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.LinearLayout.LayoutParams;

import com.biptag.biptag.Constant;
import com.biptag.biptag.ContactAdapter;
import com.biptag.biptag.ContactItem;
import com.biptag.biptag.JSONParser;
import com.biptag.biptag.ListingFilesActivity;
import com.biptag.biptag.NavigationActivity;
import com.biptag.biptag.R;
import com.biptag.biptag.dbmanager.datasource.BTContactDataSource;
import com.biptag.biptag.dbmanager.datasource.BTFollowupTemplateDataSource;
import com.biptag.biptag.dbmanager.datasource.BTFormDataSource;
import com.biptag.biptag.dbmanager.datasource.BTImageDataSource;
import com.biptag.biptag.dbmanager.objects.BTContact;
import com.biptag.biptag.dbmanager.objects.BTFollowupTemplate;
import com.biptag.biptag.dbmanager.objects.BTFormulaire;
import com.biptag.biptag.dbmanager.objects.BTImage;
import com.biptag.biptag.dbmanager.objects.BTLog;
import com.biptag.biptag.fragements.ContactFragement;
import com.biptag.biptag.fragements.EditFollowUpFragement;
import com.biptag.biptag.managers.BiptagNavigationManager;
import com.biptag.biptag.managers.BiptagUserManger;
import com.biptag.biptag.ocr.CaptureActivity;

public class EditFormStep3 extends Fragment {
	View fragmentView ;  
	long id;
	Button addfilesButton ; 
	public static Button finish  ; 
	Button backButton ; 
	Button openFollowUpActivity ; 
	JSONObject jsonObject ; 
	 
	JSONObject contactResponseJson ; 
	LinearLayout filecontainer ; 
	int RESULT_CODE_SELECT_FILES= 200 ;
	String files = new JSONArray().toString() ; 
	String links = new JSONArray().toString() ; 
	String vcards = new JSONArray().toString(); 
	int screenWidth ; 
	int screenHeight ;
 
	ListingFilesActivity filesActivity ; 
	PopupWindow deletePopUp ; 
	private static String  add_files_result_edit = "add_files_result_edit" ; 
	public final static String editImageArrayUserPreferenceName = "images_for_user_edit" ; 

	public void previousAction(){
		BiptagNavigationManager.pushFragments(EditFormStep3.this, false, "edit_form_3", NavigationActivity.getContext(), null, 2) ; 

	}
	
	 public void  initFilDarianeNavigation() {
			Button btn1 = (Button) fragmentView.findViewById(R.id.fildaraiane_step3_button1) ; 
			Button btn2 = (Button) fragmentView.findViewById(R.id.fildaraiane_step3_button2) ; 
			Button btn3 = (Button) fragmentView.findViewById(R.id.fildaraiane_step3_button3) ; 
			 btn1.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					previousAction() ;  
				}
			}) ; 
			 btn2.setOnClickListener(new View.OnClickListener() {

				 @Override
				 public void onClick(View v) {
					 // TODO Auto-generated method stub
					 previousAction();
				 }
			 }) ;
			 
			 btn3.setOnClickListener(new View.OnClickListener() {

				 @Override
				 public void onClick(View v) {
					 // TODO Auto-generated method stub
				 }
			 }) ;
			
			
		}

	public void fixLayout(LinearLayout followup_widget_container){
		 GradientDrawable border = new GradientDrawable();
		    border.setColor(0xFFFAFAFA); //white background
		    border.setStroke(3, 0xFFE6E6EA); //black border with full opacity
		    if(Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
		    	followup_widget_container.setBackgroundDrawable(border);
		    } else { 
		    	followup_widget_container.setBackground(border);
		    }
		    
		    WindowManager wm = (WindowManager) getActivity().getSystemService(Context.WINDOW_SERVICE);
			Display display  =wm.getDefaultDisplay() ;
			Point point  = new Point() ; 
			display.getSize(point) ; 
			 screenWidth = point.x ; 
			screenHeight = point.y ;
		    LinearLayout.LayoutParams params = (android.widget.LinearLayout.LayoutParams) followup_widget_container.getLayoutParams() ; 
		    params.setMargins(screenWidth / 30, screenHeight / 30, screenWidth / 30, screenHeight / 100) ;
		    followup_widget_container.setLayoutParams(params) ;
		    
		    /*
		    LinearLayout fil_dariane = (LinearLayout) fragmentView.findViewById(R.id.fil_dariane) ; 
		    android.widget.LinearLayout.LayoutParams arianeLayoutParams = new android.widget.LinearLayout.LayoutParams(android.widget.LinearLayout.LayoutParams.MATCH_PARENT, getResources().getDimensionPixelSize(R.dimen.next_prev_height_button)) ; 
		    fil_dariane.setLayoutParams(arianeLayoutParams) ;
		  */
		    
		    
		     
	}
	
	@Override
	public void onStop() {
		// TODO Auto-generated method stub
		if(deletePopUp!= null){
			
			deletePopUp.dismiss();
			deletePopUp = null;
		}
		super.onStop();
		deletePopUp = null;
	}
	
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		
		
			fragmentView = inflater.inflate(R.layout.fragement_filling_form_step3	,null);
			addfilesButton = (Button) fragmentView.findViewById(R.id.filling_form_step3_add_files) ; 
			filecontainer = (LinearLayout) fragmentView.findViewById(R.id.step3_file_container) ; 
			
			  LinearLayout step3_file_container_super = (LinearLayout) fragmentView.findViewById(R.id.step3_file_container_super) ; 
			    FrameLayout.LayoutParams paramsSuper = (FrameLayout.LayoutParams) step3_file_container_super.getLayoutParams() ; 
			    paramsSuper.setMargins(screenWidth/60, screenHeight/60, screenWidth/60, screenHeight/60) ;  
			    step3_file_container_super.setLayoutParams(paramsSuper) ;
			
			LinearLayout followup_widget_container =(LinearLayout) fragmentView.findViewById(R.id.followup_widget_container) ; 
			fixLayout(followup_widget_container) ; 
			   ScrollView step3_files_container = (ScrollView) fragmentView.findViewById(R.id.step3_files_container) ; 
			   
			   GradientDrawable border = new GradientDrawable();
			    border.setColor(0xFFFAFAFA); //white background
			    border.setStroke(3, 0xFFE6E6EA); //black border with full opacity
			    if(Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
			    	step3_files_container.setBackgroundDrawable(border);
			    } else { 
			    	step3_files_container.setBackground(border);
			    }
			    
			    WindowManager wm = (WindowManager) getActivity().getSystemService(Context.WINDOW_SERVICE);
				Display display  =wm.getDefaultDisplay() ;
				Point point  = new Point() ; 
				display.getSize(point) ; 
				int screenWidth = point.x ; 
				int screenHeight = point.y ;
			    LinearLayout.LayoutParams params = (android.widget.LinearLayout.LayoutParams) step3_files_container.getLayoutParams() ; 
			    params.setMargins(screenWidth/30, screenHeight/30, screenWidth/30, screenHeight/30) ; 
			    step3_files_container.setLayoutParams(params) ;
			    
			    backButton = (Button) fragmentView.findViewById(R.id.step3_previousbutton) ; 
			   
			    backButton.setOnClickListener(new View.OnClickListener() {
					  
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						BiptagNavigationManager.pushFragments(EditFormStep3.this, false, "edit_form_3", NavigationActivity.getContext(), null, 2) ; 

					}
				})  ; 
			    
			    
			    CheckBox checkBox = (CheckBox) fragmentView.findViewById(R.id.checkBox1) ; 
				checkBox.setButtonDrawable(getActivity().getResources().getDrawable(R.drawable.checkbox_states)) ; 
				checkBox.setPadding(checkBox.getPaddingLeft() + (int)(10.0f * 2 + 0.5f), 0, 0, 0);

			
			
			openFollowUpActivity = (Button) fragmentView.findViewById(R.id.step3_open_edit_follow_up) ; 
			openFollowUpActivity.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
				//Intent intent = new Intent(getActivity(), EditFollowUpMailActivity.class) ; 
				//startActivity(intent) ; 
				String email = String.valueOf(BiptagUserManger.getUserValue("contact_email_edit", "string", getActivity())) ; 

				EditFollowUpFragement fragement = new EditFollowUpFragement(1, email, 0 , 2) ; 
				
				JSONObject data = new JSONObject() ; 
				try {
					data.put("mode", 1) ; 
					data.put("stack", 2) ; 
					data.put("email", email) ; 
					data.put("id", 0) ; 
				} catch (JSONException e) {
					// TODO: handle exception
				}
				
				BiptagNavigationManager.pushFragments(fragement, true, "edit_follow_up", NavigationActivity.getContext(), data.toString(), 2) ; 
				
				  
				 
				} 
			});
			
			LayoutParams fileIconLayoutParams = new LayoutParams(screenHeight/8, screenHeight/8) ; 
			fileIconLayoutParams.setMargins(screenWidth/60, screenHeight/60, screenWidth/60, screenHeight/60) ; 
			addfilesButton.setLayoutParams(fileIconLayoutParams); 
			
			addfilesButton.setWidth(screenHeight/8) ; 
			addfilesButton.setHeight(screenHeight/8) ; 
			
			LayoutParams buttonLayoutParams  = (LayoutParams) addfilesButton.getLayoutParams() ; 
			buttonLayoutParams.setMargins(screenWidth/60, screenHeight/60, screenWidth/60, screenHeight/60) ;
			
			
			
			addfilesButton.setOnClickListener(new View.OnClickListener() {
				 
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
				/*Intent intent= new Intent(getActivity(), ListingFilesActivity.class) ; 
				startActivityForResult(intent,RESULT_CODE_SELECT_FILES ) ; 
				*/
					
					filesActivity = new ListingFilesActivity(2) ; 
				BiptagNavigationManager.pushFragments(filesActivity, true, "add_file_fragment", NavigationActivity.getContext(), null, 2) ; 
				
				
				}
			});
			finish = (Button) fragmentView.findViewById(R.id.step3_nextbutton) ; 
			
			finish.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					BiptagUserManger.clearField(NavigationActivity.getContext(),"cardscanned");
					BiptagUserManger.clearField(NavigationActivity.getContext(),"ocrScanReturn");
					BiptagUserManger.clearField(NavigationActivity.getContext(),"validate");
					new EditContactWS().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, null) ;
					ContactFragement.fragmentView=null;

				}
				 
			}); 
			
			 
			 
			try {
				 String contat_data = getArguments().getString("data") ; 
				JSONObject jsonObject = new JSONObject(contat_data) ; 
				int contact_local_id= jsonObject.getInt("CONTACT_LOCAL_ID") ; 
				int agent_id = Integer.parseInt(String.valueOf(BiptagUserManger.getUserValue("AGENT_ID", "int", getActivity()))) ; 
			      Log.d("found a contact", "found a contact id "+contact_local_id) ; 

					BTContactDataSource dataSource = new BTContactDataSource(NavigationActivity.getContext()) ; 
					BTContact contact  = dataSource.getContactWithLocalID(agent_id, contact_local_id) ;
					if(contact != null)
					((CheckBox) fragmentView.findViewById(R.id.checkBox1)).setChecked(contact.isDoFollowup()) ;
					
			} catch (JSONException e) {
				// TODO: handle exception
			}
			
			//loadFiles() ; 
			addContactFiles() ; 
			initFilDarianeNavigation() ;





		return fragmentView;
		
	}
	
	public void  addContactFiles() {
		
		try {
			 String contat_data = getArguments().getString("data") ; 
			 contactResponseJson = new JSONObject(contat_data).getJSONObject("CONTACT_RESPONSE") ; 
			 JSONArray files  =	new JSONArray(); 
			 JSONArray links  =	new JSONArray(); 
			 JSONArray vcards = new JSONArray(); 
						
			 if(contactResponseJson.has("FILES")){
				 files = new JSONArray(contactResponseJson.getString("FILES")) ; 
					 
			 }
			 if(contactResponseJson.has("LINKS")){
				 links = new JSONArray(contactResponseJson.getString("LINKS")) ; 
						 
			 }
			 if(contactResponseJson.has("VCARDS")){
				 vcards = new JSONArray(contactResponseJson.getString("VCARDS")) ; 
						 
			 }
			Log.d("Contact edit step3", "edit step 3 files "+files) ; 
			Log.d("Contact edit step3", "edit step 3 links "+links) ;
			Log.d("Contact edit step3", "edit step 3 vcards  "+vcards) ;
			
			
			JSONObject object = new JSONObject() ; 

				object.put("FILES", files) ; 
				object.put("LINKS", links) ; 
				object.put("VCARDS", vcards) ; 
			Log.d("edit step 3 saving files selected", "edit step 3 saving files selected ") ; 
			String add_files_result_edit_value = String.valueOf(BiptagUserManger.getUserValue("add_files_result_edit", "string", getActivity()))  ; 
			if(add_files_result_edit_value.equals("")){
				BiptagUserManger.saveUserData(add_files_result_edit, object.toString(), getActivity()) ; 	
			}
			
				
		} catch (JSONException e) {
			// TODO: handle exception
			Log.d("Contact edit step3", "edit step 3 ad files  "+e.getMessage()) ; 
			
		}
		
		
			
		
		
		
	}
	
	public void loadFiles(){

		
		try {
			

			files = contactResponseJson.getJSONArray("FILES").toString() ;   
			links= contactResponseJson.getJSONArray("LINKS").toString() ; 
			vcards = contactResponseJson.getJSONArray("VCARDS").toString() ; 
			Log.d("activity result ", " activity result file selection  "+files) ; 
			Log.d("activity result ", " activity result file selection  "+links) ; 
			Log.d("activity result ", " activity result file selection  "+vcards) ; 
			LayoutInflater inflater = LayoutInflater.from(getActivity());
		
			
			rearrangeFiles() ; 
		
		
			
		 
		 
		
		
			
		} catch (JSONException e) {
			// TODO: handle exception
		}
		
		
		
	
	
	
	
	}

	// LZR : 14/04/2016 delete the file clicked
	public void deleteAttachedFile(String type , String id ){

		// TODO Auto-generated method stub
		Log.d("delte item ", "delete item "+files + " type " + type  + " id " + id)  ;
		JSONArray filesSelectedCopy  = new JSONArray() ; 
		JSONArray linksSelectedCopy  = new JSONArray() ; 
		JSONArray cardsSelectedCopy  = new JSONArray() ;
		
		
		
		if(type.equals("FILE")){
			
			try {
				JSONArray filesSelected = new JSONArray(files) ; 
				for (int j = 0; j < filesSelected.length()	; j++) {
					if(!filesSelected.getJSONArray(j).getString(2).equals(id)){
						filesSelectedCopy.put(filesSelected.getJSONArray(j)) ; 
					}else{
						Log.d("delte item", "delete item found") ;
					}
				}
				
				files = filesSelectedCopy.toString() ; 
				Log.d("delte item ", "delete item after "+files) ;
			} catch (JSONException e) {
				// TODO: handle exception
				Log.d("delete attached file exception", "delete attached file file exception "+e.getMessage()) ; 
				Log.d("delte item exception", "delete item exception "+e.getMessage()) ; 
			}
			
			
			
		}else{
			try {
				filesSelectedCopy  = new JSONArray(files) ; 
			} catch (JSONException e) {
				// TODO: handle exception
			}
		}
		
		
			if(type.equals("LINK")){
			
			try {
				JSONArray linskSelected = new JSONArray(links) ;
				for (int j = 0; j < linskSelected.length()	; j++) {
					Log.d("delete linkg", "delete file item "+linskSelected.getJSONArray(j)) ; 
					if(!linskSelected.getJSONArray(j).getString(3).equals(id)){
						linksSelectedCopy.put(linskSelected.getJSONArray(j)) ; 
					}
				}
				links = linksSelectedCopy.toString() ; 
				
			} catch (JSONException e) {
				// TODO: handle exception
				Log.d("delete attached file exception", "delete attached file link exception "+e.getMessage()) ; 

			}
			
			
			
		}else{
			try {
				linksSelectedCopy = new JSONArray(links) ; 
			} catch (JSONException e) {
				// TODO: handle exception
			}
		}
			
			if(type.equals("VCARD")){
				
				try {
					JSONArray cardsSelected = new JSONArray(vcards) ; 
					for (int j = 0; j < cardsSelected.length()	; j++) {
						if(!cardsSelected.getJSONArray(j).getString(2).equals(id)){
							cardsSelectedCopy.put(cardsSelected.getJSONArray(j)) ; 
						}
					}
					vcards = cardsSelectedCopy.toString() ; 
					
				} catch (JSONException e) {
					// TODO: handle exception
					Log.d("delete attached file exception", "delete attached file vcard exception "+e.getMessage()) ; 

				}
				
				
				 
			}else{
				try {
					cardsSelectedCopy = new JSONArray(vcards) ; 
				} catch (JSONException e) {
					// TODO: handle exception
				}
			}
		
			JSONObject object = new JSONObject() ; 

			try {
				object.put("FILES", filesSelectedCopy) ; 
				object.put("LINKS", linksSelectedCopy) ; 
				object.put("VCARDS", cardsSelectedCopy) ; 
			} catch (JSONException e) {
				// TODO: handle exception
				Log.d("delete attached file exception", "delete attached file exception "+e.getMessage()) ;
				
			}
			
			
			Log.d("delete attahced file", "delete attached file after delete "+object.toString()) ;
			BiptagUserManger.saveUserData(add_files_result_edit, object.toString(), getActivity()) ; 
			
			deletePopUp.dismiss() ; 
				
			//((LinearLayout)fileicon.getParent()).removeView(fileicon) ;

		rearrangeFiles() ;

	
	
	}
	// LZR : 14/04/2016 arranger la liste des fichiers ( docs carte de visite lien ... ) apres jout ou suppression des fichiers
	public void rearrangeFiles(){

		//filling_form_step3_add_files
		//step3_file_container_first_row
do {
	int rowCount = filecontainer.getChildCount();
	Log.d(" rowcount ", rowCount + " row count " + filecontainer.getScrollBarSize());
	for (int i = 0; i < rowCount; i++) {
		View row = filecontainer.getChildAt(i);
		if (i == 0) {
			LinearLayout firstRow = (LinearLayout) filecontainer.getChildAt(0);
			int childCount = firstRow.getChildCount();
			Log.d("first row 1 ", "first row ");
			for (int j = 0; j < childCount; j++) {
				Log.d("first row 1 ", "first row  child count " + firstRow.getChildCount());
				View addButton = firstRow.getChildAt(j);
				if (addButton != null)
					if (j != 0) {
						Log.d("first row 1 ", "first row not the button");

						try {
							firstRow.removeViewAt(j);
							//	filecontainer.removeViewAt(j) ;
						} catch (Exception e) {
							// TODO: handle exception
							Log.d("add file", "add file remove file icon exc " + e.getMessage());
						}
					}

			}
		} else {
			try {
				filecontainer.removeViewAt(i);

			} catch (Exception e) {
				// TODO: handle exception
				Log.d("add file", "add file remove file icon exc 1 " + e.getMessage());

			}
		}
	}
}while (filecontainer.getChildCount()!=1);
/*
		int rowCount1 = filecontainer.getChildCount() ;
		Log.d(" rowcount 1 ", rowCount +" row count " + filecontainer.getScrollBarSize());
		for (int i = 0; i < rowCount1; i++) {
			View row = filecontainer.getChildAt(i) ;
			if(i == 0)
			{
				LinearLayout firstRow = (LinearLayout ) filecontainer.getChildAt(0) ;
				int childCount = firstRow.getChildCount();
				Log.d("first row 2 ", "first row " + childCount) ;
				for (int j = 0; j < childCount ; j++) {
					Log.d("first row 2 ", "first row  child count "+firstRow.getChildCount() + " i "+i +" j " + j) ;
					View addButton = firstRow.getChildAt(j) ;
					if(addButton != null)
						if(j  == 1){

							try {
									firstRow.removeViewAt(j) ;
							//	filecontainer.removeViewAt(j) ;
								Log.d("first row 2 ", "first row not the button " + j) ;


							} catch (Exception e) {
								// TODO: handle exception
							}
						}

				}
			}else{

				try {
					filecontainer.removeViewAt(i) ;

				} catch (Exception e) {
					// TODO: handle exception
				}
			}
		}



*/


				//Log.d("activity result ", " activity result file selection  "+files) ;
				//Log.d("activity result ", " activity result file selection  "+links) ;
				//Log.d("activity result ", " activity result file selection  "+vcards) ;

		addFileIcons();





	}

	// LZR : 14/04/2016 add new files
	public void addFileIcons(){
		LayoutInflater inflater = LayoutInflater.from(getActivity());
		try {
			  JSONArray filesSelected = new JSONArray(files) ; 
			
			
			for (int i = 0; i < filesSelected.length(); i++) {
				JSONArray item = filesSelected.getJSONArray(i) ;  
				Log.d("item selected", "item selected "+item) ;
				Log.d("item selected", "item selected "+item.getString(0)) ;
				final  String type = "FILE" ; 
				final String id = item.getString(2) ; 
				Log.d("item selected", "item selected size "+item.getString(0).split("\\.").length) ; 
				String filename = item.getString(0) ;
				String nameOnly = filename.split("\\.")[0] ; 
				String extension = filename.split("\\.")[filename.split("\\.").length-1] ; 
				final LinearLayout fileicon = (LinearLayout) inflater.inflate(R.layout.fileicon, null, false);
					((TextView)fileicon.findViewById(R.id.file_icon_file_name)).setText(nameOnly + "." + extension) ; 
				//	((TextView)fileicon.findViewById(R.id.file_icon_file_type)).setText(extension) ; 
					((TextView)fileicon.findViewById(R.id.file_icon_type)).setText(type) ; 
					((TextView)fileicon.findViewById(R.id.file_icon_id)).setText(String.valueOf(id)) ; 
					
					fileicon.setOnClickListener(new View.OnClickListener() {
						
						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
							Log.d("file click", "file click") ; 
							if(deletePopUp!= null){
								deletePopUp.dismiss() ; 
							}
							LayoutInflater inflater= LayoutInflater.from(getActivity()) ; 
							View deleteLayout= inflater.inflate(R.layout.pop_delete_file, null) ;
							WindowManager wm = (WindowManager) getActivity().getSystemService(Context.WINDOW_SERVICE);
							Display display  =wm.getDefaultDisplay() ;
							Point point  = new Point() ; 
							display.getSize(point) ; 
							int screenWidth = point.x ; 
							int screenHeight = point.y ; 
							deletePopUp = new PopupWindow(deleteLayout , screenWidth/6, screenHeight/12) ;
							deletePopUp.showAsDropDown(v); 
							deletePopUp.setBackgroundDrawable(new BitmapDrawable());
							
							
							Button delteItemBTN = (Button) deleteLayout.findViewById(R.id.popup_delete_button) ;
							delteItemBTN.setOnClickListener(new View.OnClickListener() {
								
								@Override
								public void onClick(View v) {
									Log.d(" delete file ", " delete 2 ");
									deleteAttachedFile(type , id) ;

								}

								
							}) ; 
							
							
							
							
						}
					}) ; 
					
					
					LayoutParams fileIconLayoutParams = new LayoutParams(screenHeight/8, screenHeight/8) ; 
					fileIconLayoutParams.setMargins(screenWidth/60, screenHeight/60, screenWidth/60, screenHeight/60) ; 
					fileicon.setLayoutParams(fileIconLayoutParams); 
					int rowNumber = filecontainer.getChildCount();
					LinearLayout lastRow = (LinearLayout)filecontainer.getChildAt(rowNumber-1) ; 
				//	fileicon.setLayoutParams(new TableLayout.LayoutParams(	LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT, 1f)) ; 
					if(lastRow.getChildCount()==4){
						LinearLayout row = new LinearLayout(getActivity()) ;
						filecontainer.addView(row) ;   
					}
					int newrowNumber = filecontainer.getChildCount();
					LinearLayout newlastRow = (LinearLayout)filecontainer.getChildAt(newrowNumber-1) ; 
					newlastRow.addView(fileicon) ; 
			}
			
			
		} catch (JSONException e) {
			// TODO: handle exception
			Log.d("activity result ", " activity result file exception  "+e.getMessage()) ; 
			
		}
		
		try {
			JSONArray cardsSelected = new JSONArray(vcards) ; 
			for (int i = 0; i < cardsSelected.length(); i++) {
				JSONArray item = cardsSelected.getJSONArray(i) ; 
				final  String type = "VCARD" ; 
				final  String id = item.getString(2) ;
				final LinearLayout fileicon = (LinearLayout) inflater.inflate(R.layout.fileicon, null, false);
					
					((TextView)fileicon.findViewById(R.id.file_icon_file_name)).setText("CARD : " +item.getString(0)) ; 
		//	((TextView)fileicon.findViewById(R.id.file_icon_file_type)).setText("CARD") ; 
					((TextView)fileicon.findViewById(R.id.file_icon_type)).setText(type) ; 
					((TextView)fileicon.findViewById(R.id.file_icon_id)).setText(id) ; 
					fileicon.setOnClickListener(new View.OnClickListener() {
						
						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
							Log.d("file click", "file click") ; 
							if(deletePopUp!= null){
								deletePopUp.dismiss() ; 
							}
							LayoutInflater inflater= LayoutInflater.from(getActivity()) ; 
							View deleteLayout= inflater.inflate(R.layout.pop_delete_file, null) ;
							WindowManager wm = (WindowManager) getActivity().getSystemService(Context.WINDOW_SERVICE);
							Display display  =wm.getDefaultDisplay() ;
							Point point  = new Point() ; 
							display.getSize(point) ; 
							int screenWidth = point.x ; 
							int screenHeight = point.y ;
							deletePopUp = new PopupWindow(deleteLayout , screenWidth/6, screenHeight/12) ;
							deletePopUp.showAsDropDown(v); 
							deletePopUp.setBackgroundDrawable(new BitmapDrawable());
							
							
							Button delteItemBTN = (Button) deleteLayout.findViewById(R.id.popup_delete_button) ;
							delteItemBTN.setOnClickListener(new View.OnClickListener() {
								
								@Override
								public void onClick(View v) {
									Log.d(" delete file ", " delete 3 ");
									deleteAttachedFile(type, id) ; 
									
								}
							}) ; 
							
							
							
							
						}
					}) ; 
					LayoutParams fileIconLayoutParams = new LayoutParams(screenHeight/8, screenHeight/8) ; 
					fileIconLayoutParams.setMargins(screenWidth/60, screenHeight/60, screenWidth/60, screenHeight/60) ; 
					fileicon.setLayoutParams(fileIconLayoutParams); 
					int rowNumber = filecontainer.getChildCount();
					LinearLayout lastRow = (LinearLayout)filecontainer.getChildAt(rowNumber-1) ; 
				//	fileicon.setLayoutParams(new TableLayout.LayoutParams(	LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT, 1f)) ; 
					if(lastRow.getChildCount()==4){
						LinearLayout row = new LinearLayout(getActivity()) ;
						filecontainer.addView(row) ;   
					}
					int newrowNumber = filecontainer.getChildCount();
					LinearLayout newlastRow = (LinearLayout)filecontainer.getChildAt(newrowNumber-1) ; 
					newlastRow.addView(fileicon) ; 
			}
			
			
		} catch (JSONException e) {
			// TODO: handle exception
			Log.d("activity result ", " activity result file exception  "+e.getMessage()) ; 
			
		}
		
		try {
			JSONArray linskSelected = new JSONArray(links) ; 
			for (int i = 0; i < linskSelected.length(); i++) {
				JSONArray item = linskSelected.getJSONArray(i) ; 
				final  String type = "LINK" ; 
				final  String id = item.getString(3) ;
				final LinearLayout fileicon = (LinearLayout) inflater.inflate(R.layout.fileicon, null, false);
					((TextView)fileicon.findViewById(R.id.file_icon_file_name)).setText("Http : " +item.getString(0)) ; 
					//((TextView)fileicon.findViewById(R.id.file_icon_file_type)).setText("Http") ; 
					((TextView)fileicon.findViewById(R.id.file_icon_type)).setText(type) ;
					((TextView)fileicon.findViewById(R.id.file_icon_id)).setText(id) ; 
					fileicon.setOnClickListener(new View.OnClickListener() {
						
						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
							Log.d("file click", "file click") ; 
							if(deletePopUp!= null){
								deletePopUp.dismiss() ; 
							}
							LayoutInflater inflater= LayoutInflater.from(getActivity()) ; 
							View deleteLayout= inflater.inflate(R.layout.pop_delete_file, null) ;
							WindowManager wm = (WindowManager) getActivity().getSystemService(Context.WINDOW_SERVICE);
							Display display  =wm.getDefaultDisplay() ;
							Point point  = new Point() ; 
							display.getSize(point) ; 
							int screenWidth = point.x ; 
							int screenHeight = point.y ;
							deletePopUp = new PopupWindow(deleteLayout , screenWidth/6, screenHeight/12) ;
							deletePopUp.showAsDropDown(v); 
							deletePopUp.setBackgroundDrawable(new BitmapDrawable());
							
							
							Button delteItemBTN = (Button) deleteLayout.findViewById(R.id.popup_delete_button) ;
							delteItemBTN.setOnClickListener(new View.OnClickListener() {
								
								@Override
								public void onClick(View v) {
							Log.d(" delete file ", " delete 1 ");
									deleteAttachedFile(type, id) ; 
								}
							}) ; 
							
							
							
							
						}
					}) ; 
					
					LayoutParams fileIconLayoutParams = new LayoutParams(screenHeight/8, screenHeight/8) ; 
					fileIconLayoutParams.setMargins(screenWidth/60, screenHeight/60, screenWidth/60, screenHeight/60) ; 
					fileicon.setLayoutParams(fileIconLayoutParams); 
					int rowNumber = filecontainer.getChildCount();
					LinearLayout lastRow = (LinearLayout)filecontainer.getChildAt(rowNumber-1) ; 
				//	fileicon.setLayoutParams(new TableLayout.LayoutParams(	LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT, 1f)) ; 
					if(lastRow.getChildCount()==4){
						LinearLayout row = new LinearLayout(getActivity()) ;
						filecontainer.addView(row) ;   
					}
					int newrowNumber = filecontainer.getChildCount();
					LinearLayout newlastRow = (LinearLayout)filecontainer.getChildAt(newrowNumber-1) ; 
					newlastRow.addView(fileicon) ; 
			}
			 
			 
		} catch (JSONException e) {
			// TODO: handle exception
			Log.d("activity result ", " activity result file exception  "+e.getMessage()) ; 
			
		} 
		 
		
		
			
	}
	
	
	
	@Override
	public void onResume() { 
		// TODO Auto-generated method stub
		super.onResume();
		String add_files_resultString = String.valueOf(BiptagUserManger.getUserValue("add_files_result_edit", "string", getActivity())) ;

		BiptagUserManger.clearField(NavigationActivity.getContext() , CaptureActivity.vcardIdKeyCreate);
		BiptagUserManger.clearField(NavigationActivity.getContext(), CaptureActivity.vcardIdKeyEdit);

		Log.d("file selected", "file selected " + add_files_resultString) ;
		if(add_files_resultString.length()!= 0){
		try {
			JSONObject add_files_result = new JSONObject(add_files_resultString) ; 
			

			files = add_files_result.getJSONArray("FILES").toString() ;   
			links= add_files_result.getJSONArray("LINKS").toString() ; 
			vcards = add_files_result.getJSONArray("VCARDS").toString() ; 
			Log.d("activity result ", " activity result file selection  "+files) ; 
			Log.d("activity result ", " activity result file selection  "+links) ; 
			Log.d("activity result ", " activity result file selection  "+vcards) ; 
		
			rearrangeFiles() ; 
		
			
		} catch (JSONException e) {
			// TODO: handle exception
		}
		}
		
	}
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		
		if(requestCode== RESULT_CODE_SELECT_FILES){}
	
	
	}
	
	
	public void gotoContactFragmen( BTContact btContact, int id , int mode){
		// List<BTContact> savedContact;
	//	ContactFragement.fragmentView = null ;
		Log.d(" update ", " mode " + mode);
			Object agent_id_object = BiptagUserManger.getUserValue("AGENT_ID", "int", NavigationActivity.getContext());
			int agent_id = Integer.parseInt(String.valueOf(agent_id_object)) ;
			Log.d(" contact " , " agent id " + agent_id + " form id " + id );
			Log.d(" contact ",btContact.getContactEmail() + "name  " + btContact.getContactName() + " formid " + btContact.getContactFormId());
			BTFormDataSource btFormDataSource = new BTFormDataSource(NavigationActivity.getContext());
			Log.d(" contact ", " form " + btFormDataSource.getFormWithID(agent_id, id).getFormName());

		if (btContact.isSynchronized() == 1) {
			ContactItem contact  = new ContactItem( btContact.getContactEmail() ,btContact.getContactName()  ,btContact.getContactFrResponse(),
					btFormDataSource.getFormWithID(agent_id,id).getFormName(), btContact.getContactId(),
					btContact.getContactFormId(),  btContact.getContactFrID(), true, btContact.getContact_local_id());

//			Log.d(" update ", " contact details "+ contact.contactId + " " + contact.contact_local_id + " " + contact.isSynchronized + " local id of 5 catact " + ContactFragement.adapterSync.data.get(5).contact_local_id );
//			Log.d(" contact ", " update name " + contact.contact_name + " email " + contact.contact_email + " id " + contact.contactId + " adaptersync " );
			ContactFragement.updateConctact(contact, mode);

		}else{

			ContactItem contact  = new ContactItem( btContact.getContactEmail() ,btContact.getContactName()  ,btContact.getContactFrResponse(),
					btFormDataSource.getFormWithID(agent_id,id).getFormName(), btContact.getContactId(),
					btContact.getContactFormId(),  btContact.getContactFrID(), false, btContact.getContact_local_id());

//			Log.d(" update ", " contact details "+ contact.contactId + " " + contact.contact_local_id + " " + contact.isSynchronized + " local id of 5 catact " + ContactFragement.adapterSync.data.get(5).contact_local_id );
			Log.d(" contact ", " update name " + contact.contact_name + " email " + contact.contact_email + " id " + contact.contactId + " adaptersync " );
			ContactFragement.updateConctact(contact, mode);
		}

		BiptagUserManger.clearField(NavigationActivity.getContext(), add_files_result_edit) ;
		BiptagUserManger.clearField(NavigationActivity.getContext(), editImageArrayUserPreferenceName)  ; 
		BiptagUserManger.clearField(NavigationActivity.getContext(), "contact_email_edit") ; 
		
		//BiptagUserManger.saveUserData(add_files_result_edit, "", getActivity()) ; 
		//BiptagUserManger.saveUserData(editImageArrayUserPreferenceName, "", getActivity()) ; 
		//BiptagUserManger.saveUserData("contact_email_edit", "",	 getActivity()) ; 
		ContactFragement contactFragement = new ContactFragement()  ; 
		
		for (int i = 0; i < NavigationActivity.stack2.size(); i++) {
			if(NavigationActivity.stack2.get(i).getClass().getName().equals(EditFormStep1.class.getName())){
				try {
						NavigationActivity.stack2.get(i).onDestroy() ; 
									
				} catch (NullPointerException e) {
					// TODO: handle exception
				}			}
			
			if(NavigationActivity.stack2.get(i).getClass().getName().equals(EditFormStep2.class.getName())){
				try {
						NavigationActivity.stack2.get(i).onDestroy() ; 
									
				} catch (NullPointerException e) {
					// TODO: handle exception
				}			}
		}
		
		
		
		NavigationActivity.stack2 = new LinkedList<Fragment>() ;
		BiptagNavigationManager.pushFragments(contactFragement, true, "", NavigationActivity.getContext() ,"", 2 );	
		onDestroy() ; 
	}
	
	public class EditContactWS extends AsyncTask<String, String, String>{

		@Override
		protected String doInBackground(String... params) {
			// TODO Auto-generated method stub
			
			try{

			String data_form_1 = String.valueOf(BiptagUserManger.getUserValue("edit_data_step1", "string", getActivity()))	;
			String data_form_2 = String.valueOf(BiptagUserManger.getUserValue("edit_data_step2", "string", getActivity()))	;
			String images = String.valueOf(BiptagUserManger.getUserValue("images", "string", getActivity())); 
			JSONArray images_json_array = new JSONArray() ; 
			Log.d("finish contact ", "finish contact image  "+images) ; 
			if(!images.equals("")) {
				images_json_array = new JSONArray(images) ; 
				
			}
			ArrayList<String> images_list_array = new ArrayList<String>();     
			
			if (images_json_array != null) { 
			   int len = images_json_array.length();
			   for (int i=0;i<len;i++){ 
				   images_list_array.add(images_json_array.get(i).toString());
			   } 
			} 
			String email = String.valueOf(BiptagUserManger.getUserValue("contact_email_edit", "string", getActivity())) ; 
			int ba_id = Integer.parseInt(String.valueOf(BiptagUserManger.getUserValue("BA_ID", "int", getActivity()))) ; 
			int agent_id = Integer.parseInt(String.valueOf(BiptagUserManger.getUserValue("AGENT_ID", "int", getActivity()))) ;
			int user_id= Integer.parseInt(String.valueOf(BiptagUserManger.getUserValue("USER_ID", "int", getActivity()))) ; 
			 
			String product_config = String.valueOf(BiptagUserManger.getUserValue("PRODUCT_CONFIG", "string", getActivity())) ; 
			JSONObject product_config_object = new JSONObject(product_config) ; 
			Log.d("fisih contact ", "finish contact product config "+product_config_object.toString()) ; 
			String follow_up = product_config_object.getString("follow_up_mail") ; 
			String contact_actif =product_config_object.getString("contact_actif") ; 
			final String jsondataString = getArguments().getString("data") ; 
			JSONObject jsondata= new JSONObject(jsondataString) ; 
			
			Log.d("step3 finish", "step3 finish data1 "+data_form_1) ; 
			Log.d("step3 finish", "step3 finish data2 "+data_form_2) ; 
			Log.d("step3 finish", "step3 finish email  "+email) ; 
			JSONObject contact_response = new JSONObject(data_form_2) ; 
			contact_response.put("FILES", files) ; 
			contact_response.put("LINKS", links) ;
			contact_response.put("VCARDS", vcards) ; 
			
			
			

			String imageArrayString = String.valueOf(BiptagUserManger.getUserValue(editImageArrayUserPreferenceName, "string", getActivity())) ; 
			JSONArray imageArray  ; 
			try {
				Log.d("sendin image", "sendin image string "+imageArrayString) ; 
				 JSONArray imageArrayIds = new JSONArray(imageArrayString)  ;
				 imageArray = new JSONArray() ; 
				 for (int i = 0; i < imageArrayIds.length(); i++) {
					int imageId = imageArrayIds.getInt(i) ; 
					BTImageDataSource imageDataSource = new BTImageDataSource(getActivity()) ; 
					BTImage image  = imageDataSource.selectImagesWithId(imageId, ba_id) ; 
					if(image != null){
						String imageName = image.getImageName() ; 
						imageArray.put(imageName) ; 
						BTImage uImage = image ; 
						uImage.setIsSent(0) ; 
						imageDataSource.updateImage(uImage) ; 
							
						
						
						
						
					}else{
						Log.d("sendin image ", "sendin image null") ; 
					}
				}
				 
				 
			} catch (JSONException e) {
				// TODO: handle exception
				Log.d("sendin image ", "sendin image exception "+e.getMessage()) ; 
				imageArray =  new JSONArray() ; 
			}
			JSONArray orginalContactImage = new JSONArray(); 
			if(contact_response.has("CONTACT_IMAGE")){
				try {

					orginalContactImage = contact_response.getJSONArray("CONTACT_IMAGE") ; 
							
				} catch (JSONException e) {
					// TODO: handle exception
				}
			}
			Log.d("sendin image ", "sendin image array  "+imageArray.toString()) ; 
			if(orginalContactImage.length() == 0  || imageArray.length() != 0)
				contact_response.put("CONTACT_IMAGE", imageArray) ; 
			

			List<NameValuePair> list = new ArrayList<NameValuePair>() ; 
			list.add(new BasicNameValuePair("ba_id", String.valueOf(ba_id))) ;
			list.add(new BasicNameValuePair("email", email)) ; 
			
			list.add(new BasicNameValuePair("agent_id", String.valueOf(agent_id))) ; 
			list.add(new BasicNameValuePair("form_id", jsondata.getString("CONTACT_FORM_ID"))) ; 
			list.add(new BasicNameValuePair("user_id", String.valueOf(user_id)));
			final String contat_data = getArguments().getString("data") ; 
			
			JSONObject jsonObject = new JSONObject(contat_data) ; 
			
			Log.d("finish contact ", "finish contact  "+jsonObject.toString()) ;
			
			list.add(new BasicNameValuePair("contact_id", jsonObject.getString("CONTACT_ID"))) ; 
			list.add(new BasicNameValuePair("fr_id", jsonObject.getString("FR_ID"))) ; 
			list.add(new BasicNameValuePair("contact_actif", contact_actif)) ; 
			list.add(new BasicNameValuePair("follow_up", follow_up)) ;
			list.add(new BasicNameValuePair("do_follow_up", ((CheckBox) getView().findViewById(R.id.checkBox1)).isChecked() ? "1" : "0")) ; 
			list.add(new BasicNameValuePair("files", files)) ;
			list.add(new BasicNameValuePair("links", links)) ;
			list.add(new BasicNameValuePair("cards", vcards));

			String  subject = "" ;
			String savedCCEmails = String.valueOf(BiptagUserManger.getUserValue("FU_CCEMAILS_TEMPS", "string", getActivity())) ; 
			String savedCCIEmails = String.valueOf(BiptagUserManger.getUserValue("FU_CCIEMAILS_TEMPS", "string", getActivity())) ;
			String savedContent= String.valueOf(BiptagUserManger.getUserValue("FU_CONTENT_TEMPS", "string", getActivity())) ;
			String savedSubject = String.valueOf(BiptagUserManger.getUserValue("FU_SUBJECT_TEMPS", "string", getActivity())) ; 
			if(savedContent.equals("") && savedSubject.equals("")){
				int selectedTemplateId = 0 ; 	
				try {
						selectedTemplateId = Integer.parseInt(String.valueOf(BiptagUserManger.getUserValue("selected_template", "string", getActivity()))) ; 
						BTFollowupTemplateDataSource dataSource = new BTFollowupTemplateDataSource(getActivity()) ; 
						BTFollowupTemplate followupTemplate =dataSource.getTemplateWithLocalID(ba_id, selectedTemplateId) ;
						Log.d(" followup ", " " + followupTemplate.fu_subject );
						savedContent = followupTemplate.getFu_content() ; 
						savedSubject = followupTemplate.getFu_subject() ; 
						JSONArray attach =followupTemplate.getFu_attach() ; 
						JSONArray ccEmails = new JSONArray() ;
						JSONArray cciEmails = new JSONArray() ;
						for (int i = 0; i < attach.length(); i++) {
							try {
								if(attach.getJSONObject(i).getString("type").equals("cci_email")){
									
								}
							} catch (JSONException e) {
								// TODO: handle exception
							}
						}
					} catch (NumberFormatException e) {
						// TODO: handle exception
					}
				
				
				
				
			}
			
			
			
			list.add(new BasicNameValuePair("text", savedContent));
			list.add(new BasicNameValuePair("subject", savedSubject)) ; 
			try {
			    savedCCEmails = URLEncoder.encode(savedCCEmails,"UTF-8");
			    savedCCIEmails = URLEncoder.encode(savedCCIEmails,"UTF-8");
			} catch (UnsupportedEncodingException e) {
			    e.printStackTrace();
			} 
			
			list.add(new BasicNameValuePair("cci", savedCCEmails )) ;
			list.add(new BasicNameValuePair("cc",savedCCIEmails )) ;
			
			
			list.add(new BasicNameValuePair("user_email", String.valueOf(BiptagUserManger.getUserValue("EMAIL", "string", getActivity())))); 
			List<NameValuePair> postList = new ArrayList<NameValuePair>() ; 
			String contact_response_encoded="";
			try {
				contact_response_encoded = URLEncoder.encode(contact_response.toString(), "UTF-8");
			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} 
			postList.add(new BasicNameValuePair("json_response", contact_response_encoded )) ; 
			Log.d("send contact ", "json hybrid response " + contact_response.toString()) ;
			Log.d("send contact ", "json hybrid response form id "+jsondata.getString("CONTACT_FORM_ID")) ;
			final int form_id = Integer.parseInt(jsondata.getString("CONTACT_FORM_ID")) ;
		
			BTFormulaire form_concerned  = null;
		 
			
			BTFormDataSource formDataSource = new BTFormDataSource(getActivity()) ; 
			form_concerned = formDataSource.getFormWithID(agent_id, form_id);
			JSONArray structure = form_concerned.getFormStructure(); 
			
		//	ContactItem  contact = new ContactItem(email, email, contact_response, structure, 0, form_id, 0	, false) ; 
			JSONObject contactItem = new JSONObject()  ; 
			contactItem.put("CONTACT_NAME", email)  ; 
			contactItem.put("CONTACT_MAIL", email) ; 
			contactItem.put("CONTACT_FORM", structure)  ; 
			contactItem.put("CONTACT_RESPONSE", contact_response) ; 
			contactItem.put("CONTACT_ID", jsonObject.getString("CONTACT_ID")) ; 
			contactItem.put("CONTACT_FORM_ID", form_id);
			contactItem.put("CONTACT_FR_ID",jsonObject.getString("FR_ID")) ; 
			final BTContact contact = new BTContact() ;
			contact.setContactEmail(email) ;

				Calendar cal = Calendar.getInstance();
				Date today = new Date();
				cal.setTime(today);


				SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");


				String 	referenceDate = formatter.format(cal.getTime());
				contact.setCreateTime(referenceDate);

				List<String> formKeys = new ArrayList<String>() ;
			
			for (int i = 0; i < structure.length(); i++) {
				if(structure.getJSONObject(i).getString("type").equals("LABEL")){
					formKeys.add(structure.getJSONObject(i).getJSONArray("VALUE").getString(0)) ; 
				}
			}
			
			int contact_local_id= jsonObject.getInt("CONTACT_LOCAL_ID") ;
			BTContactDataSource btContactDataSource =new BTContactDataSource(NavigationActivity.getContext()) ;

				BTContact btContact = btContactDataSource.getContactWithLocalID(agent_id, contact_local_id) ;
			final int isSynchro = btContact.isSynchronized() ;
				Log.d(" update ", " oldcontactId" + isSynchro);
			String name = contact_response.getString(formKeys.get(1))+" "+contact_response.getString(formKeys.get(2));
			contact.setContactName(name) ; 
			contact.setContactFormId(form_id) ; 
			contact.setContactFrAgentID(agent_id) ;
			contact.setContactFrHashcode("") ; 
			contact.setContactFrID(jsonObject.getInt("FR_ID")) ; 
			contact.setContactFrResponse(contact_response) ;
			contact.setContactId(jsonObject.getInt("CONTACT_ID")) ; 
			contact.setDoFollowup(((CheckBox) getView().findViewById(R.id.checkBox1)).isChecked()) ;
			contact.setSynchronized(btContact.isSynchronized());
			contact.setContact_local_id(contact_local_id) ;
			BTContactDataSource dataSource = new BTContactDataSource(getActivity()) ;
			dataSource.updateContact(contact) ; 
			Log.d(" contact ", " is sync " + btContact.isSynchronized());
				final	String url;
				if (isSynchro==0){
				 url = Constant.base_url + Constant.insert_contact_ws;
				}else {
					 url = Constant.base_url + Constant.update_contact_ws;
				}
				final BTContact bTContact = contact ;

			if(BiptagUserManger.isOnline(getActivity())){
				Log.d(" contact ", " isonline edit  ");
				JSONParser parser = new JSONParser() ;
				JSONObject insertResponse = parser.makeHybridHttpRequest(url, list, postList) ;
				Log.d(" contact ", " insert response  " + insertResponse);

				if(insertResponse!=null){
					if(insertResponse.getInt("success")== 1){
						contact.setSynchronized(1) ;
						contact.setContactFrID(insertResponse.getInt("FR_ID")) ;
						contact.setContactId(insertResponse.getInt("CONTACT_ID")) ;
						contact.setContactFrHashcode(insertResponse.getString("HASH_CODE")) ;

						Log.d(" contact ", " contact synchro");
						dataSource.updateContact(contact) ;
					}
				}


			} else{
				Log.d(" contact ", " isoffline edit  ");
			BTFormDataSource btFormDataSource = new BTFormDataSource(NavigationActivity.getContext());
				ContactItem contactIte  = new ContactItem( contact.getContactEmail() ,contact.getContactName()  ,contact.getContactFrResponse(),
						btFormDataSource.getFormWithID(agent_id,form_id).getFormName(), contact.getContactId(),
						contact.getContactFormId(),  contact.getContactFrID(), false, contact.getContact_local_id());

				JSONParser parser = new JSONParser() ;
				JSONObject insertResponse = parser.makeHybridHttpRequest(url, list, postList) ;
				//if(insertResponse!=null){
					Log.d(" contact ", " contact usynchro 0 ");
				//	if(insertResponse.getInt("success")== 1){
						contact.setSynchronized(0) ;
						Log.d(" contact ", " contact usynchro 1 ");
						dataSource.updateContact(contact);
						ContactFragement.updateConctact(contactIte, isSynchro );
			//		}
			//	}
			}
				getActivity().runOnUiThread(new Runnable() {

					@Override
					public void run() {
						// TODO Auto-generated method stub

						gotoContactFragmen(contact, form_id, isSynchro  ) ;
						Log.d(" contact " , " issynchro " + (isSynchro  ) + " contact.issynchro " + contact.isSynchronized());


					}
				}) ;

			Log.d("finish contact ", "finish contact url " + url) ;
			Log.d("finish contact ", "finish contact " + jsonObject.toString()) ;
		
		} catch (JSONException e) {
			// TODO: handle exception
			Log.d("finish contact ", "finish contact eception  "+e.getMessage()) ;
		}




			return null; 
		} 
		
		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);

		}


	}


}
