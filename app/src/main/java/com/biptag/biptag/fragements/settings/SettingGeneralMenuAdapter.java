package com.biptag.biptag.fragements.settings;

import android.content.Context;
import android.graphics.Point;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AbsListView.LayoutParams;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.biptag.biptag.R;

public class SettingGeneralMenuAdapter extends ArrayAdapter<String>{

	private String[] values  ; 
	private String[] subItems ; 
	int screenWidth , 	 screenHeight  ; 
	
	
	public SettingGeneralMenuAdapter(Context context, int resource,
			String[] objects) {
		super(context, resource, objects);
		// TODO Auto-generated constructor stub
		 WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
			Display display  =wm.getDefaultDisplay() ;
			Point point  = new Point() ; 
			display.getSize(point) ; 
			 screenWidth = point.x ; 
			 screenHeight = point.y ; 
	}
	
	public SettingGeneralMenuAdapter(Context context, int resource,
			String[] objects , String[] subItem ) {
		super(context, resource, objects);
		this.values = objects ; 
		this.subItems = subItem ; 
		
		 WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
			Display display  =wm.getDefaultDisplay() ;
			Point point  = new Point() ; 
			display.getSize(point) ; 
			 screenWidth = point.x ; 
			 screenHeight = point.y ; 
	
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
	
			if(convertView == null){
				 LayoutInflater inflater = (LayoutInflater) getContext()
		                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		            convertView = inflater.inflate(R.layout.genral_menu_list_item, null);
			}
			
			
			TextView item = (TextView) convertView.findViewById(R.id.general_menu_title) ; 
			TextView subItem = (TextView) convertView.findViewById(R.id.general_menu_seprator) ; 
			item.setText(values[position]) ; 
			subItem.setText(subItems[position]) ;
			
			 
			 LayoutParams layoutParams = new LayoutParams(LayoutParams.MATCH_PARENT,screenHeight/13) ; 
			 convertView.setLayoutParams(layoutParams) ; 
		        
			
			 
		 
		
		return convertView;
	}

}
