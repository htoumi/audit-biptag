package com.biptag.biptag.fragements.settings;

import org.json.JSONArray;

import com.biptag.biptag.NavigationActivity;
import com.biptag.biptag.R;
import com.biptag.biptag.managers.BiptagNavigationManager;

import android.annotation.SuppressLint;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnKeyListener;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ListView;

public class AccountsListingFragment extends Fragment {

	JSONArray coWorkers ; 
	View fragmentView ; 
	public AccountsListingFragment() {
		// TODO Auto-generated constructor stub
	}
	@SuppressLint({"NewApi", "ValidFragment"})
	public AccountsListingFragment(JSONArray coWorkers) {
		// TODO Auto-generated constructor stub
		this.coWorkers = coWorkers ; 
	}
	
	
	
	@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			// TODO Auto-generated method stub
		//account_listng_fragment
			fragmentView =  inflater.inflate(R.layout.account_listng_fragment, null) ; 
			ListView listView = (ListView) fragmentView.findViewById(R.id.list_accounts) ; 
			//Log.d("coworkers", "coworkers "+coWorkers.toString()) ;
			AccountListingAdapter adapter = new AccountListingAdapter(getActivity(), android.R.layout.simple_list_item_1, new String[coWorkers.length()],  coWorkers)  ; 
			listView.setAdapter(adapter) ;


		Button actionBarButton = (Button)	getActivity().getActionBar().getCustomView().findViewById(R.id.formste1_btn_container) ;
		actionBarButton.setBackground(NavigationActivity.getContext().getResources().getDrawable(R.drawable.go_back));

	//	actionBarButton.setText(NavigationActivity.getContext().getResources().getString(R.string.return_image_list));

		actionBarButton.setVisibility(View.VISIBLE) ;
		actionBarButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				AccountInformationSetting informationSetting = new AccountInformationSetting() ;
				BiptagNavigationManager.pushFragments(informationSetting, true, this.getClass().getName(), getActivity(), null, 3) ;

			}
		}) ;


		fragmentView.setOnKeyListener(new OnKeyListener() {
				
				@Override
				public boolean onKey(View v, int keyCode, KeyEvent event) {
					// TODO Auto-generated method stub
					//Log.d("key pressed ", "coworkers key pressed ") ;
					if(keyCode == KeyEvent.KEYCODE_BACK){
						BiptagNavigationManager.pushFragments(null, false, "list_accounts", getActivity(), null , 3) ; 
						return true  ; 
					}
					return false;
				}
			});
		
			return fragmentView;
		}

	@Override
	public void onResume() {
		super.onResume();
		Button actionBarButton = (Button)	getActivity().getActionBar().getCustomView().findViewById(R.id.formste1_btn_container) ;
		actionBarButton.setBackground(NavigationActivity.getContext().getResources().getDrawable(R.drawable.go_back));

//		actionBarButton.setText(NavigationActivity.getContext().getResources().getString(R.string.return_image_list));

		actionBarButton.setVisibility(View.VISIBLE) ;


	}

	@Override
	public void onStop() {
		super.onStop();
		Button actionBarButton = (Button)	getActivity().getActionBar().getCustomView().findViewById(R.id.formste1_btn_container) ;
		actionBarButton.setBackground(NavigationActivity.getContext().getResources().getDrawable(R.drawable.go_back));

		//actionBarButton.setText(NavigationActivity.getContext().getResources().getString(R.string.return_image_list));

		actionBarButton.setVisibility(View.INVISIBLE) ;


	}
}
