package com.biptag.biptag.fragements;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ActionBar.LayoutParams;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.GradientDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.ViewTreeObserver.OnScrollChangedListener;
import android.view.WindowManager;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.ScrollView;
import android.widget.TextView;

import com.biptag.biptag.ContactAdapter;
import com.biptag.biptag.ContactItem;
import com.biptag.biptag.NavigationActivity;
import com.biptag.biptag.R;
import com.biptag.biptag.dbmanager.datasource.BTContactDataSource;
import com.biptag.biptag.dbmanager.datasource.BTFormDataSource;
import com.biptag.biptag.dbmanager.datasource.BTLogDataSource;
import com.biptag.biptag.dbmanager.objects.BTContact;
import com.biptag.biptag.dbmanager.objects.BTFormulaire;
import com.biptag.biptag.dbmanager.objects.BTLog;
import com.biptag.biptag.fragements.create.FillingFormStep3;
import com.biptag.biptag.fragements.edit.EditFormStep1;
import com.biptag.biptag.fragements.edit.EditFormStep2;
import com.biptag.biptag.fragements.edit.EditFormStep3;
import com.biptag.biptag.interfaces.AsyncResponse;
import com.biptag.biptag.managers.BiptagNavigationManager;
import com.biptag.biptag.managers.BiptagUserManger;
import com.biptag.biptag.webservices.DownloadContactWithHashCodes;
import com.biptag.biptag.webservices.DownloadContactsWithIds;
import com.biptag.biptag.webservices.DownloadFiles;
import com.biptag.biptag.webservices.DownloadForms;
import com.biptag.biptag.webservices.UploadContacts;
import com.biptag.biptag.webservices.logs.DownloadLogs;
import com.biptag.biptag.webservices.templates.DownloadFollowupHashcodes;
import com.biptag.biptag.webservices.templates.DownloadTemplateWithIds;
import com.biptag.biptag.webservices.templates.TemplateUploader;

public class ContactFragement extends android.app.Fragment implements AsyncResponse {
	boolean totalListIsLoaded = false;
	String referenceDate;
	int indexLoading = 20;
	int startSecond;
	public CharSequence seq;
	boolean isSynchronizing;
	boolean contactDownloaded = false;
	boolean contactDownloadStarted = false;
	JSONObject jsonObject;
	List<JSONObject> organizedContactList;
	List<JSONObject> organizedContactListPart;
	HashMap<String, JSONObject> organizedContact;
	HashMap<String, JSONObject> organizedContactPart;
	public static ContactAdapter adapterSync, adapterUnSync;
	public int id;
	public static ProgressDialog synchronisationDialog;
	public List<BTContact> savedContact;
	int lastIdSelected = 0;
	int oldLastIdSelected = -1;
	public static ScrollView parentScrollView;
	int agent_id;
	NotificationFragment notifFragment;
	EditText searchField;
	float x1, x2, y1, y2, dx, dy;
	public static View fragmentView;
	String direction;
	public static ProgressDialog dialog;
	public static ListView contactlistView, contactListViewUnSync;
	List<String> listDataHeader;
	HashMap<String, List<String>> listDataChild;
	public static List<ContactItem> data_sync = null;
	public static List<ContactItem> data_unsync = null;
	List<ContactItem> data_sync_part = null;
	List<ContactItem> data_unsync_part = null;

	public static List<BTFormulaire> allFormsList = null;
	public static List<BTLog> logs = null;
	public static PopupWindow window, responseChooserWindow;

	static int screenWidth;
	static int screenHeight;


	OnScrollChangedListener generalScrollListener = new OnScrollChangedListener() {


		@Override
		public void onScrollChanged() {
			// TODO Auto-generated method stub

			if (window != null) {
				window.dismiss();
			}
			for (int i = 0; i < contactlistView.getChildCount(); i++) {
				View v = contactlistView.getChildAt(i);
				v.setBackgroundColor(Color.parseColor("#fafafa"));
			}
			for (int i = 0; i < contactListViewUnSync.getChildCount(); i++) {
				View v = contactListViewUnSync.getChildAt(i);
				v.setBackgroundColor(Color.parseColor("#fafafa"));
			}

			View view = (View) parentScrollView.getChildAt(parentScrollView.getChildCount() - 1);

			// Calculate the scrolldiff
			int diff = (int) (view.getBottom() - (parentScrollView.getHeight() + parentScrollView.getScrollY()));
			// if diff is zero, then the bottom has been reached
			if (diff == 0) {
				// notify that we have reached the bottom
				if (!totalListIsLoaded) {
					if (oldLastIdSelected != lastIdSelected) {
						new LoadContactProgressiv().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

					}
				}


			}

		}
	};


	OnScrollListener scrollListener = new OnScrollListener() {

		@Override
		public void onScrollStateChanged(AbsListView view, int scrollState) {
			// TODO Auto-generated method stub
			if (scrollState != 0) {
				for (int i = 0; i < contactlistView.getChildCount(); i++) {
					View v = contactlistView.getChildAt(i);
					v.setBackgroundColor(Color.parseColor("#fafafa"));
				}
				for (int i = 0; i < contactListViewUnSync.getChildCount(); i++) {
					View v = contactListViewUnSync.getChildAt(i);
					v.setBackgroundColor(Color.parseColor("#fafafa"));
				}

			}
		}

		@Override
		public void onScroll(AbsListView view, int firstVisibleItem,
							 int visibleItemCount, int totalItemCount) {
			// TODO Auto-generated method stub
			if (window != null) {
				window.dismiss();
			}

		}
	};
	private static final int _7Jours = Menu.FIRST;
	private static final int _30Jours = Menu.FIRST + 1;
	private static final int _60Jours = Menu.FIRST + 2;
	private static final int AllContacts = Menu.FIRST + 3;

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		super.onCreateOptionsMenu(menu, inflater);

		menu.add(0, _7Jours, 0, NavigationActivity.getContext().getResources().getString(R.string.seven_day)).setIcon(android.R.drawable.ic_menu_preferences);
		menu.add(0, _30Jours, 0, NavigationActivity.getContext().getResources().getString(R.string.thirty_day)).setIcon(android.R.drawable.ic_menu_info_details);
		menu.add(0, _60Jours, 0, NavigationActivity.getContext().getResources().getString(R.string.sixty_day)).setIcon(android.R.drawable.ic_menu_add);
		menu.add(0, AllContacts, 0, NavigationActivity.getContext().getResources().getString(R.string.allContacts)).setIcon(android.R.drawable.ic_menu_recent_history);

	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		System.gc();
		notifFragment = null;
		parentScrollView.scrollTo(0, 0);
		switch (item.getItemId()) {
			case _7Jours: {
				Calendar cal = Calendar.getInstance();
				Date today = new Date();
				cal.setTime(today);
				cal.add(Calendar.DAY_OF_YEAR, -7);

				SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");


				referenceDate = formatter.format(cal.getTime());

				Log.d(" calendrier ", " today " + today + " ref date " + referenceDate);

				adapterSync = null;
			}
			break;
			case _30Jours: {
				Calendar cal = Calendar.getInstance();
				Date today = new Date();
				cal.setTime(today);
				cal.add(Calendar.DAY_OF_YEAR, -30);

				SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");


				referenceDate = formatter.format(cal.getTime());

				Log.d(" calendrier ", " today " + today + " ref date " + referenceDate);

				adapterSync = null;
			}
			break;
			case _60Jours: {
				Calendar cal = Calendar.getInstance();
				Date today = new Date();
				cal.setTime(today);
				cal.add(Calendar.MONTH, -2);

				SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");


				referenceDate = formatter.format(cal.getTime());

				Log.d(" calendrier ", " today " + today + " ref date " + referenceDate);
				adapterSync = null;
			}
			break;
			case AllContacts: {
				Calendar cal = Calendar.getInstance();
				Date today = new Date();
				cal.setTime(today);

				cal.add(Calendar.YEAR, -1);
				SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");


				referenceDate = formatter.format(cal.getTime());


				Log.d(" calendrier ", " today " + today + " ref date " + referenceDate);
				adapterSync = null;

			}
		}


		organizedContactListPart = new LinkedList<JSONObject>();
		organizedContactPart = new HashMap<String, JSONObject>();
//		adapterSync.clear();
//		adapterSync.notifyDataSetChanged();

		data_unsync_part = new LinkedList<ContactItem>();
		data_sync_part = new LinkedList<ContactItem>();
		new LoadContactProgressiv().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
		//new prepareAdapters(true).execute() ;


		return super.onOptionsItemSelected(item);
	}


	@Override
	public void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		System.gc();
		if (window != null) {
			window.dismiss();
			window = null;
		}
		if (responseChooserWindow != null) {
			responseChooserWindow.dismiss();
			responseChooserWindow = null;
		}


	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setHasOptionsMenu(true);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		// TODO Auto-generated method  stub
		System.gc();
		Calendar cal = Calendar.getInstance();
		Date today = new Date();
		cal.setTime(today);
		cal.add(Calendar.DAY_OF_YEAR, -30);
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		referenceDate = formatter.format(cal.getTime());

		Log.d(" calendrier ", " oncreateView today " + today + " ref date " + referenceDate);
		BiptagUserManger.saveUserData("add_files_result_edit", "", NavigationActivity.getContext());
		if (fragmentView == null) {
			allFormsList = null;
			logs = null;

			Object agent_id_object = BiptagUserManger.getUserValue("AGENT_ID", "int", NavigationActivity.getContext());
			agent_id = Integer.parseInt(String.valueOf(agent_id_object));

			BTLogDataSource dataSource = new BTLogDataSource(NavigationActivity.getContext());

			logs = dataSource.getAllLogs(agent_id);

			fragmentView = inflater.inflate(R.layout.activity_contacts, null);
			BiptagUserManger.clearField(NavigationActivity.context,"ScanOCR_Edit");
			BiptagUserManger.clearField(NavigationActivity.context,"EditscanOCR_Edit");
			WindowManager wm = (WindowManager) NavigationActivity.getContext().getSystemService(Context.WINDOW_SERVICE);
			Display display = wm.getDefaultDisplay();
			Point point = new Point();
			display.getSize(point);
			screenWidth = point.x;
			screenHeight = point.y;

			searchField = (EditText) fragmentView.findViewById(R.id.contact_list_seachbox);

			searchField.addTextChangedListener(new TextWatcher() {
				String originalSubject;


				@Override
				public void onTextChanged(CharSequence s, int start, int before, int count) {
					// TODO Auto-generated method stub
					if (searchField.getText().toString().length() != 0) {
						if (searchField.getText().toString().charAt(searchField.getText().toString().length() - 1) == '\n') {
							searchField.setText(originalSubject);
						}
					}

				}

				@Override
				public void beforeTextChanged(CharSequence s, int start, int count,
											  int after) {
					// TODO Auto-generated method stub
					originalSubject = searchField.getText().toString();
					if (originalSubject.length() != 0) {
						originalSubject = originalSubject.replaceAll("\\n", "");
					}

				}

				@Override
				public void afterTextChanged(Editable s) {
					// TODO Auto-generated method stub

					originalSubject = searchField.getText().toString();
					searchField.setSelection(searchField.getText().toString().length());

				}
			});

			fixLayout(fragmentView);
			organizedContactList = new LinkedList<JSONObject>();
			organizedContactListPart = new LinkedList<JSONObject>();
			organizedContact = new HashMap<String, JSONObject>();
			organizedContactPart = new HashMap<String, JSONObject>();
			contactlistView = (ListView) fragmentView.findViewById(R.id.contactlistView);
			parentScrollView = (ScrollView) fragmentView.findViewById(R.id.contact_parent_scroll_view);
			contactListViewUnSync = (ListView) fragmentView.findViewById(R.id.contactlistViewUnSync);


			fragmentView.setOnTouchListener(new OnTouchListener() {

				@Override
				public boolean onTouch(View v, MotionEvent event) {
					// TODO Auto-generated method stub
					if (window != null) {
						window.dismiss();
					}

					if (responseChooserWindow != null) {
						responseChooserWindow.dismiss();
					}
					return false;
				}
			});
			parentScrollView.getViewTreeObserver().addOnScrollChangedListener(generalScrollListener);
			contactlistView.setOnScrollListener(scrollListener);
			contactListViewUnSync.setOnScrollListener(scrollListener);
			contactlistView.setOnItemClickListener(new OnItemClickListener() {


				@Override
				public void onItemClick(AdapterView<?> parent, View view,
										int position, long id) {
					// TODO Auto-generated method stub

					if (responseChooserWindow != null) {
						responseChooserWindow.dismiss();
					}

					for (int i = 0; i < contactlistView.getChildCount(); i++) {
						View v = contactlistView.getChildAt(i);
						v.setBackgroundColor(Color.parseColor("#fafafa"));
					}
					for (int i = 0; i < contactListViewUnSync.getChildCount(); i++) {
						View v = contactListViewUnSync.getChildAt(i);
						v.setBackgroundColor(Color.parseColor("#fafafa"));
					}
					view.setBackgroundColor(Color.parseColor("#18a9e7"));

					LayoutInflater inflater = LayoutInflater.from(NavigationActivity.getContext());
					final View responseChooser = inflater.inflate(R.layout.popup_actionchooser, null);
					if (window != null) {
						window.dismiss();
					}
					id = adapterSync.getContactItem(position).contactId;
					Log.d("contact id", "contact id " + id);
					window = new PopupWindow(responseChooser, (int) NavigationActivity.getContext().getResources().getDimension(R.dimen.popup_contact_action_chooser), LayoutParams.WRAP_CONTENT);

					String agent_id = String.valueOf(BiptagUserManger.getUserValue("AGENT_ID", "int", NavigationActivity.getContext()));

					ContactItem itemClicked = adapterSync.getContactItem(position);
								/*
								BTLogDataSource dataSource = new BTLogDataSource(NavigationActivity.getContext()) ;

								logs= dataSource.findLogsFormContact(Integer.parseInt(agent_id) , itemClicked.contactId) ;

								*/
					List<BTLog> contactLog = new LinkedList<BTLog>();
					if (logs != null) {
						for (int i = 0; i < logs.size(); i++) {
							if (logs.get(i).getFl_contact_id() == itemClicked.contactId) {
								contactLog.add(logs.get(i));
							}
						}

					}


					String[] liste_de_choix = new String[]{
							NavigationActivity.getContext().getResources().getString(R.string.send_email),
							NavigationActivity.getContext().getResources().getString(R.string.details_contact),
							NavigationActivity.getContext().getResources().getString(R.string.notif)};

					if (contactLog.size() == 0) {
						liste_de_choix = new String[]{
								NavigationActivity.getContext().getResources().getString(R.string.send_email),
								NavigationActivity.getContext().getResources().getString(R.string.details_contact)};
					}
					final int index = position;
					ListView liste = (ListView) responseChooser.findViewById(R.id.choise_liste);
					final ArrayAdapter<String> adapter1 = new ArrayAdapter<String>(NavigationActivity.getContext(), android.R.layout.simple_list_item_1, liste_de_choix) {

						@Override
						public View getView(final int position, View convertView,
											ViewGroup parent) {
							View view = super.getView(position, convertView, parent);

							TextView textView = (TextView) view.findViewById(android.R.id.text1);

						            /*YOUR CHOICE OF COLOR*/
							textView.setTextColor(Color.GRAY);
							//	ContactList(adapterSync);
							//	Log.d(" contact ", " contact list view 3 " + ContactList(adapterSync).getCount());


							view.setOnClickListener(new View.OnClickListener() {

								@Override
								public void onClick(View v) {

									// TODO Auto-generated method stub
									if (position == 1) {

										window.dismiss();
										ContactItem itemClicked = adapterSync.getContactItem(index);
										JSONObject object = new JSONObject();
										try {
											object.put("CONTACT_FORM_ID", itemClicked.form_id);
											object.put("CONTACT_RESPONSE", itemClicked.contactRespone);
											object.put("CONTACT_LOCAL_ID", itemClicked.contact_local_id);


											String agent_id_string = String.valueOf(BiptagUserManger.getUserValue("AGENT_ID", "int", NavigationActivity.getContext()));
											int agent_id = Integer.parseInt(agent_id_string);
											addResponseChooser(itemClicked.contactId, 1, agent_id, itemClicked);


										} catch (JSONException e) {
											// TODO: handle exception
											Log.d("contact detailt exception ", "contact detail exception " + e.getMessage());
										}


									} else if (position == 0) {

										ContactItem itemClicked = adapterSync.getContactItem(index);
										EditFollowUpFragement followUpFragement = new EditFollowUpFragement(2, itemClicked.contact_email, itemClicked.contactId, 2);
										followUpFragement.fragement = null;
										JSONObject data = new JSONObject();
										try {
											data.put("mode", 2);
											data.put("stack", 2);
											data.put("email", itemClicked.contact_email);
											data.put("id", itemClicked.contactId);
										} catch (JSONException e) {
											// TODO: handle exception
										}
										//	Log.d("", msg)

										BiptagNavigationManager.pushFragments(followUpFragement, true, "send_email", NavigationActivity.getContext(), data.toString(), 2);

										window.dismiss();
									} else if (position == 2) {
										ContactItem itemClicked = adapterSync.getContactItem(index);

										notifFragment = new NotificationFragment();

										JSONObject dataJson = new JSONObject();

										try {
											dataJson.put("contact_id", itemClicked.contactId);
											dataJson.put("contact_name", itemClicked.contact_name);
											dataJson.put("contact_company", "");
											dataJson.put("contact_fonction", "");
										} catch (JSONException e) {
											// TODO: handle exception
										}


										BiptagNavigationManager.pushFragments(notifFragment, true, "notification", NavigationActivity.getContext(), dataJson.toString(), 2);
										window.dismiss();
									}


								}
							});

							return view;
						}
					};
					liste.setAdapter(adapter1);


					int[] location = new int[2];
					view.getLocationOnScreen(location);

					//Initialize the Point with x, and y positions
					final Point p = new Point();
					p.x = location[0];
					p.y = location[1];


					final int width = screenWidth;
					if (window != null) {
						window.dismiss();
					}

					liste.setFocusable(false);

					fragmentView.post(new Runnable() {

						@Override
						public void run() {
							// TODO Auto-generated method stub
							window.showAtLocation(responseChooser, Gravity.NO_GRAVITY, width - (int) NavigationActivity.getContext().getResources().getDimension(R.dimen.popup_contact_action_chooser) - (int) NavigationActivity.getContext().getResources().getDimension(R.dimen.popup_contact_action_chooser_marging), p.y);
							window.setBackgroundDrawable(new BitmapDrawable());
							window.isFocusable();
							window.setTouchable(true);
							window.setOutsideTouchable(true);


						}
					});

				}

			});

			contactListViewUnSync.setOnItemClickListener(new OnItemClickListener() {


				@Override
				public void onItemClick(AdapterView<?> parent, View view,
										int position, long id) {
					// TODO Auto-generated method stub
					if (responseChooserWindow != null) {
						responseChooserWindow.dismiss();
					}
					for (int i = 0; i < contactlistView.getChildCount(); i++) {
						View v = contactlistView.getChildAt(i);
						v.setBackgroundColor(Color.parseColor("#fafafa"));
					}
					for (int i = 0; i < contactListViewUnSync.getChildCount(); i++) {
						View v = contactListViewUnSync.getChildAt(i);
						v.setBackgroundColor(Color.parseColor("#fafafa"));
					}
					if (window != null) {
						window.dismiss();
					}
					final View selectedCell = view;
					view.setBackgroundColor(Color.parseColor("#18a9e7"));

					LayoutInflater inflater = LayoutInflater.from(NavigationActivity.getContext());
					final View responseChooser = inflater.inflate(R.layout.popup_actionchooser, null);
					window = new PopupWindow(responseChooser, (int) NavigationActivity.getContext().getResources().getDimension(R.dimen.popup_contact_action_chooser), LayoutParams.WRAP_CONTENT);

					String[] liste_de_choix = {NavigationActivity.getContext().getResources().getString(R.string.send_email), NavigationActivity.getContext().getResources().getString(R.string.details_contact)};
					ListView liste = (ListView) responseChooser.findViewById(R.id.choise_liste);
					final int index = position;
					final ArrayAdapter<String> adapter1 = new ArrayAdapter<String>(NavigationActivity.getContext(), android.R.layout.simple_list_item_1, liste_de_choix) {

						@Override
						public View getView(final int position, View convertView,
											ViewGroup parent) {
							View view = super.getView(position, convertView, parent);

							TextView textView = (TextView) view.findViewById(android.R.id.text1);

						            /*YOUR CHOICE OF COLOR*/
							textView.setTextColor(Color.GRAY);

							view.setOnClickListener(new View.OnClickListener() {

								@Override
								public void onClick(View v) {
									// TODO Auto-generated method stub

									// TODO Auto-generated method stub
									if (position == 1) {

										window.dismiss();
										ContactItem itemClicked = adapterUnSync.getContactItem(index);
										JSONObject object = new JSONObject();
										try {

											object.put("CONTACT_FORM_ID", itemClicked.form_id);
											object.put("CONTACT_RESPONSE", itemClicked.contactRespone);
											object.put("CONTACT_LOCAL_ID", itemClicked.contact_local_id);
											object.put("CONTACT_ID", itemClicked.contactId);
											object.put("FR_ID", itemClicked.fr_id);
											EditFormStep1 editFormStep1 = new EditFormStep1();
											EditFormStep1.fragmentView = null;
											if (itemClicked.contactId == 0) {
												//BiptagNavigationManager.pushFragments(editFormStep1, true, "Edit Contact", NavigationActivity.getContext(), object.toString(), 2);
												addResponseChooser(itemClicked.contact_local_id, 0, agent_id, itemClicked);

											} else {


												String agent_id_string = String.valueOf(BiptagUserManger.getUserValue("AGENT_ID", "int", NavigationActivity.getContext()));
												int agent_id = Integer.parseInt(agent_id_string);
												addResponseChooser(itemClicked.contactId, 1, agent_id, itemClicked);


											}

										} catch (JSONException e) {
											// TODO: handle exception
											Log.d("contact detailt exception ", "contact detail exception " + e.getMessage());
										}


									} else if (position == 0) {

										ContactItem itemClicked = adapterUnSync.getContactItem(index);

										EditFollowUpFragement followUpFragement = new EditFollowUpFragement(2, itemClicked.contact_email, itemClicked.contactId, 2);
										followUpFragement.fragement = null;
										JSONObject data = new JSONObject();
										try {
											data.put("mode", 2);
											data.put("stack", 2);
											data.put("email", itemClicked.contact_email);
											data.put("id", itemClicked.contactId);
										} catch (JSONException e) {
											// TODO: handle exception
										}
										//Log.d("", msg)

										BiptagNavigationManager.pushFragments(followUpFragement, true, "send_email", NavigationActivity.getContext(), data.toString(), 2);


										window.dismiss();
									} else if (position == 2) {

										window.dismiss();
									}
								}


							});

							return view;
						}
					};
					liste.setAdapter(adapter1);


					ContactItem itemClicked = adapterUnSync.getContactItem(index);
					int[] location = new int[2];
					view.getLocationOnScreen(location);

					//Initialize the Point with x, and y positions
					final Point p = new Point();
					p.x = location[0];
					p.y = location[1];

					final int width = screenWidth;


					fragmentView.post(new Runnable() {

						@Override
						public void run() {
							// TODO Auto-generated method stub
							window.showAtLocation(responseChooser, Gravity.NO_GRAVITY, width - (int) NavigationActivity.getContext().getResources().getDimension(R.dimen.popup_contact_action_chooser) - (int) NavigationActivity.getContext().getResources().getDimension(R.dimen.popup_contact_action_chooser_marging), p.y);

						}
					});


					window.setTouchable(true);
					window.setOutsideTouchable(true);

							/*
							liste.setOnItemClickListener(new OnItemClickListener() {

								@Override
								public void onItemClick(AdapterView<?> parent,
										View view, int position, long id) {
									// TODO Auto-generated method stub
									if (position == 1){

									window.dismiss();
									AlertDialog.Builder builder= new AlertDialog.Builder(NavigationActivity.getContext()) ;
									ContactItem itemClicked = adapterUnSync.getContactItem(index) ;
							   		JSONObject object = new JSONObject() ;
									try {
										object.put("CONTACT_FORM_ID", itemClicked.form_id) ;
										object.put("CONTACT_RESPONSE", itemClicked.contactRespone) ;
										object.put("CONTACT_LOCAL_ID", itemClicked.contact_local_id) ;
										object.put("CONTACT_ID", itemClicked.contactId) ;
										object.put("FR_ID", itemClicked.fr_id) ;
										EditFormStep1 editFormStep1 = new EditFormStep1() ;

										if(itemClicked.contactId== 0){
										BiptagNavigationManager.pushFragments(editFormStep1, true, "Edit Contact", NavigationActivity.getContext(), object.toString(), 2) ;

										}else{


										String agent_id_string= String.valueOf(BiptagUserManger.getUserValue("AGENT_ID", "int", NavigationActivity.getContext())) ;
										int agent_id = Integer.parseInt(agent_id_string) ;
										Log.d("contact item ", "contact item id "+itemClicked.contactId) ;
										addResponseChooser(itemClicked.contactId, 1, agent_id , itemClicked) ;


										}

									} catch (JSONException e) {
										// TODO: handle exception
										Log.d("contact detailt exception ", "contact detail exception "+e.getMessage()) ;
									}


									}else if (position == 0) {

										Toast.makeText(NavigationActivity.getContext(), "Email sending", Toast.LENGTH_LONG).show() ;
										AlertDialog.Builder builder= new AlertDialog.Builder(NavigationActivity.getContext()) ;
										ContactItem itemClicked = adapterUnSync.getContactItem(index) ;
										builder.setMessage("Email "+itemClicked.contact_email) ;
										builder.show() ;
										window.dismiss();
									}else if (position == 2) {

										window.dismiss();
									}
								}
							});

							*/
				}


			});


			Button syncButton = (Button) fragmentView.findViewById(R.id.contact_list_sync_btn);
			syncButton.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub

					Log.d("contact fragemnt", "contact fragment  sync is synchronizing " + isSynchronizing);
					if (!isSynchronizing) {
						isSynchronizing = true;
						BiptagUserManger.saveUserData("IS_SYNC", "1", NavigationActivity.getContext());


						if (window != null) {
							window.dismiss();
						}
							synchronisationDialog = new ProgressDialog(NavigationActivity.getContext());
							synchronisationDialog.setTitle(NavigationActivity.getContext().getResources().getString(R.string.sync_title));
							synchronisationDialog.setMessage(NavigationActivity.getContext().getResources().getString(R.string.sync_text));
							synchronisationDialog.setProgress(0);
							synchronisationDialog.setCancelable(false);
							synchronisationDialog.setCanceledOnTouchOutside(false);

							BTContactDataSource btContactDataSource = new BTContactDataSource(NavigationActivity.getContext());
							String agent_id = String.valueOf(BiptagUserManger.getUserValue("AGENT_ID", "int", NavigationActivity.getContext()));
							final String ba_id = String.valueOf(BiptagUserManger.getUserValue("BA_ID", "int", NavigationActivity.getContext()));
							List<BTContact> list = btContactDataSource.getAllContacts(Integer.parseInt(agent_id), 0);
							Log.d(" ba id fragment ", " ba id cotact fragment " + ba_id);
							synchronisationDialog.setMax(3 + list.size() - 1);
							synchronisationDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
							synchronisationDialog.show();
							new Thread(new Runnable() {

								@Override
								public void run() {
									// TODO Auto-generated method stub

									DownloadForms downloadForms = new DownloadForms(NavigationActivity.getContext().getApplicationContext());
									downloadForms.response = ContactFragement.this;
									downloadForms.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, null);
									TemplateUploader uploader = new TemplateUploader(NavigationActivity.getContext().getApplicationContext(), Integer.parseInt(ba_id));
									uploader.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, null);
									DownloadFollowupHashcodes downloadFollowupHashcodes = new DownloadFollowupHashcodes(getActivity());
									downloadFollowupHashcodes.execute();
								}
							}).start();


					}
					//initilizeContactTables();
					adapterSync = new ContactAdapter(NavigationActivity.getContext(), R.layout.contact_row, data_sync, logs);
					contactlistView.setAdapter(adapterSync);
					setListViewHeightBasedOnChildren(contactlistView);
					Log.d(" contact ", " syncronising " + adapterSync.getCount());
					adapterSync.notifyDataSetChanged();
					Log.d(" contact ", " syncronising finish" + adapterSync.getCount());

				}
			});

			searchField.setOnTouchListener(new OnTouchListener() {

				@Override
				public boolean onTouch(View v, MotionEvent event) {
					// TODO Auto-generated method stub
					if (window != null) {
						window.dismiss();
					}
					return false;
				}
			});

			searchField.addTextChangedListener(new TextWatcher() {
				int charLengh = 0;

				@Override
				public void onTextChanged(CharSequence s, int start, int before, int count) {

					//new AsynchronousSearch().execute() ;
					if (s.length() < charLengh) {
						adapterSync.mustFiltreFromCopy = true;
						seq = s;
					}
					adapterSync.getFilter().filter(s);
					adapterUnSync.getFilter().filter(s);

					//	adapterSync =new ContactAdapter(NavigationActivity.getContext(), R.layout.contact_row,);
					contactlistView.setAdapter(new ContactAdapter(NavigationActivity.getContext(), R.layout.contact_row, adapterSync.items, logs));
					setListViewHeightBasedOnChildren(contactListViewUnSync);
					setListViewHeightBasedOnChildren(contactlistView);

				}

				@Override
				public void beforeTextChanged(CharSequence s, int start, int count,
											  int after) {
					// TODO Auto-generated method stub


				}

				@Override
				public void afterTextChanged(Editable s) {
					// TODO Auto-generated method stub
					charLengh = searchField.getText().toString().length();
				}
			});

			if (allFormsList == null) {
				BTFormDataSource formDataSource = new BTFormDataSource(NavigationActivity.getContext());
				allFormsList = formDataSource.getAllForms(agent_id);
			}
			data_unsync_part = new LinkedList<ContactItem>();
			data_sync_part = new LinkedList<ContactItem>();
			new LoadContactProgressiv().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
			new prepareAdapters(true).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

		}

		return fragmentView;
	}


	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		System.gc();
		notifFragment = null ;

		if (adapterSync!=null) {
			adapterSync.notifyDataSetChanged();
			adapterUnSync.notifyDataSetChanged();
		}
		if (adapterUnSync!=null) {
			adapterUnSync.notifyDataSetChanged();
			adapterSync.notifyDataSetChanged();
		}

		parentScrollView.scrollTo(0, 0) ;
		BiptagUserManger.clearField(NavigationActivity.getContext(), "images_for_user_edit") ;
		BiptagUserManger.clearField(NavigationActivity.getContext(), "ocrResultEdit") ;
		EditFormStep1.fragmentView= null;
	}

	public  void organizeContactPartial(List<BTContact> contacts) {
		try {
			Log.d(" contacts ", " partial size " + contacts.size());
			for (int i = 0; i < contacts.size()	; i++) {
				BTContact  object = contacts.get(i) ;



				String contact_email = object.getContactEmail() ;
				if(organizedContactPart.get(contact_email)== null){
					String nom = object.getContactName();
					JSONObject contactData = new JSONObject() ;
					contactData.put("CONTACT_NAME", nom)  ;
					contactData.put("CONTACT_MAIL", contact_email) ;
					contactData.put("CONTACT_FORM", "")  ;
					contactData.put("CONTACT_RESPONSE", object.getContactFrResponse()) ;
					contactData.put("CONTACT_ID", object.getContactId()) ;
					contactData.put("CONTACT_FORM_ID", object.getContactFormId());
					contactData.put("CONTACT_FR_ID", object.getContactFrID()) ;
					contactData.put("CONTACT_LOCAL_ID", object.getContact_local_id()) ;
					organizedContactPart.put(contact_email, contactData)  ;
					organizedContactListPart.add(contactData) ;


				}
			}

			Log.d("contact last id", "contact last id " + lastIdSelected) ;
			ContactList(organizedContactList);

		} catch (JSONException e) {
			Log.d("organize contact ", "organize contact exception  "+e.getMessage()) ;
		}

	}

	public  void organizeContact(List<BTContact> contacts) {
		try {

			for (int i = 0; i < contacts.size()	; i++) {
				BTContact  object = contacts.get(i) ;
				Log.d("organise contact " , "orga contact tr "+object.getContactEmail()) ;
				String contact_email = object.getContactEmail() ;
				String nom = object.getContactName();
				JSONObject contactData = new JSONObject() ;
				contactData.put("CONTACT_NAME", nom)  ;
				contactData.put("CONTACT_MAIL", contact_email) ;
				contactData.put("CONTACT_FORM", "")  ;
				contactData.put("CONTACT_RESPONSE", object.getContactFrResponse()) ;
				contactData.put("CONTACT_ID", object.getContactId()) ;
				contactData.put("CONTACT_FORM_ID", object.getContactFormId());
				contactData.put("CONTACT_FR_ID", object.getContactFrID()) ;
				contactData.put("CONTACT_LOCAL_ID", object.getContact_local_id()) ;


				if(organizedContact.get(contact_email)== null){
					Log.d("organise contact " , "orga contact tr added "+object.getContactEmail()) ;
					organizedContact.put(contact_email, contactData)  ;
					organizedContactList.add(contactData) ;
				}else{

					organizedContact.put(contact_email, contactData)  ;
					//organizedContactList.set(i, contactData) ;


				}
			}
			ContactList(organizedContactList);
					Log.d("populating list ", "popultating list organized list size "+organizedContactList.size()) ;
		} catch (JSONException e) {
			Log.d("organize contact ", "organize contact exception  "+e.getMessage()) ;
		}

	}


			/*
			public String getContactName(BTContact contact){
				String name = "" ;

				try {
					JSONObject object =contact.getContactFrResponse() ;
					BTFormDataSource dataSource = new BTFormDataSource(NavigationActivity.getContext()) ;
					dataSource.open() ;
					BTFormulaire  formulaire = dataSource.getFormWithID(agent_id, contact.getContactFormId()) ;
					dataSource.close() ;
					try {
						String firstNameKey = formulaire.getFormStructure().getJSONObject(2).getJSONArray("VALUE").getString(0) ;
						String lastNameKey= formulaire.getFormStructure().getJSONObject(4).getJSONArray("VALUE").getString(0) ;
						String firstName = contact.getContactFrResponse().getString(firstNameKey) ;
						String lastName = contact.getContactFrResponse().getString(lastNameKey) ;
						name = firstName+" "+lastName ;
					} catch (JSONException e) {
						// TODO: handle exception
					}

				} catch (Exception e) {
					// TODO: handle exception
				}

				return name ;
			}*/


	public void  prepareListDataPartial() {
		boolean updated = false ;
		for (int i = 0; i < organizedContactListPart.size(); i++) {

			try {
				if(organizedContactListPart.get(i).getInt("CONTACT_LOCAL_ID")> lastIdSelected  || true){
					String contact_email = organizedContactListPart.get(i).getString("CONTACT_MAIL");
					String contact_name = organizedContactListPart.get(i).getString("CONTACT_NAME") ;

					String contact_name_toshow = "" ;
					if(contact_name.equals(" ")){
						contact_name_toshow= contact_email ;
					}else {
						contact_name_toshow =contact_name ;

					}
				//	Log.d(" contact ", " to show "+ contact_name_toshow);
					ContactItem contact  = new ContactItem( organizedContactListPart.get(i).getString("CONTACT_MAIL"),contact_name_toshow  ,organizedContactListPart.get(i).getJSONObject("CONTACT_RESPONSE"),
							organizedContactListPart.get(i).getString("CONTACT_FORM"),  organizedContactListPart.get(i).getInt("CONTACT_ID"),
							organizedContactListPart.get(i).getInt("CONTACT_FORM_ID"),  organizedContactListPart.get(i).getInt("CONTACT_FR_ID"), true, organizedContactListPart.get(i).getInt("CONTACT_LOCAL_ID"));
					data_sync_part.add(contact) ;

				}

				if(lastIdSelected < organizedContactListPart.get(i).getInt("CONTACT_LOCAL_ID")){
					oldLastIdSelected = lastIdSelected ;
					lastIdSelected =  organizedContactListPart.get(i).getInt("CONTACT_LOCAL_ID") ;
					updated = true ;
				}


			} catch (JSONException e) {
				Log.d("data prepare exception  ", "data prepare exception  ") ;
			}
		}
		if(!updated)
			oldLastIdSelected = lastIdSelected ;

	}


	public void  prepareListData() {
		data_sync = new LinkedList<ContactItem>() ;
		Log.d(" contcts ", " prepare list data "+organizedContactList.size() + " ");
		for (int i = 0; i < organizedContactList.size(); i++) {

			try {

				String contact_email = organizedContactList.get(i).getString("CONTACT_MAIL");
				String contact_name = organizedContact.get(contact_email).getString("CONTACT_NAME") ;  ;
				JSONObject response = organizedContact.get(contact_email).getJSONObject("CONTACT_RESPONSE") ;

				//Log.d(" contact to show ", response.toString());
				contact_name = response.getString("Nom") + " " + response.getString("Prénom");

				String contact_name_toshow = "" ;
				if(contact_name.equals(" ")){
					contact_name_toshow= contact_email ;
				}else {
					contact_name_toshow =contact_name ;
				}
						ContactItem contact  = new ContactItem( organizedContactList.get(i).getString("CONTACT_MAIL"),contact_name_toshow  ,response,
						organizedContactList.get(i).getString("CONTACT_FORM"),  organizedContactList.get(i).getInt("CONTACT_ID"),
						organizedContactList.get(i).getInt("CONTACT_FORM_ID"),  organizedContactList.get(i).getInt("CONTACT_FR_ID"), true, organizedContactList.get(i).getInt("CONTACT_LOCAL_ID"));
				data_sync.add(contact) ;

			} catch (JSONException e) {

				Log.d("data prepare exception  ", "data prepare exception  ") ;
			}


		}
			        /*
			        adapterSync = new ContactAdapter(NavigationActivity.getContext(), R.layout.contact_row, data_sync) ;
				 	contactlistView.setAdapter(adapterSync);
					setListViewHeightBasedOnChildren(contactlistView) ;

				 	adapterUnSync = new ContactAdapter(NavigationActivity.getContext(), R.layout.contact_row, data_unsync) ;
					contactListViewUnSync.setAdapter(adapterUnSync);
					setListViewHeightBasedOnChildren(contactListViewUnSync) ;
					*/



	}
// LZR : 14/04/2016 download contact when synchronising
	public void downloadContact(){


		contactDownloadStarted = true  ;
		Object agent_id_object = BiptagUserManger.getUserValue("AGENT_ID", "int", NavigationActivity.getContext());


		try {
			int agent_id = Integer.parseInt(String.valueOf(agent_id_object)) ;

			BTContactDataSource contactDataSource = new BTContactDataSource(NavigationActivity.getContext()) ;

			List<BTContact> contacts = contactDataSource.getAllContacts(agent_id, 1) ;

			if(contacts.size() == 0){

				DownloadContactWithHashCodes contactWithHashCodes = new DownloadContactWithHashCodes(NavigationActivity.getContext()) ;
				contactWithHashCodes.response = this ;
				if(BiptagUserManger.isOnline(NavigationActivity.getContext()) && !DownloadContactWithHashCodes.isExecuting)
					contactWithHashCodes.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
			}
		} catch (NumberFormatException e) {
			// TODO: handle exception
		}
	}


	public void addResponseChooser(int contact_id , int isSync , int agent_id , ContactItem contactItem) {
		BTContactDataSource dataSource = new BTContactDataSource(NavigationActivity.getContext()) ;
		final  List<BTContact> contactResponse = dataSource.getResponseFormContact(agent_id, isSync,contactItem.contact_email, contact_id) ;

		if(responseChooserWindow != null){
			responseChooserWindow.dismiss()  ;
		}

		if(contactResponse.size()> 1){



			LayoutInflater inflater= LayoutInflater.from(NavigationActivity.getContext()) ;
			final View responseChooser = inflater.inflate(R.layout.popup_responsechooser, null) ;
			responseChooserWindow = new PopupWindow(responseChooser , screenWidth , screenHeight) ;

			LinearLayout response_chooser_black_layout = (LinearLayout)responseChooser.findViewById(R.id.response_chooser_black_layout) ;
			response_chooser_black_layout.setOnTouchListener(new OnTouchListener() {

				@Override
				public boolean onTouch(View v, MotionEvent event) {
					// TODO Auto-generated method stub

					if(event.getAction() ==  MotionEvent.ACTION_DOWN){
						responseChooserWindow.dismiss() ;
					}


					return false;
				}
			}) ;
			responseChooserWindow.setOutsideTouchable(true) ;
			BTFormDataSource formDataSource = new BTFormDataSource(NavigationActivity.getContext()) ;
			String[] editions  = new String[contactResponse.size()] ;
			String[] formNames = new String[contactResponse.size()] ;
			String[] timesStamps = new String[contactResponse.size()] ;
			int[] colors = new int[contactResponse.size()] ;
			for (int i = 0; i < contactResponse.size()	; i++) {

				BTContact contact = contactResponse.get(i) ;
				int form_id = contact.getContactFormId() ;
				BTFormulaire formulaire = formDataSource.getFormWithID(agent_id, form_id) ;
				editions[i] = formulaire.getEventName() ;
				formNames[i] = formulaire.getFormName() ;
				try {

					JSONArray strucutre = formulaire.getFormStructure() ;
					for (int j = 0; j < strucutre.length(); j++) {
						if(strucutre.getJSONObject(j).getString("type").equals("COLOR")){
							String colorHex = strucutre.getJSONObject(j).getJSONArray("VALUE").getString(0) ;
							colors[i] = Color.parseColor(colorHex) ;
						}
					}
				} catch (JSONException e) {
					// TODO: handle exception
					colors[i] = 0 ;
				}
				try {
					JSONObject  object =	contact.getContactFrResponse() ;
					String time = object.getString("BT_TIMESTAMP") ;
					timesStamps[i] = time ;
				} catch (JSONException e) {
					// TODO: handle exception

					timesStamps[i] = "" ;
				}



			}


			ListView response = (ListView) responseChooser.findViewById(R.id.responses_list) ;

			ResponseChooserAdapter adapter = new ResponseChooserAdapter(NavigationActivity.getContext(), android.R.layout.simple_list_item_1, formNames, editions, colors, timesStamps, contactResponse , responseChooserWindow) ;





			response.setAdapter(adapter) ;




			fragmentView.post(new Runnable() {

				@Override
				public void run() {
					// TODO Auto-generated method stub
					responseChooserWindow.showAtLocation(responseChooser, Gravity.CENTER, 0, 0) ;

				}
			})  ;


			Log.d("contact detail exception", "contact detail exception add popup") ;

		}else{

			JSONObject object = new JSONObject() ;
			try {


				//BTContact contact = contactResponse.get(0) ;
				object.put("CONTACT_FORM_ID", contactItem.form_id) ;
				object.put("CONTACT_RESPONSE", contactItem.contactRespone) ;
				object.put("CONTACT_LOCAL_ID", contactItem.contact_local_id) ;
				object.put("CONTACT_ID", contactItem.contactId) ;
				object.put("FR_ID", contactItem.fr_id) ;
				EditFormStep1 editFormStep1 = new EditFormStep1() ;
				EditFormStep1.fragmentView = null ;
				BiptagNavigationManager.pushFragments(editFormStep1, true, "Edit Contact", NavigationActivity.getContext(), object.toString(), 2) ;
			}catch(JSONException exception){

			}catch (Exception e) {
				// TODO: handle exception

				Log.d("add response chooser on response", "add response chooser onre response ex "+e.getMessage()) ;
			}



		}
	}


	public void preInitialiseContactTables(){


		BTContactDataSource dataSource = new BTContactDataSource(NavigationActivity.getContext()) ;
		Object agent_id_object = BiptagUserManger.getUserValue("AGENT_ID", "int", NavigationActivity.getContext());

		int agent_id = Integer.parseInt(String.valueOf(agent_id_object)) ;
		List<BTContact> unSynchronizedContact = dataSource.getF20irstContacts(agent_id , 0) ;
		BTFormDataSource formDataSource = new BTFormDataSource(NavigationActivity.getContext()) ;
		allFormsList= formDataSource.getAllForms(agent_id) ;
		data_unsync = new LinkedList<ContactItem>() ;
		if(unSynchronizedContact.size()!= 0){

			for (int i = 0; i < unSynchronizedContact.size() && i< 20; i++) {
				BTContact unsentContact = unSynchronizedContact.get(i) ;
				//JSONObject unsentContact = unsentContacTJsonArray.getJSONObject(i) ;
				String contact_email = unsentContact.getContactEmail();
				String contact_name = unsentContact.getContactName() ;

				String contact_name_toshow = "" ;
				if(contact_name.equals(" ")){
					contact_name_toshow= contact_email ;
				}else {
					contact_name_toshow =contact_name ;
				}
				ContactItem contact  = new ContactItem( unsentContact.getContactEmail(),contact_name_toshow  ,unsentContact.getContactFrResponse(),
						"",  unsentContact.getContactId(),
						unsentContact.getContactFormId(),  unsentContact.getContactFrID(), false , unsentContact.getContact_local_id());
				data_unsync.add(contact) ;

			}

		}

		savedContact = dataSource.getF20irstContacts(agent_id, 1) ;
		organizeContact(savedContact) ;
		prePrepareListData();




	}

	public void prePrepareListData(){

		listDataHeader = new LinkedList<String>();
		listDataChild = new HashMap<String, List<String>>();
		data_sync = new LinkedList<ContactItem>() ;
		for (int i = 0; i < organizedContactList.size() && i<20; i++) {

			try {

				String contact_email = organizedContactList.get(i).getString("CONTACT_MAIL");
				String contact_name = organizedContactList.get(i).getString("CONTACT_NAME") ;
				JSONObject response = organizedContact.get(contact_email).getJSONObject("CONTACT_RESPONSE") ;

			//	Log.d(" contact to show ", response.toString());
				contact_name = response.getString("Nom") + " " + response.getString("Prénom");

				String contact_name_toshow = "" ;
				if(contact_name.equals(" ")){
					contact_name_toshow= contact_email ;
				}else {
					contact_name_toshow =contact_name ;
				}

				ContactItem contact  = new ContactItem( organizedContactList.get(i).getString("CONTACT_MAIL"),contact_name_toshow  ,organizedContactList.get(i).getJSONObject("CONTACT_RESPONSE"),
						organizedContactList.get(i).getString("CONTACT_FORM"),  organizedContactList.get(i).getInt("CONTACT_ID"),
						organizedContactList.get(i).getInt("CONTACT_FORM_ID"),  organizedContactList.get(i).getInt("CONTACT_FR_ID"), true, organizedContactList.get(i).getInt("CONTACT_LOCAL_ID"));
				data_sync.add(contact) ;

			} catch (JSONException e) {
				Log.d("data prepare exception  ", "data prepare exception  ") ;
			}


		}

		adapterSync = new ContactAdapter(NavigationActivity.getContext(), R.layout.contact_row, data_sync , logs) ;
		contactlistView.setAdapter(adapterSync);
		setListViewHeightBasedOnChildren(contactlistView) ;

		adapterUnSync = new ContactAdapter(NavigationActivity.getContext(), R.layout.contact_row, data_unsync , logs) ;
		contactListViewUnSync.setAdapter(adapterUnSync);
		setListViewHeightBasedOnChildren(contactListViewUnSync) ;





	}

public List<JSONObject> ContactList( List<JSONObject> V){



	return V;
}
	//LZR : 14/04/2016 load a part of contact
	public void partialLoading(){

		Object agent_id_object = BiptagUserManger.getUserValue("AGENT_ID", "int", NavigationActivity.getContext());

		try {
			int agent_id = Integer.parseInt(String.valueOf(agent_id_object)) ;

			BTContactDataSource dataSource = new BTContactDataSource(NavigationActivity.getContext()) ;
			List<BTContact> unSynchronizedContact = dataSource.getAllContactsByDate(agent_id, 0, referenceDate) ;
			Log.d(" contact " , " contact size " + unSynchronizedContact.size());
			data_unsync_part = new LinkedList<ContactItem>() ;

			if(unSynchronizedContact.size()!= 0){

				for (int i = 0; i < unSynchronizedContact.size(); i++) {


					BTContact unsentContact = unSynchronizedContact.get(i) ;
					//JSONObject unsentContact = unsentContacTJsonArray.getJSONObject(i) ;
					String contact_email = unsentContact.getContactEmail();
					String contact_name = unsentContact.getContactName() ;

					String contact_name_toshow = "" ;
					if(contact_name.equals(" ")){
						contact_name_toshow= contact_email ;
					}else {
						contact_name_toshow =contact_name ;
					}
					ContactItem contact  = new ContactItem( unsentContact.getContactEmail(),contact_name_toshow  ,unsentContact.getContactFrResponse(),
							"",  unsentContact.getContactId(),
							unsentContact.getContactFormId(),  unsentContact.getContactFrID(), false , unsentContact.getContact_local_id());
					data_unsync_part.add(contact) ;

				}

			}

			savedContact = dataSource.getAllContactsByDate(agent_id, 1, referenceDate) ;
			Log.d(" contact " , " contact size " + savedContact.size());

			organizeContactPartial(savedContact) ;
			prepareListDataPartial();
			if (savedContact.size()==0){
				dialog.dismiss();
			}
		} catch (NumberFormatException e) {
			// TODO: handle exception
		}


	}



	public void  initilizeContactTables(){

		Object agent_id_object = BiptagUserManger.getUserValue("AGENT_ID", "int", NavigationActivity.getContext());

		try {
			int agent_id = Integer.parseInt(String.valueOf(agent_id_object)) ;

			BTContactDataSource dataSource = new BTContactDataSource(NavigationActivity.getContext()) ;
			List<BTContact> unSynchronizedContact = dataSource.getAllContactsByDate(agent_id, 0, referenceDate ) ;
			BTFormDataSource formDataSource = new BTFormDataSource(NavigationActivity.getContext()) ;
			allFormsList= formDataSource.getAllForms(agent_id) ;
			data_unsync = new LinkedList<ContactItem>() ;
			if(unSynchronizedContact.size()!= 0){

				for (int i = 0; i < unSynchronizedContact.size(); i++) {


					BTContact unsentContact = unSynchronizedContact.get(i) ;
					//JSONObject unsentContact = unsentContacTJsonArray.getJSONObject(i) ;
					String contact_email = unsentContact.getContactEmail();
					String contact_name = unsentContact.getContactName() ;

					String contact_name_toshow = "" ;
					if(contact_name.equals(" ")){
						contact_name_toshow= contact_email ;
					}else {
						contact_name_toshow =contact_name ;
					}
					ContactItem contact  = new ContactItem( unsentContact.getContactEmail(),contact_name_toshow  ,unsentContact.getContactFrResponse(),
							"",  unsentContact.getContactId(),
							unsentContact.getContactFormId(),  unsentContact.getContactFrID(), false , unsentContact.getContact_local_id());
					data_unsync.add(contact) ;

				}

			}

			savedContact = dataSource.getAllContactsByDate(agent_id, 1, referenceDate) ;
			organizeContact(savedContact) ;
			prepareListData();
		} catch (NumberFormatException e) {
			// TODO: handle exception
		}


	}
	public void fixLayout(View container) {


		WindowManager wm = (WindowManager) NavigationActivity.getContext().getSystemService(Context.WINDOW_SERVICE);
		Display display = wm.getDefaultDisplay();
		Point point = new Point();
		display.getSize(point);
		screenWidth = point.x;
		screenHeight = point.y;


		LinearLayout searchbox_container_listcontact = (LinearLayout) container.findViewById(R.id.searchbox_container_listcontact);
		GradientDrawable border = new GradientDrawable();
		border.setColor(0xFFFFFFFF); //white background
		border.setStroke(3, 0xFFE6E6EA); //black border with full opacity
		if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
			searchbox_container_listcontact.setBackgroundDrawable(border);
		} else {
			searchbox_container_listcontact.setBackground(border);
		}

		LinearLayout.LayoutParams params = (android.widget.LinearLayout.LayoutParams) searchbox_container_listcontact.getLayoutParams();
		params.setMargins(0, screenHeight / 100, 0, screenHeight / 100);
		searchbox_container_listcontact.setLayoutParams(params);


	}

		@Override
	public void synchronisationStepUp() {
		// TODO Auto-generated method stub
		if(synchronisationDialog != null)
			synchronisationDialog.setProgress(synchronisationDialog.getProgress()+ 1 ) ;
	}

	@Override
	public void processFinish(String result) {
		Log.d("contact ", " process finish " + result);
		// TODO Auto-generated method stub
		if(synchronisationDialog != null)
			synchronisationDialog.setProgress(synchronisationDialog.getProgress()+1) ;

		if(result.equals("forms")){


			allFormsList = null ;
			ConnectedhomeFragment.setIsFormAdded(false) ;
			DownloadFiles downloadFiles = new DownloadFiles(NavigationActivity.getContext()) ;
			downloadFiles.response =ContactFragement.this ;
			if(BiptagUserManger.isOnline(NavigationActivity.getContext())){

				downloadFiles.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR	, null) ; ;
			}
			else
				processFailed() ;

		}


		if(result.equals("docs")){
			// upload contact
			UploadContacts uploadContacts = new  UploadContacts(NavigationActivity.getContext()) ;
			uploadContacts.response= ContactFragement.this  ;
			if(BiptagUserManger.isOnline(NavigationActivity.getContext()))
				uploadContacts.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR	, null)  ;
			else
				processFailed() ;
		}

		if(result.equals("uploaded")){
			//download contacts



			//new prepareAdapters(true).execute() ;

			DownloadContactWithHashCodes contactWithHashCodes = new DownloadContactWithHashCodes(NavigationActivity.getContext()) ;
			contactWithHashCodes.response = ContactFragement.this ;
			if(BiptagUserManger.isOnline(NavigationActivity.getContext()) && !DownloadContactWithHashCodes.isExecuting )
				contactWithHashCodes.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR	, null) ;
			else
				processFailed() ;
		}

		if(result.equals("ids_ready")){
			// list of ids ready to download



			String ids_to_download  =	(String) BiptagUserManger.getUserValue("ids_to_download", "string", NavigationActivity.getContext()) ;
			try {
				if(ids_to_download!= null){
					JSONArray ids = new JSONArray(ids_to_download) ;
					if(synchronisationDialog!= null)
						synchronisationDialog.setMax(synchronisationDialog.getMax() + ids.length()) ;
					DownloadContactsWithIds downloadContactsWithIds = new DownloadContactsWithIds(NavigationActivity.getContext()) ;
					downloadContactsWithIds.response= ContactFragement.this ;
					if(BiptagUserManger.isOnline(NavigationActivity.getContext()))
						downloadContactsWithIds.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR	, null) ;
					else
						processFailed() ;

				}
			} catch (JSONException e) {
				// TODO: handle exception
			}




		}

		if(result.equals("contacts_done")){
			DownloadLogs downloadLogs = new DownloadLogs(NavigationActivity.getContext(), ContactFragement.this) ;
			if(BiptagUserManger.isOnline(NavigationActivity.getContext()))
				downloadLogs.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR	, null) ;
			else
				processFailed() ;


			contactDownloaded = true ;
			adapterUnSync.clear() ;
			new prepareAdapters(true).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR	, null) ;


			if(synchronisationDialog != null)
				synchronisationDialog.dismiss() ;
			BiptagUserManger.saveUserData("IS_SYNC", "", NavigationActivity.getContext()) ;
			isSynchronizing = false ;



		}


		if(result.equals("finish")){

			BTLogDataSource dataSource = new BTLogDataSource(NavigationActivity.getContext()) ;
			logs= dataSource.getAllLogs(agent_id ) ;
			isSynchronizing = false ;
			new prepareAdapters(true).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR	, null) ;  ;
			if(synchronisationDialog != null)
				synchronisationDialog.dismiss() ;
			BiptagUserManger.saveUserData("IS_SYNC", "", NavigationActivity.getContext()) ;

			DownloadFollowupHashcodes downloadFollowupHashcodes = new DownloadFollowupHashcodes(NavigationActivity.getContext()) ;
			if(BiptagUserManger.isOnline(NavigationActivity.getContext()))
				downloadFollowupHashcodes.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR	, null) ;
			else
				processFailed() ;

		}



	}


	@Override
	public void processFailed() {
		// TODO Auto-generated method stub

		isSynchronizing= false ;
		BiptagUserManger.saveUserData("IS_SYNC", "", NavigationActivity.getContext()) ;
		if(dialog!= null)
			dialog.dismiss() ;
		if(synchronisationDialog !=null)
			synchronisationDialog.dismiss() ;
		AlertDialog.Builder builder= new AlertDialog.Builder(NavigationActivity.getContext()) ;
		builder.setTitle(NavigationActivity.getContext().getResources().getString(R.string.error_synch_title))  ;
		builder.setMessage(NavigationActivity.getContext().getResources().getString(R.string.error_synch_msg)) ;
		builder.setPositiveButton("Ok", new OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				dialog.dismiss() ;
			}
		}) ;

		builder.show()  ;


	}

	public static void setListViewHeightBasedOnChildren(ListView listView) {
		ListAdapter listAdapter = listView.getAdapter();
		if (listAdapter == null)
			return;

		int desiredWidth = MeasureSpec.makeMeasureSpec(listView.getWidth(), MeasureSpec.UNSPECIFIED);
		int totalHeight = 0;
		View view = null;
		int list=0;
		Log.d("filter count " , "f c "+listAdapter.getCount() + " list " + adapterSync.filterCount) ;
		//int length = li
		for (int i = 0; i < listAdapter.getCount(); i++) {
			view = listAdapter.getView(i, view, listView);
			if (i == 0)
				view.setLayoutParams(new ViewGroup.LayoutParams(desiredWidth, LayoutParams.WRAP_CONTENT));

			view.measure(desiredWidth, MeasureSpec.UNSPECIFIED);
			totalHeight += screenHeight/18 ;   //view.getMeasuredHeight();
		}
		ViewGroup.LayoutParams params = listView.getLayoutParams();
		params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1))  ;
		listView.setLayoutParams(params);
		listView.requestLayout();
	}







	class prepareAdapters extends AsyncTask<String, String, String>{


		boolean doReload ;
		public prepareAdapters() {
			// TODO Auto-generated constructor stub
		}

		public prepareAdapters(boolean doReload) {
			// TODO Auto-generated constructor stub
			this.doReload = doReload ;
		}





		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();


		}


		@Override
		protected String doInBackground(String... params) {
			// TODO Auto-generated method stub


			initilizeContactTables() ;

			Context context= NavigationActivity.getContext() ;
			/*if(BiptagUserManger.isOnline(context)){

									//String contact = String.valueOf(BiptagUserManger.getUserValue("contacts", "string", NavigationActivity.getContext().getApplicationContext())) ;
									if(savedContact.size() == 0 ){

										((Activity) NavigationActivity.getContext()).runOnUiThread(new Runnable() {

														@Override
													public void run() {
															// TODO Auto-generated method stub
																	if(dialog == null){
																		dialog = new ProgressDialog(NavigationActivity.getContext()) ;
																		dialog.setMessage(NavigationActivity.getContext().getResources().getString(R.string.loading_title)) ;
																		dialog.setCancelable(false) ;
																		dialog.setIndeterminate(false) ;
																		dialog.show() ;
																	}

											}
										}) ;
									if(!contactDownloadStarted){
												if(BiptagUserManger.isOnline(NavigationActivity.getContext()))
													downloadContact() ;
											else{
														processFailed() ;
													}

													}
									}
						}
*/

			return null;
		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);

			if(doReload){

				if(!dialog.isShowing()){
					dialog.show() ;
				}
				if(adapterSync == null){

					adapterSync = new ContactAdapter(NavigationActivity.getContext(), R.layout.contact_row, data_sync  , logs) ;

				}else{
					for (int i = 0; i < data_sync.size(); i++) {
						ContactItem ref = data_sync.get(i) ;
						boolean found = false ;
						int itemIdex  = 0 ;
						for (int j = 0; j < adapterSync.getCount(); j++) {
							ContactItem item = adapterSync.getContactItem(j) ;
							if(item.contact_email.equals(ref.contact_email)){
								found = true ;
								itemIdex = j ;
							}
						}
						if(!found){
							adapterSync.add(ref) ;
						}else{
							adapterSync.data.set(itemIdex, ref) ;
						}
					}

				}

				if(adapterUnSync == null){

					adapterUnSync = new ContactAdapter(NavigationActivity.getContext(), R.layout.contact_row, data_unsync , logs) ;

				}else{

					for (int i = 0; i < data_unsync.size(); i++) {
						ContactItem ref = data_unsync.get(i) ;
						boolean found = false ;
						for (int j = 0; j < adapterUnSync.getCount(); j++) {
							ContactItem item = adapterUnSync.getContactItem(j) ;
							if(item.contact_email.equals(ref.contact_email)){
								found = true ;
							}
						}
						if(!found){
							adapterUnSync.add(ref) ;
						}
					}


				}




				adapterSync.notifyDataSetChanged() ;
				setListViewHeightBasedOnChildren(contactlistView) ;

				adapterUnSync.notifyDataSetChanged();
				setListViewHeightBasedOnChildren(contactListViewUnSync) ;
				totalListIsLoaded = true ;
				if(data_sync.size() != 0 || contactDownloaded || (!DownloadContactWithHashCodes.isExecuting && !DownloadContactsWithIds.isExecuting))
					dialog.dismiss() ;


			}


		}

	}




	class LoadContactProgressiv extends AsyncTask<String, String, String>{


		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			BiptagUserManger.saveUserData("IS_SYNC", "1", NavigationActivity.getContext()) ;
			dialog = new ProgressDialog(NavigationActivity.getContext()) ;
			dialog.setMessage(NavigationActivity.getContext().getResources().getString(R.string.loading_title)) ;
			dialog.setCancelable(false) ;
			dialog.setIndeterminate(false) ;
			dialog.show() ;

		}


		@Override
		protected String doInBackground(String... params) {
			// TODO Auto-generated method stub

			partialLoading() ;

			Context context= NavigationActivity.getContext();
			if(BiptagUserManger.isOnline(context)){

				//String contact = String.valueOf(BiptagUserManger.getUserValue("contacts", "string", NavigationActivity.getContext().getApplicationContext())) ;
				if(savedContact.size() == 0 ){
					if(!contactDownloadStarted)
						if(BiptagUserManger.isOnline(NavigationActivity.getContext()))
							downloadContact() ;
						else
							processFailed() ;
				}
			}


			return null;
		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);


			adapterSync = new ContactAdapter(NavigationActivity.getContext(), R.layout.contact_row, data_sync_part , logs) ;
			contactlistView.setAdapter(adapterSync);
			setListViewHeightBasedOnChildren(contactlistView) ;

			adapterUnSync = new ContactAdapter(NavigationActivity.getContext(), R.layout.contact_row, data_unsync_part , logs) ;
			contactListViewUnSync.setAdapter(adapterUnSync);
			setListViewHeightBasedOnChildren(contactListViewUnSync) ;

			Context context= NavigationActivity.getContext() ;
			BiptagUserManger.saveUserData("IS_SYNC", "", context) ;
			totalListIsLoaded = true ;
			if(data_sync_part.size()!=0)
				dialog.dismiss() ;
		}



	}

	class AsynchronousSearch extends AsyncTask<String, String, String>{

		@Override
		protected String doInBackground(String... params) {
			// TODO Auto-generated method stub

			// TODO Auto-generated method stub



			if(searchField.getText().length() != 0){
				List<ContactItem> searchList = new LinkedList<ContactItem>() ;
				for (int i = 0; i < data_sync.size()	; i++) {
					ContactItem item = data_sync.get(i) ;
					if(item.contact_email.charAt(0)==searchField.getText().charAt(0)||item.contact_name.charAt(0)==searchField.getText().charAt(0)) {

						if (item.contact_email.indexOf(searchField.getText().toString()) != -1) {
							searchList.add(item);
						} else {
							if (item.contact_name.indexOf(searchField.getText().toString()) != -1) {
								searchList.add(item);
							}
						}
					}
				}

				List<ContactItem> searchListUnSync = new LinkedList<ContactItem>() ;
				for (int i = 0; i < data_unsync.size()	; i++) {
					ContactItem item = data_unsync.get(i) ;
					if(item.contact_email.charAt(0)==searchField.getText().charAt(0)||item.contact_name.charAt(0)==searchField.getText().charAt(0)) {
						if (item.contact_email.indexOf(searchField.getText().toString()) != -1) {
							searchListUnSync.add(item);
						} else {
							if (item.contact_name.indexOf(searchField.getText().toString()) != -1) {
								searchListUnSync.add(item);
							}
						}
					}
				}
Log.d("contact ", " unsync contact " +adapterUnSync.getCount());
				adapterUnSync= new ContactAdapter(NavigationActivity.getContext(), R.layout.contact_row, searchListUnSync , logs) ;
				Log.d("contact ", " unsync contact " +adapterUnSync.getCount());

				adapterSync = new ContactAdapter(NavigationActivity.getContext(), R.layout.contact_row, searchList, logs) ;
				Log.d("contact ", " sync contact " +adapterSync.getCount());

			}else{
				adapterUnSync= new ContactAdapter(NavigationActivity.getContext(), R.layout.contact_row, data_unsync, logs) ;
				adapterSync = new ContactAdapter(NavigationActivity.getContext(), R.layout.contact_row, data_sync, logs) ;


			}



			return null;
		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);


			contactListViewUnSync.setAdapter(adapterUnSync);
			setListViewHeightBasedOnChildren(contactListViewUnSync) ;
			contactlistView.setAdapter(adapterSync);
			setListViewHeightBasedOnChildren(contactlistView) ;

		}

	}


	public long getcontactId( long id){
		//id = adapterSync.getContactItem(position).contactId;

		return id;
	}
//LZR : 14/04/2016 insert contact
	public static void insertConctact(ContactItem item , int mode){
// LZR : 14/04/2016 if mode is connected
		if(mode  == 0){
			if(adapterSync != null){
				Log.d(" contact ", " insert contact  synchro");
				boolean found = false ;
//LZR : 14/04/2016 check if the contact exist
				for (int i=0; i<adapterSync.getCount(); i++){
					Log.d(" contact ", " insert contact  synchro "+adapterSync.getItem(i).contact_email +" "+item.contact_email);

					if (adapterSync.getItem(i).contact_email.equals(item.contact_email)) {
						found= true ;
						break;
					}

				}
				if (found) {
//LZR : 14/04/2016 if contact is found ==> update it
					adapterSync.updateContact(item, item.contact_local_id);

				}else {
//LZR : 14/04/2016 else add it to sync list
					adapterSync.addContact(item);
				}
				adapterSync.notifyDataSetChanged();
				if (item.contact_name.length()==1){

					item.contact_name = item.contact_email;
					adapterSync.updateContact(item,item.contact_local_id);
				}else {
					adapterSync.updateContact(item,item.contact_local_id);
					Log.d(" contact ", " length " + item.contact_name.length());
				}
				contactlistView.setAdapter(adapterSync);
				setListViewHeightBasedOnChildren(contactlistView) ;
			}
		}else{
//LZR : 14/04/2016 if mode is disconnected
			if(adapterUnSync != null){
				Log.d(" contact ", " insert contact  Unsynchro");
				boolean found = false ;
				for (int i=0; i<adapterUnSync.getCount(); i++){
//					Log.d(" contact ", " insert contact  synchro "+adapterUnSync.getItem(i).contact_email +" "+item.contact_email);

					if (adapterUnSync.getItem(i).contact_email.equals(item.contact_email)) {
						found= true ;
						break;
					}

				}
				Log.d(" contact insert ", " found " + found);
				if (found) {
					adapterUnSync.updateContact(item, item.contact_local_id);
					Log.d(" contact insert ", " found update " );
				}else {
					adapterUnSync.addContact(item);
					Log.d(" contact insert ", " found add ");
				}

				adapterUnSync.notifyDataSetChanged();

				if (item.contact_name.length()==1){

					item.contact_name = item.contact_email;
					//	adapterUnSync.getItem(item.contact_local_id).contact_name=adapterUnSync.getItem(item.contact_local_id).contact_email;
					//Log.d(" contact ", " name " + adapterSync.getItem(item.contact_local_id).contact_name);
					adapterUnSync.updateContact(item,item.contact_local_id);
				}else {
					adapterUnSync.updateContact(item,item.contact_local_id);
					Log.d(" contact ", " length " + item.contact_name.length());
				}

				contactListViewUnSync.setAdapter(adapterUnSync);
				setListViewHeightBasedOnChildren(contactListViewUnSync) ;
			}
		}


	}
//LZR : 14/04/2016 update the contact
	public static void updateConctact(ContactItem item , int mode){
//LZR : 14/04/2016 if mode is connected
		if(mode  == 1){
			if(adapterSync != null){
				Log.d(" update ", " updatecontact synchro != null " + adapterSync.getCount());
				if(item.isSynchronized){
//LZR : 14/04/2016 if contact is synchronised
					Log.d(" contact ", " is synchronized " + item.isSynchronized);

					Log.d(" contact ", " mode " + mode +" is synchronized " + adapterSync.getCount());
					if (item.contact_name.length()==1){
						adapterSync.updateContact(item, item.contact_local_id);
						item.contact_name = item.contact_email;
						//adapterSync.getItem(item.contact_local_id).contact_name=adapterSync.getItem(item.contact_local_id).contact_email;
						//Log.d(" contact ", " name " + adapterSync.getItem(item.contact_local_id).contact_name);

					}else {
						adapterSync.updateContact(item, item.contact_local_id);
//						Log.d(" contact ", " name " + adapterSync.getItem(item.contact_local_id).contact_name);
						Log.d(" contact ", " length " + item.contact_name.length());
					}

				}else{
					Log.d(" contact ", " mode " + mode + " isn't synchronized " + item.isSynchronized);
//LZR : 14/04/2016 if is usync remove it from list of contact sync and add it to the usync list
					adapterSync.removeContact(item);
					Log.d(" contact ", " mode " + mode + " isn't synchronized " + adapterSync.getCount());
					adapterUnSync.addContact(item);

					if (item.contact_name.length()==1){

						item.contact_name = item.contact_email;
					//	adapterUnSync.getItem(item.contact_local_id).contact_name=adapterUnSync.getItem(item.contact_local_id).contact_email;
						//Log.d(" contact ", " name " + adapterSync.getItem(item.contact_local_id).contact_name);
					adapterUnSync.updateContact(item,item.contact_local_id);
					}else {
						adapterUnSync.updateContact(item,item.contact_local_id);
						Log.d(" contact ", " length " + item.contact_name.length());
					}

					Log.d(" contact ", " mode " + mode +" isn't synchronized " + adapterUnSync.getCount());
				}

			}
		} else if (mode == 0) {
			if (adapterUnSync != null) {
				Log.d(" update ", "mode " + mode +" updatecontact unsynchro != null " + adapterUnSync.getCount());

				if(!item.isSynchronized){

					//adapterUnSync.updateContact(item , item.contact_local_id);
					Log.d(" contact ", " mode " + mode +" isn't synchronized " + adapterUnSync.getCount());

					if (item.contact_name.length()==1){
						adapterUnSync.updateContact(item, item.contact_local_id);
						item.contact_name = item.contact_email;
						//adapterUnSync.getItem(item.contact_local_id).contact_name=adapterUnSync.getItem(item.contact_local_id).contact_email;
						//Log.d(" contact ", " name " + adapterUnSync.getItem(item.contact_local_id).contact_name);

					}else {
						adapterUnSync.updateContact(item, item.contact_local_id);
						Log.d(" contact ", " length " + item.contact_name.length());
					}


				}

				//adapterUnSync.updateContact(item, item.contactId);

			}
		}
		contactListViewUnSync.setAdapter(adapterUnSync);
		setListViewHeightBasedOnChildren(contactListViewUnSync) ;
	contactlistView.setAdapter(adapterSync);
		setListViewHeightBasedOnChildren(contactlistView) ;
	}
}
