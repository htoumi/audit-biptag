package com.biptag.biptag.fragements;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.biptag.biptag.DefaultHome;
import com.biptag.biptag.managers.BiptagUserManger;

public class DisconnectFragement extends android.app.Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        BiptagUserManger.clearPreferences(getActivity().getApplicationContext());
        ConnectedhomeFragment.fragmentView = null;
        ContactFragement.fragmentView = null;

        Intent i = new Intent(getActivity(), DefaultHome.class);
        startActivity(i);
        getActivity().finish();
        return super.onCreateView(inflater, container, savedInstanceState);
    }


}
