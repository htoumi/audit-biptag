package com.biptag.biptag.fragements.create;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Fragment;
import android.content.Context;
import android.graphics.Point;
import android.graphics.drawable.GradientDrawable;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.FrameLayout.LayoutParams;
import android.widget.GridLayout;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.biptag.biptag.NavigationActivity;
import com.biptag.biptag.R;
import com.biptag.biptag.CustumViews.ToggleButtonGroupTableLayout;
import com.biptag.biptag.managers.BipTagFormsManager;
import com.biptag.biptag.managers.BiptagNavigationManager;
import com.biptag.biptag.managers.BiptagUserManger;
 
public class FillingFormStep2 extends Fragment {
	List<String[]> list ; 
  public static View fragmentView ; 
	
  
  public static void updateLabelsText(){
   	  if(fragmentView != null){
   		  	Button btn1 = (Button) fragmentView.findViewById(R.id.fildaraiane_step2_button1) ; 
   			Button btn2 = (Button) fragmentView.findViewById(R.id.fildaraiane_step2_button2) ; 
   			Button btn3 = (Button) fragmentView.findViewById(R.id.fildaraiane_step2_button3) ; 
   			btn1.setText(NavigationActivity.getContext().getResources().getString(R.string.coordinate)) ; 
   			btn2.setText(NavigationActivity.getContext().getResources().getString(R.string.quest_met)) ; 
   			btn3.setText(NavigationActivity.getContext().getString(R.string.support_commercials)) ; 
   			Button nextButtonL =(Button) fragmentView.findViewById(R.id.step2_nextbutton) ; 
   			nextButtonL.setText(NavigationActivity.getContext().getString(R.string.next_btn))  ; 
   	  
   			Button previousButtonL =(Button) fragmentView.findViewById(R.id.step2_previousbutton) ; 
   			previousButtonL.setText(NavigationActivity.getContext().getString(R.string.prev_btn))  ; 
  
   	  }
     }
	//LZR : 14/04/2016 function to move to the next step( step 3)
	public void nextAction(){
		 
		JSONObject array=getData() ; 
		BiptagUserManger.saveUserData("form_data_step2", array.toString(), NavigationActivity.getContext()) ; 
		String form_id_string = getArguments().getString("data") ; 
		FillingFormStep3 formStep3 = new FillingFormStep3() ; 
		BiptagNavigationManager.pushFragments(formStep3, true, "create_form_3", NavigationActivity.getContext(), form_id_string, 1) ; 	
		
	
	}
	//LZR : 14/04/2016 the function to move to the previous step ( step 1)
	private void previousAction() {
		// TODO Auto-generated method stub
		BiptagNavigationManager.pushFragments(FillingFormStep2.this, false, this.getClass().getName(), NavigationActivity.getContext(), null, 1) ;
Log.d("navigation ", "to step 1" + this.getClass().getName() + " " + NavigationActivity.getContext());
	}
	
	  public void  initFilDarianeNavigation() {
			Button btn1 = (Button) fragmentView.findViewById(R.id.fildaraiane_step2_button1) ; 
			Button btn2 = (Button) fragmentView.findViewById(R.id.fildaraiane_step2_button2) ; 
			Button btn3 = (Button) fragmentView.findViewById(R.id.fildaraiane_step2_button3) ; 
			 btn1.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					previousAction() ; 
				}
			}) ; 
			 btn2.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
				}
			}) ; 
			 
			 btn3.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					nextAction() ; 
				}
			}) ; 
			
			
		}
	
	
	public void fixLayout(View fragmentView ){

		
		    
		    WindowManager wm = (WindowManager) getActivity().getSystemService(Context.WINDOW_SERVICE);
			Display display  =wm.getDefaultDisplay() ;
			Point point  = new Point() ; 
			display.getSize(point) ; 
			int screenWidth = point.x ; 
			int screenHeight = point.y ;
		   
		     
		    LinearLayout filling_form_step1_form_container = (LinearLayout) fragmentView.findViewById(R.id.filling_form_step1_form_container) ; 
		    
		    GradientDrawable borderContainer = new GradientDrawable();
		    borderContainer.setColor(0xFFFAFAFA); //white background
		    borderContainer.setStroke(3, 0xFFE6E6EA); //black border with full opacity
		    if(Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
		    	filling_form_step1_form_container.setBackgroundDrawable(borderContainer);
		    } else { 
		    	filling_form_step1_form_container.setBackground(borderContainer);
		    } 
		     
		    LayoutParams layoutParams = (LayoutParams) filling_form_step1_form_container.getLayoutParams() ; 
		    layoutParams.setMargins(screenWidth/30, screenWidth/30, screenWidth/30, screenHeight/30) ; 
		    filling_form_step1_form_container.setLayoutParams(layoutParams) ; 
		     
		    LinearLayout fil_dariane = (LinearLayout) fragmentView.findViewById(R.id.fil_dariane) ; 
		    android.widget.LinearLayout.LayoutParams arianeLayoutParams = new android.widget.LinearLayout.LayoutParams(android.widget.LinearLayout.LayoutParams.MATCH_PARENT, getResources().getDimensionPixelSize(R.dimen.next_prev_height_button)) ; 
		    fil_dariane.setLayoutParams(arianeLayoutParams) ;
		    /*
		    Log.d("btn layer ", "btn layer margin   "+screenHeight+"  "+(screenHeight- screenHeight/10)) ; 
		    LinearLayout step1_btn_container = (LinearLayout) fragmentView.findViewById(R.id.step2_btn_container) ; 
		    LinearLayout.LayoutParams btn_layaout_params = (LinearLayout.LayoutParams) step1_btn_container.getLayoutParams() ; 
		    btn_layaout_params.setMargins(0, 0, 0, 100) ; 
		    step1_btn_container.setLayoutParams(btn_layaout_params) ; 
		   */
		    
		    
  }
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		System.gc();
		if(fragmentView == null){
			 fragmentView = inflater.inflate(R.layout.fragement_filling_form_step2	,null);
				
				fixLayout(fragmentView) ; 
			final String form_id_string = getArguments().getString("data") ; 
			int form_id = Integer.parseInt(form_id_string) ;
			list = BipTagFormsManager.buildFormComposant(form_id, getActivity(), FillingFormStep2.this, false, 17 , -1, R.id.filling_form_step1_form_container , fragmentView) ; 
			Button next = (Button) fragmentView.findViewById(R.id.step2_nextbutton) ; 
			next.setOnClickListener(new View.OnClickListener(){
				@Override 
				public void onClick(View v){
					nextAction() ; 
					
				}
			});
			
			Button preivous = (Button) fragmentView.findViewById(R.id.step2_previousbutton)  ; 
			preivous.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
				
				previousAction() ; 
				}
			}) ; 
			
			initFilDarianeNavigation() ; 
		}
	
		return fragmentView ; 
	}
	
	// LZR : 14/04/2016 return the data of the contact
public JSONObject getData() {
	String data_form_1 = String.valueOf(BiptagUserManger.getUserValue("form_data_step1", "string", getActivity()))	;
	JSONObject jsonArray= null;
		 
		try {
			 jsonArray = new JSONObject(data_form_1) ;
			if(list!= null){
				
				//Log.d("get data result ", "get data result list is not null ") ; 
				for (int i = 1; i < list.size(); i++) { 
					
					//Log.d("get data result ", "get data result list elements  "+list.get(i)[0]+"  "+list.get(i)[1]+"  "+list.get(i)[2]) ; 
					String type = list.get(i)[1] ;
					int viewid= Integer.parseInt(list.get(i)[2]) ; 
					
					if(type.equals("text")){
						//Log.d("get data result ", "get data result enter text element") ; 
						String type_1 = list.get(i-1)[1] ;
						int viewid_1= Integer.parseInt(list.get(i-1)[2]) ; 
						if(type_1.equals("LABEL")){
							//Log.d("get data result ", "get data result enter text element enter label ") ; 
							TextView textView = (TextView) getView().findViewById(viewid_1) ; 
							String key = textView.getText().toString() ; 
							EditText editText = (EditText) getView().findViewById(viewid) ; 
							String value = editText.getText().toString() ; 
							//Log.d("get data empty text ", "get data empty text "+value+"    ");
							jsonArray.put(key, value) ; 
							
						}
						
						
					}
					
					if(type.equals("text_area")){
						//Log.d("get data result ", "get data result enter text element") ; 
						String type_1 = list.get(i-1)[1] ;
						int viewid_1= Integer.parseInt(list.get(i-1)[2]) ; 
						if(type_1.equals("LABEL")){
							//Log.d("get data result ", "get data result enter text element enter label ") ; 
							TextView textView = (TextView) getView().findViewById(viewid_1) ; 
							String key = textView.getText().toString() ; 
							EditText editText = (EditText) getView().findViewById(viewid) ; 
							String value = editText.getText().toString() ; 
							//Log.d("get data empty text ", "get data empty text "+value+"    ");
							jsonArray.put(key, value) ; 
							
						}
						
						
					}
					
					
					if(type.equals("email")){
						//Log.d("get data result ", "get data result enter text element") ; 
						String type_1 = list.get(i-1)[1] ;
						int viewid_1= Integer.parseInt(list.get(i-1)[2]) ; 
						if(type_1.equals("LABEL")){
							//Log.d("get data result ", "get data result enter text element enter label ") ; 
							TextView textView = (TextView) getView().findViewById(viewid_1) ; 
							String key = textView.getText().toString() ; 
							EditText editText = (EditText) getView().findViewById(viewid) ; 
							String value = editText.getText().toString() ; 
							jsonArray.put(key, value) ; 
							//jsonArray.put(jsonObject) ; 
						}
						
						
					}
					if(type.equals("radio")){
						//Log.d("get data result ", "get data result enter text element") ; 
						String type_1 = list.get(i-1)[1] ;
						int viewid_1= Integer.parseInt(list.get(i-1)[2]) ; 
						if(type_1.equals("LABEL")){
							//Log.d("get data result ", "get data result enter text element enter label ") ; 
							TextView textView = (TextView) getView().findViewById(viewid_1) ; 
							String key = textView.getText().toString() ; 
							ToggleButtonGroupTableLayout group = (ToggleButtonGroupTableLayout) getView().findViewById(viewid) ; 
							int selectedRadioId = group.getCheckedRadioButtonId() ; 
							//Log.d("get data empty radio ", "get data empty radio "+selectedRadioId) ; 
							String value  = "" ; 
							if(selectedRadioId!=-1){
								RadioButton selectedRadioButton = (RadioButton) getView().findViewById(selectedRadioId) ; 
								 value = selectedRadioButton.getText().toString() ; 
									
							}
							jsonArray.put(key, value) ; 
							//jsonArray.put(jsonObject) ; 
							
						}
						
						
					}
					
					if(type.equals("checkbox")){
						//Log.d("get data result ", "get data result enter checkbox  element") ; 
						String type_1 = list.get(i-1)[1] ;
						int viewid_1= Integer.parseInt(list.get(i-1)[2]) ; 
						if(type_1.equals("LABEL")){
							//Log.d("get data result ", "get data result enter text element enter label ") ; 
							TextView textView = (TextView) getView().findViewById(viewid_1) ; 
							String key = textView.getText().toString() ; 
							GridLayout group = (GridLayout) getView().findViewById(viewid) ; 
							
							int childCount =group.getChildCount() ;
							JSONArray checkboxArray = new JSONArray() ; 
							for (int j = 0; j < childCount; j++) {
								View child = group.getChildAt(j) ;
								if(child.getClass() == CheckBox.class) {
									if(((CheckBox)child).isChecked()){
										checkboxArray.put(1) ; 
									}else {
										checkboxArray.put(0) ; 
									}
								}
							
							}
							
							//int selectedRadioId = group.getCheckedRadioButtonId() ; 
							//Log.d("get data empty radio ", "get data empty radio "+selectedRadioId) ; 
							//String value  = "" ; 
							//if(selectedRadioId!=-1){
								//RadioButton selectedRadioButton = (RadioButton) getView().findViewById(selectedRadioId) ; 
								// value = selectedRadioButton.getText().toString() ; 
									
							//}
							//Log.d("get data ", "get data checkbox response  "+key+" "+checkboxArray.toString()) ; 
							jsonArray.put(key,checkboxArray ) ; 
							//jsonArray.put(jsonObject) ; 
							
						}
						
					}
					
				}
			}
			
			//Log.d("get data result ", "get data result  "+jsonArray.toString()) ; 
			
			
		} catch (JSONException e) {
			// TODO: handle exception
			//Log.d("form step 1 getdata", "get data exception  "+e.getMessage()) ; 
		}
		
		return jsonArray ; 
		
	}

	
	

	

}
