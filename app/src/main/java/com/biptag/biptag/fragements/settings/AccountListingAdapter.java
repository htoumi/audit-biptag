package com.biptag.biptag.fragements.settings;

import org.json.JSONArray;
import org.json.JSONException;

import com.biptag.biptag.R;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class AccountListingAdapter extends ArrayAdapter<String> {

	JSONArray values  ; 
	
	
	public AccountListingAdapter(Context context, int resource, String[] objects) {
		super(context, resource, objects);
		// TODO Auto-generated constructor stub
	}
	

	public AccountListingAdapter(Context context, int resource, String[] objects , JSONArray values ) {
		super(context, resource, objects);
		// TODO Auto-generated constructor stub
		this.values = values ; 
	}
	
	
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
	
		if(convertView == null){
			 LayoutInflater inflater = (LayoutInflater) getContext()
	                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	            convertView = inflater.inflate(R.layout.account_listing_item, null);
		}
		
		try {
			
			//Log.d("coworker item ", "coworker item "+values.getJSONObject(position).toString())  ;
			String email = values.getJSONObject(position).getString("EMAIL") ; 
			String name = values.getJSONObject(position).getString("FNAME")+" "+values.getJSONObject(position).getString("LNAME") ; 
			String role = values.getJSONObject(position).getString("ROLE") ; 
			
			TextView emailTxt  = (TextView) convertView.findViewById(R.id.account_email_txt) ; 
			TextView nameTxt = (TextView) convertView.findViewById(R.id.account_name_txt) ; 
			TextView roleTxt = (TextView) convertView.findViewById(R.id.account_role_txt) ; 
			emailTxt.setText(email) ; 
			nameTxt.setText(name) ; 
			roleTxt.setText(role) ; 
			
			
			
		} catch (JSONException e) {
			// TODO: handle exception
			
			//Log.d("coworker adapter", "coworker adapter exception "+e.getMessage()) ;
		}
		
		
		
		return convertView ;  
	}
	
	
	

}
