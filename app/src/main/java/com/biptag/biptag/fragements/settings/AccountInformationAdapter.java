package com.biptag.biptag.fragements.settings;

import com.biptag.biptag.R;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class AccountInformationAdapter extends ArrayAdapter<String> {

	private String[] values  ; 
	private String[] subItems ; 
	int hideThumbNailIndex ; 
	
	
	public AccountInformationAdapter(Context context, int resource,
			String[] objects) {
		super(context, resource, objects);
		// TODO Auto-generated constructor stub
	}
	
	public AccountInformationAdapter(Context context, int resource,
			String[] objects , String[] subItem  , int hideThumbNailIndex) {
		super(context, resource, objects);
		this.values = objects ; 
		this.subItems = subItem ; 
		this.hideThumbNailIndex =hideThumbNailIndex ;  
	
	}    
	 
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
	
			if(convertView == null){
				 LayoutInflater inflater = (LayoutInflater) getContext()
		                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		            convertView = inflater.inflate(R.layout.genral_menu_list_item, null);
			}
			
			
			TextView item = (TextView) convertView.findViewById(R.id.general_menu_title) ; 
			TextView subItem = (TextView) convertView.findViewById(R.id.general_menu_seprator) ; 
			item.setText(values[position]) ; 
			subItem.setText(subItems[position]) ; 
			if(position< hideThumbNailIndex){ 

				ImageView imageView = (ImageView) convertView.findViewById(R.id.account_info_thumbnail) ; 
				imageView.setVisibility(View.INVISIBLE) ; 
			}




		return convertView;
	}



}
