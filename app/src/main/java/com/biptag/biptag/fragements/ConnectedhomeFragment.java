package com.biptag.biptag.fragements;

import android.app.Fragment;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.biptag.biptag.JSONParser;
import com.biptag.biptag.NavigationActivity;
import com.biptag.biptag.R;
import com.biptag.biptag.dbmanager.MySQLiteHelper;
import com.biptag.biptag.dbmanager.datasource.BTContactDataSource;
import com.biptag.biptag.dbmanager.datasource.BTFollowupTemplateDataSource;
import com.biptag.biptag.dbmanager.datasource.BTFormDataSource;
import com.biptag.biptag.dbmanager.datasource.BTLogDataSource;
import com.biptag.biptag.dbmanager.objects.BTContact;
import com.biptag.biptag.dbmanager.objects.BTFollowupTemplate;
import com.biptag.biptag.dbmanager.objects.BTFormulaire;
import com.biptag.biptag.dbmanager.objects.BTLog;
import com.biptag.biptag.fragements.create.FillingFormStep1;
import com.biptag.biptag.interfaces.AsyncResponse;
import com.biptag.biptag.managers.BipTagFormsManager;
import com.biptag.biptag.managers.BiptagUserManger;
import com.biptag.biptag.ocr.CaptureActivity;
import com.biptag.biptag.webservices.DownloadContactWithHashCodes;
import com.biptag.biptag.webservices.DownloadContactsWithIds;
import com.biptag.biptag.webservices.DownloadFiles;
import com.biptag.biptag.webservices.DownloadForms;
import com.biptag.biptag.webservices.UploadContacts;
import com.biptag.biptag.webservices.logs.DownloadLogs;
import com.biptag.biptag.webservices.templates.DownloadContacts;
import com.biptag.biptag.webservices.templates.DownloadFollowupHashcodes;

import org.json.JSONObject;

import java.util.Calendar;
import java.util.List;


// LZR : 27/02/2016
public class ConnectedhomeFragment extends Fragment implements AsyncResponse {
    JSONParser parser;
    JSONObject jsonObject;
    int agent_id;
    public static boolean formAdded;
    Button backButton;
    TextView title;
    public static List<BTContact> contacts = null;
    public static List<BTLog> logs = null;
    public static List<BTFollowupTemplate> templates = null;
    public static View fragmentView;
    private static final String TAG = ConnectedhomeFragment.class.getSimpleName();
    ProgressDialog progressDialog;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        System.gc();
        FillingFormStep1.fragmentView = null;
        BiptagUserManger.clearField(NavigationActivity.getContext(), "EditscanOCR_Creat");
        BiptagUserManger.clearField(NavigationActivity.getContext(), "EditscanOCR_Edit");
        boolean isInternetOn = BiptagUserManger.isInternetOn();
        BiptagUserManger.saveUserData("isInternetOn", isInternetOn, NavigationActivity.getContext());

        if (fragmentView == null) {


            fragmentView = inflater.inflate(R.layout.activity_connected_home, null);
            BiptagUserManger.saveUserData("IS_SYNC", "", getActivity());
            formAdded = false;
            contacts = null;
            templates = null;
            logs = null;
        }
        return fragmentView;
    }

    @Override
    public void onResume() {
        // TODO Auto-generated method stub
        super.onResume();
        System.gc();

        String createImageArrayUserPreferenceName = "images_for_user";
        String editImageArrayUserPreferenceName = "images_for_user_edit";

        BiptagUserManger.clearField(NavigationActivity.getContext(), CaptureActivity.vcardIdKeyCreate);
        BiptagUserManger.clearField(NavigationActivity.getContext(), CaptureActivity.vcardIdKeyEdit);


        BiptagUserManger.saveUserData(createImageArrayUserPreferenceName, "", getActivity());
        BiptagUserManger.saveUserData(editImageArrayUserPreferenceName, "", getActivity());
        BiptagUserManger.saveUserData("add_files_result", "", getActivity());


        if (!formAdded) {
            if (BiptagUserManger.isOnline(getActivity())) {

                BTFormDataSource dataSource = new BTFormDataSource(getActivity().getApplicationContext());
                Object agent_id_object = BiptagUserManger.getUserValue("AGENT_ID", "int", getActivity());
                agent_id = Integer.parseInt(String.valueOf(agent_id_object));

                List<BTFormulaire> formulaires = dataSource.getAllForms(agent_id);
                if (formulaires.size() == 0) {
                    showProgressDialog();

                    DownloadForms forms = new DownloadForms(getActivity());
                    forms.response = ConnectedhomeFragment.this;
                    forms.execute();
                    //	new getUserForms().execute() ;
                    BiptagUserManger.saveUserData("IS_SYNC", "1", getActivity());


                } else {
                    BipTagFormsManager.buidFormIcons(getActivity(), this);
                    formAdded = true;
                }

            } else {
                BipTagFormsManager.buidFormIcons(getActivity(), this);
                formAdded = true;
            }
        }

        String ba_id = String.valueOf(BiptagUserManger.getUserValue("BA_ID", "int", getActivity()));

        if (templates == null) {
            BTFollowupTemplateDataSource templateDataSource = new BTFollowupTemplateDataSource(getActivity().getApplicationContext());
            templates = templateDataSource.getAllTempalte(Integer.parseInt(ba_id));

        }
        Log.d("template list size", "data list template size " + templates.size());
        if (templates.isEmpty()) {
            DownloadFollowupHashcodes downloadFollowupHashcodes = new DownloadFollowupHashcodes(getActivity());
            downloadFollowupHashcodes.execute();

        }

        if (logs == null) {
            BTLogDataSource logDataSource = new BTLogDataSource(getActivity().getApplicationContext());
            logs = logDataSource.getAllLogs(agent_id);

        }

        Log.d("template list size", "data list logs size " + logs.size());

        if (logs.isEmpty()) {
            DownloadLogs downloadLogs = new DownloadLogs(getActivity(), ConnectedhomeFragment.this);
            downloadLogs.execute();

        }


        if (contacts == null) {
            BTContactDataSource contactDataSource = new BTContactDataSource(getActivity().getApplicationContext());
            contacts = contactDataSource.getAllContacts(agent_id, 1);

        }

        if (contacts.isEmpty()) {

            DownloadContactWithHashCodes contactWithHashCodes = new DownloadContactWithHashCodes(getActivity());
            contactWithHashCodes.response = this;
            if (!DownloadContactWithHashCodes.isExecuting) {
                contactWithHashCodes.execute();
            }
        }
        contacts = null;


        FillingFormStep1.fragmentView = null;

    }


    public void showProgressDialog() {
        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setTitle(getResources().getString(R.string.loading_title));
        progressDialog.setMessage(getResources().getString(R.string.loading_title));
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setIndeterminate(false);
        progressDialog.show();

    }

    public void hideProgressDialog() {
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
    }

    /*
    class getUserForms  extends AsyncTask<String, String, String>{

        ProgressDialog dialog ;

        @Override
            protected void onPreExecute() {
                // TODO Auto-generated method stub
                super.onPreExecute();
                    dialog = new ProgressDialog(getActivity()) ;
                    dialog.setTitle("Telechargement de vos formulaires") ;
                    dialog.setMessage("Veuillez patienter") ;
                    dialog.setCancelable(false) ;
                    dialog.setIndeterminate(false) ;
                    //dialog.show() ;



        }
        @Override
        protected String doInBackground(String... params) {
            // TODO Auto-generated method stub
            String url= getResources().getString(R.urls.base_url ) +getResources().getString(R.urls.get_forms_ws);
            List<NameValuePair> list= new ArrayList<NameValuePair>() ;
            String ba_id = String.valueOf(BiptagUserManger.getUserValue("BA_ID", "int", getActivity().getApplicationContext())) ;
            list.add(new BasicNameValuePair("ba_id",ba_id ));
            parser= new JSONParser() ;
            jsonObject = parser.makeHttpRequest(url, "GET", list) ;
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            //dialog.dismiss() ;
            ImageView progress = (ImageView) getView().findViewById(R.id.imgProgress);
            progress.setVisibility(View.GONE) ;

            if(jsonObject!= null){
                try {
                    if(jsonObject.getInt("success")==1){
                        // success

                        BTFormDataSource dataSource = new BTFormDataSource(getActivity()) ;
                        JSONArray forms  = jsonObject.getJSONArray("forms") ;
                        for (int i = 0; i < forms.length(); i++) {
                            JSONObject jsonForm =  forms.getJSONObject(i) ;
                            dataSource.open() ;
                            BTFormulaire localForm = dataSource.getFormWithID(agent_id, jsonForm.getInt("FORM_ID")) ;
                                    dataSource.close() ;
                                    BTFormulaire formulaire = new BTFormulaire() ;
                            formulaire.setAgentId(agent_id) ;
                            formulaire.setEventEdition(jsonForm.getString("EV_EDITION")) ;
                            formulaire.setEventEndDate(jsonForm.getString("EV_END_DATE")) ;
                            formulaire.setEventStartDate(jsonForm.getString("EV_START_DATE")) ;
                            formulaire.setEventName(jsonForm.getString("EV_NAME")) ;
                            formulaire.setFormId(jsonForm.getInt("FORM_ID")) ;
                            formulaire.setFormName(jsonForm.getString("FORM_LABEL")) ;
                            formulaire.setFormStatus(jsonForm.getString("FORM_STATUS")) ;
                            formulaire.setFormStructure(jsonForm.getJSONArray("FORM_STRUCTURE")) ;

                            if(localForm == null){

                                dataSource.open() ;
                                dataSource.insertForm(formulaire) ;
                                dataSource.close() ;
                            }else{
                                dataSource.open() ;
                                dataSource.updateForm(formulaire) ;
                            }
                        }


                        //BiptagUserManger.saveUserData("forms", jsonObject.getString("forms"), getActivity())	 ;
                        BipTagFormsManager.buidFormIcons(getActivity(), ConnectedhomeFragment.this) ;

                    }else {
                        AlertDialog.Builder alert = new AlertDialog.Builder(getActivity()) ;
                        alert.setTitle("Erreur") ;
                        alert.setMessage("Une erreur s'est produite , Veuillez reessayer plus tard") ;
                        alert.show() ;

                    }


                } catch (JSONException e) {
                    // TODO: handle exception
                    AlertDialog.Builder alert = new AlertDialog.Builder(getActivity()) ;
                    alert.setTitle("Erreur") ;
                    alert.setMessage("Une erreur s'est produite , Veuillez reessayer plus tard") ;
                    alert.show() ;

                }
            }else {
                AlertDialog.Builder alert = new AlertDialog.Builder(getActivity()) ;
                alert.setTitle("Erreur") ;
                alert.setMessage("Une erreur s'est produite , Veuillez verifier votre connexion ") ;
                alert.show() ;
            }

        }
    }
*/
    @Override
    public void processFinish(String result) {
        // TODO Auto-generated method stub

        Calendar cal = Calendar.getInstance();
        if (result.equals("forms")) {
            Log.d(TAG + " forms ", " form dub " + cal.getTime());
            BipTagFormsManager.buidFormIcons(getActivity(), ConnectedhomeFragment.this);
            DownloadFiles downloadFiles = new DownloadFiles(NavigationActivity.getContext());
            downloadFiles.response = ConnectedhomeFragment.this;
            downloadFiles.execute();

            Log.d(TAG + " forms ", "form fin " + cal.getTime());

        }

        if (result.equals("docs")) {
            Log.d(TAG + " forms ", "Doc deb " + cal.getTime());
            // upload contact
            Log.d(TAG + "uploac contact ", "uploac contact  excpetion update starting upload contact ");

            UploadContacts uploadContacts = new UploadContacts(NavigationActivity.getContext());
            uploadContacts.response = ConnectedhomeFragment.this;
            uploadContacts.execute();
            Log.d(TAG + " forms ", "doc fin " + cal.getTime());
        }

        if (result.equals("uploaded")) {
            //download contacts
            Log.d(TAG + " forms ", "uploaded deb " + cal.getTime());
            BTContactDataSource contactDataSource = new BTContactDataSource(getActivity().getApplicationContext());

            Object agent_id_object = BiptagUserManger.getUserValue("AGENT_ID", "int", getActivity());
            agent_id = Integer.parseInt(String.valueOf(agent_id_object));
            contacts = contactDataSource.getAllContacts(agent_id, 1);
            if (contacts.size() == 0) {
                Log.d(TAG + " forms ", "idsready if data base null " + cal.getTime() + " " + contacts.size());
                DownloadContacts downloadContacts = new DownloadContacts(NavigationActivity.getContext());
                downloadContacts.response = ConnectedhomeFragment.this;
                downloadContacts.execute();
            } else {


                DownloadContactWithHashCodes contactWithHashCodes = new DownloadContactWithHashCodes(NavigationActivity.getContext());
                contactWithHashCodes.response = ConnectedhomeFragment.this;
                contactWithHashCodes.execute();
            }
            BiptagUserManger.saveUserData("IS_SYNC", "", NavigationActivity.getContext());
            hideProgressDialog();

            Log.d(TAG + " forms ", "uploaded fin " + cal.getTime());
        }

        if (result.equals("ids_ready")) {
            // list of ids ready to download
            Log.d(TAG + " forms ", "idsready data base not null " + cal.getTime() + " " + MySQLiteHelper.TABLE_CONTACT.length());
            DownloadContactsWithIds downloadContactsWithIds = new DownloadContactsWithIds(NavigationActivity.getContext());
            downloadContactsWithIds.response = ConnectedhomeFragment.this;
            downloadContactsWithIds.execute();

            Log.d(TAG + " forms ", "idsready fin " + cal.getTime());
        }

        if (result.equals("contacts_done")) {
            // finish
            Log.d(TAG + " forms ", "contacts deb " + cal.getTime());
            BiptagUserManger.saveUserData("IS_SYNC", "", NavigationActivity.getContext());
            hideProgressDialog();
            Log.d(TAG + " forms ", "contacs fin " + cal.getTime());
        }

        Log.d(TAG + "process finish", "process finish " + result);
        Calendar calen = Calendar.getInstance();
        Log.d(TAG + " formulaire ", result + " process finish " + calen.getTime());

    }

    @Override
    public void processFailed() {
        // TODO Auto-generated method stub
        BiptagUserManger.saveUserData("IS_SYNC", "", NavigationActivity.getContext());

    }

    @Override
    public void synchronisationStepUp() {
        // TODO Auto-generated method stub


    }

    @Override
    public void onStop() {
        super.onStop();
        System.gc();
        fragmentView = null;
    }

    public static void setIsFormAdded(boolean value) {
        formAdded = value;
    }
}
