package com.biptag.biptag.fragements.edit;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.biptag.biptag.NavigationActivity;
import com.biptag.biptag.R;
import com.biptag.biptag.CustumViews.ToggleButtonGroupTableLayout;
import com.biptag.biptag.dbmanager.datasource.BTFormDataSource;
import com.biptag.biptag.dbmanager.objects.BTFormulaire;
import com.biptag.biptag.fragements.create.FillingFormStep2;
import com.biptag.biptag.managers.BipTagFormsManager;
import com.biptag.biptag.managers.BiptagNavigationManager;
import com.biptag.biptag.managers.BiptagUserManger;

import android.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.GridLayout;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TableRow;
import android.widget.TextView;

public class EditFormStep2 extends Fragment {
	public static View fragmentView ;
	List<String[]> list ; 
	 public static void updateLabelsText(){
	   	  if(fragmentView != null){
	   		  	Button btn1 = (Button) fragmentView.findViewById(R.id.fildaraiane_step2_button1) ; 
	   			Button btn2 = (Button) fragmentView.findViewById(R.id.fildaraiane_step2_button2) ; 
	   			Button btn3 = (Button) fragmentView.findViewById(R.id.fildaraiane_step2_button3) ; 
	   			btn1.setText(NavigationActivity.getContext().getResources().getString(R.string.coordinate)) ; 
	   			btn2.setText(NavigationActivity.getContext().getResources().getString(R.string.quest_met)) ; 
	   			btn3.setText(NavigationActivity.getContext().getString(R.string.support_commercials)) ; 
	   			Button nextButtonL =(Button) fragmentView.findViewById(R.id.step2_nextbutton) ; 
	   			nextButtonL.setText(NavigationActivity.getContext().getString(R.string.next_btn))  ; 
	   	  
	   			Button previousButtonL =(Button) fragmentView.findViewById(R.id.step2_previousbutton) ; 
	   			previousButtonL.setText(NavigationActivity.getContext().getString(R.string.prev_btn))  ; 
	  
	   	  }
	     }

	//LZR : 14/04/2016 function to move to the previous step( step 1)
	private void previousAction(){
		BiptagNavigationManager.pushFragments(EditFormStep2.this, false, this.getClass().getName(), NavigationActivity.getContext(), null, 2) ;

	}
	//LZR : 14/04/2016 function to move to the next step( step 3)
	private void nextAction() {
		// TODO Auto-generated method stub

		JSONObject array=getData() ; 
		BiptagUserManger.saveUserData("edit_data_step2", array.toString(), NavigationActivity.getContext()) ; 
		 String contat_data = getArguments().getString("data") ; 
		EditFormStep3 step3 = new EditFormStep3() ; 
		BiptagNavigationManager.pushFragments(step3, true, "edit_form_3", NavigationActivity.getContext(), contat_data, 2) ; 	
	
	} 
	 
    public void  initFilDarianeNavigation() {
		Button btn1 = (Button) fragmentView.findViewById(R.id.fildaraiane_step2_button1) ; 
		Button btn2 = (Button) fragmentView.findViewById(R.id.fildaraiane_step2_button2) ; 
		Button btn3 = (Button) fragmentView.findViewById(R.id.fildaraiane_step2_button3) ; 
		 btn1.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				previousAction() ; 
			}
		}) ; 
		 btn2.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
			}
		}) ; 
		 
		 btn3.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				nextAction() ; 
			}
		}) ; 
		
		
	}
	
	
	@Override 
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		if(fragmentView ==  null){
			fragmentView = inflater.inflate(R.layout.fragement_filling_form_step2	,null);
			LinearLayout fil_dariane = (LinearLayout) fragmentView.findViewById(R.id.fil_dariane) ; 
		    android.widget.LinearLayout.LayoutParams arianeLayoutParams = new android.widget.LinearLayout.LayoutParams(android.widget.LinearLayout.LayoutParams.MATCH_PARENT, getResources().getDimensionPixelSize(R.dimen.next_prev_height_button)) ; 
		    fil_dariane.setLayoutParams(arianeLayoutParams) ;
			
			
			try{
			final String contat_data = getArguments().getString("data") ; 
			
			JSONObject jsonObject = new JSONObject(contat_data) ; 
			//Log.d("" , "edit step 1 contact data "+contat_data) ; 
			//Log.d("", "edit step 1  json object  "+jsonObject.toString()) ; 
			final int form_id = jsonObject.getInt("CONTACT_FORM_ID") ;
				JSONObject contactResponse = jsonObject.getJSONObject("CONTACT_RESPONSE") ; 
				BTFormDataSource dataSource = new  BTFormDataSource(getActivity()) ; 
				BTFormulaire formulaire = dataSource.getFormWithID(Integer.parseInt(String.valueOf(BiptagUserManger.getUserValue("AGENT_ID", "int", getActivity()))), form_id) ; 
				
				int limit = 16 ;
				JSONArray strcuture   = formulaire.getFormStructure() ; 
				try {
					for (int i = 0; i < formulaire.getFormStructure().length(); i++) {
						if(strcuture.getJSONObject(i).getString("type").equals("address-details")){
							limit = 17  ; 
							break ; 
						}
					}
				} catch (JSONException e) {
					// TODO: handle exception
				}	
			list = BipTagFormsManager.buildFormComposant(form_id, getActivity(), EditFormStep2.this, false, limit , -1, R.id.filling_form_step1_form_container , fragmentView) ; 
			fillformWithContactResponse(form_id, 16, -1, contactResponse) ; 
			Button next = (Button) fragmentView.findViewById(R.id.step2_nextbutton) ; 
			next.setOnClickListener(new View.OnClickListener(){ 
				@Override 
				public void onClick(View v){
					nextAction() ; 
				}
			});
			}catch(JSONException e){
				//Log.d("" , "edit step 2 next action  "+e.getMessage()) ;	
			}
			
			Button preivous = (Button )fragmentView.findViewById(R.id.step2_previousbutton) ; 
			preivous.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					previousAction() ; 
				}
			});
			 
			initFilDarianeNavigation() ; 
		}

		return fragmentView ;  
	}
	////LZR : 14/04/2016 function to get data of contact ( step 1 + step 2)
	public JSONObject getData() {
		String data_form_1 = String.valueOf(BiptagUserManger.getUserValue("edit_data_step1", "string", getActivity()))	;
		JSONObject jsonArray= null;
			 
			try {
				 jsonArray = new JSONObject(data_form_1) ;
				if(list!= null){
					 
					//Log.d("get data result ", "get data result list is not null ") ; 
					for (int i = 1; i < list.size(); i++) { 
						
						//Log.d("get data result ", "get data result list elements  "+list.get(i)[0]+"  "+list.get(i)[1]+"  "+list.get(i)[2]) ; 
						String type = list.get(i)[1] ;
						int viewid= Integer.parseInt(list.get(i)[2]) ; 
						
						if(type.equals("text_area")){
							//Log.d("get data result ", "get data result enter text element") ; 
							String type_1 = list.get(i-1)[1] ;
							int viewid_1= Integer.parseInt(list.get(i-1)[2]) ; 
							if(type_1.equals("LABEL")){
								//Log.d("get data result ", "get data result enter text element enter label ") ; 
								TextView textView = (TextView) getView().findViewById(viewid_1) ; 
								String key = textView.getText().toString() ; 
								EditText editText = (EditText) getView().findViewById(viewid) ; 
								String value = editText.getText().toString() ; 
								//Log.d("get data empty text ", "get data empty text "+value+"    ");
								jsonArray.put(key, value) ; 
								
							}
							
							
						}
						
						
						if(type.equals("text")){
							//Log.d("get data result ", "get data result enter text element") ; 
							String type_1 = list.get(i-1)[1] ;
							int viewid_1= Integer.parseInt(list.get(i-1)[2]) ; 
							if(type_1.equals("LABEL")){
								//Log.d("get data result ", "get data result enter text element enter label ") ; 
								TextView textView = (TextView) getView().findViewById(viewid_1) ; 
								String key = textView.getText().toString() ; 
								EditText editText = (EditText) getView().findViewById(viewid) ; 
								String value = editText.getText().toString() ; 
								//Log.d("get data empty text ", "get data empty text "+value+"    ");
								jsonArray.put(key, value) ; 
								
							}
							
							
						}
						if(type.equals("email")){
							//Log.d("get data result ", "get data result enter text element") ; 
							String type_1 = list.get(i-1)[1] ;
							int viewid_1= Integer.parseInt(list.get(i-1)[2]) ; 
							if(type_1.equals("LABEL")){
								//Log.d("get data result ", "get data result enter text element enter label ") ; 
								TextView textView = (TextView) getView().findViewById(viewid_1) ; 
								String key = textView.getText().toString() ; 
								EditText editText = (EditText) getView().findViewById(viewid) ; 
								String value = editText.getText().toString() ; 
								jsonArray.put(key, value) ; 
								//jsonArray.put(jsonObject) ; 
							}
							
							
						}
						if(type.equals("radio")){
							//Log.d("get data result ", "get data result enter text element") ; 
							String type_1 = list.get(i-1)[1] ;
							int viewid_1= Integer.parseInt(list.get(i-1)[2]) ; 
							if(type_1.equals("LABEL")){
								//Log.d("get data result ", "get data result enter text element enter label ") ; 
								TextView textView = (TextView) getView().findViewById(viewid_1) ; 
								String key = textView.getText().toString() ; 
								ToggleButtonGroupTableLayout group = (ToggleButtonGroupTableLayout) getView().findViewById(viewid) ; 
								int selectedRadioId = group.getCheckedRadioButtonId() ; 
								//Log.d("get data empty radio ", "get data empty radio "+selectedRadioId) ; 
								String value  = "" ; 
								if(selectedRadioId!=-1){
									RadioButton selectedRadioButton = (RadioButton) getView().findViewById(selectedRadioId) ; 
									 value = selectedRadioButton.getText().toString() ; 
										
								}
								jsonArray.put(key, value) ; 
								//jsonArray.put(jsonObject) ; 
								
							}
							
							
						}
						
						if(type.equals("checkbox")){
							//Log.d("get data result ", "get data result enter checkbox  element") ; 
							String type_1 = list.get(i-1)[1] ;
							int viewid_1= Integer.parseInt(list.get(i-1)[2]) ; 
							if(type_1.equals("LABEL")){
								//Log.d("get data result ", "get data result enter text element enter label ") ; 
								TextView textView = (TextView) getView().findViewById(viewid_1) ; 
								String key = textView.getText().toString() ; 
								GridLayout group = (GridLayout) getView().findViewById(viewid) ; 
								
								int childCount =group.getChildCount() ;
								JSONArray checkboxArray = new JSONArray() ; 
								for (int j = 0; j < childCount; j++) {
									View child = group.getChildAt(j) ;
									if(child.getClass() == CheckBox.class) {
										if(((CheckBox)child).isChecked()){
											checkboxArray.put(1) ; 
										}else {
											checkboxArray.put(0) ; 
										}
									}
								
								}
								
								//int selectedRadioId = group.getCheckedRadioButtonId() ; 
								//Log.d("get data empty radio ", "get data empty radio "+selectedRadioId) ; 
								//String value  = "" ; 
								//if(selectedRadioId!=-1){
									//RadioButton selectedRadioButton = (RadioButton) getView().findViewById(selectedRadioId) ; 
									// value = selectedRadioButton.getText().toString() ; 
										
								//}
								//Log.d("get data ", "get data checkbox response  "+key+" "+checkboxArray.toString()) ; 
								jsonArray.put(key,checkboxArray ) ; 
								//jsonArray.put(jsonObject) ; 
								
							}
							
						}
						
					}
				}
				
				//Log.d("get data result ", "get data result  "+jsonArray.toString()) ; 
				
				
			} catch (JSONException e) {
				// TODO: handle exception
				//Log.d("form step 1 getdata", "get data exception  "+e.getMessage()) ; 
			}
			
			return jsonArray ; 
			
		}
	
	
	
	
public void fillformWithContactResponse(int form_id , int offset  , int limit  , JSONObject jsonResponse) {
	
	try {
		//Log.d("edit step 1 ", "edit step 2  filling response  in filling form ") ;
	
		BTFormDataSource formDataSource = new BTFormDataSource(getActivity()) ;
		String agent_id= String.valueOf(BiptagUserManger.getUserValue("AGENT_ID", "int", getActivity().getApplicationContext())) ; 
		BTFormulaire  formulaire = formDataSource.getFormWithID(Integer.parseInt(agent_id), form_id) ;  
		List<String[]> keys = new ArrayList<String[]>() ; 
		JSONArray formStructure = formulaire.getFormStructure() ; 
			// recuperation des clé
			if(limit== -1 ){
				limit= formStructure.length() ; 
			}
			for (int k = offset; k < limit	; k++) {
			if(formStructure.getJSONObject(k).getString("type").equals("LABEL")){
				keys.add(new String[]{formStructure.getJSONObject(k).getJSONArray("VALUE").getString(0) , String.valueOf(k) }) ; 
			}
		}
			JSONArray array = new JSONArray(list) ; 
			
			//Log.d("edit step 1 ", "edit step 2  list composante "+array.toString()) ;
			for(int l = 0 ; l < keys.size() ; l++){  
				//Log.d("edit step 1 ", "edit step 2  list key "+keys.get(l)[0]+"  "+keys.get(l)[1]) ;
				int  viewForResponse = Integer.parseInt(keys.get(l)[1]) ; 
				//Log.d("edit step 1 ", "edit step 2  filling response key view index "+viewForResponse) ;
				int viewIdForResponse = Integer.parseInt(list.get(viewForResponse-16)[2]) ;
				String viewType = list.get(viewForResponse-16)[1] ; 
				//Log.d("edit step 1 ", "edit step 2  filling response key "+keys.get(l)[0]) ;
				//Log.d("edit step 1 ", "edit step 2  filling response type "+viewType) ;
				if(viewType.equals("text")){  
					EditText editText= (EditText) fragmentView.findViewById(viewIdForResponse) ; 
					String response  = "" ; 
					if(jsonResponse.has(keys.get(l)[0]))
					response = jsonResponse.getString(keys.get(l)[0]) ; 
					editText.setText(response) ; 
					//Log.d("edit step 1 ", "edit step 2  filling response "+response) ;
				} 
				
				if(viewType.equals("text_area")){  
					EditText editText= (EditText) fragmentView.findViewById(viewIdForResponse) ; 
					String response  = "" ; 
					if(jsonResponse.has(keys.get(l)[0]))
					response = jsonResponse.getString(keys.get(l)[0]) ; 
					editText.setText(response) ; 
					//Log.d("edit step 1 ", "edit step 2  filling response "+response) ;
				} 
				
				
				if(viewType.equals("radio")){
					ToggleButtonGroupTableLayout group = (ToggleButtonGroupTableLayout) fragmentView.findViewById(viewIdForResponse) ; 
					String response  = "" ; 
					if(jsonResponse.has(keys.get(l)[0]))
					response = jsonResponse.getString(keys.get(l)[0]) ; 
					int rowsCount = group.getChildCount() ; 
					for(int n= 0 ; n< rowsCount;n++){
						
						TableRow row= (TableRow) group.getChildAt(n) ; 
						int radioCount = row.getChildCount() ;
						for (int m = 0; m < radioCount; m++) {
							RadioButton radio =(RadioButton) row.getChildAt(m) ; 
							if(radio.getText().equals(response)){
								radio.setChecked(true) ;
								group.activeRadioButton =radio ; 
							}else{
								radio.setChecked(false) ; 
							}
						} 
						
					}
				}
				
				if(viewType.equals("checkbox")){
					GridLayout gridLayout = (GridLayout) fragmentView.findViewById(viewIdForResponse) ; 
						if(jsonResponse.has(keys.get(l)[0])){
							
							
							JSONArray response  = new JSONArray() ; 
							if(jsonResponse.has(keys.get(l)[0]))
							response = jsonResponse.getJSONArray(keys.get(l)[0]) ; 
							
							int checkboxCount = gridLayout.getChildCount() ; 
							for(int n= 0 ; n< checkboxCount ;n++){
								CheckBox checkBox =(CheckBox) gridLayout.getChildAt(n) ; 
								if(response.getInt(n)== 1){
									checkBox.setChecked(true) ; 
								}else{
									checkBox.setChecked(false) ; 
		 						}
							}		
						}else{
							//Log.d("has not key ", "has no key "+keys.get(l)[0]) ; 
						}
					
				}
			}
			
		 
		
	

} catch (JSONException e) {
	// TODO: handle exception
	//Log.d("edit step 1 ", "edit step 1  filling response exception "+e.getMessage()) ;
}
	}

}
