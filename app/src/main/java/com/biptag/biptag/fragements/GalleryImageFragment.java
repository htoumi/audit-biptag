package com.biptag.biptag.fragements;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.GradientDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.Settings.Global;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.biptag.biptag.NavigationActivity;
import com.biptag.biptag.R;
import com.biptag.biptag.dbmanager.datasource.BTImageDataSource;
import com.biptag.biptag.dbmanager.objects.BTImage;
import com.biptag.biptag.interfaces.ImageHandlerInterface;
import com.biptag.biptag.managers.BipTagBitmapLoader;
import com.biptag.biptag.managers.BiptagNavigationManager;
import com.biptag.biptag.managers.BiptagUserManger;
import com.biptag.biptag.webservices.ImageDownloader;

public class GalleryImageFragment  extends Fragment implements ImageHandlerInterface{
	Button addFomCamera , addFromGalley , okButton , cancelButton ;
	private static final int SELECT_PICTURE = 1;
	private static final int CAMERA_REQUEST= 2 ;
	int imageGlobalCount = 0 ;
	private String selectedImagePath;
	JSONArray imagesJson , imageJsonTemp  ;
	List<Integer> idsTreated ;
	LinearLayout imageContainer ;
	public boolean isEditMode  ;
	public final static String createImageArrayUserPreferenceName = "images_for_user" ;
	public final static String editImageArrayUserPreferenceName = "images_for_user_edit" ;
	View fragmentView ;
	TextView  imageNumberLabel ;
	int responseID;
	int stack = 1 ;
	PopupWindow window ;

	int screenWidth  ;
	int screenHeight ;
	public  boolean imageChanged =false;
	@SuppressLint({"NewApi", "ValidFragment"})
	public GalleryImageFragment(int stack) {
		this.stack = stack ;
		// TODO Auto-generated constructor stub
	}
	public GalleryImageFragment() {

		// TODO Auto-generated constructor stub
	}

	@SuppressLint({"NewApi", "ValidFragment"})
	public GalleryImageFragment(JSONArray array , boolean editMode , int stack ) {
		// TODO Auto-generated constructor stub
		this.isEditMode = editMode ;
		imageJsonTemp = new JSONArray() ;
		idsTreated = new ArrayList<Integer>() ;
		this.stack = stack ;

		if(array != null) {
			imagesJson = array;
				}
		else
			imagesJson = new JSONArray() ;

	}

	@Override
	public void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		if(window != null) {
			window.dismiss();
			window = null;
		}
	}

protected String pictureImagePath = "";
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
							 Bundle savedInstanceState) {
		// TODO Auto-generated method stub  galleryimagefragment
		imageGlobalCount= 0;
		WindowManager wm = (WindowManager) NavigationActivity.getContext().getSystemService(Context.WINDOW_SERVICE);
		Display display  =wm.getDefaultDisplay() ;
		Point point  = new Point() ;
		display.getSize(point) ;
		screenWidth = point.x ;
		screenHeight = point.y ;


		//Log.d("image json", "image json on create view") ;
		String key= "" ;

		if(!isEditMode){
			key = createImageArrayUserPreferenceName ;
		}else{
			key = editImageArrayUserPreferenceName ;
		}
		String imageJsonString = String.valueOf(BiptagUserManger.getUserValue(key, "string", NavigationActivity.getContext())) ;
		
		try {
			imagesJson = new JSONArray(imageJsonString) ;
			imageJsonTemp = imagesJson ;
		} catch (JSONException e) {
			// TODO Auto-generated catch block

		}
		//Log.d("image json", "image json "+imageJsonString) ;
		fragmentView = inflater.inflate(R.layout.galleryimagefragment,null);
		LinearLayout gallery_action_bar = (LinearLayout) fragmentView.findViewById(R.id.gallery_action_bar) ;
		GradientDrawable border = new GradientDrawable();
		border.setColor(0xFFE7ECEE); //white background
		border.setStroke(3, 0xFFCACAD0); //black border with full opacity
		if(Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
			//gallery_action_bar.setBackgroundDrawable(border);
		} else {
			//gallery_action_bar.setBackground(border);
		}


		LinearLayout gallery_img_container = (LinearLayout) fragmentView.findViewById(R.id.gallery_img_container) ;
		border = new GradientDrawable();
		border.setColor(0xffefeff4); //white background
		border.setStroke(5, 0xFFE6E6EA); //black border with full opacity
		if(Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
			//gallery_img_container.setBackgroundDrawable(border);
		} else {
			//gallery_img_container.setBackground(border);
		}

		LinearLayout.LayoutParams params =(LayoutParams) gallery_img_container.getLayoutParams();
		params.setMargins(screenWidth/30, screenHeight/30, screenWidth/30, screenHeight/30) ;
		gallery_img_container.setLayoutParams(params) ;

		imageNumberLabel = (TextView) fragmentView.findViewById(R.id.gallery_number_img_lbl ) ;
		imageNumberLabel.setText(NavigationActivity.getContext().getResources().getString(R.string.gall_photos_added)) ;
		imageContainer = (LinearLayout) fragmentView.findViewById(R.id.gallery_imgs) ;

		addFomCamera = (Button) fragmentView.findViewById(R.id.gallery_camera_btn) ;
		addFromGalley = (Button) fragmentView.findViewById(R.id.scan_vcard_button) ;
		try {
			addFomCamera.setBackground(NavigationActivity.getContext().getResources().getDrawable(R.drawable.gallery_bib_button_circle)) ;
		} catch (OutOfMemoryError e) {
			// TODO: handle exception
		}

		try {
			addFromGalley.setBackground(NavigationActivity.getContext().getResources().getDrawable(R.drawable.gallery_camera_button_circle)) ;
		} catch (OutOfMemoryError e) {
			// TODO: handle exception
		}

		addFromGalley.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				//Log.d("image ", "image json "+imagesJson.toString()) ;
				if(imageGlobalCount < 3){
					Intent intent = new Intent();
					intent.setType("image/*");
					intent.setAction(Intent.ACTION_PICK);
					startActivityForResult(Intent.createChooser(intent,
							"Select Picture"), SELECT_PICTURE);
					imageChanged = true;
				}else{
					AlertDialog.Builder builder = new AlertDialog.Builder(NavigationActivity.getContext()) ;
					builder.setMessage(getResources().getString(R.string.limit_photos_msg)) ;
					builder.setTitle(getResources().getString(R.string.limit_photos_title)) ;
					builder.setPositiveButton("OK", new OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {
							// TODO Auto-generated method stub
							dialog.dismiss() ;
						}
					}) ;

					builder.show() ;
				}
			}
		});

		addFomCamera.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				if(imageGlobalCount< 3){

					String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
					String imageFileName = timeStamp + ".jpg";
					File storageDir = Environment.getExternalStoragePublicDirectory(
							Environment.DIRECTORY_PICTURES);
					pictureImagePath = storageDir.getAbsolutePath() + "/" + imageFileName;
					File file = new File(pictureImagePath);
					Uri outputFileUri = Uri.fromFile(file);
					Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
					cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);







					//Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
					startActivityForResult(cameraIntent, CAMERA_REQUEST);
					imageChanged = true;
				}else{
					AlertDialog.Builder builder = new AlertDialog.Builder(NavigationActivity.getContext()) ;
					builder.setMessage(getResources().getString(R.string.limit_photos_msg)) ;
					builder.setTitle(getResources().getString(R.string.limit_photos_title)) ;
					builder.setPositiveButton("OK", new OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {
							// TODO Auto-generated method stub
							dialog.dismiss() ;
						}
					}) ;

					builder.show() ;
				}
			}
		}) ;

		okButton = (Button) fragmentView.findViewById(R.id.gallery_submit_bnt) ;
		cancelButton = (Button) fragmentView.findViewById(R.id.galleyr_cancel_btn) ;

		cancelButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				//imagesJson = new JSONArray() ;
				//fragmentView = null ;
				BiptagNavigationManager.pushFragments(GalleryImageFragment.this, false, GalleryImageFragment.class.getName(), NavigationActivity.getContext(), null, stack) ;
			}
		}) ;

		okButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				String key= "" ;
				if(!isEditMode){
					key = createImageArrayUserPreferenceName ;
				}else{
					key = editImageArrayUserPreferenceName ;
				}
				imagesJson = imageJsonTemp  ;
				BiptagUserManger.saveUserData(key, imagesJson.toString(), NavigationActivity.getContext()) ;
				BiptagNavigationManager.pushFragments(GalleryImageFragment.this, false, GalleryImageFragment.class.getName(), NavigationActivity.getContext(), null, stack) ;

			}
		}) ;



		/*
		    for (int i = 0; i < imageContainer.getChildCount(); i++) {
				imageContainer.removeViewAt(i) ;

			} */
		if(imageJsonTemp != null)
			imageGlobalCount = imageJsonTemp.length() ;

		if(this.isEditMode){
			JSONArray copy = this.imagesJson ;
			int ba_id= Integer.parseInt(String.valueOf(BiptagUserManger.getUserValue("BA_ID", "int", NavigationActivity.getContext()))) ;

			try {
				imagesJson = new JSONArray() ;
				BTImageDataSource source = new BTImageDataSource(NavigationActivity.getContext()) ;
				for (int i = 0; i < copy.length(); i++) {
					imageGlobalCount++ ;
					String imgName = copy.getString(i) ;
					Log.d("image json", "image json img name search "+imgName) ;
					BTImage image = source.selectImagesWithName(imgName, ba_id) ;



					if(image != null){
						imageJsonTemp.put(image.getImageId()) ;
						Log.d("image json", "image json img "+image.getImageId()) ;
					}else{
						ImageDownloader downloader = new ImageDownloader(imgName+".jpg", NavigationActivity.getContext().getCacheDir().getAbsolutePath()+"/"+imgName+".jpg", NavigationActivity.getContext() , responseID, imgName, null , 0 , addImage(i,i)) ;
						downloader.response = GalleryImageFragment.this ;
						downloader.execute() ;
						Log.d("image json", "image json img null") ;
					}


				}
			} catch (JSONException e) {
				// TODO: handle exception
			}



		}


		return fragmentView ;


	}


	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		//Log.d("image json", "image json on resume") ;

		getView().setFocusableInTouchMode(true);
		getView().requestFocus();
		getView().setOnKeyListener(new View.OnKeyListener() {
			@Override
			public boolean onKey(View v, int keyCode, KeyEvent event) {
				if (imageChanged) {
					if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {
						// handle back button's click listener
						Log.d(" return ", " ok ");

						AlertDialog.Builder builder = new AlertDialog.Builder(NavigationActivity.getContext());
						builder.setTitle(NavigationActivity.getContext().getResources().getString(R.string.save_change_title));
						builder.setMessage(NavigationActivity.getContext().getResources().getString(R.string.save_change_desc));
						builder.setPositiveButton(NavigationActivity.getContext().getResources().getString(R.string.leave_change), new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog, int which) {
								// TODO Auto-generated method stub
								dialog.dismiss();
								BiptagNavigationManager.pushFragments(GalleryImageFragment.this, false, GalleryImageFragment.class.getName(), NavigationActivity.getContext(), null, stack);

							}
						});

						builder.setNegativeButton(NavigationActivity.getContext().getResources().getString(R.string.save_change), new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog, int which) {
								// TODO Auto-generated method stub
								String key = "";
								if (!isEditMode) {
									key = createImageArrayUserPreferenceName;
								} else {
									key = editImageArrayUserPreferenceName;
								}
								imagesJson = imageJsonTemp;
								BiptagUserManger.saveUserData(key, imagesJson.toString(), NavigationActivity.getContext());
								BiptagNavigationManager.pushFragments(GalleryImageFragment.this, false, GalleryImageFragment.class.getName(), NavigationActivity.getContext(), null, stack);

								dialog.dismiss();

							}
						});
						builder.setNeutralButton(NavigationActivity.getContext().getResources().getString(R.string.return_image_list),  new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog, int which) {
								// TODO Auto-generated method stub
									dialog.dismiss();

							}
						});

						builder.show();

						return true;
					}
				}
				return false;
			}
		});

		organizePictures() ;
	}

	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (resultCode == Activity.RESULT_OK) {
			if (requestCode == SELECT_PICTURE) {
				imageGlobalCount++ ;
				Uri selectedImageUri = data.getData();
				selectedImagePath = getPath(selectedImageUri);
				Log.d("Image ", " path " + selectedImagePath + " uri " + selectedImageUri);
	                /*AlertDialog.Builder  builder = new AlertDialog.Builder(ty()) ;
	                builder.setTitle("Path") ;
	                builder.setMessage("path is "+selectedImagePath) ;
	                builder.setPositiveButton("ok", new OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {
							// TODO Auto-generated method stub
							dialog.dismiss()  ;
						}
					}) ;
	                builder.show() ;  */
				int agent_id= Integer.parseInt(String.valueOf(BiptagUserManger.getUserValue("BA_ID", "int", NavigationActivity.getContext()))) ;
				Calendar c = Calendar.getInstance();
				SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
				String formattedDate = df.format(c.getTime());
				BTImage image = new BTImage(NavigationActivity.getContext()) ;
				image.setImagePath(selectedImagePath) ;
				image.setAgentId(agent_id) ;
				image.setImageDate(formattedDate) ;
				image.setResponseId(0) ;
				image.setIsSent(-1) ;
				image.cacheImage() ;
				BTImageDataSource dataSource = new BTImageDataSource(NavigationActivity.getContext()) ;
				long imageId  = dataSource.saveImage(image) ;
				Log.d("size image", "save image height "+image.getImageName()) ;

				imageJsonTemp.put(imageId) ;


				if(imageId != 0){
	                	/*try {
							//Bitmap bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(),selectedImageUri);
							//addPicture(bitmap) ;
	                	} catch (FileNotFoundException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}*/



				}else{
					// error
				}

			}

			if(requestCode == CAMERA_REQUEST){
				File imgFile = new  File(pictureImagePath);
//if (imgFile.exists()) {
				final BitmapFactory.Options options = new BitmapFactory.Options();
				options.inSampleSize = 2;
				Bitmap photo = BitmapFactory.decodeFile(imgFile.getAbsolutePath(),options);
				SaveBitmap saveBitmap = new SaveBitmap();
				saveBitmap.setBitmap(photo);
				Log.d(" size ", photo.getWidth()+" ");
//}
				imageGlobalCount++ ;
				// camera result
			//	Bitmap photo = (Bitmap) data.getExtras().get("data");
				Log.d("image size" , " " +saveBitmap.getBitmap().getWidth());
				//addPicture(photo) ;
				int agent_id= Integer.parseInt(String.valueOf(BiptagUserManger.getUserValue("BA_ID", "int", NavigationActivity.getContext()))) ;
				Calendar c = Calendar.getInstance();
				SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
				String formattedDate = df.format(c.getTime());
				BTImage image = new BTImage(NavigationActivity.getContext()) ;
				image.setBitmap(saveBitmap.getBitmap()) ;
				//image.setImageName();


				image.setAgentId(agent_id) ;
				image.setImageDate(formattedDate) ;
				image.setResponseId(0) ;
				image.setIsSent(-1) ;
				image.saveBitmap();
				BTImageDataSource dataSource = new BTImageDataSource(NavigationActivity.getContext()) ;
				long imageId  = dataSource.saveImage(image) ;
				//Log.d("save image", "save image id "+imageId) ;

				imageJsonTemp.put(imageId) ;

			}
			imageNumberLabel.setText( imageJsonTemp.length()+" "+NavigationActivity.getContext().getResources().getString(R.string.gall_photos_added)) ;
		}
	}



	public ImageView addImage(int i, int j){
		final int indexToRemove= j ;

		int marginTop=0 , marginLeft =0;
		switch (i) {
			case 0:

				marginLeft = screenWidth/4 - screenWidth/6 ;
				marginTop  = 0 ;
				break;
			case 1 :

				marginLeft=-7*screenWidth/12;
				marginTop = screenHeight/6 ;
				break ;
			case 2 :

				marginLeft = screenWidth/4 - screenWidth/6 ;
				marginTop  = screenHeight/6 ;

				//marginTop= screenHeight/6 ;
				//marginLeft = -7*screenWidth/12;
				break ;
			case 3 :
				marginLeft=-7*screenWidth/12;
				marginTop = screenHeight/3 ;
				break ;
			default:
				break;
		}
		//Log.d("image margins", "image margins "+i+"  "+marginTop+" "+marginLeft)  ;
		ImageView imageView = new ImageView(NavigationActivity.getContext()) ;
			/*
			BipTagBitmapLoader loader = new BipTagBitmapLoader(imagePath, getActivity(), imageView , GalleryImageFragment.this) ;
			loader.execute() ;*/



		//Log.d("image json", "image json bitmap null") ;

		LinearLayout.LayoutParams layoutParams = new LayoutParams(screenWidth/4, screenWidth/4) ;
		layoutParams.setMargins(marginLeft, marginTop, 0, 0) ;
		//imageView.setBackgroundColor(0xFFbbbbbb);
		imageView.setBackgroundColor(0xffffffff);
		//imageView.setBackgroundColor(Color.YELLOW) ;
		//final Bitmap bitmapCopy = bitmap ;
		imageView.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Bitmap bitmap  = null ;
				try {
					bitmap = ((BitmapDrawable)((ImageView) v).getDrawable()).getBitmap();
				} catch (NullPointerException e) {
					// TODO: handle exception
				}


				if(bitmap != null){
					WindowManager wm = (WindowManager) NavigationActivity.getContext().getSystemService(Context.WINDOW_SERVICE);
					Display display  =wm.getDefaultDisplay() ;
					Point point  = new Point() ;
					display.getSize(point) ;
					int screenWidth = point.x ;
					int screenHeight = point.y ;
					LayoutInflater inflater= LayoutInflater.from(NavigationActivity.getContext()) ;
					View previwLayout = inflater.inflate(R.layout.popup_preview_image, null) ;
					window = new PopupWindow(previwLayout , screenWidth , screenHeight*7/10) ;
					ImageView imageView = (ImageView) previwLayout.findViewById(R.id.gallery_preivew_image) ;
					imageView.setImageBitmap(bitmap) ;
					Button okButton = (Button)previwLayout.findViewById(R.id.gallery_preview_ok_btn) ;
					Button deleteButton = (Button) previwLayout.findViewById(R.id.gallery_preview_delete_btn) ;

					okButton.setOnClickListener(new View.OnClickListener() {

						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
							window.dismiss() ;
						}
					}) ;
					deleteButton.setOnClickListener(new View.OnClickListener() {

						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub

							try {
								List<Integer> ids = new ArrayList<Integer>() ;
								Log.d("gallery delete", "gallery delete "+indexToRemove + " " + imageJsonTemp )  ;
								for (int j = 0; j < imageJsonTemp.length(); j++) {
									if(j!= indexToRemove){
										ids.add(imageJsonTemp.getInt(j)) ;
									}
								}
								imageJsonTemp = new JSONArray(ids);
								imageGlobalCount = imageJsonTemp.length() ;
								imageChanged = true;
								Log.d("image json temp", "image json temp "+imageJsonTemp.toString())  ;
							} catch (JSONException e) {
								// TODO: handle exception
							}

							organizePictures() ;

							window.dismiss() ;

						}
					}) ;

					window.showAtLocation(previwLayout, Gravity.CENTER , 0, 0) ;

				}else{
					AlertDialog.Builder  builder = new AlertDialog.Builder(NavigationActivity.getContext()) ;
					builder.setTitle(getResources().getString(R.string.error_image_title)) ;
					builder.setMessage(getResources().getString(R.string.error_image_msg)) ;
					builder.setPositiveButton("OK", new OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {
							// TODO Auto-generated method stub
							dialog.dismiss() ;
						}
					}) ;
					builder.show() ;

				}

			}
		});


		imageView.setLayoutParams(layoutParams) ;
		//Log.d("image json ", "image json child count"+imageContainer.getChildCount()) ;



		imageContainer.addView(imageView) ;
		Log.d("Text ", "image container "+ imageView.getHeight());
		return imageView ;

	}

	public void organizePictures(){
		imageContainer.removeAllViews() ;

		ImageView holdeRef = new ImageView(NavigationActivity.getContext()) ;

		try {
			holdeRef.setImageDrawable(NavigationActivity.getContext().getResources().getDrawable(R.drawable.gallery_holder)) ;

		} catch (OutOfMemoryError e) {
			// TODO: handle exception
		}
		LinearLayout layout = new LinearLayout(NavigationActivity.getContext()) ;
		//Log.d("image add", "image add child count 0 ") ;
		layout.addView(holdeRef) ;
		layout.setBackgroundColor(Color.RED) ;
		LinearLayout.LayoutParams layoutParams = new LayoutParams(screenWidth/4, screenWidth/4) ;
		holdeRef.setLayoutParams(layoutParams) ;
		imageContainer.addView(layout)  ;


		//imageNumberLabel.setText( imageJsonTemp.length()+" "+NavigationActivity.getContext().getResources().getString(R.string.gall_photos_added)) ;
		int imageOCRCoun = 0;
		if(imageJsonTemp!= null){



			//Log.d("json image ", "image json on resume "+imagesJson.toString()) ;
			//Log.d("json image ", "image json temp on resume "+imageJsonTemp.toString()) ;
			BTImageDataSource dataSource = new BTImageDataSource(NavigationActivity.getContext()) ;

			for (int i = 0; i < imageJsonTemp.length(); i++) {
				try {


					final int imageId = imageJsonTemp.getInt(i) ;

					BTImage image = dataSource.selectImagesWithId(imageId, Integer.parseInt(String.valueOf(BiptagUserManger.getUserValue("BA_ID", "int", NavigationActivity.getContext())))) ;
					if(image != null){
//LZR : 28 03 2016 : afficher les images non scannées
						if (image.getImagePathOCR() == null) {
						//Log.d("image json ", "image json path "+image.getImageName()) ;
						String imagePath = NavigationActivity.getContext().getCacheDir().getAbsolutePath()+"/"+image.getImageName()+".jpg" ;
						Log.d(" image ", " image path "+ imagePath + " position " + imageOCRCoun);
						ImageDownloader downloader = new ImageDownloader(image.getImageName()+".jpg", NavigationActivity.getContext().getCacheDir().getAbsolutePath()+"/"+image.getImageName()+".jpg", NavigationActivity.getContext() , responseID , image.getImageName(), imagePath, image.getImageId() , addImage(imageOCRCoun,i)) ;
						Log.d(" image ", " image downloader ");
						downloader.response = GalleryImageFragment.this ;
						Log.d(" image ", " image downloader.response ");
						downloader.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR) ;
						Log.d(" image ", " image downloader executed ");
						imageOCRCoun++;

						}

					}


				} catch (JSONException e) {
					// TODO: handle exception
					//Log.d("resume image ", "image resume exception "+e.getMessage()) ;

				}
			}
		}else{
			imageJsonTemp = new JSONArray() ;
		}
		
		imageGlobalCount = imageOCRCoun;
		imageNumberLabel.setText( (imageOCRCoun)+" "+NavigationActivity.getContext().getResources().getString(R.string.gall_photos_added)) ;

 	
	}


	public void addPicture(final Bitmap image){
		ImageView imageView = new ImageView(NavigationActivity.getContext()) ;
		imageView.setImageBitmap(image) ;
		LinearLayout.LayoutParams layoutParams = new LayoutParams(imageContainer.getWidth()/2, imageContainer.getHeight()/2) ;
		imageView.setLayoutParams(layoutParams) ;

		imageView.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				WindowManager wm = (WindowManager) NavigationActivity.getContext().getSystemService(Context.WINDOW_SERVICE);
				Display display  =wm.getDefaultDisplay() ;
				Point point  = new Point() ;
				display.getSize(point) ;
				int screenWidth = point.x ;
				int screenHeight = point.y ;
				LayoutInflater inflater= LayoutInflater.from(NavigationActivity.getContext()) ;
				View previwLayout = inflater.inflate(R.layout.popup_preview_image, null) ;
				final PopupWindow window = new PopupWindow(previwLayout , screenWidth , screenHeight*7/10) ;
				ImageView imageView = (ImageView) previwLayout.findViewById(R.id.gallery_preivew_image) ;
				imageView.setImageBitmap(image) ;
				Button okButton = (Button)previwLayout.findViewById(R.id.gallery_preview_ok_btn) ;
				Button deleteButton = (Button) previwLayout.findViewById(R.id.gallery_preview_delete_btn) ;

				okButton.setOnClickListener(new View.OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						window.dismiss() ;
					}
				}) ;
				deleteButton.setOnClickListener(new View.OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub

					}
				}) ;

				window.showAtLocation(previwLayout, Gravity.CENTER , 0, 0) ;
			}
		});

		if(imageContainer.getChildCount() == 0){
			// create d'un lienar layout qui contient les images
			LinearLayout layout = new LinearLayout(NavigationActivity.getContext()) ;
			//Log.d("image add", "image add child count 0 ") ;
			layout.addView(imageView) ;
			layout.setBackgroundColor(Color.RED) ;
			layout.setLayoutParams(layoutParams) ;
			imageContainer.addView(layout)  ;

		}else{
			// test sur le 1 er enfant is il contient une image ou bien 2
			//Log.d("image add", "image add child count !0 ") ;
			LinearLayout layout = (LinearLayout) imageContainer.getChildAt(0) ;
			if(layout.getChildCount()  == 2){
				//Log.d("image add", "image add child count layout 2") ;
				LinearLayout newlayout = new LinearLayout(NavigationActivity.getContext()) ;
				newlayout.setLayoutParams(layoutParams) ;
				newlayout.addView(imageView) ;
				imageContainer.addView(newlayout)  ;

			}else{

				//Log.d("image add", "image add child count layout ! 2") ;
				layout.addView(imageView) ;

			}
		}
	}

	public String getPath(Uri uri) {
		// just some safety built in
		if( uri == null ) {
			// TODO perform some logging or show user feedback
			return null;
		}
		// try to retrieve the image from the media store first
		// this will only work for images selected from gallery
		String[] projection = { MediaStore.Images.Media.DATA };
		Cursor cursor = NavigationActivity.getContext().getContentResolver().query(uri, projection, null, null, null);
		if( cursor != null ){
			int column_index = cursor
					.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
			cursor.moveToFirst();
			return cursor.getString(column_index);
		}
		// this is our fallback here
		return uri.getPath();
	}

	@Override
	public void processFinish(String result) {
		// TODO Auto-generated method stub



		try {
			//Log.d("process finish gallery ", "process finish gallery "+result) ;
			int id  = Integer.parseInt(result) ;
			if(id!=0){
				if(imageJsonTemp ==  null){
					imageJsonTemp = new JSONArray() ;
				}
				boolean found  = false ;
				for (int i = 0; i < imageJsonTemp.length(); i++) {
					try {
						if(imageJsonTemp.getInt(i) == id){
							found  = true ;
						}
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				if(!found){
					imageJsonTemp.put(id) ;
					organizePictures() ;
				}

			}
		} catch (NumberFormatException e) {
			// TODO: handle exception
		}

	}




}
