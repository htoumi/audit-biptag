package com.biptag.biptag.fragements;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Fragment;
import android.content.Context;
import android.graphics.Point;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.TextView;
import android.widget.LinearLayout.LayoutParams;

import com.biptag.biptag.NavigationActivity;
import com.biptag.biptag.R;
import com.biptag.biptag.dbmanager.datasource.BTFormDataSource;
import com.biptag.biptag.dbmanager.datasource.BTLogDataSource;
import com.biptag.biptag.dbmanager.objects.BTLog;
import com.biptag.biptag.managers.BiptagUserManger;
import com.biptag.biptag.managers.ExpandableListAdapter;

public class NotificationFragment extends Fragment {
	
	public  int contactId ; 
	public String contactName ; 
	public String contactFonction ; 
	public String contactCompany ; 
	TextView nameLBL , workLbl ; 
	List<BTLog> logs ;  
	BTLogDataSource logDataSource ; 
	BTFormDataSource formDataSource ; 
	int agent_id ; 
	int ba_id ; 
	List<HashMap<String, String>> listDataHeader   ; 
	HashMap<String, List<String>> listDataChild   ;
	HashMap<String, String> numberHeaderMatchingMap ; 
	ExpandableListView listView ; 
	ExpandableListAdapter listAdapter ; 
	JSONArray links = new JSONArray() ;   
	JSONArray cards = new JSONArray() ;
	JSONArray files  = new JSONArray() ; 
	View fragmentView ;
	@SuppressLint({"NewApi", "ValidFragment"})
	public NotificationFragment(int contactId ,	String contactName ,	String contactFonction , 
	String contactCompany	) {
		// TODO Auto-generated constructor stub
		this.contactId = contactId ; 
		this.contactName = contactName ; 
		this.contactCompany = contactCompany ; 
		this.contactFonction = contactFonction ; 
		
	}  
	
	public NotificationFragment() {
		// TODO Auto-generated constructor stub
	
		
	
		
		
	} 
	
	
	
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		
		
		
		
		fragmentView = inflater.inflate(R.layout.notification_fragment	,null);
		
		String data = this.getArguments().getString("data") ; 
		if(data != null){
			try {
				JSONObject dataJson = new JSONObject(data) ; 
				
				this.contactId = dataJson.getInt("contact_id") ; 
				this.contactName = dataJson.getString("contact_name") ; 
				this.contactCompany = dataJson.getString("contact_company") ; 
				this.contactFonction= dataJson.getString("contact_fonction") ; 
				
			} catch (JSONException e) {
				// TODO: handle exception
				//Log.d("notif ", "notif init json exception "+e.getMessage() ) ;
			}		
		}
		
		
		logDataSource = new BTLogDataSource(getActivity()) ; 
		formDataSource = new BTFormDataSource(getActivity()) ; 
		String agent_idString = String.valueOf(BiptagUserManger.getUserValue("AGENT_ID", "int", getActivity())) ; 
		String ba_idString = String.valueOf(BiptagUserManger.getUserValue("BA_ID", "int", getActivity())) ; 
		this.agent_id = Integer.parseInt(agent_idString)  ; 
		this.ba_id = Integer.parseInt(ba_idString) ; 
		
		String docs = String.valueOf(BiptagUserManger.getUserValue("docs", "string", getActivity())) ;
			//Log.d("files shared pref", "docs shared pref "+docs) ;
		try {
			JSONObject  documents  = new JSONObject(docs) ; 
			    files = new JSONArray() ; 
				files = documents.getJSONArray("DOC").getJSONObject(0).getJSONArray("FILES") ; 
		} catch (JSONException e) {
			// TODO: handle exception
			//Log.d("log_type", "log_type init files exception "+e.getMessage()) ;
		}
		try {
			JSONObject  documents  = new JSONObject(docs) ; 
			
			links = new JSONArray() ; 
			links = documents.getJSONArray("LINK").getJSONObject(0).getJSONArray("LINKS")  ; 
			
		} catch (Exception e) {
			// TODO: handle exception
			
		}	
		
		try{
			JSONObject  documents  = new JSONObject(docs) ; 
			cards = new JSONArray() ; 
				cards = documents.getJSONArray("CARDS").getJSONObject(0).getJSONArray("CARDS")  ; 
			
			
			
		} catch (JSONException e) {
			// TODO: handle exception
			//Log.d("log_type", "log_type init files exception "+e.getMessage()) ;
		}
		
		//Log.d("notif contact id ", "notif contact id "+this.contactId) ;
		
		logs = logDataSource.findLogsFormContact(this.agent_id, this.contactId)  ; 
		
		listView = (ExpandableListView) fragmentView.findViewById(R.id.expandableListView1) ; 
		prepareListData() ; 
		listAdapter = new ExpandableListAdapter(getActivity(), listDataHeader, listDataChild , numberHeaderMatchingMap);
		// setting list adapter
		listView.setAdapter(listAdapter);
		
		nameLBL = (TextView) fragmentView.findViewById(R.id.notif_contact_name) ; 
		workLbl = (TextView) fragmentView.findViewById(R.id.notif_contact_wrok) ; 
		
		nameLBL.setText(this.contactName) ; 
		workLbl.setText(this.contactFonction+" "+this.contactCompany) ; 
		
		  WindowManager wm = (WindowManager) getActivity().getSystemService(Context.WINDOW_SERVICE);
			Display display  =wm.getDefaultDisplay() ;
			Point point  = new Point() ; 
			display.getSize(point) ; 
			int screenWidth = point.x ; 
			int screenHeight = point.y ;
		Button notif_first_letter_btn = (Button) fragmentView.findViewById(R.id.notif_first_letter_btn) ; 
		LayoutParams layoutParams = (LayoutParams) notif_first_letter_btn.getLayoutParams() ; 
		layoutParams.setMargins(screenWidth/40, 0, screenWidth/40, screenHeight/40) ;
		notif_first_letter_btn.setLayoutParams(layoutParams) ;
		if(!this.contactName.equals("")) 
		notif_first_letter_btn.setText(new String(new char[]{this.contactName.charAt(0)}))  ; 
		
		TextView notif_contact_name = (TextView) fragmentView.findViewById(R.id.notif_contact_name) ; 
		notif_contact_name.setTextSize(getResources().getDimension(R.dimen.form_name_text_size)) ; 
		
		
		
		return fragmentView;
	}
	
	
	private void prepareListData() {
		//Log.d("log details", "log details "+logs.size()) ;
		
		listDataHeader = new ArrayList<HashMap<String, String>>();
		listDataChild = new HashMap<String, List<String>>();
		numberHeaderMatchingMap  = new HashMap<String, String>(); 
		
		// Adding child data
		String elementName  = "" ;
		for (int i = 0; i < logs.size(); i++) {
			BTLog log = logs.get(i) ; 
			//Log.d("log details", "log details "+log.getFl_subject()) ;
			String subject= log.getFl_subject() ; 
			String type = log.getLog_type() ; 
			String timeStamp = log.getLog_timestamps()  ; 
			String content  = log.getFl_content() ; 
			HashMap<String, String> map = new HashMap<String, String>() ; 
			map.put("subject", String.valueOf(i)) ; 
			int actionIdHeader = 0 ; 
			if(type.equals("click")){
				actionIdHeader = R.string.notif_click_action ; 
			}
			if(type.equals("opened")){
				actionIdHeader = R.string.notif_opened_action ; 
			}
			
			if(type.equals("download")){
				actionIdHeader = R.string.notif_download_action; 
			}
			
			map.put("type", getResources().getString(actionIdHeader)) ; 
			map.put("timestamp", timeStamp) ; 
			map.put("content", content) ;
			listDataHeader.add(map) ; 
			
			elementName  = "" ;
			//Log.d("log_type", "log_type '"+log.getLog_type()+"' "+log.getLog_rel_object_2()) ;
			if(log.getLog_type().equals("opened")){
				
				elementName  = NavigationActivity.getContext().getResources().getString(R.string.notif_followup_mail); 
		
			}
			
			try {
					//Log.d("log ", "log  Log_rel_object_2    gob "+log.getLog_rel_object_2()+"  "+log.getLog_rel_object_2_id()) ;
				
				if(log.getLog_rel_object_2().equals("DOC")){
					for (int j = 0; j < files.length(); j++) {
				
						//Log.d("log ", "log  Log_rel_object_2  "+files.getJSONObject(j).getInt("DOC_ID")+"   "+log.getLog_rel_object_2_id() ) ;
						
						if(files.getJSONObject(j).getInt("DOC_ID") == log.getLog_rel_object_2_id()){
							elementName  = files.getJSONObject(j).getString("DOC_NAME") ; 
						}
					}
				}
				if(log.getLog_rel_object_2().equals("LINK")){
					for (int j = 0; j < links.length(); j++) {
						if(links.getJSONObject(j).getInt("LINK_ID") == log.getLog_rel_object_2_id()){
							elementName  = links.getJSONObject(j).getString("LINK_URL") != "" ? links.getJSONObject(j).getString("LINK_URL") : links.getJSONObject(j).getString("LINK_LABEL") ; 
						} 
					}
				}
				
				if(log.getLog_rel_object_2().equals("VCARD")){
					
					
					for (int j = 0; j < cards.length(); j++) {	
						//Log.d("log ", "log  Log_rel_object_2    gob "+log.getLog_rel_object_2_id()) ;
						//Log.d("log ", "log  Log_rel_object_2    gob "+cards.getJSONObject(j).getInt("VCARD_ID")+" "+log.getLog_rel_object_2_id()+" "+(cards.getJSONObject(j).getInt("VCARD_ID") == log.getLog_rel_object_2_id())) ;

						if(cards.getJSONObject(j).getInt("VCARD_ID") == log.getLog_rel_object_2_id()){
							JSONObject vcard_def= cards.getJSONObject(j).getJSONObject("VCARD_DEF") ;
							elementName  = vcard_def.getString("LAST_NAME")+" "+vcard_def.getString("FIRST_NAME"); 
						//	Log.d("log ", "log  Log_rel_object_2    gob element "+cards.getJSONObject(j).getInt("VCARD_ID") +"  "+elementName) ;

						
						}
					}
				
					
					
					
				}
				
				
				
				
			} catch (JSONException e) {
				// TODO: handle exception
				
				//Log.d("log_type", "log Log_rel_object_2 gob exception "+e.getMessage()) ;
			}
			
			List<String> child = new ArrayList<String>() ; 
			//Log.d("log_type", "log_type "+elementName ) ;
			
			int actionId = 0 ; 
			if(type.equals("click")){
				actionId = R.string.notif_click ; 
			}
			if(type.equals("opened")){
				actionId = R.string.notif_opened ; 
			}
			
			if(type.equals("download")){
				actionId = R.string.notif_download; 
			}
			
			
			
			child.add(getActivity().getResources().getString(R.string.notif_prefix)+" "+getActivity().getResources().getString(actionId)+" "+elementName);
			listDataChild.put(String.valueOf(i), child); 
			numberHeaderMatchingMap.put(String.valueOf(i), subject) ; 
			
		
		}
		
				}

}
