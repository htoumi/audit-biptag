package com.biptag.biptag.fragements.settings;

import java.io.File;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Stack;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Fragment;
import android.content.Context;
import android.content.Loader;
import android.graphics.Point;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;

import com.biptag.biptag.NavigationActivity;
import com.biptag.biptag.R;
import com.biptag.biptag.dbmanager.datasource.BTImageDataSource;
import com.biptag.biptag.dbmanager.objects.BTImage;
import com.biptag.biptag.managers.BipTagBitmapLoader;
import com.biptag.biptag.managers.BiptagNavigationManager;
import com.biptag.biptag.managers.BiptagUserManger;

public class SettingAllImagesFragement extends Fragment {

	View fragmentView ;
	JSONObject sortedImagesSync ;
	JSONObject sortedImagesUnSync ;
	LinearLayout  unsync_images_container , sync_images_container ;

	int screenWidth  ;
	int screenHeight ;
	int ba_id  ;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub

		 WindowManager wm = (WindowManager) getActivity().getSystemService(Context.WINDOW_SERVICE);
			Display display  =wm.getDefaultDisplay() ;
			Point point  = new Point() ;
			display.getSize(point) ;
			screenWidth = point.x ;
			screenHeight = point.y ;

		fragmentView = inflater.inflate(R.layout.all_images_layout,null);



	TextView separator = (TextView) fragmentView.findViewById(R.id.sync_separator_0) ;
	LayoutParams  layoutParams = (LayoutParams)separator.getLayoutParams() ;

	layoutParams.setMargins(screenWidth/40,0, screenWidth/40, 0) ;
	separator.setLayoutParams(layoutParams)  ;
	separator.setHeight(screenHeight /480) ;
	//Log.d("screen height", "screen heigth "+screenHeight) ;
	TextView title = (TextView) fragmentView.findViewById(R.id.sync_title_0 ) ;
	//title.setTextSize(screenWidth/20) ;
	title.setTextSize(NavigationActivity.getContext().getResources().getDimension(R.dimen.acc_info_size));
	LayoutParams params = (LayoutParams) title.getLayoutParams() ;
	params.setMargins(screenWidth/40,0, screenWidth/40, 0) ;
	title.setLayoutParams(params) ;




	TextView separator1 = (TextView) fragmentView.findViewById(R.id.sync_seraprator_1) ;
	LayoutParams  layoutParams1 = (LayoutParams)separator1.getLayoutParams() ;

	layoutParams1.setMargins(screenWidth/40,0, screenWidth/40, 0) ;
	separator1.setLayoutParams(layoutParams1)  ;
	separator1.setHeight(screenHeight /480) ;
	//Log.d("screen height", "screen heigth "+screenHeight) ;
	TextView title1 = (TextView) fragmentView.findViewById(R.id.sync_title_1 ) ;
	//title1.setTextSize(screenWidth/20) ;
	title1.setTextSize(NavigationActivity.getContext().getResources().getDimension(R.dimen.acc_info_size));
	LayoutParams params1 = (LayoutParams) title1.getLayoutParams() ;
	params1.setMargins(screenWidth/40,0, screenWidth/40, 0) ;
	title1.setLayoutParams(params1) ;



			sortedImagesSync = new JSONObject()  ;
			sortedImagesUnSync = new JSONObject()  ;

			String ba_id_s= String.valueOf(BiptagUserManger.getUserValue("BA_ID", "int", getActivity().getApplicationContext())) ;
			 ba_id  = Integer.parseInt(ba_id_s) ;
			BTImageDataSource dataSource = new BTImageDataSource(getActivity()) ;
			List<BTImage> list = dataSource.selectAllImages(ba_id) ;

			Log.d(" images liste ", " " + list.size());
			for (int i = 0; i < list.size(); i++) {
				try {
					String date = list.get(i).getImageDate() ;

					if(list.get(i).getIsSent() == 1){
						JSONArray array = new JSONArray() ;
						if(sortedImagesSync.has(date)){
							array =sortedImagesSync.getJSONArray(date) ;
						}
						array.put(list.get(i).getImageId()) ;
						sortedImagesSync.put(date, array) ;

					}


					if(list.get(i).getIsSent() == 0){
						JSONArray array = new JSONArray() ;
						if(sortedImagesUnSync.has(date)){
							array =sortedImagesUnSync.getJSONArray(date) ;
						}
						array.put(list.get(i).getImageId()) ;
						sortedImagesUnSync.put(date, array) ;

					}
				} catch (JSONException e) {
					// TODO: handle exception
				//	Log.d("all image json", "all image json excpetion "+e.getMessage()) ;
				}
			}

			//Log.d("all image json", "all image json sync "+sortedImagesSync.toString()) ;
			//Log.d("all image json", "all image json unsync "+sortedImagesUnSync.toString()) ;

			unsync_images_container  = (LinearLayout ) fragmentView.findViewById(R.id.unsync_images_container) ;
			sync_images_container = (LinearLayout) fragmentView.findViewById(R.id.sync_images_container) ;

		addImagesThumbNail(sortedImagesSync, sync_images_container) ;
		addImagesThumbNail(sortedImagesUnSync, unsync_images_container);



		Button actionBarButton = (Button)	getActivity().getActionBar().getCustomView().findViewById(R.id.formste1_btn_container) ;
		actionBarButton.setBackground(NavigationActivity.getContext().getResources().getDrawable(R.drawable.go_back));

//		actionBarButton.setText(NavigationActivity.getContext().getResources().getString(R.string.return_image_list));

		actionBarButton.setVisibility(View.VISIBLE) ;
		actionBarButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				SettingSynchronisationFragment settingSynchronisationFragment = new SettingSynchronisationFragment();
				BiptagNavigationManager.pushFragments(settingSynchronisationFragment, true, this.getClass().getName(), getActivity(), null, 3);

			}
		}) ;



		return fragmentView;

	}



	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
	//unsync_images_container  = (LinearLayout ) fragmentView.findViewById(R.id.unsync_images_container) ;
	//	sync_images_container = (LinearLayout) fragmentView.findViewById(R.id.sync_images_container) ;

	//	addImagesThumbNail(sortedImagesSync, sync_images_container) ;
		//addImagesThumbNail(sortedImagesUnSync, unsync_images_container);

		Button actionBarButton = (Button)	getActivity().getActionBar().getCustomView().findViewById(R.id.formste1_btn_container) ;
		actionBarButton.setBackground(NavigationActivity.getContext().getResources().getDrawable(R.drawable.go_back));

		//actionBarButton.setText(NavigationActivity.getContext().getResources().getString(R.string.return_image_list));

		actionBarButton.setVisibility(View.VISIBLE) ;




	}


	public void addImagesThumbNail(JSONObject images , LinearLayout scrollView){
		Iterator<String> keys2  = images.keys() ;
		Iterator<String> keys3  = images.keys() ;

ArrayList<Date> key_Inter = new ArrayList<Date>() ;


		while (keys2.hasNext()){
			String key = keys2.next();
			DateFormat formatter ;
			Date date ;
			formatter = new SimpleDateFormat("dd-MM-yyyy");
			try {
				date = (Date)formatter.parse(key);
				key_Inter.add(date);
				Log.d("date "  , date.toString());
			} catch (ParseException e) {
				e.printStackTrace();
			}


		}
		String[]  keys = new String[key_Inter.size()];

		ArrayList<Date> key_date = key_Inter;
		Log.d(" key ", key_Inter.size() + "  ");
		Collections.sort(key_Inter, Collections.reverseOrder());

		for (int j=0;j<key_Inter.size();j++){

			String intMonth = (String) android.text.format.DateFormat.format("MM", key_Inter.get(j)); //06
			String year = (String) android.text.format.DateFormat.format("yyyy", key_Inter.get(j)); //2013
			String day = (String) android.text.format.DateFormat.format("dd", key_Inter.get(j)); //20

			Log.d(" key ", " sorted " +key_Inter.get(j)+" " +day+ "-"+ intMonth + "-" + year );
			keys[j]= day+"-"+intMonth+"-"+year;

		}
		//Hashtable<String, String> vars = images. ;
		LinearLayout layout = new LinearLayout(getActivity()) ;
		LayoutParams layoutParams = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT) ;
		layout.setLayoutParams(layoutParams) ;
		layout.setOrientation(LinearLayout.VERTICAL) ;
		BTImageDataSource source = new BTImageDataSource(getActivity()) ;
		String imageBasePath = getActivity().getCacheDir().getAbsolutePath() ;

	//while (keys.hasNext()) {
for (int j=0; j<keys.length;j++){

		//	String key =String.valueOf( key_Inter.get(j));
				String key = keys[j]+"";
			 Log.d("all image key ", "all image key "+key) ;
			    try {
			        JSONArray array = images.getJSONArray(key) ;

			        TextView view = new TextView(getActivity()) ;
			        view.setText(key) ;
					int textSize  =(int) getResources().getDimension(R.dimen.radio_chech_text_size) ;
			        view.setTextSize(textSize * 2);

			        LayoutParams textLayoutParams =new LayoutParams(screenWidth, screenHeight/15) ;
			        view.setLayoutParams(textLayoutParams) ;
			        scrollView.addView(view) ;
			        int imageCounter  =  0 ;

			        LinearLayout row = new  LinearLayout(getActivity()) ;
			        LayoutParams rowLayoutParams =  new LayoutParams(screenWidth, screenHeight/7) ;
			        row.setLayoutParams(rowLayoutParams) ;
			        scrollView.addView(row) ;
			        for (int i = 0; i < array.length(); i++) {
			        	int imageId   = array.getInt(i) ;
			        	//Log.d("all image json", "all image json image id "+imageId) ;
			        	BTImage image = source.selectImagesWithId(imageId, ba_id) ;
						Log.d("image size ", "" +image.getImageName() + " size " + images.length());
			        	if(image!=  null){
							String imagePath = imageBasePath+"/"+image.getImageName()+".jpg" ;

							LinearLayout lastRow = (LinearLayout ) scrollView.getChildAt(scrollView.getChildCount()-1) ;


							//Log.d("all image json", "all image json image db not null "+imagePath) ;

								ImageView imageView  = new ImageView(getActivity()) ;
								imageView.setBackgroundColor(0xffffffff);
								LayoutParams imageViewLayoutParams = new LayoutParams(screenWidth/4, LayoutParams.MATCH_PARENT ) ;
								imageView.setLayoutParams(imageViewLayoutParams) ;
								imageViewLayoutParams.setMargins(screenWidth / 45, screenHeight / 50, screenWidth / 45, screenHeight / 50);
								lastRow.addView(imageView) ;
								Log.d("image " , " image number " + i + "/" + array.length());
								BipTagBitmapLoader loader = new BipTagBitmapLoader(imagePath, getActivity(), imageView , null) ;

							Calendar c = Calendar.getInstance();
							int second = c.get(Calendar.MILLISECOND);
							Log.d("size bitmap ", "loaded " + second);
							loader.execute() ;Calendar
									c2 = Calendar.getInstance();
							int second2 = c2.get(Calendar.MILLISECOND);
							Log.d("size bitmap ",  "executed at " + second2);
								imageCounter ++  ;
					        	//d("all image json", "all image json image  graphic added ") ;
							Log.d("size bitmap ", " imagecounter"+ imageCounter);
								if(imageCounter> 2){
//						        	Log.d("all image json", "all image json image  graphic added image view  ") ;
									Log.d("size bitmap ", " imagecounter >2 "+ imageCounter);
									imageCounter  =  0 ;
							        row = new  LinearLayout(getActivity()) ;
							        row.setLayoutParams(rowLayoutParams) ;
							        scrollView.addView(row) ;
								}




			        	}else{
							Log.d("size Image ", " else date" +  key );
			        		//Log.d("all image json", "all image json image null "+imageId) ;
			        	}

			        }


			    } catch (JSONException e) {
			        // Something went wrong!
					//Log.d("all image json", "all image json image excpetion"+e.getMessage()) ;


			    }

		}

	}



	@Override
	public void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
/*
		if(unsync_images_container != null){
			unsync_images_container.removeAllViews() ;
			unsync_images_container = null;
		}

		if(sync_images_container != null){
			sync_images_container.removeAllViews() ;
			sync_images_container = null;
		}
*/
		Button actionBarButton = (Button)	getActivity().getActionBar().getCustomView().findViewById(R.id.formste1_btn_container) ;
		actionBarButton.setBackground(NavigationActivity.getContext().getResources().getDrawable(R.drawable.go_back));

//		actionBarButton.setText(NavigationActivity.getContext().getResources().getString(R.string.return_image_list));

		actionBarButton.setVisibility(View.INVISIBLE) ;


	}


}
