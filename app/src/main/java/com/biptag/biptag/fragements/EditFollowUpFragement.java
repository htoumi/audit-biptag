
package com.biptag.biptag.fragements;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.R.integer;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.database.Cursor;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.drawable.GradientDrawable;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v4.widget.DrawerLayout;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import com.biptag.biptag.NavigationActivity;
import com.biptag.biptag.R;
import com.biptag.biptag.CustumViews.TemplateListAdapter;
import com.biptag.biptag.dbmanager.datasource.BTFollowupTemplateDataSource;
import com.biptag.biptag.dbmanager.objects.BTFollowupTemplate;
import com.biptag.biptag.managers.BiptagNavigationManager;
import com.biptag.biptag.managers.BiptagUserManger;
import com.biptag.biptag.webservices.BTMailSender;
import com.biptag.biptag.webservices.templates.SaveTemplate;

public class EditFollowUpFragement extends Fragment  {
	EditText subject , email , content , ccEmailsFields , cciEmailFields;
	TextView cciSeperator ;
	Button  cancel , validate , showTemplate , saveTemplate ;
	List<BTFollowupTemplate> templatesList ;
	BTFollowupTemplateDataSource dataSource ;
	ListView drawerList ;
	LinearLayout drawermenu  ;
	View emailpropView ;
	DrawerLayout drawerLayout ;
	ListView listView ;
	PopupWindow popupWindow ;
	int screenWidth ;
	int screenHeight ;
	PopupWindow sideBarWindow ;
	int mode ;
	String contactEmail ;
	int contact_id ;
	JSONArray ccEmailJson  ;
	JSONArray cciEmailJson ;
	int stack = 1 ;
	int x =0 ;
	int y = 0 ;

	String FU_CONTENT_TEMPS, FU_SUBJECT_TEMPS,selected_template,selected_template_local,FU_CONTENT,FU_SUBJECT;
	boolean didCreateTemplate = false ;
	boolean didEditTemplate = false ;
 	String local_ID;

	public View fragement  ;
	public EditFollowUpFragement() {
		// TODO Auto-generated constructor stub
	}
	@SuppressLint({"NewApi", "ValidFragment"})
	public EditFollowUpFragement(int stack) {
		// TODO Auto-generated constructor stub
		ccEmailJson = new JSONArray() ;
		cciEmailJson = new JSONArray() ;
	}

	@SuppressLint({"NewApi", "ValidFragment"})
	public EditFollowUpFragement(int mode , String contactEmail , int contact_id , int stack ){
		ccEmailJson = new JSONArray() ;
		cciEmailJson = new JSONArray() ;
	}



	public void onStop() {

		super.onStop() ;
		if(popupWindow != null){
			popupWindow.dismiss() ;
			popupWindow = null;
		}

		if(sideBarWindow != null){
			sideBarWindow.dismiss() ;
			sideBarWindow = null;
		}

		Button actionBarButton = (Button)	getActivity().getActionBar().getCustomView().findViewById(R.id.formste1_btn_container) ;
		actionBarButton.setOnClickListener(null) ;
		TextView action_bar_title_label = (TextView) getActivity().getActionBar().getCustomView().findViewById(R.id.action_bar_title_label) ;
		action_bar_title_label.setOnTouchListener(null) ;
		actionBarButton.setVisibility(View.INVISIBLE) ;

	};





	public void ccFieldAddValue(String text) {
		// TODO Auto-generated method stub

		String newCcContent =  ccEmailsFields.getText().toString() ;
		String content = "" ;
		String[] emails = newCcContent.split(" ") ;
		if(emails.length >1){
			for (int i = 0; i < emails.length - 1; i++) {
				content+=emails[i]+" " ;
			}
		}
		content+=text+" " ;

		ccEmailsFields.setText(content) ;
		ccEmailsFields.setSelection(ccEmailsFields.getText().toString().length()) ;
		popupWindow.dismiss() ;
	}



	public void cciFieldAddValue(String text) {
		// TODO Auto-generated method stub

		String newCcContent =  cciEmailFields.getText().toString() ;
		String content = "" ;
		String[] emails = newCcContent.split(" ") ;
		if(emails.length >1){
			for (int i = 0; i < emails.length - 1; i++) {
				content+=emails[i]+" " ;
			}
		}
		content+=text+" " ;
		cciEmailFields.setText(content) ;
		cciEmailFields.setSelection(cciEmailFields.getText().toString().length()) ;


		popupWindow.dismiss() ;
	}

	public void fixLayout(View view ){
		LinearLayout edit_follow_up_text_container = (LinearLayout) view.findViewById(R.id.edit_follow_up_text_container) ;

		GradientDrawable border = new GradientDrawable();
		border.setColor(0xfafafa); //white background
		border.setStroke(3, 0xdadadf); //black border with full opacity
		if(Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
			edit_follow_up_text_container.setBackgroundDrawable(border);
		} else {
			edit_follow_up_text_container.setBackground(border);
		}
		WindowManager wm = (WindowManager) getActivity().getSystemService(Context.WINDOW_SERVICE);
		Display display  =wm.getDefaultDisplay() ;
		Point point  = new Point() ;
		display.getSize(point) ;
		int screenWidth = point.x ;
		int screenHeight = point.y ;
		LinearLayout.LayoutParams params = (android.widget.LinearLayout.LayoutParams) edit_follow_up_text_container.getLayoutParams() ;
		params.setMargins(screenWidth / 30, screenHeight / 30, screenWidth / 30, screenHeight / 30) ;
		edit_follow_up_text_container.setLayoutParams(params) ;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
							 Bundle savedInstanceState) {
		// TODO Auto-generated method stub



		String dataString = getArguments().getString("data") ;
		Log.d("edit follow argument ", "edit followup argumetn "+dataString) ;

		try {
			JSONObject data  = new JSONObject(dataString) ;
			mode = data.getInt("mode") ;
			stack = data.getInt("stack")  ;
			contactEmail = data.getString("email") ;
			contact_id = data.getInt("id") ;
		} catch (JSONException e) {
			// TODO: handle exception

			Log.d("follow up ", "follow up init params exception  "+e.getMessage()) ;


		}



		String savedContactEmail = String.valueOf(BiptagUserManger.getUserValue("contact_email", "string", getActivity())) ;
		if(this.mode == 0 && !savedContactEmail.equals("")){
			contactEmail =  savedContactEmail ;
		}

		String savedContactEmailEdit = String.valueOf(BiptagUserManger.getUserValue("contact_email_edit", "string", getActivity())) ;
		if(this.mode == 1 && !savedContactEmailEdit.equals("")){
			contactEmail =  savedContactEmailEdit ;
		}

		fragement  = inflater.inflate(R.layout.followup_fragment_layout, null) ;
		email= (EditText) fragement.findViewById(R.id.editfollowup_destinataire) ;
		Log.d("edit follow argument ", "edit followup argumetn "+contactEmail) ;

		email.setText(contactEmail) ;
		email.setFocusableInTouchMode(false) ;

		String fu_subject ="";
		String fu_content ="" ;
		local_ID = String.valueOf(BiptagUserManger.getUserValue("selected_template", "string", getActivity()));

		int  selectedTemplateId =0 ;

		Log.d("selected template ", "selected template "+String.valueOf(BiptagUserManger.getUserValue("selected_template", "string", getActivity()))) ;
		try {
			selectedTemplateId = Integer.parseInt(String.valueOf(BiptagUserManger.getUserValue("selected_template", "string", getActivity()))) ;

		} catch (NumberFormatException e) {
			// TODO: handle exception
		}


		if(selectedTemplateId != 0){
			int ba_id = Integer.parseInt(String.valueOf(BiptagUserManger.getUserValue("BA_ID", "int", getActivity()))) ;

			Log.d("selected teplate crash" , "selected teplate crash "+ba_id+" "+selectedTemplateId) ;

			BTFollowupTemplateDataSource dataSource = new BTFollowupTemplateDataSource(getActivity()) ;
			BTFollowupTemplate followupTemplate =dataSource.getTemplateWithLocalID(ba_id, selectedTemplateId) ;
			String value = String.valueOf(BiptagUserManger.getUserValue("validate", "string", getActivity())) ;
			if (value.indexOf("1")!=-1){
				fu_content = (String) BiptagUserManger.getUserValue("FU_CONTENT", "string", getActivity()) ;
				fu_subject = (String) BiptagUserManger.getUserValue("FU_SUBJECT", "string", getActivity()) ;
			}else {
				fu_content = followupTemplate.getFu_content();
				fu_subject = followupTemplate.getFu_subject() ;
			}


		}else{

			fu_subject = (String) BiptagUserManger.getUserValue("FU_SUBJECT", "string", getActivity()) ;
			fu_content = (String) BiptagUserManger.getUserValue("FU_CONTENT", "string", getActivity()) ;

		}


		subject = (EditText) fragement.findViewById(R.id.editfollowup_subject) ;


		subject.addTextChangedListener(new TextWatcher() {
			String originalSubject ;


			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
				if(subject.getText().toString().length() !=0){
					if(subject.getText().toString().charAt(subject.getText().toString().length()-1) =='\n'){
						Log.d("edit follow up keycode", "edit follow up keycode enter") ;
						subject.setText(originalSubject) ;
					}
				}

			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
										  int after) {
				// TODO Auto-generated method stub
				originalSubject = subject.getText().toString() ;
				if(originalSubject.length()!=0){
					originalSubject = originalSubject.replaceAll("\\n","");
				}

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub

				originalSubject = subject.getText().toString() ;
				subject.setSelection(subject.getText().toString().length()) ;
			}
		})   ;

		content = (EditText) fragement.findViewById(R.id.editfollowup_content) ;
		email.setEnabled(false);

		cancel=(Button) fragement.findViewById(R.id.editfollowup_cancel) ;
		validate = (Button) fragement.findViewById(R.id.editfollowup_valdiate) ;
		drawermenu = (LinearLayout) fragement.findViewById(R.id.drawermenu) ;
		drawerLayout = (DrawerLayout) fragement.findViewById(R.id.drawerLayout) ;


		fixLayout(fragement) ;


		/*showTemplate = (Button) fragement.findViewById(R.id.show_template_btn) ;
		showTemplate.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				showTemplateMenu() ;
			}
		});
		  */
		subject.setText(fu_subject) ;
		content.setText(fu_content)  ;

		cancel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				//finish() ;
				fragmentWillDisappear() ;
				BiptagUserManger.saveUserData("selected_template", local_ID, NavigationActivity.getContext());

				if(mode ==0 )
					BiptagNavigationManager.pushFragments(EditFollowUpFragement.this, false, "edit_follow_up", getActivity(), null, stack) ;
				else
					BiptagNavigationManager.pushFragments(EditFollowUpFragement.this, false, "send_email", getActivity(), null, stack) ;



			}
		});

		validate.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub


				Log.d("validate follow up ", "validate follow up "+mode) ;

				fragmentWillDisappear() ;


			//	if(!didCreateTemplate)
			//		BiptagUserManger.saveUserData("selected_template", "", getActivity()) ;


				cciEmailJson = new JSONArray()  ;
				String[] emailsCCi  = cciEmailFields.getText().toString().split(" ");
				for (int i = 0; i < emailsCCi.length; i++) {
					if(isValidEmail(emailsCCi[i])){
						cciEmailJson.put(emailsCCi[i]) ;
					}
				}

				ccEmailJson = new JSONArray()  ;
				String[] emailsCC  = ccEmailsFields.getText().toString().split(" ");
				for (int i = 0; i < emailsCC.length; i++) {
					if(isValidEmail(emailsCC[i])){
						ccEmailJson.put(emailsCC[i]) ;
					}
				}

				if(mode == 2){
					BTMailSender  mailSender = new BTMailSender(getActivity(), content.getText().toString(), subject.getText().toString(),contactEmail , ccEmailJson.toString(), cciEmailJson.toString(), String.valueOf(contact_id)) ;
					mailSender.execute() ;
					BiptagNavigationManager.pushFragments(EditFollowUpFragement.this, false, "send_email", getActivity(), null, stack) ;

				}else{
					Log.d("follow up save cci ", "follow up save cci "+cciEmailJson.toString()) ;
					Log.d("follow up save cci ", "follow up save cc "+ccEmailJson.toString()) ;
					BiptagUserManger.saveUserData("FU_CCIEMAILS_TEMPS", cciEmailJson.toString(), getActivity()) ;
					BiptagUserManger.saveUserData("FU_CCEMAILS_TEMPS", ccEmailJson.toString(), getActivity()) ;
					BiptagUserManger.saveUserData("FU_CONTENT_TEMPS", content.getText().toString(), getActivity()) ;
					BiptagUserManger.saveUserData("FU_SUBJECT_TEMPS", subject.getText().toString(), getActivity()) ;
					BiptagUserManger.saveUserData("FU_CONTENT", content.getText().toString(), NavigationActivity.getContext());
					BiptagUserManger.saveUserData("FU_SUBJECT", subject.getText().toString(), NavigationActivity.getContext());
					BiptagUserManger.saveUserData("validate", "1", NavigationActivity.getContext());
					BiptagNavigationManager.pushFragments(EditFollowUpFragement.this, false, "edit_follow_up", getActivity(), null, stack) ;
					Log.d(" followup ", " subject " + subject.getText().toString() + " content " + content.getText().toString() + " ok");
					Log.d(" followup ", " content " +content.getText().toString().length() +" "+ content.getText().toString() + " ok");

				}
				if (didEditTemplate) {
					BiptagUserManger.saveUserData("FU_CONTENT_TEMPS", FU_CONTENT_TEMPS, NavigationActivity.getContext());
					BiptagUserManger.saveUserData("FU_SUBJECT_TEMPS", FU_SUBJECT_TEMPS, NavigationActivity.getContext());
					//BiptagUserManger.saveUserData("selected_template", selected_template, NavigationActivity.getContext());
					BiptagUserManger.saveUserData("FU_CONTENT", FU_CONTENT, NavigationActivity.getContext());
					BiptagUserManger.saveUserData("FU_SUBJECT", FU_SUBJECT, NavigationActivity.getContext());
				}


			}
		}) ;

		String ba_id = String.valueOf(BiptagUserManger.getUserValue("BA_ID", "int", getActivity())) ;

		dataSource = new BTFollowupTemplateDataSource(getActivity()) ;
		templatesList = dataSource.getAllTempalte(Integer.parseInt(ba_id)) ;


		for (int i = 0; i < templatesList.size(); i++) {
			Log.d("tempalte item", "tempalte item name "+templatesList.get(i).getTemp_name()) ;
		}


		saveTemplate = (Button) fragement.findViewById(R.id.editfollowup_saveastemp ) ;
		saveTemplate.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				AlertDialog.Builder builder = new AlertDialog.Builder(getActivity()) ;
				builder.setTitle(NavigationActivity.getContext().getResources().getString(R.string.save_as_tmpl_name_title)) ;
				final EditText editText = new EditText(getActivity()) ;
				builder.setMessage(NavigationActivity.getContext().getResources().getString(R.string.save_as_tmpl_name_title)) ;
				builder.setView(editText) ;
				builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						String subjectString  = subject.getText().toString() ;
						String contentStirng = content.getText().toString() ;
						String tempName = editText.getText().toString() ;
						if(tempName.length() != 0){
							JSONArray attachement = new JSONArray() ;
							String ba_id = String.valueOf(BiptagUserManger.getUserValue("BA_ID", "int", getActivity())) ;

							BTFollowupTemplate followupTemplate  = new BTFollowupTemplate()  ;
							followupTemplate.setTemp_name(tempName) ;
							followupTemplate.setFu_attach(attachement) ;
							followupTemplate.setFu_content(contentStirng) ;
							followupTemplate.setFu_subject(subjectString) ;
							followupTemplate.setSynchonized(false) ;
							followupTemplate.setFu_id(0) ;
							followupTemplate.setFu_hashcode("") ;
							followupTemplate.setBa_id(Integer.parseInt(ba_id)) ;

							Log.d("ba id " , "ba id "+ba_id) ;
							BTFollowupTemplateDataSource dataSource  = new BTFollowupTemplateDataSource(getActivity()) ;

							long insertId   =dataSource.insertTemplate(followupTemplate) ;
							followupTemplate.setLocal_id(insertId) ;


							BiptagUserManger.saveUserData("FU_CONTENT", followupTemplate.getFu_content(), NavigationActivity.getContext()) ;
							BiptagUserManger.saveUserData("FU_SUBJECT", followupTemplate.getFu_subject(), NavigationActivity.getContext()) ;


							BiptagUserManger.saveUserData("FU_CONTENT_TEMPS", followupTemplate.getFu_content(), NavigationActivity.getContext()) ;
							BiptagUserManger.saveUserData("FU_SUBJECT_TEMPS", followupTemplate.getFu_subject(), NavigationActivity.getContext()) ;
							BiptagUserManger.saveUserData("selected_template", String.valueOf(insertId), NavigationActivity.getContext()) ;
							// ddddddddddddddddddddd
							didCreateTemplate = true ;

							SaveTemplate saveTemplate = new SaveTemplate(getActivity(), followupTemplate) ;
							saveTemplate.execute() ;

						}else{
							AlertDialog.Builder builder = new AlertDialog.Builder(getActivity()) ;
							builder.setTitle(NavigationActivity.getContext().getResources().getString(R.string.save_as_tmpl_name_empty_title)) ;
							builder.setMessage(NavigationActivity.getContext().getResources().getString(R.string.save_as_tmpl_name_empty_msg)) ;
							builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {

								@Override
								public void onClick(DialogInterface dialog, int which) {
									// TODO Auto-generated method stub
									dialog.dismiss() ;
								}
							}) ;
							builder.show() ;
						}

					}
				}) ;

				builder.setNegativeButton(NavigationActivity.getContext().getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						dialog.dismiss() ;
					}
				});
				builder.show() ;


			}
		}) ;
		/*
		drawerList = (ListView) fragement.findViewById(R.id.drawerList);
		drawerList.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
				BTFollowupTemplate template= templatesList.get(position) ;
				content.setText(template.getFu_content()) ;
				subject.setText(template.getFu_subject()) ;
				BiptagUserManger.saveUserData("FU_CONTENT_TEMPS", content.getText().toString(), getActivity()) ;
				BiptagUserManger.saveUserData("FU_SUBJECT_TEMPS", subject.getText().toString(), getActivity()) ;

			}
		});
		*/
		ccEmailsFields = (EditText) fragement.findViewById(R.id.ccemails_field) ;
		cciEmailFields = (EditText) fragement.findViewById(R.id.cciemails_field) ;
		cciSeperator = (TextView) fragement.findViewById(R.id.cci_text_separator) ;
		final List<Map<String, String>> data = getContactEmailsList(getActivity().getContentResolver()) ;
		emailpropView = inflater.inflate(R.layout.email_proposition_view	, null) ;
		WindowManager wm = (WindowManager) getActivity().getSystemService(Context.WINDOW_SERVICE);
		Display display  =wm.getDefaultDisplay() ;
		Point point  = new Point() ;
		display.getSize(point) ;
		screenWidth = point.x ;
		screenHeight = point.y ;






		ccEmailsFields.setOnFocusChangeListener(new OnFocusChangeListener() {

			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				// TODO Auto-generated method stub

				if(ccEmailsFields.hasFocus()){

					if(popupWindow !=  null){
						popupWindow.dismiss() ;
					}
					int maxscreenheight = 0;
					if (data.size()>5){
						maxscreenheight = screenHeight/4;
					}else{
						maxscreenheight = 70*data.size();
					}


					cciEmailFields.setVisibility(View.VISIBLE) ;
					cciSeperator.setVisibility(View.VISIBLE) ;
					popupWindow = new PopupWindow(emailpropView , screenWidth*2/3 , maxscreenheight) ;
					SimpleAdapter adapter = new SimpleAdapter(getActivity(), data,
							android.R.layout.simple_list_item_2,
							new String[] {"name", "email"},
							new int[] {android.R.id.text1,
									android.R.id.text2});
					listView = (ListView) emailpropView.findViewById(R.id.emailsList)  ;
					listView.setAdapter(adapter) ;


					listView.setOnTouchListener(new View.OnTouchListener() {

						@Override
						public boolean onTouch(View v, MotionEvent event) {
							// TODO Auto-generated method stub
							if(event.getAction() == MotionEvent.ACTION_DOWN){
								x = (int) event.getX() ;
								y = (int) event.getY() ;
							}
							int localX = 0 ;
							int localY =0  ;
							if(event.getAction() ==  MotionEvent.ACTION_UP){
								localX = (int) event.getX() ;
								localY= (int) event.getY() ;
							}

							Log.d("most used ", "most used touch listener triggered "+x+" "+localX+"  "+y+"  "+localY) ;
							Rect rect = null ;
							if(x ==  localX && y== localY ){
								for (int j = 0; j < listView.getChildCount(); j++) {
									View view= listView.getChildAt(j) ;
									rect = new Rect((int)view.getX() , (int)view.getY() ,(int) view.getX()+ view.getWidth() ,(int) view.getY()+ view.getHeight()) ;
									if(rect.contains(x, y)){
										TextView  txt = (TextView) view.findViewById(android.R.id.text2) ;
										Log.d("most used ", "most used  touch listener found item "+txt.getText().toString()) ;

										ccFieldAddValue(txt.getText().toString()) ;

										break ;

									}

								}
							}


							return false;
						}
					}) ;


					if(data.size() != 0 ){
						popupWindow.showAsDropDown(ccEmailsFields)  ;

					}

				}else{
					popupWindow.dismiss() ;


				}

			}
		}) ;

		ccEmailsFields.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub

				List<Map<String, String>> data = getContactEmailsList(getActivity().getContentResolver()) ;
				List<Map<String, String>> dataFilter = new ArrayList<Map<String,String>>() ;

				String[] ccData = ccEmailsFields.getText().toString().split(" ") ;
				for (int i = 0; i < ccData.length; i++) {
					Log.d("user input", "user input "+ccData[i])   ;
				}
				String  userInput = "" ;
				if(ccData.length != 0){
					userInput= ccData[ccData.length -1] ;
				}else{
					userInput = ccEmailsFields.getText().toString() ;
				}
				Log.d("user input ", "user input  "+userInput) ;
				for (int i = 0; i < data.size(); i++) {
					if(data.get(i).get("name").toUpperCase().indexOf(userInput.toUpperCase()) == 0 || data.get(i).get("email").toUpperCase().indexOf(userInput.toUpperCase()) == 0){
						dataFilter.add(data.get(i)) ;
					}
				}

				if(dataFilter.size()  == 0){
					if(popupWindow != null)
						popupWindow.dismiss() ;
				}else{
					SimpleAdapter adapter = new SimpleAdapter(getActivity(), dataFilter,
							android.R.layout.simple_list_item_2,
							new String[] {"name", "email"},
							new int[] {android.R.id.text1,
									android.R.id.text2});
					if(listView != null)
						listView.setAdapter(adapter) ;
					if(popupWindow != null)
						if(!popupWindow.isShowing()){
							popupWindow.showAsDropDown(ccEmailsFields) ;
						}
				}
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
										  int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub

			}
		})  ;

		cciEmailFields.setOnFocusChangeListener(new OnFocusChangeListener() {

			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				// TODO Auto-generated method stub
				if(cciEmailFields.hasFocus()){

					if(popupWindow !=  null){
						popupWindow.dismiss() ;
					}
					popupWindow = new PopupWindow(emailpropView , screenWidth*2/3 , screenHeight/4) ;
					SimpleAdapter adapter = new SimpleAdapter(getActivity(), data,
							android.R.layout.simple_list_item_2,
							new String[] {"name", "email"},
							new int[] {android.R.id.text1,
									android.R.id.text2});
					listView = (ListView) emailpropView.findViewById(R.id.emailsList)  ;
					listView.setAdapter(adapter) ;
					listView.setOnTouchListener(new View.OnTouchListener() {

						@Override
						public boolean onTouch(View v, MotionEvent event) {
							// TODO Auto-generated method stub
							if(event.getAction() == MotionEvent.ACTION_DOWN){
								x = (int) event.getX() ;
								y = (int) event.getY() ;
							}
							int localX = 0 ;
							int localY =0  ;
							if(event.getAction() ==  MotionEvent.ACTION_UP){
								localX = (int) event.getX() ;
								localY= (int) event.getY() ;
							}

							Log.d("most used ", "most used touch listener triggered "+x+" "+localX+"  "+y+"  "+localY) ;
							Rect rect = null ;
							if(x ==  localX && y== localY ){
								for (int j = 0; j < listView.getChildCount(); j++) {
									View view= listView.getChildAt(j) ;
									rect = new Rect((int)view.getX() , (int)view.getY() ,(int) view.getX()+ view.getWidth() ,(int) view.getY()+ view.getHeight()) ;
									if(rect.contains(x, y)){
										TextView  txt = (TextView) view.findViewById(android.R.id.text2) ;
										Log.d("most used ", "most used  touch listener found item "+txt.getText().toString()) ;

										cciFieldAddValue(txt.getText().toString()) ;

										break ;

									}

								}
							}


							return false;
						}
					}) ;


					if(data.size() != 0 ){
						popupWindow.showAsDropDown(cciEmailFields)  ;

					}
				}else{
					popupWindow.dismiss() ;
				}
			}
		}) ;

		cciEmailFields.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub

				// TODO Auto-generated method stub

				List<Map<String, String>> data = getContactEmailsList(getActivity().getContentResolver()) ;
				List<Map<String, String>> dataFilter = new ArrayList<Map<String,String>>() ;

				String[] ccData = cciEmailFields.getText().toString().split(" ") ;
				String  userInput = "" ;
				if(ccData.length != 0){
					userInput= ccData[ccData.length -1] ;
				}else{
					userInput = cciEmailFields.getText().toString() ;
				}
				for (int i = 0; i < data.size(); i++) {
					if(data.get(i).get("name").toUpperCase().indexOf(userInput.toUpperCase()) == 0 || data.get(i).get("email").toUpperCase().indexOf(userInput.toUpperCase()) == 0){
						dataFilter.add(data.get(i)) ;
					}
				}

				if(dataFilter.size()  == 0){
					if(popupWindow != null)
						popupWindow.dismiss() ;
				}else{
					SimpleAdapter adapter = new SimpleAdapter(getActivity(), dataFilter,
							android.R.layout.simple_list_item_2,
							new String[] {"name", "email"},
							new int[] {android.R.id.text1,
									android.R.id.text2});
					if(listView != null)
						listView.setAdapter(adapter) ;
					if(popupWindow != null)
						if(!popupWindow.isShowing()){
							popupWindow.showAsDropDown(cciEmailFields) ;
						}
				}

			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
										  int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub

			}
		}) ;

		Button actionBarButton = (Button)	getActivity().getActionBar().getCustomView().findViewById(R.id.formste1_btn_container) ;
		actionBarButton.setBackground(NavigationActivity.getContext().getResources().getDrawable(R.drawable.action_bar_btn));
		actionBarButton.setVisibility(View.VISIBLE) ;
		actionBarButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
				imm.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);

				showTemplateMenu() ;
			}
		}) ;
		TextView action_bar_title_label = (TextView) getActivity().getActionBar().getCustomView().findViewById(R.id.action_bar_title_label) ;
		action_bar_title_label.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub

				if(event.getAction()== MotionEvent.ACTION_DOWN){
					showTemplateMenu() ;
				}

				return false;
			}
		}) ;
		return fragement ;



	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();


		String tempCCiEmailString  = String.valueOf(BiptagUserManger.getUserValue("FU_CCIEMAILS_TEMPS", "string", getActivity()));
		String tempCCEmailString  = String.valueOf(BiptagUserManger.getUserValue("FU_CCEMAILS_TEMPS", "string", getActivity())) ;
		Log.d("eidt follow up mail", "edit follow up mail "+tempCCEmailString) ;
		Log.d("eidt follow up mail", "edit follow up mail "+tempCCiEmailString) ;
		if(!tempCCEmailString.equals("")){

			try {
				JSONArray tempCCEmail = new JSONArray(tempCCEmailString) ;
				ccEmailsFields.setText("") ;
				String content = "" ;
				for (int i = 0; i < tempCCEmail.length(); i++) {
					content += tempCCEmail.getString(i)+" " ;
				}
				ccEmailsFields.setText(content) ;
			} catch (JSONException e) {
				// TODO: handle exception
				Log.d("edit follow up mail ", "edit follow up mail "+e.toString()) ;
			}

		}

		if(popupWindow != null){
			popupWindow.dismiss();
		}
		Button actionBarButton = (Button)	getActivity().getActionBar().getCustomView().findViewById(R.id.formste1_btn_container);
		actionBarButton.setBackground(NavigationActivity.getContext().getResources().getDrawable(R.drawable.action_bar_btn));
		actionBarButton.setVisibility(View.VISIBLE);

		actionBarButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
				imm.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
				showTemplateMenu();
			}
		}) ;
	}


	public List<Map<String, String>> getContactEmailsList(ContentResolver cr){
		List<Map<String, String>> list = new ArrayList<Map<String,String>>();

		String coWorkerString = String.valueOf(BiptagUserManger.getUserValue("CO-WORKERS", "string", getActivity()));
		try {
			JSONArray coWorker  = new JSONArray(coWorkerString) ;
			Log.d("get proposition", "get proposition coWorker  "+coWorker.toString()) ;
			for (int i = 0; i < coWorker.length(); i++) {
				String name = coWorker.getJSONObject(i).getString("FNAME")+" "+coWorker.getJSONObject(i).getString("LNAME") ;
				String email =  coWorker.getJSONObject(i).getString("EMAIL") ;
				Map<String, String> map  = new HashMap<String, String>() ;
				map.put("name", name) ;
				map.put("email", email) ;
				list.add(map) ;

			}

		} catch (JSONException e) {
			// TODO: handle exception
			Log.d("get proposition", "get proposition  "+e.getMessage()) ;
		}



		Cursor cursor = cr.query(ContactsContract.CommonDataKinds.Email.CONTENT_URI	, null,null, null ,null, null) ;
		while(cursor.moveToNext()){
			String name = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME)) ;
			String email = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Email.ADDRESS)) ;
			Map<String, String> map  = new HashMap<String, String>() ;
			map.put("name", name) ;
			map.put("email", email) ;
			list.add(map) ;
		}

		return list ;

	}


	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		//setContentView(R.layout.activity_edit_follow_up_mail);



	}
	public void showTemplateMenu(){

		String ba_id = String.valueOf(BiptagUserManger.getUserValue("BA_ID", "int", getActivity())) ;
		templatesList = dataSource.getAllTempalte(Integer.parseInt(ba_id)) ;


		List<String> templatesNamesList = new ArrayList<String>() ;
		List<JSONArray > templatesAttachementList = new ArrayList<JSONArray>() ;
		List<String> templateIds = new ArrayList<String>() ;




		for (int i = 0; i < templatesList.size(); i++) {
			templatesNamesList.add(templatesList.get(i).getTemp_name()) ;
			templatesAttachementList.add(templatesList.get(i).getFu_attach()) ;
			templateIds.add(String.valueOf(templatesList.get(i).getLocal_id())) ;

		}
		TemplateListAdapter adapter = new TemplateListAdapter(getActivity(), R.layout.drawer_list_item, templatesNamesList, templatesAttachementList , templateIds) ;
		LayoutInflater inflater= LayoutInflater.from(getActivity()) ;
		View sideBarView = inflater.inflate(R.layout.followup_side_bar_menu	, null) ;
		final ListView tempalteList = (ListView) sideBarView.findViewById(R.id.followup_sidebar_list) ;
		tempalteList.setAdapter(adapter) ;

		tempalteList.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
				if(event.getAction() == MotionEvent.ACTION_DOWN){
					x = (int) event.getX() ;
					y = (int) event.getY() ;
				}
				int localX = 0 ;
				int localY =0  ;
				if(event.getAction() ==  MotionEvent.ACTION_UP){
					localX = (int) event.getX() ;
					localY= (int) event.getY() ;
				}

				Log.d("most used ", "most used touch listener triggered "+x+" "+localX+"  "+y+"  "+localY) ;

				if(localX ==x || localY==y ){
					Rect rect = null ;
					for (int j = 0; j < tempalteList.getChildCount(); j++) {


						View view= tempalteList.getChildAt(j) ;
						rect = new Rect((int)view.getX() , (int)view.getY() ,(int) view.getX()+ view.getWidth() ,(int) view.getY()+ view.getHeight()) ;
						//	Log.d(" most used ", " rect " + view.getX() + " " + view.getY() + view.getX()+view.getWidth() + " " + view.getY()+view.getHeight()) ;


						if(rect.contains(localX, localY)){


							Log.d("tempalte ", "template choosed "+j) ;
							templateChoosed(view, j) ;

							break ;

						}

					}
				}



				return false;
			}
		}) ;

				  /*
				  tempalteList.setOnItemClickListener(new OnItemClickListener() {

					@Override
					public void onItemClick(AdapterView<?> parent, View view,
							int position, long id) {
						// TODO Auto-generated method stub

					}
				}) ; */

		TextView followup_dismiss_view = (TextView) sideBarView.findViewById(R.id.followup_dismiss_view) ;
		followup_dismiss_view.setOnClickListener(new  OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				sideBarWindow.dismiss() ;
			}
		}) ;
		sideBarWindow = new PopupWindow(sideBarView , screenWidth , screenHeight-screenHeight/10) ;
		sideBarWindow.showAtLocation(sideBarView, Gravity.NO_GRAVITY, 0, 0)  ;

	}


	public void templateChoosed(View view , int position){
		TextView textId = (TextView) view.findViewById(R.id.followup_item_id);
		String idString = textId.getText().toString() ;
		Log.d("select template", "selected  template "+idString) ;

		for (int i = 0; i < templatesList.size(); i++) {
			if(templatesList.get(i).getLocal_id() == Integer.parseInt(idString)){
				FU_CONTENT_TEMPS = templatesList.get(i).getFu_content();
				FU_SUBJECT_TEMPS= templatesList.get(i).getFu_subject();
				selected_template= idString;
				//selected_template_local= String.valueOf(position);
				FU_CONTENT=templatesList.get(i).getFu_content();
				FU_SUBJECT=templatesList.get(i).getFu_subject();
				didEditTemplate =true;
				BiptagUserManger.saveUserData("selected_template", selected_template, NavigationActivity.getContext());
				/*	BiptagUserManger.saveUserData("FU_CONTENT_TEMPS", templatesList.get(i).getFu_content(), NavigationActivity.getContext()) ;
					BiptagUserManger.saveUserData("FU_SUBJECT_TEMPS", templatesList.get(i).getFu_subject(), NavigationActivity.getContext()) ;
					BiptagUserManger.saveUserData("selected_template", idString, NavigationActivity.getContext()) ;
					BiptagUserManger.saveUserData("FU_CONTENT", templatesList.get(i).getFu_content(), NavigationActivity.getContext()) ;
					BiptagUserManger.saveUserData("FU_SUBJECT", templatesList.get(i).getFu_subject(), NavigationActivity.getContext()) ;
*/

				content.setText(templatesList.get(i).getFu_content()) ;
				subject.setText(templatesList.get(i).getFu_subject()) ;

			}
		}






		sideBarWindow.dismiss() ;
	}


	/*
	class UpdateFollowUpMail extends AsyncTask<String, String, String>{

		JSONObject object ;
		ProgressDialog dialog ;

		@Override
			protected void onPreExecute() {
				// TODO Auto-generated method stub
				super.onPreExecute();
					dialog = new ProgressDialog(EditFollowUpMailActivity.this) ;
					dialog.setTitle("Enregistrement") ;
					dialog.setMessage("Veuillez patienter") ;
					dialog.setCancelable(false) ;
					dialog.setIndeterminate(false) ;
					dialog.show() ;


		}



		@Override
		protected String doInBackground(String... params) {
			// TODO Auto-generated method stub
			Log.d("edit followup ", "edit followup "+BiptagUserManger.getUserValue("FU_ID", "int", EditFollowUpMailActivity.this)) ;
			int fu_id = Integer.parseInt(String.valueOf(BiptagUserManger.getUserValue("FU_ID", "int", EditFollowUpMailActivity.this))) ;


			if(fu_id ==0 ){
				// insert fu
				String url = getResources().getString(R.urls.base_url)+getResources().getString(R.urls.insert_followup) ;
				List<NameValuePair> list = new ArrayList<NameValuePair>() ;
				list.add(new BasicNameValuePair("ba_id",String.valueOf(BiptagUserManger.getUserValue("BA_ID", "int", EditFollowUpMailActivity.this)))) ;
				list.add(new BasicNameValuePair("text", content.getText().toString())) ;
				list.add(new BasicNameValuePair("subject", subject.getText().toString())) ;


				JSONParser parser = new JSONParser() ;
				object = parser.makeHttpRequest(url, "GET", list) ;


			}else{
				// update fu
				String url = getResources().getString(R.urls.base_url)+getResources().getString(R.urls.update_followup) ;
				List<NameValuePair> list = new ArrayList<NameValuePair>() ;
				list.add(new BasicNameValuePair("fu_id",String.valueOf(BiptagUserManger.getUserValue("FU_ID", "int", EditFollowUpMailActivity.this)))) ;
				list.add(new BasicNameValuePair("text", content.getText().toString())) ;
				list.add(new BasicNameValuePair("subject", subject.getText().toString())) ;


				JSONParser parser = new JSONParser() ;
				object = parser.makeHttpRequest(url, "GET", list) ;


			}

			return null;
		}


		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			dialog.dismiss() ;
				try {
					if(object != null){
						if(object.getInt("success")== 1){

							if(object.has("fu_id")){
								BiptagUserManger.saveUserData("FU_ID", object.getInt("fu_id"), EditFollowUpMailActivity.this) ;
							}
						}
					}

				} catch (JSONException e) {
					// TODO: handle exception
				}

		}

	}

*/



	public  boolean isValidEmail(String target) {
		if (TextUtils.isEmpty(target)) {
			return false;
		} else {
			return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
		}
	}

	public void fragmentWillDisappear() {
		// TODO Auto-generated method stub

		if(sideBarWindow != null){
			sideBarWindow.dismiss() ;
		}
		if(popupWindow !=  null){
			popupWindow.dismiss() ;
		}

	}

}
