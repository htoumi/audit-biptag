package com.biptag.biptag.ocr;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.res.AssetManager;
import android.os.AsyncTask;
import android.util.Log;

import com.biptag.biptag.SplashScreenActivity;
import com.biptag.biptag.NavigationActivity;
import com.biptag.biptag.R;
import com.googlecode.tesseract.android.TessBaseAPI;

import org.xeustechnologies.jtar.TarEntry;
import org.xeustechnologies.jtar.TarInputStream;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.RandomAccessFile;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.zip.GZIPInputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

/**
 * Created by lenovo on 07/12/2015.
 */
public class OcrInitAsyncTaskCon extends AsyncTask<String, String, Boolean> {
    private static final String TAG = OcrInitAsyncTask.class.getSimpleName();

    /** Suffixes of required data files for Cube. */
    private static final String[] CUBE_DATA_FILES = {
            ".cube.bigrams",
            ".cube.fold",
            ".cube.lm",
            ".cube.nn",
            ".cube.params",
            ".cube.size", // This file is not available for Hindi
            ".cube.word-freq",
            ".tesseract_cube.nn",
            ".traineddata",
            ".word-dawg"
    };

    private SplashScreenActivity activity;

    private Context context;
    private TessBaseAPI baseApi;
    private ProgressDialog dialog;
    private ProgressDialog indeterminateDialog;
    private String languageCode;
    private String languageName;
    private int ocrEngineMode;

    public OcrInitAsyncTaskCon(SplashScreenActivity activity, TessBaseAPI baseApi, ProgressDialog dialog,
                            ProgressDialog indeterminateDialog, String languageCode, String languageName,
                            int ocrEngineMode) {
        this.activity = activity;
        this.context = activity.getBaseContext();
        this.baseApi = baseApi;
        this.dialog = dialog;
        this.indeterminateDialog = indeterminateDialog;
        this.languageCode = languageCode;
        this.languageName = languageName;
        this.ocrEngineMode = ocrEngineMode;
    }



    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        dialog.setTitle("veuillez patienter");
        dialog.setMessage("Vérification de l'installation des données...");
        dialog.setIndeterminate(false);
        dialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        dialog.setCancelable(false);
      //  dialog.show();
        activity.setButtonVisibility(false);
    }

    protected Boolean doInBackground(String... params) {
        // Check whether we need Cube data or Tesseract data.
        // Example Cube data filename: "tesseract-ocr-3.01.eng.tar"
        // Example Tesseract data filename: "eng.traineddata"

       // Check for, and create if necessary, folder to hold model data
        String destinationDirBase = params[0]; // The storage directory, minus the

        //copy assets
        copyAssetFolder(context.getAssets(), "tessdata",
                destinationDirBase + "/tessdata");
        Log.d(" ocr ", " path 2 " + destinationDirBase +"/tessdata");
        // "tessdata" subdirectory
       return true;
    }
        // Create a reference to the file to save the download in

        // Check if an incomplete download is present. If a *.download file is there, delete it and
        // any (possibly half-unzipped) Tesseract and Cube data files that may be there.


        // Check whether all Cube data files have already been installed


        // If language data files are not present, install them










    @Override
    protected void onProgressUpdate(String... message) {
        super.onProgressUpdate(message);
        int percentComplete = 0;

        percentComplete = Integer.parseInt(message[1]);
        dialog.setMessage(message[0]);
        dialog.setProgress(percentComplete);
       // dialog.show();
    }

    @Override
    protected void onPostExecute(Boolean result) {
        super.onPostExecute(result);

        try {
            indeterminateDialog.dismiss();
        } catch (IllegalArgumentException e) {
            // Catch "View not attached to window manager" error, and continue
        }

        if (result) {
            // Restart recognition
            activity.resumeOCR();

        } else {
            activity.showErrorMessage("Error", "Network is unreachable - cannot download language data. "
                    + "Please enable network access and restart this app.");
        }
    }


    private static boolean copyAssetFolder(AssetManager assetManager,
                                           String fromAssetPath, String toPath) {
        try {
            String[] files = assetManager.list(fromAssetPath);
            new File(toPath).mkdirs();
            boolean res = true;
            for (String file : files)
                if (file.contains("."))
                    res &= copyAsset(assetManager,
                            fromAssetPath + "/" + file,
                            toPath + "/" + file);
                else
                    res &= copyAssetFolder(assetManager,
                            fromAssetPath + "/" + file,
                            toPath + "/" + file);
            return res;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    private static boolean copyAsset(AssetManager assetManager,
                                     String fromAssetPath, String toPath) {
        InputStream in = null;
        OutputStream out = null;
        try {
            in = assetManager.open(fromAssetPath);
            new File(toPath).createNewFile();
            out = new FileOutputStream(toPath);
            copyFile(in, out);
            in.close();
            in = null;
            out.flush();
            out.close();
            out = null;
            return true;
        } catch(Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    private static void copyFile(InputStream in, OutputStream out) throws IOException {
        byte[] buffer = new byte[1024];
        int read;
        while((read = in.read(buffer)) != -1){
            out.write(buffer, 0, read);
        }
    }





}
