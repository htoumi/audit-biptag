package com.biptag.biptag.ocr;

import org.json.JSONObject;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Point;
import android.os.Handler;
import android.os.Message;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;


//import eniso.ii.pfe.myocrapp.R;
import com.biptag.biptag.CustumViews.BTEditText;
import com.biptag.biptag.NavigationActivity;
import com.biptag.biptag.R;
import com.biptag.biptag.camera.CameraManager;
import com.biptag.biptag.dbmanager.datasource.BtUserConfigDataSource;
import com.biptag.biptag.dbmanager.objects.BTUserConfig;
import com.biptag.biptag.fragements.EditOCRFragment;
import com.biptag.biptag.fragements.create.FillingFormStep1;
import com.biptag.biptag.managers.BiptagNavigationManager;
import com.biptag.biptag.managers.BiptagUserManger;
import com.biptag.biptag.ocr.CaptureActivity;
import com.biptag.biptag.ocr.CaptureActivityHandler;
import com.biptag.biptag.ocr.DecodeHandler;
import com.biptag.biptag.ocr.DecodeThread;
import com.biptag.biptag.ocr.OcrResult;
import com.biptag.biptag.ocr.OcrResultFailure;
/* This class handles all the messaging which comprises the state machine for capture*/
public class CaptureActivityHandler extends Handler{
  private static final String TAG = CaptureActivityHandler.class.getSimpleName();

private final CaptureActivity activity;
private final DecodeThread decodeThread;
private static State state;
private final CameraManager cameraManager;
private JSONObject object ;
  TextView textView;
  static boolean b ;
  Toast toast;

private enum State {
  PREVIEW,
  PREVIEW_PAUSED,
  CONTINUOUS,
  CONTINUOUS_PAUSED,
  SUCCESS,
  DONE
}

CaptureActivityHandler(CaptureActivity activity, CameraManager cameraManager, boolean isContinuousModeActive) {
  this.activity = activity;
  this.cameraManager = cameraManager;
  WindowManager wm = (WindowManager) NavigationActivity.getContext().getSystemService(Context.WINDOW_SERVICE);
  Display display  =wm.getDefaultDisplay() ;
  Point point  = new Point() ;
  display.getSize(point) ;
  if (getlanguageCode().indexOf("fra")!=-1) {
    toast = Toast.makeText(activity.getBaseContext(), "Merci de positionner la carte de visite selon ce cadre", Toast.LENGTH_LONG);

  }else{
    toast = Toast.makeText(activity.getBaseContext(),"Please position your business card within this box", Toast.LENGTH_LONG);

  }
    toast.setGravity(Gravity.TOP, 0, 0);



  fireLongToast(toast);

  // Start ourselves capturing previews (and decoding if using continuous recognition mode).

  cameraManager.startPreview();
  decodeThread = new DecodeThread(activity);
  decodeThread.start();

  if (isContinuousModeActive) {
    state = State.CONTINUOUS;

    // Show the shutter and torch buttons
    activity.setButtonVisibility(true);

    // Display a "be patient" message while first recognition request is running
    activity.setStatusViewForContinuous();

    restartOcrPreviewAndDecode();
  } else {
    state = State.SUCCESS;
b=false;
    toast.cancel();
    // Show the shutter and torch buttons
    activity.setButtonVisibility(true);

    restartOcrPreview();
  }

}



  public String getlanguageCode() {

    Resources res = NavigationActivity.getContext().getResources();
    // Change locale settings in the app.
    DisplayMetrics dm = res.getDisplayMetrics();
    android.content.res.Configuration conf = res.getConfiguration();
    Log.d(" lang ", "local " + conf.locale.getDisplayLanguage());
    String languagecode = conf.locale.getDisplayLanguage();

    int agent_id = Integer.parseInt(String.valueOf(BiptagUserManger.getUserValue("AGENT_ID", "int", NavigationActivity.getContext())));

    BtUserConfigDataSource source = new BtUserConfigDataSource(NavigationActivity.getContext());
    BTUserConfig config = source.getUserConfig(agent_id);
    if (config != null) {

      if (config.getLanguage().equals("Français")) {
        languagecode = "fra";
        Log.d(" lang "," config !=null" +languagecode);
        return languagecode;
      } else {
        languagecode = "eng";
        Log.d(" lang ", "config != null"+languagecode);
        return languagecode;
      }

    } else {
      if (conf.locale.getDisplayLanguage().equals("English")) {
        Log.d(" ocr ", " lang 2 " + conf.locale.getDisplayLanguage());
        languagecode = "eng";
        Log.d(" lang "," "+ languagecode);
        return languagecode;
      } else {
        //if (conf.locale.getDisplayLanguage() == "English") {
        languagecode = "fra";
        Log.d(" lang "," "+ languagecode);
        return languagecode;
        //}
      }

    }

    //return languagecode;
  }



  private void fireLongToast(final Toast toast) {

    Thread t = new Thread() {
      public void run() {
int count =0;
        try {
          while (true && count < 5) {
            toast.show();
            sleep(1000);
            count++;

            // do some logic that breaks out of the while loop
          }
        } catch (Exception e) {
          Log.e("LongToast", "", e);
        }
      }
    };
    t.start();
  }
public void hideToast(Toast toast){

  if (toast.getDuration()== 1){
    toast.cancel();
  }
}

@Override
public void handleMessage(Message message) {

toast.cancel();
  switch (message.what) {
    case R.id.restart_preview:
      restartOcrPreview();
      break;
    case R.id.ocr_continuous_decode_failed:
      DecodeHandler.resetDecodeState();
      try {
        activity.handleOcrContinuousDecode((OcrResultFailure) message.obj);
      } catch (NullPointerException e) {
        //Log.w(TAG, "got bad OcrResultFailure", e);
      }
      if (state == State.CONTINUOUS) {
        restartOcrPreviewAndDecode();
      }
      break;
    case R.id.ocr_continuous_decode_succeeded:
      DecodeHandler.resetDecodeState();
      try {
        activity.handleOcrContinuousDecode((OcrResult) message.obj);
      } catch (NullPointerException e) {
        // Continue
      }
      if (state == State.CONTINUOUS) {
        restartOcrPreviewAndDecode();
      }
      break;
    case R.id.ocr_decode_succeeded:
      state = State.SUCCESS;
      b=false;
      toast.cancel();
      activity.setShutterButtonClickable(true);
     /* if (textView.getVisibility() == 1){
        textView.visi
      }*/
      object =  activity.handleOcrDecode((OcrResult) message.obj);



      break;
    case R.id.ocr_decode_failed:
      object=activity.handleOcrDecode((OcrResult)message.obj);
      state = State.SUCCESS;
/*
      AlertDialog.Builder builder= new AlertDialog.Builder(this.activity) ;
      builder.setCancelable(false);
      builder.setTitle(NavigationActivity.getContext().getResources().getString(R.string.recognition_fld_title))  ;
      builder.setMessage(NavigationActivity.getContext().getResources().getString(R.string.recognition_fld_desc)) ;
      builder.setPositiveButton(NavigationActivity.getContext().getResources().getString(R.string.ocr_no_data_btn_close), new DialogInterface.OnClickListener() {

        @Override
        public void onClick(DialogInterface dialog, int which) {
          // TODO Auto-generated method stub

          toast.cancel();
          dialog.dismiss();
          b = false;
          activity.finish();


        }
      }) ;

      builder.setNegativeButton(NavigationActivity.getContext().getResources().getString(R.string.ocr_no_data_btn_retake), new DialogInterface.OnClickListener() {

        @Override
        public void onClick(DialogInterface dialog, int which) {
          // TODO Auto-generated method stub
          state = State.PREVIEW;

          activity.setShutterButtonClickable(true);
          dialog.dismiss();
        }
      }) ;
      builder.setNeutralButton("Saisir", new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {


           }
      });

builder.show();
*/
  }
}

void stop() {
  //Log.d(TAG, "Setting state to CONTINUOUS_PAUSED.");
  state = State.CONTINUOUS_PAUSED;
  removeMessages(R.id.ocr_continuous_decode);
  removeMessages(R.id.ocr_decode);
  removeMessages(R.id.ocr_continuous_decode_failed);
  removeMessages(R.id.ocr_continuous_decode_succeeded);

}

public void resetState() {
  if (state == State.CONTINUOUS_PAUSED) {
    //Log.d(TAG, "Setting state to CONTINUOUS");
    state = State.CONTINUOUS;
    restartOcrPreviewAndDecode();
  }
}

public void quitSynchronously() {
  state = State.DONE;
  if (cameraManager != null) {
    cameraManager.stopPreview();
  }

  try {

    // Wait at most half a second; should be enough time, and onPause() will timeout quickly
    decodeThread.join(500L);
  } catch (RuntimeException e) {
    //Log.w(TAG, "Caught RuntimeException in quitSyncronously()", e);
    // continue
  } catch (Exception e) {
   // Log.w(TAG, "Caught unknown Exception in quitSynchronously()", e);
  }

  // Be absolutely sure we don't send any queued up messages
  removeMessages(R.id.ocr_continuous_decode);
  removeMessages(R.id.ocr_decode);

}

// Start the preview, but don't try to OCR anything until the user presses the shutter button.

private void restartOcrPreview() {
  // Display the shutter and torch buttons
  activity.setButtonVisibility(true);
Log.d(" ocr failed ", " restart");
  if (state == State.SUCCESS) {
    state = State.PREVIEW;

    // Draw the viewfinder.
    activity.drawViewfinder();
  }
}

//  Send a decode request for realtime OCR mode

private void restartOcrPreviewAndDecode() {
  // Continue capturing camera frames
  cameraManager.startPreview();

  // Continue requesting decode of images
  cameraManager.requestOcrDecode(decodeThread.getHandler(), R.id.ocr_continuous_decode);
  activity.drawViewfinder();
}

// Request OCR on the current preview frame. 

private void ocrDecode() {
  state = State.PREVIEW_PAUSED;
  cameraManager.requestOcrDecode(decodeThread.getHandler(), R.id.ocr_decode);
}

// Request OCR when the hardware shutter button is clicked.

void hardwareShutterButtonClick() {
  // Ensure that we're not in continuous recognition mode
  if (state == State.PREVIEW) {
    ocrDecode();
  }
}

//Request OCR when the on-screen shutter button is clicked.

void shutterButtonClick() {
  toast.cancel();
  // Disable further clicks on this button until OCR request is finished
  activity.setShutterButtonClickable(false);
  ocrDecode();
}

}
