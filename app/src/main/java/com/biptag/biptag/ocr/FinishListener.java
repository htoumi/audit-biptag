package com.biptag.biptag.ocr;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
/*Simple listener used to exit the app in a few cases.*/
public class FinishListener implements DialogInterface.OnClickListener,DialogInterface.OnCancelListener,Runnable{

	  private final Activity activityToFinish;

	  public FinishListener(Activity activityToFinish) {
	    this.activityToFinish = activityToFinish;
	  }

	  public void onCancel(DialogInterface dialogInterface) {
	    run();
	  }

	  public void onClick(DialogInterface dialogInterface, int i) {
	    run();
	  }

	  public void run() {
	    activityToFinish.finish();
	  }

}
