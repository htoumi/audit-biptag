package com.biptag.biptag.ocr;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.AssetManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Environment;
import android.util.DisplayMetrics;
import android.util.Log;

import com.biptag.biptag.NavigationActivity;
import com.biptag.biptag.R;
import com.biptag.biptag.dbmanager.datasource.BtUserConfigDataSource;
import com.biptag.biptag.dbmanager.objects.BTUserConfig;
import com.biptag.biptag.interfaces.AsyncResponse;
import com.biptag.biptag.managers.BiptagUserManger;
import com.googlecode.tesseract.android.TessBaseAPI;

public class TessOCR  extends AsyncTask<String, String, String>{

	private TessBaseAPI baseAPI ; 
	private Bitmap bitmap ; 
	private Context context ; 
	String ocrResult  ;
	private AsyncResponse response  ; 
	ProgressDialog dialog  ; 
	public TessOCR(Bitmap bitmap , Context context , AsyncResponse response) {
		// TODO Auto-generated constructor stub
		baseAPI = new TessBaseAPI() ; 
	        String language = "eng";
	        baseAPI.init(CopyAssets("", ""), language);
	        this.bitmap = bitmap ; 
	        this.context = context ; 
	        this.response = response ; 
	}

	  private  String getOCRResult(Bitmap bitmap) {

	        baseAPI.setImage(bitmap);
	        String result = baseAPI.getUTF8Text();
	 
	        return result;
	    }
	 
	    public void onDestroy() {
	        if (baseAPI != null)
	        	baseAPI.end();
	    }
	    	  private String CopyAssets(String name , String targetDirectory) {
	    		  AssetManager assetManager = NavigationActivity.getContext().getResources().getAssets();
	    		  //Log.d("tessdata", "tessdata files enter copy") ;
String[] files = null;

try {
    files = assetManager.list("tesseract-ocr-3.02.eng/tesseract-ocr/tessdata"); //tessdata is folder name
	Log.d("API ", "tesseract from tessocr");
} catch (Exception e) {
    //Log.e("", "tessdata ERROR: " + e.toString());
}

File tessDataDirectory = new File(NavigationActivity.getContext().getCacheDir().getAbsolutePath()+"/tessdata") ; 
	tessDataDirectory.mkdir() ; 


//Log.d("tessdata", "tessdata files size"+files.length) ;

for (int i = 0; i < files.length; i++) {
    InputStream in = null;
    OutputStream out = null;
    try {
        in = assetManager.open("tesseract-ocr-3.02.eng/tesseract-ocr/tessdata/" + files[i]);
		Log.d("API ", "tesseract in tessocr");
        out = new FileOutputStream(tessDataDirectory.getAbsolutePath()+"/" + files[i]);
        byte[] buffer = new byte[65536 * 2];
        int read;
        while ((read = in.read(buffer)) != -1) {
            out.write(buffer, 0, read);
        }
        in.close();
        in = null;
        out.flush();
        out.close();
        out = null;
        //Log.d("", "tessdata File "+files[i]+" Copied in "+tessDataDirectory.getAbsolutePath()+"/" + files[i]);
    } catch (Exception e) {
       // Log.e("", "ERROR: " + e.toString());
    }
}
 
	    	  return NavigationActivity.getContext().getCacheDir().getAbsolutePath() ; 
	    	  
	    	  
	    	  }
	    	  
	    	  
	    	  @Override
	    	protected void onPreExecute() {
	    		// TODO Auto-generated method stub
	    		super.onPreExecute();
	    	
	    	   
	    	  
	    	  dialog = new ProgressDialog(context) ;
				  if (getlanguageCode().indexOf("fra")!=-1) {
					  dialog.setTitle("Reconnaissance") ;
					  dialog.setMessage("Traitement en cours ...") ;

				  }else{
					  dialog.setTitle("Recognition ") ;
					  dialog.setMessage("Loading ... ") ;

				  }


				dialog.setCancelable(false) ;
	    	  dialog.setIndeterminate(false) ;
	    	  dialog.setButton(NavigationActivity.getContext().getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
				
				@Override
				public void onClick(DialogInterface dialog, int which) {
					// TODO Auto-generated method stub
					TessOCR.this.cancel(true) ; 
					try {
						JSONObject jsonObject = new JSONObject() ; 
						jsonObject.put("EMAIL", "") ; 
						if(response != null){
							response.processFinish(jsonObject.toString()) ; 
						}
					} catch (JSONException e) {
						// TODO: handle exception
					}
					
				}
			}) ; 
	    	  dialog.show() ; 
	    	  }

			@Override
			protected String doInBackground(String... params) {
				// TODO Auto-generated method stub
				
				
				
				ocrResult = getOCRResult(bitmap)  ;
				
				
				return null;
			}
			
			
			
			@Override
			protected void onPostExecute(String result) {
				// TODO Auto-generated method stub
				super.onPostExecute(result);
			
				if(dialog != null){
					dialog.dismiss() ; 
				}
				if(ocrResult != null ){
					if(response!= null)
						response.processFinish(ocrResult) ; 
				}else{
						if(response != null)
							response.processFailed() ; 
				}
			
			
			}



	public String getlanguageCode() {

		Resources res = NavigationActivity.getContext().getResources();
		// Change locale settings in the app.
		DisplayMetrics dm = res.getDisplayMetrics();
		android.content.res.Configuration conf = res.getConfiguration();
		Log.d(" lang ", "local " + conf.locale.getDisplayLanguage());
		String languagecode = conf.locale.getDisplayLanguage();

		int agent_id = Integer.parseInt(String.valueOf(BiptagUserManger.getUserValue("AGENT_ID", "int", NavigationActivity.getContext())));

		BtUserConfigDataSource source = new BtUserConfigDataSource(NavigationActivity.getContext());
		BTUserConfig config = source.getUserConfig(agent_id);
		if (config != null) {

			if (config.getLanguage().equals("Français")) {
				languagecode = "fra";
				Log.d(" lang "," config !=null" +languagecode);
				return languagecode;
			} else {
				languagecode = "eng";
				Log.d(" lang ", "config != null"+languagecode);
				return languagecode;
			}

		} else {
			if (conf.locale.getDisplayLanguage().equals("English")) {
				Log.d(" ocr ", " lang 2 " + conf.locale.getDisplayLanguage());
				languagecode = "eng";
				Log.d(" lang "," "+ languagecode);
				return languagecode;
			} else {
				//if (conf.locale.getDisplayLanguage() == "English") {
				languagecode = "fra";
				Log.d(" lang "," "+ languagecode);
				return languagecode;
				//}
			}

		}

		//return languagecode;
	}
	
}
