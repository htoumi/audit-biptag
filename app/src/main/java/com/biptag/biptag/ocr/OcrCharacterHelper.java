package com.biptag.biptag.ocr;

import android.content.SharedPreferences;
import android.util.Log;

/*Helper class to enable language-specific character blacklists/whitelists*/
public class OcrCharacterHelper {
	 
	  public static final String KEY_CHARACTER_BLACKLIST_ARABIC = "preference_character_blacklist_arabic";
	  public static final String KEY_CHARACTER_BLACKLIST_ENGLISH = "preference_character_blacklist_english";
	  public static final String KEY_CHARACTER_BLACKLIST_FRENCH = "preference_character_blacklist_french";
	  
	  public static final String KEY_CHARACTER_WHITELIST_ARABIC = "preference_character_whitelist_arabic";
	  public static final String KEY_CHARACTER_WHITELIST_ENGLISH = "preference_character_whitelist_english";
	  public static final String KEY_CHARACTER_WHITELIST_FRENCH = "preference_character_whitelist_french";
	  
	  private OcrCharacterHelper() {} 
	  // Private constructor to enforce noninstantiability
	  
	  public static String getDefaultBlacklist(String languageCode) {
	    //final String DEFAULT_BLACKLIST = "`~|";
	   
	     if (languageCode.equals("ara")) { return ""; } // Arabic
	   
	    else if (languageCode.equals("eng")) { return ""; } // English
	    else if (languageCode.equals("fra")) { return ""; } // French
	
	    else {
	      throw new IllegalArgumentException();
	    }
	  }
	  
	  public static String getDefaultWhitelist(String languageCode) {
	   
	     if (languageCode.equals("ara")) { return ""; } // Arabic
	    else if (languageCode.equals("eng")) { return "!?@#$%&*()<>_-+=/.,:;'\"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789"; } // English
	    else if (languageCode.equals("fra")) { return ""; } // French
	    
	    else {
	      throw new IllegalArgumentException();
	    }
	  }

	  public static String getBlacklist(SharedPreferences prefs, String languageCode) {

	   Log.d(" ocr " ," blacklist "  + languageCode);
	    if (languageCode.equals("ara")) { return prefs.getString(KEY_CHARACTER_BLACKLIST_ARABIC, getDefaultBlacklist(languageCode)); }
	   
	   else if (languageCode.equals("eng")) { return prefs.getString(KEY_CHARACTER_BLACKLIST_ENGLISH, getDefaultBlacklist(languageCode)); }
	    else if (languageCode.equals("fra")) { return prefs.getString(KEY_CHARACTER_BLACKLIST_FRENCH, getDefaultBlacklist(languageCode)); }
	    else{
	      throw new IllegalArgumentException();
	    }    
	  }
	  
	  public static String getWhitelist(SharedPreferences prefs, String languageCode) {
	    
	    if (languageCode.equals("ara")) { return prefs.getString(KEY_CHARACTER_WHITELIST_ARABIC, getDefaultWhitelist(languageCode)); }
	    else if (languageCode.equals("eng")) { return prefs.getString(KEY_CHARACTER_WHITELIST_ENGLISH, getDefaultWhitelist(languageCode)); }
	 
	    else if (languageCode.equals("fra")) { return prefs.getString(KEY_CHARACTER_WHITELIST_FRENCH, getDefaultWhitelist(languageCode)); }
	    
	    else {
	      throw new IllegalArgumentException();
	    }        
	  }
	  
	  public static void setBlacklist(SharedPreferences prefs, String languageCode, String blacklist) {
	    if (languageCode.equals("ara")) { prefs.edit().putString(KEY_CHARACTER_BLACKLIST_ARABIC, blacklist).commit(); }
	    
	   else if (languageCode.equals("eng")) { prefs.edit().putString(KEY_CHARACTER_BLACKLIST_ENGLISH, blacklist).commit(); }
	    else if (languageCode.equals("fra")) { prefs.edit().putString(KEY_CHARACTER_BLACKLIST_FRENCH, blacklist).commit(); }
	   else {
	      throw new IllegalArgumentException();
	    }    
	  }
	  
	  public static void setWhitelist(SharedPreferences prefs, String languageCode, String whitelist) {
	    
	    if (languageCode.equals("ara")) { prefs.edit().putString(KEY_CHARACTER_WHITELIST_ARABIC, whitelist).commit(); }
	    else if (languageCode.equals("eng")) { prefs.edit().putString(KEY_CHARACTER_WHITELIST_ENGLISH, whitelist).commit(); }
	    else if (languageCode.equals("fra")) { prefs.edit().putString(KEY_CHARACTER_WHITELIST_FRENCH, whitelist).commit(); }
	    else {
	      throw new IllegalArgumentException();
	    }    
	  }
}
