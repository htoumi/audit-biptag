package com.biptag.biptag.ocr;

import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.content.ContentResolver;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;

import com.biptag.biptag.NavigationActivity;
import com.biptag.biptag.dbmanager.datasource.BTImageDataSource;
import com.biptag.biptag.dbmanager.objects.BTImage;
import com.biptag.biptag.fragements.GalleryImageFragment;
import com.biptag.biptag.fragements.create.FillingFormStep1;
import com.biptag.biptag.managers.BiptagUserManger;
import com.googlecode.leptonica.android.ReadFile;
import com.googlecode.tesseract.android.TessBaseAPI;

//import eniso.ii.pfe.myocrapp.R;
import com.biptag.biptag.R;
import com.biptag.biptag.ocr.CaptureActivity;
import com.biptag.biptag.ocr.OcrResult;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/*
 * Class to send OCR requests to the OCR engine in a separate thread, 
 * send a success/failure message,
 * and dismiss the indeterminate progress dialog box. 
 * Used for non-continuous mode OCR only.
 */
final class OcrRecognizeAsyncTask extends AsyncTask<Void, Void, Boolean>{
      private CaptureActivity activity;
	  private TessBaseAPI baseApi;
	  private byte[] data;
	  private int width;
	  private int height;
	  private OcrResult ocrResult;
	  private long timeRequired;

	  OcrRecognizeAsyncTask(CaptureActivity activity, TessBaseAPI baseApi, byte[] data, int width, int height) {
	    this.activity = activity;
	    this.baseApi = baseApi;
	    this.data = data;
	    this.width = width;
	    this.height = height;
	  }

	/** Create a File for saving an image or video */



	@Override
	  protected Boolean doInBackground(Void... arg0) {
	    long start = System.currentTimeMillis();

		PlanarYUVLuminanceSource source = activity.getCameraManager().buildLuminanceSource(data, width, height) ;
		Bitmap colorB = source.getBitmapImageFromYUV(data,width,height)  ;
	    Bitmap bitmap = source.renderCroppedGreyscaleBitmap();
	  //  Bitmap bitmap2 = activity.getCameraManager().buildLuminanceSource2(data, width, height);

		Log.d("camera ", "bitmap.getcameramanager");
		save Save = new save();
		//	Save.SaveImage(activity, bitmap);
		Log.d("camera ", "storeBitmp");




/*


		BTImage image = new BTImage(NavigationActivity.getContext());


		int agent_id = Integer.parseInt(String.valueOf(BiptagUserManger.getUserValue("BA_ID", "int", NavigationActivity.getContext())));

		Calendar c = Calendar.getInstance();
		SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
		String formattedDate = df.format(c.getTime());
		image.setAgentId(agent_id);
		image.setImageDate(formattedDate);
		image.setResponseId(0);
		image.setIsSent(-1);
		BTImageDataSource dataSource = new BTImageDataSource(NavigationActivity.getContext());

		image.setBitmap(colorB);
		Log.d("image ", "saving image");
		image.saveBitmap();
		Log.d("image ", "image saved");
		BTImageDataSource imageDataSource = new BTImageDataSource(NavigationActivity.getContext());
		long id = imageDataSource.saveImage(image);
		int vcardimage = (int) imageDataSource.saveImage(image);
		String key = "";
		/*Intent iin = getIntent();
		Bundle b = iin.getExtras();
		int Mode = b.getInt("EditMode");
		Log.d("Image ", "mode : " + Mode);
		if (Mode == 5) {

			key = createImageArrayUserPreferenceName;
			//	BiptagUserManger.saveUserData("key", key, NavigationActivity.getContext());


			Log.d("Image ", key);
		} else if (Mode == 6) {
			key = editImageArrayUserPreferenceName;
			//BiptagUserManger.saveUserData("key", key, NavigationActivity.getContext());

			Log.d("Image ", key);
		}
*/
/*		String vcardIdKey =String.valueOf( BiptagUserManger.getUserValue("vcardIdKey","string",NavigationActivity.getContext()));

	int	vcardImageId = Integer.parseInt(String.valueOf(BiptagUserManger.getUserValue(vcardIdKey, "int", NavigationActivity.getContext())));

		key =String.valueOf( BiptagUserManger.getUserValue("key","string",NavigationActivity.getContext()));

		Log.d("Image ", "Key " + key);

		BTImage dbImage = dataSource.selectImagesWithId(vcardImageId, agent_id);
		String imageJsonString = String.valueOf(BiptagUserManger.getUserValue(key, "string", NavigationActivity.getContext()));

		JSONArray imagesJson = null;
		int oldIndex = -1;
		try {
			if (imageJsonString.length() != 0) {
				imagesJson = new JSONArray(imageJsonString);
			} else {
				imagesJson = new JSONArray();
			}
			//  vcardImageId  = (int) dataSource.saveImage(image) ;
			//  for
			for (int i = 0; i <= imagesJson.length(); i++) {
				Log.d("image ", " json " + imagesJson.getInt(i) + " vcard " + vcardImageId);
				if (vcardImageId == imagesJson.getInt(i)) {
					oldIndex = i;
				}
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block

		}
		//

		if (dbImage == null)
			vcardImageId = (int) dataSource.saveImage(image);
		else {
			vcardImageId = dbImage.getImageId();
			image.setImageId((int) vcardImageId);
			boolean update = dataSource.updateImage(image);
			//	Log.d("bdimage", "bdimage upadte "+update) ;

		}

		try {
			if (oldIndex != -1) {
				imagesJson.put(oldIndex, vcardImageId);
			} else {
				imagesJson.put(vcardImageId);
			}

			//Log.d("sendin image string", "sendin image string "+imagesJson.toString()) ;
			BiptagUserManger.saveUserData(key, imagesJson.toString(), NavigationActivity.getContext());
			BiptagUserManger.saveUserData(vcardIdKey, vcardImageId, NavigationActivity.getContext());

		} catch (JSONException e) {
			// TODO Auto-generated catch block

		}
		//finish();

*/












		String textResult;

	    try {
	      baseApi.setImage(ReadFile.readBitmap(bitmap));
	      textResult = baseApi.getUTF8Text();
	      timeRequired = System.currentTimeMillis() - start;

	      // Check for failure to recognize text
	      if (textResult == null || textResult.equals("")) {
			  ocrResult = new OcrResult();
			  ocrResult.setBitmap(colorB);
	        return false;
	      }
	      ocrResult = new OcrResult();

	      ocrResult.setWordConfidences(baseApi.wordConfidences());
	      ocrResult.setMeanConfidence(baseApi.meanConfidence());
	      ocrResult.setRegionBoundingBoxes(baseApi.getRegions().getBoxRects());
	      ocrResult.setTextlineBoundingBoxes(baseApi.getTextlines().getBoxRects());
	      ocrResult.setWordBoundingBoxes(baseApi.getWords().getBoxRects());
	      ocrResult.setStripBoundingBoxes(baseApi.getStrips().getBoxRects());
	      
	    } catch (RuntimeException e) {
	      //Log.e("OcrRecognizeAsyncTask", "Caught RuntimeException in request to Tesseract. Setting state to CONTINUOUS_STOPPED.");
	      e.printStackTrace();
	      try {
	        baseApi.clear();
	        activity.stopHandler();
	      } catch (NullPointerException e1) {
	        // Continue
	      }
	      return false;
	    }
	    timeRequired = System.currentTimeMillis() - start;
		 ocrResult.setBitmap(colorB);
	    ocrResult.setText(textResult);
	    ocrResult.setRecognitionTimeRequired(timeRequired);
	    return true;
	  }

	  @Override
	  protected void onPostExecute(Boolean result) {
	    super.onPostExecute(result);

	    Handler handler = activity.getHandler();
	    if (handler != null) {
	      // Send results for single-shot mode recognition.
	      if (result) {
	        Message message = Message.obtain(handler, R.id.ocr_decode_succeeded, ocrResult);
	        message.sendToTarget();
	      } else {
	        Message message = Message.obtain(handler, R.id.ocr_decode_failed, ocrResult);
	        message.sendToTarget();
	      }
	      activity.getProgressDialog().dismiss();
	    }
	    if (baseApi != null) {
	      baseApi.clear();
	    }
	  }
}
