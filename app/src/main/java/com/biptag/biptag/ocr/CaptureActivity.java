package com.biptag.biptag.ocr;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Rect;
import android.hardware.Camera;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.text.SpannableStringBuilder;
import android.text.style.CharacterStyle;
import android.text.style.ForegroundColorSpan;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.biptag.biptag.NavigationActivity;
import com.biptag.biptag.R;
import com.biptag.biptag.camera.CameraManager;
import com.biptag.biptag.camera.ShutterButton;

import com.biptag.biptag.dbmanager.datasource.BTImageDataSource;
import com.biptag.biptag.dbmanager.datasource.BtUserConfigDataSource;
import com.biptag.biptag.dbmanager.objects.BTImage;
import com.biptag.biptag.dbmanager.objects.BTUserConfig;
import com.biptag.biptag.fragements.EditOCRFragment;
import com.biptag.biptag.fragements.GalleryImageFragment;
import com.biptag.biptag.fragements.create.FillingFormStep1;
import com.biptag.biptag.language.LanguageCodeHelper;
import com.biptag.biptag.managers.BipTagFormsManager;
import com.biptag.biptag.managers.BiptagNavigationManager;
import com.biptag.biptag.managers.BiptagUserManger;
import com.biptag.biptag.regularepression.EmailValidator;
import com.biptag.biptag.regularepression.FullNameValidator;
import com.biptag.biptag.regularepression.PhoneValidator;
import com.googlecode.tesseract.android.TessBaseAPI;

import javax.sql.DataSource;
//import eniso.ii.pfe.myocrapp.R;
//import eniso.ii.pfe.myocrapp.R.id;
//import eniso.ii.pfe.myocrapp.R.layout;
//import eniso.ii.pfe.myocrapp.R.menu;
//import android.support.v4.app.FragmentActivity;
//import android.support.v7.app.ActionBarActivity;
/**cette activité sert à ouvrir la camera et fait le balayage reele d'un background thread ,
 * elle dessine un Viewfinder pour aider l'utilisateur à placer correctement
 * le texte ,elle montre un feedback quand le traitement de l'image est en cours ,
 * ensuite elle  chevauche les resultats lorsque le scan est réussie
*/
public class CaptureActivity extends  Activity implements SurfaceHolder.Callback,
ShutterButton.OnShutterButtonListener {
	public final static String createImageArrayUserPreferenceName = "images_for_user";
	public final static String editImageArrayUserPreferenceName = "images_for_user_edit";

	private static final String TAG = CaptureActivity.class.getSimpleName();

	// ISO 639-3 language code indicating the default recognition language.
	// LZR 14/06/2014 language code indicating the default recognition language.
	public static final String DEFAULT_SOURCE_LANGUAGE_CODE = "fra";
	// LZR 14/06/2014 The default OCR engine to use
	public static final String DEFAULT_OCR_ENGINE_MODE = "Tesseract";
	// LZR 14/06/2014 The default page segmentation mode to use
	public static final String DEFAULT_PAGE_SEGMENTATION_MODE = "Auto";
	// LZR 14/06/2014
	public static final boolean DEFAULT_TOGGLE_AUTO_FOCUS = true;
	// LZR 14/06/2014
	public static final boolean DEFAULT_DISABLE_CONTINUOUS_FOCUS = true;
	// LZR 14/06/2014 Whether to beep by default when the shutter button is pressed
	public static final boolean DEFAULT_TOGGLE_BEEP = false;
	// LZR 14/06/2014  Whether to initially show a looping, real-time OCR display.
	public static final boolean DEFAULT_TOGGLE_CONTINUOUS = false;
	// LZR 14/06/2014 Whether to initially reverse the image returned by the camera
	public static final boolean DEFAULT_TOGGLE_REVERSED_IMAGE = false;
	// LZR 14/06/2014
	public static final boolean DEFAULT_TOGGLE_LIGHT = false;
	// LZR 14/06/2014  Flag to display the real-time recognition results at the top of the scanning screen
	private static final boolean CONTINUOUS_DISPLAY_RECOGNIZED_TEXT = true;
	// LZR 14/06/2014 Flag to display recognition-related statistics on the scanning screen
	private static final boolean CONTINUOUS_DISPLAY_METADATA = true;
	// LZR 14/06/2014 Flag to enable display of the on-screen shutter button.
	private static final boolean DISPLAY_SHUTTER_BUTTON = true;


	public static final String vcardIdKeyEdit = "vcard_id_key_edit";
	public static final String vcardIdKeyCreate = "vcard_id_key_create";
	Uri imageUri;
	// Languages for which Cube data is available.
	static final String[] CUBE_SUPPORTED_LANGUAGES = {
			"ara", // Arabic
			"eng", // English
			"fra" // français
	};

	// Languages that require Cube, and cannot run using Tesseract.
	private static final String[] CUBE_REQUIRED_LANGUAGES = {
			"ara" // Arabic
	};

	static final String DOWNLOAD_BASE = "http://tesseract-ocr.googlecode.com/files/";
	static final String OSD_FILENAME = "tesseract-ocr-3.01.osd.tar";
	static final String OSD_FILENAME_BASE = "osd.traineddata";
	static final int MINIMUM_MEAN_CONFIDENCE = 0;

	// Context menu
	private static final int SETTINGS_ID = Menu.FIRST;
	private static final int ABOUT_ID = Menu.FIRST + 1;
	// Options menu, for copy to clipboard
	private static final int OPTIONS_COPY_RECOGNIZED_TEXT_ID = Menu.FIRST;
	private static final int OPTIONS_SHARE_RECOGNIZED_TEXT_ID = Menu.FIRST + 1;

	public static CameraManager cameraManager;
	private CaptureActivityHandler handler;
	private ViewfinderView viewfinderView;
	private SurfaceView surfaceView;
	private SurfaceHolder surfaceHolder;
	private TextView statusViewBottom;
	private TextView statusViewTop;
	private TextView ocrResultView;
	private View cameraButtonView;
	private View resultView;
	private View progressView;
	ProgressDialog progressDialog ;

	private OcrResult lastResult;
	private Bitmap lastBitmap;
	private boolean hasSurface;
	private BeepManager beepManager;
	private TessBaseAPI baseApi;
	private String sourceLanguageCodeOcr;
	private String sourceLanguageReadable;
	private int pageSegmentationMode = TessBaseAPI.PageSegMode.PSM_AUTO_OSD;
	private int ocrEngineMode = TessBaseAPI.OEM_TESSERACT_ONLY;
	private String characterBlacklist;
	private String characterWhitelist;
	private ShutterButton shutterButton;
	private boolean isContinuousModeActive;
	int vcardImageId = 0;
	boolean isEditMode;
	String vcardIdKey = "";
	// Whether we are doing OCR in continuous mode
	private SharedPreferences prefs;
	private OnSharedPreferenceChangeListener listener;
	private ProgressDialog dialog, dialog1;
	private ProgressDialog indeterminateDialog;
	private boolean isEngineReady;
	private boolean isPaused;
	// True if this is the first time the app is being run
	private static boolean isFirstLaunch;

	Handler getHandler() {
		Log.d("API ", "getHandler()");
		return handler;
	}

	TessBaseAPI getBaseApi() {
		Log.d("API ", "tessbasepi");
		return baseApi;
	}

public static CameraManager getCameraManager() {

			return cameraManager;
	}
	public void hideProgressDialog(){
		if(progressDialog !=  null)
			progressDialog.dismiss() ;
	}
	public void showProgressDialog(){

		progressDialog  = new ProgressDialog(NavigationActivity.getContext()) ;
		progressDialog.setTitle(getResources().getString(R.string.loading_title)) ;
		progressDialog.setMessage(getResources().getString(R.string.loading_title)) ;
		progressDialog.setCancelable(false) ;
		progressDialog.setCanceledOnTouchOutside(false) ;
		progressDialog.setIndeterminate(false) ;
		progressDialog.show() ;

	}
public JSONObject decodeString(String ocrResult){

	showProgressDialog();

	String EMAIL = "";
	String phone = "";
	String GSM = "";
	String firstName = "";
	String lastName = "";
	String fullName = "";
	String fonction = "";
	String societe = "";
	String address = "";
	String zipcode = "";
	String city = "";
	String country = "";

	JSONObject object = new JSONObject();

	String text_details;

	// LZR 14/06/2016  loading the list of countries FirstNames LastNames and function ( the json file in assets) used for dictionary
	// if the text scanned contains one of the word in one of these dictionaries, this word will be affected to this variable
	String coutriesString = BipTagFormsManager.loadJSONFromAsset(NavigationActivity.getContext(), "pays.json");
	String namesString = BipTagFormsManager.loadJSONFromAsset(NavigationActivity.getContext(), "prenom.json");
	String lastNamesString = BipTagFormsManager.loadJSONFromAsset(NavigationActivity.getContext(), "nom.json");
	String functionString = BipTagFormsManager.loadJSONFromAsset(NavigationActivity.getContext(), "function.json");
	String key = "nom_en_gb";

	JSONArray countries = null;
	try {
		countries = new JSONArray(coutriesString);
		Log.d(" ocr result ", " length of contries " + countries.length());

	} catch (JSONException e) {

	}

JSONArray functions = null;
	try {
		functions = new JSONArray(functionString);
		Log.d(" ocr result ", " length of contries " + functions.length());

	} catch (JSONException e) {

	}
JSONArray names = null;
	try {
		names = new JSONArray(namesString);
		Log.d(" ocr result ", " length of names " + names.length());

	} catch (JSONException e) {

	}

JSONArray lastNames = null;
	try {
		lastNames = new JSONArray(lastNamesString);
		Log.d(" ocr result ", " length of last names " + lastNames.length());

	} catch (JSONException e) {

	}



	text_details = ocrResult.replaceAll("   ", "\n");
	text_details = text_details.replaceAll("n\n\n", "\n");
	text_details = text_details.replaceAll("\n\n", "\n");
	text_details = text_details.replaceAll("\n ", "");
	//text_details = text_details.replaceAll("-", "");
	text_details = text_details.replaceAll(":", "");
	text_details = text_details.replaceAll("©", "@");
	text_details = text_details.replaceAll("®", "@");
	//text_details = text_details.replace("-", "");
	//text_details = text_details.replaceAll("|", "l");
	//text_details = text_details.replaceAll("\"", "l");
	text_details = text_details.replace("E-MAIL", "");
	text_details = text_details.replace("e-mail", "");
	text_details = text_details.replace("email", "");
	text_details = text_details.replace("EMAIL", "");
	text_details = text_details.replaceAll(".c0m", ".com");

	String[] ocr_result_txt = text_details.split("\n");

	for (int i=0;i<ocr_result_txt.length;i++){
		if (ocr_result_txt[i].contains("@")) {

			//ocr_result_txt[i].replaceAll("'" , ".");

				//Log.d(" ocr result ", " email " + EMAIL);
				EMAIL = ocr_result_txt[i];
				EMAIL = EMAIL.replaceAll("," , ".");
				EMAIL = EMAIL.replace(" ", "");
				EMAIL = EMAIL.replace("\\", "l");
				EMAIL = EMAIL.replace("/", "l");
				EMAIL = EMAIL.replace("'", "");
		break;
		}

	}
	for (int i = ocr_result_txt.length-1;i>0;i--) {

		Pattern phoneePattern = Pattern.compile("((\\d{1}))");
		Matcher phoneeMatcher = phoneePattern.matcher(ocr_result_txt[i]);
		if (phoneeMatcher.find()) {
			String phone1 = ocr_result_txt[i];
			phone1 = phone1.replaceAll(" ", "");
			phone1 = phone1.replaceAll("-", "");
			phone1 = phone1.replaceAll(":", "");
			phone1 = phone1.replaceAll("\\.", "");
			phone1 = phone1.replaceAll("\\(", "");
			phone1 = phone1.replaceAll("\\)", "");
			phone1 = phone1.replaceAll("\\+", "");
			Pattern phonePattern = Pattern.compile("((\\d{8,11}))");
			Matcher phoneMatcher = phonePattern.matcher(phone1);
			if (phoneMatcher.find()) {
				GSM = phoneMatcher.group(1);
				String phone2 = ocr_result_txt[i];
				phone2 = phone2.replaceAll(" ", "");
				phone2 = phone2.replaceAll("-", "");
				phone2 = phone2.replaceAll(":", "");
				phone2 = phone2.replaceAll("\\.", "");
				GSM = phone2;

				break;
				//text = text.replaceAll(ocr_result_txt[i],"");


			}
		}

	}



//matching phone
	Pattern phoneePattern = Pattern.compile("((\\d{1}))");

	for (int i = ocr_result_txt.length-1;i>0;i--) {

	Matcher phoneeMatcher = phoneePattern.matcher(ocr_result_txt[i]);
	if (phoneeMatcher.find()) {
		String phone1 = ocr_result_txt[i];
		phone1 = phone1.replaceAll(" ", "");
		phone1 = phone1.replaceAll("-", "");
		phone1 = phone1.replaceAll(":", "");
		phone1 = phone1.replaceAll("\\.", "");
		phone1 = phone1.replaceAll("\\[", "\\(");
		phone1 = phone1.replaceAll("\\]", "\\)");
		//phone1 = phone1.replaceAll("\\+", "");
		Pattern phonePattern = Pattern.compile("( *\\+? *[0-9]{0,4} *\\(? *\\+? *[0-9]{0,3} *\\)? *\\d{6,11})"); //

		// *\\+? : "+" 0 ou une occurance  								+											+
		//[0-9]{0,4] : un chiffre avec occurence de 0 à 4 				33					0033					216					00
		//  *\\(? : ( 0 ou une occurence 								(					(
		//*[0-9]{0,3} : un chiffre avec occurence de 0 à 3 				0					0											216
		//*\\)?  : ) 0 ou une occurence 								)					)
		// *\\d{6,11} : un chiffre avec occurence de 6 à 12 			111 111 111			111 111 111	 			22 222 222			22 222 222
			Matcher phoneMatcher = phonePattern.matcher(phone1);
		if (phoneMatcher.find()) {
			if(phoneMatcher.group(1).length()>8) {
				phone = phoneMatcher.group(1);

				address = ocr_result_txt[i - 1];
			}
			//text = text.replaceAll(ocr_result_txt[i],"");


		}
	}

}


//matching zipcode
	Pattern zipPattern = Pattern.compile("(\\d{4,6})");
	for (int j = ocr_result_txt.length-1;j> 0;j--) {

	Matcher zipMatcher = zipPattern.matcher(ocr_result_txt[j]);
		if (zipMatcher.find()) {
			String zip = zipMatcher.group(1);
			if (!phone.contains(zip) ) {
				zipcode = zip;
			if (address.length()==0) {
				address = ocr_result_txt[j];
				city = address.replace(zip, " ");
	if(city.split(" ").length>1) {
	city = city.split(" ")[city.split(" ").length - 1];
	}
			}else
				if (!address.contains(zipcode)){
					address = ocr_result_txt[j];
					city = address.replace(zip, " ");
				if (city.split(" ").length>0)	city = city.split(" ")[city.split(" ").length - 1];
			}
				//	address = address.replaceAll(zipcode, "");
				//	text=text.replaceAll(ocr_result_txt[i],"");
				break;
			}
		}

	}




//matching country

for (int k = 0; k<ocr_result_txt.length;k++) {
	for (int j = 0; j < countries.length(); j++) {
		try {
			//Log.d(" ocr result ", " for countries " + countries.getJSONObject(j).getString("alpha3"));

			if (ocr_result_txt[k].contains(countries.getJSONObject(j).getString("nom_fr_fr"))) {
				Log.d(" ocr result ", " if contries ");
				country = countries.getJSONObject(j).getString("nom_fr_fr");
				address = ocr_result_txt[k];
				//  j=payys.length;
				if (ocr_result_txt[k].split(" ").length>2) {
					city = ocr_result_txt[k].split(" ")[ocr_result_txt[k].split(" ").length - 2];
					Log.d("ocr result ", " country  " + country);
					break;
				}
				else{
					city = ocr_result_txt[k].split(" ")[ocr_result_txt[k].split(" ").length - 1];
					Log.d("ocr result ", " country  " + country);
					break;
				}
			}
			if ( ocr_result_txt[k].contains(countries.getJSONObject(j).getString("nom_en_gb"))) {
				Log.d(" ocr result ", " if contries ");
				country = countries.getJSONObject(j).getString("nom_en_gb");
				address = ocr_result_txt[k];
				//  j=payys.length;
				if (ocr_result_txt[k].split(" ").length>2) {
					city = ocr_result_txt[k].split(" ")[ocr_result_txt[k].split(" ").length - 2];
					Log.d("ocr result ", " country  " + country);
					break;
				}
				else{
					city = ocr_result_txt[k].split(" ")[ocr_result_txt[k].split(" ").length - 1];
					Log.d("ocr result ", " country  " + country);
					break;
				}
			}
		} catch (JSONException e) {
		//	e.printStackTrace();
		}
	}
}


	//for (int k = 0; k<ocr_result_txt.length;k++) {
	//matching name
		for (int j = 0; j < names.length(); j++) {
		try {
			Log.d(" ocr result ", " for names " + names.getJSONObject(j).getString("prenom"));

		//	while (!ocr_result_txt[k].contains(names.getJSONObject(j).getString("prenom")) ) {
				Log.d(" ocr result ", " if name ");
				//firstName = names.getJSONObject(j).getString("prenom");
			//	lastName = ocr_result_txt[k].replaceAll(firstName, "");
				/*if (k<ocr_result_txt.length-1) {
					fonction = ocr_result_txt[k + 1];
				}*/
//if (names.getJSONObject(j).getString("prenom").indexOf(ocrResult)!=-1) {
if (ocrResult.indexOf(names.getJSONObject(j).getString("prenom"))!=-1) {

	firstName = names.getJSONObject(j).getString("prenom");

	Log.d("ocr result ", " name  " + firstName);
	break;
}
				break;
		//	}
			/*if (firstName.length()==0){
			if (ocr_result_txt[k].contains(lastNames.getJSONObject(j).getString("nom"))) {
				Log.d(" ocr result ", " if name ");
				lastName = lastNames.getJSONObject(j).getString("nom");
				firstName = ocr_result_txt[k].replaceAll(lastName, "");
				fonction = ocr_result_txt[k + 1];

				Log.d("ocr result ", " name  " + firstName);
				break;
			}
			}*/
		} catch (JSONException e) {
			//e.printStackTrace();
		}
	}


		//matching LastName
		for (int j = 0; j < lastNames.length(); j++) {
		try {
			Log.d(" ocr result ", " for names " + lastNames.getJSONObject(j).getString("prenom"));

		//	while (!ocr_result_txt[k].contains(names.getJSONObject(j).getString("prenom")) ) {
				Log.d(" ocr result ", " if name ");
				//firstName = names.getJSONObject(j).getString("prenom");
			//	lastName = ocr_result_txt[k].replaceAll(firstName, "");
				/*if (k<ocr_result_txt.length-1) {
					fonction = ocr_result_txt[k + 1];
				}*/
//if (names.getJSONObject(j).getString("prenom").indexOf(ocrResult)!=-1) {
if (ocrResult.indexOf(lastNames.getJSONObject(j).getString("nom"))!=-1) {

	lastName = lastNames.getJSONObject(j).getString("nom");

	Log.d("ocr result ", " name  " + lastName);
	break;
}
				break;
		//	}
			/*if (firstName.length()==0){
			if (ocr_result_txt[k].contains(lastNames.getJSONObject(j).getString("nom"))) {
				Log.d(" ocr result ", " if name ");
				lastName = lastNames.getJSONObject(j).getString("nom");
				firstName = ocr_result_txt[k].replaceAll(lastName, "");
				fonction = ocr_result_txt[k + 1];

				Log.d("ocr result ", " name  " + firstName);
				break;
			}
			}*/
		} catch (JSONException e) {
		//	e.printStackTrace();
		}
	}
//}




	//matching function
		for (int j = 0; j < functions.length(); j++) {
		try {
			Log.d(" ocr result ", " for names " + functions.getJSONObject(j).getString("function"));

		//	while (!ocr_result_txt[k].contains(names.getJSONObject(j).getString("prenom")) ) {
				Log.d(" ocr result ", " if function ");
		if (ocrResult.indexOf(functions.getJSONObject(j).getString("function"))!=-1) {

	fonction = functions.getJSONObject(j).getString("function");

	Log.d("ocr result ", " fonction  " + fonction);
	break;
}
				break;
		} catch (JSONException e) {
			//e.printStackTrace();
		}
	}
//}




//  LZR 22 04 2016  obtenir le text scanné
	String text_result = "";
	for(int i=0; i<ocr_result_txt.length;i++){

		text_result = text_result + "\n" +ocr_result_txt[i];
	}



	int num=0;
//  LZR 22 04 2016 eliminer les variables non null du text scanné
	for (int k = 0; k<ocr_result_txt.length;k++) {
	if (ocr_result_txt[k].contains("@") & EMAIL!=null){
		//  LZR 22 04 2016  si le text scanné contien l'email et il est affecté à sa variable, le champs email sera éliminé
		text_result = text_result.replace(ocr_result_txt[k],"");
		Log.d(" text result ", " contains email ");
		num = num+1;
	}
if (ocr_result_txt[k].contains(address)&address!=null){
	//  LZR 22 04 2016 si le text scanné contien l'adress et il est affecté à sa variable, le champs adresse sera éliminé

	text_result = text_result.replace(ocr_result_txt[k], "");
	Log.d(" text result ", " contains address ");
	num++;
	}
if (ocr_result_txt[k].contains(zipcode)&zipcode!=null){
	text_result = text_result.replace(ocr_result_txt[k], "");
	Log.d(" text result ", " contains zipcode");
	num++;
}
if (ocr_result_txt[k].contains(country)&country!=null){
	text_result  = text_result .replace(ocr_result_txt[k], "");
	Log.d(" text result ", " contains country");
	num++;
}
if (ocr_result_txt[k].contains(phone)&phone!=null){
	text_result  = text_result .replace(ocr_result_txt[k], "");
	Log.d(" text result ", " contains phone");
	num++;
}
if (ocr_result_txt[k].contains(firstName)&firstName!=null){
	text_result  = text_result .replace(ocr_result_txt[k], "");
	Log.d(" text result ", " contains firstname");
	num++;
}
if (ocr_result_txt[k].contains(fonction)&fonction!=null){
	text_result  = text_result .replace(ocr_result_txt[k], "");
	Log.d(" text result ", " contains firstname");
	num++;
}
//  LZR 22 04 2016  jusqu'a obtenir le text restant apres avoir éliminer les variables non null
}
		text_result = text_result.replaceAll(" \n", "");
		text_result = text_result.replaceAll("\n\n", "\n");
		text_result = text_result.replaceAll("\n ", "");
	text_result = text_result.replaceAll("  ", " ");
	String[] text_result_txt = text_result.split("\n");
	Log.d(" length " , " : " +text_result_txt.length + " num " + num );


for (int i=0; i<text_result_txt.length;i++) {
Log.d(" text result " , text_result_txt[i] + " i " + i );
}

// LZR 22 04 2016 : d'apres les cartes visites qu'on a j'ai remarqué que dans la plus part la première ligne contient le nom de la société ( ce n'est pas obligatoirement  puisqu'il n y a pas un modèle fixe pour les cartes visites )
// la deuxieme ligne généralement le Nom et prenom suivi en 3eme ligne par la fonction pour cela vous trouvez dans mon code le champs function prend la ligne qui suit celui contenant le nom prenom
// aussi pour l'adresse dans la plupart des cate visite on trouve l'adresse et dans la ligne suivante le numéro de téléphone et puisque c'est facile de détecter le numéro du téléphone vous trouvez dans mon code le champs address prend la ligne qui precède la ligne contenant le téléphone

	// Societe [0]
	// Nom Prenom [1]
	//function [2]
	// address [3]
	//Telephone [4]
	//email


	if (text_result_txt.length > 4){
	//  LZR 22 04 2016  si apres le matching le text restant est de + 4 lignes
			if(fonction.length()==0) {
			fonction = ocr_result_txt[2];
		}
		if (text_result_txt[1].split(" ").length>1){
		if (firstName.length()>0) {
			fullName = text_result_txt[1];
			firstName = fullName.split(" ")[0];
			fullName = fullName.replaceAll(firstName,"");
			lastName = fullName;
		}
		}
		societe = text_result_txt[0];
		if (address.length()==0){
			address = text_result_txt[3];
			city = address.split(" ")[city.split(" ").length - 1];

		}
	}




	if (text_result_txt.length == 4){
	if (fonction.length()==0){	fonction = text_result_txt[2];}
		if (text_result_txt[1].split(" ").length>1){
		if (firstName.length()==0) {
			fullName = text_result_txt[1];

			firstName = fullName.split(" ")[0];
			fullName = fullName.replaceAll(firstName, "");
			lastName = fullName;
		}
		}
		societe = text_result_txt[0];
		if (zipcode.length()==0){
			address = text_result_txt[3];
			city = address.split(" ")[city.split(" ").length - 1];

		}

	}



// Societe [0]
	// Nom Prenom [1]
	//function [2]
	// address [3]
	//Telephone [4]
	//email

	if (text_result_txt.length == 3) {

		// Nom Prenom [0]
		//function [1]
		// address [2]
//ou
		// societe[0]
		// Nom Prenom [1]
		//function [2]
		if (address.length() == 0) {
			address = text_result_txt[2];
			city = address.split(" ")[city.split(" ").length - 1];
		}
		if (fonction.length()==0){
			fonction = text_result_txt[1];
		}
		if (text_result_txt[0].split(" ").length > 1) {
			if (firstName.length()==0) {
				fullName = text_result_txt[1];
				firstName = fullName.split(" ")[0];
				fullName = fullName.replaceAll(firstName, "");
				lastName = fullName;
			}
		}else {
				if (fonction.length()==0){	fonction = text_result_txt[2];}
				if (text_result_txt[1].split(" ").length > 1) {
				if (firstName.length()==0) {
				fullName = text_result_txt[1];
				firstName = fullName.split(" ")[0];
				fullName = fullName.replaceAll(firstName, "");
				lastName = fullName;
				}
				}


			societe = text_result_txt[0];
		}
	}
// Nom Prenom [0]
	//function [1]
	if (text_result_txt.length == 2){
		if (fonction.length()==0){fonction = text_result_txt[1];}
		if (text_result_txt[0].split(" ").length>1){
		if (firstName.length()==0) {
			fullName = text_result_txt[0];
			firstName = fullName.split(" ")[0];
			fullName = fullName.replaceAll(firstName, "");
			lastName = fullName;
		}
		}
	}
	// Nom Prenom [0]
//ou
	//societe [0]
if (text_result_txt.length == 1){

		if (text_result_txt[0].split(" ").length>1){
			fullName = text_result_txt[0];
			firstName = fullName.split(" ")[0];
			fullName = fullName.replaceAll(firstName,"");
			lastName = fullName;

		}
	}

	//  LZR 22 04 2016 dans les cartes visites tunisienne on trouve la première ligne contenant le nom de la société en arabe suivi de celui en francais
	// si le champs société est vide et que le text restant apres avoir faire le matching contient deux lignes on affecte le deuxieme a la société s'il contient une seule ligne cette ligne sera affecté à la société
	//  loguique n'est pas peut etre idéal mais on peut pas avoir un matching parfait pour tout type de carte de visite
if (societe.length()==0){
	Log.d(" result text ", " length 0");
	if (ocr_result_txt.length>1) {
		societe = ocr_result_txt[1];

	}
	if (ocr_result_txt.length==1) {
		societe = ocr_result_txt[0];
		Log.d(" result text ", " length 0 " + societe);

	}
}

//}


	try {
		if (dialog1!=null){
			dialog1.dismiss();

		}


		object.put("ocr", ocrResult);
		object.put("EMAIL", EMAIL); //ok
		object.put("PHONE", phone); //ok
		object.put("FNAME", firstName);
		object.put("LNAME", lastName);
		object.put("TITLE", fonction);
		object.put("COMPANY", societe);
		object.put("ADDRESS", address); //ok
		object.put("ZIPCODE", zipcode); //ok
		object.put("CITY", city); // ok
		object.put("COUNTRY", country);//ok
		//Log.d("lines details contact: First name  ",firstName +" last name " +lastName + " function "+fonction + " societe " + societe+ " adresse " + address+ " ZipCode " +zipcode+ " city "+ city + "country" +country);
	} catch (JSONException e) {
		// TODO: handle exception
	}

	try {
		Log.d(" ocr result ", " capture activity editocr " + object.getString("ocr"));
	} catch (JSONException e) {
		//ceci est exactement comme le Log
		e.printStackTrace();
	}
	return object;
}



	@Override
	public void onCreate(Bundle icicle) {
		super.onCreate(icicle);
		System.gc();
		Log.d("API ", "captureActivity");
		setDefaultPreferences();

		isEditMode = getIntent().getBooleanExtra("is_edit", false);

		Intent iin = getIntent();
		Bundle b = iin.getExtras();
		int Mode = b.getInt("EditMode");
		// LZR 22 04 2016 : Move vien de la ligne 605 de la class EditFormStep1 et FillingformStep1 lors du lancement de l'activity on lui attribue une variable et si on vient de EditFormStep sa variable doit contenir 6 si de FillingFormStep 1 elle doit contenir 5
		if (Mode == 6) {
			vcardIdKey = vcardIdKeyEdit;
		} else if (Mode == 5) {
			vcardIdKey = vcardIdKeyCreate;
		}

		//vcardImageId = Integer.parseInt(String.valueOf(BiptagUserManger.getUserValue(vcardIdKey , "string" , NavigationActivity.getContext()))) ;

		//int jsonImageVcard =
		vcardImageId = Integer.parseInt(String.valueOf(BiptagUserManger.getUserValue(vcardIdKey, "int", NavigationActivity.getContext())));
		/*try {
			JSONArray jsonArray = new JSONArray(jsonImageVcard) ;

			vcardImageId = jsonArray.getInt(0) ;
			Log.d("image "," " + vcardImageId );
		}catch (JSONException e){

		}*/

		Log.d("image ", " vcardimageid ");
		// LZR 22 04 2016 source https://jmandroid.googlecode.com/svn/trunk/JJisho/src/com/jm/jisho/ocr/CaptureActivity.java
		Window window = getWindow();
		window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
		setContentView(R.layout.capture);
		viewfinderView = (ViewfinderView) findViewById(R.id.viewfinder_view);
		cameraButtonView = findViewById(R.id.camera_button_view);
		resultView = findViewById(R.id.result_view);

		statusViewBottom = (TextView) findViewById(R.id.status_view_bottom);
		registerForContextMenu(statusViewBottom);
		statusViewTop = (TextView) findViewById(R.id.status_view_top);
		registerForContextMenu(statusViewTop);

		handler = null;
		lastResult = null;
		hasSurface = false;
		beepManager = new BeepManager(this);

		// Camera shutter button
		if (DISPLAY_SHUTTER_BUTTON) {
			shutterButton = (ShutterButton) findViewById(R.id.shutter_button);
			shutterButton.setOnShutterButtonListener(this);
		}


	    /*Button focusButton = (Button) findViewById(R.id.focus_button) ;
	    focusButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
			cameraManager.requestAutoFocus(50L) ;

			}
		}) ;
*/

		//ocr_result_text_view
		ocrResultView = (TextView) findViewById(R.id.ocr_result_text_view);
		registerForContextMenu(ocrResultView);

		progressView = (View) findViewById(R.id.indeterminate_progress_indicator_view);

		cameraManager = new CameraManager(getApplication());
		Log.d(" ocr ", " cameramanager ");
		viewfinderView.setCameraManager(cameraManager);

		// Set listener to change the size of the viewfinder rectangle.
		viewfinderView.setOnTouchListener(new View.OnTouchListener() {
			int lastX = -1;
			int lastY = -1;

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				switch (event.getAction()) {
					case MotionEvent.ACTION_DOWN:
						lastX = -1;
						lastY = -1;
						return true;
					case MotionEvent.ACTION_MOVE:
						int currentX = (int) event.getX();
						int currentY = (int) event.getY();

						try {

							Rect rect = cameraManager.getFramingRect();
							//Log.d(" ocr ", " cameramanager.getfragmingrect ");

							final int BUFFER = 50;
							final int BIG_BUFFER = 60;
							if (lastX >= 0) {
								// Adjust the size of the viewfinder rectangle.
								//Check if the touch event occurs in the corner areas first,
								//because the regions overlap.
								if (((currentX >= rect.left - BIG_BUFFER && currentX <= rect.left + BIG_BUFFER) || (lastX >= rect.left - BIG_BUFFER && lastX <= rect.left + BIG_BUFFER))
										&& ((currentY <= rect.top + BIG_BUFFER && currentY >= rect.top - BIG_BUFFER) || (lastY <= rect.top + BIG_BUFFER && lastY >= rect.top - BIG_BUFFER))) {
									// Top left corner: adjust both top and left sides
									cameraManager.adjustFramingRect(2 * (lastX - currentX), 2 * (lastY - currentY));
									viewfinderView.removeResultText();
								} else if (((currentX >= rect.right - BIG_BUFFER && currentX <= rect.right + BIG_BUFFER) || (lastX >= rect.right - BIG_BUFFER && lastX <= rect.right + BIG_BUFFER))
										&& ((currentY <= rect.top + BIG_BUFFER && currentY >= rect.top - BIG_BUFFER) || (lastY <= rect.top + BIG_BUFFER && lastY >= rect.top - BIG_BUFFER))) {
									// Top right corner: adjust both top and right sides
									cameraManager.adjustFramingRect(2 * (currentX - lastX), 2 * (lastY - currentY));
									viewfinderView.removeResultText();
								} else if (((currentX >= rect.left - BIG_BUFFER && currentX <= rect.left + BIG_BUFFER) || (lastX >= rect.left - BIG_BUFFER && lastX <= rect.left + BIG_BUFFER))
										&& ((currentY <= rect.bottom + BIG_BUFFER && currentY >= rect.bottom - BIG_BUFFER) || (lastY <= rect.bottom + BIG_BUFFER && lastY >= rect.bottom - BIG_BUFFER))) {
									// Bottom left corner: adjust both bottom and left sides
									cameraManager.adjustFramingRect(2 * (lastX - currentX), 2 * (currentY - lastY));
									viewfinderView.removeResultText();
								} else if (((currentX >= rect.right - BIG_BUFFER && currentX <= rect.right + BIG_BUFFER) || (lastX >= rect.right - BIG_BUFFER && lastX <= rect.right + BIG_BUFFER))
										&& ((currentY <= rect.bottom + BIG_BUFFER && currentY >= rect.bottom - BIG_BUFFER) || (lastY <= rect.bottom + BIG_BUFFER && lastY >= rect.bottom - BIG_BUFFER))) {
									// Bottom right corner: adjust both bottom and right sides
									cameraManager.adjustFramingRect(2 * (currentX - lastX), 2 * (currentY - lastY));
									viewfinderView.removeResultText();
								} else if (((currentX >= rect.left - BUFFER && currentX <= rect.left + BUFFER) || (lastX >= rect.left - BUFFER && lastX <= rect.left + BUFFER))
										&& ((currentY <= rect.bottom && currentY >= rect.top) || (lastY <= rect.bottom && lastY >= rect.top))) {
									// Adjusting left side: event falls within BUFFER pixels of left side, and between top and bottom side limits
									cameraManager.adjustFramingRect(2 * (lastX - currentX), 0);
									viewfinderView.removeResultText();
								} else if (((currentX >= rect.right - BUFFER && currentX <= rect.right + BUFFER) || (lastX >= rect.right - BUFFER && lastX <= rect.right + BUFFER))
										&& ((currentY <= rect.bottom && currentY >= rect.top) || (lastY <= rect.bottom && lastY >= rect.top))) {
									// Adjusting right side: event falls within BUFFER pixels of right side, and between top and bottom side limits
									cameraManager.adjustFramingRect(2 * (currentX - lastX), 0);
									viewfinderView.removeResultText();
								} else if (((currentY <= rect.top + BUFFER && currentY >= rect.top - BUFFER) || (lastY <= rect.top + BUFFER && lastY >= rect.top - BUFFER))
										&& ((currentX <= rect.right && currentX >= rect.left) || (lastX <= rect.right && lastX >= rect.left))) {
									// Adjusting top side: event falls within BUFFER pixels of top side, and between left and right side limits
									cameraManager.adjustFramingRect(0, 2 * (lastY - currentY));
									viewfinderView.removeResultText();
								} else if (((currentY <= rect.bottom + BUFFER && currentY >= rect.bottom - BUFFER) || (lastY <= rect.bottom + BUFFER && lastY >= rect.bottom - BUFFER))
										&& ((currentX <= rect.right && currentX >= rect.left) || (lastX <= rect.right && lastX >= rect.left))) {
									// Adjusting bottom side: event falls within BUFFER pixels of bottom side, and between left and right side limits
									cameraManager.adjustFramingRect(0, 2 * (currentY - lastY));
									viewfinderView.removeResultText();
								}
							}
						} catch (NullPointerException e) {
							//Log.e(TAG, "Framing rect not available", e);
						}
						v.invalidate();
						lastX = currentX;
						lastY = currentY;
						return true;
					case MotionEvent.ACTION_UP:
						lastX = -1;
						lastY = -1;
						return true;
				}
				return false;
			}
		});

		isEngineReady = false;
	}

	@Override
	protected void onStop() {
		super.onStop();
		if (handler!=null){

			handler.hideToast(handler.toast);
		}
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
		BiptagUserManger.clearField(NavigationActivity.getContext(),"ocrResult");
		this.finish();
	}

	@Override
	protected void onResume() {
		super.onResume();
		System.gc();
		resetStatusView();

		String previousSourceLanguageCodeOcr = sourceLanguageCodeOcr;
		int previousOcrEngineMode = ocrEngineMode;

		retrievePreferences();

		// Set up the camera preview surface.
		surfaceView = (SurfaceView) findViewById(R.id.preview_view);
		surfaceHolder = surfaceView.getHolder();
		if (!hasSurface) {
			surfaceHolder.addCallback(this);
			surfaceHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
		}

		// Do OCR engine initialization, if necessary
		boolean doNewInit = (baseApi == null) || !sourceLanguageCodeOcr.equals(previousSourceLanguageCodeOcr) ||
				ocrEngineMode != previousOcrEngineMode;

//		Bundle bundle = getIntent().getExtras();
//String donewinit = bundle.getString("newinit");

		//  boolean donewinit =
		if (doNewInit) {
			// Initialize the OCR engine
			Log.d(" ocr ", " donewinit ");
			File storageDirectory = getStorageDirectory();
			if (storageDirectory != null) {
				Log.d(" ocr ", " storage not nul ");
				initOcrEngine(storageDirectory, sourceLanguageCodeOcr, sourceLanguageReadable);

			}
		} else {
			// We already have the engine initialized, so just start the camera.
			resumeOCR();
		}
	}

	//  Method to start or restart recognition after the OCR engine has been initialized,
	//or after the app regains focus. Sets state related settings and OCR engine parameters,
	//and requests camera initialization.

	void resumeOCR() {
		//Log.d(TAG, "resumeOCR()");

		// This method is called when Tesseract has already been successfully initialized, so set
		// isEngineReady = true here.
		isEngineReady = true;

		isPaused = false;

		if (handler != null) {
			handler.resetState();
		}
		if (baseApi != null) {
			baseApi.setPageSegMode(pageSegmentationMode);
			baseApi.setVariable(TessBaseAPI.VAR_CHAR_BLACKLIST, characterBlacklist);
			baseApi.setVariable(TessBaseAPI.VAR_CHAR_WHITELIST, characterWhitelist);
		}

		if (hasSurface) {
			// The activity was paused but not stopped, so the surface still exists. Therefore
			// surfaceCreated() won't be called, so init the camera here.
			initCamera(surfaceHolder);
		}
	}


	// Called when the shutter button is pressed in continuous mode. */
	void onShutterButtonPressContinuous() {
		isPaused = true;
		handler.stop();
		if (handler.toast!=null) {
			handler.toast.cancel();
		}
		beepManager.playBeepSoundAndVibrate();
		if (lastResult != null) {

			JSONObject object = handleOcrDecode(lastResult);
			finish();

		} else {

//			resumeContinuousDecoding();

		}
	}

	// Called to resume recognition
	@SuppressWarnings("unused")
	void resumeContinuousDecoding() {
		isPaused = false;
		resetStatusView();
		setStatusViewForContinuous();
		DecodeHandler.resetDecodeState();

		if (shutterButton != null && DISPLAY_SHUTTER_BUTTON) {
			shutterButton.setVisibility(View.VISIBLE);
		}
	}

	@Override
	public void surfaceCreated(SurfaceHolder holder) {
		// Log.d(TAG, "surfaceCreated()");

		if (holder == null) {
			// Log.e(TAG, "surfaceCreated gave us a null surface");
		}

		// Only initialize the camera if the OCR engine is ready to go.
		if (!hasSurface && isEngineReady) {
			// Log.d(TAG, "surfaceCreated(): calling initCamera()...");
			initCamera(holder);
		}
		hasSurface = true;
	}

	// Initializes the camera and starts the handler to begin previewing.
	private void initCamera(SurfaceHolder surfaceHolder) {
		Log.d(TAG, "initCamera()");
		if (surfaceHolder == null) {
			throw new IllegalStateException("No Surface Holder provided");
		}
		try {

			// Open and initialize the camera
			cameraManager.openDriver(surfaceHolder);
			Log.d(" ocr ", " ocr camera init");
			// Creating the handler starts the preview,
			//which can also throw a RuntimeException.
			handler = new CaptureActivityHandler(this, cameraManager, isContinuousModeActive);
		if (handler.toast!=null){	handler.toast.cancel();}
		} catch (IOException ioe) {
			showErrorMessage("Error", "Could not initialize camera. Please try restarting device.");
		} catch (RuntimeException e) {
			// Barcode Scanner has seen crashes in the wild of this variety:
			// java.?lang.?RuntimeException: Fail to connect to camera service
			showErrorMessage("Error", "Could not initialize camera. Please try restarting device.");
		}
	}

	@Override
	protected void onPause() {
		if (handler != null) {
			handler.quitSynchronously();
		}

		// Stop using the camera, to avoid conflicting with other camera-based apps
		cameraManager.closeDriver();

		if (!hasSurface) {
			SurfaceView surfaceView = (SurfaceView) findViewById(R.id.preview_view);
			SurfaceHolder surfaceHolder = surfaceView.getHolder();
			surfaceHolder.removeCallback(this);
		}
		super.onPause();
	}

	void stopHandler() {
		if (handler != null) {
			handler.stop();
		}
	}

	@Override
	protected void onDestroy() {
		if (baseApi != null) {
			baseApi.end();
		}
		super.onDestroy();
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {

			// First check if we're paused in continuous mode, and if so, just unpause.
			if (isPaused) {
				//Log.d(TAG, "only resuming continuous recognition, not quitting...");
				//resumeContinuousDecoding();
				return true;
			}

			// Exit the app if we're not viewing an OCR result.
			if (lastResult == null) {
				setResult(RESULT_CANCELED);
				finish();
				return true;
			} else {
				// Go back to previewing in regular OCR mode.
				resetStatusView();
				if (handler != null) {
					handler.sendEmptyMessage(R.id.restart_preview);
				}
				return true;
			}
		} else if (keyCode == KeyEvent.KEYCODE_CAMERA) {
			if (isContinuousModeActive) {
				onShutterButtonPressContinuous();
			} else {
				handler.hardwareShutterButtonClick();
			}
			return true;
		} else if (keyCode == KeyEvent.KEYCODE_FOCUS) {
			// Only perform autofocus if user is not holding down the button.
			if (event.getRepeatCount() == 0) {
				cameraManager.requestAutoFocus(50L);
			}
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		super.onCreateOptionsMenu(menu);
		//menu.add(0, SETTINGS_ID, 0, "Settings").setIcon(android.R.drawable.ic_menu_preferences);
	//	menu.add(0, ABOUT_ID, 0, "About").setIcon(android.R.drawable.ic_menu_info_details);
		return true;
	}


	public void surfaceDestroyed(SurfaceHolder holder) {
		hasSurface = false;
	}

	public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
	}

	//Sets the necessary language code values for the given OCR language.
	private boolean setSourceLanguage(String languageCode) {
		Log.d(" ocr ", " languagecode " + languageCode);
		sourceLanguageCodeOcr = languageCode;
		sourceLanguageReadable = LanguageCodeHelper.getOcrLanguageName(this, languageCode);
		return true;
	}

	// Finds the proper location on the SD card where we canave files.
	private File getStorageDirectory() {

		String state = null;
		try {
			state = Environment.getExternalStorageState();
		} catch (RuntimeException e) {
			//Log.e(TAG, "Is the SD card visible?", e);
			showErrorMessage("Error", "Required external storage (such as an SD card) is unavailable.");
		}

		if (Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {

			try {
				return getExternalFilesDir(Environment.MEDIA_MOUNTED);
			} catch (NullPointerException e) {
				// We get an error here if the SD card is visible, but full
				//Log.e(TAG, "External storage is unavailable");
				showErrorMessage("Error", "Required external storage (such as an SD card) is full or unavailable.");
			}

		} else if (Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
			// We can only read the media
			//Log.e(TAG, "External storage is read-only");
			showErrorMessage("Error", "Required external storage (such as an SD card) is unavailable for data storage.");
		} else {
			// Something else is wrong. It may be one of many other states, but all we need
			// to know is we can neither read nor write
			//Log.e(TAG, "External storage is unavailable");
			showErrorMessage("Error", "Required external storage (such as an SD card) is unavailable or corrupted.");
		}
		return null;
	}

	/**
	 * Requests initialization of the OCR engine with the given parameters.
	 *
	 * @param storageRoot  Path to location of the tessdata directory to use
	 * @param languageCode Three-letter ISO 639-3 language code for OCR
	 * @param languageName Name of the language for OCR, for example, "English"
	 */
	private void initOcrEngine(File storageRoot, String languageCode, String languageName) {
		isEngineReady = false;

		// Set up the dialog box for the thermometer-style download progress indicator
		if (dialog != null) {
			dialog.dismiss();
		}
		dialog = new ProgressDialog(this);

		// If we have a language that only runs using Cube, then set the ocrEngineMode to Cube
/*		if (ocrEngineMode != TessBaseAPI.OEM_CUBE_ONLY) {
			for (String s : CUBE_REQUIRED_LANGUAGES) {
				if (s.equals(languageCode)) {
					ocrEngineMode = TessBaseAPI.OEM_CUBE_ONLY;
					SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
					prefs.edit().putString(PreferencesActivity.KEY_OCR_ENGINE_MODE, getOcrEngineModeName()).commit();
				}
			}
		}

		// If our language doesn't support Cube, then set the ocrEngineMode to Tesseract
		if (ocrEngineMode != TessBaseAPI.OEM_TESSERACT_ONLY) {
			boolean cubeOk = false;
			for (String s : CUBE_SUPPORTED_LANGUAGES) {
				if (s.equals(languageCode)) {
					cubeOk = true;
					Log.d(" ocr ", " if languagecode " + cubeOk);
				}
			}
			if (!cubeOk) {
				Log.d(" ocr ", " if not cube ok  ");
				ocrEngineMode = TessBaseAPI.OEM_TESSERACT_ONLY;
				SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
				prefs.edit().putString(PreferencesActivity.KEY_OCR_ENGINE_MODE, getOcrEngineModeName()).commit();
			}
		}
*/
		// Display the name of the OCR engine we're initializing in the indeterminate progress dialog box
		indeterminateDialog = new ProgressDialog(this);
		Log.d(" ocr ", " " + indeterminateDialog.getProgress());
		indeterminateDialog.setTitle(NavigationActivity.getContext().getResources().getString(R.string.dialog_title));
		String ocrEngineModeName = getOcrEngineModeName();
		Log.d(" ocr ", " ocrenginemodeName " + ocrEngineModeName);
/*
		if (ocrEngineModeName.equals("Both")) {
			Log.d(" ocr ", " if ocrenginemodeName " + ocrEngineModeName);

			indeterminateDialog.setMessage(NavigationActivity.getContext().getResources().getString(R.string.indeterminateDialog_message) + languageName + "...");
		} else {
			Log.d(" ocr ", "else ocrenginemodeName " + ocrEngineModeName);
			indeterminateDialog.setMessage(NavigationActivity.getContext().getResources().getString(R.string.indeterminateDialog_message2) + ocrEngineModeName + " OCR engine for " + languageName + "...");
		}
*/		indeterminateDialog.setCancelable(true);
		//  indeterminateDialog.show();

		if (handler != null) {
			handler.quitSynchronously();
		}

		// Disable continuous mode if we're using Cube. This will prevent bad states for devices
		// with low memory that crash when running OCR with Cube, and prevent unwanted delays.
		Log.d(" ocr ", " ocrenginemode " + ocrEngineMode + " ocrEngineModeName " + ocrEngineModeName);
		if (ocrEngineMode == TessBaseAPI.OEM_CUBE_ONLY || ocrEngineMode == TessBaseAPI.OEM_TESSERACT_CUBE_COMBINED) {
			Log.d(" ocr ", " if ocrenginemode " + ocrEngineMode);

			Log.d(" ocr ", "Disabling continuous preview");
			isContinuousModeActive = false;
			SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
			prefs.edit().putBoolean(PreferencesActivity.KEY_CONTINUOUS_PREVIEW, false);
		}

		// Start AsyncTask to install language data and init OCR
		baseApi = new TessBaseAPI();
		Log.d(" ocr ", " ocrinitasynctaskcon " + " languageCode " + languageCode + " languageName " + languageName + " ocrEngineMode " + ocrEngineMode);

		new OcrInitAsyncTask(this, baseApi, dialog, indeterminateDialog, languageCode, languageName, ocrEngineMode)
				.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR,storageRoot.toString());
	}

	/*
	   * Displays information relating to the result of OCR.
	   *
	   * param ocrResult Object representing successful OCR results
	   * return True if a non-null result was received for OCR
	   */
	JSONObject handleOcrDecode(final OcrResult ocrResult) {
		lastResult = ocrResult;

	//LZR : 28-03-2016 : données non reconnues
		if (ocrResult.getText() == null ) {
			Log.d(" ocr Result ", " capture activity ocrresult null ");


			final AlertDialog.Builder builder= new AlertDialog.Builder(this) ;
			builder.setCancelable(false);
			builder.setTitle(NavigationActivity.getContext().getResources().getString(R.string.recognition_fld_title))  ;
			builder.setMessage(NavigationActivity.getContext().getResources().getString(R.string.recognition_fld_desc)) ;
			builder.setPositiveButton(NavigationActivity.getContext().getResources().getString(R.string.ocr_no_data_btn_close), new DialogInterface.OnClickListener() {

				@Override
				public void onClick(DialogInterface dialog, int which) {
					// TODO Auto-generated method stub


					dialog.dismiss();
					finish();


				}
			}) ;

			builder.setNegativeButton(NavigationActivity.getContext().getResources().getString(R.string.ocr_no_data_btn_retake), new DialogInterface.OnClickListener() {

				@Override
				public void onClick(DialogInterface dialog, int which) {
					// TODO Auto-generated method stub


					setShutterButtonClickable(true);
					dialog.dismiss();
				}
			}) ;
			final JSONObject object = new JSONObject();
			builder.setNeutralButton("Saisir", new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {

					try {
						//object.put("ocr", ocrResult);
						object.put("COUNTRY", "");//ok
						object.put("GENDER", "");//ok
						object.put("FNAME", "");//ok
						object.put("LNAME", "");//ok
						object.put("CITY", "");//ok
						object.put("EMAIL", "");//ok
						object.put("COMPANY", "");//ok
						object.put("TITLE", "");//ok
						object.put("ADDRESS", "");//ok
						object.put("ZIPCODE", "");//ok
						object.put("PHONE", "");//ok
						BiptagUserManger.saveUserData("ocrResult", object.toString(), NavigationActivity.getContext());
						BiptagUserManger.clearField(NavigationActivity.getContext(),"ocrResultedit");
						lastBitmap = ocrResult.getBitmapocr();
						save saveImage = new save();
						Log.d("text ", "last bitmap " + lastBitmap.getWidth() + " " + lastBitmap.getHeight());
						saveImage.SaveImage(NavigationActivity.getContext(), lastBitmap);

						Intent iin = getIntent();
						Bundle b = iin.getExtras();
						int Mode = b.getInt("EditMode");
						if (Mode == 6) {
							BiptagUserManger.saveUserData("updated_contact_edit", object.toString(), NavigationActivity.getContext());

						} else if (Mode == 5) {
							BiptagUserManger.saveUserData("updated_contact_creat", object.toString(), NavigationActivity.getContext());

						}




						finish();
					} catch (JSONException e) {
						e.printStackTrace();
					}

				}
			});

			builder.show();
			return object;
		}

		// Turn off capture-related UI elements
		shutterButton.setVisibility(View.GONE);
		statusViewBottom.setVisibility(View.GONE);
		statusViewTop.setVisibility(View.GONE);
		cameraButtonView.setVisibility(View.GONE);
		viewfinderView.setVisibility(View.GONE);

		//resultView.setVisibility(View.VISIBLE);

		//////////////
		// JSONObject


		Log.println(1, "ocr result message", ocrResult.getText().toString());
		dialog1 = new ProgressDialog(NavigationActivity.getContext()) ;
		dialog1.setMessage(NavigationActivity.getContext().getResources().getString(R.string.loading_title)) ;
		dialog1.setCancelable(false) ;
		dialog1.setIndeterminate(false) ;
		dialog1.show() ;


		JSONObject object = decodeString(ocrResult.getText().toString());



		if (object!=null){
					hideProgressDialog();
								}

		lastBitmap = ocrResult.getBitmap();

		Intent intent = getIntent();
		intent.putExtra("ocr_result", object.toString());
		intent.putExtra("ocr_origin", ocrResult.getText().toString());
		BiptagUserManger.saveUserData("ocrResult",object.toString(),NavigationActivity.getContext());
		BiptagUserManger.saveUserData("ocrOrigin",ocrResult.getText().toString(),NavigationActivity.getContext());
		Log.d("ocr result", " ocr origine " + ocrResult.getText().toString());
		Log.d("ocr result", " ocr origine " + object.toString());
		setResult(600, intent);
		save saveImage = new save();
		lastBitmap = ocrResult.getBitmap();

		Log.d("text ", "last bitmap " + lastBitmap.getWidth() + " " + lastBitmap.getHeight());
		saveImage.SaveImage(this, lastBitmap);
		//GalleryImageFragment galleryImageFragment = new GalleryImageFragment();
		//galleryImageFragment.getView();
/*
		BTImage image = new BTImage(getApplication());


		int agent_id = Integer.parseInt(String.valueOf(BiptagUserManger.getUserValue("BA_ID", "int", NavigationActivity.getContext())));

		Calendar c = Calendar.getInstance();
		SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
		String formattedDate = df.format(c.getTime());
		image.setAgentId(agent_id);
		image.setImageDate(formattedDate);
		image.setResponseId(0);
		image.setIsSent(-1);
		BTImageDataSource dataSource = new BTImageDataSource(NavigationActivity.getContext());

		image.setBitmap(lastBitmap);
		Log.d("image ", "saving image");
		image.saveBitmap();
		Log.d("image ", "image saved");
		BTImageDataSource imageDataSource = new BTImageDataSource(getApplicationContext());
		long id = imageDataSource.saveImage(image);
		int vcardimage = (int) imageDataSource.saveImage(image);
		String key = "";
		Intent iin = getIntent();
		Bundle b = iin.getExtras();
		int Mode = b.getInt("EditMode");
		Log.d("Image ", "mode : " + Mode);
		if (Mode == 5) {
			key = createImageArrayUserPreferenceName;

			Log.d("Image ", key);
		} else if (Mode == 6) {
			key = editImageArrayUserPreferenceName;
			Log.d("Image ", key);
		}


		Log.d("Image ", "Key " + key);

		BTImage dbImage = dataSource.selectImagesWithId(vcardImageId, agent_id);
		String imageJsonString = String.valueOf(BiptagUserManger.getUserValue(key, "string", NavigationActivity.getContext()));

		JSONArray imagesJson = null;
		int oldIndex = -1;
		try {
			if (imageJsonString.length() != 0) {
				imagesJson = new JSONArray(imageJsonString);
			} else {
				imagesJson = new JSONArray();
			}
			//  vcardImageId  = (int) dataSource.saveImage(image) ;
			//  for
			for (int i = 0; i <= imagesJson.length(); i++) {
				Log.d("image ", " json " + imagesJson.getInt(i) + " vcard " + vcardImageId);
				if (vcardImageId == imagesJson.getInt(i)) {
					oldIndex = i;
				}
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block

		}
		//

		if (dbImage == null)
			vcardImageId = (int) dataSource.saveImage(image);
		else {
			vcardImageId = dbImage.getImageId();
			image.setImageId((int) vcardImageId);
			boolean update = dataSource.updateImage(image);
			//	Log.d("bdimage", "bdimage upadte "+update) ;

		}

		try {
			if (oldIndex != -1) {
				imagesJson.put(oldIndex, vcardImageId);
			} else {
				imagesJson.put(vcardImageId);
			}

			//Log.d("sendin image string", "sendin image string "+imagesJson.toString()) ;
			BiptagUserManger.saveUserData(key, imagesJson.toString(), NavigationActivity.getContext());
			BiptagUserManger.saveUserData(vcardIdKey, vcardImageId, NavigationActivity.getContext());

		} catch (JSONException e) {
			// TODO Auto-generated catch block

		}*/
		finish();

		// finishActivityFromChild(this,1);
		//finishActivityFromChild(child, requestCode);



	    /*Intent data = new Intent() ;
	    data.putExtra("ocr_result", object.toString()) ;
	    */
		// setResult(600, data);
		// finish();
		//finishActivityFromChild(child, requestCode);


		ImageView bitmapImageView = (ImageView) findViewById(R.id.image_view);
		lastBitmap = ocrResult.getBitmap();
		if (lastBitmap == null) {
			bitmapImageView.setImageBitmap(BitmapFactory.decodeResource(getResources(),
					R.drawable.ic_launcher));
		} else {
			bitmapImageView.setImageBitmap(lastBitmap);
		}

		// Display the recognized text
		TextView sourceLanguageTextView = (TextView) findViewById(R.id.source_language_text_view);
		sourceLanguageTextView.setText(sourceLanguageReadable);
		TextView ocrResultTextView = (TextView) findViewById(R.id.ocr_result_text_view);
		ocrResultTextView.setText(ocrResult.getText());
		int scaledSize = Math.max(22, 32 - ocrResult.getText().length() / 4);
		ocrResultTextView.setTextSize(TypedValue.COMPLEX_UNIT_SP, scaledSize);
		progressView.setVisibility(View.VISIBLE);
		setProgressBarVisibility(true);

		progressView.setVisibility(View.GONE);
		setProgressBarVisibility(false);

		return object;
	}



	/*
	   * Displays information relating to the results of a successful real-time OCR request.
	   * param ocrResult Object representing successful OCR results
	   */
	void handleOcrContinuousDecode(OcrResult ocrResult) {
		Log.d("ocrcontinousdecode", ocrResult.toString());
		lastResult = ocrResult;

		// Send an OcrResultText object to the ViewfinderView for text rendering
		viewfinderView.addResultText(new OcrResultText(ocrResult.getText(),
				ocrResult.getWordConfidences(),
				ocrResult.getMeanConfidence(),
				ocrResult.getBitmapDimensions(),
				ocrResult.getRegionBoundingBoxes(),
				ocrResult.getTextlineBoundingBoxes(),
				ocrResult.getStripBoundingBoxes(),
				ocrResult.getWordBoundingBoxes(),
				ocrResult.getCharacterBoundingBoxes()));

		Integer meanConfidence = ocrResult.getMeanConfidence();

		if (CONTINUOUS_DISPLAY_RECOGNIZED_TEXT) {
			// Display the recognized text on the screen
			statusViewTop.setText(ocrResult.getText());
			int scaledSize = Math.max(22, 32 - ocrResult.getText().length() / 4);
			statusViewTop.setTextSize(TypedValue.COMPLEX_UNIT_SP, scaledSize);
			statusViewTop.setTextColor(Color.BLACK);
			statusViewTop.setBackgroundResource(R.color.status_top_text_background);

			statusViewTop.getBackground().setAlpha(meanConfidence * (255 / 100));
		}

		if (CONTINUOUS_DISPLAY_METADATA) {
			// Display recognition-related metadata at the bottom of the screen
			long recognitionTimeRequired = ocrResult.getRecognitionTimeRequired();
			statusViewBottom.setTextSize(14);
			statusViewBottom.setText("OCR: " + sourceLanguageReadable + " - Mean confidence: " +
					meanConfidence.toString() + " - Time required: " + recognitionTimeRequired + " ms");
		}
	}

	/*
	   * Version of handleOcrContinuousDecode for failed OCR requests. Displays a failure message.
	   *
	   * param obj Metadata for the failed OCR request.
	   */
	void handleOcrContinuousDecode(OcrResultFailure obj) {
		lastResult = null;
		Log.d("ocrcontinousdecode ", "2");
		viewfinderView.removeResultText();

		// Reset the text in the recognized text box.
		statusViewTop.setText("");

		if (CONTINUOUS_DISPLAY_METADATA) {
			// Color text delimited by '-' as red.
			statusViewBottom.setTextSize(14);
			CharSequence cs = setSpanBetweenTokens("OCR: " + sourceLanguageReadable + " - OCR failed - Time required: "
					+ obj.getTimeRequired() + " ms", "-", new ForegroundColorSpan(0xFFFF0000));
			statusViewBottom.setText(cs);
		}
	}

	/*
	   * Given either a Spannable String or a regular String and a token, apply
	   * the given CharacterStyle to the span between the tokens.
	   */
	private CharSequence setSpanBetweenTokens(CharSequence text, String token,
											  CharacterStyle... cs) {
		// Start and end refer to the points where the span will apply
		int tokenLen = token.length();
		int start = text.toString().indexOf(token) + tokenLen;
		int end = text.toString().indexOf(token, start);

		if (start > -1 && end > -1) {
			// Copy the spannable string to a mutable spannable string
			SpannableStringBuilder ssb = new SpannableStringBuilder(text);
			for (CharacterStyle c : cs)
				ssb.setSpan(c, start, end, 0);
			text = ssb;
		}
		return text;
	}

	@Override
	public void onCreateContextMenu(ContextMenu menu, View v,
									ContextMenuInfo menuInfo) {
		super.onCreateContextMenu(menu, v, menuInfo);
		if (v.equals(ocrResultView)) {
			menu.add(Menu.NONE, OPTIONS_COPY_RECOGNIZED_TEXT_ID, Menu.NONE, "Copy recognized text");
			menu.add(Menu.NONE, OPTIONS_SHARE_RECOGNIZED_TEXT_ID, Menu.NONE, "Share recognized text");
		}
	}


	/*
	   * Resets view elements.
	   */
	private void resetStatusView() {
		resultView.setVisibility(View.GONE);
		if (CONTINUOUS_DISPLAY_METADATA) {
			statusViewBottom.setText("");
			statusViewBottom.setTextSize(14);
			statusViewBottom.setTextColor(getResources().getColor(R.color.status_text));
			statusViewBottom.setVisibility(View.VISIBLE);
		}
		if (CONTINUOUS_DISPLAY_RECOGNIZED_TEXT) {
			statusViewTop.setText("");
			statusViewTop.setTextSize(14);
			statusViewTop.setVisibility(View.VISIBLE);
		}
		viewfinderView.setVisibility(View.VISIBLE);
		cameraButtonView.setVisibility(View.VISIBLE);
		if (DISPLAY_SHUTTER_BUTTON) {
			shutterButton.setVisibility(View.VISIBLE);
		}
		lastResult = null;
		viewfinderView.removeResultText();
	}

	// Displays a pop-up message showing the name of the current OCR source language.


	/*
	   * Displays an initial message to the user while waiting for the first OCR request to be
	   * completed after starting realtime OCR.
	   */
	void setStatusViewForContinuous() {
		viewfinderView.removeResultText();
		if (CONTINUOUS_DISPLAY_METADATA) {
			statusViewBottom.setText("OCR: " + sourceLanguageReadable + " - waiting for OCR...");
		}
	}

	@SuppressWarnings("unused")
	void setButtonVisibility(boolean visible) {
		if (shutterButton != null && visible == true && DISPLAY_SHUTTER_BUTTON) {
			shutterButton.setVisibility(View.VISIBLE);
		} else if (shutterButton != null) {
			shutterButton.setVisibility(View.GONE);
		}
	}

	/*
	   * Enables/disables the shutter button to prevent double-clicks on the button.
	   *
	   * param clickable True if the button should accept a click
	   */
	void setShutterButtonClickable(boolean clickable) {
		shutterButton.setClickable(clickable);
	}

	// Request the viewfinder to be invalidated.
	void drawViewfinder() {
		viewfinderView.drawViewfinder();
	}


	@Override
	public void onShutterButtonClick(ShutterButton b) {
		if (isContinuousModeActive) {
			onShutterButtonPressContinuous();
		} else {
			if (handler != null) {
				handler.shutterButtonClick();
			}
		}
	}

	@Override
	public void onShutterButtonFocus(ShutterButton b, boolean pressed) {
		requestDelayedAutoFocus();
	}

	/*
	   * Requests autofocus after a 350 ms delay. This delay prevents requesting focus when the user
	   * just wants to click the shutter button without focusing. Quick button press/release will
	   * trigger onShutterButtonClick() before the focus kicks in.
	   */
	private void requestDelayedAutoFocus() {
		// Wait 350 ms before focusing to avoid interfering with quick button presses when
		// the user just wants to take a picture without focusing.
		cameraManager.requestAutoFocus(350L);
	}

	static boolean getFirstLaunch() {
		return isFirstLaunch;
	}

	  /*
	   * We want the help screen to be shown automatically the first time a new version of the app is
	   * run. The easiest way to do this is to check android:versionCode from the manifest, and compare
	   * it to a value stored as a preference.
	   */


	/*
	   * Returns a string that represents which OCR engine(s) are currently set to be run.
	   *
	   * return OCR engine mode
	   */
	String getOcrEngineModeName() {
		String ocrEngineModeName = "";
		String[] ocrEngineModes = getResources().getStringArray(R.array.ocrenginemodes);
		if (ocrEngineMode == TessBaseAPI.OEM_TESSERACT_ONLY) {
			ocrEngineModeName = ocrEngineModes[0];
		} else if (ocrEngineMode == TessBaseAPI.OEM_CUBE_ONLY) {
			ocrEngineModeName = ocrEngineModes[1];
		} else if (ocrEngineMode == TessBaseAPI.OEM_TESSERACT_CUBE_COMBINED) {
			ocrEngineModeName = ocrEngineModes[2];
		}
		return ocrEngineModeName;
	}

	/*
	   * Gets values from shared preferences and sets the corresponding data members in this activity.
	   */
	private void retrievePreferences() {
		prefs = PreferenceManager.getDefaultSharedPreferences(this);

		// Retrieve from preferences, and set in this Activity, the language preferences
		PreferenceManager.setDefaultValues(this, R.xml.preferences, false);
		setSourceLanguage(getlanguageCode());
		//   setSourceLanguage(prefs.getString(PreferencesActivity.KEY_SOURCE_LANGUAGE_PREFERENCE, CaptureActivity.DEFAULT_SOURCE_LANGUAGE_CODE));

		// Retrieve from preferences, and set in this Activity, the capture mode preference
		if (prefs.getBoolean(PreferencesActivity.KEY_CONTINUOUS_PREVIEW, CaptureActivity.DEFAULT_TOGGLE_CONTINUOUS)) {
			isContinuousModeActive = true;
		} else {
			isContinuousModeActive = false;
		}

		// Retrieve from preferences, and set in this Activity, the page segmentation mode preference
		String[] pageSegmentationModes = getResources().getStringArray(R.array.pagesegmentationmodes);
		String pageSegmentationModeName = prefs.getString(PreferencesActivity.KEY_PAGE_SEGMENTATION_MODE, pageSegmentationModes[0]);
		if (pageSegmentationModeName.equals(pageSegmentationModes[0])) {
			pageSegmentationMode = TessBaseAPI.PageSegMode.PSM_AUTO_OSD;
		} else if (pageSegmentationModeName.equals(pageSegmentationModes[1])) {
			pageSegmentationMode = TessBaseAPI.PageSegMode.PSM_AUTO;
		} else if (pageSegmentationModeName.equals(pageSegmentationModes[2])) {
			pageSegmentationMode = TessBaseAPI.PageSegMode.PSM_SINGLE_BLOCK;
		} else if (pageSegmentationModeName.equals(pageSegmentationModes[3])) {
			pageSegmentationMode = TessBaseAPI.PageSegMode.PSM_SINGLE_CHAR;
		} else if (pageSegmentationModeName.equals(pageSegmentationModes[4])) {
			pageSegmentationMode = TessBaseAPI.PageSegMode.PSM_SINGLE_COLUMN;
		} else if (pageSegmentationModeName.equals(pageSegmentationModes[5])) {
			pageSegmentationMode = TessBaseAPI.PageSegMode.PSM_SINGLE_LINE;
		} else if (pageSegmentationModeName.equals(pageSegmentationModes[6])) {
			pageSegmentationMode = TessBaseAPI.PageSegMode.PSM_SINGLE_WORD;
		} else if (pageSegmentationModeName.equals(pageSegmentationModes[7])) {
			pageSegmentationMode = TessBaseAPI.PageSegMode.PSM_SINGLE_BLOCK_VERT_TEXT;
		} else if (pageSegmentationModeName.equals(pageSegmentationModes[8])) {
			pageSegmentationMode = TessBaseAPI.PageSegMode.PSM_SPARSE_TEXT;
		}

		// Retrieve from preferences, and set in this Activity, the OCR engine mode
		String[] ocrEngineModes = getResources().getStringArray(R.array.ocrenginemodes);
		String ocrEngineModeName = prefs.getString(PreferencesActivity.KEY_OCR_ENGINE_MODE, ocrEngineModes[0]);
		if (ocrEngineModeName.equals(ocrEngineModes[0])) {
			ocrEngineMode = TessBaseAPI.OEM_TESSERACT_ONLY;
		} else if (ocrEngineModeName.equals(ocrEngineModes[1])) {
			ocrEngineMode = TessBaseAPI.OEM_CUBE_ONLY;
		} else if (ocrEngineModeName.equals(ocrEngineModes[2])) {
			ocrEngineMode = TessBaseAPI.OEM_TESSERACT_CUBE_COMBINED;
		}

		// Retrieve from preferences, and set in this Activity, the character blacklist and whitelist
		Log.d(" ocr ", " deconnected " + LanguageCode());
		characterBlacklist = OcrCharacterHelper.getBlacklist(prefs, getlanguageCode());
		characterWhitelist = OcrCharacterHelper.getWhitelist(prefs, getlanguageCode());

		prefs.registerOnSharedPreferenceChangeListener(listener);

		beepManager.updatePrefs();
	}

	/*
	   * Sets default values for preferences. To be called the first time this app is run.
	   */
	private void setDefaultPreferences() {
		prefs = PreferenceManager.getDefaultSharedPreferences(this);

		// Continuous preview
		prefs.edit().putBoolean(PreferencesActivity.KEY_CONTINUOUS_PREVIEW, CaptureActivity.DEFAULT_TOGGLE_CONTINUOUS).commit();

		// Recognition language
		prefs.edit().putString(PreferencesActivity.KEY_SOURCE_LANGUAGE_PREFERENCE, CaptureActivity.DEFAULT_SOURCE_LANGUAGE_CODE).commit();
		// OCR Engine
		prefs.edit().putString(PreferencesActivity.KEY_OCR_ENGINE_MODE, CaptureActivity.DEFAULT_OCR_ENGINE_MODE).commit();

		// Autofocus
		prefs.edit().putBoolean(PreferencesActivity.KEY_AUTO_FOCUS, CaptureActivity.DEFAULT_TOGGLE_AUTO_FOCUS).commit();

		// Disable problematic focus modes
		prefs.edit().putBoolean(PreferencesActivity.KEY_DISABLE_CONTINUOUS_FOCUS, CaptureActivity.DEFAULT_DISABLE_CONTINUOUS_FOCUS).commit();

		// Beep
		prefs.edit().putBoolean(PreferencesActivity.KEY_PLAY_BEEP, CaptureActivity.DEFAULT_TOGGLE_BEEP).commit();

		// Character blacklist
		prefs.edit().putString(PreferencesActivity.KEY_CHARACTER_BLACKLIST,
				OcrCharacterHelper.getDefaultBlacklist(CaptureActivity.DEFAULT_SOURCE_LANGUAGE_CODE)).commit();

		// Character whitelist
		prefs.edit().putString(PreferencesActivity.KEY_CHARACTER_WHITELIST,
				OcrCharacterHelper.getDefaultWhitelist(CaptureActivity.DEFAULT_SOURCE_LANGUAGE_CODE)).commit();

		// Page segmentation mode
		prefs.edit().putString(PreferencesActivity.KEY_PAGE_SEGMENTATION_MODE, CaptureActivity.DEFAULT_PAGE_SEGMENTATION_MODE).commit();

		// Reversed camera image
		prefs.edit().putBoolean(PreferencesActivity.KEY_REVERSE_IMAGE, CaptureActivity.DEFAULT_TOGGLE_REVERSED_IMAGE).commit();

		// Light
		prefs.edit().putBoolean(PreferencesActivity.KEY_TOGGLE_LIGHT, CaptureActivity.DEFAULT_TOGGLE_LIGHT).commit();
	}

	void displayProgressDialog() {
		// Set up the indeterminate progress dialog box
		indeterminateDialog = new ProgressDialog(this);
		if(LanguageCode().indexOf("fra")!=-1){
			indeterminateDialog.setTitle("Veuillez patienter");
		}else {

			indeterminateDialog.setTitle("Please wait");
		}
		String ocrEngineModeName = getOcrEngineModeName();
		if (ocrEngineModeName.equals("Both")) {
			indeterminateDialog.setMessage(NavigationActivity.getContext().getResources().getString(R.string.dialog_message2));
		} else {
			if (LanguageCode().indexOf("fra") != -1) {
				indeterminateDialog.setMessage("Reconnaissance en cours");

			} else {
				indeterminateDialog.setMessage("Performing Recognition");
			}
		}
		indeterminateDialog.setCancelable(false);
		indeterminateDialog.show();
	}

	ProgressDialog getProgressDialog() {
		return indeterminateDialog;
	}

	/*
	   * Displays an error message dialog box to the user on the UI thread.
	   * param title The title for the dialog box
	   * param message The error message to be displayed
	   */
	void showErrorMessage(String title, String message) {
		new AlertDialog.Builder(this)
				.setTitle(title)
				.setMessage(message)
				.setOnCancelListener(new FinishListener(this))
				.setPositiveButton("Done", new FinishListener(this))
				.show();
	}

	public String getlanguageCode() {

		Resources res = getResources();
		// Change locale settings in the app.
		DisplayMetrics dm = res.getDisplayMetrics();
		android.content.res.Configuration conf = res.getConfiguration();
		Log.d(" lang ", "local " + conf.locale.getDisplayLanguage());
		String languagecode = conf.locale.getDisplayLanguage();

		int agent_id = Integer.parseInt(String.valueOf(BiptagUserManger.getUserValue("AGENT_ID", "int", CaptureActivity
				.this)));

		BtUserConfigDataSource source = new BtUserConfigDataSource(CaptureActivity.this);
		BTUserConfig config = source.getUserConfig(agent_id);
		if (config != null) {

			if (config.getLanguage().equals("Français")) {
				languagecode = "fra";
				Log.d(" lang "," config !=null" +languagecode);
				return languagecode;
			} else {
				languagecode = "eng";
				Log.d(" lang ", "config != null"+languagecode);
				return languagecode;
			}

		} else {
			if (conf.locale.getDisplayLanguage().equals("English")) {
				Log.d(" ocr ", " lang 2 " + conf.locale.getDisplayLanguage());
				languagecode = "eng";
				Log.d(" lang "," "+ languagecode);
				return languagecode;
			} else {
				//if (conf.locale.getDisplayLanguage() == "English") {
					languagecode = "fra";
				Log.d(" lang "," "+ languagecode);
					return languagecode;
				//}
					}

		}

		//return languagecode;
	}


	public String LanguageCode(){

		String languagecode = "";
		Resources res = getResources();
		// Change locale settings in the app.
		DisplayMetrics dm = res.getDisplayMetrics();
		android.content.res.Configuration conf = res.getConfiguration();
		Log.d(" lang "," "+ conf.locale.getDisplayLanguage());
		languagecode = conf.locale.getDisplayLanguage();
		int agent_id = Integer.parseInt(String.valueOf(BiptagUserManger.getUserValue("AGENT_ID", "int", CaptureActivity.this)));

		BtUserConfigDataSource source = new BtUserConfigDataSource(CaptureActivity.this);
		BTUserConfig config = source.getUserConfig(agent_id);
		if (config != null) {

			if (config.getLanguage().equals("Français")) {
				languagecode = "fra";
				return languagecode;
			} else {
				languagecode = "eng";
				return languagecode;
			}

		}


		return languagecode;

	}

}
