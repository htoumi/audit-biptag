package com.biptag.biptag;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.GradientDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.biptag.biptag.managers.BiptagNavigationManager;
import com.biptag.biptag.managers.BiptagUserManger;

public class ListingFilesActivity extends Fragment {
	JSONObject filesDownloaded ;
	Button fileButton , linkButton , vcardbutton , finishButton ;
	int listingType  ;
	List<String[]> selectedFiles = new ArrayList<String[]>()  ;
	List<String[]> selectedLinks = new ArrayList<String[]>() ;
	List<String[]> selectedVcards = new ArrayList<String[]>();
	ListView listview  ;
	View fragmentView ;
	int stack = 1 ;
	int listFileChanged = 0;
	private static String add_files_result_create = "add_files_result" ;
	private static String  add_files_result_edit = "add_files_result_edit" ;

	public ListingFilesActivity(){


	}
	@SuppressLint({"NewApi", "ValidFragment"})
	public ListingFilesActivity(int stack ) {
		// TODO Auto-generated constructor stub
		this.selectedFiles  = new ArrayList<String[]>();
		this.selectedLinks  = new ArrayList<String[]>();
		this.selectedVcards = new ArrayList<String[]>();
		this.stack = stack ;
	}
	@SuppressLint({"NewApi", "ValidFragment"})
	public ListingFilesActivity(List<String[]> selectedFiles , 		List<String[]> selectedLinks  ,		List<String[]> selectedVcards , int stack  ) {
		// TODO Auto-generated constructor stub
		//Log.d("open add files", "open add files init before counts "+this.selectedFiles.size()+" "+this.selectedLinks.size()+" "+this.selectedVcards.size()) ;

		this.selectedFiles = selectedFiles ;
		this.selectedLinks = selectedLinks ;
		this.selectedVcards = selectedVcards ;
		this.stack = stack ;


		//Log.d("open add files", "open add files init after counts "+this.selectedFiles.size()+" "+this.selectedLinks.size()+" "+this.selectedVcards.size()) ;

	}


	@Override
	public void onStop() {
		super.onStop();
		Button actionBarButton = (Button)	getActivity().getActionBar().getCustomView().findViewById(R.id.formste1_btn_container) ;
		actionBarButton.setBackground(NavigationActivity.getContext().getResources().getDrawable(R.drawable.go_back));

		//actionBarButton.setText(NavigationActivity.getContext().getResources().getString(R.string.return_image_list));
		actionBarButton.setVisibility(View.INVISIBLE);

	}

	@Override
	public void onResume() {
		super.onResume();
		Button actionBarButton = (Button)	getActivity().getActionBar().getCustomView().findViewById(R.id.formste1_btn_container) ;
		actionBarButton.setBackground(NavigationActivity.getContext().getResources().getDrawable(R.drawable.go_back));

		//actionBarButton.setText(NavigationActivity.getContext().getResources().getString(R.string.return_image_list));
		actionBarButton.setVisibility(View.VISIBLE);

	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
							 Bundle savedInstanceState) {
		// TODO Auto-generated method stub

		fragmentView = inflater.inflate(R.layout.activity_listing_files	,null);
		listview = (ListView) fragmentView.findViewById(R.id.contact_listing_listview);
		fileButton= (Button) fragmentView.findViewById(R.id.fragement_listing_file_doc) ;
		linkButton= (Button) fragmentView.findViewById(R.id.fragement_listing_file_links) ;
		vcardbutton= (Button) fragmentView.findViewById(R.id.fragement_listing_file_cards) ;
		finishButton= (Button) fragmentView.findViewById(R.id.file_listing_finish_button) ;

		GradientDrawable border = new GradientDrawable();
		border.setColor(0xFF18a9e7); //background
		border.setStroke(3, 0xFFFFFFFF); //border
		if(Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
			fileButton.setBackgroundDrawable(border);
			linkButton.setBackgroundDrawable(border);
			vcardbutton.setBackgroundDrawable(border);

		} else {
			fileButton.setBackground(border);
			linkButton.setBackground(border);
			vcardbutton.setBackground(border);

		}

		String allFiles ="" ;
		if(stack == 1){
			allFiles = String.valueOf(BiptagUserManger.getUserValue(add_files_result_create, "string", getActivity())) ;

		}
		if(stack == 2){
			allFiles = String.valueOf(BiptagUserManger.getUserValue(add_files_result_edit, "string", getActivity())) ;

		}

		this.selectedFiles = new ArrayList<String[]>() ;
		try {

			JSONArray array = new JSONArray(new JSONObject(allFiles).getString("FILES")) ;
			for (int i = 0; i < array.length(); i++) {
				//	Log.d("open add files ", "open files files "+array.getJSONArray(i)) ;
				String[] items =new String[array.getJSONArray(i).length()] ;
				for (int j = 0; j < items.length; j++) {
					items[j]= array.getJSONArray(i).getString(j) ;
				}
				selectedFiles.add(items) ;

			}
		} catch (JSONException e) {
			// TODO: handle exception
			//Log.d("open add files ", "open files exeption files "+e.getMessage()) ;
		}

		this.selectedLinks= new ArrayList<String[]>() ;
		try {
			JSONArray array = new JSONArray(new JSONObject(allFiles).getString("LINKS"));
			for (int i = 0; i < array.length(); i++) {
				String[] items =new String[array.getJSONArray(i).length()] ;
				for (int j = 0; j < items.length; j++) {
					items[j]= array.getJSONArray(i).getString(j) ;
				}
				this.selectedLinks.add(items) ;
			}
		} catch (JSONException e) {
			// TODO: handle exception
			//Log.d("open add files ", "open files exeption links "+e.getMessage()) ;
		}

		this.selectedVcards= new ArrayList<String[]>() ;
		try {

			JSONArray array = new JSONArray(new JSONObject(allFiles).getString("VCARDS"));
			for (int i = 0; i < array.length(); i++) {
				String[] items =new String[array.getJSONArray(i).length()] ;
				for (int j = 0; j < items.length; j++) {
					items[j]= array.getJSONArray(i).getString(j) ;
				}
				this.selectedVcards.add(items) ;

			}
		} catch (JSONException e) {
			// TODO: handle exception
			//Log.d("open add files ", "open files exeption vcards "+e.getMessage()) ;
		}

		//Log.d("open add files", "open add files counts "+selectedFiles.size()+" "+this.selectedLinks.size()+" "+this.selectedVcards.size()) ;


		//Log.d("open add files", "open add files init after counts oncreate view "+this.selectedFiles.size()+" "+this.selectedLinks.size()+" "+this.selectedVcards.size()) ;


		listview.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
									int position, long id) {
				// TODO Auto-generated method stub

				String[] items = new String[3] ;
				items[0] = ((TextView) view.findViewById(R.id.file_list_label)).getText().toString() ;
				items[1] = DateFormat.getDateTimeInstance().format(new Date());
				items[2] = ((TextView) view.findViewById(R.id.file_list_id)).getText().toString() ;

				if(listingType==0){
					int index = indexOfElements(ListingFilesActivity.this.selectedFiles, items[2]) ;
					if(index!=-1){
						listFileChanged--;
						((ImageView) view.findViewById(R.id.file_list_label_checked)).setVisibility(View.INVISIBLE)  ;
						ListingFilesActivity.this.selectedFiles.remove(index) ;
					}else {
						((ImageView) view.findViewById(R.id.file_list_label_checked)).setVisibility(View.VISIBLE)  ;
						ListingFilesActivity.this.selectedFiles.add(items) ;
						listFileChanged++;
					}
				}
				if(listingType==1){
					items = new String[4] ;

					try {
						JSONArray linksArray = filesDownloaded.getJSONArray("LINK").getJSONObject(0).getJSONArray("LINKS") ;
						for (int i = 0; i < linksArray.length(); i++) {
							HashMap<String, String> map = new HashMap<String, String>()  ;
							String link_id  = linksArray.getJSONObject(i).getString("LINK_ID") ;
							if(link_id.equals(((TextView) view.findViewById(R.id.file_list_id)).getText().toString())){
								items[0] = linksArray.getJSONObject(i).getString("LINK_LABEL") ;
								items[1] = linksArray.getJSONObject(i).getString("LINK_URL");
								items[2] = DateFormat.getDateTimeInstance().format(new Date());
								items[3] = ((TextView) view.findViewById(R.id.file_list_id)).getText().toString() ;
							}
						}
					} catch (JSONException e) {
						// TODO: handle exception
					}

					int index = indexOfElements(ListingFilesActivity.this.selectedLinks, items[3]) ;
					if(index!=-1){
						listFileChanged--;
						((ImageView) view.findViewById(R.id.file_list_label_checked)).setVisibility(View.INVISIBLE)  ;
						ListingFilesActivity.this.selectedLinks.remove(index) ;
					}else {
						listFileChanged++;
						((ImageView) view.findViewById(R.id.file_list_label_checked)).setVisibility(View.VISIBLE)  ;
						ListingFilesActivity.this.selectedLinks.add(items) ;
					}
				}
				if(listingType==2){
					int index = indexOfElements(ListingFilesActivity.this.selectedVcards, items[2]) ;
					if(index!=-1){
						listFileChanged--;
						((ImageView) view.findViewById(R.id.file_list_label_checked)).setVisibility(View.INVISIBLE)  ;
						ListingFilesActivity.this.selectedVcards.remove(index) ;
					}else {
						listFileChanged++;
						((ImageView) view.findViewById(R.id.file_list_label_checked)).setVisibility(View.VISIBLE)  ;
						ListingFilesActivity.this.selectedVcards.add(items) ;
					}
				}

			}
		}) ;
		finishButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent() ;
				JSONArray files= new JSONArray() ;
				JSONArray links = new JSONArray() ;
				JSONArray vcards = new JSONArray() ;

				for (int i = 0; i < ListingFilesActivity.this.selectedFiles.size(); i++) {
					JSONArray item = new JSONArray() ;
					item.put(ListingFilesActivity.this.selectedFiles.get(i)[0]);
					item.put(ListingFilesActivity.this.selectedFiles.get(i)[1]);
					item.put(ListingFilesActivity.this.selectedFiles.get(i)[2]);
					files.put(item) ;
				}
				for (int i = 0; i < ListingFilesActivity.this.selectedVcards.size(); i++) {
					JSONArray item = new JSONArray() ;
					item.put(ListingFilesActivity.this.selectedVcards.get(i)[0]);
					item.put(ListingFilesActivity.this.selectedVcards.get(i)[1]);
					item.put(ListingFilesActivity.this.selectedVcards.get(i)[2]);
					vcards.put(item) ;
				}
				for (int i = 0; i < ListingFilesActivity.this.selectedLinks.size(); i++) {
					JSONArray item = new JSONArray() ;
					//Log.d("link to send", "link to send"+ListingFilesActivity.this.selectedLinks.get(i))  ;
					item.put(ListingFilesActivity.this.selectedLinks.get(i)[0]);
					item.put(ListingFilesActivity.this.selectedLinks.get(i)[1]);
					item.put(ListingFilesActivity.this.selectedLinks.get(i)[2]);
					item.put(ListingFilesActivity.this.selectedLinks.get(i)[3]);
					links.put(item) ;
				}

				intent.putExtra("FILES", files.toString()) ;
				intent.putExtra("LINKS", links.toString()) ;
				intent.putExtra("VCARDS", vcards.toString()) ;
				//Log.d("activity result ", " activity result file selection listing   "+files.toString()) ;
				//	Log.d("activity result ", " activity result file selection listing   "+links.toString()) ;
				//	Log.d("activity result ", " activity result file selection listing   "+vcards.toString()) ;

				//setResult(RESULT_OK, intent) ;
				//finish() ;
				JSONObject object = new JSONObject() ;

				try {
					object.put("FILES", files) ;
					object.put("LINKS", links) ;
					object.put("VCARDS", vcards) ;
				} catch (JSONException e) {
					// TODO: handle exception
				}
				if(stack == 1){
					BiptagUserManger.saveUserData(add_files_result_create, object.toString(), getActivity()) ;

				}else{
					BiptagUserManger.saveUserData(add_files_result_edit, object.toString(), getActivity()) ;

				}
				BiptagNavigationManager.pushFragments(ListingFilesActivity.this, false, "add_file_fragment", getActivity(), null , stack) ;

				ListingFilesActivity.this.selectedFiles  = new ArrayList<String[]>();
				ListingFilesActivity.this.selectedLinks  = new ArrayList<String[]>();
				ListingFilesActivity.this.selectedVcards = new ArrayList<String[]>();

			}
		}) ;

		listingType = 0 ;

		String docsString = String.valueOf(BiptagUserManger.getUserValue("docs", "string", getActivity())) ;
		if( docsString==null || docsString.length() == 0){
			new fileDownloadingWebService().execute() ;
		}else{
			try {
				filesDownloaded = new JSONObject(docsString) ;
				listingType = 0 ;
				setUpFiles() ;
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		fileButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				listingType = 0 ;
				setUpFiles();
			}
		});
		linkButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				listingType = 1 ;
				setUpFiles();
			}
		}) ;
		vcardbutton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				listingType = 2;
				setUpFiles();
			}
		});


		Log.d("open add files", "open add files init after counts on create view end" + this.selectedFiles.size() + " " + this.selectedLinks.size() + " " + this.selectedVcards.size()) ;



		Log.d(" liste changed ", " ok " + listFileChanged);



		Button actionBarButton = (Button)	getActivity().getActionBar().getCustomView().findViewById(R.id.formste1_btn_container) ;
		actionBarButton.setBackground(NavigationActivity.getContext().getResources().getDrawable(R.drawable.go_back));

		//actionBarButton.setText(NavigationActivity.getContext().getResources().getString(R.string.return_image_list));
		actionBarButton.setVisibility(View.VISIBLE);
		actionBarButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				BiptagNavigationManager.pushFragments(ListingFilesActivity.this, false, "add_file_fragment", getActivity(), null, stack);

			}
		});



		return fragmentView;
	}






	public int  indexOfElements(List<String[]> searchList , String id ) {
		//	Log.d("index of element ", "index of element serachlistSize  "+searchList.size()) ;

		for (int i = 0; i < searchList.size(); i++) {
			//Log.d("index of eleme", "index of element "+searchList.get(i)[2]+" "+id) ;
			int listId  =0 ;
			int searchId = Integer.parseInt(id) ;

			if(searchList.get(i).length>3){
				//Log.d("index of eleme", "index of element link "+searchList.get(i)[3]+" "+id) ;

				listId  = Integer.parseInt(searchList.get(i)[3]) ;

				if(listId == searchId){
					return i ;
				}
			}else{
				listId  = Integer.parseInt(searchList.get(i)[2]) ;
				searchId = Integer.parseInt(id) ;
				if(listId ==  searchId) {
					return i ;
				}
			}




		}

		return -1 ;
	}




	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	public void setUpFiles() {


		//Log.d("open add files", "open add files init after counts setupfiles"+this.selectedFiles.size()+" "+this.selectedLinks.size()+" "+this.selectedVcards.size()) ;

		List<String>items  = new ArrayList<String>() ;
		String[] values = null   ;
		List<HashMap<String, String>> listMap = new ArrayList<HashMap<String,String>>() ;
		if (filesDownloaded!=null) {
			try {
				if (listingType == 0) {
					JSONArray docs = filesDownloaded.getJSONArray("DOC");
					JSONArray fileArray = docs.getJSONObject(0).getJSONArray("FILES");

					values = new String[fileArray.length()];
					for (int i = 0; i < fileArray.length(); i++) {
						items.add(fileArray.getJSONObject(i).getString("DOC_NAME"));
						HashMap<String, String> map = new HashMap<String, String>();
						map.put("ID", fileArray.getJSONObject(i).getString("DOC_ID"));
						map.put("PH_NAME", fileArray.getJSONObject(i).getString("DOC_PH_NAME"));
						map.put("INDEX", String.valueOf(i));
						listMap.add(map);
						values[i] = items.get(i);

					}

				}
				if (listingType == 1) {
					//if (filesDownloaded!=null) {
					JSONArray linksArray = filesDownloaded.getJSONArray("LINK").getJSONObject(0).getJSONArray("LINKS");

					values = new String[linksArray.length()];
					for (int i = 0; i < linksArray.length(); i++) {
						HashMap<String, String> map = new HashMap<String, String>();
						map.put("ID", linksArray.getJSONObject(i).getString("LINK_ID"));
						//Log.d("link element ", "link element "+linksArray.getJSONObject(i).toString()) ;
						if (linksArray.getJSONObject(i).has("LINK_LABEL")) {
							map.put("PH_NAME", linksArray.getJSONObject(i).getString("LINK_LABEL"));
							items.add(linksArray.getJSONObject(i).getString("LINK_LABEL"));

						} else {
							map.put("PH_NAME", linksArray.getJSONObject(i).getString("LINK_URL"));
							items.add(linksArray.getJSONObject(i).getString("LINK_URL"));

						}
						map.put("INDEX", String.valueOf(i));
						listMap.add(map);
						values[i] = items.get(i);
					}
				}
				//}
				if (listingType == 2) {
					//	if (filesDownloaded != null) {
					JSONArray vcardArray = filesDownloaded.getJSONArray("CARDS").getJSONObject(0).getJSONArray("CARDS");

					values = new String[vcardArray.length()];
					for (int i = 0; i < vcardArray.length(); i++) {
						//items[i]= vcardArray.getJSONObject(i).getString("LINK_URL") ;
						JSONObject vcard_def = vcardArray.getJSONObject(i).getJSONObject("VCARD_DEF");
						items.add(vcard_def.getString("FIRST_NAME") + " " + vcard_def.getString("LAST_NAME"));
						HashMap<String, String> map = new HashMap<String, String>();
						map.put("ID", vcardArray.getJSONObject(i).getString("VCARD_ID"));
						map.put("PH_NAME", vcard_def.getString("FIRST_NAME") + " " + vcard_def.getString("LAST_NAME"));
						map.put("INDEX", String.valueOf(i));
						listMap.add(map);
						values[i] = items.get(i);
					}
					//	}
				}


				final FileListAdapter adapter = new FileListAdapter(getActivity(), values, listMap);
				Log.d(" changed ", "" + adapter.getCount());
				listview.setAdapter(adapter);


			} catch (JSONException e) {
				// TODO: handle exception
				//Log.d("listing files ", "listing files exception  "+e.getMessage()) ;
			}

		}
		//Log.d("open add files", "open add files init after counts setupfiles end "+this.selectedFiles.size()+" "+this.selectedLinks.size()+" "+this.selectedVcards.size()) ;


	}


	private class FileListAdapter extends ArrayAdapter<String> {
		private final Context context;
		private final String[] values;
		private final List<HashMap<String, String>> filesInfo ;

		public FileListAdapter(Context context, String[] values , List<HashMap<String, String>> filesInfo ) {
			super(context, R.layout.file_list_item, values);
			this.context = context;
			this.values = values;
			this.filesInfo = filesInfo ;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			LayoutInflater inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			View rowView = inflater.inflate(R.layout.file_list_item, parent, false); //
			TextView textView = (TextView) rowView.findViewById(R.id.file_list_label);
			TextView file_ph_name = (TextView) rowView.findViewById(R.id.file_list_ph_name);
			ImageView isChecked = (ImageView) rowView.findViewById(R.id.file_list_label_checked);
			TextView file_id = (TextView) rowView.findViewById(R.id.file_list_id);
			TextView file_index = (TextView) rowView.findViewById(R.id.file_list_index);
			// ImageView imageView = (ImageView) rowView.findViewById(R.id.icon);
			textView.setText(values[position]);
			file_id.setText(this.filesInfo.get(position).get("ID")) ;
			file_index.setText(this.filesInfo.get(position).get("INDEX")) ;
			file_ph_name.setText(this.filesInfo.get(position).get("PH_NAME")) ;
			if(listingType==0){

				if(indexOfElements(ListingFilesActivity.this.selectedFiles, filesInfo.get(position).get("ID")) !=-1){
					isChecked.setVisibility(View.VISIBLE) ;
				}else{
					isChecked.setVisibility(View.INVISIBLE) ;
				}
			}
			if(listingType==1){
				//Log.d("index of file", "index of file "+filesInfo.get(position).get("ID")) ;
				if(indexOfElements(ListingFilesActivity.this.selectedLinks, filesInfo.get(position).get("ID")) !=-1){
					isChecked.setVisibility(View.VISIBLE) ;
				}else{
					isChecked.setVisibility(View.INVISIBLE) ;
				}
			}
			if(listingType==2){
				if(indexOfElements(ListingFilesActivity.this.selectedVcards, filesInfo.get(position).get("ID")) !=-1){
					isChecked.setVisibility(View.VISIBLE) ;
				}else{
					isChecked.setVisibility(View.INVISIBLE) ;
				}
			}

			return rowView;
		}



	}


	class fileDownloadingWebService extends AsyncTask<String, String, String>{
		ProgressDialog dialog ;


		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			dialog = new ProgressDialog(getActivity()) ;
			dialog.setTitle("Telechargement de vos formulaires") ;
			dialog.setMessage("Veuillez patienter") ;
			dialog.setCancelable(false) ;
			dialog.setIndeterminate(false) ;
			dialog.show() ;
		}
		@Override
		protected String doInBackground(String... params) {
			// TODO Auto-generated method stub
			List<NameValuePair > list = new ArrayList<NameValuePair>();
			//Log.d("listing files ", "listing files ba_id "+String.valueOf(BiptagUserManger.getUserValue("BA_ID", "int", getActivity()))) ;

			list.add(new BasicNameValuePair("ba_id", String.valueOf(BiptagUserManger.getUserValue("BA_ID", "int", getActivity())))) ;
			String url =Constant.base_url +Constant.get_files_ws;
			JSONParser jsonParser = new JSONParser() ;
			if(BiptagUserManger.isOnline(NavigationActivity.getContext())) {

				filesDownloaded = jsonParser.makeHttpRequest(url, "GET", list);
				//Log.d("listing files  files", "listing files  files   json response "+filesDownloaded.toString()) ;
			}
			return null;
		}


		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			dialog.dismiss() ;
			try {
				if(filesDownloaded!= null){
					if(filesDownloaded.getInt("success")==1){
						JSONObject docs = filesDownloaded;
						BiptagUserManger.saveUserData("docs",docs.toString() , getActivity());
						listingType = 0 ;
						Log.d("listing files  files", "listing files  files   json response "+filesDownloaded.toString()) ;
						setUpFiles() ;
					}
				}
			} catch (JSONException e) {
				// TODO: handle exception
			}

		}

	}
}

/*package com.biptag.biptagexposant;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.GradientDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.biptag.biptagexposant.dbmanager.datasource.BTCardDataSource;
import com.biptag.biptagexposant.dbmanager.datasource.BTFileDataSource;
import com.biptag.biptagexposant.dbmanager.datasource.BTLinkDataSource;
import com.biptag.biptagexposant.dbmanager.objects.BTCard;
import com.biptag.biptagexposant.dbmanager.objects.BTFile;
import com.biptag.biptagexposant.dbmanager.objects.BTLink;
import com.biptag.biptagexposant.managers.BiptagNavigationManager;
import com.biptag.biptagexposant.managers.BiptagUserManger;

public class ListingFilesActivity extends Fragment {
JSONObject filesDownloaded ; 
Button fileButton , linkButton , vcardbutton , finishButton ; 
int listingType  ;  
List<String[]> selectedFiles = new ArrayList<String[]>()  ; 
List<String[]> selectedLinks = new ArrayList<String[]>() ; 
List<String[]> selectedVcards = new ArrayList<String[]>(); 
ListView listview  ;  
View fragmentView ; 
int stack = 1 ; 

private static String add_files_result_create = "add_files_result" ; 
private static String  add_files_result_edit = "add_files_result_edit" ; 


public ListingFilesActivity(int stack ) {
	// TODO Auto-generated constructor stub
	this.selectedFiles  = new ArrayList<String[]>(); 
	this.selectedLinks  = new ArrayList<String[]>(); 
	this.selectedVcards = new ArrayList<String[]>();
	this.stack = stack ; 
	}

public ListingFilesActivity(List<String[]> selectedFiles , 		List<String[]> selectedLinks  ,		List<String[]> selectedVcards , int stack  ) {
	// TODO Auto-generated constructor stub
	Log.d("open add files", "open add files init before counts "+this.selectedFiles.size()+" "+this.selectedLinks.size()+" "+this.selectedVcards.size()) ; 
	
	this.selectedFiles = selectedFiles ; 
	this.selectedLinks = selectedLinks ; 
	this.selectedVcards = selectedVcards ; 
	this.stack = stack ; 
	
	
	Log.d("open add files", "open add files init after counts "+this.selectedFiles.size()+" "+this.selectedLinks.size()+" "+this.selectedVcards.size()) ; 
	
}






	@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			// TODO Auto-generated method stub

			fragmentView = inflater.inflate(R.layout.activity_listing_files	,null);
			listview = (ListView) fragmentView.findViewById(R.id.contact_listing_listview);
			fileButton= (Button) fragmentView.findViewById(R.id.fragement_listing_file_doc) ; 
			linkButton= (Button) fragmentView.findViewById(R.id.fragement_listing_file_links) ; 
			vcardbutton= (Button) fragmentView.findViewById(R.id.fragement_listing_file_cards) ; 
			finishButton= (Button) fragmentView.findViewById(R.id.file_listing_finish_button) ; 
	 
			GradientDrawable border = new GradientDrawable();
		    border.setColor(0xFF18a9e7); //background
		    border.setStroke(3, 0xFFFFFFFF); //border 
		    if(Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
		    	fileButton.setBackgroundDrawable(border);
		    	linkButton.setBackgroundDrawable(border);
		    	vcardbutton.setBackgroundDrawable(border);
		    	
		    } else { 
		    	fileButton.setBackground(border);
		    	linkButton.setBackground(border);
		    	vcardbutton.setBackground(border);
		    	
		    }
			 
			String allFiles ="" ;
			if(stack == 1){
				allFiles = String.valueOf(BiptagUserManger.getUserValue(add_files_result_create, "string", getActivity())) ; 				
				
			}
			if(stack == 2){
				allFiles = String.valueOf(BiptagUserManger.getUserValue(add_files_result_edit, "string", getActivity())) ; 				
				
			}
			
			this.selectedFiles = new ArrayList<String[]>() ;
			try {
				
				JSONArray array = new JSONArray(new JSONObject(allFiles).getString("FILES")) ; 
				for (int i = 0; i < array.length(); i++) {
					Log.d("open add files ", "open files files "+array.getJSONArray(i)) ; 
					String[] items =new String[array.getJSONArray(i).length()] ; 
					for (int j = 0; j < items.length; j++) {
						items[j]= array.getJSONArray(i).getString(j) ; 
					}
					selectedFiles.add(items) ; 
				}
			} catch (JSONException e) {
				// TODO: handle exception
				Log.d("open add files ", "open files exeption files "+e.getMessage()) ; 
			}
			
			this.selectedLinks= new ArrayList<String[]>() ;
			try {
				JSONArray array = new JSONArray(new JSONObject(allFiles).getString("LINKS")); 
				for (int i = 0; i < array.length(); i++) {
					String[] items =new String[array.getJSONArray(i).length()] ; 
					for (int j = 0; j < items.length; j++) {
						items[j]= array.getJSONArray(i).getString(j) ; 
					}
					this.selectedLinks.add(items) ; 
				}
			} catch (JSONException e) {
				// TODO: handle exception
				Log.d("open add files ", "open files exeption links "+e.getMessage()) ; 
			}
			
			this.selectedVcards= new ArrayList<String[]>() ;
			try {
				
				JSONArray array = new JSONArray(new JSONObject(allFiles).getString("VCARDS")); 
				for (int i = 0; i < array.length(); i++) {
					String[] items =new String[array.getJSONArray(i).length()] ; 
					for (int j = 0; j < items.length; j++) {
						items[j]= array.getJSONArray(i).getString(j) ; 
					}
					this.selectedVcards.add(items) ; 
				}
			} catch (JSONException e) {
				// TODO: handle exception
				Log.d("open add files ", "open files exeption vcards "+e.getMessage()) ; 
			}
			 
			Log.d("open add files", "open add files counts "+selectedFiles.size()+" "+this.selectedLinks.size()+" "+this.selectedVcards.size()) ; 
		

			Log.d("open add files", "open add files init after counts oncreate view "+this.selectedFiles.size()+" "+this.selectedLinks.size()+" "+this.selectedVcards.size()) ; 
			
		    
			listview.setOnItemClickListener(new OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> parent, View view,
						int position, long id) {
					// TODO Auto-generated method stub
					
					String[] items = new String[3] ;
					items[0] = ((TextView) view.findViewById(R.id.file_list_label)).getText().toString() ; 
					items[1] = DateFormat.getDateTimeInstance().format(new Date());
					items[2] = ((TextView) view.findViewById(R.id.file_list_id)).getText().toString() ; 
					
					if(listingType==0){
						int index = indexOfElements(ListingFilesActivity.this.selectedFiles, items[2]) ; 
						if(index!=-1){

							((ImageView) view.findViewById(R.id.file_list_label_checked)).setVisibility(View.INVISIBLE)  ; 
							ListingFilesActivity.this.selectedFiles.remove(index) ; 
						}else {
							((ImageView) view.findViewById(R.id.file_list_label_checked)).setVisibility(View.VISIBLE)  ; 
							ListingFilesActivity.this.selectedFiles.add(items) ; 
						} 
					}
					if(listingType==1){
						items = new String[4] ;
						
							try {
							JSONArray linksArray = filesDownloaded.getJSONArray("LINK").getJSONObject(0).getJSONArray("LINKS") ; 
							for (int i = 0; i < linksArray.length(); i++) {
							HashMap<String, String> map = new HashMap<String, String>()  ; 
							String link_id  = linksArray.getJSONObject(i).getString("LINK_ID") ; 
							if(link_id.equals(((TextView) view.findViewById(R.id.file_list_id)).getText().toString())){
								items[0] = linksArray.getJSONObject(i).getString("LINK_LABEL") ; 
								items[1] = linksArray.getJSONObject(i).getString("LINK_URL"); 
								items[2] = DateFormat.getDateTimeInstance().format(new Date());
								items[3] = ((TextView) view.findViewById(R.id.file_list_id)).getText().toString() ; 
							}
							}
							} catch (JSONException e) {
								// TODO: handle exception
							}
						
						int index = indexOfElements(ListingFilesActivity.this.selectedLinks, items[3]) ; 
						if(index!=-1){

							((ImageView) view.findViewById(R.id.file_list_label_checked)).setVisibility(View.INVISIBLE)  ; 
							ListingFilesActivity.this.selectedLinks.remove(index) ;
						}else {

							((ImageView) view.findViewById(R.id.file_list_label_checked)).setVisibility(View.VISIBLE)  ; 
							ListingFilesActivity.this.selectedLinks.add(items) ; 
						}
					}
					if(listingType==2){
						int index = indexOfElements(ListingFilesActivity.this.selectedVcards, items[2]) ; 
						if(index!=-1){

							((ImageView) view.findViewById(R.id.file_list_label_checked)).setVisibility(View.INVISIBLE)  ; 
							ListingFilesActivity.this.selectedVcards.remove(index) ; 
						}else {

							((ImageView) view.findViewById(R.id.file_list_label_checked)).setVisibility(View.VISIBLE)  ; 
							ListingFilesActivity.this.selectedVcards.add(items) ; 
						}
					}
					
				}
			}) ; 
			finishButton.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					Intent intent = new Intent() ; 
					JSONArray files= new JSONArray() ; 
					JSONArray links = new JSONArray() ; 
					JSONArray vcards = new JSONArray() ;
					
					for (int i = 0; i < ListingFilesActivity.this.selectedFiles.size(); i++) {
						JSONArray item = new JSONArray() ; 
						item.put(ListingFilesActivity.this.selectedFiles.get(i)[0]);
						item.put(ListingFilesActivity.this.selectedFiles.get(i)[1]);
						item.put(ListingFilesActivity.this.selectedFiles.get(i)[2]);
						files.put(item) ; 
					}
					for (int i = 0; i < ListingFilesActivity.this.selectedVcards.size(); i++) {
						JSONArray item = new JSONArray() ; 
						item.put(ListingFilesActivity.this.selectedVcards.get(i)[0]);
						item.put(ListingFilesActivity.this.selectedVcards.get(i)[1]);
						item.put(ListingFilesActivity.this.selectedVcards.get(i)[2]);
						vcards.put(item) ; 
					}
					for (int i = 0; i < ListingFilesActivity.this.selectedLinks.size(); i++) {
						JSONArray item = new JSONArray() ; 
						Log.d("link to send", "link to send"+ListingFilesActivity.this.selectedLinks.get(i))  ; 
						item.put(ListingFilesActivity.this.selectedLinks.get(i)[0]);
						item.put(ListingFilesActivity.this.selectedLinks.get(i)[1]);
						item.put(ListingFilesActivity.this.selectedLinks.get(i)[2]);
						item.put(ListingFilesActivity.this.selectedLinks.get(i)[3]);
						links.put(item) ; 
					}
					
					intent.putExtra("FILES", files.toString()) ; 
					intent.putExtra("LINKS", links.toString()) ; 
					intent.putExtra("VCARDS", vcards.toString()) ; 
					Log.d("activity result ", " activity result file selection listing   "+files.toString()) ; 
					Log.d("activity result ", " activity result file selection listing   "+links.toString()) ; 
					Log.d("activity result ", " activity result file selection listing   "+vcards.toString()) ; 
			
					//setResult(RESULT_OK, intent) ; 
					//finish() ; 	
					JSONObject object = new JSONObject() ; 

					try {
						object.put("FILES", files) ; 
						object.put("LINKS", links) ; 
						object.put("VCARDS", vcards) ; 
					} catch (JSONException e) {
						// TODO: handle exception
					}
					if(stack == 1){
						BiptagUserManger.saveUserData(add_files_result_create, object.toString(), getActivity()) ; 
							
					}else{
						BiptagUserManger.saveUserData(add_files_result_edit, object.toString(), getActivity()) ; 
						
					}
					BiptagNavigationManager.pushFragments(ListingFilesActivity.this, false, "add_file_fragment", getActivity(), null , stack) ; 

					ListingFilesActivity.this.selectedFiles  = new ArrayList<String[]>(); 
					ListingFilesActivity.this.selectedLinks  = new ArrayList<String[]>(); 
					ListingFilesActivity.this.selectedVcards = new ArrayList<String[]>();
					
				}
			}) ; 
			
			listingType = 0 ; 
			
			BTFileDataSource fileDataSource = new BTFileDataSource(NavigationActivity.getContext()) ; 
			
			int ba_id =Integer.parseInt(String.valueOf(BiptagUserManger.getUserValue("BA_ID", "int", NavigationActivity.getContext()))) ;

			if(fileDataSource.selectFiles(ba_id).size()== 0){
			new fileDownloadingWebService().execute() ;
			}else{
					listingType = 0 ; 
					setUpFiles() ; 
				
				
			} 
			fileButton.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					listingType = 0 ; 
					setUpFiles();
				}
			}); 
			linkButton.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					listingType = 1 ; 
					setUpFiles();
				}
			}) ; 
			vcardbutton.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					listingType= 2 ; 
					setUpFiles();
				}
			});
			

			Log.d("open add files", "open add files init after counts on create view end"+this.selectedFiles.size()+" "+this.selectedLinks.size()+" "+this.selectedVcards.size()) ; 
			
			return fragmentView;
		}


	

	
	
	public int  indexOfElements(List<String[]> searchList , String id ) {
		Log.d("index of element ", "index of element serachlistSize  "+searchList.size()) ;
		
		for (int i = 0; i < searchList.size(); i++) {
			Log.d("index of eleme", "index of element "+searchList.get(i)[2]+" "+id) ; 
			int listId  =0 ; 
			int searchId = Integer.parseInt(id) ; 
			
			if(searchList.get(i).length>3){
				Log.d("index of eleme", "index of element link "+searchList.get(i)[3]+" "+id) ; 

				listId  = Integer.parseInt(searchList.get(i)[3]) ; 
				
				if(listId == searchId){
					return i ; 
				}
			}else{
				listId  = Integer.parseInt(searchList.get(i)[2]) ; 
				searchId = Integer.parseInt(id) ; 
				if(listId ==  searchId) {
					return i ; 
				}	
			}
			
			
			
			
		}
		
		return -1 ;  
	}


	

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	public void setUpFiles() {
		
		int ba_id =Integer.parseInt(String.valueOf(BiptagUserManger.getUserValue("BA_ID", "int", NavigationActivity.getContext()))) ;

		Log.d("open add files", "open add files init after counts setupfiles"+this.selectedFiles.size()+" "+this.selectedLinks.size()+" "+this.selectedVcards.size()) ; 
		
			List<String>items  = new ArrayList<String>() ; 
			String[] values = null   ; 
			List<HashMap<String, String>> listMap = new ArrayList<HashMap<String,String>>() ; 
		try {
			if(listingType==0){
				BTFileDataSource fileDataSource = new BTFileDataSource(NavigationActivity.getContext()) ; 
				List<BTFile> files = fileDataSource.selectFiles(ba_id) ; 
				
				 values = new String[files.size()]  ; 
				for (int i = 0; i < files.size(); i++) {
					items.add(files.get(i).getName()) ; 
					HashMap<String, String> map = new HashMap<String, String>()  ; 
					map.put("ID", String.valueOf(files.get(i).getId())) ; 
					map.put("PH_NAME", files.get(i).getPhName()) ;
					map.put("INDEX", String.valueOf(i)) ; 
					listMap.add(map);
					values[i]= items.get(i); 
					
				}
				
				}
			if(listingType == 1){
				BTLinkDataSource linkDataSource = new BTLinkDataSource(NavigationActivity.getContext()) ; 
				List<BTLink> links  = linkDataSource.selectLinks(ba_id) ; 
				values = new String[links.size()]  ; 
				for (int i = 0; i < links.size(); i++) {
					HashMap<String, String> map = new HashMap<String, String>()  ; 
					map.put("ID", String.valueOf(links.get(i).getId())) ; 
					if(links.get(i).getLable()!=null){
						map.put("PH_NAME", links.get(i).getLable()) ;
						items.add(links.get(i).getLable()) ; 

					}else{
						map.put("PH_NAME", links.get(i).getUrl()) ;
						items.add(links.get(i).getUrl()) ; 

					} 
					map.put("INDEX", String.valueOf(i)) ; 
					listMap.add(map);
					values[i]= items.get(i); 
				}
				}
			if(listingType== 2){
				BTCardDataSource cardDataSource = new BTCardDataSource(NavigationActivity.getContext()) ; 
				List<BTCard> cards = cardDataSource.selectCards(ba_id) ; 
				values = new String[cards.size()]  ; 
				for (int i = 0; i < cards.size(); i++) {
					//items[i]= vcardArray.getJSONObject(i).getString("LINK_URL") ; 
					JSONObject vcard_def = cards.get(i).getDef() ; 
					items.add(vcard_def.getString("FIRST_NAME")+" "+vcard_def.getString("LAST_NAME")) ; 
					HashMap<String, String> map = new HashMap<String, String>()  ; 
					map.put("ID", String.valueOf(cards.get(i).getId())) ; 
					map.put("PH_NAME", vcard_def.getString("FIRST_NAME")+" "+vcard_def.getString("LAST_NAME")) ;
					map.put("INDEX", String.valueOf(i)) ; 
					listMap.add(map);
					values[i]= items.get(i); 
				}
			} 

			
			 final FileListAdapter adapter = new FileListAdapter(getActivity(), values, listMap) ; 
			 listview.setAdapter(adapter);
				    
			
			
			
		} catch (JSONException e) {
			// TODO: handle exception
			Log.d("listing files ", "listing files exception  "+e.getMessage()) ; 
		}
		

		Log.d("open add files", "open add files init after counts setupfiles end "+this.selectedFiles.size()+" "+this.selectedLinks.size()+" "+this.selectedVcards.size()) ; 
		
		
	}
	
	
	private class FileListAdapter extends ArrayAdapter<String> {
		  private final Context context;
		  private final String[] values;
		  private final List<HashMap<String, String>> filesInfo ; 

		  public FileListAdapter(Context context, String[] values , List<HashMap<String, String>> filesInfo ) {
		    super(context, R.layout.file_list_item, values);
		    this.context = context;
		    this.values = values;
		    this.filesInfo = filesInfo ; 
		  }
 
		  @Override
		  public View getView(int position, View convertView, ViewGroup parent) {
		    LayoutInflater inflater = (LayoutInflater) context
		        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		    View rowView = inflater.inflate(R.layout.file_list_item, parent, false); //
		    TextView textView = (TextView) rowView.findViewById(R.id.file_list_label);
		    TextView file_ph_name = (TextView) rowView.findViewById(R.id.file_list_ph_name);
		    ImageView isChecked = (ImageView) rowView.findViewById(R.id.file_list_label_checked);
		    TextView file_id = (TextView) rowView.findViewById(R.id.file_list_id);
		    TextView file_index = (TextView) rowView.findViewById(R.id.file_list_index);
		   // ImageView imageView = (ImageView) rowView.findViewById(R.id.icon);
		    textView.setText(values[position]);
		    file_id.setText(this.filesInfo.get(position).get("ID")) ; 
		    file_index.setText(this.filesInfo.get(position).get("INDEX")) ; 
		    file_ph_name.setText(this.filesInfo.get(position).get("PH_NAME")) ; 
		    if(listingType==0){
		    	if(indexOfElements(ListingFilesActivity.this.selectedFiles, filesInfo.get(position).get("ID")) !=-1){
		    		isChecked.setVisibility(View.VISIBLE) ; 
		    	}else{
		    		isChecked.setVisibility(View.INVISIBLE) ; 
		    	}
		    }	
		    if(listingType==1){
		    	Log.d("index of file", "index of file "+filesInfo.get(position).get("ID")) ; 
		    	if(indexOfElements(ListingFilesActivity.this.selectedLinks, filesInfo.get(position).get("ID")) !=-1){
		    		isChecked.setVisibility(View.VISIBLE) ; 
		    	}else{
		    		isChecked.setVisibility(View.INVISIBLE) ; 
		    	}
		    }
		    if(listingType==2){
		    	if(indexOfElements(ListingFilesActivity.this.selectedVcards, filesInfo.get(position).get("ID")) !=-1){
		    		isChecked.setVisibility(View.VISIBLE) ; 
		    	}else{
		    		isChecked.setVisibility(View.INVISIBLE) ; 
		    	}
		    }

		    return rowView;
		  }
		  
		
		  
		} 
	
	
	class fileDownloadingWebService extends AsyncTask<String, String, String>{
		ProgressDialog dialog ; 
		
		
		@Override 
			protected void onPreExecute() {
				// TODO Auto-generated method stub
				super.onPreExecute();
					dialog = new ProgressDialog(getActivity()) ; 
					dialog.setTitle("Telechargement de vos formulaires") ; 
					dialog.setMessage("Veuillez patienter") ;
					dialog.setCancelable(false) ; 
					dialog.setIndeterminate(false) ;  
					dialog.show() ;
		}
		@Override
		protected String doInBackground(String... params) {
			// TODO Auto-generated method stub
			List<NameValuePair > list = new ArrayList<NameValuePair>(); 
			Log.d("listing files ", "listing files ba_id "+String.valueOf(BiptagUserManger.getUserValue("BA_ID", "int", getActivity()))) ; 
			
			list.add(new BasicNameValuePair("ba_id",String.valueOf(BiptagUserManger.getUserValue("BA_ID", "int", getActivity())))) ; 
			String url =getResources().getString(R.urls.base_url ) +getResources().getString(R.urls.get_files_ws); 
			JSONParser jsonParser = new JSONParser() ;
			filesDownloaded = jsonParser.makeHttpRequest(url, "GET", list) ; 
			Log.d("listing files  files", "listing files  files   json response "+filesDownloaded.toString()) ; 
			return null;
		}
		
		
		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
		dialog.dismiss() ; 
		try {
			if(filesDownloaded!= null){
				if(filesDownloaded.getInt("success")==1){
					Log.d("saved file", "file saved  "+filesDownloaded.toString()) ; 

					//BiptagUserManger.saveUserData("docs",docs.toString() ,context);
					int ba_id =Integer.parseInt(String.valueOf(BiptagUserManger.getUserValue("BA_ID", "int", NavigationActivity.getContext()))) ;
					JSONArray links = filesDownloaded.getJSONArray("LINK").getJSONObject(0).getJSONArray("LINKS") ; 
					BTLinkDataSource linkDataSource = new BTLinkDataSource(NavigationActivity.getContext()) ; 
					BTFileDataSource fileDataSource = new BTFileDataSource(NavigationActivity.getContext()) ; 
					BTCardDataSource cardDataSource = new BTCardDataSource(NavigationActivity.getContext()) ; 
					for (int i = 0; i < links.length(); i++) {
						
						BTLink link= linkDataSource.getLink(ba_id, links.getJSONObject(i).getInt("LINK_ID")) ; 
						if(link == null){
							link = new BTLink() ; 
							
							link.setBaID(ba_id) ; 
							link.setId(links.getJSONObject(i).getInt("LINK_ID")) ; 
							link.setLable(links.getJSONObject(i).getString("LINK_LABEL")) ; 
							link.setUrl(links.getJSONObject(i).getString("LINK_URL")) ; 
							boolean saved = linkDataSource.insertLink(link) ; 
							Log.d("file saved ", "file saved   "+link.getUrl()+" ? "+saved) ; 
						}else{
							Log.d("file saved ", "file saved  exist "+link.getUrl()) ; 
							
						}
						
					}
					
					JSONArray files = filesDownloaded.getJSONArray("DOC").getJSONObject(0).getJSONArray("FILES") ; 
					for (int i = 0; i < files.length(); i++) {
						
						BTFile file= fileDataSource.getFile(ba_id, files.getJSONObject(i).getInt("DOC_ID")) ; 
						if(file== null){
							file = new BTFile() ; 
							
							file.setBaId(ba_id) ; 
							file.setId(files.getJSONObject(i).getInt("DOC_ID")) ; 
							file.setName(files.getJSONObject(i).getString("DOC_NAME")) ; 
							file.setPhName(files.getJSONObject(i).getString("DOC_PH_NAME")) ; 
							boolean saved = fileDataSource.insertFile(file) ; 
							Log.d("file saved ", "file saved  "+file.getName()+" saved ? "+saved) ; 
						}else{
							Log.d("file saved ", "file saved  exist "+file.getName()) ; 
						}
						
					}
					 
					JSONArray  cards =filesDownloaded.getJSONArray("CARDS").getJSONObject(0).getJSONArray("CARDS") ; 
					
					for (int i = 0; i < cards.length(); i++) {
						BTCard card = cardDataSource.getCard(ba_id, cards.getJSONObject(i).getInt("VCARD_ID")) ; 
						if(card == null){
							card = new BTCard() ; 
							card.setBaID(ba_id) ; 
							card.setId(cards.getJSONObject(i).getInt("VCARD_ID")) ; 
							card.setDef(cards.getJSONObject(i).getJSONObject("VCARD_DEF")) ; 
							boolean saved = cardDataSource.insertVcar(card) ; 
							Log.d("file saved ", "file saved  "+card.getDef()+" saved ? "+saved) ; 
						}else{
							Log.d("file saved ", "file saved  exist "+card.getDef().toString()) ; 
						}
					}
					
					
					
					
					Log.d("uploac contact ", "uploac contact finish doc move to upload contact") ;
				
					
					
					listingType = 0 ; 
					Log.d("listing files  files", "listing files  files   json response "+filesDownloaded.toString()) ; 
					setUpFiles() ; 
				}
			}
		} catch (JSONException e) {
			// TODO: handle exception
			Log.d("file saved ", "file saved  exception "+e.getMessage()) ; 

		}
		
		}
		
	}
}
*/