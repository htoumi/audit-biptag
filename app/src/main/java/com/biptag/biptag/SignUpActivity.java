package com.biptag.biptag;

import org.json.JSONException;
import org.json.JSONObject;

import com.biptag.biptag.interfaces.AsyncResponse;
import com.biptag.biptag.managers.BiptagUserManger;
import com.biptag.biptag.webservices.BTSignUpWS;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.DialogInterface.OnClickListener;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;

public class SignUpActivity extends Activity implements AsyncResponse{
	
	Button signUpButton ; 
	EditText  name , lname , company, email , password ; 
	CheckBox acceptConditionCheckBox ; 
	ProgressDialog dialog ;  
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_sign_up);
		
		ActionBar mActionBar = getActionBar();
		mActionBar.setDisplayShowHomeEnabled(false);
		mActionBar.setDisplayShowTitleEnabled(false);
		LayoutInflater mInflater = LayoutInflater.from(this);

		View mCustomView = mInflater.inflate(R.layout.custom_actionbar, null);
		ActionBar.LayoutParams actionParams = new ActionBar.LayoutParams(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT) ;
		mCustomView.setLayoutParams(actionParams) ; 
		mActionBar.setCustomView(mCustomView);
		mActionBar.setDisplayShowCustomEnabled(true);
		
		signUpButton = (Button) findViewById(R.id.signup_submit_btn) ; 
		acceptConditionCheckBox = (CheckBox) findViewById(R.id.signup_accept_cond) ; 
		name = (EditText) findViewById(R.id.signup_fname) ; 
		lname = (EditText) findViewById(R.id.signup_lname) ;
		company = (EditText) findViewById(R.id.singup_company) ;
		email = (EditText) findViewById(R.id.signup_email) ;
		password= (EditText) findViewById(R.id.signup_password) ;
		signUpButton.setOnClickListener(new View.OnClickListener() {
			
			@Override 
			public void onClick(View v) { 
				// TODO Auto-generated method stub
				String emailValue = email.getText().toString().replaceAll(" ", "") ; 
				email.setText(emailValue) ; 
				if(BiptagUserManger.isOnline(SignUpActivity.this)){
					if(acceptConditionCheckBox.isChecked()){
						if(email.getText().length() != 0    && password.getText().length() != 0 && company.getText().length() != 0 && name.getText().length() !=0 && lname.getText().length() != 0){
							// ok signup
							if(isValidEmail(email.getText().toString()) && password.getText().length()>7){
								dialog = new ProgressDialog(SignUpActivity.this) ; 
								dialog.setTitle("Creation du formulaire ") ; 
								dialog.setMessage("Veuillez patienter pendant le traitement de la requette") ; 
								dialog.setCancelable(false) ; 
								dialog.setIndeterminate(false) ; 
								dialog.show() ;   
								
								
								BTSignUpWS signUpWS = new BTSignUpWS(name.getText().toString(), lname.getText().toString(), email.getText().toString(), company.getText().toString(), password.getText().toString(), SignUpActivity.this) ;
								signUpWS.response = SignUpActivity.this; 
								signUpWS.execute() ;  
									
							}else{

								AlertDialog.Builder builder= new AlertDialog.Builder(SignUpActivity.this);
								builder.setTitle(getResources().getString(R.string.signup_field_invalid_title)) ; 
								builder.setMessage(getResources().getString(R.string.signup_field_invalid_msg)) ; 
								builder.setNeutralButton("OK", new OnClickListener() {
									
									@Override
									public void onClick(DialogInterface dialog, int which) {
										// TODO Auto-generated method stub
										dialog.dismiss() ; 
									}
								}) ; 
								builder.show()  ; 
									
							} 
							
						}else{
							AlertDialog.Builder builder= new AlertDialog.Builder(SignUpActivity.this);
							builder.setTitle(getResources().getString(R.string.signup_field_mandatory_title)) ; 
							builder.setMessage(getResources().getString(R.string.signup_field_mandatory_msg)) ; 
							builder.setNeutralButton("OK", new OnClickListener() {
								
								@Override
								public void onClick(DialogInterface dialog, int which) {
									// TODO Auto-generated method stub
									dialog.dismiss() ; 
								}
							}) ; 
							builder.show()  ; 
								
						}
						
						
						
					}else{
						AlertDialog.Builder builder= new AlertDialog.Builder(SignUpActivity.this);
						builder.setTitle(getResources().getString(R.string.Policy_error_title)) ; 
						builder.setMessage(getResources().getString(R.string.Policy_error_msg)) ; 
						builder.setNeutralButton("OK", new OnClickListener() {
							
							@Override
							public void onClick(DialogInterface dialog, int which) {
								// TODO Auto-generated method stub
								dialog.dismiss() ; 
							}
						}) ; 
						builder.show()  ; 
					
					
					}
				}else{
					AlertDialog.Builder builder = new AlertDialog.Builder(SignUpActivity.this) ;
					builder.setTitle(getResources().getString(R.string.cnx_error_title)) ; 
					builder.setMessage(getResources().getString(R.string.cnx_error_desc)) ; 
					builder.setPositiveButton("OK", new OnClickListener() {
						
						@Override
						public void onClick(DialogInterface dialog, int which) {
							// TODO Auto-generated method stub
							dialog.dismiss() ; 
						}
					}) ; 
					builder.show() ; 
				}
 
			}
		}) ; 
	
	 
	}
	
	
	


	@Override
	public void processFinish(String result) {
		// TODO Auto-generated method stub
		
		dialog.dismiss() ; 
		if(result != null){
			
			try {
				JSONObject jsonObject = new JSONObject(result) ; 
				if(jsonObject.getInt("success")== 1){ 
					//Log.d("saved email ", "saved email "+email.getText().toString()) ;
					BiptagUserManger.saveUserData("EMAIL", email.getText().toString(), SignUpActivity.this) ;
					BiptagUserManger.saveUserData("USER_ID", jsonObject.getInt("USER_ID"), SignUpActivity.this) ; 
					BiptagUserManger.saveUserData("BA_ID", jsonObject.getInt("BA_ID"), SignUpActivity.this) ;
					BiptagUserManger.saveUserData("AGENT_ID", jsonObject.getInt("AGENT_ID"), SignUpActivity.this) ;
					BiptagUserManger.saveUserData("ROLE_DESC", jsonObject.getString("ROLE_DESC"), SignUpActivity.this) ;
					BiptagUserManger.saveUserData("AGENT_FNAME", jsonObject.getString("AGENT_FNAME"), SignUpActivity.this) ;
					BiptagUserManger.saveUserData("AGENT_LNAME", jsonObject.getString("AGENT_LNAME"), SignUpActivity.this) ;
					BiptagUserManger.saveUserData("BA_ADRESS", jsonObject.getString("BA_ADRESS"), SignUpActivity.this) ;
					BiptagUserManger.saveUserData("BA_ZIPCODE", jsonObject.getString("BA_ZIPCODE"), SignUpActivity.this) ;
					BiptagUserManger.saveUserData("BA_COUNTRY", jsonObject.getString("BA_COUNTRY"), SignUpActivity.this) ;
					BiptagUserManger.saveUserData("BA_COMPANY", jsonObject.getString("BA_COMPANY"), SignUpActivity.this) ;
					BiptagUserManger.saveUserData("LAST_UPDATE", jsonObject.getString("LAST_UPDATE"), SignUpActivity.this) ;
					BiptagUserManger.saveUserData("PRODUCT_CONFIG", jsonObject.getString("PRODUCT_CONFIG"), SignUpActivity.this) ;
					BiptagUserManger.saveUserData("CO-WORKERS", "", SignUpActivity.this) ;

					//BiptagUserManger.saveUserData("FU_ID", jsonObject.getInt("FU_ID"), SignUpActivity.this) ;
					//BiptagUserManger.saveUserData("FU_SUBJECT", jsonObject.getString("FU_SUBJECT"), SignUpActivity.this) ;
					//BiptagUserManger.saveUserData("FU_CONTENT", jsonObject.getString("FU_CONTENT"), SignUpActivity.this) ;
					//BiptagUserManger.saveUserData("CO-WORKERS", jsonObject.getString("CO-WORKERS"), SignUpActivity.this) ;
					
					Intent intent = new Intent(SignUpActivity.this , SignupCreateFormStep1.class) ; 
						startActivity(intent) ; 
				  
				}else {
					
					
						if(jsonObject.getInt("success")  ==  2){
							AlertDialog.Builder builder= new AlertDialog.Builder(SignUpActivity.this);
							builder.setTitle(getResources().getString(R.string.signup_exist_account_error_title)) ; 
							builder.setMessage(getResources().getString(R.string.signup_exist_account_error_msg)) ; 
							builder.setNeutralButton("OK", new OnClickListener() {
								
								@Override
								public void onClick(DialogInterface dialog, int which) {
									// TODO Auto-generated method stub
									dialog.dismiss() ; 
								}
							}) ; 
							builder.show()  ; 
						
					}else{

						AlertDialog.Builder dialog = new AlertDialog.Builder(SignUpActivity.this) ; 
						dialog.setTitle(getResources().getString(R.string.signup_error_title)) ; 
						dialog.setMessage(getResources().getString(R.string.signup_error_msg)) ;
						dialog.show() ; 	
					}
					
					
				}
			} catch (JSONException e) {
				// TODO: handle exception
				//Log.d("sign error ", "sign up error  "+e.getMessage()) ;
			}
		}
	}

	@Override
	public void processFailed() {
		// TODO Auto-generated method stub 
		 

		AlertDialog.Builder dialog = new AlertDialog.Builder(SignUpActivity.this) ; 
		dialog.setTitle(getResources().getString(R.string.signup_error_title)) ; 
		dialog.setMessage(getResources().getString(R.string.signup_error_msg)) ;
		dialog.setPositiveButton("OK", new OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				dialog.dismiss() ; 
			} 
		}) ; 
		dialog.show() ; 
		
	} 

	@Override
	public void synchronisationStepUp() {
		// TODO Auto-generated method stub
		
		
		
		
	}
	
	public  boolean isValidEmail(String target) {
		  if (TextUtils.isEmpty(target)) {
		    return false;
		  } else {
		    return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
		  }
		}
}
