package com.biptag.biptag.fragments.home;


import java.util.List;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

public class ViewPagerAdapter extends FragmentPagerAdapter {

        
		
		private final List fragments;

        //On fournit  l'adapter la liste des fragments à afficher
        public ViewPagerAdapter(FragmentManager fm, List fragments) {
                super(fm);
                this.fragments = fragments;
        }

        @Override
        public Fragment getItem(int position) {
                return (Fragment) this.fragments.get(position);
        }

        @Override
        public int getCount() {
                return this.fragments.size();
        }
}












/*import android.content.Context;
import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;

import com.biptag.biptagexposant.R;

public class ViewPagerAdapter extends PagerAdapter {

	

	public int getCount() {
		return 4;
	}

	public Object instantiateItem(View collection, int position) {
		LayoutInflater inflater = (LayoutInflater) collection.getContext()
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		int resId = 0;
		switch (position) {
		case 0:
			resId = R.layout.activity_default_home;
			System.out.println(position);
			break;
		case 1:
			System.out.println(position);
			resId = R.layout.activity_home_step1;
			

			break;
		case 2:
			System.out.println(position);
			resId = R.layout.activity_default_home;
			break;
		case 3:
			System.out.println(position);
			resId = R.layout.activity_default_home;
			break;
		}
		View view = inflater.inflate(resId, null);
		((ViewPager) collection).addView(view, 0);
		return view;
	}

	@Override
	public void destroyItem(View arg0, int arg1, Object arg2) {
		((ViewPager) arg0).removeView((View) arg2);
	}

	@Override
	public boolean isViewFromObject(View arg0, Object arg1) {
		return arg0 == ((View) arg1);
	}

	@Override
	public Parcelable saveState() {
		return null;
	}
	
	
}
*/