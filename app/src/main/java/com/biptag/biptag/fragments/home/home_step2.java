package com.biptag.biptag.fragments.home;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.biptag.biptag.R;

public class home_step2 extends Fragment {
	
	private PagerAdapter mPagerAdapter;
	 @Override
     public View onCreateView(LayoutInflater inflater, ViewGroup container,
                     Bundle savedInstanceState) {
           //  return inflater.inflate(R.layout.activity_home_step1, container, false);
		 LayoutInflater li = (LayoutInflater)

				 LayoutInflater.from( getActivity()) ;
		 final View fragmentView = li.inflate(R.layout.activity_home_step0	,null);
         Button commancer = (Button) fragmentView.findViewById(R.id.btnCommancer) ;
         commancer.setOnClickListener(new View.OnClickListener() {
        	  
        	  @Override public void onClick(View v) {
        	// TODO Auto-generated method stub
        	
        		  FragmentTransaction trans = getFragmentManager()
  						.beginTransaction();
  				trans.replace(R.id.step2, new home_step0());

  				trans.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
  				trans.addToBackStack(null);
  				trans.commit();
        	  } 
        	  
         });
         
         
         
         
         
        
      
        
		return fragmentView;
     }
	 
}
