package com.biptag.biptag.fragments.home;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.biptag.biptag.Connexion;
import com.biptag.biptag.DefaultHome;
import com.biptag.biptag.NavigationActivity;
import com.biptag.biptag.R;
import com.biptag.biptag.SignUpActivity;
import com.biptag.biptag.managers.BiptagUserManger;

public class home_step0 extends Fragment {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
        LayoutInflater li = (LayoutInflater)

                LayoutInflater.from(getActivity());
        final View fragmentView = li.inflate(R.layout.activity_default_home, null);

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        LayoutInflater li = (LayoutInflater) LayoutInflater.from(getActivity());
        final View fragmentView = li.inflate(R.layout.activity_default_home, null);
        //final View fragmentView = inflater.inflate(R.layout.activity_default_home	,null);
        Button connectBtn = (Button) fragmentView.findViewById(R.id.default_home_connect_btn);
        connectBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) { // TODO Auto-generated method stub
                Intent intent = new Intent(getActivity(), Connexion.class);
                startActivity(intent);


                if (DefaultHome.instance != null) {
                    //  DefaultHome.instance.finish();
                }

            }
        });


        Button signupBtn = (Button) fragmentView.findViewById(R.id.default_homde_signup_btn);
        signupBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Intent intent = new Intent(getActivity(), SignUpActivity.class);
                startActivity(intent);
                if (DefaultHome.instance != null) {
                    //DefaultHome.instance.finish();
                }
            }
        });

        int user_id = Integer.parseInt(String.valueOf(BiptagUserManger.getUserValue("USER_ID", "int", getActivity())));
        if (user_id != 0) {
            Intent intent = new Intent(getActivity(), NavigationActivity.class);
            startActivity(intent);


            if (DefaultHome.instance != null) {
                DefaultHome.instance = null;
            }

        }


        return fragmentView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getView().setFocusableInTouchMode(true);
        getView().requestFocus();

        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_DOWN) {
                    if (keyCode == KeyEvent.KEYCODE_BACK) {
                        Intent intent = new Intent(Intent.ACTION_MAIN);
                        intent.addCategory(Intent.CATEGORY_HOME);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
//						getActivity().finish();
                        return true;
                    }
                }
                return false;
            }
        });

    }


}