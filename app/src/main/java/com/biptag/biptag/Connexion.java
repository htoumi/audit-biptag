package com.biptag.biptag;

import java.io.File;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources.NotFoundException;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.biptag.biptag.camera.ShutterButton;
import com.biptag.biptag.language.LanguageCodeHelper;
import com.biptag.biptag.managers.BiptagUserManger;
import com.biptag.biptag.ocr.BeepManager;
import com.biptag.biptag.ocr.CaptureActivity;
import com.biptag.biptag.ocr.CaptureActivityHandler;
import com.biptag.biptag.ocr.FinishListener;
import com.biptag.biptag.ocr.OcrCharacterHelper;
import com.biptag.biptag.ocr.OcrInitAsyncTask;
import com.biptag.biptag.ocr.OcrInitAsyncTaskCon;
import com.biptag.biptag.ocr.PreferencesActivity;
import com.googlecode.tesseract.android.TessBaseAPI;

public class Connexion extends Activity  {

	EditText emailFiedl , passwordField ;
	CheckBox keepAlive ;
	private SharedPreferences prefs;
	TextView cant_cnct ;
	Button connectButton ;
	JSONObject jsonObject ;
	JSONParser parser  ;
	String email , password ;




	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);


		setContentView(R.layout.activity_connexion);

		ActionBar mActionBar = getActionBar();
		mActionBar.setDisplayShowHomeEnabled(false);
		mActionBar.setDisplayShowTitleEnabled(false);
		LayoutInflater mInflater = LayoutInflater.from(this);

		View mCustomView = mInflater.inflate(R.layout.custom_actionbar, null);
		ActionBar.LayoutParams actionParams = new ActionBar.LayoutParams(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);
		mCustomView.setLayoutParams(actionParams);
		mActionBar.setCustomView(mCustomView);
		mActionBar.setDisplayShowCustomEnabled(true);

		cant_cnct = (TextView) findViewById(R.id.connection_cant_connect_text);


		cant_cnct.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				AlertDialog.Builder dialogcant = new AlertDialog.Builder(Connexion.this);
				dialogcant.setTitle(getResources().getString(R.string.cannot_access));
				dialogcant.setMessage("merci de contacter support@biptag.com");
				dialogcant.setPositiveButton("Ok", new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						dialog.dismiss();
					}
				});
				dialogcant.show();
			}
		});

		emailFiedl = (EditText) findViewById(R.id.connexion_email_field);
		passwordField = (EditText) findViewById(R.id.connexion_password_field);
		connectButton = (Button) findViewById(R.id.connection_connect_btn);
		//keepAlive= (CheckBox) findViewById(R.id.connection_keep_alive) ;

		connectButton.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					try {
					emailFiedl.setText(emailFiedl.getText().toString().replaceAll(" ", "")) ;

						if(emailFiedl.getText().toString() != "" && md5(passwordField.getText().toString()) != "" ){
							email = emailFiedl.getText().toString() ;
							password= md5(passwordField.getText().toString()) ;

							if(BiptagUserManger.isOnline(getApplicationContext())){
								new AuthentificateUser().execute() ;

							}else {
								AlertDialog.Builder dialog = new AlertDialog.Builder(Connexion.this) ;
								dialog.setTitle(getResources().getString(R.string.cnx_error_title)) ;
								dialog.setMessage(getResources().getString(R.string.cnx_error_desc)) ;
								dialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {

									@Override
									public void onClick(DialogInterface dialog, int which) {
										// TODO Auto-generated method stub
										dialog.dismiss() ;
									}
								}) ;
								dialog.show() ;

							}

						}else {
							AlertDialog.Builder dialog = new AlertDialog.Builder(Connexion.this) ;

							dialog.setTitle(getResources().getString(R.string.cnx_error_title)) ;
							dialog.setMessage(getResources().getString(R.string.cnx_error_desc)) ;

							dialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {

								@Override
								public void onClick(DialogInterface dialog, int which) {
									// TODO Auto-generated method stub
									dialog.dismiss() ;
								}
							}) ;
							dialog.show() ;
						}
					} catch (NoSuchAlgorithmException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (NotFoundException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}) ;




	}



	public static String md5(String input) throws NoSuchAlgorithmException {
	    String result = input;
	    if(input != null) {
	        MessageDigest md = MessageDigest.getInstance("MD5"); //or "SHA-1"
	        md.update(input.getBytes());
	        BigInteger hash = new BigInteger(1, md.digest());
	        result = hash.toString(16);
	        while(result.length() < 32) { //40 for SHA-1
	            result = "0" + result;
	        }
	    }
	    return result;
	}

		class AuthentificateUser extends AsyncTask<String, String, String>{
		ProgressDialog dialog ;

			@Override
				protected void onPreExecute() {
					// TODO Auto-generated method stub
					super.onPreExecute();
						dialog = new ProgressDialog(Connexion.this) ;
						dialog.setTitle(getResources().getString(R.string.cnx_authentif_title)) ;
						dialog.setMessage(getResources().getString(R.string.cnx_authentif_msg)) ;
						dialog.setCancelable(false) ;
						dialog.setIndeterminate(false) ;
						dialog.show() ;


			}


			@Override
			protected String doInBackground(String... params) {
				// TODO Auto-generated method stub
				String url= Constant.base_url  +Constant.authentification_ws;
				List<NameValuePair> list = new ArrayList<NameValuePair>() ;
				list.add(new BasicNameValuePair("email",email));
				list.add(new BasicNameValuePair("password",	password)) ;
				parser = new JSONParser() ;

				jsonObject = parser.makeHttpRequest(url, "GET", list) ;
				if(jsonObject!= null)
				Log.d("connection result ", "connection result "+jsonObject.toString()) ;

				return null;
			}



			@Override
				protected void onPostExecute(String result) {
					// TODO Auto-generated method stub
					super.onPostExecute(result);
					dialog.dismiss() ;
				 	//Log.d("connection json object ", "connection json object "+jsonObject.toString()) ;
					try {
							if(jsonObject != null){
								if(jsonObject.getInt("success")== 1){
									BiptagUserManger.saveUserData("EMAIL", email, Connexion.this) ;
									BiptagUserManger.saveUserData("USER_ID", jsonObject.getInt("USER_ID"), Connexion.this) ;
									BiptagUserManger.saveUserData("BA_ID", jsonObject.getInt("BA_ID"), Connexion.this) ;
									BiptagUserManger.saveUserData("AGENT_ID", jsonObject.getInt("AGENT_ID"), Connexion.this) ;
									BiptagUserManger.saveUserData("ROLE_DESC", jsonObject.getString("ROLE_DESC"), Connexion.this) ;
									BiptagUserManger.saveUserData("AGENT_FNAME", jsonObject.getString("AGENT_FNAME"), Connexion.this) ;
									BiptagUserManger.saveUserData("AGENT_LNAME", jsonObject.getString("AGENT_LNAME"), Connexion.this) ;
									BiptagUserManger.saveUserData("BA_ADRESS", jsonObject.getString("BA_ADRESS"), Connexion.this) ;
									BiptagUserManger.saveUserData("BA_ZIPCODE", jsonObject.getString("BA_ZIPCODE"), Connexion.this) ;
									BiptagUserManger.saveUserData("BA_COUNTRY", jsonObject.getString("BA_COUNTRY"), Connexion.this) ;
									BiptagUserManger.saveUserData("BA_COMPANY", jsonObject.getString("BA_COMPANY"), Connexion.this) ;
									BiptagUserManger.saveUserData("LAST_UPDATE", jsonObject.getString("LAST_UPDATE"), Connexion.this) ;
									BiptagUserManger.saveUserData("PRODUCT_CONFIG", jsonObject.getString("PRODUCT_CONFIG"), Connexion.this) ;

									BiptagUserManger.saveUserData("FU_ID", jsonObject.getInt("FU_ID"), Connexion.this) ;
									BiptagUserManger.saveUserData("FU_SUBJECT", jsonObject.getString("FU_SUBJECT"), Connexion.this) ;
									BiptagUserManger.saveUserData("FU_CONTENT", jsonObject.getString("FU_CONTENT"), Connexion.this) ;
									BiptagUserManger.saveUserData("CO-WORKERS", jsonObject.getString("CO-WORKERS"), Connexion.this) ;

									BiptagUserManger.saveUserData("USER_CHANGED", "1", Connexion.this) ;

									Intent intent = new Intent(Connexion.this , NavigationActivity.class) ;
										startActivity(intent) ;
									finish();

								}else {
									AlertDialog.Builder dialog = new AlertDialog.Builder(Connexion.this) ;
									dialog.setTitle(getResources().getString(R.string.authentification_error_title)) ;
									dialog.setMessage(getResources().getString(R.string.authentification_error_desc)) ;
									dialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {

										@Override
										public void onClick(DialogInterface dialog, int which) {
											// TODO Auto-generated method stub
											dialog.dismiss() ;
										}
									}) ;
									dialog.show() ;
								}
							}else{

								AlertDialog.Builder dialog = new AlertDialog.Builder(Connexion.this) ;
								dialog.setTitle(getResources().getString(R.string.authentification_error_title)) ;
								dialog.setMessage(getResources().getString(R.string.authentification_error_desc)) ;
								dialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {

									@Override
									public void onClick(DialogInterface dialog, int which) {
										// TODO Auto-generated method stub
										dialog.dismiss() ;
									}
								}) ;
								dialog.show() ;

							}

					} catch (JSONException e) {
						// TODO: handle exception
						//Log.d("json exception  authentficiation " , "json exception  authentficiation "+e.getMessage()) ;

					}


			}
		}
}
