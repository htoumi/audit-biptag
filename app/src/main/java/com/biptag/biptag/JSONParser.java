package com.biptag.biptag;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

public class JSONParser {

    static InputStream is = null;
    static JSONObject jObj = null;
    static String json = "";
    private static final String IMAGE_DIRECTORY_NAME = "biptagimg";
    // constructor
    public JSONParser() {
 
    } 

    // function get json from url
    // by making HTTP POST or GET mehtod
    public JSONObject makeHttpRequest(String url, String method,
                                      List<NameValuePair> params) {


        // Making HTTP request
        try { 

            // check for request method
            if(method == "POST"){
                // request method is POST
                // defaultHttpClient
                DefaultHttpClient httpClient = new DefaultHttpClient();
                HttpPost httpPost = new HttpPost(url);
                httpPost.setEntity(new UrlEncodedFormEntity(params));

                HttpResponse httpResponse = httpClient.execute(httpPost);
                HttpEntity httpEntity = httpResponse.getEntity();
                is = httpEntity.getContent();

            }else if(method == "GET"){
                // request method is GET

                DefaultHttpClient httpClient = new DefaultHttpClient();
                String paramString = URLEncodedUtils.format(params, "utf-8");
                url += "?" + paramString;
              //  Log.d("json parser", "json parser url "+url) ;
                HttpGet httpGet = new HttpGet(url); 
                HttpResponse httpResponse = httpClient.execute(httpGet);
                HttpEntity httpEntity = httpResponse.getEntity();
                is = httpEntity.getContent();

            } 
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(
                    is, "iso-8859-1"), 8);
            StringBuilder sb = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            } 
            is.close();

            json = sb.toString();
            json = json.replaceAll("&amp;", "&") ; 
        } catch (Exception e) {
        }

        // try parse the string to a JSON object
        try {
            jObj = new JSONObject(json);
        } catch (JSONException e) {
        }
        Log.d("connection result ", "connection result "+url) ;

        // return JSON String
        return jObj;

    }
    
    
    
    public JSONObject makeHybridHttpRequest(String url, 
            List<NameValuePair> getParams, List<NameValuePair> postParams) {

// Making HTTP request
try {

// check for request method

// request method is POST
// defaultHttpClient
	String paramString = URLEncodedUtils.format(getParams, "utf-8");
	url += "?" + paramString;
	Log.d("make http" , "make http "+url) ;
DefaultHttpClient httpClient = new DefaultHttpClient();
HttpPost httpPost = new HttpPost(url);

httpPost.setEntity(new UrlEncodedFormEntity(postParams));
	

HttpResponse httpResponse = httpClient.execute(httpPost);
HttpEntity httpEntity = httpResponse.getEntity();
is = httpEntity.getContent();


} catch (UnsupportedEncodingException e) {
e.printStackTrace();
} catch (ClientProtocolException e) {
e.printStackTrace();
} catch (IOException e) {
e.printStackTrace();
}

try {
BufferedReader reader = new BufferedReader(new InputStreamReader(
is, "iso-8859-1"), 8);
StringBuilder sb = new StringBuilder();
String line = null;
while ((line = reader.readLine()) != null) {
sb.append(line + "\n");
} 
is.close();

json = sb.toString();
} catch (Exception e) {
}

// try parse the string to a JSON object
try {
jObj = new JSONObject(json);
} catch (JSONException e) {
}

// return JSON String
return jObj;

}
  
    /*
    public JSONObject makeHttpRequestWithFile(String url, List<NameValuePair> params , List<String> files, Context context) {
        HttpClient httpClient = new DefaultHttpClient();
      //  HttpContext localContext = new BasicHttpContext();
       // String paramString = URLEncodedUtils.format(params, "utf-8");
        url += "?" ;
        for(int i =0 ; i < params.size()- 1 ; i++){
        	NameValuePair pair = params.get(i) ;  
       
        		 
        		String encodedParam = URLUTF8Encoder.encode(pair.getValue()) ; 
//				url+=pair.getName()+"="+URLEncoder.encode(pair.getValue(), "utf-8")+"&" ;
				url+=pair.getName()+"="+encodedParam +"&" ;
			
        }
        NameValuePair pair = params.get(params.size()-1) ; 
        String encodedParam = URLUTF8Encoder.encode(pair.getValue()) ; 
			url+=pair.getName()+"="+encodedParam  ;
		
        Log.d("url json ",	"json success makeHttpRequestWithFile  url "+url);
        HttpPost httpPost = new HttpPost(url);

        try {
            MultipartEntityBuilder entity =  MultipartEntityBuilder.create();
            entity.setMode(HttpMultipartMode.BROWSER_COMPATIBLE) ; 
            //new MultipartEntity();
             
            File mediaStorageDir = new File(
		            Environment
		                    .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
		            IMAGE_DIRECTORY_NAME);
            for(int index=0; index < files.size(); index++) {
               
                   Log.d("sending image ", "sending image  "+index+"  "+files.get(index)) ; 
                    entity.addPart("attachment", new FileBody(new File (context.getFilesDir()+File.separator+files.get(index)+".jpg")));
               
            }
            	HttpEntity httpEntity1= entity.build()  ; 
            httpPost.setEntity(httpEntity1);

            HttpResponse response = httpClient.execute(httpPost);
            HttpEntity httpEntity = response.getEntity();
            is = httpEntity.getContent();
            try { 
            	BufferedReader reader = new BufferedReader(new InputStreamReader(
            	is, "iso-8859-1"), 8);
            	StringBuilder sb = new StringBuilder();
            	String line = null;
            	while ((line = reader.readLine()) != null) {
            	sb.append(line + "\n");
            	}
            	is.close();
            	Log.d("json response ", "json response		"+sb.toString());

            	json = sb.toString();
            	} catch (Exception e) {
            	Log.e("Buffer Error", "Error converting result " + e.toString());
            	}

            	// try parse the string to a JSON object
            	try {
            	jObj = new JSONObject(json);
            	} catch (JSONException e) {
            	Log.e("JSON Parser", "Error parsing data " + e.toString());
            	}

            	// return JSON String
            	
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace(); 
            Log.d("json response exception", "json response exception"+e.getMessage());
        } catch (ClientProtocolException e) {
            e.printStackTrace();
            Log.d("json response exception", "json response exception"+e.getMessage());
        } catch (IOException e) {
            e.printStackTrace();
            Log.d("json response exception", "json response exception"+e.getMessage());
        
    	} 
        try {
        	BufferedReader reader = new BufferedReader(new InputStreamReader(
        	is, "iso-8859-1"), 8);
        	StringBuilder sb = new StringBuilder();
        	String line = null;
        	while ((line = reader.readLine()) != null) {
        	sb.append(line + "\n");
        	}
        	is.close(); 
        	Log.d("json response ", "json response	ws	"+sb.toString());

        	json = sb.toString();
        	} catch (Exception e) {
        	Log.e("Buffer Error", "Error converting result " + e.toString());
        	}

        	// try parse the string to a JSON object
        	try {
        	jObj = new JSONObject(json);
        	} catch (JSONException e) {
        	Log.e("JSON Parser", "Error parsing data " + e.toString());
        	}
 
        	// return JSON String
        	return jObj;
    }
    
    */

 
}