package com.biptag.biptag;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

public class SignupCreateFormStep1 extends Activity {
	EditText gender , fname , lname , email , company , fonction , zipcode , city , country , address , phone ;

	@Override 
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_signup_create_form_step1);
		
		ActionBar mActionBar = getActionBar();
		mActionBar.setDisplayShowHomeEnabled(false);
		mActionBar.setDisplayShowTitleEnabled(false);
		LayoutInflater mInflater = LayoutInflater.from(this);

		View mCustomView = mInflater.inflate(R.layout.custom_actionbar, null);
		ActionBar.LayoutParams actionParams = new ActionBar.LayoutParams(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT) ;
		mCustomView.setLayoutParams(actionParams) ; 
		mActionBar.setCustomView(mCustomView);
		mActionBar.setDisplayShowCustomEnabled(true);

		((Button) findViewById(R.id.signup_create_form_ok)).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
		
				JSONArray formStructure = new JSONArray() ; 
				JSONArray values = new JSONArray() ; 
				JSONObject element = new JSONObject() ; 
				try {
					element.put("type", "LABEL") ; 
					values.put(getResources().getString(R.string.civilite_j)) ; 
					element.put("VALUE",values ) ; 
					formStructure.put(element) ; 
 
					element = new JSONObject() ; 
					values = new JSONArray()  ;
					element.put("type", "radio") ; 
					values.put(getResources().getString(R.string.gender_mr)) ; 
					values.put(getResources().getString(R.string.gender_mms)) ; 
					values.put(getResources().getString(R.string.gender_ms)) ;  
					element.put("VALUE",values ) ; 
					formStructure.put(element) ; 

					element = new JSONObject() ; 
					values = new JSONArray()  ;
					element.put("type", "LABEL") ; 
					values.put(getResources().getString(R.string.contact_lastname)) ; 
					element.put("VALUE",values ) ; 
					formStructure.put(element) ;
					
					element = new JSONObject() ; 
					values = new JSONArray()  ;
					element.put("type", "text") ; 
					values.put("") ; 
					element.put("VALUE",values ) ; 
					formStructure.put(element) ;
  
					element = new JSONObject() ; 
					values = new JSONArray()  ;
					element.put("type", "LABEL") ; 
					values.put(getResources().getString(R.string.contact_firstname_j)) ; 
					element.put("VALUE",values ) ; 
					formStructure.put(element) ;
					

					element = new JSONObject() ; 
					values = new JSONArray()  ;
					element.put("type", "text") ; 
					values.put("") ; 
					element.put("VALUE",values ) ; 
					formStructure.put(element) ;
					
					element = new JSONObject() ; 
					values = new JSONArray()  ;
					element.put("type", "LABEL") ; 
					values.put(getString(R.string.contact_email_j)) ; 
					element.put("VALUE",values ) ; 
					formStructure.put(element) ;
					
					
					element = new JSONObject() ; 
					values = new JSONArray()  ;
					element.put("type", "email") ; 
					values.put("") ; 
					element.put("VALUE",values ) ; 
					formStructure.put(element) ;
					
					 

					element = new JSONObject() ; 
					values = new JSONArray()  ;
					element.put("type", "LABEL") ; 
					values.put(getResources().getString(R.string.contact_company_j)) ; 
					element.put("VALUE",values ) ; 
					formStructure.put(element) ;
					
					
					element = new JSONObject() ; 
					values = new JSONArray()  ;
					element.put("type", "text") ; 
					values.put("") ; 
					element.put("VALUE",values ) ; 
					formStructure.put(element) ;
					
					
					element = new JSONObject() ; 
					values = new JSONArray()  ;
					element.put("type", "LABEL") ; 
					values.put(getString(R.string.contact_position)) ; 
					element.put("VALUE",values ) ; 
					formStructure.put(element) ;
					
					
					element = new JSONObject() ; 
					values = new JSONArray()  ;
					element.put("type", "text") ; 
					values.put("") ; 
					element.put("VALUE",values ) ; 
					formStructure.put(element) ;
					
					
					element = new JSONObject() ; 
					values = new JSONArray()  ;
					element.put("type", "LABEL") ; 
					values.put(getResources().getString(R.string.contact_address)) ; 
					element.put("VALUE",values ) ; 
					formStructure.put(element) ;
					
					
					element = new JSONObject() ; 
					values = new JSONArray()  ;
					element.put("type", "text") ; 
					values.put("") ; 
					element.put("VALUE",values ) ; 
					formStructure.put(element) ;
					
					
					
					element = new JSONObject() ; 
					values = new JSONArray()  ;
					element.put("type", "address-details") ; 
					values.put(getResources().getString(R.string.contact_zip_code)) ;
					values.put("Ville") ; 
					values.put(getResources().getString(R.string.contact_country)) ; 
					element.put("VALUE",values ) ; 
					formStructure.put(element) ;
					 
					
					element = new JSONObject() ; 
					values = new JSONArray()  ;
					element.put("type", "LABEL") ; 
					values.put(getResources().getString(R.string.contact_phone_j)) ; 
					element.put("VALUE",values ) ; 
					formStructure.put(element) ;
					
					
					element = new JSONObject() ; 
					values = new JSONArray()  ;
					element.put("type", "text") ; 
					values.put("") ; 
					element.put("VALUE",values ) ; 
					formStructure.put(element) ;
					
					
					
					element = new JSONObject() ; 
					values = new JSONArray()  ;
					element.put("type", "COLOR") ; 
					values.put("#18a9e7") ; 
					element.put("VALUE",values ) ; 
					formStructure.put(element) ;
					
					
					
				} catch (JSONException e) {
					// TODO: handle exception
				}
				
				
			
				
				
				Intent intent= new Intent(SignupCreateFormStep1.this, SignupCreateFormStep2.class); 
				intent.putExtra("form_structure", formStructure.toString()) ; 
				startActivity(intent) ;  
			}
		}) ; 
		
		
		gender = (EditText) findViewById(R.id.create_form_gender) ; 
		fname = (EditText) findViewById(R.id.create_form_fname) ; 
		lname = (EditText) findViewById(R.id.create_form_lname) ;
		email = (EditText) findViewById(R.id.create_form_email) ; 
		company =(EditText) findViewById(R.id.create_form_company) ; 
		fonction = (EditText) findViewById(R.id.create_form_fonction) ; 
		zipcode = (EditText) findViewById(R.id.create_form_zipcode) ; 
		city = (EditText) findViewById(R.id.create_form_city); 
		country = (EditText) findViewById(R.id.create_form_country) ; 
		address = (EditText) findViewById(R.id.create_form_address) ; 
		phone = (EditText) findViewById(R.id.create_form_phone) ; 
		
		
		
	}
	
	@Override
	public void onBackPressed() {
	// TODO Auto-generated method stub
	//	super.onBackPressed();
		
		AlertDialog.Builder builder = new AlertDialog.Builder(SignupCreateFormStep1.this) ; 
		builder.setMessage(getResources().getString(R.string.signup_back_error_title)) ; 
		builder.setTitle(getResources().getString(R.string.signup_back_error_title)) ; 
		builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				dialog.dismiss() ; 
			}
		}) ; 
		builder.show() ; 
		
		 
	}


}
