package com.biptag.biptag;

import org.json.JSONArray;
import org.json.JSONObject;

public class ContactItem {
		 
	public String contact_email ;   
	public String contact_name ; 
	public JSONObject contactRespone ;  
	public String form_structure ; 
	public int contactId ; 
	public int form_id ;  
	public int fr_id ; 
	public boolean isSynchronized ;  
	public int contact_local_id ; 
		
		public ContactItem(String contac_email,String contac_name , JSONObject contact_response , String form_strucutre  , int contactid , int form_id , int fr_id , boolean isSynchronized , int contactLocalID  ){
			super() ;  
			
			this.contact_email = contac_email ; 
			this.contact_name= contac_name ; 
			this.contactRespone = contact_response ; 
			this.form_structure = form_strucutre ; 
			this.contactId = contactid ; 
			this.form_id = form_id ; 
			this.fr_id = fr_id ; 
			this.isSynchronized = isSynchronized ; 
			this.contact_local_id = contactLocalID ; 
		}
}
