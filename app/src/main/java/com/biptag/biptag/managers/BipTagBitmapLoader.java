package com.biptag.biptag.managers;

import org.json.JSONException;
import org.json.JSONObject;

import com.biptag.biptag.NavigationActivity;
import com.biptag.biptag.interfaces.AsyncResponse;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.os.AsyncTask;
import android.util.Log;
import android.view.Display;
import android.view.WindowManager;
import android.widget.ImageView;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Calendar;

public class BipTagBitmapLoader  extends AsyncTask<String, String, String>{

	
	public String imagePath  ;
	public Context context ; 
	public ImageView imageView ; 
	Bitmap bitmap ; 
	AsyncResponse response ;
	private static final int REQ_WIDTH = 450;
	private static final int REQ_HEIGHT = 450;
	
	public BipTagBitmapLoader(String imagePath , Context context , ImageView imageView , AsyncResponse response) {
		// TODO Auto-generated constructor stub
		this.imagePath = imagePath ; 
		this.context = context ; 
		this.imageView = imageView ; 
		this.response = response ; 
	}



	@Override
	protected String doInBackground(String... params) {
		// TODO Auto-generated method stub
	//Log.d("size " ,  " doing sth in backgrund?");

try{
	final BitmapFactory.Options options = new BitmapFactory.Options();
	options.inJustDecodeBounds = true;
	bitmap = BitmapFactory.decodeFile(imagePath, options) ;

	// Calculate inSampleSize based on a preset ratio
	options.inSampleSize = calculateInSampleSize(options,450, 450);

	// Decode bitmap with inSampleSize set
	options.inJustDecodeBounds = false;
	bitmap = BitmapFactory.decodeFile(imagePath, options);
	Log.d("image ", "bitmap path " +imagePath);
	if (bitmap != null) {
		Log.d(" bitmap ", " bitmap not null ");
	}else{
		Log.d(" bitmap ", " bitmap null ");
	}
		bitmap =Bitmap.createScaledBitmap(bitmap, bitmap.getWidth() / 16, bitmap.getHeight() / 16, true);

		Log.d("size " ,  " image compressed " + bitmap.getHeight());

	} catch (OutOfMemoryError e) {
		// TODO: handle exception
			e.printStackTrace();
			Log.d("out of memory error", "image out of memory error  " + e.getMessage()) ;
		}




		//	try {
			
		//	BitmapFactory.Options opts=new BitmapFactory.Options();
		//	opts.inDither=false;                     //Disable Dithering mode
	//opts.inPurgeable=true;                   //Tell to gc that whether it needs free memory, the Bitmap can be cleared
		//opts.inInputShareable=true;              //Which kind of reference will be used to recover the Bitmap data after being clear, when it will be used in the future
			//opts.inTempStorage=new byte[32 * 32];

		//	bitmap = BitmapFactory.decodeFile(imagePath, opts)  ;
		//	Log.d("size " ,  " image  compress  " );
		//	bitmap =Bitmap.createScaledBitmap(bitmap, bitmap.getWidth() / 16, bitmap.getHeight() / 16, true);
		//	Log.d("size " ,  " doing sth in backgrund? " + bitmap.getHeight());

	//	} catch (OutOfMemoryError e) {
			// TODO: handle exception
		//	e.printStackTrace();
		//	Log.d("out of memory error", "image out of memory error  " + e.getMessage()) ;
		//}

		
		return null;
	}

	public static int calculateInSampleSize(BitmapFactory.Options options,int reqWidth, int reqHeight) {

		final int height = options.outHeight;
		final int width = options.outWidth;
		int inSampleSize = 1; // default to not zoom image

		if (height > reqHeight || width > reqWidth) {
			final int heightRatio = Math.round((float) height/ (float) reqHeight);
			final int widthRatio = Math.round((float) width / (float) reqWidth);
			inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
		}
		return inSampleSize;
	}


	@Override
	protected void onPostExecute(String result) {
		// TODO Auto-generated method stub
		super.onPostExecute(result);
		Calendar cal = Calendar.getInstance();

	Log.d(" size ",  " BiptagBitmapdownloader at " + cal.get(Calendar.MILLISECOND) +" with image path " +imagePath);
		if(bitmap != null){
//String extension=
			final BitmapFactory.Options opts = new BitmapFactory.Options();
			opts.inJustDecodeBounds = true;
			bitmap = BitmapFactory.decodeFile(imagePath, opts) ;
			Log.d(" size ",  " BiptagBitmapdownloader at " + cal.get(Calendar.MILLISECOND) +" with image path " +imagePath );

			// Calculate inSampleSize based on a preset ratio
			opts.inSampleSize = calculateInSampleSize(opts, REQ_HEIGHT, REQ_WIDTH);

			// Decode bitmap with inSampleSize set
			opts.inJustDecodeBounds = false;
		bitmap = BitmapFactory.decodeFile(imagePath, opts);


			Log.e("original size of image : ", String.valueOf(bitmap.getWidth()) + " " + String.valueOf(bitmap.getHeight()));


			//	ByteArrayOutputStream out = new ByteArrayOutputStream();
			//	bitmap.compress(Bitmap.CompressFormat.JPEG,50,out);
			//Bitmap decoded = BitmapFactory.decodeStream(new ByteArrayInputStream(out.toByteArray()));


			WindowManager wm = (WindowManager) NavigationActivity.getContext().getSystemService(Context.WINDOW_SERVICE);
			Display display  =wm.getDefaultDisplay() ;
			Point point  = new Point() ;
			display.getSize(point) ;
			int screenWidth = point.x ;
			int screenHeight = point.y ;

			int imgHeight = bitmap.getHeight() ;
			int imgWidth  = bitmap.getWidth() ;
			Bitmap resized;
			if(bitmap.getHeight()>1024){
				resized = Bitmap.createScaledBitmap(bitmap, bitmap.getWidth() / 16, bitmap.getHeight() / 16, true);
				Log.e("new size : ", String.valueOf(resized.getWidth()) + " " + String.valueOf(resized.getHeight()));

				imageView.setImageBitmap(resized);
			}else {
				resized = Bitmap.createScaledBitmap(bitmap, bitmap.getWidth() / 5, bitmap.getHeight() / 5, true);
				Log.e("new size : ", String.valueOf(resized.getWidth()) + " " + String.valueOf(resized.getHeight()));
				imageView.setImageBitmap(resized);
			}




		//	Bitmap thumbnail;
		//	thumbnail = Bitmap.createScaledBitmap(bitmap, bitmap.getWidth()/16, bitmap.getHeight()/16, true);
		//	Log.e("new size : ", String.valueOf(thumbnail.getWidth()) + " " + String.valueOf(thumbnail.getHeight()));

		}else{
			Log.d("size ", "this bitmap is null");
			if(response != null){
				try {

					JSONObject object = new JSONObject() ;
					object.put("image_url", imagePath) ; 
					object.put("action", "do_download") ;
					response.processFinish(object.toString()) ; 
				} catch (JSONException e) {
					// TODO: handle exception
				}
				
				
			}
		}
	
	}
	
	
}
