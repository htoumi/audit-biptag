package com.biptag.biptag.managers;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.HashMap;
import java.util.List;

import com.biptag.biptag.R;
import com.biptag.biptag.fragements.edit.EditFormStep1;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.graphics.Typeface;
import android.support.v7.widget.PopupMenu;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.BaseExpandableListAdapter;
import android.widget.Button;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;
 
public class ExpandableListAdapter extends BaseExpandableListAdapter {

	private Context _context;
	private List<HashMap<String, String>> _listDataHeader; // header titles
	// child data in format of header title, child title
	private HashMap<String, List<String>> _listDataChild;
	private HashMap<String, String> numberHeaderMatchingMap ; 

	public ExpandableListAdapter(Context context, List<HashMap<String, String>> listDataHeader,
			HashMap<String, List<String>> listChildData , HashMap<String, String> numberHeaderMatchingMap) {
		this._context = context;
		this._listDataHeader = listDataHeader;
		this._listDataChild = listChildData;
		this.numberHeaderMatchingMap = numberHeaderMatchingMap ; 
	}  

	@Override
	public Object getChild(int groupPosition, int childPosititon) {
		return this._listDataChild.get(this._listDataHeader.get(groupPosition).get("subject"))
				.get(childPosititon);
	}

	@Override
	public long getChildId(int groupPosition, int childPosition) {
		return childPosition;
	}

	@Override
	public View getChildView(final int groupPosition, final int childPosition,
			boolean isLastChild, View convertView, ViewGroup parent) {

		final String childText = (String) getChild(groupPosition, childPosition);

		if (convertView == null) {
			LayoutInflater infalInflater = (LayoutInflater) this._context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = infalInflater.inflate(R.layout.list_item_notif, null);
		} 

		TextView txtListChild = (TextView) convertView
				.findViewById(R.id.lblListItem);

		txtListChild.setText(childText);
		
		convertView.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				WebView view = new WebView(_context) ;
				String encodedHtml = _listDataHeader.get(groupPosition).get("content"); 
				byte[] bytes = encodedHtml.getBytes() ; 
				String html;
					
					html = URLDecoder.decode(encodedHtml) ; 
					//Log.d("html", "html "+html) ;
					view.loadDataWithBaseURL("",html , "text/html", "UTF-8", "");
					AlertDialog.Builder builder = new Builder(_context) ; 
					builder.setTitle("Follow up email") ; 
					builder.setView(view) ; 
					builder.setPositiveButton("OK", new OnClickListener() {
						
						@Override
						public void onClick(DialogInterface dialog, int which) {
							// TODO Auto-generated method stub
							dialog.dismiss() ; 
						}
					}) ; 
					builder.show() ;
				
				
			}
		}) ; 
		
		return convertView;
	}

	@Override
	public int getChildrenCount(int groupPosition) {
		return this._listDataChild.get(this._listDataHeader.get(groupPosition).get("subject"))
				.size();
	}

	@Override
	public HashMap<String, String> getGroup(int groupPosition) {
		return this._listDataHeader.get(groupPosition);
	}

	@Override
	public int getGroupCount() {
		return this._listDataHeader.size();
	}

	@Override
	public long getGroupId(int groupPosition) {
		return groupPosition;
	}

	@Override
	public View getGroupView(int groupPosition, boolean isExpanded,
			View convertView, ViewGroup parent) {
		String headerTitle =  numberHeaderMatchingMap.get(getGroup(groupPosition).get("subject"));
		if (convertView == null) {
			LayoutInflater infalInflater = (LayoutInflater) this._context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = infalInflater.inflate(R.layout.list_group_notif, null);
		}
 
		TextView lblListHeader = (TextView) convertView
				.findViewById(R.id.lblListHeader);
		lblListHeader.setTypeface(null, Typeface.BOLD);
		lblListHeader.setText(headerTitle);
		TextView lblType = (TextView) convertView.findViewById(R.id.log_type) ; 
		TextView lblTimeStamp = (TextView) convertView.findViewById(R.id.log_timestamps) ; 
		lblType.setText(getGroup(groupPosition).get("type")) ; 
		lblTimeStamp.setText(getGroup(groupPosition).get("timestamp")) ; 
		return convertView;
	}

	@Override
	public boolean hasStableIds() {
		return false;
	}

	@Override
	public boolean isChildSelectable(int groupPosition, int childPosition) {
		return true;
	}

}
