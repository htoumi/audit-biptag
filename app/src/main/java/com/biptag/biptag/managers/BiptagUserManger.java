package com.biptag.biptag.managers;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.ProtocolException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.StrictMode;
import android.util.Log;

import com.biptag.biptag.NavigationActivity;

public class BiptagUserManger {
	static SharedPreferences preferences;
	static Editor editor;
	static String prefrencesName = "prefenreces";


	public static boolean isOnline(Context c) {
		ConnectivityManager manager = (ConnectivityManager) c.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo info = manager.getActiveNetworkInfo();
				if (info != null && info.isConnectedOrConnecting()) {
				return true;

		}

		return false;

	}

	public static final boolean isInternetOn() {
		//if (android.os.Build.VERSION.SDK_INT > 9) {
			StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
					.permitAll().build();
			StrictMode.setThreadPolicy(policy);


			try {
				HttpURLConnection urlConnection = null;
				URL url = new URL("http://www.google.com");
				urlConnection = (HttpURLConnection) url.openConnection();
				urlConnection.setAllowUserInteraction(false);
				urlConnection.setInstanceFollowRedirects(true);

				urlConnection.setRequestMethod("GET");

				urlConnection.connect();
				if (urlConnection.getResponseCode() != HttpURLConnection.HTTP_OK)

				{
					Log.e(" connectionion ", "Did not get HTTP_OK response." + HttpURLConnection.HTTP_OK + HttpURLConnection.HTTP_CLIENT_TIMEOUT);
					 Log.e(" connection ", "Response code: " + urlConnection.getResponseCode());
					 Log.e(" connection ", "Response message: " + urlConnection.getResponseMessage().toString());
					return false;
				} else {
					Log.e(" connection ", "Did  get HTTP_OK response." + HttpURLConnection.HTTP_OK + HttpURLConnection.HTTP_CLIENT_TIMEOUT);
					Log.e(" connection ", "Response code: " + urlConnection.getResponseCode());
					Log.e(" connection ", "Response message: " + urlConnection.getResponseMessage().toString());

					return true;
				}


			} catch (ProtocolException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		//}
		return false;
	}

	public static void saveUserData(String key , Object value , Context c ) {
		preferences = c.getSharedPreferences(prefrencesName, Context.MODE_PRIVATE);
		editor = preferences.edit(); 
		if(value.getClass() == Integer.class){
			editor.putInt(key, Integer.parseInt( String.valueOf(value))) ; 
		}
		if(value.getClass() == Float.class){
			editor.putFloat(key, Float.parseFloat(String.valueOf(value))) ; 
		}
		if(value.getClass() == String.class){
			editor.putString(key, String.valueOf(value)) ; 
		}

		 
		editor.commit() ; 

	}
	
	public static Object  getUserValue(String key ,String  type  , Context c) {
		if(c != null){
			
			
			
			preferences = c.getSharedPreferences(prefrencesName, Context.MODE_PRIVATE);
			editor = preferences.edit(); 
			if(type ==  "int"){
				 return preferences.getInt(key, 0) ;
			 }
			 if(type == "string"){
				 return preferences.getString(key, "") ;
			 }
			 if(type== "float"){
				 return preferences.getFloat(key, 0) ;
			 }
			 editor.commit() ; 
				
		}
		 return null; 
		
	}
	public static void clearPreferences(Context c) {
		preferences = NavigationActivity.context.getSharedPreferences(prefrencesName, Context.MODE_PRIVATE);
		editor = preferences.edit(); 
		editor.clear(); 
		editor.commit() ; 
	}
	
	public static void clearField(Context c , String key) {
		preferences = NavigationActivity.context.getSharedPreferences(prefrencesName, Context.MODE_PRIVATE);
		editor = preferences.edit(); 
		if(preferences.getAll().containsKey(key)){
			editor.remove(key) ; 	
		}
		editor.commit() ; 
	}
	
	
	public static void saveRequestInQueue(String url, List<NameValuePair> params , List<String> files, Context context) {
		//Log.d("saving request in queue ", "saving request in queue  methode") ; 
		try {
		
				JSONObject object = new JSONObject() ; 
				
				//Log.d("saving request in queue ", "saving request in queue  saving files") ; 
				
				List<String> names = new ArrayList<String>() ; 
				List<String> value = new ArrayList<String>() ; 
				
				for (int i = 0; i < params.size(); i++) {
					names.add(params.get(i).getName().toString())  ; 
					value.add(params.get(i).getValue().toString()) ; 
				}
				object.put("url", url) ; 
				object.put("files", new JSONArray(files)) ; 
				object.put("names", new JSONArray(names)) ;
				object.put("values", new JSONArray(value)) ;
				String queue = String.valueOf(BiptagUserManger.getUserValue("operation_queue", "string", context)) ;
				JSONArray array = new JSONArray() ; 
				if(queue.length()!=0){
					array= new JSONArray(queue) ; 
				}
				array.put(object) ; 
				BiptagUserManger.saveUserData("operation_queue", array.toString() ,  context) ; 
			
		} catch (JSONException e) {
			// TODO: handle exception
			//Log.d("saving request in queue ", "json saving request in queue exception  "+e.getMessage()) ; 
		}
		catch (Exception e) {
			// TODO: handle exception
			//Log.d("saving request in queue ", "saving request in queue exception  "+e.getMessage()) ; 
		}	
				
				
				
	}
	public static  void performePendingRequest(Context context) {
		//Log.d("performPendingRequest", " performPendingRequest") ; 
		try { 
			String queue = String.valueOf(BiptagUserManger.getUserValue("operation_queue", "string", context)) ;
			JSONArray queues = new JSONArray(queue) ; 
			JSONArray newQueue= new JSONArray() ; 
			for(int i = 0; i < queues.length(); i++) {
				/*object.put("url", url) ; 
				object.put("files", new JSONArray(files)) ; 
				object.put("params", new JSONArray(params.toString())) ;*/
				
				JSONObject operation = queues.getJSONObject(i) ; 
				
				String url = operation.getString("url") ; 
				List<String> list = new ArrayList<String>();     
				JSONArray jsonArray = operation.getJSONArray("files");; 
				if (jsonArray != null) { 
				   int len = jsonArray.length();
				   for (int j=0;j<len;j++){ 
				    list.add(jsonArray.get(j).toString());
				   } 
				} 
				List<String> files =  list ; 
				JSONArray names = operation.getJSONArray("names") ; 
				JSONArray values = operation.getJSONArray("values") ; 
				List<NameValuePair> pairs = new ArrayList<NameValuePair>() ; 
				 
				for (int j = 0; j < names.length(); j++) {
					pairs.add(new BasicNameValuePair(names.getString(j), values.getString(j))) ; 
					
				}
				
				//Log.d("exceuting pending ops", "exceuting pending ops "+pairs.toString() ) ;
				boolean isDone=BiptagUserManger.performOnePendingRequest(context, url, pairs, files) ; 
				if(!isDone){
					JSONObject newOp = new JSONObject() ; 
					newOp.put("url", url) ; 
					newOp.put("files", files) ; 
					newOp.put("names", names) ; 
					newOp.put("values", values) ;
					newQueue.put(newOp) ; 
				}
				
			}
		BiptagUserManger.saveUserData("operation_queue", newQueue.toString() ,  context) ; 
			
		} catch (JSONException e) {
			// TODO: handle exception
			//Log.d("saving request in queue ", "performe pending request"+e.getMessage()) ; 
		}
		catch (Exception e) {
			// TODO: handle exception
			//Log.d("saving request in queue ", "performe pending request  "+e.getMessage()) ; 
		}
  
	}
	public static  boolean performOnePendingRequest(Context context , String url  ,List<NameValuePair> list ,  List<String> files) {
		
		  //Log.d("performOnePendingRequest", " performOnePendingRequest") ; 
		  //Log.d("step3 finish", "performe on request    url  "+url) ;
		  //Log.d("step3 finish", "performe on request     list  "+list.toString()) ;
		  //Log.d("step3 finish", "performe on request    files  "+files.toString()) ;
		
		com.biptag.biptag.HttpClient client = new com.biptag.biptag.HttpClient(url, list) ; 
		  try {
			  client.connectForMultipart() ; 
		for(int index=0; index < files.size(); index++) {
             
             //Log.d("sending image ", "sending image  "+index+"  "+files.get(index)) ; 
            // entity.addPart("attachment", new FileBody(new File (getActivity().getFilesDir()+File.separator+images_list_array.get(index)+".jpg")));
          
             File file =new File (context.getFilesDir()+File.separator+files.get(index)+".jpg");
             
             int size = (int) file.length();
             byte[] bytes = new byte[size];
           
                 BufferedInputStream buf = new BufferedInputStream(new FileInputStream(file));
                 buf.read(bytes, 0, bytes.length);
                 buf.close();
                 client.addFilePart("attachment[file]["+index+"]", "picture"+index+".jpg", bytes ) ; 
                 
            
            
      }
		client.finishMultipart() ; 
		String data = client.getResponse() ; 
		//Log.d("step3 finish", "client response  data    "+data) ; 
		return true ; 
		
		  } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            //Log.d("step3 finish", "performe on request    filenot found  "+e.getMessage()) ; 
            return false ; 
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            //Log.d("step3 finish", "performe on request     IO Exc "+e.getMessage()) ;
            return false ; 
        } catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			//Log.d("step3 finish", "performe on request   Exce   "+e.getMessage()) ;
			return false ; 
		}
		  
	}
	
	
	
	
	

}
