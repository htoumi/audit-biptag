package com.biptag.biptag.managers;

import java.util.LinkedList;
import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.FrameLayout;

import com.biptag.biptag.ListingFilesActivity;
import com.biptag.biptag.NavigationActivity;
import com.biptag.biptag.R;
import com.biptag.biptag.fragements.ConnectedhomeFragment;
import com.biptag.biptag.fragements.ContactFragement;
import com.biptag.biptag.fragements.EditFollowUpFragement;
import com.biptag.biptag.fragements.EditOCRFragment;
import com.biptag.biptag.fragements.GalleryImageFragment;
import com.biptag.biptag.fragements.NotificationFragment;
import com.biptag.biptag.fragements.create.FillingFormStep1;
import com.biptag.biptag.fragements.create.FillingFormStep2;
import com.biptag.biptag.fragements.edit.EditFormStep1;
import com.biptag.biptag.fragements.edit.EditFormStep2;
import com.biptag.biptag.fragements.settings.SettingGeneralMenu;
import com.biptag.biptag.fragements.settings.SettingMenuFragment;
import com.biptag.biptag.fragements.settings.SettingSynchronisationFragment;

public class BiptagNavigationManager {


	public static void pushFragments(Fragment frag, boolean shouldAdd, String tag , final Context context , String data , int stack) {



		View view = ((Activity) context).getCurrentFocus();
		if (view != null) {
		    InputMethodManager imm = (InputMethodManager)((Activity) context).getSystemService(Context.INPUT_METHOD_SERVICE);
		    imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
		}


		String originalTag = tag ;


		String isSync = String.valueOf(BiptagUserManger.getUserValue("IS_SYNC", "string", context)) ;
		if(isSync.equals("") && ((Activity) context) != null && !((Activity) context).isFinishing()  ){
			if(frag != null)
			tag = frag.getClass().getName() ;

			FragmentManager manager = ((Activity) context).getFragmentManager();
	         FragmentTransaction ft = manager.beginTransaction();



	        if (shouldAdd) {



		        if(frag != null) {

		        	int indexOfFrag = -1 ;
		        	List<Fragment> costumStack = null ;
		        	if(stack == 1){
		        		costumStack  = NavigationActivity.stack1 ;

		        	}
		        	if(stack == 2){
		        		costumStack  = NavigationActivity.stack2 ;

		        	}
		        	if(stack == 3){
		        		costumStack  = NavigationActivity.stack3 ;
		        	}

		        	for (int i = costumStack.size()-1; i > 0; i--) {
		        		if(costumStack.get(i).getClass().getName().equals(frag.getClass().getName())){

		        			if(frag.getClass().getName().equals(EditFollowUpFragement.class.getName())|| frag.getClass().getName().equals(GalleryImageFragment.class.getName())){
		        				costumStack.set(i, frag)  ;
		        			}
		        			frag = costumStack.get(i) ;
		        			indexOfFrag = i ;
							if (frag.getClass().getName().equals(SettingMenuFragment.class.getName())||frag.getClass().getName().equals(SettingGeneralMenu.class.getName())||frag.getClass().getName().equals(SettingSynchronisationFragment.class.getName())){
								indexOfFrag = -1;
							}
		        		}

					}

		        	if(data!= null){
			        	Bundle bundle= new Bundle();
			        	bundle.putString("data", data);
						Log.d("ocr result ", "contact object data " + data);

						frag.setArguments(bundle);
			        }

		        	 FrameLayout realtabcontent = (FrameLayout)  ((Activity) context).findViewById(R.id.realtabcontent) ;
		        	 realtabcontent.removeAllViews() ;
		        	 ft.replace(R.id.realtabcontent, frag ) ;

			        	//ft.addToBackStack(tag);

			        	  if(stack == 1){
			        		  		if(indexOfFrag == -1)
			        		  			NavigationActivity.stack1.add(frag);
			        		  		NavigationActivity.tab1ActivFragment = frag ;

			  	        	}
			  	        if(stack == 2 ){
			  	        	if(indexOfFrag == -1 )
			  	        	NavigationActivity.stack2.add(frag);
			  	        	NavigationActivity.tab2ActivFragment = frag ;
			  	        }
			  	        if(stack == 3){
			  	        	if(indexOfFrag == -1)
			  	        		NavigationActivity.stack3.add(frag) ;
							Log.d(" Stack ", "3.0" + NavigationActivity.stack3.size() + NavigationActivity.stack3);
			  	        		NavigationActivity.tab3ActivFragment = frag ;
			  	        }

		        }


	        } else {

	        	List<Fragment> costumStack = null ;
	        	if(stack == 1){
	        		costumStack  = NavigationActivity.stack1 ;
					Log.d("Stack ", "1 " + costumStack.toString());
	        	}
	        	if(stack == 2){
	        		costumStack  = NavigationActivity.stack2 ;
					Log.d("Stack ", "2 " + costumStack.toString());
	        	}
	        	if(stack == 3){

	        		costumStack  = NavigationActivity.stack3 ;
					Log.d("Stack ", "3 " + costumStack.toString());
	        	}


	        	int index = 0 ;

	        	for (int i = 1; i < costumStack.size(); i++) {


	        		if(originalTag.equals(GalleryImageFragment.class.getName())){

	        			if(costumStack.get(i).getClass().getName().equals(FillingFormStep1.class.getName()) || costumStack.get(i).getClass().getName().equals(EditFormStep1.class.getName())){
	        				frag = costumStack.get(i) ;
							Log.d("navigation ", "is it ");
	        				break ;
	        			}


	        		}else{

	        			if(costumStack.get(i).getClass().getName().equals(tag) ){
							frag = costumStack.get(i-1) ;
							index = i ;
								break ;

						}


	        		}
					if(originalTag.equals(EditOCRFragment.class.getName())){
						Log.d("navigation ", costumStack.get(i).getClass().getName());
	        			if(costumStack.get(i).getClass().getName().equals(FillingFormStep1.class.getName()) || costumStack.get(i).getClass().getName().equals(EditFormStep1.class.getName())){
	        				frag = costumStack.get(i) ;
							Log.d("navigation ", "is it ");
	        				break ;
	        			}


	        		}else{

	        			if(costumStack.get(i).getClass().getName().equals(tag) ){
							frag = costumStack.get(i-1) ;
							index = i ;
								break ;

						}


	        		}
				if(originalTag.equals(ContactFragement.class.getName())){
				Log.d("navigation ", costumStack.get(i).getClass().getName());
	        			if( costumStack.get(i).getClass().getName().equals(EditFormStep1.class.getName())){
	        				frag = costumStack.get(i) ;
							Log.d("navigation ", "is it ");
	        				break ;
	        			}


	        		}else{

	        			if(costumStack.get(i).getClass().getName().equals(tag) ){
							frag = costumStack.get(i-1) ;
							index = i ;
								break ;

						}


	        		}

					if(originalTag.equals(FillingFormStep2.class.getName())){

						if(costumStack.get(i).getClass().getName().equals(FillingFormStep1.class.getName()) ){
							frag = costumStack.get(i) ;
							Log.d("navigation ", "is it ");
							break ;
						}


					}else{

						if(costumStack.get(i).getClass().getName().equals(tag) ){
							frag = costumStack.get(i-1) ;
							index = i ;
							break ;

						}

						if(originalTag.equals(EditFormStep2.class.getName())){

							if(costumStack.get(i).getClass().getName().equals(EditFormStep1.class.getName())){
								frag = costumStack.get(i) ;
								Log.d("navigation ", "is it ");
								break ;
							}


						}else{

							if(costumStack.get(i).getClass().getName().equals(tag) ){
								frag = costumStack.get(i-1) ;
								index = i ;
								break ;

							}


						}


					}

				}







	        	if(frag != null){
	        		 if( frag.getClass().getName().indexOf(GalleryImageFragment.class.getName()) !=-1|| frag.getClass().getName().indexOf(EditFollowUpFragement.class.getName()) != -1 || frag.getClass().getName().indexOf(ListingFilesActivity.class.getName()) != -1|| frag.getClass().getName().indexOf(NotificationFragment.class.getName()) != -1 ){
	      			  	frag = costumStack.get(index-2) ;
	 	        	 }

	        		 if(stack == 2){
	        				 for (int i = 0; i < costumStack.size(); i++) {
								if(costumStack.get(i).getClass().getName().equals(EditFollowUpFragement.class.getName())){
									costumStack.remove(i) ;
									break ;
								}
	        			 }
	        		 }


	        		 if(stack == 3){
	      					try {
								costumStack.remove(index) ;
							} catch (Exception e) {
								// TODO: handle exception
							}
	      				}

	        		 boolean shouldRemoveFrag = true ;

	        		if(stack == 1){
	        			shouldRemoveFrag = !frag.getClass().getName().equals(NavigationActivity.tab1ActivFragment.getClass().getName()) ;
	        			if(tag.equals(FillingFormStep1.class.getName()) && frag.getClass().getName().equals(ConnectedhomeFragment.class.getName())){
	        				shouldRemoveFrag = false ;
	        				AlertDialog.Builder builder = new AlertDialog.Builder(context) ;
			        		builder.setMessage(context.getResources().getString(R.string.lost_data_desc)) ;
			        		builder.setTitle(context.getResources().getString(R.string.lost_data_title)) ;
			        		builder.setCancelable(false) ;
			        		final Fragment fragment = frag ;

			        		builder.setNegativeButton(context.getResources().getString(R.string.yes), new OnClickListener() {

								@Override
								public void onClick(DialogInterface dialog, int which) {
									// TODO Auto-generated method stub
									dialog.dismiss() ;
									FillingFormStep1.fragmentView = null;
									NavigationActivity.tab1ActivFragment = fragment;
									FrameLayout realtabcontent = (FrameLayout)  ((Activity) context).findViewById(R.id.realtabcontent) ;
						        	 realtabcontent.removeAllViews() ;
						        	 FragmentManager manager = ((Activity) context).getFragmentManager();
						        	 FragmentTransaction ft = manager.beginTransaction();
						        	 ft.replace(R.id.realtabcontent, fragment ) ;
						        	 ft.commit() ;
								}
							}) ;
			        		builder.setPositiveButton(context.getResources().getString(R.string.no), new OnClickListener() {

								@Override
								public void onClick(DialogInterface dialog, int which) {
									// TODO Auto-generated method stub
									dialog.dismiss() ;
								}
							}) ;

			        		builder.show() ;

	        			 }else{
	        				 NavigationActivity.tab1ActivFragment = frag;
	        			 }
		        	}
		        	if(stack == 2){
		        		shouldRemoveFrag = !frag.getClass().getName().equals(NavigationActivity.tab2ActivFragment.getClass().getName()) ;


		        				if(tag.equals(EditFormStep1.class.getName()) && frag.getClass().getName().equals(ContactFragement.class.getName())){
			        				shouldRemoveFrag = false ;
			        				AlertDialog.Builder builder = new AlertDialog.Builder(context) ;
					        		builder.setMessage(context.getResources().getString(R.string.lost_data_desc)) ;
					        		builder.setTitle(context.getResources().getString(R.string.lost_data_title)) ;
					        		builder.setCancelable(false) ;
					        		if(frag.getClass().getName().equals(ContactFragement.class.getName()))
					        			frag = new ContactFragement() ;

					        		final Fragment fragment = frag ;

					        		builder.setNegativeButton(context.getResources().getString(R.string.yes), new OnClickListener() {

										@Override
										public void onClick(DialogInterface dialog, int which) {
											// TODO Auto-generated method stub
											dialog.dismiss() ;
											EditFormStep1.fragmentView = null ;
											NavigationActivity.tab2ActivFragment = fragment;
											NavigationActivity.stack2 = new LinkedList<Fragment>() ;
											NavigationActivity.stack2.add(fragment) ;
											FrameLayout realtabcontent = (FrameLayout)  ((Activity) context).findViewById(R.id.realtabcontent) ;
								        	 realtabcontent.removeAllViews() ;
								        	 FragmentManager manager = ((Activity) context).getFragmentManager();
								        	 FragmentTransaction ft = manager.beginTransaction();
								        	 ft.replace(R.id.realtabcontent, fragment ) ;
								        	 ft.commit() ;
										}
									}) ;
					        		builder.setPositiveButton(context.getResources().getString(R.string.no), new OnClickListener() {

										@Override
										public void onClick(DialogInterface dialog, int which) {
											// TODO Auto-generated method stub
											dialog.dismiss() ;
										}
									}) ;

					        		builder.show() ;

			        			 }else{
			        				 NavigationActivity.tab2ActivFragment = frag;
			        			 }





		        	}
		        	if(stack == 3){
		        		shouldRemoveFrag = !frag.getClass().getName().equals(NavigationActivity.tab3ActivFragment.getClass().getName()) ;
		        		NavigationActivity.tab3ActivFragment =frag ;
		        	}



	        		if(shouldRemoveFrag){
	        			if(frag.getClass().getName().equals(ContactFragement.class.getName())){
	        				frag = new ContactFragement() ;
	        				NavigationActivity.stack2 = new LinkedList<Fragment>() ;
	        				NavigationActivity.stack2.add(frag) ;

	        			}

	        			FrameLayout realtabcontent = (FrameLayout)  ((Activity) context).findViewById(R.id.realtabcontent) ;
			        	 realtabcontent.removeAllViews() ;
			        	 ft.replace(R.id.realtabcontent, frag ) ;

	        		}

	        	}else{

	        	}


	        }
	        ft.commit();
		}

}


}
