package com.biptag.biptag.managers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.util.Log;

public class NetworkStateReceiver extends BroadcastReceiver {
	public  Context context ; 
	@Override
	public void onReceive(Context context, Intent intent) {
		// TODO Auto-generated method stub
		 //Log.d("app","Network connectivity change");
		 this.context = context ; 
	     if(intent.getExtras()!=null) {
	        NetworkInfo ni=(NetworkInfo) intent.getExtras().get(ConnectivityManager.EXTRA_NETWORK_INFO);
	        if(ni!=null && ni.getState()==NetworkInfo.State.CONNECTED) {
	            //Log.i("app","Network "+ni.getTypeName()+" connected");
	           new PerformRequestInBackground().execute() ; 
	        } else if(intent.getBooleanExtra(ConnectivityManager.EXTRA_NO_CONNECTIVITY,Boolean.FALSE)) {
	           // Log.d("app","There's no network connectivity");
	        }
	}
	}
	
	
	class PerformRequestInBackground extends AsyncTask<String, String, String>{

		@Override
		protected String doInBackground(String... params) {
			// TODO Auto-generated method stub
			 BiptagUserManger.performePendingRequest(context) ; 
			return null;
		}
		
	}

}
