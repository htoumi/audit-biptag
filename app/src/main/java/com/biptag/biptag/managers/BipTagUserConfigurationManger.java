package com.biptag.biptag.managers;

import android.util.Log;

import com.biptag.biptag.NavigationActivity;
import com.biptag.biptag.dbmanager.datasource.BtUserConfigDataSource;
import com.biptag.biptag.dbmanager.objects.BTUserConfig;

public class BipTagUserConfigurationManger {
	
	public static BTUserConfig getUserConfig(int agent_id){
		BtUserConfigDataSource source  = new BtUserConfigDataSource(NavigationActivity.getContext()) ; 
		//Log.d("closing data source", "open data source") ; 
		BTUserConfig  config = source.getUserConfig(agent_id) ; 
		if(config== null){ 
			config = new BTUserConfig() ; 
			config.setConfigId(0) ; 
			config.setDefaultCountry("") ; 
			config.setLanguage("") ; 
			config.setAgent_id(agent_id) ; 		
		}
		//Log.d("closing data source", "closing data source") ; 
		return config ; 
	}
	
	public static long commitConfig(BTUserConfig config ){
		long id= 0;
	BtUserConfigDataSource source  = new BtUserConfigDataSource(NavigationActivity.getContext()) ; 
		if(config.getConfigId()== 0){
		id=source.insertConfig(config) ; 
		}else{
		source.updateConfig(config) ; 
		id= config.getAgent_id() ; 
		}
		
		//Log.d("user config", "user config commit change "+id) ;
		
		return id; 
	}


}
