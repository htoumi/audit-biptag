package com.biptag.biptag.managers;

import java.io.IOException;
import java.io.InputStream;
import java.util.LinkedList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Fragment;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.GradientDrawable;
import android.os.Build;
import android.text.InputType;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.CheckBox;
import android.widget.GridLayout;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TableRow.LayoutParams;
import android.widget.TextView;

import com.biptag.biptag.NavigationActivity;
import com.biptag.biptag.R;
import com.biptag.biptag.ViewId;
import com.biptag.biptag.CustumViews.BTEditText;
import com.biptag.biptag.CustumViews.ToggleButtonGroupTableLayout;
import com.biptag.biptag.dbmanager.datasource.BTFormDataSource;
import com.biptag.biptag.dbmanager.objects.BTFormulaire;
import com.biptag.biptag.fragements.create.FillingFormStep1;

public class BipTagFormsManager {

	public static void	 buidFormIcons(final Context c , Fragment fragment) {
		WindowManager wm = (WindowManager) c.getSystemService(Context.WINDOW_SERVICE);
		Display display  =wm.getDefaultDisplay() ;
		Point point  = new Point() ;
		display.getSize(point) ;
		int screenWidth = point.x ;
		int screenHeight = point.y ;

		int formIconWidth  =   (int) (3.5 *screenWidth / 8) ;
		int formIconHeight = screenHeight / 6    ;

		String color = null ;
		//Log.d("create forms", "create forms childcount 0 ") ;

		try {

			TableLayout gridLayout = (TableLayout) fragment.getView().findViewById(R.id.connectedhome_form_container)   ;

			if(((TableLayout) gridLayout).getChildCount() > 0)
				((TableLayout) gridLayout).removeAllViews();


			LayoutInflater inflater = LayoutInflater.from(c);

			BTFormDataSource dataSource = new BTFormDataSource(NavigationActivity.getContext()) ;
			Object agent_id_object = BiptagUserManger.getUserValue("AGENT_ID", "int", NavigationActivity.getContext());
			int agent_id = Integer.parseInt(String.valueOf(agent_id_object)) ;

			List<BTFormulaire> list = dataSource.getAllForms(agent_id) ;
			gridLayout.setWeightSum(list.size()) ;
			int gridLayoutSumWeight = 0 ;
			//Log.d("json form status ", "json form status forms size  "+list.size())  ;

			for(int i = 0 ; i < list.size() ; i++){

				BTFormulaire formulaire = list.get(i) ;

				//Log.d("saving form ", "saving form  build icon "+formulaire.getEventEndDate()) ;

				//Log.d("json form status ", "json form status "+formulaire.getFormStatus())  ;
				if(formulaire.getFormStatus().equals("PUBLISHED")){
					//Log.d("json form status ", "json form status accepted "+formulaire.getFormStatus())  ;

					final LinearLayout formicon = (LinearLayout) inflater.inflate(R.layout.formicon, null, false);
					//form_icon_bar_color

					JSONArray stucture = formulaire.getFormStructure() ;
					color = "#000000" ;

					for(int j = 0 ; j< stucture.length() ; j++){
						if(stucture.getJSONObject(j).getString("type").equals("COLOR")){
							//	//Log.d("building forms json exception", "building forms json exception coloàr befor ex  "+stucture.getJSONObject(j).getJSONArray("VALUE").toString()) ;
							if(!stucture.getJSONObject(j).getJSONArray("VALUE").getString(0).equals("null"))
								color = stucture.getJSONObject(j).getJSONArray("VALUE").getString(0) ;
						}
					}
					//	Log.d("building forms json exception", "building forms json exception coloàr befor ex  "+color) ;
					int backgroud =0  ;
					if(color.length()> 2) {
						backgroud= Color.parseColor(color);

					}

					String[] startDateArray = formulaire.getEventStartDate().split("-") ;
					String startDateString = startDateArray[2]+"-"+startDateArray[1]+"-"+startDateArray[0] ;


					String[] endDateArray = formulaire.getEventEndDate().split("-") ;
					String endDateString = endDateArray[2]+"-"+endDateArray[1]+"-"+endDateArray[0] ;




					((TextView) formicon.findViewById(R.id.form_icon_bar_color_right
					)).setBackgroundColor(backgroud) ;
					String form_name = formulaire.getFormName().toString();
					if (form_name.length()>40){
						form_name = form_name.substring(0, 40);
						form_name +="...";
					}

					String event_name = formulaire.getEventName().toString();
					if (event_name.length()>20){
						event_name = event_name.substring(0,20);
						event_name+="...";
					}

					//((TextView) formicon.findViewById(R.id.form_icon_bar_color_up)).setBackgroundColor(backgroud) ;
					((TextView) formicon.findViewById(R.id.formicon_formlabel)).setText(form_name) ;
					((TextView) formicon.findViewById(R.id.formicon_form_id)).setText(String.valueOf(formulaire.getFormId())) ;
					((TextView) formicon.findViewById(R.id.form_icon_formevname)).setText(event_name) ;
					((TextView) formicon.findViewById(R.id.formicon_formedition)).setText(formulaire.getEventEdition()) ;
					//Log.d("", "form date "+startDateString+" "+endDateString) ;
					((TextView) formicon.findViewById(R.id.formicon_formvalidity)).setText(c.getResources().getString(R.string.form_icon_from)+" "+startDateString+" "+c.getResources().getString(R.string.form_icon_to)+" "+endDateString)  ;

					// setting fonts

					((TextView) formicon.findViewById(R.id.formicon_formlabel)).setTextSize((int) c.getResources().getDimension(R.dimen.form_name_text_size) ) ;
					((TextView) formicon.findViewById(R.id.form_icon_formevname)).setTextSize((int) c.getResources().getDimension(R.dimen.event_name_text_size) ) ;
					((TextView) formicon.findViewById(R.id.formicon_formedition)).setTextSize((int) c.getResources().getDimension(R.dimen.event_edition_text_size) ) ;
					((TextView) formicon.findViewById(R.id.formicon_formvalidity)).setTextSize((int) c.getResources().getDimension(R.dimen.event_date_text_size) ) ;


					//formicon.setLayoutParams(new TableLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT, 1)) ;
					formicon.setOnClickListener(new View.OnClickListener() {

						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
							FillingFormStep1 formStep1 =  new FillingFormStep1();
							List<Fragment> stack= NavigationActivity.stack1 ;
							for (int j = stack.size()-1; j > 1; j--) {
								stack.remove(j) ;
							}
							BiptagNavigationManager.pushFragments(formStep1, true, "create_form_1", NavigationActivity.getContext() ,((TextView) formicon.findViewById(R.id.formicon_form_id)).getText().toString() , 1 );
						}
					});

					//Log.d("create forms", "create forms childcount "+gridLayout.getChildCount()) ;
					TableRow.LayoutParams layoutParams =new TableRow.LayoutParams(formIconWidth, formIconHeight) ;
					layoutParams.setMargins(screenWidth/32, 0, screenWidth/32, 0) ;
					formicon.setLayoutParams(layoutParams);
					GradientDrawable border = new GradientDrawable();
					border.setColor(backgroud); //white background
					border.setStroke(8, backgroud); //black border with full opacity
					if(Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
						formicon.setBackgroundDrawable(border);
					} else {
						formicon.setBackground(border);
					}
					//formicon.setPadding(screenHeight/8,0, screenHeight/8, 0) ;

					if(gridLayout.getChildCount()== 0){


						TableRow tr = new TableRow(c);
						tr.addView(formicon) ;

						gridLayout.addView(tr)  ;
					}else{
						TableRow row = (TableRow) gridLayout.getChildAt(gridLayout.getChildCount()-1) ;
						//Log.d("create forms", "create forms childcount "+row.getChildCount()) ;
						if(row.getChildCount() == 3){


							//gridLayoutSumWeight+= 1.5 ;
							TextView blanc = new TextView(c);
							blanc.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT,0.5f));
							blanc.setBackgroundColor(Color.BLACK) ;
							gridLayout.addView(blanc);
							//gridLayoutSumWeight++ ;
							TableRow tr = new TableRow(c);
							tr.setBackgroundColor(Color.TRANSPARENT) ;
							//tr.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.MATCH_PARENT,1f));
							tr.addView(formicon) ;
							gridLayout.addView(tr)  ;
						}else{

							if(row.getChildCount() == 1){

								//formicon.setPadding(20, 0, 0, 0) ;

								TextView blanc = new TextView(c);

								//gridLayoutSumWeight+= 1.5 ;
								blanc.setLayoutParams(new TableRow.LayoutParams(screenWidth/120, TableRow.LayoutParams.WRAP_CONTENT));
								row.addView(blanc)   ;
							}


							row.addView(formicon) ;



						}
					}








				}


			}
			gridLayout.setWeightSum(gridLayoutSumWeight)  ;

		}

		catch (JSONException e) {
			// TODO: handle exception
			//Log.d("building forms json exception", "building forms json exception "+color) ;

			//Log.d("building forms json exception", "building forms json exception "+e.getMessage()) ;
		}
		/*catch (Exception e ){
			Log.d("building forms json exception", "building forms json exception "+color) ;
			Log.d("building forms json exception", "building forms json exception"+e.getMessage()) ;
		}*/





	}



	public static List<String[]> buildFormComposant(int form_id , final Context c , Fragment fragment , boolean isStructuredData , int offset , int limit, int layout_id, View view  ){
		ViewId idGenerator = ViewId.getInstance() ;
		WindowManager wm = (WindowManager) c.getSystemService(Context.WINDOW_SERVICE);
		Display display  =wm.getDefaultDisplay() ;
		Point point  = new Point() ;
		display.getSize(point) ;
		int screenWidth = point.x ;
		int screenHeight = point.y ;

		if(view== null){
			//Log.d("building form ", "buidlinf form layout null ") ;
		}
		LinearLayout layout = (LinearLayout) view.findViewById(R.id.filling_form_step1_form_container) ;
		view.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.MATCH_PARENT,8f)) ;
		try {

			BTFormDataSource dataSource = new BTFormDataSource(NavigationActivity.getContext()) ;

			Object agent_id_object = BiptagUserManger.getUserValue("AGENT_ID", "int", NavigationActivity.getContext());
			int agent_id = Integer.parseInt(String.valueOf(agent_id_object)) ;

			List<BTFormulaire> allForms = dataSource.getAllForms(agent_id) ;
			BTFormulaire form_concerned  = null;
			for (int i = 0; i < allForms.size(); i++) {

				if(allForms.get(i).getFormId()== form_id){
					form_concerned = allForms.get(i) ;
					break ;
				}
			}
			JSONArray structure = form_concerned.getFormStructure() ;
			if(limit== -1 ){
				limit = structure.length() ;
			}
			List<String[]> list = new LinkedList<String[]>() ;
			layout.setWeightSum(limit-offset) ;
			if(limit> structure.length() ){
				limit = structure.length() ;
			}

			for (int i = offset; i < limit; i++) {
				JSONObject element = structure.getJSONObject(i) ;
				//Log.d("json structure ", "composant def  "+i+" "+element.toString());
				String type = element.getString("type") ;
				String value  = element.getJSONArray("VALUE").getString(0) ;
				if (value.length()>80){
					value = value.substring(0,80);
					value+="...";
				}
				if(type.equals("LABEL")){
					TextView textView = new TextView(c);
					textView.setText(value) ;
					textView.setTextSize(NavigationActivity.getContext().getResources().getDimension(R.dimen.questions_text_size));
					if(isStructuredData){
						textView.setVisibility(View.GONE) ;
					}
					TableLayout.LayoutParams params = new TableLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT, 1f) ;
					params.setMargins(screenWidth/20, screenHeight/100, screenWidth/20, screenHeight/100) ;
					textView.setLayoutParams(params);
					//int viewid = textView.generateViewId();
					int viewid = idGenerator.getUniqueId() ;
					textView.setId(viewid) ;
					String[] def = new String[]{String.valueOf(i), "LABEL" ,String.valueOf(viewid) } ;
					//Log.d("c", "composant def tab  "+String.valueOf(i)+" LABEL "+String.valueOf(viewid)) ;
					list.add(def) ;
					layout.addView(textView) ;
				}

				if(type.equals("heading")){
					TextView textView = new TextView(c);
					textView.setText(value) ;
					textView.setTextSize(22);
					if(isStructuredData){
						textView.setVisibility(View.GONE) ;
					}
					TableLayout.LayoutParams params = new TableLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT, 1f) ;
					params.setMargins(screenWidth/20, screenHeight/100, screenWidth/20, screenHeight/100) ;
					textView.setLayoutParams(params);
					//int viewid = textView.generateViewId();
					int viewid = idGenerator.getUniqueId() ;
					textView.setId(viewid) ;
					String[] def = new String[]{String.valueOf(i), "heading" ,String.valueOf(viewid) } ;
					//Log.d("c", "composant def tab  "+String.valueOf(i)+" heading "+String.valueOf(viewid)) ;
					list.add(def) ;
					layout.addView(textView) ;
				}


				if(type.equals("text")){
					BTEditText editText = new BTEditText(c);
					TableLayout.LayoutParams params = new TableLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT, 1f) ;
					params.setMargins(screenWidth/20, screenHeight/100, screenWidth/20, screenHeight/100) ;

					if(i == 16 || i == 15){
						if(isStructuredData)
							editText.setInputType(InputType.TYPE_CLASS_PHONE) ;
					}else{
						editText.setInputType(InputType.TYPE_TEXT_FLAG_CAP_SENTENCES)  ;
					}
					editText.setLayoutParams(params);
					editText.setFreezesText(true) ;
					if(isStructuredData){
						JSONObject object = structure.getJSONObject(i-1) ;
						String value_previous_element = object.getJSONArray("VALUE").getString(0) ;
						editText.setHint(value_previous_element) ;
						//Log.d("structured data ", "stuctured data index value "+i+"   "+value_previous_element) ;

					}






					//	int viewid = editText.generateViewId();
					int viewid = idGenerator.getUniqueId() ;
					editText.setId(viewid) ;
					String[] def = new String[]{String.valueOf(i), "text" ,String.valueOf(viewid) } ;
					list.add(def) ;
					editText.setBackgroundColor(Color.parseColor("#efeff4"))  ;
					layout.addView(editText) ;
				}


				if(type.equals("address-details")){
					JSONObject object = structure.getJSONObject(i) ;
					//Log.d("address-details", "address-details "+object.toString()) ;
					JSONArray value_previous_element = object.getJSONArray("VALUE") ;
					LinearLayout addressContainer = new LinearLayout(c) ;
					TableLayout.LayoutParams params = new TableLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT, 1f) ;
					params.setMargins(screenWidth/20, screenHeight/100, screenWidth/20, screenHeight/100) ;
					addressContainer.setLayoutParams(params) ;
					addressContainer.setOrientation(LinearLayout.HORIZONTAL) ;
					//addressContainer.setLayoutParams(new FrameLayout.LayoutParams(android.widget.FrameLayout.LayoutParams.MATCH_PARENT , android.widget.FrameLayout.LayoutParams.WRAP_CONTENT)) ;
					for(int k = 0 ; k < value_previous_element.length() ; k++ ){

						BTEditText editText = new BTEditText(c);
						//Log.d("address details", "address details "+k+" "+value_previous_element.getString(k)) ;
						editText.setHint(value_previous_element.getString(k)) ;
						editText.setBackgroundColor(Color.parseColor("#efeff4"))  ;
						LinearLayout.LayoutParams paramsAdressDetails =new LinearLayout.LayoutParams(screenWidth/5,LayoutParams.MATCH_PARENT , 1f) ;
						paramsAdressDetails.setMargins(0, 0, screenWidth/50, 0) ;
						editText.setLayoutParams(paramsAdressDetails);
						if(k==0){
							editText.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_TEXT_VARIATION_PHONETIC) ;
						}


						int viewid = idGenerator.getUniqueId() ;
						editText.setId(viewid) ;
						String[] def = new String[]{String.valueOf(i*10+k+1), "address-details" ,String.valueOf(viewid) } ;
						list.add(def) ;
						//editText.setLayoutParams(new FrameLayout.LayoutParams(android.widget.FrameLayout.LayoutParams.MATCH_PARENT , android.widget.FrameLayout.LayoutParams.WRAP_CONTENT)) ;

						addressContainer.addView(editText) ;


					}
					addressContainer.setBackgroundColor(Color.parseColor("#00efeff4"))  ;
					layout.addView(addressContainer) ;




					//	int viewid = editText.generateViewId();
				}



				if(type.equals("number")){
					BTEditText editText = new BTEditText(c);
					TableLayout.LayoutParams params = new TableLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT, 1f) ;
					params.setMargins(screenWidth/20, screenHeight/100, screenWidth/20, screenHeight/100) ;
					editText.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_TEXT_VARIATION_PHONETIC) ;
					editText.setLayoutParams(params);
					editText.setFreezesText(true) ;
					if(isStructuredData){
						JSONObject object = structure.getJSONObject(i-1) ;
						String value_previous_element = object.getJSONArray("VALUE").getString(0) ;
						editText.setHint(value_previous_element) ;
						//Log.d("structured data ", "stuctured data index value "+i+"   "+value_previous_element) ;

					}


					//	int viewid = editText.generateViewId();
					int viewid = idGenerator.getUniqueId() ;
					editText.setId(viewid) ;
					String[] def = new String[]{String.valueOf(i), "text" ,String.valueOf(viewid) } ;
					list.add(def) ;
					editText.setBackgroundColor(Color.parseColor("#efeff4"))  ;
					layout.addView(editText) ;
				}


				if(type.equals("email")){
					BTEditText editText = new BTEditText(c);
					TableLayout.LayoutParams params = new TableLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT, 1f) ;
					params.setMargins(screenWidth/20, screenHeight/100, screenWidth/20, screenHeight/100) ;
					editText.setLayoutParams(params);
					editText.setFreezesText(true) ;

					if(isStructuredData){
						JSONObject object = structure.getJSONObject(i-1) ;
						String value_previous_element = object.getJSONArray("VALUE").getString(0) ;
						editText.setHint(value_previous_element) ;
						editText.setBackgroundColor(Color.parseColor("#efeff4"));
						//Log.d("structured data ", "stuctured data index value "+i+"   "+value_previous_element) ;

					}


					//int viewid = editText.generateViewId();
					int viewid = idGenerator.getUniqueId() ;
					editText.setId(viewid) ;
					String[] def = new String[]{String.valueOf(i), "email" ,String.valueOf(viewid) } ;
					list.add(def) ;
					editText.setBackgroundColor(Color.parseColor("#efeff4"))  ;
					editText.setInputType( InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS | InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS ); ;

					layout.addView(editText) ;
				}

				if(type.equals("text_area")){
					BTEditText editText = new BTEditText(c);
					editText.setHeight(120) ;
					if(isStructuredData){
						JSONObject object = structure.getJSONObject(i-1) ;
						String value_previous_element = object.getJSONArray("VALUE").getString(0) ;
						editText.setHint(value_previous_element) ;
					}
					editText.setInputType(InputType.TYPE_TEXT_FLAG_CAP_SENTENCES)  ;
					editText.setSingleLine(false) ;
					editText.setHeight(screenHeight/10);
					editText.setGravity(Gravity.TOP);
					//int viewid = editText.generateViewId();
					int viewid = idGenerator.getUniqueId() ;
					editText.setId(viewid) ;
					String[] def = new String[]{String.valueOf(i), "text_area" ,String.valueOf(viewid) } ;
					list.add(def) ;
					TableLayout.LayoutParams params = new TableLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT, 1f) ;
					params.setMargins(screenWidth/100, screenHeight/100, screenWidth/100, screenHeight/100) ;
					editText.setLayoutParams(params)  ;
					editText.setBackgroundColor(Color.parseColor("#efeff4"))  ;
					layout.addView(editText) ;
				}

				if(type.equals("checkbox")){
					GridLayout gridLayout = new GridLayout(c) ;

					int max_row_count = 1;
					int maxlenth = 0;
					for (int j = 0; j < element.getJSONArray("VALUE").length()	; j++){
						int radio_value_length = element.getJSONArray("VALUE").getString(j).toString().length();
						if (radio_value_length>maxlenth){
							maxlenth = radio_value_length;
						}
					}
					if (maxlenth<=20){
						max_row_count = 3;
						gridLayout.setColumnCount(max_row_count) ;
					}
					else{
						if (maxlenth>20 && maxlenth<40){
							max_row_count = 2;
							gridLayout.setColumnCount(max_row_count) ;
						}
						else {
							max_row_count = 1;
							gridLayout.setColumnCount(max_row_count) ;
						}
					}


					//gridLayout.setColumnCount(3) ;
					TableLayout.LayoutParams params = new TableLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT, 1f) ;
					params.setMargins(screenWidth/20, screenHeight/100, screenWidth/20, screenHeight/100) ;
					gridLayout.setLayoutParams(params) ;
					//int groupid= gridLayout.generateViewId() ;
					int groupid = idGenerator.getUniqueId() ;
					gridLayout.setId(groupid) ;
					for (int j = 0; j < element.getJSONArray("VALUE").length()	; j++) {
						CheckBox checkBox = new CheckBox(c) ;
						checkBox.setPadding(checkBox.getPaddingLeft() + (int)(10.0f * 2 + 0.5f), checkBox.getPaddingLeft() + (int)(10.0f * 2 + 0.5f), 0, checkBox.getPaddingLeft() + (int)(10.0f * 2 + 0.5f));
						String checkbox_txt = element.getJSONArray("VALUE").getString(j);
						checkBox.setText(checkbox_txt) ;
						gridLayout.addView(checkBox) ;
						checkBox.setButtonDrawable(c.getResources().getDrawable(R.drawable.checkbox_states)) ;
						int textSize  =(int) c.getResources().getDimension(R.dimen.radio_chech_text_size) ;
						checkBox.setTextSize(textSize) ;
						checkBox.setWidth((screenWidth- 2*screenWidth/10)/max_row_count);
						//checkBox.setHeight(screenHeight/15);
						//int viewid = checkBox.generateViewId();
						int viewid = idGenerator.getUniqueId() ;
						checkBox.setId(viewid) ;

					}
					String[] def = new String[]{String.valueOf(i), "checkbox" ,String.valueOf(groupid) } ;
					list.add(def) ;
					layout.addView(gridLayout) ;

				}
				if(type.equals("radio")){
					GridLayout gridLayout = new GridLayout(c) ;

					int max_row_count = 1;
					int maxlenth = 0;
					for (int j = 0; j < element.getJSONArray("VALUE").length()	; j++){
						int radio_value_length = element.getJSONArray("VALUE").getString(j).toString().length();
						if (radio_value_length>maxlenth){
							maxlenth = radio_value_length;
						}
					}
					if (maxlenth<=20){
						max_row_count = 3;
						gridLayout.setColumnCount(max_row_count) ;
					}
					else{
						if (maxlenth>20 && maxlenth<40){
							max_row_count = 2;
							gridLayout.setColumnCount(max_row_count) ;
						}
						else {
							max_row_count = 1;
							gridLayout.setColumnCount(max_row_count) ;
						}
					}


					//gridLayout.setColumnCount(3) ;
					ToggleButtonGroupTableLayout group = new ToggleButtonGroupTableLayout(c) ;
					//group.setOrientation(RadioGroup.HORIZONTAL) ;
					TableLayout.LayoutParams params= new TableLayout.LayoutParams(RadioGroup.LayoutParams.MATCH_PARENT, RadioGroup.LayoutParams.WRAP_CONTENT);
					params.setMargins(screenWidth/20 , screenHeight/100, screenWidth/20, screenHeight/100) ;
					group.setLayoutParams(params) ;
					if(isStructuredData)
						group.setGravity(Gravity.CENTER) ;
					//int viewid = group.generateViewId();
					int viewid = idGenerator.getUniqueId() ;
					group.setId(viewid) ;
					String[] def = new String[]{String.valueOf(i), "radio" ,String.valueOf(viewid) } ;
					list.add(def) ;
					int radioCounter  = 0 ;
					int marginTop = 0 ;
					int marginLeft = 0 ;

					TableRow row = new TableRow(c) ;
					for (int j = 0; j < element.getJSONArray("VALUE").length()	; j++) {
						RadioButton checkBox = new RadioButton(c) ;
						checkBox.setPadding(checkBox.getPaddingLeft() + (int)(10.0f * 2 + 0.5f), checkBox.getPaddingLeft() + (int)(10.0f * 2 + 0.5f), 0, checkBox.getPaddingLeft() + (int)(10.0f * 2 + 0.5f));
						checkBox.setButtonDrawable(c.getResources().getDrawable(R.drawable.radios_states)) ;
						int textSize  =(int) c.getResources().getDimension(R.dimen.radio_chech_text_size) ;
						checkBox.setTextSize(textSize) ;
						int radioID = idGenerator.getUniqueId() ;
						checkBox.setId(radioID) ;
						checkBox.setWidth(((screenWidth- 2*screenWidth/10)/max_row_count));
						//checkBox.setHeight(screenHeight/15);
						String radio_txt = element.getJSONArray("VALUE").getString(j).toString();



						checkBox.setText(radio_txt) ;
						LayoutParams radioLayoutParam = new LayoutParams(layout.getWidth()/3, android.widget.RadioGroup.LayoutParams.WRAP_CONTENT) ;
						radioLayoutParam.setMargins(marginLeft, marginTop, 0, 0) ;
						//checkBox.setLayoutParams(radioLayoutParam) ;
						if(j==0){
							group.activeRadioButton=  checkBox  ;
							checkBox.setChecked(true) ;
						}

						row.addView(checkBox) ;
						radioCounter++ ;
						if(radioCounter>= max_row_count && radioCounter % max_row_count == 0){

							group.addView(row) ;
							row = new TableRow(c) ;

							marginTop+= checkBox.getHeight() ;
							marginLeft-= layout.getWidth() ;
						}

					}
					group.addView(row) ;

					if(isStructuredData){
						LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) group.getLayoutParams() ;
						layoutParams.setMargins(screenWidth/20, screenHeight/80, screenWidth/20, screenHeight/100) ;
						group.setLayoutParams(layoutParams) ;
					}
					layout.addView(group) ;

				}

			}

			layout.setBackgroundColor(Color.parseColor("#fafafa")) ;
			return list ;


		} catch (JSONException e) {
			// TODO: handle exception
			//Log.d("Buidding form json exception", "Building form json exception  "+e.getMessage()) ;
			return null ;

		}
	}

	public static  String loadJSONFromAsset(Context c , String fileName) {
		String json = null;
		try {

			InputStream is = c.getResources().getAssets().open(fileName)  ;

			int size = is.available();

			byte[] buffer = new byte[size];

			is.read(buffer);

			is.close();

			// json = new String(buffer);
			json = new String(buffer, "UTF-8");
			//Log.d("load form json", "load from json "+json) ;
			//json = new String(buffer) ;
		} catch (IOException ex) {
			//Log.d("load form json", "load from json "+ex.getMessage()) ;
			ex.printStackTrace();
			return null;
		}
		catch (OutOfMemoryError e) {
			// TODO: handle exception

			//Log.d("out of memorie exception ", "out of memoiry exception "+e.getMessage()) ;
			json = "" ;
		}

		return json;

	}

}
