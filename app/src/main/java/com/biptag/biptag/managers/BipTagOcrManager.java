package com.biptag.biptag.managers;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.util.Log;

import com.biptag.biptag.NavigationActivity;
import com.biptag.biptag.R;
import com.biptag.biptag.ocr.CaptureActivity;
import com.biptag.biptag.ocr.OcrInitAsyncTask;
import com.biptag.biptag.ocr.PreferencesActivity;
import com.googlecode.tesseract.android.TessBaseAPI;

public class BipTagOcrManager {
	 
	
	public static void processImage(String path , Bitmap bitmap , Context context){
		
		try {
			ExifInterface exifInterface;
			exifInterface = new ExifInterface(path);
			int exifOrientation = exifInterface.getAttributeInt(ExifInterface.TAG_ORIENTATION	, ExifInterface.ORIENTATION_NORMAL) ; 
			int rotate = 0;

			switch (exifOrientation) {
			case ExifInterface.ORIENTATION_ROTATE_90:
			    rotate = 90;
			    break;
			case ExifInterface.ORIENTATION_ROTATE_180:
			    rotate = 180;
			    break;
			case ExifInterface.ORIENTATION_ROTATE_270:
			    rotate = 270;
			    break;
			}
			
			if (rotate != 0) {
			    int w = bitmap.getWidth();
			    int h = bitmap.getHeight();

			    // Setting pre rotate
			    Matrix mtx = new Matrix();
			    mtx.preRotate(rotate);

			    // Rotating Bitmap & convert to ARGB_8888, required by tess
			    bitmap = Bitmap.createBitmap(bitmap, 0, 0, w, h, mtx, false);
			}
			bitmap = bitmap.copy(Bitmap.Config.ARGB_8888, true);
			
			
			TessBaseAPI baseAPI = new TessBaseAPI() ; 
			File directory = context.getCacheDir() ; 
			if(!directory.exists()){
				directory.mkdir() ;	
			}
			String trainedFilePath= directory.getAbsolutePath()+"/tessdata/eng.traineddata" ;
			AssetManager am = context.getAssets();
			InputStream inputStream = am.open("tesseract-ocr-3.02.eng/tesseract-ocr/tessdata/eng.traineddata");
			Log.d("api ", "tessocr from biptagocrmanager");
			createFileFromInputStream(inputStream, trainedFilePath, context) ; 
			
			//File myDir = context.getExternalFilesDir(Environment.MEDIA_MOUNTED);
			String dataPath = directory.getAbsolutePath() ; 
			String lang = "eng" ; 
			baseAPI.init(dataPath, lang) ;
			final BitmapFactory.Options options = new BitmapFactory.Options();
			options.inJustDecodeBounds = true;
			bitmap = BitmapFactory.decodeFile(path,options) ;

			// Calculate inSampleSize based on a preset ratio
			options.inSampleSize = calculateInSampleSize(options, 480, 800);

			// Decode bitmap with inSampleSize set
			options.inJustDecodeBounds = false;
			bitmap = BitmapFactory.decodeFile(path, options);
			bitmap = Bitmap.createScaledBitmap(bitmap, bitmap.getWidth() / 5, bitmap.getHeight() / 5, true);
			baseAPI.setImage(bitmap) ;
			String text = baseAPI.getUTF8Text() ; 
			//Log.d("ocr text", "ocr text "+text) ;
			AlertDialog.Builder builder = new AlertDialog.Builder(context) ; 
			builder.setTitle("OCR RESULT") ; 
			builder.setMessage(text) ; 
			builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
				
				@Override
				public void onClick(DialogInterface dialog, int which) {
					// TODO Auto-generated method stub
					dialog.dismiss() ; 
				}
			}) ; 
			builder.show() ; 
			
			baseAPI.end() ; 
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			//Log.d("ocr text", "ocr text process exception "+e.getMessage()) ;
		} 
		

	}

	public static int calculateInSampleSize(BitmapFactory.Options options,int reqWidth, int reqHeight) {

		final int height = options.outHeight;
		final int width = options.outWidth;
		int inSampleSize = 1; // default to not zoom image

		if (height > reqHeight || width > reqWidth) {
			final int heightRatio = Math.round((float) height/ (float) reqHeight);
			final int widthRatio = Math.round((float) width / (float) reqWidth);
			inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
		}
		return inSampleSize;
	}

	private static File createFileFromInputStream(InputStream inputStream , String fileName , Context context) {
			
		
		File tessDataDirectory = new File(context.getCacheDir().getAbsolutePath()+"/tessdata");
			if(!tessDataDirectory.exists()){
				tessDataDirectory.mkdir() ; 
			}
			
	
		
		File f = new File(fileName);
			
			
			
			
			 if(!f.exists()){
					try {
						f.createNewFile() ;
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} 
				}
				   try{
					  
					      OutputStream outputStream = new FileOutputStream(f);
					      byte buffer[] = new byte[1024];
					      int length = 0;

					      while((length=inputStream.read(buffer)) > 0) {
					        outputStream.write(buffer,0,length);
					      }

					      outputStream.close();
					      inputStream.close();
 
					      
					   }catch (IOException e) {
					         //Logging exception
						   //Log.d("ocr text", "ocr text trained data exception "+e.getMessage()) ;
					   }
			

		   return f;
		}
	



}
