package com.biptag.biptag.webservices.logs;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.biptag.biptag.Constant;
import com.biptag.biptag.JSONParser;
import com.biptag.biptag.NavigationActivity;
import com.biptag.biptag.R;
import com.biptag.biptag.dbmanager.datasource.BTLogDataSource;
import com.biptag.biptag.dbmanager.objects.BTLog;
import com.biptag.biptag.interfaces.AsyncResponse;
import com.biptag.biptag.managers.BiptagUserManger;

public class DownloadLogs extends AsyncTask<String, String, String> {

	JSONParser parser ; 
	JSONObject jsonObject ; 
	Context context ; 
	AsyncResponse response ; 
	
	public DownloadLogs(Context context , AsyncResponse response) {
		// TODO Auto-generated constructor stub
		this.context = NavigationActivity.getContext() ; 
		this.response = response ; 
	}
	
	
	
	
	@Override
	protected String doInBackground(String... params) {
		// TODO Auto-generated method stub
		String url= Constant.base_url +Constant.select_logs;
		String ba_id = String.valueOf(BiptagUserManger.getUserValue("BA_ID", "int", context.getApplicationContext())) ; 
		String agent_id= String.valueOf(BiptagUserManger.getUserValue("AGENT_ID", "int", context.getApplicationContext())) ; 
		
		List<NameValuePair> list= new ArrayList<NameValuePair>() ; 
		list.add(new BasicNameValuePair("ba_id",ba_id ));
		list.add(new BasicNameValuePair("agent_id", agent_id)) ; 
				
		
		
		try {

			parser = new JSONParser() ; 
			jsonObject = parser.makeHttpRequest(url, "GET", list) ; 
			if(jsonObject != null){
				try {
					if(jsonObject.getInt("success") == 1){
					JSONArray logs = jsonObject.getJSONArray("logs") ; 
					BTLogDataSource dataSource = new BTLogDataSource(context); 
					
					for (int i = 0; i < logs.length(); i++) {
						BTLog wsLog = new BTLog() ; 
						JSONObject jsonLog = logs.getJSONObject(i) ; 
						
						wsLog.setLog_id(jsonLog.getInt("LOG_ID")) ; 
						wsLog.setLog_type(jsonLog.getString("LOG_TYPE")) ;
						wsLog.setLog_timestamps(jsonLog.getString("LOG_TIMESTAMP")) ; 
						wsLog.setLog_rel_object(jsonLog.getString("LOG_REL_OBJECT")) ; 
						wsLog.setLog_rel_object_id(jsonLog.getInt("LOG_REL_OBJECT_ID")) ; 
						wsLog.setLog_rel_object_2(jsonLog.getString("LOG_REL_OBJECT_2")) ; 
						wsLog.setLog_rel_object_2_id(jsonLog.getInt("LOG_REL_OBJECT_2_ID")) ;
						wsLog.setFl_subject(jsonLog.getString("FL_SUBJECT")) ; 
						wsLog.setFl_content(jsonLog.getString("FL_CONTENT")) ; 
						wsLog.setFl_id(jsonLog.getInt("LOG_FL_ID")) ; 
						wsLog.setFl_contact_id(jsonLog.getInt("FL_CONTACT_ID")) ; 
						wsLog.setFl_send_date(jsonLog.getString("FL_SEND_DATE")) ; 
						wsLog.setFl_agent_id(jsonLog.getInt("FL_AGENT_ID")) ; 
						BTLog localLog = dataSource.findLogWithId(Integer.parseInt(agent_id),  wsLog.getLog_id() )	;
						 
						if(localLog == null){
							boolean insert =dataSource.insertLog(wsLog) ; 
						}else {

							boolean update =dataSource.updateLog(wsLog) ; 
						}
						
						
						
					
					}  
					
					
					
					}
				} catch (JSONException e) {
					// TODO: handle exception
					//Log.d("out of memeory exception ", "download log out of memory exception ")   ; 
				}
			}
			
			

		} catch (OutOfMemoryError e) {
			// TODO: handle exception
		}
		
		
		
	
		
		
		
		return null;
	}
	
	@Override
	protected void onPostExecute(String result) {
		// TODO Auto-generated method stub
		super.onPostExecute(result);
		response.processFinish("finish") ; 
	
	}

}
