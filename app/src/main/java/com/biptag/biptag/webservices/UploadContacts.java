package com.biptag.biptag.webservices;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.biptag.biptag.Constant;
import com.biptag.biptag.JSONParser;
import com.biptag.biptag.NavigationActivity;
import com.biptag.biptag.R;
import com.biptag.biptag.dbmanager.datasource.BTContactDataSource;
import com.biptag.biptag.dbmanager.datasource.BTFollowupTemplateDataSource;
import com.biptag.biptag.dbmanager.objects.BTContact;
import com.biptag.biptag.dbmanager.objects.BTFollowupTemplate;
import com.biptag.biptag.interfaces.AsyncResponse;
import com.biptag.biptag.managers.BiptagUserManger;

public class UploadContacts extends AsyncTask<String, String, String>{
	JSONParser parser ;
	JSONObject jsonObject ;
	Context context ;

	public AsyncResponse response = null ;

	public UploadContacts(Context context ) {
		// TODO Auto-generated constructor stub
		this.context= NavigationActivity.getContext() ;
	}

	@Override
	protected String doInBackground(String... params) {
		// TODO Auto-generated method stub
		Object agent_id_object = BiptagUserManger.getUserValue("AGENT_ID", "int", context);
		int agent_id = Integer.parseInt(String.valueOf(agent_id_object)) ;

		BTContactDataSource contactDataSource = new BTContactDataSource(context) ;
		List<BTContact > allUnSynchronizedContact= contactDataSource.getAllContacts(agent_id, 0) ;
		int ba_id = Integer.parseInt(String.valueOf(BiptagUserManger.getUserValue("BA_ID", "int", context))) ;
		int agent_id1 = Integer.parseInt(String.valueOf(BiptagUserManger.getUserValue("AGENT_ID", "int", context))) ;
		int user_id= Integer.parseInt(String.valueOf(BiptagUserManger.getUserValue("USER_ID", "int", context))) ;
		if(allUnSynchronizedContact.size() != 0){
			for (int i = 0; i < allUnSynchronizedContact.size(); i++) {
				BTContact contact = allUnSynchronizedContact.get(i) ;

				if(contact.getContactId() == 0){
					// insert 

					String product_config = String.valueOf(BiptagUserManger.getUserValue("PRODUCT_CONFIG", "string", context)) ;
					JSONObject product_config_object;
					try {
						product_config_object = new JSONObject(product_config);
						//Log.d("fisih contact ", "finish contact product config "+product_config_object.toString()) ;
						String follow_up = product_config_object.getString("follow_up_mail") ;
						String contact_actif =product_config_object.getString("contact_actif") ;

						JSONObject contact_response = contact.getContactFrResponse() ;
						List<NameValuePair> list = new ArrayList<NameValuePair>() ;
						list.add(new BasicNameValuePair("ba_id", String.valueOf(ba_id))) ;
						list.add(new BasicNameValuePair("email", contact.getContactEmail())) ;

						list.add(new BasicNameValuePair("agent_id", String.valueOf(agent_id1))) ;
						list.add(new BasicNameValuePair("form_id", String.valueOf(contact.getContactFormId()))) ;
						list.add(new BasicNameValuePair("user_id", String.valueOf(user_id)));
						list.add(new BasicNameValuePair("contact_actif", contact_actif)) ;
						list.add(new BasicNameValuePair("follow_up", follow_up)) ;
						list.add(new BasicNameValuePair("do_follow_up", (contact.isDoFollowup() ? "1" : "0"))) ;
						int templateSelected = 0 ;
						if(String.valueOf(BiptagUserManger.getUserValue("selected_template", "string", context)).length() != 0)
							templateSelected = Integer.parseInt(String.valueOf(BiptagUserManger.getUserValue("selected_template", "string", context))) ;
						String savedCCEmails = contact.getCcEmailRegister().toString() ;
						String savedCCIEmails =contact.getCciEmailRegister().toString() ;


						if(templateSelected!=0){
							BTFollowupTemplateDataSource dataSource = new BTFollowupTemplateDataSource(context) ;
							BTFollowupTemplate followupTemplate =dataSource.getTemplateWithLocalID(ba_id, templateSelected) ;
								Log.d("contact ", " followup template " + followupTemplate + " "+ templateSelected);
								if (followupTemplate!=null){
								Log.d(" followup template ", " not null");
								}else{
									Log.d(" followup template ", " null");
					}
							String savedContent= followupTemplate.getFu_content() ;
							String savedSubject = followupTemplate.getFu_subject() ;
							list.add(new BasicNameValuePair("text", savedContent));
							list.add(new BasicNameValuePair("subject", savedSubject)) ;
						}
						list.add(new BasicNameValuePair("cc", savedCCEmails )) ;
						list.add(new BasicNameValuePair("cci",savedCCIEmails )) ;
						if(contact_response.has("FILES"))
							list.add(new BasicNameValuePair("files", contact_response.getString("FILES"))) ;
						else
							list.add(new BasicNameValuePair("files", "")) ;

						if(contact_response.has("LINKS"))
							list.add(new BasicNameValuePair("links", contact_response.getString("LINKS"))) ;
						else
							list.add(new BasicNameValuePair("links", "")) ;

						if(contact_response.has("VCARDS"))
							list.add(new BasicNameValuePair("cards", contact_response.getString("VCARDS")));
						else
							list.add(new BasicNameValuePair("cards",""));

						list.add(new BasicNameValuePair("user_email", String.valueOf(BiptagUserManger.getUserValue("EMAIL", "string", context))));
						List<NameValuePair> postList = new ArrayList<NameValuePair>() ;
						String contact_response_encoded="";
						try {
							contact_response_encoded = URLEncoder.encode(contact_response.toString(), "UTF-8");
						} catch (UnsupportedEncodingException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						postList.add(new BasicNameValuePair("json_response", contact_response_encoded )) ;
						String url= Constant.base_url + Constant.insert_contact_ws;



						if(BiptagUserManger.isOnline(context)){
							contact.setSynchronized(2) ;
							contactDataSource.updateContact(contact) ;
							JSONParser parser = new JSONParser() ;
							jsonObject = parser.makeHybridHttpRequest(url, list, postList) ; if(jsonObject != null){
								if(jsonObject.getInt("success") == 1){

									contact.setContactId(jsonObject.getInt("CONTACT_ID")) ;
									contact.setContactFrID(jsonObject.getInt("FR_ID")) ;
									contact.setContactFrHashcode(jsonObject.getString("HASH_CODE")) ;
									contact.setSynchronized(1) ;
									contactDataSource.updateContact(contact) ;
								}
							}
						}
					} catch (JSONException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
						Log.d("upload contact ", "upload contact  excpetion insert " + e1.getMessage()) ;
						return null ;
					}


				}else{
					// update 
					try {
						String product_config = String.valueOf(BiptagUserManger.getUserValue("PRODUCT_CONFIG", "string", context)) ;
						JSONObject product_config_object = new JSONObject(product_config) ;
						//Log.d("fisih contact ", "finish contact product config "+product_config_object.toString()) ; 
						String follow_up = product_config_object.getString("follow_up_mail") ;
						String contact_actif =product_config_object.getString("contact_actif") ;

						JSONObject contact_response = contact.getContactFrResponse();
						List<NameValuePair> list = new ArrayList<NameValuePair>() ;
						list.add(new BasicNameValuePair("ba_id", String.valueOf(ba_id))) ;
						list.add(new BasicNameValuePair("email", contact.getContactEmail())) ;
						list.add(new BasicNameValuePair("contact_id", String.valueOf(contact.getContactId()))) ;
						list.add(new BasicNameValuePair("agent_id", String.valueOf(agent_id))) ;
						list.add(new BasicNameValuePair("fr_id", String.valueOf(contact.getContactFrID())));
						list.add(new BasicNameValuePair("form_id", String.valueOf(contact.getContactFormId()))) ;
						list.add(new BasicNameValuePair("user_id", String.valueOf(user_id)));
						list.add(new BasicNameValuePair("contact_actif", contact_actif)) ;
						list.add(new BasicNameValuePair("follow_up", follow_up)) ;
						list.add(new BasicNameValuePair("do_follow_up", (contact.isDoFollowup() ? "1" : "0"))) ;
						if(contact_response.has("FILES"))
							list.add(new BasicNameValuePair("files", contact_response.getString("FILES"))) ;
						else
							list.add(new BasicNameValuePair("files", "")) ;

						if(contact_response.has("LINKS"))
							list.add(new BasicNameValuePair("links", contact_response.getString("LINKS"))) ;
						else
							list.add(new BasicNameValuePair("links", "")) ;

						if(contact_response.has("VCARDS"))
							list.add(new BasicNameValuePair("cards", contact_response.getString("VCARDS")));
						else
							list.add(new BasicNameValuePair("cards",""));
						int templateSelected =0  ;
						if(String.valueOf(BiptagUserManger.getUserValue("selected_template", "string", context)).length()!=0)
							templateSelected = Integer.parseInt(String.valueOf(BiptagUserManger.getUserValue("selected_template", "string", context))) ;
						String savedCCEmails = contact.getCcEmailRegister().toString() ;
						String savedCCIEmails =contact.getCciEmailRegister().toString() ;

						Log.d(" template ", templateSelected + " ");
						if(templateSelected!=0){
							BTFollowupTemplateDataSource dataSource = new BTFollowupTemplateDataSource(context) ;
							BTFollowupTemplate followupTemplate =dataSource.getTemplateWithLocalID(ba_id, templateSelected) ;
							String savedContent= followupTemplate.getFu_content() ;
							String savedSubject = followupTemplate.getFu_subject() ;
							list.add(new BasicNameValuePair("text", savedContent));
							list.add(new BasicNameValuePair("subject", savedSubject)) ;
						}else{
							list.add(new BasicNameValuePair("text", ""));
							list.add(new BasicNameValuePair("subject", "")) ;

						}
						list.add(new BasicNameValuePair("cc", savedCCEmails )) ;
						list.add(new BasicNameValuePair("cci",savedCCIEmails )) ;
						list.add(new BasicNameValuePair("user_email", String.valueOf(BiptagUserManger.getUserValue("EMAIL", "string", context))));
						List<NameValuePair> postList = new ArrayList<NameValuePair>() ;
						String contact_response_encoded="";
						try {
							contact_response_encoded = URLEncoder.encode(contact_response.toString(), "UTF-8");
						} catch (UnsupportedEncodingException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						postList.add(new BasicNameValuePair("json_response", contact_response_encoded )) ;
						String url= Constant.base_url +Constant.update_contact_ws;

						if(BiptagUserManger.isOnline(context)){
							contact.setSynchronized(2) ;
							contactDataSource.updateContact(contact) ;
							JSONParser parser = new JSONParser() ;
							jsonObject = parser.makeHybridHttpRequest(url, list, postList) ;

							if(jsonObject != null){


								//Log.d("upload contact ", "uploac contact response "+jsonObject.toString()) ; 
								if(jsonObject.getInt("success") == 1){


									contact.setContactId(jsonObject.getInt("CONTACT_ID")) ;
									contact.setContactFrID(jsonObject.getInt("FR_ID")) ;
									contact.setContactFrHashcode(jsonObject.getString("HASH_CODE")) ;
									contact.setSynchronized(1) ;
									boolean upadted =contactDataSource.updateContact(contact) ;
								}else{
									contact.setSynchronized(0) ;
									contactDataSource.updateContact(contact) ;
								}
							}else{
								contact.setSynchronized(0) ;
								contactDataSource.updateContact(contact) ;
							}
						}

					} catch (JSONException e) {
						// TODO: handle exception
						Log.d("uploac contact ", "uploac contact  excpetion update "+e.getMessage()) ;
						return null  ;
					}

				}
				//Log.d("uploac contact ", "uploac contact result "+jsonObject.toString()) ; 

			}

		}else{
			jsonObject = new JSONObject() ;
			try {
				jsonObject.put("success", "1") ;
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		return null;
	}

	@Override
	protected void onPostExecute(String result) {
		// TODO Auto-generated method stub
		super.onPostExecute(result);
		try {
			if(jsonObject!= null){
				if(jsonObject.getInt("success")==1){

					if(response != null)
						response.processFinish("uploaded")  ;
				}else{
					if(response != null)
						response.processFailed() ;
				}
			}else {
				if(response != null)
					response.processFailed() ;		}
		} catch (JSONException e) {
			// TODO: handle exception
			//Log.d("uploac contact ", "uploac contact  excpetion "+e.getMessage()) ; 
			if(response != null)
				response.processFailed() ;
		}
	}

}
