package com.biptag.biptag.webservices;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import org.json.JSONException;
import org.json.JSONObject;

import com.biptag.biptag.Constant;

import com.biptag.biptag.interfaces.AsyncResponse;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.os.AsyncTask;
import android.util.Log;
	
public class HttpFileUpload extends AsyncTask<String, String, String> implements Runnable{
        URL connectURL;
        String responseString;
        String Title;
        byte[ ] dataToServer;
        String path ; 
        String url ;
        FileInputStream fileInputStream = null;
        public FileInputStream fStream = null ; 
        public AsyncResponse response = null ; 
        int imgId ; 
        JSONObject responseObject = null  ; 
        
        
        
        public HttpFileUpload(String urlString, String vTitle , String path , int imgId ){
                try{
                        this.connectURL = new URL(urlString);
                        this.Title= vTitle;
                        this.path = path ; 
                        this.url = urlString ;
                        this.imgId = imgId ; 
                }catch(Exception ex){
                   Log.d("HttpFileUpload","sending image URL Malformatted");
                }
        }
	 
        public void Send_Now(FileInputStream fStream){
                fileInputStream = fStream;
                Sending();
        }
	
        void Sending(){
                String iFileName = Title+".jpg";
                String lineEnd = "\r\n";
                String twoHyphens = "--";
                String boundary = "*****";
                String Tag="fSnd";
                try
                {
                       Log.d(Tag," sending image Starting Http File Sending to URL");
	
                        // Open a HTTP connection to the URL
                        HttpURLConnection conn = (HttpURLConnection)connectURL.openConnection();
	
                        // Allow Inputs
                        conn.setDoInput(true);
	
                        // Allow Outputs
                        conn.setDoOutput(true);
	
                        // Don't use a cached copy.
                        conn.setUseCaches(false);
	
                        // Use a post method.
                        conn.setRequestMethod("POST");
	
                        conn.setRequestProperty("Connection", "Keep-Alive");
	
                        conn.setRequestProperty("Content-Type", "multipart/form-data;boundary="+boundary);
	
                        DataOutputStream dos = new DataOutputStream(conn.getOutputStream());
                        
                        dos.writeBytes(twoHyphens + boundary + lineEnd);
                        dos.writeBytes("Content-Disposition: form-data; name=\"attachment\";filename=\"" + iFileName +"\"  "+ lineEnd);
                        dos.writeBytes(lineEnd);
                        dos.writeBytes(iFileName); 
                        dos.writeBytes(lineEnd);
                        dos.writeBytes(twoHyphens + boundary + lineEnd);
	                        
	                        
                        dos.writeBytes("Content-Disposition: form-data; name=\"attachment\";filename=\"" + iFileName +"\"" + lineEnd);
                        dos.writeBytes(lineEnd);
	
                        Log.d(Tag,"sending image Headers are written");
	
                        // create a buffer of maximum size
                        int bytesAvailable = fileInputStream.available();
	                        
                        int maxBufferSize = 1024; 
                        int bufferSize = Math.min(bytesAvailable, maxBufferSize);
                        byte[ ] buffer = new byte[bufferSize];
	
                        // read file and write it into form...
                        int bytesRead = fileInputStream.read(buffer, 0, bufferSize);
	
                        while (bytesRead > 0)
                        {
                                dos.write(buffer, 0, bufferSize);
                                bytesAvailable = fileInputStream.available();
                                bufferSize = Math.min(bytesAvailable,maxBufferSize);
                                bytesRead = fileInputStream.read(buffer, 0,bufferSize);
                        }
                        dos.writeBytes(lineEnd);
                        dos.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);
	
                        // close streams
                        fileInputStream.close();
	                        
                        dos.flush();
	                        
                      Log.d(Tag,"sending image File Sent, Response: "+conn.getResponseMessage());
	                         
                        InputStream is = conn.getInputStream();
	                        
                        // retrieve the response from server
                        int ch;
	
                        StringBuffer b =new StringBuffer();
                        while( ( ch = is.read() ) != -1 ){ b.append( (char)ch ); }
                        String s=b.toString();

                        
                        String sb = s ;
                        String TAG = "sendin image" ; 
                        if (sb.length() > 4000) {
                            Log.d(TAG, "sendin sb.length = " + sb.length());
                            int chunkCount = sb.length() / 4000;     // integer division
                            for (int i = 0; i <= chunkCount; i++) {
                                int max = 4000 * (i + 1);
                                if (max >= sb.length()) {
                                  Log.d(TAG, "sendin chunk " + i + " of " + chunkCount + ":" + sb.substring(4000 * i));
                                } else {
                                   Log.d(TAG, "sendin chunk " + i + " of " + chunkCount + ":" + sb.substring(4000 * i, max));
                                }
                            }
                        } else {
                           Log.d(TAG, "sendin "+sb.toString());
                        }
                        
                        
                        dos.close();
                }
                catch (MalformedURLException ex)
                {
                       Log.d(Tag, "sending image URL error: " + ex.getMessage(), ex);
                }
	
                catch (IOException ioe)
                {
                       Log.d(Tag, "sending image IO error: " + ioe.getMessage(), ioe);
                }
        }
	
        @Override
        public void run() {
                // TODO Auto-generated method stub
        }
        
        
        
        private  void httpPostFileUpload(String fpath){
            HttpURLConnection connection = null;
            DataOutputStream outputStream = null;
            DataInputStream inputStream = null;

            String pathToOurFile = fpath;
           Log.v("file path",fpath.toString());
            String urlServer = this.url;
            //String urlServer = "http://65.182.110.63/public/handle_upload.php";
            String lineEnd = "\r\n";
            String twoHyphens = "--";
            String boundary =  "*****";
            String fname = "";

            int bytesRead, bytesAvailable, bufferSize;
            byte[] buffer;
            int maxBufferSize = 1*1024*1024;

            try
            {
                File f = new File(pathToOurFile);
                fname =  f.getName();

                FileInputStream fileInputStream = new FileInputStream(f);

                URL url = new URL(urlServer);
                connection = (HttpURLConnection) url.openConnection();

                // Allow Inputs & Outputs
                connection.setDoInput(true);
                connection.setDoOutput(true);
                connection.setUseCaches(false);

                // Enable POST method
                connection.setRequestMethod("POST"); 

                connection.setRequestProperty("Connection", "Keep-Alive");
                connection.setRequestProperty("Content-Type", "multipart/form-data;boundary="+boundary);

                outputStream = new DataOutputStream( connection.getOutputStream() );
                outputStream.writeBytes(twoHyphens + boundary + lineEnd);
                outputStream.writeBytes("Content-Disposition: form-data; name=\"attachment[]\";filename=\"" + pathToOurFile +"\"" + lineEnd);
                outputStream.writeBytes("Content-Type: image/jpg" + lineEnd);
                outputStream.writeBytes(lineEnd); 

                Log.v("ouput stream",outputStream.toString());

                bytesAvailable = fileInputStream.available();
                bufferSize = Math.min(bytesAvailable, maxBufferSize);

     try {
         buffer = new byte[bufferSize];
         bytesRead = fileInputStream.read(buffer, 0, bufferSize);

         while (bytesRead > 0)
         {
             outputStream.write(buffer, 0, bufferSize);
             bytesAvailable = fileInputStream.available();
             bufferSize = Math.min(bytesAvailable, maxBufferSize);
             bytesRead = fileInputStream.read(buffer, 0, bufferSize);
         }

         outputStream.writeBytes(lineEnd);
         outputStream.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);

     }catch (OutOfMemoryError e){
         e.printStackTrace();
     }


                // Read file

                // Responses from the server (code and message)
                int serverResponseCode = connection.getResponseCode();
                String serverResponseMessage = connection.getResponseMessage();

                Log.d("amanda", "upload async finished, server response : " + serverResponseMessage);

                InputStream is = connection.getInputStream();
                int ch;

                StringBuffer b =new StringBuffer();
                while( ( ch = is.read() ) != -1 ){ b.append( (char)ch ); }
                String s=b.toString();


                String sb = s ;
                String TAG = "sendin image" ;
                if (sb.length() > 4000) {
                    Log.d(TAG, "sendin sb.length = " + sb.length());
                    int chunkCount = sb.length() / 4000;     // integer division
                    for (int i = 0; i <= chunkCount; i++) {
                        int max = 4000 * (i + 1);
                        if (max >= sb.length()) {
                           Log.d(TAG, "sendin chunk " + i + " of " + chunkCount + ":" + sb.substring(4000 * i));
                        } else {
                            Log.d(TAG, "sendin chunk " + i + " of " + chunkCount + ":" + sb.substring(4000 * i, max));
                        }
                    }
                } else {
                   Log.d(TAG, "sendin "+sb.toString());
                }

                try {
					responseObject = new JSONObject(s) ;
				} catch (JSONException e) {
					// TODO: handle exception
					Log.d("sendin image ", "sendin image json exception  "+e.getMessage()) ;
				}


                fileInputStream.close();
                fileInputStream = null;
                outputStream.flush();
                outputStream.close();
                outputStream = null;
                f= null;



                //parent.invokeJs("pictureUploadEnded('" + fname + "')");
                //this.delete(fpath);

            }
                catch (Exception ex)
            {
                    Log.d("amanda", "exception uploading image : " + ex.getMessage());
            }     
    }

		@Override
		protected String doInBackground(String... params) {
			// TODO Auto-generated method stub
			
			 fileInputStream = fStream;
             //Sending();
             
             httpPostFileUpload(path) ; 
			return null;
		}
		
		@Override
		protected void onPostExecute(String result) {
		// TODO Auto-generated method stub
		super.onPostExecute(result);
		try {
			
			JSONObject sent = new JSONObject() ; 
			sent.put("img_id", this.imgId) ; 
			if(responseObject!= null){
					if(responseObject.getInt("stored") == 1){
						sent.put("sent", 1) ; 
					}else{
						sent.put("sent", 0) ; 
						
						
					}
			}else{
				sent.put("sent", 0) ; 
				
				
			}
		
		
			response.processFinish(sent.toString())  ;
			
		} catch (JSONException e) {
			// TODO: handle exception
			Log.d("sendin image", "sendin image json excpetion on post execute "+e.getMessage()) ;
			
			response.processFinish("img_sent_no")  ;
			
		
		
		}
	
		
		}
}