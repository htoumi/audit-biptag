package com.biptag.biptag.webservices;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import android.content.Context;
import android.os.AsyncTask;

import com.biptag.biptag.Constant;
import com.biptag.biptag.JSONParser;
import com.biptag.biptag.R;
import com.biptag.biptag.interfaces.AsyncResponse;

public class BTSignUpWS  extends AsyncTask<String, String, String>{
	String fname ; 
	String lname ; 
	String email ; 
	String company ; 
	String password ; 
	Context c ;  
	JSONObject signupResult ; 
	public AsyncResponse response = null ; 
	public BTSignUpWS(	String fname ,	String lname , 	String email ,	String company ,String password  , Context c) {
		// TODO Auto-generated constructor stub
		this.fname  = fname ;  
		this.lname = lname ; 
		this.email = email ; 
		this.company = company ; 
		this.password = password ; 
		this.c =c ; 
	}
	public String md5(String s) {
	    String result = s;
	    if(s != null) { 
	        MessageDigest md = null;
			try {
				md = MessageDigest.getInstance("MD5");
			} catch (NoSuchAlgorithmException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} //or "SHA-1"
	        md.update(s.getBytes());
	        BigInteger hash = new BigInteger(1, md.digest());
	        result = hash.toString(16);
	        while(result.length() < 32) { //40 for SHA-1
	            result = "0" + result;
	        } 
	    }
	    return result;
	}
	
	@Override
	protected void onPreExecute() {
		// TODO Auto-generated method stub
		super.onPreExecute();
	
	}
	
	
	@Override
	protected String doInBackground(String... params) {
		// TODO Auto-generated method stub
		
		 
		String url = Constant.base_url+Constant.signup ;
		List<NameValuePair> list = new ArrayList<NameValuePair>() ; 
		list.add(new BasicNameValuePair("user_email",this.email )) ;
		list.add(new BasicNameValuePair("user_password", md5(this.password))) ; 
		list.add(new BasicNameValuePair("user_company", this.company)) ; 
		list.add(new BasicNameValuePair("user_firstname", this.fname)) ; 
		list.add(new BasicNameValuePair("user_lastname" , this.lname)) ; 
		
		 
		JSONParser parser = new JSONParser() ; 
		signupResult = parser.makeHttpRequest(url, "GET", list) ; 
		
		
		return null;
	}
	
	@Override
	protected void onPostExecute(String result) {
		// TODO Auto-generated method stub
		super.onPostExecute(result);
		
		if(signupResult != null){
					
					response.processFinish(signupResult.toString()) ; 
				
			
		}else{
			// erreur
		}
	
	
	
	}
	
}
