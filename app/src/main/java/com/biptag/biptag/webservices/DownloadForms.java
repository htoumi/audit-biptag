package com.biptag.biptag.webservices;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.biptag.biptag.Constant;
import com.biptag.biptag.JSONParser;
import com.biptag.biptag.NavigationActivity;
import com.biptag.biptag.R;
import com.biptag.biptag.dbmanager.datasource.BTFormDataSource;
import com.biptag.biptag.dbmanager.objects.BTFormulaire;
import com.biptag.biptag.interfaces.AsyncResponse;
import com.biptag.biptag.managers.BiptagUserManger;

public class DownloadForms extends AsyncTask<String, String, String> {
JSONParser parser ; 
JSONObject jsonObject ; 
Context context ; 
public AsyncResponse response = null ; 
public DownloadForms(Context c) {
	// TODO Auto-generated constructor stub
	this.context= NavigationActivity.getContext() ; 
}


	@Override
	protected String doInBackground(String... params) {
		// TODO Auto-generated method stub

		//try {
		String url= Constant.base_url +Constant.get_forms_ws;
		List<NameValuePair> list= new ArrayList<NameValuePair>() ; 
		String ba_id = String.valueOf(BiptagUserManger.getUserValue("BA_ID", "int", context)) ; 
		list.add(new BasicNameValuePair("ba_id",ba_id ));
		parser= new JSONParser() ; 
		jsonObject = parser.makeHttpRequest(url, "GET", list) ;

		/*
		if(jsonObject != null)
		BiptagUserManger.saveUserData("forms", jsonObject.getString("forms"), context)	 ;

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			//Log.d("DownloadForms ", "DownloadForms exception "+e.getMessage()) ;

		} 	*/
				return null ; 
	}
	
	@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);

			if(jsonObject != null){ 
				try {
					String agent_id_string = String.valueOf(BiptagUserManger.getUserValue("AGENT_ID", "int", context)) ;
					int agent_id =  Integer.parseInt(agent_id_string) ; 
					if(jsonObject.getInt("success")== 1){
						
						BTFormDataSource dataSource = new BTFormDataSource(context) ; 
						JSONArray forms  = jsonObject.getJSONArray("forms") ; 
						for (int i = 0; i < forms.length(); i++) {
							JSONObject jsonForm =  forms.getJSONObject(i) ;
							BTFormulaire localForm = dataSource.getFormWithID(agent_id, jsonForm.getInt("FORM_ID")) ;
							
							BTFormulaire formulaire = new BTFormulaire() ;
							formulaire.setAgentId(agent_id) ;
							formulaire.setEventEdition(jsonForm.getString("EV_EDITION")) ;
							formulaire.setEventEndDate(jsonForm.getString("EV_END_DATE")) ; 
							formulaire.setEventStartDate(jsonForm.getString("EV_START_DATE")) ; 
							formulaire.setEventName(jsonForm.getString("EV_NAME")) ; 
							formulaire.setFormId(jsonForm.getInt("FORM_ID")) ; 
							formulaire.setFormName(jsonForm.getString("FORM_LABEL")) ; 
							formulaire.setFormStatus(jsonForm.getString("FORM_STATUS")) ; 
							formulaire.setFormStructure(jsonForm.getJSONArray("FORM_STRUCTURE")) ; 
					
							if(localForm == null){

								dataSource.insertForm(formulaire) ;
								Log.d(" formulaire ", " insert name "+ jsonForm.getString("EV_NAME"));
							}else{
								dataSource.updateForm(formulaire) ;
								Log.d(" formulaire ", " update  name " + jsonForm.getString("EV_NAME"));
							}
						}
						if(response != null)
						response.processFinish("forms") ; 
										
					}else {
						if(response != null)
						response.processFailed() ;
					}
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					if(response != null)
					response.processFailed() ;
				}
			}else{
				if(response != null)
				response.processFailed() ; 
							
			}
	}

}
