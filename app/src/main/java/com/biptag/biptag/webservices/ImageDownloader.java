package com.biptag.biptag.webservices;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import org.apache.http.util.ByteArrayBuffer;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.os.AsyncTask;
import android.util.Log;
import android.view.Display;
import android.view.WindowManager;
import android.widget.ImageView;

import com.biptag.biptag.Constant;
import com.biptag.biptag.NavigationActivity;
import com.biptag.biptag.R;
import com.biptag.biptag.dbmanager.datasource.BTImageDataSource;
import com.biptag.biptag.dbmanager.objects.BTImage;
import com.biptag.biptag.interfaces.ImageHandlerInterface;
import com.biptag.biptag.managers.BiptagUserManger;

public class ImageDownloader extends AsyncTask<String, String, String> {


	String fileName ; 
	String imageUrl ; 
	Context context ; 
	public ImageHandlerInterface response = null  ;
	int responseId ; 
	String imageName ; 
	String imagePath  ; 
	int imageId ; 
	ImageView imageView  ; 
	Bitmap bitmap ;
	public ImageDownloader(String imageURL, String fileName  , Context context , int responseId, String imageName ,String imagePath , int  imageId , ImageView view ) {
		// TODO Auto-generated constructor stub
		this.fileName = fileName ; 
		String prefixUrl = Constant.base_url+"upload/" ;
		this.imageUrl  = prefixUrl+imageURL ; 
		this.context = NavigationActivity.getContext() ; 
		this.responseId = responseId ;
		this.imageName = imageName ; 
		this.imagePath =  imagePath ;
		this.imageId  = imageId ; 
		this.imageView = view ; 
	}
	
	
	   public void downloadFromUrl(String imageURL, String fileName) {  //this is the downloader method
           try {
                   URL url = new URL(imageURL); //you can write here any link
                   File file = new File(fileName);

                   long startTime = System.currentTimeMillis();
                   Log.d("ImageManager", "download begining");
                   Log.d("ImageManager", "download url:" + url);
                   Log.d("ImageManager", "downloaded file name:" + fileName);
                   /* Open a connection to that URL. */
                   URLConnection ucon = url.openConnection();

                   /*
                    * Define InputStreams to read from the URLConnection.
                    */
                   InputStream is = ucon.getInputStream();
                   BufferedInputStream bis = new BufferedInputStream(is);

                   /*
                    * Read bytes to the Buffer until there is nothing more to read(-1).
                    */
                   ByteArrayBuffer baf = new ByteArrayBuffer(50);
                   int current = 0;
                   while ((current = bis.read()) != -1) {
                           baf.append((byte) current);
                   }

                   /* Convert the Bytes read to a String. */
                   FileOutputStream fos = new FileOutputStream(file);
                   fos.write(baf.toByteArray());
                   fos.close();
                   Log.d("ImageManager", "download ready in" + ((System.currentTimeMillis() - startTime) / 1000) + " sec");

			   final BitmapFactory.Options opts = new BitmapFactory.Options();
			   opts.inJustDecodeBounds = true;
			   bitmap = BitmapFactory.decodeFile(file.getAbsolutePath(),opts) ;
			   Log.d("image ", "image path 1" + file.getAbsolutePath());
			   // Calculate inSampleSize based on a preset ratio
			   opts.inSampleSize = calculateInSampleSize(opts,450, 450);

			   // Decode bitmap with inSampleSize set
			   opts.inJustDecodeBounds = false;
			   bitmap = BitmapFactory.decodeFile(file.getAbsolutePath(), opts);
if (bitmap!=null){
	Log.d(" image ", " not null ");
}else {
	Log.d(" image ", " null ");
}

/*
			   // Calculate inSampleSize based on a preset ratio
			   opts.inSampleSize = calculateInSampleSize(opts,450, 450);

			   // Decode bitmap with inSampleSize set
			   opts.inJustDecodeBounds = false;
			   bitmap = BitmapFactory.decodeFile(imagePath, opts);
*/
//			   Bitmap bitmap2 = Bitmap.createScaledBitmap(bitmap, bitmap.getWidth() / 4, bitmap.getHeight() / 4, true);
	//		bitmap=bitmap2;
           } catch (IOException e) {
                   Log.d("image ", "Error: " + e);
           }

   }

	public static int calculateInSampleSize(BitmapFactory.Options options,int reqWidth, int reqHeight) {

		final int height = options.outHeight;
		final int width = options.outWidth;
		int inSampleSize = 1; // default to not zoom image

		if (height > reqHeight || width > reqWidth) {
			final int heightRatio = Math.round((float) height/ (float) reqHeight);
			final int widthRatio = Math.round((float) width / (float) reqWidth);
			inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
		}
		return inSampleSize;
	}


	@Override
	protected String doInBackground(String... params) {
		// TODO Auto-generated method stub

		try {
			final BitmapFactory.Options options = new BitmapFactory.Options();
			options.inJustDecodeBounds = true;
			bitmap = BitmapFactory.decodeFile(imagePath, options) ;

			// Calculate inSampleSize based on a preset ratio
			options.inSampleSize = calculateInSampleSize(options,450, 450);

			// Decode bitmap with inSampleSize set
			options.inJustDecodeBounds = false;
			bitmap = BitmapFactory.decodeFile(imagePath, options);
			Log.d("image ", "bitmap path " +imagePath);

			if(bitmap == null){
				Log.d("image ", "process finish gallery image downloader image is null");
				this.downloadFromUrl(imageUrl, fileName) ;
			}
		} catch (OutOfMemoryError e) {
			// TODO: handle exception
		}

		return null;
	}
	 
	@Override
	protected void onPostExecute(String result) {
		// TODO Auto-generated method stub
		super.onPostExecute(result);
		
		
		if(bitmap  != null && imageView != null){
		
			WindowManager wm = (WindowManager) NavigationActivity.getContext().getSystemService(Context.WINDOW_SERVICE);
				Display display  =wm.getDefaultDisplay() ;
				Point point  = new Point() ;  
				display.getSize(point) ; 
				int screenWidth = point.x ; 
				int screenHeight = point.y ;
			
			int imgHeight = bitmap.getHeight() ;
			int imgWidth  = bitmap.getWidth() ;
			Bitmap resized;


/*
			final BitmapFactory.Options opts = new BitmapFactory.Options();
			opts.inJustDecodeBounds = true;
			bitmap = BitmapFactory.decodeFile(imagePath,opts) ;
			Log.d("image ", "image path " + imagePath);

			// Calculate inSampleSize based on a preset ratio
			opts.inSampleSize = calculateInSampleSize(opts,450, 450);

			// Decode bitmap with inSampleSize set
			opts.inJustDecodeBounds = false;
			bitmap = BitmapFactory.decodeFile(imagePath, opts);
*/

			if (imgHeight>500) {


				resized = Bitmap.createScaledBitmap(bitmap, bitmap.getWidth() / 16, bitmap.getHeight() / 16, true);
			}else{
				resized = Bitmap.createScaledBitmap(bitmap, bitmap.getWidth() / 5, bitmap.getHeight() / 5, true);
			}
			Log.d(" image ","resized size 1 : " + String.valueOf(bitmap.getWidth()) + String.valueOf(bitmap.getHeight()));
			Log.d(" image ","resized size 2 : "+ String.valueOf(resized.getWidth()) + String.valueOf(resized.getHeight()));
			if(imgHeight> imgWidth){
				// portrait
				double rapport   = ( (double) imgHeight)/ (screenWidth / 4.0 )  ;


				//Log.d("scral img", "img dim protrait rapp "+rapport+" width  "+(int) (imgWidth /rapport  )+" heigth "+screenHeight/4) ; 
				//resized =Bitmap.createScaledBitmap(bitmap, (int) (  imgWidth /10), imgHeight/10, true) ;
				Log.d("resized size 2 : ", String.valueOf(resized.getWidth()) + String.valueOf(resized.getHeight()));
				//resized = new CompressFormat(bitmap);
				Log.d("Image ", " image frm what? ");

			}

			imageView.setImageBitmap(bitmap) ;

			
		}
		
		if(imageId == 0){
			
			if(bitmap != null){
				//Log.d("image json ", "image json finish downlaod image  ok") ; 

				BTImage image = new BTImage() ; 
	    		int agent_id= Integer.parseInt(String.valueOf(BiptagUserManger.getUserValue("BA_ID", "int", this.context))) ; 
	    		image.setAgentId(agent_id) ; 
	    		image.setImageName(this.imageName) ; 
	    		image.setResponseId(responseId) ; 
	    		image.setIsSent(1) ; 
	    		Calendar c = Calendar.getInstance();
	    		SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
	    		String formattedDate = df.format(c.getTime());
	    		image.setImageDate(formattedDate) ; 
	    		BTImageDataSource imageDataSource = new BTImageDataSource(context); 
	    		long id =imageDataSource.saveImage(image) ; 
	    		if(response!=  null)
	    		response.processFinish(String.valueOf(id)) ; 
				  
				
			}else{
				//Log.d("image json ", "image json finish downlaod image  ko") ; 
				
			}
			
			
			
		}else{
			if(response !=null)
			response.processFinish(String.valueOf(imageId )) ; 
		}

		
		
	}
	

}