package com.biptag.biptag.webservices.templates;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.R.integer;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.biptag.biptag.Constant;
import com.biptag.biptag.JSONParser;
import com.biptag.biptag.R;
import com.biptag.biptag.dbmanager.datasource.BTFollowupTemplateDataSource;
import com.biptag.biptag.dbmanager.objects.BTFollowupTemplate;
import com.biptag.biptag.interfaces.AsyncResponse;
import com.biptag.biptag.managers.BiptagUserManger;

public class DownloadTemplateWithIds extends AsyncTask<String, String, String> {
	
	

	JSONParser parser ; 
	JSONObject jsonObject ; 
	Context context ; 
	JSONArray ids ; 
	
	public AsyncResponse response = null ; 
	
	public DownloadTemplateWithIds(Context context  , JSONArray ids ) {
		// TODO Auto-generated constructor stub
		this.context= context ; 
		this.ids = ids ; 
	}

	@Override
	protected String doInBackground(String... params) {
		// TODO Auto-generated method stub
		
		String url= Constant.base_url +Constant.get_templates_with_ids;
		String ba_id = String.valueOf(BiptagUserManger.getUserValue("BA_ID", "int", context.getApplicationContext())) ; 
		List<NameValuePair> list= new ArrayList<NameValuePair>() ; 
		list.add(new BasicNameValuePair("ba_id",ba_id ));
		list.add(new BasicNameValuePair("ids", ids.toString())) ; 
		parser = new JSONParser() ; 
		jsonObject = parser.makeHttpRequest(url, "GET", list) ;
		if(jsonObject!= null){
			//Log.d("get templates ", " get templates "+jsonObject.toString()) ; 
			
			try {
				if(jsonObject.getInt("success") == 1){
					JSONArray  templates= jsonObject.getJSONArray("templates") ; 
					BTFollowupTemplateDataSource dataSource = new BTFollowupTemplateDataSource(context) ; 
					for (int i = 0; i < templates.length(); i++) {
						JSONObject wsTemplate = templates.getJSONObject(i) ; 
						BTFollowupTemplate localtemplate = dataSource.getTemplateWithID(Integer.parseInt(ba_id), wsTemplate.getInt("FU_ID")) ; 
						
						BTFollowupTemplate tmp = new BTFollowupTemplate() ; 
						tmp.setFu_id(wsTemplate.getInt("FU_ID")) ; 
						tmp.setFu_attach(wsTemplate.getJSONArray("FU_ATTACH")) ; 
						tmp.setFu_content(wsTemplate.getString("FU_CONTENT")); 
						tmp.setFu_hashcode(wsTemplate.getString("FU_HASHCODE")) ; 
						tmp.setBa_id(Integer.parseInt(ba_id)) ; 
						tmp.setFu_subject(wsTemplate.getString("FU_SUBJECT")) ; 
						tmp.setSynchonized(true) ; 
						tmp.setTemp_name(wsTemplate.getString("FU_TEMP_NAME")) ; 
						
						if(localtemplate ==  null){
							dataSource.insertTemplate(tmp) ; 
						}else{
							tmp.setLocal_id(localtemplate.getLocal_id()) ; 
							dataSource.insertTemplate(tmp) ; 
						} 
						    
						
						
						
						
						
						
						
					}
					
					
					
					
				}
			} catch (JSONException e) {
				// TODO: handle exception
			}
			
			
		}
		
		return null;
	}

}
