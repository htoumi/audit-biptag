
package com.biptag.biptag.webservices;
/*
import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.biptag.biptag.Constant;
import com.biptag.biptag.JSONParser;
import com.biptag.biptag.NavigationActivity;
import com.biptag.biptag.R;
import com.biptag.biptag.interfaces.AsyncResponse;
import com.biptag.biptag.managers.BiptagUserManger;

public class DownloadFiles extends AsyncTask<String, String, String>{
	JSONParser parser ; 
	JSONObject jsonObject ; 
	Context context ; 
	
	public AsyncResponse response = null ; 
	
	public DownloadFiles(Context context ) {
		// TODO Auto-generated constructor stub
		this.context= NavigationActivity.getContext() ; 
	}
	
	
	
	@Override
	protected String doInBackground(String... params) {
		// TODO Auto-generated method stub
		List<NameValuePair > list = new ArrayList<NameValuePair>(); 
		
		list.add(new BasicNameValuePair("ba_id",String.valueOf(BiptagUserManger.getUserValue("BA_ID", "int", context)))) ; 
		String url = Constant.base_url  +Constant.get_files_ws;
		JSONParser jsonParser = new JSONParser() ;
		jsonObject = jsonParser.makeHttpRequest(url, "GET", list) ; 
		//Log.d("listing files  files", "listing files  files   json response "+jsonObject.toString()) ; 
		return null; 
	}
	
	
	@Override
	protected void onPostExecute(String result) {
		// TODO Auto-generated method stub
		super.onPostExecute(result);
	try {
		if(jsonObject!= null){
			if(jsonObject.getInt("success")==1){
				JSONObject docs = jsonObject; 
				BiptagUserManger.saveUserData("docs",docs.toString() ,context);
				//Log.d("uploac contact ", "uploac contact finish doc move to upload contact") ;
				if(response != null)
				response.processFinish("docs")  ; 
			}else{
				if(response != null)
				response.processFailed() ;
			}
		}else {
			if(response != null)
			response.processFailed() ;		}
	} catch (JSONException e) {
		// TODO: handle exception
		//Log.d("listing files  files", "download files excpetion "+e.getMessage()) ; 
		if(response != null)
			response.processFailed() ; 
	}
	
	}
	
}

package com.biptag.biptagexposant.webservices;
*/
import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.biptag.biptag.Constant;
import com.biptag.biptag.JSONParser;
import com.biptag.biptag.NavigationActivity;
import com.biptag.biptag.R;
import com.biptag.biptag.dbmanager.datasource.BTCardDataSource;
import com.biptag.biptag.dbmanager.datasource.BTFileDataSource;
import com.biptag.biptag.dbmanager.datasource.BTLinkDataSource;
import com.biptag.biptag.dbmanager.objects.BTCard;
import com.biptag.biptag.dbmanager.objects.BTFile;
import com.biptag.biptag.dbmanager.objects.BTLink;
import com.biptag.biptag.interfaces.AsyncResponse;
import com.biptag.biptag.managers.BiptagUserManger;

public class DownloadFiles extends AsyncTask<String, String, String>{
	JSONParser parser ;
	JSONObject jsonObject ;
	Context context ;

	public AsyncResponse response = null ;

	public DownloadFiles(Context context ) {
		// TODO Auto-generated constructor stub
		this.context= NavigationActivity.getContext() ;
	}



	@Override
	protected String doInBackground(String... params) {
		// TODO Auto-generated method stub
		List<NameValuePair > list = new ArrayList<NameValuePair>();

		list.add(new BasicNameValuePair("ba_id",String.valueOf(BiptagUserManger.getUserValue("BA_ID", "int", context)))) ;
		String url = Constant.base_url +Constant.get_files_ws;
		JSONParser jsonParser = new JSONParser() ;
		jsonObject = jsonParser.makeHttpRequest(url, "GET", list) ;
		BiptagUserManger.saveUserData("docs",jsonObject,NavigationActivity.getContext());
		Log.d("listing files  files", "listing files  files   json response "+jsonObject.toString()) ;
		return null;
	}


	@Override
	protected void onPostExecute(String result) {
		// TODO Auto-generated method stub
		super.onPostExecute(result);
	try {
		if(jsonObject!= null){
			if(jsonObject.getInt("success")==1){
				Log.d("saved file", "file saved  "+jsonObject.toString()) ;
				BiptagUserManger.saveUserData("docs",jsonObject.toString() ,context);
				int ba_id =Integer.parseInt(String.valueOf(BiptagUserManger.getUserValue("BA_ID", "int", context))) ;
				JSONArray links = jsonObject.getJSONArray("LINK").getJSONObject(0).getJSONArray("LINKS") ;
				BTLinkDataSource linkDataSource = new BTLinkDataSource(NavigationActivity.getContext()) ;
				BTFileDataSource fileDataSource = new BTFileDataSource(NavigationActivity.getContext()) ;
				BTCardDataSource cardDataSource = new BTCardDataSource(NavigationActivity.getContext()) ;
				for (int i = 0; i < links.length(); i++) {

					BTLink link= linkDataSource.getLink(ba_id, links.getJSONObject(i).getInt("LINK_ID")) ;
					if(link == null){
						link = new BTLink() ;

						link.setBaID(ba_id) ;
						link.setId(links.getJSONObject(i).getInt("LINK_ID")) ;
						link.setLable(links.getJSONObject(i).getString("LINK_LABEL")) ;
						link.setUrl(links.getJSONObject(i).getString("LINK_URL")) ;
						boolean saved = linkDataSource.insertLink(link) ;
						Log.d("file saved ", "file saved   "+link.getUrl()+" ? "+saved) ;
					}else{
						Log.d("file saved ", "file saved  exist "+link.getUrl()) ;

					}

				}

				JSONArray files = jsonObject.getJSONArray("DOC").getJSONObject(0).getJSONArray("FILES") ;
				for (int i = 0; i < files.length(); i++) {

					BTFile file= fileDataSource.getFile(ba_id, files.getJSONObject(i).getInt("DOC_ID")) ;
					if(file== null){
						file = new BTFile() ;

						file.setBaId(ba_id) ;
						file.setId(files.getJSONObject(i).getInt("DOC_ID")) ;
						file.setName(files.getJSONObject(i).getString("DOC_NAME")) ;
						file.setPhName(files.getJSONObject(i).getString("DOC_PH_NAME")) ;
						boolean saved = fileDataSource.insertFile(file) ;
						Log.d("file saved ", "file saved  "+file.getName()+" saved ? "+saved) ;
					}else{
						Log.d("file saved ", "file saved  exist "+file.getName()) ;
					}
				}

				JSONArray  cards =jsonObject.getJSONArray("CARDS").getJSONObject(0).getJSONArray("CARDS") ;

				for (int i = 0; i < cards.length(); i++) {
					BTCard card = cardDataSource.getCard(ba_id, cards.getJSONObject(i).getInt("VCARD_ID")) ;
					if(card == null){
						card = new BTCard() ;
						card.setBaID(ba_id) ;
						card.setId(cards.getJSONObject(i).getInt("VCARD_ID")) ;
						card.setDef(cards.getJSONObject(i).getJSONObject("VCARD_DEF")) ;
						boolean saved = cardDataSource.insertVcar(card) ;
						Log.d("file saved ", "file saved  "+card.getDef()+" saved ? "+saved) ;
					}else{
						Log.d("file saved ", "file saved  exist "+card.getDef().toString()) ;
					}
				}




				Log.d("uploac contact ", "uploac contact finish doc move to upload contact") ;
				if(response != null)
				response.processFinish("docs")  ;
			}else{
				if(response != null)
				response.processFailed() ;
			}
		}else {
			if(response != null)
			response.processFailed() ;		}
	} catch (JSONException e) {
		// TODO: handle exception
		Log.d("listing files  files", "download files excpetion "+e.getMessage()) ;
		if(response != null)
			response.processFailed() ;
	}

	}

}