package com.biptag.biptag.webservices.templates;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import com.biptag.biptag.Constant;
import com.biptag.biptag.JSONParser;
import com.biptag.biptag.R;
import com.biptag.biptag.dbmanager.datasource.BTFollowupTemplateDataSource;
import com.biptag.biptag.dbmanager.objects.BTFollowupTemplate;
import com.biptag.biptag.interfaces.AsyncResponse;
import com.biptag.biptag.managers.BiptagUserManger;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

public class SaveTemplate extends AsyncTask<String, String, String> {

	

	JSONParser parser ; 
	JSONObject jsonObject ; 
	Context context ; 

	BTFollowupTemplate template  ; 
	
	public AsyncResponse response = null ; 
	
	public SaveTemplate(Context context , BTFollowupTemplate tmp  ) {
		// TODO Auto-generated constructor stub
		this.context= context ; 
		this.template = tmp ; 
	}
	
	@Override
	protected String doInBackground(String... params) {
		// TODO Auto-generated method stub
		
		
		
		String url= Constant.base_url +Constant.insert_followup ;
		String ba_id = String.valueOf(BiptagUserManger.getUserValue("BA_ID", "int", context.getApplicationContext())) ; 
		List<NameValuePair> list= new ArrayList<NameValuePair>() ; 
		list.add(new BasicNameValuePair("ba_id",ba_id ));
		list.add(new BasicNameValuePair("text", template.getFu_content())) ; 
		list.add(new BasicNameValuePair("subject", template.getFu_subject())) ; 
		list.add(new BasicNameValuePair("temp_name", template.getTemp_name())) ; 
		list.add(new BasicNameValuePair("attach",template.getFu_attach().toString())) ; 
		
		parser = new JSONParser() ; 
		
		jsonObject =parser.makeHttpRequest(url, "GET", list) ; 
		
		
		if(jsonObject !=  null){
			//Log.d("save template ", "save template json "+jsonObject.toString()) ; 
			
			try {
				if(jsonObject.getInt("success")== 1){
					 
					template.setFu_hashcode(jsonObject.getString("FU_HASHCODE")) ; 
					template.setFu_id(jsonObject.getInt("FU_ID")) ; 
					template.setSynchonized(true) ; 
					BTFollowupTemplateDataSource dataSource = new BTFollowupTemplateDataSource(context); 
					dataSource.updateTemplate(template) ; 
					 
					
				}
			} catch (JSONException e) {
				// TODO: handle exception
				//Log.d("save template ", "save template exception  "+e.getMessage()) ; 
			}
			
		}
		
		
		
		
		
		
		return null;
	}

}
