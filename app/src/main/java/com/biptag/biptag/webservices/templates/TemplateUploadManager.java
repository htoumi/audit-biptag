package com.biptag.biptag.webservices.templates;

import android.content.Context;
import android.os.AsyncTask;

import com.biptag.biptag.Constant;
import com.biptag.biptag.JSONParser;
import com.biptag.biptag.NavigationActivity;
import com.biptag.biptag.dbmanager.datasource.BTFollowupTemplateDataSource;
import com.biptag.biptag.dbmanager.objects.BTFollowupTemplate;
import com.biptag.biptag.managers.BiptagUserManger;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by lenovo on 24/02/2016.
 */
public class TemplateUploadManager extends AsyncTask<String , String , String> {

  Context context ;
    BTFollowupTemplate template;

    JSONObject jsonObject ;
    JSONParser parser  ;
    public  TemplateUploadManager(Context context , BTFollowupTemplate  template){
        this.context= context ;
        this.template = template;

    }

    @Override
    protected String doInBackground(String... params) {

        String url  = "" ;

        if(template.getFu_id() == 0){
            // insert
            url = Constant.base_url+Constant.insert_followup ;
            String ba_id = String.valueOf(BiptagUserManger.getUserValue("BA_ID", "int", context.getApplicationContext())) ;
            List<NameValuePair> list= new ArrayList<NameValuePair>() ;
            list.add(new BasicNameValuePair("ba_id",ba_id ));
            list.add(new BasicNameValuePair("text", template.getFu_content())) ;
            list.add(new BasicNameValuePair("subject", template.getFu_subject())) ;
            list.add(new BasicNameValuePair("temp_name", template.getTemp_name())) ;
            list.add(new BasicNameValuePair("attach",template.getFu_attach().toString())) ;

            parser = new JSONParser() ;

            jsonObject =parser.makeHttpRequest(url, "GET", list) ;



        }else{
            url = Constant.base_url+Constant.update_followup ;

            String ba_id = String.valueOf(BiptagUserManger.getUserValue("BA_ID", "int", context.getApplicationContext())) ;
            List<NameValuePair> list= new ArrayList<NameValuePair>() ;
            list.add(new BasicNameValuePair("fu_id",String.valueOf(template.getFu_id())));
            list.add(new BasicNameValuePair("ba_id",ba_id ));
            list.add(new BasicNameValuePair("text", template.getFu_content())) ;
            list.add(new BasicNameValuePair("subject", template.getFu_subject())) ;
            list.add(new BasicNameValuePair("temp_name", template.getTemp_name())) ;
            list.add(new BasicNameValuePair("attach",template.getFu_attach().toString())) ;

            parser = new JSONParser() ;

            jsonObject =parser.makeHttpRequest(url, "GET", list) ;


        }

        return null;
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);

        if(jsonObject != null){

            try {
                if(jsonObject.getInt("success")== 1){
                    int fu_id  = jsonObject.getInt("FU_ID") ;
                    String hashCode = jsonObject.getString("FU_HASHCODE") ;
                    template.setFu_id(fu_id);
                    template.setFu_hashcode(hashCode);
                    template.setSynchonized(true);
                    BTFollowupTemplateDataSource dataSource = new BTFollowupTemplateDataSource(NavigationActivity.getContext()) ;
                    dataSource.updateTemplate(template) ;

                }
            }catch (JSONException e){

            }


        }

    }
}
