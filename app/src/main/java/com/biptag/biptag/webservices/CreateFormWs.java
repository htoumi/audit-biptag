package com.biptag.biptag.webservices;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.biptag.biptag.Constant;
import com.biptag.biptag.R;
import com.biptag.biptag.interfaces.AsyncResponse;
import com.biptag.biptag.managers.BiptagUserManger;

public class CreateFormWs extends AsyncTask<String, String, String> {
 
	String form_structure  ; 
	String event_name ;  
	String event_edition ; 
	String event_start ;  
	String event_end ;  
	String form_title ;  
	String agent_id  ; 
	Context context ; 
	 static InputStream is = null;
	    static JSONObject jObj = null;
	    static String json = "";
	public AsyncResponse response = null ; 
	JSONObject createFormResult ; 
	public CreateFormWs(	String form_structure  ,	String event_name ,	String event_edition , 	String event_start, String event_end ,String form_title ,  Context context ) {
		// TODO Auto-generated constructor stub
		this.form_structure = form_structure  ;
		this.form_title = form_title ;  
		this.event_edition = event_edition ; 
		this.event_end = event_end ; 
		this.event_name = event_name ; 
		this.event_start  = event_start ;  
		this.context = context ; 
		agent_id = String.valueOf(BiptagUserManger.getUserValue("AGENT_ID", "int", context)) ; 
		 
	}
	 
	 
	
	@Override
	protected void onPreExecute() {
		// TODO Auto-generated method stub
		super.onPreExecute();
		
	}
	
	
	@Override
	protected String doInBackground(String... params) {
		// TODO Auto-generated method stub
		
		
		String url= Constant.base_url+Constant.publish_form ;
		
		List<NameValuePair> list = new ArrayList<NameValuePair>() ; 
	
		//Log.d("form structure", "form structure "+this.form_structure) ; 
		try {
			list.add(new BasicNameValuePair("form_structure", URLEncoder.encode(this.form_structure, "utf-8"))) ;
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		list.add(new BasicNameValuePair("event_name", this.event_name)) ; 
		list.add(new BasicNameValuePair("event_edition", this.event_edition)); 
		list.add(new BasicNameValuePair("event_start", this.event_start)) ; 
		list.add(new BasicNameValuePair("event_end", this.event_end)) ; 
		list.add(new BasicNameValuePair("form_title", this.form_title)) ; 
		list.add(new BasicNameValuePair("agent_id", this.agent_id)) ;
		
		createFormResult = makeHttpRequest(url, "GET", list) ; 
		 
		return null;
	}
	
	@Override
	protected void onPostExecute(String result) {
		// TODO Auto-generated method stub
		super.onPostExecute(result);
		
	
		if(createFormResult != null){
			response.processFinish(createFormResult.toString()) ; 
		}else{
			response.processFailed() ; 
		}
	
	}
	
	  public JSONObject makeHttpRequest(String url, String method,
              List<NameValuePair> params) {


//Log.d("makeHttpRequest",	 "makeHttpRequest "+url) ; 
// Making HTTP request
try { 

// check for request method
if(method == "POST"){
// request method is POST
// defaultHttpClient
DefaultHttpClient httpClient = new DefaultHttpClient();
HttpPost httpPost = new HttpPost(url);
httpPost.setEntity(new UrlEncodedFormEntity(params));

HttpResponse httpResponse = httpClient.execute(httpPost);
HttpEntity httpEntity = httpResponse.getEntity();
is = httpEntity.getContent();

}else if(method == "GET"){
// request method is GET
DefaultHttpClient httpClient = new DefaultHttpClient();
String paramString =  "" ; // URLEncodedUtils.format(params, "utf-8");
	for (int i = 0; i < params.size(); i++) {
		if(!params.get(i).getName().equals("form_structure"))
		paramString += params.get(i).getName()+"="+URLEncoder.encode(params.get(i).getValue(),"utf-8" )+"&" ; 
		else
			paramString += params.get(i).getName()+"="+params.get(i).getValue()+"&" ; 
			
	}

url += "?" + paramString;
//Log.d("url json ",	"json success url "+url);

HttpGet httpGet = new HttpGet(url); 
HttpResponse httpResponse = httpClient.execute(httpGet);
HttpEntity httpEntity = httpResponse.getEntity();
is = httpEntity.getContent();

} 
//Log.d("json response json parser", "json response json parser "+is.toString());
} catch (UnsupportedEncodingException e) {
e.printStackTrace();
//Log.d("json response exception", "json response exception"+e.getMessage());
} catch (ClientProtocolException e) {
e.printStackTrace();
//Log.d("json response exception", "json response exception"+e.getMessage());
} catch (IOException e) {
e.printStackTrace();
//Log.d("json response exception", "json response exception"+e.getMessage());
}

try {
BufferedReader reader = new BufferedReader(new InputStreamReader(
is, "iso-8859-1"), 8);
StringBuilder sb = new StringBuilder();
String line = null;
while ((line = reader.readLine()) != null) {
sb.append(line + "\n");
} 
is.close();
//Log.d("json response ", "json response		"+sb.toString());

json = sb.toString();
json = json.replaceAll("&amp;", "&") ; 
} catch (Exception e) {
//Log.e("Buffer Error", "Error converting result " + e.toString());
}

// try parse the string to a JSON object
try {
jObj = new JSONObject(json);
} catch (JSONException e) {
//Log.e("JSON Parser", "Error parsing data " + e.toString());
}

// return JSON String
return jObj;

}
	
	
	
	
	
	

}
