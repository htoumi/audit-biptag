package com.biptag.biptag.webservices.templates;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.R.integer;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.biptag.biptag.Constant;
import com.biptag.biptag.JSONParser;
import com.biptag.biptag.R;
import com.biptag.biptag.dbmanager.datasource.BTFollowupTemplateDataSource;
import com.biptag.biptag.dbmanager.objects.BTFollowupTemplate;
import com.biptag.biptag.interfaces.AsyncResponse;
import com.biptag.biptag.managers.BiptagUserManger;

public class DownloadFollowupHashcodes extends AsyncTask<String, String, String>  {
	
	
	JSONParser parser ; 
	JSONObject jsonObject ; 
	Context context ; 
	
	public AsyncResponse response = null ; 
	
	public DownloadFollowupHashcodes(Context context ) {
		// TODO Auto-generated constructor stub
		this.context= context ; 
	}

	@Override
	protected String doInBackground(String... params) {
		// TODO Auto-generated method stub
		String url= Constant.base_url +Constant.get_templates_whth_hashcode;
		String ba_id = String.valueOf(BiptagUserManger.getUserValue("BA_ID", "int", context.getApplicationContext())) ; 
		List<NameValuePair> list= new ArrayList<NameValuePair>() ; 
		list.add(new BasicNameValuePair("ba_id",ba_id ));
		parser = new JSONParser() ; 
		jsonObject = parser.makeHttpRequest(url, "GET", list) ;
		JSONArray listToDownload = new JSONArray() ; 
		
		if(jsonObject != null){
			
			try {
				if(jsonObject.getInt("success") == 1 ){
					JSONArray tempaltes = jsonObject.getJSONArray("templates") ; 
					BTFollowupTemplateDataSource dataSource = new BTFollowupTemplateDataSource(context) ;
					for (int i = 0; i < tempaltes.length() ; i++) {
						
						int tempalteId = tempaltes.getJSONObject(i).getInt("FU_ID") ; 
						
						BTFollowupTemplate template = dataSource.getTemplateWithID(Integer.parseInt(ba_id), tempalteId) ; 
						if(template == null){
							//Log.d("template id", "template id  template null ") ; 
							listToDownload.put(tempalteId) ; 
						}else{
							//Log.d("template id", "template id  template not null "+template.getFu_id()+" "+template.getFu_hashcode()+"   "+tempaltes.getJSONObject(i).getString("FU_HASHCODE")) ; 
							if(!template.getFu_hashcode().equals(tempaltes.getJSONObject(i).getString("FU_HASHCODE"))){
								listToDownload.put(tempalteId) ; 		
							}
							 
						}
					}
					
					
					
					if(listToDownload.length() != 0){
						DownloadTemplateWithIds downloadTemplateWithIds = new DownloadTemplateWithIds(context, listToDownload) ;
						downloadTemplateWithIds.execute() ; 
					
					
					}
					
					
					
				}
			} catch (JSONException e) {
				// TODO: handle exception
			}
			
		}
		
		
		
		return null;
	}
	

}
