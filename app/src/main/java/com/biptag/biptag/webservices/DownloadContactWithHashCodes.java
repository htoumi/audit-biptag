package com.biptag.biptag.webservices;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.R.bool;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.biptag.biptag.Constant;
import com.biptag.biptag.JSONParser;
import com.biptag.biptag.NavigationActivity;
import com.biptag.biptag.R;
import com.biptag.biptag.dbmanager.datasource.BTContactDataSource;
import com.biptag.biptag.dbmanager.objects.BTContact;
import com.biptag.biptag.interfaces.AsyncResponse;
import com.biptag.biptag.managers.BiptagUserManger;

public class DownloadContactWithHashCodes  extends AsyncTask<String, String, String>{
	JSONParser parser ; 
	JSONObject jsonObject ; 
	Context context ; 
	public static boolean isExecuting  = false ; 
	
	public AsyncResponse response = null ; 
	
	public DownloadContactWithHashCodes(Context context ) {
		// TODO Auto-generated constructor stub
		this.context= NavigationActivity.getContext() ; 
	}

	@Override
	protected String doInBackground(String... params) {
		// TODO Auto-generated method stub
		isExecuting = true ; 
		String url= Constant.base_url  +Constant.get_contacts_hash;
		List<NameValuePair> list= new ArrayList<NameValuePair>() ; 
		String ba_id = String.valueOf(BiptagUserManger.getUserValue("BA_ID", "int", context.getApplicationContext())) ; 
		String agent_id= String.valueOf(BiptagUserManger.getUserValue("AGENT_ID", "int", context.getApplicationContext())) ; 
		String role= String.valueOf(BiptagUserManger.getUserValue("ROLE_DESC", "string", context.getApplicationContext())) ; 
		list.add(new BasicNameValuePair("ba_id",ba_id ));
		list.add(new BasicNameValuePair("agent_id", agent_id)) ; 
		list.add(new BasicNameValuePair("role", role)) ; 
		JSONParser parser = new JSONParser(); 
		jsonObject = parser.makeHttpRequest(url, "GET", list) ;

		JSONArray listToDownload = new JSONArray() ; 
		if(jsonObject != null){
		//Log.d("download contact whit hashcode", "download contact whit hashcode "+jsonObject.toString()) ; 
			try {
				if(jsonObject.getInt("success") == 1){
					BTContactDataSource contactDataSource = new BTContactDataSource(context) ; 
					for (int i = 0; i < jsonObject.getJSONArray("contacts").length(); i++) {
						int contact_id = jsonObject.getJSONArray("contacts").getJSONObject(i).getInt("FR_ID") ;
						BTContact contact = contactDataSource.getContactWithFrId(Integer.parseInt(agent_id), contact_id) ; 
						if(contact == null){
							listToDownload.put(contact_id) ; 
						}else{ 
							//Log.d("contact hash code ", "contact hashcode "+jsonObject.getJSONArray("contacts").getJSONObject(i).getString("FR_HASHCODE")+"  "+contact.getContactFrHashcode()+"  "+(!contact.getContactFrHashcode().equals(jsonObject.getJSONArray("contacts").getJSONObject(i).getString("FR_HASHCODE")))) ;
							if(!contact.getContactFrHashcode().equals(jsonObject.getJSONArray("contacts").getJSONObject(i).getString("FR_HASHCODE"))){
								listToDownload.put(contact_id) ;
							}
						}
					}
				
				}
			} catch (JSONException e) {
				// TODO: handle exception
				//Log.d("download contact whit hashcode 	", "download contact whit hashcode exception "+e.getMessage())  ; 
			}
			Log.d("download contact whit hashcode 	", "download contact whit hashcode id to download " + listToDownload.length())  ;
			Calendar cal = Calendar.getInstance();
			Log.d(" formulaire ", " " + cal.getTime());
			BiptagUserManger.saveUserData("ids_to_download", listToDownload.toString(), context) ;


		}else{
			//Log.d("download contact whit hashcode", "download contact whit hashcode  null ") ; 
		}

		
		
		return null;
	}
	@Override
	protected void onPostExecute(String result) {
		// TODO Auto-generated method stub
		super.onPostExecute(result);
		
		isExecuting = false ;
	try {
		if(jsonObject!= null){
			if(jsonObject.getInt("success")==1){
				if(response != null)
				response.processFinish("ids_ready")  ; 
			}else{
				if(response != null)
				response.processFailed() ;
			}
		}else {
			if(response != null)
			response.processFailed() ;	
			}
	} catch (JSONException e) {
		// TODO: handle exception
		//Log.d("listing files  files", "download files excpetion "+e.getMessage()) ;
			response.processFailed() ; 
	}
	
	}
	
	
	
}
