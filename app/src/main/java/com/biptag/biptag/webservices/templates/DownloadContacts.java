package com.biptag.biptag.webservices.templates;

/**
 * Created by lenovo on 31/12/2015.
 */


import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.biptag.biptag.Constant;
import com.biptag.biptag.JSONParser;
import com.biptag.biptag.NavigationActivity;
import com.biptag.biptag.R;
import com.biptag.biptag.dbmanager.datasource.BTContactDataSource;
import com.biptag.biptag.dbmanager.datasource.BTFormDataSource;
import com.biptag.biptag.dbmanager.objects.BTContact;
import com.biptag.biptag.dbmanager.objects.BTFormulaire;
import com.biptag.biptag.interfaces.AsyncResponse;
import com.biptag.biptag.managers.BiptagUserManger;

public class DownloadContacts extends AsyncTask<String, String, String> {

    JSONParser parser ;
    JSONObject jsonObject ;
    Context context ;
    int agent_id_int ;
    public AsyncResponse response = null ;
    public static boolean isExecuting = false  ;

    public DownloadContacts(Context context ) {
        // TODO Auto-generated constructor stub
        this.context= NavigationActivity.getContext() ;
    }

    @Override
    protected String doInBackground(String... params) {
        // TODO Auto-generated method stub
        isExecuting = true ;
        //get_contact_with_ids

        Calendar calen = Calendar.getInstance();
        Log.d(" formulaire "," do in backgd debu " + calen.getTime());

        if(context != null){
            String url= Constant.base_url  +Constant.get_contact_ws;
            List<NameValuePair> list= new ArrayList<NameValuePair>() ;
            String ba_id = String.valueOf(BiptagUserManger.getUserValue("BA_ID", "int", context.getApplicationContext())) ;
            String agent_id= String.valueOf(BiptagUserManger.getUserValue("AGENT_ID", "int", context.getApplicationContext())) ;
            agent_id_int = Integer.parseInt(agent_id) ;
            String role= String.valueOf(BiptagUserManger.getUserValue("ROLE_DESC", "string", context.getApplicationContext())) ;
            list.add(new BasicNameValuePair("ba_id",ba_id ));
            list.add(new BasicNameValuePair("agent_id", agent_id)) ;
            list.add(new BasicNameValuePair("role", role)) ;
              //Log.d("download contact with ids", "download contact with ids  ids are "+ids_to_download) ;
           parser = new JSONParser() ;

            jsonObject = parser.makeHttpRequest(url,"GET",list) ;



        }


        Log.d(" formulaire "," do in backgd fin " + calen.getTime() );


        return null;
    }

    @Override
    protected void onPostExecute(String result) {
        // TODO Auto-generated method stub
        super.onPostExecute(result);
        Calendar calenda = Calendar.getInstance();
        Log.d(" contact ", " time loading deb " + calenda.getTime());

        if(jsonObject != null){
            Log.d("download contact with ids 1 ", "download contact with ids "+jsonObject.toString()) ;
            try {
                Log.d(" contact ", "before if size " + jsonObject.getJSONArray("contacts").length());
                if(jsonObject.getInt("success")== 1){

                    BTContactDataSource  dataSource = new BTContactDataSource(context)   ;

                    for (int i = 0; i < jsonObject.getJSONArray("contacts").length(); i++) {
                        try {
                            JSONObject wsContact = jsonObject.getJSONArray("contacts").getJSONObject(i) ;
                            BTContact contact = dataSource.getContactWithFrId(agent_id_int, wsContact.getInt("FR_ID")) ;
                            Log.d(" contact ", " for and try agent id" + agent_id_int + " frid " + wsContact.getInt("FR_ID"));
                           List<BTContact> contact2 = dataSource.getAllContacts(agent_id_int,1);



                            if(contact !=  null){
                                Log.d("process finish save contact 2 ", "process finish save contact contact not null"+contact.getContactEmail()+" "+contact.getContactFrID()) ;
                                contact.setContactEmail(wsContact.getString("CONTACT_MAIL")) ;
                                contact.setContactFrResponse(wsContact.getJSONObject("FR_CONTENT")) ;
                                contact.setContactFrID(wsContact.getInt("FR_ID")) ;
                                contact.setContactFrHashcode(wsContact.getString("FR_HASHCODE")) ;
                                contact.setSynchronized(1) ;
                                contact.setCreateTime(wsContact.getString("LOG_TIMESTAMP"));
                                dataSource.updateContact(contact) ;

                            }else{

                                BTContact btContact = new BTContact()  ;
                                btContact.setContactEmail(wsContact.getString("CONTACT_MAIL")) ;
                                btContact.setContactFormId(wsContact.getInt("FORM_ID")) ;
                                btContact.setContactFrAgentID(agent_id_int) ;
                                btContact.setContactFrID(wsContact.getInt("FR_ID")) ;
                                Log.d("contact saving exception 3 ", " process finish  content causing  exception "+wsContact.toString());
                                btContact.setContactFrResponse(wsContact.getJSONObject("FR_CONTENT")) ;
                                btContact.setContactId(wsContact.getInt("CONTACT_ID")) ;
                                btContact.setContactFrHashcode(wsContact.getString("FR_HASHCODE")) ;
                                btContact.setDoFollowup(false) ;
                                btContact.setCreateTime(wsContact.getString("LOG_TIMESTAMP"));

                                JSONObject response  = btContact.getContactFrResponse() ;
                                btContact.setSynchronized(1);
                                btContact.setContactName(" ") ;
                                BTFormDataSource formDataSource = new BTFormDataSource(context) ;
                                List<BTFormulaire> allFormsList = formDataSource.getAllForms(agent_id_int);
                                for (int j = 0; j < allFormsList.size(); j++) {
                                    BTFormulaire formulaire = allFormsList.get(j) ;
                                    if(formulaire.getFormId() ==  btContact.getContactFormId()){
                                        JSONArray structure = formulaire.getFormStructure() ;
                                        List<String> keys = new ArrayList<String>() ;
                                        for (int k = 0; k < structure.length(); k++) {
                                            if(structure.getJSONObject(k).getString("type").equals("LABEL")){
                                                keys.add(structure.getJSONObject(k).getJSONArray("VALUE").getString(0)) ;
                                            }
                                        }
                                        btContact.setContactName(getContactName(btContact)) ;


										/*
										String name = btContact.getContactFrResponse().getString(keys.get(1))+" "+btContact.getContactFrResponse().getString(keys.get(3)) ;
										if(!name.equals(" ")){
											btContact.setContactName(name) ;
										}*/
                                    }
                                }

                                Log.d("process finish save contact 4 ", "process finish save contact contact not found "+btContact.getContactEmail()+" "+btContact.getContactFrID()) ;

                                dataSource.insertContact(btContact) ;
                            }

                            this.response.synchronisationStepUp() ;
                        } catch (JSONException e) {
                            // TODO: handle exception
                            e.printStackTrace();
                            Log.d("contact saving exception 5 ", " process finish contact saving exception "+e.getMessage() + " " );
                        }
                    }
                }
            } catch (JSONException e) {
                // TODO: handle exception
                Log.d("contact saving exception 6 ", " process finish contact saving exception "+e.getMessage());
            }


        }else{
            Log.d("download contact with ids 7 ", "download contact with ids response null ") ;
        }

        ((NavigationActivity) context).updateContactNumberView() ;

        if(jsonObject != null){
            try {
                if(jsonObject.getInt("success")== 1){
                    if(response != null)
                        response.processFinish("contacts_done") ;

                }else {
                    if(response != null)
                        response.processFailed() ;
                }
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                response.processFailed() ;
            }
        }else{
            if(response != null)
                response.processFailed() ;

        }
        Calendar calendar = Calendar.getInstance();
        Log.d(" contact ", " time loading end " + calendar.getTime());
        isExecuting = false ;
    }



    public String getContactName(BTContact contact){
        String name = "" ;

        try {
            JSONObject object =contact.getContactFrResponse() ;
            BTFormDataSource dataSource = new BTFormDataSource(NavigationActivity.getContext()) ;
            BTFormulaire  formulaire = dataSource.getFormWithID(agent_id_int, contact.getContactFormId()) ;
            try {
                String firstNameKey = formulaire.getFormStructure().getJSONObject(2).getJSONArray("VALUE").getString(0) ;
                String lastNameKey= formulaire.getFormStructure().getJSONObject(4).getJSONArray("VALUE").getString(0) ;
                String firstName = contact.getContactFrResponse().getString(firstNameKey) ;
                String lastName = contact.getContactFrResponse().getString(lastNameKey) ;
                name = firstName+" "+lastName ;
            } catch (JSONException e) {
                // TODO: handle exception
            }

        } catch (Exception e) {
            // TODO: handle exception
        }

        return name ;
    }






}
