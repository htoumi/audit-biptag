package com.biptag.biptag.webservices.templates;

import android.content.Context;
import android.os.AsyncTask;

import com.biptag.biptag.Constant;
import com.biptag.biptag.NavigationActivity;
import com.biptag.biptag.dbmanager.datasource.BTFollowupTemplateDataSource;
import com.biptag.biptag.dbmanager.objects.BTFollowupTemplate;

import java.util.List;

/**
 * Created by lenovo on 24/02/2016.
 */
public class TemplateUploader extends AsyncTask<String ,String , String> {

    Context context  ;
    int ba_id  ;
    public TemplateUploader(Context context , int ba_id){
        this.context = context ;
        this.ba_id = ba_id ;
    }

    @Override
    protected String doInBackground(String... params) {

        BTFollowupTemplateDataSource dataSource = new BTFollowupTemplateDataSource(NavigationActivity.getContext()) ;

        List<BTFollowupTemplate> list =  dataSource.getAllTempalte(ba_id) ;


        for (int i =  0  ; i < list.size() ; i++){
            if(!list.get(i).isSynchonized()){
                TemplateUploadManager templateUploadManager = new TemplateUploadManager(NavigationActivity.getContext() , list.get(i)) ;
                templateUploadManager.execute() ;
            }
        }


        return null;
    }
}
