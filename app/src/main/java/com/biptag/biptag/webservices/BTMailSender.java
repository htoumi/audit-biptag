package com.biptag.biptag.webservices;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import com.biptag.biptag.Constant;
import com.biptag.biptag.JSONParser;
import com.biptag.biptag.NavigationActivity;
import com.biptag.biptag.R;
import com.biptag.biptag.managers.BiptagUserManger;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

public class BTMailSender extends AsyncTask<String, String, String>  {
	String ba_id  ; 
	String user_id ; 
	String do_follow_up  = "1" ;
	String user_email ; 
	String email ; 
	String contact_id ; 
	String text ; 
	String cci , cc ; 
	String subject ; 
	Context context ;  
	
	JSONObject sendEmailObject ; 
	
	public BTMailSender(Context c , String text , String subject , String email , String cc , String cci , String contact_id  ) {
	// TODO Auto-generated constructor stub
		this.context = NavigationActivity.getContext() ; 
		ba_id = String.valueOf(BiptagUserManger.getUserValue("BA_ID", "int", context)) ; 
		user_id =String.valueOf(BiptagUserManger.getUserValue("USER_ID", "int", context)) ;
		user_email =String.valueOf(BiptagUserManger.getUserValue("EMAIL", "string", context)) ;
		this.email = email ; 
		this.text = text ; 
		this.subject = subject  ; 
		this.cc = cc ; 
		this.cci = cci ; 
		this.contact_id =contact_id ; 
	}

	@Override
	protected String doInBackground(String... params) {
		// TODO Auto-generated method stub
		
		
		String url = Constant.base_url+Constant.send_mail ;
		List<NameValuePair> list = new ArrayList<NameValuePair>() ; 
		list.add(new BasicNameValuePair("ba_id", ba_id)) ; 
		list.add(new BasicNameValuePair("user_id", user_id)) ; 
		list.add(new BasicNameValuePair("do_follow_up", "1")) ; 
		list.add(new BasicNameValuePair("user_email", user_email)) ; 
		list.add(new BasicNameValuePair("email", email)) ; 
		list.add(new BasicNameValuePair("contact_id", contact_id)) ; 
		list.add(new BasicNameValuePair("text", text)) ; 
		list.add(new BasicNameValuePair("subject", subject)) ; 
		list.add(new BasicNameValuePair("cc", cc)) ; 
		list.add(new BasicNameValuePair("cci", cci)) ; 
		JSONParser parser = new JSONParser() ; 
		sendEmailObject = parser.makeHttpRequest(url, "GET", list) ; 
		if(sendEmailObject != null){
			//Log.d("send email object ", "send email object "+sendEmailObject.toString()) ; 
		}
		
		return null;
	}
}
