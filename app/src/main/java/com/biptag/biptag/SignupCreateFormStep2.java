package com.biptag.biptag;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.biptag.biptag.dbmanager.datasource.BTFormDataSource;
import com.biptag.biptag.dbmanager.objects.BTFormulaire;
import com.biptag.biptag.interfaces.AsyncResponse;
import com.biptag.biptag.managers.BiptagUserManger;
import com.biptag.biptag.webservices.CreateFormWs;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;

public class SignupCreateFormStep2 extends Activity implements AsyncResponse {
	
	EditText eventName , eventEdition ; 
	DatePicker startDate , endDate ; 
	Button createButton ;  

	@Override 
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_signup_create_form_step2);
		
		
		ActionBar mActionBar = getActionBar();
		mActionBar.setDisplayShowHomeEnabled(false);
		mActionBar.setDisplayShowTitleEnabled(false);
		LayoutInflater mInflater = LayoutInflater.from(this);

		View mCustomView = mInflater.inflate(R.layout.custom_actionbar, null);
		ActionBar.LayoutParams actionParams = new ActionBar.LayoutParams(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT) ;
		mCustomView.setLayoutParams(actionParams) ; 
		mActionBar.setCustomView(mCustomView);
		mActionBar.setDisplayShowCustomEnabled(true);
		
		final String form_structureString = getIntent().getExtras().getString("form_structure") ; 
		//Log.d("create form ", "create form structure "+form_structureString) ;
		
		startDate = (DatePicker ) findViewById(R.id.create_form_step2_start_date) ; 
		endDate = (DatePicker ) findViewById(R.id.create_form_step2_end_date) ; 
		eventName = (EditText) findViewById(R.id.create_form_step2_salon) ; 
		eventEdition = (EditText) findViewById(R.id.create_form_step2_edition) ; 
		createButton = (Button) findViewById(R.id.create_form_step2_ok) ; 
		eventName.addTextChangedListener(new TextWatcher() {
			String originalSubject ; 
			
			
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
				if(eventName.getText().toString().length() !=0){
					if(eventName.getText().toString().charAt(eventName.getText().toString().length()-1) =='\n'){
						//Log.d("edit follow up keycode", "edit follow up keycode enter") ;
						eventName.setText(originalSubject) ; 
					}	 
				}
				
			}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub
				originalSubject = eventName.getText().toString() ; 
				if(originalSubject.length()!=0){
					originalSubject = originalSubject.replaceAll("\\n","");
				}
			
			}
			
			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				
				originalSubject = eventName.getText().toString() ; 
				eventName.setSelection(eventName.getText().toString().length()) ; 
			}
		})   ; 
		eventEdition.addTextChangedListener(new TextWatcher() {
			String originalSubject ; 
			
			
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
				if(eventEdition.getText().toString().length() !=0){
					if(eventEdition.getText().toString().charAt(eventEdition.getText().toString().length()-1) =='\n'){
						//Log.d("edit follow up keycode", "edit follow up keycode enter") ;
						eventEdition.setText(originalSubject) ; 
					}	 
				}
				
			}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub
				originalSubject = eventEdition.getText().toString() ; 
				if(originalSubject.length()!=0){
					originalSubject = originalSubject.replaceAll("\\n","");
				}
			
			}
			 
			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				
				originalSubject = eventEdition.getText().toString() ; 
				eventEdition.setSelection(eventEdition.getText().toString().length()) ; 
			}
		})   ; 
		createButton.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				
				if(eventName.getText().toString().length() != 0){
					
					AlertDialog.Builder builder = new AlertDialog.Builder(SignupCreateFormStep2.this) ; 
					builder.setTitle(getApplicationContext().getResources().getString(R.string.form_name_title)) ;
					builder.setMessage(getApplicationContext().getResources().getString(R.string.form_name_desc)) ;
					final EditText formNameEditText = new EditText(getApplicationContext()) ; 
					formNameEditText.addTextChangedListener(new TextWatcher() {
						String originalSubject ; 
						
						
						@Override
						public void onTextChanged(CharSequence s, int start, int before, int count) {
							// TODO Auto-generated method stub
							if(formNameEditText.getText().toString().length() !=0){
								if(formNameEditText.getText().toString().charAt(formNameEditText.getText().toString().length()-1) =='\n'){
									//Log.d("edit follow up keycode", "edit follow up keycode enter") ;
									formNameEditText.setText(originalSubject) ; 
								}	 
							}
							
						}
						
						@Override
						public void beforeTextChanged(CharSequence s, int start, int count,
								int after) {
							// TODO Auto-generated method stub
							originalSubject = formNameEditText.getText().toString() ; 
							if(originalSubject.length()!=0){
								originalSubject = originalSubject.replaceAll("\\n","");
							}
						
						}
						
						@Override
						public void afterTextChanged(Editable s) {
							// TODO Auto-generated method stub
							 
							originalSubject = formNameEditText.getText().toString() ; 
							formNameEditText.setSelection(formNameEditText.getText().toString().length()) ; 
						}
					}) ;  
					
					formNameEditText.setTextColor(Color.BLACK) ;
					formNameEditText.setBackgroundColor(Color.WHITE);
					builder.setView(formNameEditText) ; 
					builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
						
						@Override
						public void onClick(DialogInterface dialog, int which) {
							// TODO Auto-generated method stub
							String formName  = formNameEditText.getText().toString() ;
							if(formName.length() != 0){
								
								Date start =  getDateFromDatePicker(startDate) ; 
								Date end = getDateFromDatePicker(endDate) ;
								
								SimpleDateFormat format= new SimpleDateFormat("yyyy-MM-dd") ; 
								String startString = format.format(start) ; 
								String endString = format.format(end) ; 
								try {
									JSONArray formStructure = new JSONArray(form_structureString) ; 
									  
									JSONObject element = new JSONObject() ; 
									JSONArray values = new JSONArray()  ;
									element.put("type", "title") ;  
									values.put(formName) ;  
									element.put("VALUE",values ) ; 
									formStructure.put(element) ;
										//Log.d("date", "date start "+startString+" end "+endString) ;
									CreateFormWs formWs = new CreateFormWs(formStructure.toString(), eventName.getText().toString(), eventEdition.getText().toString(), startString, endString, formName, SignupCreateFormStep2.this) ; 
									formWs.response = SignupCreateFormStep2.this ; 
									formWs.execute() ; 

									 
								} catch (JSONException e) {
									// TODO: handle exception
									//Log.d("create form step 2 exception ", "create form exception "+e.getMessage()) ;
								
								} 
								
								
							}else{
								AlertDialog.Builder builder = new AlertDialog.Builder(SignupCreateFormStep2.this) ;
								builder.setMessage(NavigationActivity.getContext().getResources().getString(R.string.form_name_err)) ;
								builder.setNeutralButton("OK", new DialogInterface.OnClickListener() {

									@Override
									public void onClick(DialogInterface dialog, int which) {
										// TODO Auto-generated method stub
										dialog.dismiss();
									}
								}) ; 
								
								builder.show() ; 
							}	
						}
					}) ; 
					
					builder.show() ; 
					
					
				}else{
					AlertDialog.Builder builder = new AlertDialog.Builder(SignupCreateFormStep2.this) ;
					builder.setMessage(NavigationActivity.getContext().getResources().getString(R.string.salon_name_err)) ;
					builder.setNeutralButton("OK", new DialogInterface.OnClickListener() {
						
						@Override
						public void onClick(DialogInterface dialog, int which) {
							// TODO Auto-generated method stub
							dialog.dismiss() ; 
						}
					}) ; 
					
					builder.show() ; 
				}
				
				
			}
		}) ; 
		
		
		
		
		
		
		
		
		
	}
 
	
	public static java.util.Date getDateFromDatePicker(DatePicker datePicker){
	    int day = datePicker.getDayOfMonth();
	    int month = datePicker.getMonth();
	    int year =  datePicker.getYear();

	    Calendar calendar = Calendar.getInstance();
	    calendar.set(year, month, day);

	    return calendar.getTime();
	}
	



	@Override
	public void processFinish(String result) {
		// TODO Auto-generated method stub
		
		if(result.length() !=0){
			try { 
				JSONObject object = new JSONObject(result);
				if(object.getInt("success")== 1){
						
					Object agent_id_object = BiptagUserManger.getUserValue("AGENT_ID", "int", SignupCreateFormStep2.this);
					int agent_id = Integer.parseInt(String.valueOf(agent_id_object)) ;
					String formStructureString = object.getJSONArray("FORM_STRUCTURE").toString() ; 
					formStructureString = formStructureString.replaceAll("\u00c3", "") ; 
					BTFormulaire formulaire = new BTFormulaire() ; 
					formulaire.setAgentId(agent_id) ; 
					formulaire.setEventEdition(object.getString("EV_EDITION")) ; 
					formulaire.setEventEndDate(object.getString("EV_END_DATE")) ; 
					formulaire.setEventStartDate(object.getString("EV_START_DATE")) ; 
					formulaire.setFormId(object.getInt("FORM_ID")) ; 
					formulaire.setFormName(object.getString("FORM_LABEL")) ; 
					formulaire.setFormStatus(object.getString("FORM_STATUS")) ; 
					formulaire.setFormStructure(new JSONArray(formStructureString)) ; 
					//Log.d("saving form ", "saving form  "+object.getString("EV_END_DATE") +"  "+formulaire.getEventEndDate()) ;
					  
					BTFormDataSource dataSource = new BTFormDataSource(SignupCreateFormStep2.this) ; 
					dataSource.insertForm(formulaire) ; 
					
					
					
					
					
					
					
					Intent intent = new Intent(SignupCreateFormStep2.this , NavigationActivity.class) ;

					startActivity(intent) ; 
					
					
				}else{
					AlertDialog.Builder builder = new AlertDialog.Builder(SignupCreateFormStep2.this) ;
					builder.setTitle(NavigationActivity.getContext().getResources().getString(R.string.form_err)) ;
					builder.setMessage(NavigationActivity.getContext().getResources().getString(R.string.form_err_desc)) ;
					builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
						
						@Override
						public void onClick(DialogInterface dialog, int which) {
							// TODO Auto-generated method stub
							dialog.dismiss() ; 
						} 
					}) ; 
				}
			} catch (JSONException e) {
				// TODO: handle exception
				//Log.d("saveing form ", "saving form exception "+e.getMessage()) ;
			}
		}
		
		
	}


	@Override
	public void processFailed() {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void synchronisationStepUp() {
		// TODO Auto-generated method stub
		
	}
}
