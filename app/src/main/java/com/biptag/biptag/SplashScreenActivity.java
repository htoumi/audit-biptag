package com.biptag.biptag;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;

import com.biptag.biptag.camera.ShutterButton;
import com.biptag.biptag.dbmanager.datasource.BtUserConfigDataSource;
import com.biptag.biptag.dbmanager.objects.BTUserConfig;
import com.biptag.biptag.language.LanguageCodeHelper;
import com.biptag.biptag.managers.BiptagUserManger;
import com.biptag.biptag.ocr.BeepManager;
import com.biptag.biptag.ocr.CaptureActivity;
import com.biptag.biptag.ocr.CaptureActivityHandler;
import com.biptag.biptag.ocr.FinishListener;
import com.biptag.biptag.ocr.OcrCharacterHelper;
import com.biptag.biptag.ocr.OcrInitAsyncTaskCon;
import com.biptag.biptag.ocr.PreferencesActivity;
import com.googlecode.tesseract.android.TessBaseAPI;

import java.io.File;
import java.util.Locale;

public class SplashScreenActivity extends Activity {
    private final int SPLASH_DISPLAY_LENGTH = 1000;
    private static final String TAG = SplashScreenActivity.class.getSimpleName();
    private SharedPreferences prefs;
    private boolean isEngineReady;
    private boolean isPaused;
    private int pageSegmentationMode = TessBaseAPI.PageSegMode.PSM_AUTO_OSD;
    private int ocrEngineMode = TessBaseAPI.OEM_TESSERACT_ONLY;
    private ProgressDialog dialog;
    private ProgressDialog indeterminateDialog;
    private BeepManager beepManager;
    private SharedPreferences.OnSharedPreferenceChangeListener listener;
    private static final boolean DISPLAY_SHUTTER_BUTTON = true;
    private ShutterButton shutterButton;
    private boolean isContinuousModeActive;
    private String sourceLanguageReadable;
    private String sourceLanguageCodeOcr;
    private String characterBlacklist;
    private String characterWhitelist;
    private boolean hasSurface;
    public static final String DOWNLOAD_BASE = "http://tesseract-ocr.googlecode.com/files/";
    public static final String OSD_FILENAME = "tesseract-ocr-3.01.osd.tar";
    public static final String OSD_FILENAME_BASE = "osd.traineddata";

    // Languages for which Cube data is available.
    static final String[] CUBE_SUPPORTED_LANGUAGES = {
            "ara", // Arabic
            "eng", // English
            "fra" // franÃ§ais
    };

    // Languages that require Cube, and cannot run using Tesseract.
    private static final String[] CUBE_REQUIRED_LANGUAGES = {
            "ara" // Arabic
    };
    private TessBaseAPI baseApi;
    private CaptureActivityHandler handler;
    static final int MINIMUM_MEAN_CONFIDENCE = 0;

    Handler getHandler() {
        Log.d("API ", "getHandler()");

        return handler;
    }

    TessBaseAPI getBaseApi() {
        Log.d("API ", "tessbasepi");
        return baseApi;
    }

    String languagecode = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().requestFeature(Window.FEATURE_NO_TITLE);

        Log.d(" lang  ", " splashscreenactivity " + LanguageCode());


        String previousSourceLanguageCodeOcr = sourceLanguageCodeOcr;
        int previousOcrEngineMode = ocrEngineMode;
        retrievePreferences();

        boolean doNewInit = (baseApi == null) || !sourceLanguageCodeOcr.equals(previousSourceLanguageCodeOcr) ||
                ocrEngineMode != previousOcrEngineMode;
        if (doNewInit) {
            // Initialize the OCR engine

            //			CaptureActivity captureActivity= new CaptureActivity();
            //									Intent intent= new Intent(captureActivity,getClass());
            //									intent.putExtra("newinit", doNewInit);

            Log.d(" ocr ", " donewinit ");
            File storageDirectory = getStorageDirectory();
            if (storageDirectory != null) {
                Log.d(" ocr ", " storage not nul ");
                initOcrEngine(storageDirectory, sourceLanguageCodeOcr, sourceLanguageReadable);
            }
        } else {
            // We already have the engine initialized, so just start the camera.
            resumeOCR();
        }


        setContentView(R.layout.activity_splash_screen);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                    /* Create an Intent that will start the Menu-Activity. */
                try {

                    int agent_id = Integer.parseInt(String.valueOf(BiptagUserManger.getUserValue("AGENT_ID", "int", SplashScreenActivity.this)));

                    BtUserConfigDataSource source = new BtUserConfigDataSource(SplashScreenActivity.this);
                    BTUserConfig config = source.getUserConfig(agent_id);
                    if (config != null) {
                        String language_code = "";
                        if (config.getLanguage().equals("Français")) {
                            language_code = "fr";


                        } else {
                            language_code = "en";
                        }

                        Log.d(" lang ", config.getLanguage());
                        Resources res = getResources();
                        // Change locale settings in the app.
                        DisplayMetrics dm = res.getDisplayMetrics();
                        android.content.res.Configuration conf = res.getConfiguration();
                        conf.locale = new Locale(language_code.toLowerCase());
                        res.updateConfiguration(conf, dm);


                    } else {
                        Log.d(" lang ", " config nul ");
                    }


                } catch (NumberFormatException e) {
                    // TODO: handle exception
                }


                Intent mainIntent = new Intent(SplashScreenActivity.this, DefaultHome.class);
                SplashScreenActivity.this.startActivity(mainIntent);
                SplashScreenActivity.this.finish();
            }
        }, SPLASH_DISPLAY_LENGTH);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.splash_screen, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    public void resumeOCR() {
        //Log.d(TAG, "resumeOCR()");

        // This method is called when Tesseract has already been successfully initialized, so set
        // isEngineReady = true here.
        isEngineReady = true;

        isPaused = false;

        if (handler != null) {
            handler.resetState();
        }
        if (baseApi != null) {
            baseApi.setPageSegMode(pageSegmentationMode);
            baseApi.setVariable(TessBaseAPI.VAR_CHAR_BLACKLIST, characterBlacklist);
            baseApi.setVariable(TessBaseAPI.VAR_CHAR_WHITELIST, characterWhitelist);
        }

        if (hasSurface) {
            // The activity was paused but not stopped, so the surface still exists. Therefore
            // surfaceCreated() won't be called, so init the camera here.
            //initCamera(surfaceHolder);
        }
    }

    private boolean setSourceLanguage(String languageCode) {
        //Log.d(" ocr ", " language code " + languageCode);
        sourceLanguageCodeOcr = languageCode;
        sourceLanguageReadable = LanguageCodeHelper.getOcrLanguageName(this, languageCode);
        return true;
    }

    public void showErrorMessage(String title, String message) {
        new AlertDialog.Builder(this)
                .setTitle(title)
                .setMessage(message)
                .setOnCancelListener(new FinishListener(this))
                .setPositiveButton("Done", new FinishListener(this))
                .show();
    }

    public void setButtonVisibility(boolean visible) {
        if (shutterButton != null && visible == true && DISPLAY_SHUTTER_BUTTON) {
            shutterButton.setVisibility(View.VISIBLE);
        } else if (shutterButton != null) {
            shutterButton.setVisibility(View.GONE);
        }
    }

    private File getStorageDirectory() {

        String state = null;
        try {
            state = Environment.getExternalStorageState();
        } catch (RuntimeException e) {
            //Log.e(TAG, "Is the SD card visible?", e);
            showErrorMessage("Error", "Required external storage (such as an SD card) is unavailable.");
        }
        if (Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {

            try {
                return getExternalFilesDir(Environment.MEDIA_MOUNTED);
            } catch (NullPointerException e) {
                // We get an error here if the SD card is visible, but full
                //Log.e(TAG, "External storage is unavailable");
                showErrorMessage("Error", "Required external storage (such as an SD card) is full or unavailable.");
            }

        } else if (Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
            // We can only read the media
            //Log.e(TAG, "External storage is read-only");
            showErrorMessage("Error", "Required external storage (such as an SD card) is unavailable for data storage.");
        } else {
            // Something else is wrong. It may be one of many other states, but all we need
            // to know is we can neither read nor write
            //Log.e(TAG, "External storage is unavailable");
            showErrorMessage("Error", "Required external storage (such as an SD card) is unavailable or corrupted.");
        }
        return null;
    }

    private void initOcrEngine(File storageRoot, String languageCode, String languageName) {
        isEngineReady = false;

        // Set up the dialog box for the thermometer-style download progress indicator
        if (dialog != null) {
            dialog.dismiss();
        }
        dialog = new ProgressDialog(this);
        // If we have a language that only runs using Cube, then set the ocrEngineMode to Cube
        if (ocrEngineMode != TessBaseAPI.OEM_CUBE_ONLY) {
            for (String s : CUBE_REQUIRED_LANGUAGES) {
                if (s.equals(languageCode)) {
                    ocrEngineMode = TessBaseAPI.OEM_CUBE_ONLY;
                    SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
                    prefs.edit().putString(PreferencesActivity.KEY_OCR_ENGINE_MODE, getOcrEngineModeName()).commit();
                }
            }
        }
        // If our language doesn't support Cube, then set the ocrEngineMode to Tesseract
        if (ocrEngineMode != TessBaseAPI.OEM_TESSERACT_ONLY) {
            boolean cubeOk = false;
            for (String s : CUBE_SUPPORTED_LANGUAGES) {
                if (s.equals(languageCode)) {
                    cubeOk = true;
                    Log.d(" ocr ", " if languagecode " + cubeOk);
                }
            }
            if (!cubeOk) {
                Log.d(" ocr ", " if not cube ok  ");
                ocrEngineMode = TessBaseAPI.OEM_TESSERACT_ONLY;
                SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
                prefs.edit().putString(PreferencesActivity.KEY_OCR_ENGINE_MODE, getOcrEngineModeName()).commit();
            }
        }

        // Display the name of the OCR engine we're initializing in the indeterminate progress dialog box
        indeterminateDialog = new ProgressDialog(this);
        Log.d(" ocr ", " " + indeterminateDialog.getProgress());
        indeterminateDialog.setTitle(" please wait ");
        String ocrEngineModeName = getOcrEngineModeName();
        Log.d(" ocr ", " ocrenginemodeName " + ocrEngineModeName);

        if (ocrEngineModeName.equals("Both")) {
            Log.d(" ocr ", " if ocrenginemodeName " + ocrEngineModeName);

            indeterminateDialog.setMessage(NavigationActivity.getContext().getResources().getString(R.string.indeterminateDialog_message) + languageName + "...");
        } else {
            Log.d(" ocr ", "else ocrenginemodeName " + ocrEngineModeName);
            indeterminateDialog.setMessage("Initialisation" + ocrEngineModeName + " OCR engine for " + languageName + "...");
        }
        indeterminateDialog.setCancelable(false);
        //indeterminateDialog.show();

        if (handler != null) {
            handler.quitSynchronously();
        }
        Log.d(" ocr ", " ocrenginemode " + ocrEngineMode + " ocrEngineModeName " + ocrEngineModeName);
        // Disable continuous mode if we're using Cube. This will prevent bad states for devices
        // with low memory that crash when running OCR with Cube, and prevent unwanted delays.
        if (ocrEngineMode == TessBaseAPI.OEM_CUBE_ONLY || ocrEngineMode == TessBaseAPI.OEM_TESSERACT_CUBE_COMBINED) {
            Log.d(" ocr ", " if ocrenginemode " + ocrEngineMode);

            Log.d(" ocr ", "Disabling continuous preview");
            isContinuousModeActive = false;
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
            prefs.edit().putBoolean(PreferencesActivity.KEY_CONTINUOUS_PREVIEW, false);
        }

        // Start AsyncTask to install language data and init OCR
        baseApi = new TessBaseAPI();
        Log.d(" ocr ", " ocrinitasynctaskcon " + " languageCode " + languageCode + " languageName " + languageName + " ocrEngineMode " + ocrEngineMode);
        new OcrInitAsyncTaskCon(this, baseApi, dialog, indeterminateDialog, languageCode, languageName, ocrEngineMode)
                .execute(storageRoot.toString());
    }

    String getOcrEngineModeName() {
        String ocrEngineModeName = "";
        String[] ocrEngineModes = getResources().getStringArray(R.array.ocrenginemodes);
        if (ocrEngineMode == TessBaseAPI.OEM_TESSERACT_ONLY) {
            ocrEngineModeName = ocrEngineModes[0];
        } else if (ocrEngineMode == TessBaseAPI.OEM_CUBE_ONLY) {
            ocrEngineModeName = ocrEngineModes[1];
        } else if (ocrEngineMode == TessBaseAPI.OEM_TESSERACT_CUBE_COMBINED) {
            ocrEngineModeName = ocrEngineModes[2];
        }
        return ocrEngineModeName;
    }

    private void retrievePreferences() {
        prefs = PreferenceManager.getDefaultSharedPreferences(this);
        // Retrieve from preferences, and set in this Activity, the language preferences
        PreferenceManager.setDefaultValues(this, R.xml.preferences, false);


        //	setSourceLanguage(LanguageCode());

        setSourceLanguage(prefs.getString(PreferencesActivity.KEY_SOURCE_LANGUAGE_PREFERENCE, CaptureActivity.DEFAULT_SOURCE_LANGUAGE_CODE));
        // Retrieve from preferences, and set in this Activity, the capture mode preference
        if (prefs.getBoolean(PreferencesActivity.KEY_CONTINUOUS_PREVIEW, CaptureActivity.DEFAULT_TOGGLE_CONTINUOUS)) {
            isContinuousModeActive = true;
        } else {
            isContinuousModeActive = false;
        }
        // Retrieve from preferences, and set in this Activity, the page segmentation mode preference
        String[] pageSegmentationModes = getResources().getStringArray(R.array.pagesegmentationmodes);
        String pageSegmentationModeName = prefs.getString(PreferencesActivity.KEY_PAGE_SEGMENTATION_MODE, pageSegmentationModes[0]);
        if (pageSegmentationModeName.equals(pageSegmentationModes[0])) {
            pageSegmentationMode = TessBaseAPI.PageSegMode.PSM_AUTO_OSD;
        } else if (pageSegmentationModeName.equals(pageSegmentationModes[1])) {
            pageSegmentationMode = TessBaseAPI.PageSegMode.PSM_AUTO;
        } else if (pageSegmentationModeName.equals(pageSegmentationModes[2])) {
            pageSegmentationMode = TessBaseAPI.PageSegMode.PSM_SINGLE_BLOCK;
        } else if (pageSegmentationModeName.equals(pageSegmentationModes[3])) {
            pageSegmentationMode = TessBaseAPI.PageSegMode.PSM_SINGLE_CHAR;
        } else if (pageSegmentationModeName.equals(pageSegmentationModes[4])) {
            pageSegmentationMode = TessBaseAPI.PageSegMode.PSM_SINGLE_COLUMN;
        } else if (pageSegmentationModeName.equals(pageSegmentationModes[5])) {
            pageSegmentationMode = TessBaseAPI.PageSegMode.PSM_SINGLE_LINE;
        } else if (pageSegmentationModeName.equals(pageSegmentationModes[6])) {
            pageSegmentationMode = TessBaseAPI.PageSegMode.PSM_SINGLE_WORD;
        } else if (pageSegmentationModeName.equals(pageSegmentationModes[7])) {
            pageSegmentationMode = TessBaseAPI.PageSegMode.PSM_SINGLE_BLOCK_VERT_TEXT;
        } else if (pageSegmentationModeName.equals(pageSegmentationModes[8])) {
            pageSegmentationMode = TessBaseAPI.PageSegMode.PSM_SPARSE_TEXT;
        }

        // Retrieve from preferences, and set in this Activity, the OCR engine mode
        String[] ocrEngineModes = getResources().getStringArray(R.array.ocrenginemodes);
        String ocrEngineModeName = prefs.getString(PreferencesActivity.KEY_OCR_ENGINE_MODE, ocrEngineModes[0]);
        if (ocrEngineModeName.equals(ocrEngineModes[0])) {
            ocrEngineMode = TessBaseAPI.OEM_TESSERACT_ONLY;
        } else if (ocrEngineModeName.equals(ocrEngineModes[1])) {
            ocrEngineMode = TessBaseAPI.OEM_CUBE_ONLY;
        } else if (ocrEngineModeName.equals(ocrEngineModes[2])) {
            ocrEngineMode = TessBaseAPI.OEM_TESSERACT_CUBE_COMBINED;
        }

        // Retrieve from preferences, and set in this Activity, the character blacklist and whitelist
        characterBlacklist = OcrCharacterHelper.getBlacklist(prefs, sourceLanguageCodeOcr);
        characterWhitelist = OcrCharacterHelper.getWhitelist(prefs, sourceLanguageCodeOcr);

        prefs.registerOnSharedPreferenceChangeListener(listener);

        //beepManager.updatePrefs();
    }

    public String LanguageCode() {

        String languagecode = "";
        Resources res = getResources();
        // Change locale settings in the app.
        DisplayMetrics dm = res.getDisplayMetrics();
        android.content.res.Configuration conf = res.getConfiguration();
        Log.d(" lang ", " " + conf.locale.getDisplayLanguage());
        languagecode = conf.locale.getDisplayLanguage();
        int agent_id = Integer.parseInt(String.valueOf(BiptagUserManger.getUserValue("AGENT_ID", "int", SplashScreenActivity.this)));

        BtUserConfigDataSource source = new BtUserConfigDataSource(SplashScreenActivity.this);
        BTUserConfig config = source.getUserConfig(agent_id);
        if (config != null) {

            if (config.getLanguage().equals("Français")) {
                languagecode = "fra";
                return languagecode;
            } else {
                languagecode = "eng";
                return languagecode;
            }

        }


        return languagecode;

    }

}
