package com.biptag.biptag.regularepression;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

public class FullNameValidator {

	private Pattern pattern;
	private Matcher matcher;
	
	private static final String FULLNAME_PATTERN=
			"^[A-Za-z]\t[A-Za-z]$";
	public  FullNameValidator() {
  
	}
	//validate hex with regular expression

	public boolean validate(String name) {
	    char[] chars = name.toCharArray();

	    for (char c : chars) {
	        if(!Character.isLetter(c) && c != ' ') {
	            return false;
	        }
	    }

	    return true;
	}
}
