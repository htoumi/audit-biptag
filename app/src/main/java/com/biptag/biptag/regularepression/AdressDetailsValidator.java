package com.biptag.biptag.regularepression;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import android.util.Log;

public class AdressDetailsValidator {

	private Pattern pattern;
	private Matcher matcher;
	
	private static final String ADRESS_PATTERN=
			"[0-9]{4,6}(([,. ]?){1}[a-zA-Zàâäéèêëïîôöùûüç']+)*$";
	public  AdressDetailsValidator() {
		
	try {
		 pattern=Pattern.compile(ADRESS_PATTERN);
	} catch (PatternSyntaxException e) {
		// TODO: handle exception
	}
	}
	//validate hex with regular expression

	public boolean validate(final String hex){
		matcher= pattern.matcher(hex);
		//Log.d("AdressValidator", "constructeur "+hex);
		return matcher.matches();
	}
}
