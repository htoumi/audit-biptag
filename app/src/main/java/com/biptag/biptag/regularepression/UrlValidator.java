package com.biptag.biptag.regularepression;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

public class UrlValidator {
	private Pattern pattern;
	private Matcher matcher;
	
	private static final String URL_PATTERN= "((http|https):\\/\\/)|[w]{3,3}.[A-Za-z\\.]*\\.(com|org|net)";
		
			public UrlValidator() {
		//Pattern returns a compiled form of the given regularExpression, 
		// as modified by the given flags. See the flags overview for 
		 //more on flags. Throws
		//PatternSyntaxException if the regular expression is 
		// syntactically incorrect
	try {
		 pattern=Pattern.compile(URL_PATTERN);
	} catch (PatternSyntaxException e) {
		// TODO: handle exception
	}
	}
	//validate hex with regular expression

	public boolean validate(final String hex){
		//matcher:Returns a Matcher for this pattern applied to the given 
		// input. The Matcher can be used to match the Pattern 
		 //against the whole input, find occurrences of the Pattern in 
		 //the input, or replace parts of the input.
		matcher= pattern.matcher(hex);
		return matcher.matches();
	}

}
