package com.biptag.biptag.regularepression;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import android.util.Log;

public class EmailValidator {
	private Pattern pattern;
	private Matcher matcher;
	
	private static final String EMAIL_PATTERN=
			"^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
					+ "[A-Za-z0-9-]$";
	//"^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
	//+ "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
	public  EmailValidator() {
		//Returns a compiled form of the given regularExpression, 
		// as modified by the given flags. See the flags overview for 
		 //more on flags. Throws
		//PatternSyntaxException if the regular expression is 
		// syntactically incorrect

	try {
		 pattern=Pattern.compile(EMAIL_PATTERN);
	} catch (PatternSyntaxException e) {
		// TODO: handle exception
	}
       // pattern=Pattern.compile(EMAIL_PATTERN);
        
	}
	//validate hex with regular expression
	
	public boolean validate(final String hex){
		//matcher:Returns a Matcher for this pattern applied to the given 
		// input. The Matcher can be used to match the Pattern 
		 //against the whole input, find occurrences of the Pattern in 
		 //the input, or replace parts of the input.
		matcher= pattern.matcher(hex);
		//Log.d("EmailValidator", "constructeur "+hex+"  "+(hex.indexOf("@") != -1));
		
		return hex.indexOf("@") != -1;
	}
}
