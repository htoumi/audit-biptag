package com.biptag.biptag.interfaces;

public interface AsyncResponse {
	
	public void processFinish(String result );
	public void processFailed()  ; 
	public void synchronisationStepUp()  ; 

} 
