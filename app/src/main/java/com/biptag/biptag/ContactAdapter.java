package com.biptag.biptag;

import java.util.LinkedList;
import java.util.List;

import com.biptag.biptag.dbmanager.datasource.BTLogDataSource;
import com.biptag.biptag.dbmanager.objects.BTContact;
import com.biptag.biptag.dbmanager.objects.BTLog;
import com.biptag.biptag.managers.BiptagUserManger;

import android.app.Activity;
import android.content.Context;
import android.graphics.Point;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AbsListView.LayoutParams;

import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

public class ContactAdapter extends ArrayAdapter<ContactItem> implements Filterable{

    Context context; 
    int layoutResourceId;    
    public List<ContactItem> data = null;
    public List<ContactItem> dataCopy = null;
    int screenWidth  ;
	int screenHeight ;
    public  boolean mustFiltreFromCopy = false ;
    public List<ContactItem> items;
    public int filterCount=0;
    List<BTLog> allLogs = null ;
    public ContactAdapter(Context context, int layoutResourceId, List<ContactItem> data , List<BTLog> logs) {
        super(context, layoutResourceId, data);
    	this.layoutResourceId = layoutResourceId;
        this.context = context;
        this.data = data;
        this.dataCopy = data;
        Object agent_id_object = BiptagUserManger.getUserValue("AGENT_ID", "int", NavigationActivity.getContext());
        allLogs =logs ; 
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
		Display display  =wm.getDefaultDisplay() ;
		Point point  = new Point() ; 
		display.getSize(point) ; 
		 screenWidth = point.x ; 
		 screenHeight = point.y ; 

        
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        ContactHolder holder = null;
        
        if(row == null)
        {
            LayoutInflater inflater = ((Activity)context).getLayoutInflater();
            row = inflater.inflate(layoutResourceId, parent, false);
            
            holder = new ContactHolder();
            holder.imgIcon = (ImageView)row.findViewById(R.id.contact_selected_3_points);
            holder.fr_id = (TextView) row.findViewById(R.id.cr_fr_id) ;  
            holder.contactname = (TextView)row.findViewById(R.id.cr_contact_name);
            holder.contactResponse = (TextView)row.findViewById(R.id.cr_contact_response);
            holder.contact_id = (TextView)row.findViewById(R.id.cr_contact_id);
            holder.form_structure = (TextView)row.findViewById(R.id.cr_form_structure);
            holder.form_id = (TextView)row.findViewById(R.id.cr_form_id);
            holder.contact_email= (TextView) row.findViewById(R.id.cr_contact_email) ; 
            holder.isSynchronized= (TextView) row.findViewById(R.id.cr_issynchronized)  ; 
            holder.contact_local_id = (TextView) row.findViewById(R.id.cr_contact_local_id) ; 
            holder.notification_icon = (ImageView) row.findViewById(R.id.notification_icon) ; 
            row.setTag(holder); 
        }
        else
        {
            holder = (ContactHolder)row.getTag();
        }   
        
        ContactItem contact = data.get(position);
        holder.contactname.setText(contact.contact_name); 
        //holder.imgIcon.setImageResource(contact.icon);
        holder.contact_id.setText(String.valueOf(contact.contactId)) ; 
        holder.fr_id.setText(String.valueOf(contact.fr_id)) ; 
        holder.form_structure.setText(contact.form_structure.toString()) ; 
        holder.form_id.setText(String.valueOf(contact.form_id)) ; 
        holder.contactResponse.setText(contact.contactRespone.toString()) ;
        holder.isSynchronized.setText((contact.isSynchronized? "1" : "0")) ;
        holder.contact_local_id.setText(String.valueOf(contact.contact_local_id)) ; 
       //BTLogDataSource dataSource = new BTLogDataSource(getContext()) ; 
		//String agent_id = String.valueOf(BiptagUserManger.getUserValue("AGENT_ID", "int", getContext())) ; 
       //List<BTLog> list = dataSource.findLogsFormContact(Integer.parseInt(agent_id), contact.contactId) ; 
       boolean  logFound = false ; 
       for (int i = 0; i < allLogs.size(); i++) {
    	   if(contact.contactId ==  allLogs.get(i).getFl_contact_id()){
    		   logFound = true  ; 
    		   break ; 
    	   }
       
       }
       holder.notification_icon.setVisibility(( !logFound ? View.INVISIBLE  : View.VISIBLE)) ; 
        
       LayoutParams layoutParams = new LayoutParams(LayoutParams.MATCH_PARENT,screenHeight/18) ; 
        row.setLayoutParams(layoutParams) ; 
        
        return row;    
    }   
    
    public ContactItem getContactItem(int index ) {
		return data.get(index) ; 
	}
    
    
    static class ContactHolder 
    {
        ImageView imgIcon;
        TextView contactname ;
        TextView contactResponse ;
        TextView form_structure ;
        TextView fr_id ;
        TextView form_id ; 
        TextView contact_id ;
        TextView contact_email ;
        TextView isSynchronized ;
        TextView contact_local_id ;
        ImageView notification_icon; 
    }
    
    @Override
    public int getCount() {
    	// TODO Auto-generated method stub
    	return data !=null ? data.size() :0;
    }
    
    @Override
    public Filter getFilter() {
    	// TODO Auto-generated method stub
    	Filter filter = new Filter() {
			
			@SuppressWarnings("unchecked")
			@Override
			protected void publishResults(CharSequence constraint, FilterResults results) {
				// TODO Auto-generated method stub
				data = (List<ContactItem>)results.values ; 
				notifyDataSetChanged() ; 
				
			}
			
			@Override
			protected FilterResults performFiltering(CharSequence constraint) {
				// TODO Auto-generated method stub
				FilterResults results = new FilterResults() ; 
				items = new LinkedList<ContactItem>() ;
				String start  = constraint.toString().toLowerCase() ;

                List<ContactItem> source = data ;
                if(mustFiltreFromCopy){
                    source = dataCopy ;
                    mustFiltreFromCopy = false ;
                }

				for (int i = 0; i < source.size(); i++) {

					if(source.get(i).contact_name.toLowerCase().startsWith(start)){
						items.add(source.get(i) ) ;
					}
				}
				
				results.count = items.size() ;
                Log.d(" filter ", " result count " + results.count);
				results.values = items ; 
				filterCount = results.count;
				return results;
			}
           };
    	
    	return filter;
    }


    public void addContact(ContactItem contact){
        if(data != null){
            Log.d(" contact " , " added " + contact.contact_email);
            data.add(contact)  ;
        }
    }

    public void removeContact(ContactItem contact){
        if(data != null){

            for (int i = 0  ; i < data.size() ; i++){
                if(data.get(i).contact_local_id == contact.contact_local_id){
                    Log.d(" contact ", " remove contact ");
                    data.remove(i)  ;
                    break;

                }
            }
        }
    }

 public void removeContact(BTContact contact){
        if(data != null){

            for (int i = 0  ; i < data.size() ; i++){
                if(data.get(i).contact_local_id == contact.getContact_local_id()){
                    Log.d(" contact ", " remove contact ");
                    data.remove(i)  ;
                    break;

                }
            }
        }
    }


    public void updateContact(ContactItem item , int id ){
        if(data != null){
            for(int i = 0 ; i< data.size() ;i++){
                if(data.get(i).contact_local_id == id){
                    Log.d(" contact ", " update contact adapter ");
                    data.set(i, item) ;
                    break;
                }
            }
        }
    }
 public void updateContact2(ContactItem item , int id ){
        if(data != null){
            for(int i = 0 ; i< data.size() ;i++){
                if(data.get(i).contact_local_id == id) {
                    Log.d(" contact ", " update contact adapter ");
                    data.set(i, item);
                    if (data.get(i).contact_name.length()==0) {
                        data.get(i).contact_name= data.get(i).contact_email;
                    }
                }
                    break;
                }
            }
        }
    }
